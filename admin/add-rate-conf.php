<?php
// Session Handling
session_start();
// Including files
include ("../include/config.php");
include ("security.php");
include("connectOtherDataBase.php");
include("ledgersUpdation.php");
$agentType = getAgentType();
 ///////////////////////History is maintained via method named 'activities'

// $historicalID = $_POST["historicalID"];
 
 
 
if ($_GET["newOffset"]!="")
{
	$offset = $_GET["newOffset"];
}

$loggedUserID = $_SESSION["loggedUserData"]["userID"];

if($_POST["currencyOrigin"] == "")
{
	$currencyOrigin = selectFrom("SELECT currencyName  FROM  currencies where country = '".$_POST["countryOrigin"]."'");	
		$_SESSION["currencyOrigin"] = $currencyOrigin["currencyName"];
}else{
		$_SESSION["currencyOrigin"] = $_POST["currencyOrigin"];
}

	session_register("country");
	session_register("currency");
	session_register("primaryExchange");
	session_register("marginPercentage");
	session_register("rateFor");
	session_register("agentID");
	session_register("agentMarginType");
	session_register("agentMarginValue");
	
	$_SESSION["country"] = $_POST["country"];
	$_SESSION["countryOrigin"] = $_POST["countryOrigin"];
	$_SESSION["currency"] = $_POST["currency"];
	$_SESSION["primaryExchange"] = $_POST["primaryExchange"];
	$_SESSION["marginPercentage"] = $_POST["marginPercentage"];
	
	if($_POST["agentID"] == "" && $agentType == "SUPA" && CONFIG_ADD_SUB_AGENT_RATE == "1"){
		
		$_SESSION["rateFor"] = "agent";
		$_SESSION["agentID"] = $loggedUserID;
		
		}else{
	
	
					$_SESSION["rateFor"] = $_POST["rateFor"];
					$_SESSION["agentID"] = $_POST["agentID"];
		}
	
	$_SESSION["paymentMode"] = $_POST["paymentMode"];

	$_SESSION["agentMarginValue"] = $_POST["agentMarginValue"];

	
	$_SESSION["defaultRecievingCurrency"] = $_POST["defaultRecievingCurrency"];
	$_SESSION["defaultCurrencyOrigin"] = $_POST["defaultCurrencyOrigin"];
	$_SESSION["defaultCountryOrigin"] = $_POST["defaultCountryOrigin"];
	$_SESSION["defaultRecievingCountry"] = $_POST["defaultRecievingCountry"];
	
	$_SESSION["_24hrMarginType"] = $_POST["_24hrMarginType"];
	$_SESSION["_24hrMarginValue"] = $_POST["_24hrMarginValue"];


 $relatedRateId  = ($_GET["relatedRateId"] != "" ? $_GET["relatedRateId"] : $_POST["relatedRateId"]);

	if($_POST["marginType"] != '')
	{	
		$_SESSION["marginType"] = $_POST["marginType"];
	}else{
		$_SESSION["marginType"] = "percent";	
	}
	
	if($_POST["agentMarginType"] != '')
	{	
		$_SESSION["agentMarginType"] = $_POST["agentMarginType"];
	}else{
		$_SESSION["agentMarginType"] = "percent";	
	}


if ($_POST["erID"] == ""){

	
	
	if($_SESSION["rateFor"] == 'distributor' || $_SESSION["rateFor"] == 'transType'|| $_SESSION["rateFor"] == 'MoneyPaid' || $_SESSION["rateFor"] == 'agent' || $_SESSION["rateFor"] == 'intermediate' || $_SESSION["rateFor"] == 'customer')
	{
		$value = $_SESSION["agentID"];
	}elseif($_SESSION["rateFor"] == "money")
	{
		$value = $_POST["from"].",".$_POST["to"];
		
	}else{
		
		 $value = "";
		}
	
		$backURL = "add-rate.php?msg=Y&currency=" . $_POST["currency"];
		
} else {
	$backURL = "update-rate.php?erID=$_POST[erID]&msg=Y&newOffset=$offset";
}


if (trim($_POST["country"]) == ""){
	insertError(ER2);
	redirect($backURL);
}

if (trim($_POST["countryOrigin"]) == ""){
	insertError(ER1);
	redirect($backURL);
}

if($_REQUEST["denominationBased"] != "Y")
{
	if (trim($_POST["primaryExchange"]) == ""){
		insertError(ER4);
		redirect($backURL);
	}


	if (trim($_POST["marginPercentage"]) == ""){
		insertError(ER5);
		redirect($backURL);
	}

}
if (trim($_POST["currency"]) == ""){
	insertError(ER9);
	redirect($backURL);
}
if (trim($_POST["rateFor"]) == ""){
	insertError(ER10);
	redirect($backURL);
}
if (trim($_POST["rateFor"]) == "distributor"){
	if(trim($_POST["agentID"])== "")
	{
	insertError(ER11);
	redirect($backURL);
	}
}
if (trim($_POST["rateFor"]) == "money"){
	if(trim($_POST["from"])== "" || trim($_POST["to"] == ""))
	{
	insertError(ER12);
	redirect($backURL);
	}
}

if (trim($_POST["rateFor"]) == "intermediate"){
	if(trim($_POST["agentID"] == ""))
	{
	insertError(ER13);
	redirect($backURL);
	}
}

if (trim($_POST["rateFor"]) == "intermediate"){
	if(trim($_POST["intermediateExchangeRate"] == ""))
	{
	insertError(ER14);
	redirect($backURL);
	}
}

if (trim($_POST["rateFor"]) == "intermediate"){
	if(trim($_POST["intermediateDestExchangeRate"] == ""))
	{
	insertError(ER15);
	redirect($backURL);
	}
}


if (trim($_POST["rateFor"]) == "intermediate"){
	if(trim($_POST["intermediateCurrency"] == ""))
	{
	insertError(ER16);
	redirect($backURL);
	}
}	
if (trim($_POST["rateFor"]) == "customer"){
	if(trim($_POST["agentID"] == ""))
	{
	insertError(ER17);
	redirect($backURL);
	}	
}

///debug($_REQUEST, true); 

if ($_POST["dDate"] == "")
{
	$exchangeDate = "";
}
else
{
	$dDate = explode("/",$_POST["dDate"]);
	if(count($dDate) == 3)
	{
		$dDate = $dDate[2]."-".$dDate[1]."-".$dDate[0];
	}
	else
	{
		$dDate = $_POST["dDate"];
	}			
	$exchangeDate = $dDate;
}

// this comment for new functionality only add not update.


//debug($_REQUEST,true);

if ($_POST["erID"] == ""){

/*	if (!isExist("select erID from ".TBL_EXCHANGE_RATES." where country='$_POST[country]' and rateFor = '".$_SESSION["rateFor"]."' and rateValue = '".$value."'")){/// ...For money range there would be another condition...it is for distributor now.
		$Querry_Sqls = "insert into ".TBL_EXCHANGE_RATES." (country, currency, primaryExchange, marginPercentage, dated, rateFor, rateValue) values ('$_POST[country]', '$_POST[currency]', '".checkValues($_POST["primaryExchange"])."', '".checkValues($_POST["marginPercentage"])."', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$_SESSION["rateFor"]."', '".$value."')";
	} else {
		insertError("Exchange rate already exist. Please click Manage Rates and update that one.");
		redirect($backURL);
		//$Querry_Sqls = "update ".TBL_EXCHANGE_RATES." set dated='".getCountryTime(CONFIG_COUNTRY_CODE)."', country='$_POST[country]', currency='$_POST[currency]', primaryExchange='".checkValues($_POST["primaryExchange"])."', marginPercentage='".checkValues($_POST["marginPercentage"])."', rateFor = '".$_SESSION["rateFor"]."', rateValue = '".$value."' where country='$_POST[country]'";
	}*/
		/**
		 * To enter the erID field of auto generated from exchange rate table into related exchange rate table
		 * this variable will capture that is $relatedRateId == "" && CONFIG_SETTLEMENT_COMMISSION == '1' condition matched or not
		 * @Ticket #4041
		 */
		$bolIsNewRecordInsertedIntoRelatedTable = false;
		if($relatedRateId == "" && CONFIG_SETTLEMENT_COMMISSION == '1'){
			$bolIsNewRecordInsertedIntoRelatedTable = true;
			 $RelatedExchangeRateQuerry = "insert into ".TBL_RELATED_EXCHANGE_RATES." 
					(DefaultSendingCurrency,defaultRecievingCurrency,defaultSendingCountry,defaultRecievingCountry) 
					values ('$_POST[defaultCurrencyOrigin]', '$_POST[defaultRecievingCurrency]', '$_POST[defaultCountryOrigin]', '$_POST[defaultRecievingCountry]')";
			insertInto($RelatedExchangeRateQuerry);
			$relatedRateId = @mysql_insert_id();
		}
 	
		if($_REQUEST["denominationBased"] == "Y")
		{
			/* Denomination based Exchange Rate */
			$Querry_Sqls = "insert into ".TBL_EXCHANGE_RATES." 
							(countryOrigin, country, currency,  
							 dated, rateFor, rateValue, 
							 updationDate, currencyOrigin, marginType,
							 createdBy,updatedBy, isDenomination) 
							values 
							('".$_POST["countryOrigin"]."', '".$_POST["country"]."', '".$_POST["currency"]."', 
							 '".($exchangeDate != "" ? $exchangeDate : getCountryTime(CONFIG_COUNTRY_CODE))."', 
							 '".$_SESSION["rateFor"]."', '".$value."', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$_SESSION["currencyOrigin"]."', 
							 '".$_SESSION["marginType"]."', '".$loggedUserID."','".$loggedUserID."', 'Y')";

		}
		else
		{
			/* Normal Exchange Rate */
			$Querry_Sqls = "insert into ".TBL_EXCHANGE_RATES." 
							(countryOrigin, country, currency, primaryExchange, 
							 marginPercentage, dated, rateFor, rateValue, 
							 updationDate, currencyOrigin, marginType,agentMarginType, 
							 agentMarginValue,relatedID,createdBy,updatedBy) 
							values 
							('".$_POST["countryOrigin"]."', '".$_POST["country"]."', '".$_POST["currency"]."', '".checkValues($_POST["primaryExchange"])."', 
							 '".checkValues($_POST["marginPercentage"])."', '".($exchangeDate != "" ? $exchangeDate : getCountryTime(CONFIG_COUNTRY_CODE))."', 
							 '".$_SESSION["rateFor"]."', '".$value."', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$_SESSION["currencyOrigin"]."', 
							 '".$_SESSION["marginType"]."', '".$_SESSION["agentMarginType"]."', '".$_SESSION["agentMarginValue"]."',
							 '".$relatedRateId."','".$loggedUserID."',0)";
		}
		
	
		insertInto($Querry_Sqls);
		$ratId = @mysql_insert_id();

		/* @Ticket #4041 */	
		if($bolIsNewRecordInsertedIntoRelatedTable)
		{
			update("update ".TBL_RELATED_EXCHANGE_RATES." set erID = '".$ratId."' where id=".$relatedRateId);
		}
		
		/**
		 * @Ticket 3318
		 */
		if(CONFIG_24HR_BANKING == "1")
		{
			$updateBankRates = "update ".TBL_EXCHANGE_RATES. " 
				set _24hrMarginType='".$_POST["_24hrMarginType"]."',
						_24hrMarginValue='".$_POST["_24hrMarginValue"]."'
						where erID = $ratId ";
			update($updateBankRates);
		}
		
		/* Add rate into the denomination related table */
		if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1" && $_REQUEST["denominationBased"] == "Y")
		{
			$intTotalIntervals = ceil(($_REQUEST["amountUpto"] - $_REQUEST["amountFrom"])/$_REQUEST["denominationInterval"]);
			
			for($in = 0; $in < $intTotalIntervals; $in++) 
			{ 
			
				$intRateFrom = $_REQUEST["amountFrom"]+($_REQUEST["denominationInterval"]*$in);
				$intRateUpto = ($_REQUEST["denominationInterval"]*$in)+$_REQUEST["denominationInterval"]+($_REQUEST["amountFrom"]-1) + 0.99;
				
				$intervalRate = $_REQUEST["intervalRate".$in];
				
				if($intRateUpto > $_REQUEST["amountUpto"])
					$intRateUpto = $_REQUEST["amountUpto"];
				
				$strInsertDenominationSql = "insert into denominationBasedRate (parentRateId, amountFrom, amountTo, exchangeRate)
											values (
											'".$ratId."','".$intRateFrom."',
											'".$intRateUpto."',
											'".$intervalRate."'
											)
											";
			 	//debug($strInsertDenominationSql);
				insertInto($strInsertDenominationSql);
			 
			 }
		}
		
		//debug("",true);
		
		$descript = "Exchange rates are added";
		$backURL .= "&success=Y";
		activities($_SESSION["loginHistoryID"],"INSERTION",$ratId,TBL_EXCHANGE_RATES,$descript);
	
		if(CONFIG_ENABLE_EX_RATE_LIMIT == '1')
		{////exchange rate Limit ffor Global Exchange
			include("exchRateLimitIntimation.php");	
		}
	
	
		$QuerryHistoricalData = "insert into  historicalExchangeRate 
			(country, primaryExchange, dated,currency,marginPercentage, rateFor, rateValue, updationDate, countryOrigin, currencyOrigin, marginType,agentMarginType, agentMarginValue,relatedID,erID) 
			
			values ('".$_POST["country"]."', '".checkValues($_POST["primaryExchange"])."', '".($exchangeDate != "" ? $exchangeDate : getCountryTime(CONFIG_COUNTRY_CODE))."', '".$_POST["currency"]."','".$_POST["marginPercentage"]."',  '".$_SESSION["rateFor"]."', '".$value."', '".($exchangeDate != "" ? $exchangeDate : getCountryTime(CONFIG_COUNTRY_CODE))."','".$_POST["countryOrigin"]."', '".$_SESSION["currencyOrigin"]."', '".$_SESSION["marginType"]."', '".$_SESSION["agentMarginType"]."', '".$_SESSION["agentMarginValue"]."','0','".$ratId."')";
	
		insertInto($QuerryHistoricalData);
		
		$historicalID = @mysql_insert_id();
		
	
	
	/*$getHistoricalID = selectFrom("select h.id from historicalExchangeRate h, exchangerate e where h.erID = e.erID and e.relatedID = '".$relatedRateId."'");
	$historicalID = $getHistoricalID["id"];
	
	echo "UPDATE".$updateDefaultHistoricalData1 = "update historicalExchangeRate set relatedID = '".$historicalID."' where id = '".$historicalID."'";
	update($updateDefaultHistoricalData1);
	
	echo $historicalID."   ";*/
	
		if(CONFIG_SETTLEMENT_COMMISSION == '1')
		{
				 $checkNextRateQuery = "select relatedID,currency,currencyOrigin from exchangerate where currency = (select defaultRecievingCurrency from  relatedExchangeRate where id = '".$relatedRateId."') and relatedID = '".$relatedRateId."'";
				$checkNextRate = selectFrom($checkNextRateQuery);
				
				if($checkNextRate["relatedID"] == ''){
					
								$backURL .= "&relatedRateId=$relatedRateId&next=Y";
					
				}elseif($checkNextRate["relatedID"] != ''){
				
							
									$checkRateQuery = "select relatedID,currency,currencyOrigin from exchangerate where currency = (select defaultRecievingCurrency from  relatedExchangeRate where id = '".$relatedRateId."') and currencyOrigin = (select  DefaultSendingCurrency from  relatedExchangeRate where id = '".$relatedRateId."') and relatedID = '".$relatedRateId."'";
									$checkRate = selectFrom($checkRateQuery);
									if($checkRate == ''){
								
							  $calculateRateQuery = "select primaryExchange,marginPercentage,agentMarginValue from exchangerate where relatedID = '".$relatedRateId."'"; 
								$calculateRate = selectMultiRecords($calculateRateQuery);
								
								$calculatedAmount = 1;
								$calculteNetExchange = 1;
								$AgentCalculteNetExchange = 1;
								for($j = 0; $j < count($calculateRate); $j++){
											$calculteNetExchange2 = 0;
									
										 $calculatedAmount = $calculatedAmount * $calculateRate[$j]["primaryExchange"];
													$calculatedAmount2 = $calculatedAmount;
									
											$calculteNetExchange2 = ($calculateRate[$j]["primaryExchange"]-($calculateRate[$j]["primaryExchange"] * $calculateRate[$j]["marginPercentage"])/100);
									 $calculteNetExchange = $calculteNetExchange * $calculteNetExchange2;
									
									
									 $AgentCalculteNetExchange = $AgentCalculteNetExchange *(($calculteNetExchange2 -($calculteNetExchange2 * $calculateRate[$j]["agentMarginValue"])/100));
									
									
									
									//$AgentCalculteNetExchange = $AgentCalculteNetExchange *  ($calculteNetExchange -($calculteNetExchange * $calculateRate[$j]["agentMarginValue"])/100);
								}
									
									 $calculatedMargin = $calculatedAmount - $calculteNetExchange;
									$AgentCalculatedMargin =  $calculteNetExchange - $AgentCalculteNetExchange  ;
									
									
									 $calculatedMarginPercentage = ($calculatedMargin * 100)/$calculatedAmount;
									$AgentCalculatedMarginPercentage = ($AgentCalculatedMargin * 100)/$AgentCalculteNetExchange;
									
								  $defaultRateQuery = "insert into ".TBL_EXCHANGE_RATES." 
											(countryOrigin, country, currency, primaryExchange, marginPercentage, dated, rateFor, rateValue, updationDate, currencyOrigin, marginType,agentMarginType, agentMarginValue,relatedID,createdBy,updatedBy) 
											
											values ('$_POST[defaultCountryOrigin]', '$_POST[defaultRecievingCountry]', '".$_POST['defaultRecievingCurrency']."', '".$calculatedAmount."', '".$calculatedMarginPercentage."', '".($exchangeDate != "" ? $exchangeDate : getCountryTime(CONFIG_COUNTRY_CODE))."', '".$_SESSION["rateFor"]."', '".$value."', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$_POST['defaultCurrencyOrigin']."', '".$_SESSION["marginType"]."', 'percent', '".$AgentCalculatedMarginPercentage."','".$relatedRateId."','".$loggedUserID."',0)";
									
									insertInto($defaultRateQuery);
									$defaultRateID = @mysql_insert_id();
									
									
							$defaultQuerryHistoricalData = "insert into  historicalExchangeRate	(country, primaryExchange, dated,currency,marginPercentage, rateFor, rateValue, updationDate, countryOrigin, currencyOrigin, marginType,agentMarginType, agentMarginValue,relatedID,erID) 
									
			values ('".$_POST["defaultRecievingCountry"]."', '".$calculatedAmount."', '".($exchangeDate != "" ? $exchangeDate : getCountryTime(CONFIG_COUNTRY_CODE))."', '".$_POST["defaultRecievingCurrency"]."', '".$calculatedMarginPercentage."',  '".$_SESSION["rateFor"]."', '".$value."', '".($exchangeDate != "" ? $exchangeDate : getCountryTime(CONFIG_COUNTRY_CODE))."', '".$_POST["defaultCountryOrigin"]."','".$_SESSION["defaultCurrencyOrigin"]."', '".$_SESSION["marginType"]."', 'percent', '".$AgentCalculatedMarginPercentage."','".$historicalID."','".$defaultRateID."')";
	
		insertInto($defaultQuerryHistoricalData);
									$defaultRateQuery = "update ".TBL_RELATED_EXCHANGE_RATES." set erID = '".$defaultRateID."' where id = '".$relatedRateId."' ";
									update($defaultRateQuery);
									
								/*	echo "UPDATE".$updateDefaultHistoricalData = "update historicalExchangeRate set relatedID = '".$historicalID."' where id = '".$historicalID."'";
	update($updateDefaultHistoricalData);*/
									
								}
						}
						
				 $getHistoricalDataQuery = "select erID from  exchangerate  where relatedID = '".$relatedRateId."'";
				$getHistoricalData = selectMultiRecords($getHistoricalDataQuery);
				
				 $getHistoricalIDQuery = "select h.id as id from historicalExchangeRate h, exchangerate e where h.erID = e.erID and e.relatedID = '".$relatedRateId."'";
				$getHistoricalID = selectFrom($getHistoricalIDQuery);
				$historicalID = $getHistoricalID["id"];
						
				
					for($x = 0; $x < count($getHistoricalData); $x++){
				$updateHistoricalData = "update historicalExchangeRate set relatedID = '".$historicalID."' where erID = '".$getHistoricalData[$x]["erID"]."'";
				update($updateHistoricalData );
					}
				}
	
	$_SESSION["exchangeDate"] = "";
	$_SESSION["country"] = "";
	$_SESSION["countryOrigin"] = "";
	$_SESSION["currency"] = "";
	$_SESSION["primaryExchange"] = "";
	$_SESSION["marginPercentage"] = "";
	$_SESSION["rateFor"] = "";
	$_SESSION["agentID"] = "";
	$_SESSION["currencyOrigin"] = "";
	$_SESSION["marginType"] = "";
	$_SESSION["agentMarginType"] = "";
	$_SESSION["agentMarginValue"] = "";
	$_SESSION["_24hrMarginType"] = "";
	$_SESSION["_24hrMarginValue"] = "";
	
	
	
	insertError(ER7);
}else{
	
////if for updating multiple exchange rates		
	if(CONFIG_UPDATE_SAME_CURRENCY_RATE == "1" && $_SESSION["rateFor"] == 'generic'){
				
				$MultipleUpdate = true;
				 
			$sameCurrencyQuery = "select relatedID,erID,rateFor,rateValue,marginType,agentMarginType,agentMarginValue from ".TBL_EXCHANGE_RATES." where currency ='". $_POST["currency"]."' and currencyOrigin = '".$_SESSION["currencyOrigin"]."' and (rateFor = 'agent' or rateFor = 'distributor')";
				$sameCurrency = selectMultiRecords($sameCurrencyQuery);
				
				for($i = 0; $i < count($sameCurrency); $i++){
					
					
				
					
									
								$QuerryMultipleRates = "update ".TBL_EXCHANGE_RATES." set
								 updationDate='".getCountryTime(CONFIG_COUNTRY_CODE)."',
								 currency='$_POST[currency]',
								 primaryExchange='".checkValues($_POST["primaryExchange"])."',
								 marginPercentage='".checkValues($_POST["marginPercentage"])."',
								 rateFor = '".$sameCurrency[$i]["rateFor"]."',
								 rateValue = '".$sameCurrency[$i]["rateValue"]."',
								 currencyOrigin = '".$_SESSION["currencyOrigin"]."',
								 marginType = '".$sameCurrency[$i]["marginType"]."',
								 agentMarginType = '".$sameCurrency[$i]["agentMarginType"]."',
								 agentMarginValue = '".$sameCurrency[$i]["agentMarginValue"]."',
								 updatedBy = '".$loggedUserID."'
								
								 
								  where erID='".$sameCurrency[$i]["erID"]."'";
			
									update($QuerryMultipleRates);
					}
	}
		
		if($_REQUEST["denominationBased"] == "Y")
			$charIsDenomination = "Y";
		else
			$charIsDenomination = "N";
		
				
		  $Querry_Sqls = "update ".TBL_EXCHANGE_RATES." set
			 updationDate='".getCountryTime(CONFIG_COUNTRY_CODE)."',
			 currency='$_POST[currency]',
			 primaryExchange='".checkValues($_POST["primaryExchange"])."',
			 marginPercentage='".checkValues($_POST["marginPercentage"])."',
			 rateFor = '".$_SESSION["rateFor"]."',
			 rateValue = '".$_SESSION["agentID"]."',
			 currencyOrigin = '".$_SESSION["currencyOrigin"]."',
			 marginType = '".$_SESSION["marginType"]."',
			 agentMarginType = '".$_SESSION["agentMarginType"]."',
			 agentMarginValue = '".$_SESSION["agentMarginValue"]."',
			  updatedBy = '".$loggedUserID."',
			  isDenomination = '".$charIsDenomination."'
			
			 
			  where erID='$_POST[erID]'";
			
			
			
			
			
			update($Querry_Sqls);
			
			
			if(CONFIG_24HR_BANKING == "1")
			{
				/**
				 * If the exchange rate is of the banking type
				 */
				$updateBankRates = "update ".TBL_EXCHANGE_RATES. " 
					set _24hrMarginType='".$_POST["_24hrMarginType"]."',
							_24hrMarginValue='".$_POST["_24hrMarginValue"]."'
							where erID = '$_POST[erID]'";
				update($updateBankRates);

			}
		
		/* Added by Niaz Ahmad at 15-04-2009 @4658 (milexchange Exchange Rate Enhancement & Rollback)
		   When update Generic exchange rate then value of Upper Exchange of all agents will be updated
		   automatically. The old value  of exchange rate also insert into historical exchange rate table before updation.
		*/
		if(CONFIG_AGENT_BASED_EXCHANGE_RATE_GENERIC_UPPER_VALUE == "1" && $_SESSION["rateFor"] == 'generic')
			{

				$genericExQuery = "select
											*
								   from 
								   			".TBL_EXCHANGE_RATES." 
								   where 
								   			erID = '".$_POST["erID"]."' ";
				
				$genericContents = selectFrom($genericExQuery);
				
				$genericOldRateQuery = "insert into ".TBL_HISTORICAL_EXCHANGE_RATES." 
										 (
											countryOrigin,
											country, 
											currency, 
											primaryExchange,
											marginPercentage,
											dated, 
											rateFor,
											rateValue,
											updationDate,
											currencyOrigin, 
											marginType,agentMarginType, 
											agentMarginValue,erID) 
					 				values (
					 						'".$genericContents['countryOrigin']."', 
											'".$genericContents['country']."', 
											'".$genericContents['currency']."', 
											'".$genericContents['primaryExchange']."', 
											'".$genericContents['marginPercentage']."', 
											'".$genericContents['dated']."', 
											'".$genericContents['rateFor']."', 
											'".$genericContents['rateValue']."', 
											'".getCountryTime(CONFIG_COUNTRY_CODE)."', 
											'".$genericContents['currencyOrigin']."', 
											'".$genericContents['marginType']."', 
											'".$agentContents['agentMarginType']."', 
											'".$genericContents['agentMarginValue']."',
											'".$genericContents['erID']."')";
					
					  insertInto($genericOldRateQuery);		
							
				$agentExQuery = "select
										 *
								 from 
								 		".TBL_EXCHANGE_RATES." 
								 where 
								 		currency ='". $_POST["currency"]."' and 
										currencyOrigin = '".$_SESSION["currencyOrigin"]."' and 
										country = '".$_SESSION["country"]."' and 
										countryOrigin = '".$_SESSION["countryOrigin"]."' and 
										rateFor = 'agent'";
				
				$agentContents = selectMultiRecords($agentExQuery);
				
				for($i = 0; $i < count($agentContents); $i++){
					
					$agentOldRateQuery = "insert into ".TBL_HISTORICAL_EXCHANGE_RATES."
					 					  (
											countryOrigin, 
											country, 
											currency, 
											primaryExchange, 
											marginPercentage, 
											dated, 
											rateFor, 
											rateValue, 
											updationDate, 
											currencyOrigin, 
											marginType,
											agentMarginType, 
											agentMarginValue,erID) 
					 				values (
											'".$agentContents[$i]['countryOrigin']."', 
											'".$agentContents[$i]['country']."', 
											'".$agentContents[$i]['currency']."', 
											'".$agentContents[$i]['primaryExchange']."', 
											'".$agentContents[$i]['marginPercentage']."', 
											'".$agentContents[$i]['dated']."', 
											'".$agentContents[$i]['rateFor']."', 
											'".$agentContents[$i]['rateValue']."', 
											'".getCountryTime(CONFIG_COUNTRY_CODE)."', 
											'".$agentContents[$i]['currencyOrigin']."', 
											'".$agentContents[$i]['marginType']."', 
											'".$agentContents[$i]['agentMarginType']."', 
											'".$agentContents[$i]['agentMarginValue']."',
											'".$agentContents[$i]['erID']."')";
					 
					   insertInto($agentOldRateQuery);		
							
									
								$QuerryAgentRates = "update ".TBL_EXCHANGE_RATES." 
													set
															 updationDate='".getCountryTime(CONFIG_COUNTRY_CODE)."',
															 currency='$_POST[currency]',
															 primaryExchange='".checkValues($_POST["primaryExchange"])."',
															 currencyOrigin = '".$_SESSION["currencyOrigin"]."',
															 updatedBy = '".$loggedUserID."'
													 where 
													 		 erID='".$agentContents[$i]["erID"]."'";
								 update($QuerryAgentRates);
					}
			
			}
			
		/* Add rate into the denomination related table */
		if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1" && $_REQUEST["denominationBased"] == "Y")
		{
			$intTotalIntervals = ceil(($_REQUEST["amountUpto"] - $_REQUEST["amountFrom"])/$_REQUEST["denominationInterval"]);
			
			$removeOldRates = "delete from denominationBasedRate where parentRateId='".$_REQUEST["erID"]."'";
			
			deleteFrom($removeOldRates);
			
			for($in = 0; $in < $intTotalIntervals; $in++) 
			{ 
				$intRateFrom = $_REQUEST["amountFrom"]+($_REQUEST["denominationInterval"]*$in);
				$intRateUpto = ($_REQUEST["denominationInterval"]*$in)+$_REQUEST["denominationInterval"]+($_REQUEST["amountFrom"]-1)+ 0.99;
				$intervalRate = $_REQUEST["intervalRate".$in];
				
				$strInsertDenominationSql = "insert into denominationBasedRate (parentRateId, amountFrom, amountTo, exchangeRate)
										values (
										'".$_REQUEST["erID"]."','".$intRateFrom."',
										'".$intRateUpto."',
										'".$intervalRate."'
										)
										";
				//debug($strInsertDenominationSql);
				insertInto($strInsertDenominationSql);
			}
			
		}
		
	
		/**
		 * If the generic rate updates than its associated banking rate 
		 * should also be update
		 * @Ticket #3672
		 */
		if($_SESSION["rateFor"] == "generic")
		{
			/**
			 * This rate is latest one, than check if there exists any rate based on the banking
			 * If such rate found than update the banking rate also.
			 * else skip this condition
			 * @Ticket #3672
			 */				
			$strCheckBankingRateSql = "select 
											erID 
									   from 
											".TBL_EXCHANGE_RATES." 
									   where 
											country = '".$_REQUEST["country"]."' and
											currency = '".$_REQUEST["currency"]."' and
											countryOrigin = '".$_REQUEST["countryOrigin"]."' and
											currencyOrigin = '".$_REQUEST["currencyOrigin"]."' and
											rateFor = 'bank24hr' and
											isActive = 'Y'
										order by 
											updationDate desc
										limit 1
										"; 

			//debug($strCheckBankingRateSql);
			$arrCheckBankingRate = selectFrom($strCheckBankingRateSql);
			//debug($arrCheckBankingRate);
			if(!empty($arrCheckBankingRate["erID"]))
			{
				
				/**
				 * Update the generic exchange rate to keep the related id of the 
				 * same banking rate
				 * This will help out to maintain the record of the exchange rate in histroy table
				 */
				$strGenericRateUpdateSql = "update ".TBL_EXCHANGE_RATES."
											set
												relatedID = '".$arrCheckBankingRate["erID"]."'
											where
												erID = '".$_REQUEST["erID"]."'";	
				update($strGenericRateUpdateSql);
				
				$strUpdateSql = "update ".TBL_EXCHANGE_RATES."
								set
									updationDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."',
									primaryExchange = '".checkValues($_REQUEST["primaryExchange"])."',
									marginPercentage = '".checkValues($_REQUEST["marginPercentage"])."',
									updatedBy = '".$loggedUserID."',
									relatedID = '".$_REQUEST["erID"]."'
								where
									erID = '".$arrCheckBankingRate["erID"]."'	";
				update($strUpdateSql);
			}
		}

			
			
			
				if(CONFIG_ENABLE_EX_RATE_LIMIT == '1')
				{////exchange rate Limit ffor Global Exchange
					$ratId = $_POST["erID"];
					include("exchRateLimitIntimation.php");	
				}
				
				
				
$combinationRate = selectFrom("select erID from  relatedExchangeRate where id = '".$relatedRateId."'");
			
		  $calculateRateQuery1 = "select primaryExchange,marginPercentage,agentMarginValue from exchangerate where relatedID = '".$relatedRateId."' and erID != '".$combinationRate['erID']."'"; 
				$calculateRate = selectMultiRecords($calculateRateQuery1);
		
		$calculatedAmount = 1;
		$calculteNetExchange = 1;
		$AgentCalculteNetExchange = 1;
		for($i = 0; $i < count($calculateRate); $i++){
					
		
				 $calculatedAmount = $calculatedAmount * $calculateRate[$i]["primaryExchange"];
							$calculatedAmount2 = $calculatedAmount;
			
					$calculteNetExchange2 = ($calculateRate[$i]["primaryExchange"]-($calculateRate[$i]["primaryExchange"] * $calculateRate[$i]["marginPercentage"])/100);
			 $calculteNetExchange = $calculteNetExchange * $calculteNetExchange2;
			
			
			 $AgentCalculteNetExchange = $AgentCalculteNetExchange * ($calculteNetExchange2 -($calculteNetExchange2 * $calculateRate[$i]["agentMarginValue"])/100);
		//	echo $AgentCalculteNetExchange = $AgentCalculteNetExchange * $AgentCalculteNetExchange2;
		
			
			}
			
				$calculatedMargin = $calculatedAmount - $calculteNetExchange;
			$AgentCalculatedMargin =  $calculteNetExchange - $AgentCalculteNetExchange  ;
			
			
			$calculatedMarginPercentage = ($calculatedMargin * 100)/$calculatedAmount;
			$AgentCalculatedMarginPercentage = ($AgentCalculatedMargin * 100)/$AgentCalculteNetExchange;
			
		 	 $QuerryCombinationRate = "update ".TBL_EXCHANGE_RATES." set
			 
			 primaryExchange='".$calculatedAmount."',
			 marginPercentage = '".$calculatedMarginPercentage."',
			 agentMarginValue	 = '".$AgentCalculatedMarginPercentage."',
			 updationDate='".getCountryTime(CONFIG_COUNTRY_CODE)."'
			  where erID='$combinationRate[erID]'";
			
			update($QuerryCombinationRate);
			$oldrateQuery = selectFrom("select relatedID from ".TBL_EXCHANGE_RATES." where erID ='".$_POST['erID']."'");
			if($oldrateQuery['relatedID'] != '')
			{
					$combinationOldRatesQuery = "select * from ".TBL_EXCHANGE_RATES." where relatedID = '".$oldrateQuery['relatedID']."'";
					$getBank24HrQuery  = "select erID,rateFor,_24hrMarginType,_24hrMarginValue from ".TBL_EXCHANGE_RATES." where erID ='".$oldrateQuery['relatedID']."'";
					$combinationOldRates = selectMultiRecords($combinationOldRatesQuery);
					$getBank24Hr = selectFrom($getBank24HrQuery);
					$historicalRelatedID = 0;
					for($i = 0; $i < count($combinationOldRates); $i++){
					 $inserOldRateQuery = "insert into ".TBL_HISTORICAL_EXCHANGE_RATES." 
					(countryOrigin, country, currency, primaryExchange, marginPercentage, dated, rateFor, rateValue, updationDate, currencyOrigin, marginType,agentMarginType, agentMarginValue,_24hrMarginType,_24hrMarginValue,relatedID,erID) 
							values ('".$combinationOldRates[$i]['countryOrigin']."', '".$combinationOldRates[$i]['country']."', '".$combinationOldRates[$i]['currency']."', '".$combinationOldRates[$i]['primaryExchange']."', '".$combinationOldRates[$i]['marginPercentage']."', '".$combinationOldRates[$i]['dated']."', '".$combinationOldRates[$i]['rateFor']."', '".$combinationOldRates[$i]['rateValue']."', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$combinationOldRates[$i]['currencyOrigin']."', '".$combinationOldRates[$i]['marginType']."', '".$combinationOldRates[$i]['agentMarginType']."', '".$combinationOldRates[$i]['agentMarginValue']."','".$combinationOldRates[$i]['_24hrMarginType']."','".$combinationOldRates[$i]['_24hrMarginValue']."','".$historicalRelatedID."','".$combinationOldRates[$i]['erID']."')";
					/////////// #3672 added by ashahid
					 $inserOldRateRelQuery = "insert into ".TBL_HISTORICAL_EXCHANGE_RATES." 
					(countryOrigin, country, currency, primaryExchange, marginPercentage, dated, rateFor, rateValue, updationDate, currencyOrigin, marginType,agentMarginType, agentMarginValue,_24hrMarginType,_24hrMarginValue,relatedID,erID) 
							values ('".$combinationOldRates[$i]['countryOrigin']."', '".$combinationOldRates[$i]['country']."', '".$combinationOldRates[$i]['currency']."', '".$combinationOldRates[$i]['primaryExchange']."', '".$combinationOldRates[$i]['marginPercentage']."', '".$combinationOldRates[$i]['dated']."', '".$getBank24Hr['rateFor']."', '".$combinationOldRates[$i]['rateValue']."', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$combinationOldRates[$i]['currencyOrigin']."', '".$combinationOldRates[$i]['marginType']."', '".$combinationOldRates[$i]['agentMarginType']."', '".$combinationOldRates[$i]['agentMarginValue']."','".$getBank24Hr['_24hrMarginType']."','".$getBank24Hr['_24hrMarginValue']."','".$historicalRelatedID."','".$getBank24Hr['erID']."')";
					insertInto($inserOldRateQuery);
					$historicalRelatedIDBank24Hr="";
					if($i == 0)
					{
						$historicalRelatedID = @mysql_insert_id();
						if($getBank24Hr['rateFor']=="bank24hr" || $historicalRelatedID){
							if($getBank24Hr['rateFor']=="bank24hr"){
								insertInto($inserOldRateRelQuery);
								$historicalRelatedIDBank24Hr = @mysql_insert_id();
							}
							elseif($historicalRelatedID){
								$updateInserOldRateOnlyBank24HrQuery = "update historicalExchangeRate set _24hrMarginType = '".$_POST['_24hrMarginType']."', _24hrMarginValue = '".$_POST['_24hrMarginValue']."' where id = ".$historicalRelatedID;
								update($updateInserOldRateOnlyBank24HrQuery);
							}
						}
					}
				}
				
				$updateInserOldRateQuery = "update historicalExchangeRate set relatedID = '".$historicalRelatedID."' where id = '".$historicalRelatedID."'";
				$updateInserOldRateBank24HrQuery = "update historicalExchangeRate set relatedID = '".$historicalRelatedIDBank24Hr."' where id = '".$historicalRelatedIDBank24Hr."'";
				update($updateInserOldRateQuery);
				if($historicalRelatedIDBank24Hr){
					update($updateInserOldRateBank24HrQuery);
				}
			// #3672 added by ashahid \\\\\\\\\\\\\\\
			}
			
			

	if(CONFIG_UPDATE_SAME_CURRENCY_RATE == "1" && $_SESSION["rateFor"] == 'generic'){
	for($i = 0; $i < count($sameCurrency); $i++){
				
			$combinationRate = selectFrom("select erID from  relatedExchangeRate where id = '".$sameCurrency[$i]["relatedID"]."' ");
			
		 $calculateRateQuery1 = "select primaryExchange,marginPercentage,agentMarginValue from exchangerate where relatedID = '".$sameCurrency[$i]["relatedID"]."' and erID != '".$combinationRate['erID']."' "; 
				$calculateRate = selectMultiRecords($calculateRateQuery1);
		
		$calculatedAmount = 1;
		$calculteNetExchange = 1;
		$AgentCalculteNetExchange = 1;
		for($i = 0; $i < count($calculateRate); $i++){
					
		
				 $calculatedAmount = $calculatedAmount * $calculateRate[$i]["primaryExchange"];
							$calculatedAmount2 = $calculatedAmount;
			
					$calculteNetExchange2 = ($calculateRate[$i]["primaryExchange"]-($calculateRate[$i]["primaryExchange"] * $calculateRate[$i]["marginPercentage"])/100);
			 $calculteNetExchange = $calculteNetExchange * $calculteNetExchange2;
			
			
			 $AgentCalculteNetExchange = $AgentCalculteNetExchange * ($calculteNetExchange2 -($calculteNetExchange2 * $calculateRate[$i]["agentMarginValue"])/100);
		//	echo $AgentCalculteNetExchange = $AgentCalculteNetExchange * $AgentCalculteNetExchange2;
		
			
			}
			
				$calculatedMargin = $calculatedAmount - $calculteNetExchange;
			$AgentCalculatedMargin =  $calculteNetExchange - $AgentCalculteNetExchange  ;
			
			
			$calculatedMarginPercentage = ($calculatedMargin * 100)/$calculatedAmount;
			$AgentCalculatedMarginPercentage = ($AgentCalculatedMargin * 100)/$AgentCalculteNetExchange;
			
		 	 $QuerryCombinationRate = "update ".TBL_EXCHANGE_RATES." set
			 
			 primaryExchange='".$calculatedAmount."',
			 marginPercentage = '".$calculatedMarginPercentage."',
			 agentMarginValue	 = '".$AgentCalculatedMarginPercentage."',
			 updationDate='".getCountryTime(CONFIG_COUNTRY_CODE)."'
			  where erID='".$combinationRate["erID"]."'";
			
			update($QuerryCombinationRate);
			
			
			$oldrateQuery = selectFrom("select relatedID from ".TBL_EXCHANGE_RATES." where erID ='".$_POST['erID']."'");
			if($oldrateQuery['relatedID'] != '')
			{
					$combinationOldRatesQuery = "select * from ".TBL_EXCHANGE_RATES." where relatedID = '".$oldrateQuery['relatedID']."'";
					$combinationOldRates = selectMultiRecords($combinationOldRatesQuery);
					$historicalRelatedID = 0;
					for($i = 0; $i < count($combinationOldRates); $i++){
					 $inserOldRateQuery = "insert into ".TBL_HISTORICAL_EXCHANGE_RATES." 
					(countryOrigin, country, currency, primaryExchange, marginPercentage, dated, rateFor, rateValue, updationDate, currencyOrigin, marginType,agentMarginType, agentMarginValue,relatedID,erID) 
							values ('".$combinationOldRates[$i]['countryOrigin']."', '".$combinationOldRates[$i]['country']."', '".$combinationOldRates[$i]['currency']."', '".$combinationOldRates[$i]['primaryExchange']."', '".$combinationOldRates[$i]['marginPercentage']."', '".$combinationOldRates[$i]['dated']."', '".$combinationOldRates[$i]['rateFor']."', '".$combinationOldRates[$i]['rateValue']."', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$combinationOldRates[$i]['currencyOrigin']."', '".$combinationOldRates[$i]['marginType']."', '".$combinationOldRates[$i]['agentMarginType']."', '".$combinationOldRates[$i]['agentMarginValue']."','".$historicalRelatedID."','".$combinationOldRates[$i]['erID']."')";
					
					insertInto($inserOldRateQuery);
					if($i == 0)
					{
						$historicalRelatedID = @mysql_insert_id();
					}
				}
				
				$updateInserOldRateQuery = "update historicalExchangeRate set relatedID = '".$historicalRelatedID."' where id = '".$historicalRelatedID."'";
				update($updateInserOldRateQuery);
			}
			
			
			
}}///end of for and if loop for updating multiple exchange rates		
			
			
			$descript = "Exchange rates are updated";
			$backURL .= "&success=Y";
					activities($_SESSION["loginHistoryID"],"UPDATION",$_POST[erID],TBL_EXCHANGE_RATES,$descript);
				
			$_SESSION["exchangeDate"] = "";
			$_SESSION["country"] = "";
			$_SESSION["countryOrigin"] = "";
			$_SESSION["currency"] = "";
			$_SESSION["primaryExchange"] = "";
			$_SESSION["marginPercentage"] = "";
			$_SESSION["rateFor"] = "";
			$_SESSION["agentID"] = "";
			$_SESSION["currencyOrigin"] = "";
			$_SESSION["marginType"] ="";
			$_SESSION["agentMarginType"] = "";
			$_SESSION["agentMarginValue"] = "";
			$_SESSION["_24hrMarginType"] = "";
			$_SESSION["_24hrMarginValue"] = "";
			
			
			$_SESSION["amountFrom"] = "";
			$_SESSION["amountUpto"] = "";
			$_SESSION["denominationInterval"] = "";

			for($in = 0; $in < $intTotalIntervals; $in++) 
				unset($_SESSION["intervalRate".$in]);
			
			insertError(ER8);

	if(CONFIG_SHARE_OTHER_NETWORK == '1')
	{
		include("remoteExchange.php");
	}
	

}
$backURL .= "&historicalID=".$historicalID;	
redirect($backURL);
?>