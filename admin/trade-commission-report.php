<?php
session_start();
/**
* @package Payex
* @subpackage Trading
* Short Description
* This page show the listing of Trade Profit/Loss Report which is result of
* Deal Traded to Not-Traded page operations.
*/
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("javaScript.php");
if(CONFIG_SECURE_ALL_PAGES == '0' )
include_once ("secureAllPages.php");

$modifyby = $_SESSION["loggedUserData"]["userID"];
$currencyies = selectMultiRecords("SELECT DISTINCT(currencyName) FROM currencies");
?>
<html>
<head>
<title>Trade Profit/Loss Report</title>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee/grid.css" title="coffee" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/grid.celledit.js" type="text/javascript"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" type="text/javascript">

	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var tAmt = 0;
	var lAmt = 0;
	var extraParams;
	jQuery(document).ready(function(){
		var lastSel;
		var maxRows = 10;
		jQuery("#commissionList").jqGrid({
			url:'trade-trans-action.php?getGrid=tradeCommList&nd='+new Date().getTime(),
			datatype: "json",
			height: 250, 
			colNames:[
				'Date', 
//				'Batch ID', 
				'Trans Amount',
				'Currency Bought', 
				'Trans Rate',
				'Trade Rate',
				'Currency Sold', 
				'Profit/Loss',
				'Trade#',
				'Reference Number'
			],
			colModel:[
				{name:'dated',index:'dated', width:60, height:30, sorttype:'date', datefmt:'d-m-Y'},
//				{name:'batchID',index:'batchID', width:100, align:"center"},
				{name:'amount',index:'amount', width:100, align:"center"},
				{name:'currencyBuy',index:'currencyBuy', width:100, align:"left"},
				{name:'transRate',index:'transRate', width:90, align:"left"},
				{name:'tradeRate',index:'tradeRate', width:90, align:"left"},
				{name:'currencySell',index:'currencySell', width:100, align:"left"},
				{name:'profitLoss',index:'profitLoss', width:100, align:"left"},
				{name:'tradenumber',index:'tradenumber', width:120, align:"left"},
				{name:'referencenumber',index:'referencenumber', width:120, align:"left"}

			],
			totalAmount:amount,
			imgpath:gridimgpath, 
			rowNum: maxRows,
			rowList: [10,20,50,100,200],
			pager: jQuery('#pagernav'),
//			sortname: 'importedOn',
			viewrecords: true,
//			multiselect:true,
			sortorder: "desc",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			caption: "Trade Commission",
		});
		
		//jQuery('a').cluetip({splitTitle: '|'});
	});
	function gridReload(grid)
	{
		var theUrl = "trade-trans-action.php?";
		
		var extraParam='';
		var from = jQuery("#from").val();
		var to = jQuery("#to").val();
		var amount = jQuery("#amount").val();
		
		var currencyBuy = jQuery("#currencyBuy").val();
		var tradenumber1 = jQuery("#tradenumber1").val();
		var referencenumber1 = jQuery("#referencenumber1").val();
		if(grid=='commissionList'){
			extraParam = extraParam + "&from="+from+"&to="+to+"&amount="+amount+"&currencyBuy="+currencyBuy+"&tradenumber1="+tradenumber1+"&referencenumber1="+referencenumber1+"&Submit=SearchCommission&getGrid=tradeCommList&nd="+new Date().getTime();
		}
		else{
			extraParam = extraParam + "&getGrid=tradeCommList&nd="+new Date().getTime();
		}
		jQuery("#commissionList").setGridParam({
			url: theUrl+extraParam,
			page:1
			//alert("he");
		}).trigger("reloadGrid");

	}
	function uniqueArr(arrayName)
	{
		var newArray=new Array();
		label:for(var i=0; i<arrayName.length;i++ )
		{  
			for(var j=0; j<newArray.length;j++ )
			{
				if(newArray[j]==arrayName[i]) 
					continue label;
			}
			newArray[newArray.length] = arrayName[i];
		}
		return newArray;
	}
	
</script>
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000}
.style1 {color: #005b90}
.style4 {color: #005b90; font-weight: bold; }

-->
</style>
<script language="javascript">
/*
function clearDates()
{
	document.frmSearch.from.value = "";
	document.frmSearch.to.value = "";
	return true;
}
*/
</script>
</head>
<body>
	<form name="frmSearch" id="frmSearch">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc"><b><strong><font color="#FFFFFF">Trade Profit/Loss Report</font></strong></b></td>
  </tr>
  <tr>
	<td valign='top'>
	<table  border="0" style="float:left; vertical-align:top;" width="100%">
    <tr>
    <td align="center">
      <table border="1" cellpadding="5" bordercolor="#666666">
      <tr>
            <td nowrap bgcolor="#6699cc"><span class="tab-u"><strong>Profit/Loss Search Filter</strong></span></td>
        </tr>
        <tr>
		  <td nowrap align="center">From Date 
				<input name="from" type="text" id="from" readonly >
				&nbsp;<a href="javascript:show_calendar('frmSearch.from');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp; &nbsp;To Date&nbsp;
				<input name="to" type="text" id="to" readonly >
				&nbsp;<a href="javascript:show_calendar('frmSearch.to');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
				<!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->
			</td>
        </tr>
        <tr>
	      <td nowrap align="center"> 
              Search Type
              <select name="searchType" id="searchType">
				  <option value="introducer">Introducer</option>
				  <option value="account">account</option>
			  </select>
			  &nbsp;&nbsp;&nbsp;
           	  Username / Account #
           	  <input name="searchName" type="text" id="searchName" >
		</td>
      </tr>
	  <tr>
	  	<td nowrap="nowrap" align="center">
		 Trade# 
              <input name="tradenumber1" type="text" id="tradenumber1" >
			  &nbsp;&nbsp;&nbsp;
         Reference Number 
              <input name="referencenumber1" type="text" id="referencenumber1" >
		</td>
	  </tr>
	  <tr>
		  <td nowrap="nowrap" align="center">
	  	  Amount 
            <input name="amount" type="text" id="amount" >
			&nbsp;&nbsp;&nbsp;
              Select Currency
              <select name="currencyBuy" id="currencyBuy">
				  <option value="">Select Currency</option>
			  	  <? if(count($currencyies)>0){?>
					  <? for($ct=0;$ct<count($currencyies);$ct++){?>
					  <option value="<?=$currencyies[$ct]["currencyName"]?>"><?=$currencyies[$ct]["currencyName"]?></option>
					  <? }?>
				  <? }?>
			  </select>
			  &nbsp;&nbsp;&nbsp;
			<input type="button" name="Submit" value="Search" onClick="gridReload('commissionList')">
		  </td>
	  </tr>
    </table>
  	</td>
	</tr>
    
    <tr><td>&nbsp;</td></tr>
	<tr>
    <td valign="top">
    			<table id="commissionList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernav" class="scroll" style="text-align:center;"></div>
	</td>
    </tr>
	  <tr bgcolor="#FFFFFF">
		<td align="center">&nbsp;</td>
	  </tr>
</table>

</td>
  </tr>
</table>
</form>
</body>
</html>
