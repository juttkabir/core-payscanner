<?
session_start();


$nError =  urldecode($_GET['nError']);
$sCourntry = urldecode($_GET['sCourntry']);
$dCourntry = urldecode($_GET['dCourntry']);
$amount = urldecode($_GET['amount']);
$fee = urldecode($_GET['fee']);
$total = urldecode($_GET['total']);
$rate = urldecode($_GET['rate']);
$damount = urldecode($_GET['damount']);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>

		function clearAll()
		{
			//alert("sadf");
			frmfeecalculator.sCourntry.value = '';
			frmfeecalculator.dCourntry.value = '';
			frmfeecalculator.amount.value = '';
			frmfeecalculator.fee.value = '';
			frmfeecalculator.rate.value = '';
			frmfeecalculator.damount.value = '';
			frmfeecalculator.total.value = '';
			//alert("sadf");
			
		}

function checkForm(theForm) {	

	 var bName = navigator.appName;
	 var bVer = parseInt(navigator.appVersion);
	// document.write(bName);
	// document.write(bVer);
	
	 var MF = (bName == "Netscape");
	 //var IE = (bName == "Microsoft Internet Explorer");
	 if(MF)
	 {
		if(theForm.sCourntry.value == "" ){
			alert("Please provide From Country Name.");
			theForm.sCourntry.focus();
			return false;
		}
		if(theForm.dCourntry.value == "" ){
			alert("Please provide To Country Name.");
			theForm.dCourntry.focus();
			return false;
		}
	}

	if(theForm.amount.value == ""  && theForm.damount.value == "")
	{    	
		alert("Please provide Amount in Local or Destination Currency");				
		theForm.amount.focus();
        return false;
    }


	return true;
}

function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
	</script>

		

<title>Fee Calculator</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="interface.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.style4 {font-family: Verdana; font-weight: bold; }
-->
</style>
</head>

<body>
<br>
<form action="fee_calculator.php" method="post"  onSubmit="return checkForm(this);" name="frmfeecalculator">
<table width="600" border="0" align="center" cellpadding="1" cellspacing="0" >
<tr>
	<td bgColor=#99C6E2>
		<font  size="4" color="ffffff">&nbsp;&nbsp;Fee Calculator</font>
	</td>
</tr>
<tr>
   <td>
	  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgColor=#ffffff>
		<tr>
			<Td colspan="3" align="center"><FONT face=arial size=2>
					<?					
					// if any field is empty or 
						if(!empty($nError))
						{
							if($nError == 1)
								{
									$strErrMsg = "Please Select <b>From</b> Country.";
									echo "
										<font color=ff0000 size=+1><u>Empty Field</u><br></font>
										<font color=ff0000>$strErrMsg</font><br><br>
									";
								}	
							if($nError == 2)
								{
									$strErrMsg = "Please Select <b>To</b> Country.";
									echo "
										<font color=ff0000 size=+1><u>Empty Field</u><br></font>
										<font color=ff0000>$strErrMsg</font><br><br>
									";
								}	
							if($nError == 3)
								{
									$strErrMsg = "Please Enter the Amount in <b>Local Currency</b>.";
									echo "
										<font color=ff0000 size=+1><u>Empty Field</u><br></font>
										<font color=ff0000>$strErrMsg</font><br><br>
									";
								}	
							if($nError == 4)
								{
									$strErrMsg = "Amount can not be a string.";
									echo "
										<font color=ff0000 size=+1><u>Error</u><br></font>
										<font color=ff0000>$strErrMsg</font><br><br>
									";
								}		
							if($nError == 5)
								{
									$strErrMsg = "Please Enter the Amount in <b>Destination Currency</b>.";
									echo "
										<font color=ff0000 size=+1><u>Empty Field</u><br></font>
										<font color=ff0000>$strErrMsg</font><br><br>
									";
								}												
			
						}
					?>	</font><br>
			</Td>
		  </tr>	
		  <tr>
		      <td align="center" class="style4">From</td>
		      <td align="center">&nbsp;</td>
		      <td align="center"><span class="style4">To</span></td>
	      </tr>
	      <tr>
			<td align="center">
					
			<select name="sCourntry" size="8"  style="WIDTH: 140px;FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;FONT-SIZE: 8pt; " >
			
				<?
				$strQuery = "SELECT distinct origCountry  FROM imfee  ORDER BY origCountry   ";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
		
				while($rstRow = mysql_fetch_array($nResult))
				{
					$strCountry = $rstRow["origCountry"];															
					if($sCourntry == $strCountry)
						echo "<option value='$sCourntry' selected>".$strCountry."</option>";			
					else
					echo "<option>".$strCountry."</option>";	
				}
				
				?>
			</select>&nbsp;</td>
			<td align="center"><font  face="Verdana"><b></font></td>
			<td align="center"><select name="dCourntry" size="8"  style="WIDTH: 140px;FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;FONT-SIZE: 8pt; ">
			
				
				<?
				
		
				$strQuery = "SELECT distinct country    FROM  exchangerate  where country != 'Any Country' and  country != 'United Kingdom' ORDER BY country   ";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
			
				while($rstRow = mysql_fetch_array($nResult))
				{
					$strCountry = $rstRow["country"];															
					if($dCourntry == $strCountry)
						echo "<option value='$dCourntry' selected>".$strCountry."</option>";			
					else
					echo "<option>".$strCountry."</option>";	
				}
				
				?>
			</select>&nbsp;</td>
		  </tr>
		  <tr>
			<Td colspan="3">
				<br>
			</Td>
		  </tr>	  
	</table>	
  </td>
</tr>
<tr bgColor=#99C6E2>
<td ><font size="2" color="#ffffff" ><b>&nbsp;&nbsp;Calculate Fee and  Amount</font></td>
</tr>
<tr>
  <Td>
      <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="ffffff">
		  <tr>
			<td width="217">&nbsp;</td>
			<td width="383">&nbsp;</td>                           
		  </tr>
		  <tr>
			<td colspan="2" >
			<font size="2" color="#000000">&nbsp;&nbsp;&nbsp;
			<?
			
			if($dCourntry != "")
			{
				$strQuery = "SELECT * FROM cities where country = '$dCourntry'";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
				$rstRow = mysql_fetch_array($nResult);				
				$serviceAvailable = $rstRow["serviceAvailable"];
				$serviceAvailable1 = $rstRow["serviceAvailable"];
				$Delivery = $rstRow["deliveryTime"];	
				if($serviceAvailable=="")
					echo "None";
				else
				{
				 	$serviceAvailable  = explode (',',$serviceAvailable);
					if($serviceAvailable[0] == "Cash Collection")
					{										
						echo "Services available for <b>$dCourntry</b>:<b>&nbsp;<a href='city-locator.php?Courntry=$dCourntry'>".$serviceAvailable[0]."</a></b>&nbsp;";
						echo ",".$serviceAvailable[1].",&nbsp;".$serviceAvailable[2];				
					}
					else				
						echo "Services available for <b>$dCourntry</b>:".$serviceAvailable1;	
						echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;Estimated Delivery Time:&nbsp;<b>".$Delivery;		
				}
			}
			?></font></td>
			                           
		  </tr>		
		  
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>                           
		  </tr>		    
		  <tr>
			<td align="right"  style="font-family: Verdana;font-size: 8pt;font-weight: normal;font-variant: normal; ">Amount in Local Currency:&nbsp;</td>
			<td align="left">
				  <?
					if(empty($amount) || $amount == 0)
					{
				  ?>
						<INPUT maxLength=15 size=21  name=amount class="Login">
					<?
					}
					else
					{
					?>
						<INPUT maxLength=15 size=21  name=amount value="<? echo $amount;?>" class="Login">
					<?
					}								
					?>	
			&nbsp;&nbsp;<INPUT type=submit value=" Calculate Dest. Amount" name=SUBMIT1 bgcolor="YELLOW" class="Button">							
			</td>                           
		  </tr>
		  <tr>
			<td align="right" style="font-family: Verdana;font-size: 8pt;font-weight: normal;font-variant: normal; ">Fee:&nbsp;</td>
			<td>
			  <?
					
			  
				if(empty($fee) && $fee != 0)
				{
			  ?>
					<INPUT maxLength=15 size=21 value=0 name=fee disabled class="TextBox">
				<?								
				}
				if($fee == 0)
				{
			  ?>
					<INPUT maxLength=15 size=21 value='N/A' disabled name=fee class="Login">
				<?								
				}								
				else
				{
				?>
					<INPUT maxLength=15 size=21  name=fee disabled value="<? echo $fee;?>" class="Login">
				<?
				}								
				?>							
			</td>                            
		  </tr>
		  <tr>
			<td align="right" style="font-family: Verdana;font-size: 8pt;font-weight: normal;font-variant: normal; ">Total:&nbsp;</td>
			<td>							
			<INPUT maxLength=15 size=21  name=total disabled class="Login" value="<? echo $total;?>">&nbsp;&nbsp;&nbsp;<input type="button" value="Reset" name="B2" class = "Button" onClick="clearAll()"></td>
			<td width="10">&nbsp;</td>
		  </tr>
		  <tr>
			<td align="right" style="font-family: Verdana;font-size: 8pt;font-weight: normal;font-variant: normal; ">Exchange Rate:&nbsp; </td>
			<td><INPUT maxLength=15 size=21  name=rate disabled class="Login" value="<? echo $rate;?>"></td>
			<td>&nbsp;</td>
		  </tr>			
		  <tr>
			<td align="right" style="font-family: Verdana;font-size: 8pt;font-weight: normal;font-variant: normal; "> Amount in Destination Currency:&nbsp;</td>
			<td>
			<INPUT maxLength=15 size=21  name=damount class="Login" value="<? echo $damount;?>">&nbsp;&nbsp;&nbsp;<INPUT type=submit value="Calculate Local Amount" name=SUBMIT2 class="Button" bgcolor="YELLOW">
			</td>
			
		  </tr>
		  <tr>
			<td colspan="2"><br><br></td>
		  </tr>
			<?
			if($rate != "")
			{
			?>			 

		  <tr>
			<td colspan="2" align="center"><table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
             
			  <tr>
                <td align="center"><font color="#333333" size="2">&nbsp;&nbsp;&nbsp;&nbsp;<b>NOTE: ALL THESE EXCHANGE RATE APPLIES FOR ANY TRANSACTION CREATED BEFORE 3:00 PM.<br> ANY TRANSACTION CREATED AFTER 3:00 PM WILL GET TOMORROW RATE.</b></font></td>
              </tr>
            </table>
		</td>
	 </tr>
	 <?
	 }
	 ?>
		  		  <tr>
			<td colspan="2"><br></td>
		  </tr>	
	  </table>
   </td>
</tr>

</table>
<Br>
</form>		
</body>
</html>
