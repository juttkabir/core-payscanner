<?php
session_start();
include ("../include/config.php");
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$agentType = getAgentType();

if ($_POST["Submit"] == "Export") {
if ($_POST["tempID"] != '') {

$username  = $_SESSION["loggedUserData"]["username"];
$userID  = $_SESSION["loggedUserData"]["userID"];
$date_time = date('d-m-Y-h-i-s-A');
$fileFormatQuery = selectFrom("select id, format from ".TBL_EXPORT_TEMPLATE." where `id` = '".$_POST["tempID"]."'");
$fileFormat = $fileFormatQuery["format"];
$fileNamed = $username."_".$date_time;
	if ($fileFormat == "txt") {
		$appType = "txt";
	} else if ($fileFormat == "csv") {
		$appType = "csv";
	} else {
		$appType = "x-msexcel";	
	}
	header ("Content-type: application/$appType");
	header ("Content-Disposition: attachment; filename=$fileNamed.$fileFormat" ); 
	header ("Content-Description: PHP/INTERBASE Generated Data" );

$query = "select * from ". TBL_TRANSACTIONS." where 1 ";

if ($agentType == "SUPI" || $agentType == "SUBI")
{
	$query .= " and benAgentID = '".$userID."' ";
}
		
$query .= " order by transDate DESC";

$contentTrans = selectMultiRecords($query);

if ($fileFormat == "xls")
{
$data = "<table width='900' border='1' cellspacing='0' cellpadding='0' bordercolor='#000000'>"; 
	$data .="<tr>";
	$fieldQuery = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where `tempID` = '".$_POST["tempID"]."'");
	for ($j = 0; $j < count($fieldQuery); $j++)
	{
		$data .= "<td><font face='Verdana' color='#000000' size='2'><b>".$fieldQuery[$j]["field_title"]."</font></td>\n";	
	}
	$data .="</tr>";
	
	for($i=0;$i < count($contentTrans);$i++)
	{
		$refNumberIM = $contentTrans[$i]["refNumberIM"];
		$transDate = $contentTrans[$i]["transDate"];
		$companyCode = $contentTrans[$i]["refNumber"];
		$customerID = $contentTrans[$i]["customerID"];
		$customerName = "";
		$beneName = "";
		if($contentTrans[$i]["createdBy"] == "CUSTOMER")			  	
		{		  
		   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentTrans[$i]["customerID"]."'");  
		   $beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from cm_beneficiary where benID ='".$contentTrans[$i]["benID"]."'");
		   
		   $customerName = $customerContent["FirstName"]." ".$customerContent["LastName"];
		   $beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		   $beneAddress = $beneContent["Address"];
		   $beneCity = $beneContent["City"];
		   $beneState = $beneContent["State"];
		   $benePhone = $beneContent["Phone"];
		   $beneCPF = $beneContent["CPF"];
		}else
			  {
		  	$customerContent = selectFrom("select firstName, lastName, accountName, payinBook from " . TBL_CUSTOMER . " where customerID='".$contentTrans[$i]["customerID"]."'");
		  	$beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from " . TBL_BENEFICIARY . " where benID='".$contentTrans[$i]["benID"]."'");
		  	
		  	$customerName = $customerContent["firstName"]." ".$customerContent["lastName"];
		  	$beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		  	$beneAddress = $beneContent["Address"];
		    $beneCity = $beneContent["City"];
		    $beneState = $beneContent["State"];
		    $benePhone = $beneContent["Phone"];
		    $beneCPF = $beneContent["CPF"];
		  }
		$gbpAmount = $contentTrans[$i]["transAmount"];
		$exchRate = $contentTrans[$i]["exchangeRate"];
		$foriegnAmount = $contentTrans[$i]["localAmount"];
		$toCountry = $contentTrans[$i]["toCountry"];
		$currencyFrom = $contentTrans[$i]["currencyFrom"];
		$currencyTo = $contentTrans[$i]["currencyTo"];
		$type = $contentTrans[$i]["transType"];
		$transTotal += $gbpAmount;
		$localTotal += $foriegnAmount;
		$collection = '';
		$bankName = '';						
		$account = '';						
		$brCode = '';						
		$brAddress = '';			
		if($contentTrans[$i]["transType"]=='Pick up')
		{
		$collectionPoint = selectFrom("select cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'");
		$collection = $collectionPoint["cp_branch_address"].$collectionPoint["cp_city"].$collectionPoint["cp_state"];
		}elseif($contentTrans[$i]["transType"]=='Bank Transfer')
			{
				if($contentTrans[$i]["createdBy"] == "CUSTOMER")
					{
						$bankDetails = selectFrom("select * from ".TBL_CM_BANK_DETAILS." where benID = '".$contentTrans[$i]["benID"]."'");
					
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];
						$accountType = $bankDetails["accountType"];						
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];						
						}else{
						$bankDetails = selectFrom("select * from ".TBL_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
						
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];			
						$accountType = $bankDetails["accountType"];		
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];	
					}
					$bankNumQuery = selectFrom("select bankCode from imbanks where bankName = '".$bankName."'");
					$bankNumber	= $bankNumQuery["bankCode"];				
			}		
		$data .= " <tr>
		
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$refNumberIM</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$transDate</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$gbpAmount $currencyFrom</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$exchRate</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$foriegnAmount $currencyTo</font></td>";
		
		$data .="
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$customerName</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$beneName</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$beneAddress</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$beneCity</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$beneState</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$benePhone</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$beneCPF</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$bankNumber</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$bankName</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$brCode</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$account</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$accountType</font></td>
		</tr>";
		  	
	
	$data.="<tr>	
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>";
			
			$data.="<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
	</tr>";
	
	}
	$data.="<tr>	
			<td>&nbsp;</td>
			<td><font color='#000000'  size='1' face='Verdana'><b>Total:-</b></font></td>
			<td><font color='#000000'  size='1' face='Verdana'><b>&nbsp;$transTotal</b></font></td>
			<td>&nbsp;</td>
			<td><font color='#000000'  size='1' face='Verdana'><b>&nbsp;$localTotal</b></font></td>
			<td>&nbsp;</td>";
			
			$data.="<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>	
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>	
			<td>&nbsp;</td>
			<td>&nbsp;</td>				
	</tr>";

$data.="</table>";
}
elseif ($fileFormat == "csv")
{
	$fieldQuery = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where `tempID` = '".$_POST["tempID"]."'");
	for ($j = 0; $j < count($fieldQuery); $j++)
	{
		$data .= $fieldQuery[$j]["field_title"] . ",";	
	}
	$data .= "\r\n";
	$data .= "\r\n";
	for($i=0;$i < count($contentTrans);$i++)
	{
		$refNumberIM = $contentTrans[$i]["refNumberIM"];
		$transDate = $contentTrans[$i]["transDate"];
		$companyCode = $contentTrans[$i]["refNumber"];
		$customerID = $contentTrans[$i]["customerID"];
		$customerName = "";
		$beneName = "";
		if($contentTrans[$i]["createdBy"] == "CUSTOMER")			  	
		{		  
		   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentTrans[$i]["customerID"]."'");  
		   $beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from cm_beneficiary where benID ='".$contentTrans[$i]["benID"]."'");
		   
		   $customerName = $customerContent["FirstName"]." ".$customerContent["LastName"];
		   $beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		   $beneAddress = $beneContent["Address"];
		   $beneCity = $beneContent["City"];
		   $beneState = $beneContent["State"];
		   $benePhone = $beneContent["Phone"];
		   $beneCPF = $beneContent["CPF"];
		}else
			  {
		  	$customerContent = selectFrom("select firstName, lastName, accountName, payinBook from " . TBL_CUSTOMER . " where customerID='".$contentTrans[$i]["customerID"]."'");
		  	$beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from " . TBL_BENEFICIARY . " where benID='".$contentTrans[$i]["benID"]."'");
		  	
		  	$customerName = $customerContent["firstName"]." ".$customerContent["lastName"];
		  	$beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		  	$beneAddress = $beneContent["Address"];
		    $beneCity = $beneContent["City"];
		    $beneState = $beneContent["State"];
		    $benePhone = $beneContent["Phone"];
		    $beneCPF = $beneContent["CPF"];
		  }
		$gbpAmount = $contentTrans[$i]["transAmount"];
		$exchRate = $contentTrans[$i]["exchangeRate"];
		$foriegnAmount = $contentTrans[$i]["localAmount"];
		$currencyFrom = $contentTrans[$i]["currencyFrom"];
		$currencyTo = $contentTrans[$i]["currencyTo"];
		$toCountry = $contentTrans[$i]["toCountry"];
		$type = $contentTrans[$i]["transType"];
		$transTotal += $gbpAmount;
		$localTotal += $foriegnAmount;
		$collection = '';
		$bankName = '';						
		$account = '';						
		$brCode = '';						
		$brAddress = '';			
		if($contentTrans[$i]["transType"]=='Pick up')
		{
		$collectionPoint = selectFrom("select cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'");
		$collection = $collectionPoint["cp_branch_address"].$collectionPoint["cp_city"].$collectionPoint["cp_state"];
		}elseif($contentTrans[$i]["transType"]=='Bank Transfer')
			{
				if($contentTrans[$i]["createdBy"] == "CUSTOMER")
					{
						$bankDetails = selectFrom("select * from ".TBL_CM_BANK_DETAILS." where benID = '".$contentTrans[$i]["benID"]."'");
					
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];
						$accountType = $bankDetails["accountType"];						
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];						
						}else{
						$bankDetails = selectFrom("select * from ".TBL_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
						
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];
						$accountType = $bankDetails["accountType"];						
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];	
					}	
					$bankNumQuery = selectFrom("select bankCode from imbanks where bankName = '".$bankName."'");
					$bankNumber	= $bankNumQuery["bankCode"];				
			}		
		$data .= "$refNumberIM,$transDate,$gbpAmount $currencyFrom,$exchRate";
		
	  	
		$data .= ",$foriegnAmount $currencyTo,$customerName,$beneName,$beneAddress,$beneCity,$beneState,$benePhone,$beneCPF,$bankNumber,$bankName,$brCode,$account,$accountType\r\n";
	}
}
elseif ($fileFormat == "txt")
{
	$fieldQuery = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where `tempID` = '".$_POST["tempID"]."'");
	for ($j = 0; $j < count($fieldQuery); $j++)
	{
		$data .= $fieldQuery[$j]["field_title"] . " ";	
	}
	$data .= "\r\n";
	$data .= "\r\n";
	for($i=0;$i < count($contentTrans);$i++)
	{
		$refNumberIM = $contentTrans[$i]["refNumberIM"];
		$transDate = $contentTrans[$i]["transDate"];
		$companyCode = $contentTrans[$i]["refNumber"];
		$customerID = $contentTrans[$i]["customerID"];
		$customerName = "";
		$beneName = "";
		if($contentTrans[$i]["createdBy"] == "CUSTOMER")			  	
		{		  
		   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentTrans[$i]["customerID"]."'");  
		   $beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from cm_beneficiary where benID ='".$contentTrans[$i]["benID"]."'");
		   
		   $customerName = $customerContent["FirstName"]." ".$customerContent["LastName"];
		   $beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		   $beneAddress = $beneContent["Address"];
		   $beneCity = $beneContent["City"];
		   $beneState = $beneContent["State"];
		   $benePhone = $beneContent["Phone"];
		   $beneCPF = $beneContent["CPF"];
		}else
			  {
		  	$customerContent = selectFrom("select firstName, lastName, accountName, payinBook from " . TBL_CUSTOMER . " where customerID='".$contentTrans[$i]["customerID"]."'");
		  	$beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from " . TBL_BENEFICIARY . " where benID='".$contentTrans[$i]["benID"]."'");
		  	
		  	$customerName = $customerContent["firstName"]." ".$customerContent["lastName"];
		  	$beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		  	$beneAddress = $beneContent["Address"];
		    $beneCity = $beneContent["City"];
		    $beneState = $beneContent["State"];
		    $benePhone = $beneContent["Phone"];
		    $beneCPF = $beneContent["CPF"];
		  }
		$gbpAmount = $contentTrans[$i]["transAmount"];
		$exchRate = $contentTrans[$i]["exchangeRate"];
		$foriegnAmount = $contentTrans[$i]["localAmount"];
		$toCountry = $contentTrans[$i]["toCountry"];
		$currencyFrom = $contentTrans[$i]["currencyFrom"];
		$currencyTo = $contentTrans[$i]["currencyTo"];
		$type = $contentTrans[$i]["transType"];
		$transTotal += $gbpAmount;
		$localTotal += $foriegnAmount;
		$collection = '';
		$bankName = '';						
		$account = '';						
		$brCode = '';						
		$brAddress = '';			
		if($contentTrans[$i]["transType"]=='Pick up')
		{
		$collectionPoint = selectFrom("select cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'");
		$collection = $collectionPoint["cp_branch_address"].$collectionPoint["cp_city"].$collectionPoint["cp_state"];
		}elseif($contentTrans[$i]["transType"]=='Bank Transfer')
			{
				if($contentTrans[$i]["createdBy"] == "CUSTOMER")
					{
						$bankDetails = selectFrom("select * from ".TBL_CM_BANK_DETAILS." where benID = '".$contentTrans[$i]["benID"]."'");
					
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];	
						$accountType = $bankDetails["accountType"];					
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];						
						}else{
						$bankDetails = selectFrom("select * from ".TBL_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
						
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];		
						$accountType = $bankDetails["accountType"];				
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];	
					}		
					$bankNumQuery = selectFrom("select bankCode from imbanks where bankName = '".$bankName."'");
					$bankNumber	= $bankNumQuery["bankCode"];				
			}		
		$data .= "$refNumberIM $transDate $gbpAmount $exchRate";
		
	  	
		$data .= " $foriegnAmount $customerName $beneName $beneAddress $beneCity $beneState $benePhone $beneCPF $bankNumber $bankName $brCode $account $accountType \r\n";
	}
}
echo $data;
}	else {
	$msg2 = CAUTION_MARK . " Please select the template to export";
}
}
?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
<body>

<table width="100%" border="0" cellspacing="1" cellpadding="5">
	<?	if ($msg2 != "") {  ?>
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
                <td colspan="2" align="center" class="tab-r"><? echo $msg2 ?></td>
              </tr>
            </table></td>
          </tr>
	<?	} else if ($_GET["msg"] != "") {  ?>
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
                <td width="40" align="center">&nbsp;</td>
                <td width="635" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"] != "" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font>&nbsp;&nbsp;<? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?></td>
              </tr>
            </table></td>
          </tr>
	<?	}  ?>
  <form action="export_templates.php" method="post" onSubmit="return checkForm();">
  <tr>
    <td>
		<table width="450" border="0" cellspacing="1" cellpadding="1">
          <tr bgcolor="#DFE6EA"> 
          <td colspan="2" bgcolor="#000000">
		  <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Export Templates</strong></font></td>
			</tr>
		  </table>
		</td>
          </tr>
          <tr bgcolor="#eeeeee"> 
            <td colspan="2"><a href="main.php" class="style2">Go Back</a>
            </td>
          </tr>
          <tr bgcolor="#eeeeee"> 
            <td width="150"><font color="#005b90"><strong>Select Template</strong></font></td>
            <td width="300">
				<select name="tempID" id="tempID" style="font-family:verdana; font-size: 11px">
				<option value="">-- Select --</option>
			<?
					$templates = selectMultiRecords("SELECT * FROM " . TBL_EXPORT_TEMPLATE . " WHERE 1");
					for ($i = 0; $i < count($templates); $i++) {
			?>
				<option value="<? echo $templates[$i]["id"] ?>"><? echo $templates[$i]["title"] ?></option>
			<?	}  ?>
				</select>
            	</td>
          </tr>
          <tr bgcolor="#eeeeee"> 
            <td colspan="2" align="center"> <input type="submit" name="Submit" value="Export"> 
            </td>
          </tr>
     </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>