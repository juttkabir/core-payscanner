<?php 
	/*
	 * @package Transaction
	 * @subpackage save data from pdf file
	 * short description
	 * this page gets the data from pdf file, validate the input from pdf and save it database
	 * @author Mirza Arslan Baig
	 */
	session_start();
	include ("../include/config.php");
	dbConnect();
	$date_time = date('d-m-Y  h:i:s A');
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	//debug($_POST, true);
	//debug($_POST['benCountry']);
	/*
	 * Condition for Validation
	 */
	//if((isset($_POST['bankName']) && isset($_POST['accountNumber']) && isset($_POST['ibanNumber']) &&  && isset($_POST['branchAddress']) && isset($_POST['branchCode']) && isset($_POST['accountNumber']) &&  isset($_POST['routingNumber']) && isset($_POST['swiftCode']) && isset($_POST['sortCode'])) && (!empty($_POST['ibanNumber']) || !empty($_POST['branchAddress']) || !empty($_POST['branchCode']) || !empty($_POST['accountNumber']) || !empty($_POST['bankName']) || !empty($_POST['accountNumber']) || !empty($_POST['swiftCode']) || !empty($_POST['sortCode'])){
	if(isset($_POST['transID']) && isset($_POST['benID']) && isset($_POST['benCountry']) && !empty($_POST['benCountry']) && !empty($_POST['transID'])){
		
		$queryBankDetailsRule = "SELECT id, countryName, accountName, accNo, branchNameNumber, branchAddress, swiftCode, sortCode, iban, routingNumber, creationDate, updateDate, status FROM ben_country_rule_bank_details AS benBankDetailsRule INNER JOIN `countries` AS country ON benBankDetailsRule.benCountry  WHERE country.countryName = '{$_POST['benCountry']}' AND country.countryID = benBankDetailsRule.benCountry ORDER BY benBankDetailsRule.updateDate DESC";
		$bankDetailRule = selectFrom($queryBankDetailsRule);
		$bankDetailFields = "";
		$flagForDetails = false;
		//debug($bankDetailRule);
		if(isset($_POST['bankName']))
			$bankName = trim($_POST['bankName']);
		if(isset($_POST['accountNumber']))
			$accountNumber = trim($_POST['accountNumber']);
		if(isset($_POST['ibanNumber']))
			$IBAN = trim($_POST['ibanNumber']);
		if(isset($_POST['branchCode']))
			$branchCode = trim($_POST['branchCode']);
		if(isset($_POST['branchAddress']))
			$branchAddress = trim($_POST['branchAddress']);
		if(isset($_POST['swiftCode']))
			$swiftCode = trim($_POST['swiftCode']);
		if(isset($_POST['sortCode']))
			$sortCode = trim($_POST['sortCode']);
		if(isset($_POST['routingNumber']))
			$routingNumber = trim($_POST['routingNumber']);
			
			$insertBenBanksFields = " (";
			$insertBenBanksVal = " VALUES(";
		//debug($bankName);
		//debug($accountNumber);
		//debug($branchCode, true);*/
		if(empty($bankName) && isset($_POST['reqBankName'])){
			die("Account name is required!!");
		}
		elseif(!empty($bankName)){
			$benBankTable .= "`bankName` = '$bankName',";
			$bankDetailFields .= "`bankName` = '$bankName',";
			$insertBenBanksFields .= "`bankName`,";
			$insertBenBanksVal .= "'$bankName',";
			if($bankDetailRule['accountName'] == 'Y')
				$flagForDetails = true;
		}
		//debug($bankDetailRule['accNo']);
		//debug($accountNumber, true);
		if(empty($accountNumber) && isset($_POST['reqAccountNumber'])){
			die("Account number is required!!");
		}
		elseif(!empty($accountNumber)){
			$benBankTable .= " `accountNo` = '$accountNumber', ";
			$bankDetailFields .= " `accNo` = '$accountNumber',";
			$insertBenBanksFields .= " `accountNo`,";
			$insertBenBanksVal .= " '$accountNumber',";
			if($bankDetailRule['accNo'] == 'Y')
				$flagForDetails = true;
		}
		
		if(empty($IBAN) && isset($_POST['reqIbanNumber'])){
			die("IBAN number is required!!");
		}
		elseif(!empty($IBAN)){
			$benBankTable .= " `IBAN` = '$IBAN',";
			$bankDetailFields .= " `IBAN` = '$IBAN',";
			$insertBenBanksFields .= " `IBAN`,";
			$insertBenBanksVal .= " '$IBAN',";
			if($bankDetailRule['iban'] == 'Y')
				$flagForDetails = true;
		}
		
		
		if(empty($branchCode) && isset($_POST['reqBranchCode'])){
			die("Branch code is required!!");
		}
		elseif(!empty($branchCode)){
			$benBankTable .= " `branchCode` = '$branchCode',";
			$bankDetailFields .= " `branchCode` = '$branchCode',";
			$insertBenBanksFields .= " `branchCode`,";
			$insertBenBanksVal .= " '$branchCode',";
			if($bankDetailRule['branchNameNumber'] == 'Y')
				$flagForDetails = true;
		}
		
		if(empty($branchAddress) && isset($_POST['reqBranchAddress'])){
			die("Branch Address is required!!");
		}
		elseif(!empty($branchAddress)){
			$benBankTable .= " `branchAddress` = '$branchAddress',";
			$bankDetailFields .= " `branchAddress` = '$branchAddress',";
			$insertBenBanksFields .= " `branchAddress`,";
			$insertBenBanksVal .= " '$branchAddress',";
			if($bankDetailRule['branchAddress'] == 'Y')
				$flagForDetails = true;
		}
		
		if(empty($swiftCode) && isset($_POST['reqSwiftCode'])){
			die("Swift code is required!!");
		}
		elseif(!empty($swiftCode)){
			$benBankTable .= " `swiftCode` = '$swiftCode',";
			$bankDetailFields .= " `swiftCode` = '$swiftCode',";
			$insertBenBanksFields .= " `swiftCode`,";
			$insertBenBanksVal .= " '$swiftCode',";
			if($bankDetailRule['swiftCode'] == 'Y')
				$flagForDetails = true;
		}
		
		if(empty($sortCode) && isset($_POST['reqSortCode'])){
			die("Sort code is required!!");
		}
		elseif(!empty($sortCode)){
			$benBankTable .= " `sortCode` = '$sortCode',";
			$bankDetailFields .= " `sortCode` = '$sortCode',";
			$insertBenBanksFields .= " `sortCode`,";
			$insertBenBanksVal .= " '$sortCode',";
			if($bankDetailRule['sortCode'] == 'Y')
				$flagForDetails = true;
		}
		
		if(empty($routingNumber) && (isset($_POST['reqRoutingNumber']))){
			die("Routing number is required!!");
		}
		elseif(!empty($routingNumber)){
			$benBankTable .= " `routingNumber` = '$routingNumber'";
			$bankDetailFields .= " `routingNumber` = '$routingNumber'";
			$insertBenBanksFields .= " `routingNumber`,";
			$insertBenBanksVal .= " '$routingNumber',";
			if($bankDetailRule['routingNumber'] == 'Y')
				$flagForDetails = true;
		}
		$benID = $_POST['benID'];
		$transID = $_POST['transID'];
		
		/*
		 * Update the record in beneficiary bank details table
		 */
		//$queryBankDetails = "UPDATE ".TBL_BANK_DETAILS." SET `bankName` = '$bankName', `accNo` = '$accountNumber', `branchCode` = '$branchCode', `branchAddress` = '$branchAddress', `IBAN` = '$IBAN', `swiftCode` = '$swiftCode', `sortCode` = '$sortCode', `routingNumber` = '$routingNumber' WHERE transID = '$transID' LIMIT 1";
		
		$bankDetailFields = rtrim($bankDetailFields, ',');
		
		$queryBankDetails = "UPDATE ".TBL_BANK_DETAILS." SET $bankDetailFields WHERE transID = '$transID'";
		
		if($flagForDetails){
			$queryCompleteTrans = "UPDATE ".TBL_TRANSACTIONS." SET `transDetails` = 'Yes' WHERE transID = '$transID'";
			mysql_query($queryCompleteTrans) or die(mysql_error());
			//debug($queryCompleteTrans);
		}
		$updateBenVerificationCode = "UPDATE ".TBL_TRANSACTIONS." SET `tip` = '{$_POST['tip']}' WHERE transID = '$transID'";
		mysql_query($updateBenVerificationCode) or die(mysql_error());
		mysql_query($queryBankDetails) or die(mysql_error());
		
		
		/*
		 * Bank ID generator
		 */
		/*
		
		do
		{
			$dbBankId = uniqid();
			$dbBankIdData = selectFrom("SELECT bankId from ".TBL_BANKS. " WHERE bankId ='".$dbBankId."' ");
		}	
		while(!empty($dbBankIdData["bankId"]));
		*/
		
		/**
		 * Check the bank details already exists or not 
		 */
		/*
		$queryChkBanks = "SELECT id FROM ".TBL_BANKS." WHERE name = '$bankName' AND branchCode = '$branchCode' AND country = '$bankCountry' AND branchAddress = '$branchAddress'";
		$existBank = selectMultiRecords($queryChkBanks);
		//debug($queryChkBanks);
		$records = count($existBank);
		if( $records > 0){
			$queryUpdateBank = "UPDATE ".TBL_BANKS." SET `name` = '$bankName', `branchCode` = '$branchCode', `branchAddress` = '$branchAddress', `country` = '$bankCountry' WHERE 
			id = '{$existBank[$records-1]['id']}'";
			mysql_query($queryUpdateBank) or die(mysql_error());
			$bankID = $existBank[$records-1]['id'];
		}else{*/
		
			/*
			 * Insert record in banks table 
			 */
		/*	
			$queryBank = "INSERT INTO ".TBL_BANKS." (bankId, name, branchCode, branchAddress, swiftCode, country) VALUES('$dbBankId', '$bankName', '$branchCode', '$branchAddress' , '$swiftCode', '$bankCountry')";
			mysql_query($queryBank) or die(mysql_error());
			$bankID = mysql_insert_id();
			//debug($bankID, true);
		}
		*/
		
			//$queryBenBanks = "UPDATE ".TBL_BEN_BANK_DETAILS." SET `bankName` = '$bankName', `accountNo` = '$accNumber', `branchCode` = '$branchCode', `branchAddress` = '$branchAddress', `swiftCode` = '$swiftCode', `IBAN` = '$IBAN', `routingNumber` = '$rountingNumber', `sortCode` = '$sortCode' WHERE benId = '$benID'";
			$benBankTable = rtrim($benBankTable, ',');
			//$insertBenBanksFields = rtrim($insertBenBanksFields, ',');
			//$insertBenBanksVal = rtrim($insertBenBanksVal, ',');
			$insertBenBanksFields .= " `benId`) ";
			$insertBenBanksVal .= " '$benID') ";
			$queryChkBen = "SELECT * FROM ".TBL_BEN_BANK_DETAILS." WHERE benId = '$benID'";
			$countBenBank = selectMultiRecords($queryChkBen);
			if(count($countBenBank) >= 1){ 
				$queryBenBanks = "UPDATE ".TBL_BEN_BANK_DETAILS." SET $benBankTable WHERE benId = '$benID'";
			}else{
				$queryBenBanks = "INSERT INTO ".TBL_BEN_BANK_DETAILS." ".$insertBenBanksFields.$insertBenBanksVal;
			}
			//debug($queryBenBanks, true);
			mysql_query($queryBenBanks) or die(mysql_error());
			
			
		
		/*
		 * changing incomplete transaction to pending
		 */
		/*
		 * $queryIncompleteTrans = "UPDATE ".TBL_TRANSACTIONS." SET `transDetails` = 'Yes' WHERE createdBy != 'CUSTOMER'  AND (transStatus = 'Processing' OR transStatus ='Pending')  AND transDetails ='No'";
		mysql_query($queryIncompleteTrans) or die(mysql_error());
		*/
		die("Your information has been sent successfully!");
	}
	else{
		$updateBenVerificationCode = "UPDATE ".TBL_TRANSACTIONS." SET `tip` = '{$_POST['tip']}' WHERE transID = '{$_POST['transID']}'";
		//debug($updateBenVerificationCode);
		mysql_query($updateBenVerificationCode) or die(mysql_error());
		echo "Beneficiary Verification code is sent successfully!!";
	}
?>