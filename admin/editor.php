<STYLE TYPE="text/css">
TABLE#tblCoolbar 
	{ 
	background-color:threedface; padding:1px; color:menutext; 
	border-width:1px; border-style:flat; 
	border-color:threedhighlight threedshadow threedshadow threedhighlight;
	}
.cbtn
	{
	height:18;
	BORDER-LEFT: threedface 1px solid;
	BORDER-RIGHT: threedface 1px solid;
	BORDER-TOP: threedface 1px solid;
	BORDER-BOTTOM: threedface 1px solid; 
	}
.txtbtn {font-family:tahoma; font-size:70%; color:menutext;}
</STYLE>

<script LANGUAGE="JavaScript">
//<!--
function button_over(eButton)
	{
	eButton.style.backgroundColor = "#B5BDD6";
	eButton.style.borderColor = "darkblue darkblue darkblue darkblue";
	}
function button_out(eButton)
	{
	eButton.style.backgroundColor = "threedface";
	eButton.style.borderColor = "threedface";
	}
function button_down(eButton)
	{
	eButton.style.backgroundColor = "#8494B5";
	eButton.style.borderColor = "darkblue darkblue darkblue darkblue";
	}
function button_up(eButton)
	{
	eButton.style.backgroundColor = "#B5BDD6";
	eButton.style.borderColor = "darkblue darkblue darkblue darkblue";
	eButton = null; 
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var isHTMLMode=false;

function document.onreadystatechange()
	{
  	idContent.document.designMode="On";
	}
function cmdExec(cmd,opt) 
	{
  	if (isHTMLMode){alert("Please uncheck 'Edit HTML'");return;}
  	idContent.document.execCommand(cmd,"",opt);idContent.focus();
	}
function setMode(bMode)
	{
	var sTmp;
  	isHTMLMode = bMode;
  	if (isHTMLMode){sTmp=idContent.document.body.innerHTML;idContent.document.body.innerText=sTmp;} 
	else {sTmp=idContent.document.body.innerText;idContent.document.body.innerHTML=sTmp;}
  	idContent.focus();
	}
function createLink()
	{
	if (isHTMLMode){alert("Please uncheck 'Edit HTML'");return;}
	cmdExec("CreateLink");
	}
function insertImage()
	{
	if (isHTMLMode){alert("Please uncheck 'Edit HTML'");return;}
	var sImgSrc=prompt("Insert Image File (You can use your local image file) : ", "http://www.sohnapakistan.com/sample.jpg");
	if(sImgSrc!=null) cmdExec("InsertImage",sImgSrc);
	}
function Save() 
	{
	if (isHTMLMode){alert("Please uncheck 'Edit HTML'");return;}

	document.myForm.data.value=idContent.document.body.innerHTML;
	}
function foreColor()
	{
	var arr = showModalDialog("selcolor.htm","","font-family:Verdana; font-size:12; dialogWidth:30em; dialogHeight:34em" );
	if (arr != null) cmdExec("ForeColor",arr);	
	}
//-->	
</script>
<table  id="tblCoolbar" width=542 cellpadding="0" cellspacing="0">
  <tr valign="middle"> 
    <td colspan=16> <select onchange="cmdExec('formatBlock',this[this.selectedIndex].value);" >
        <option selected>Style</option>
        <option value="Normal">Normal</option>
        <option value="Heading 1">Heading 1</option>
        <option value="Heading 2">Heading 2</option>
        <option value="Heading 3">Heading 3</option>
        <option value="Heading 4">Heading 4</option>
        <option value="Heading 5">Heading 5</option>
        <option value="Address">Address</option>
        <option value="Formatted">Formatted</option>
        <option value="Definition Term">Definition Term</option>
      </select>
      <select name="select" onChange="cmdExec('fontname',this[this.selectedIndex].value);">
        <option selected>Font</option>
        <option value="Verdana">Verdana</option>
        <option value="Arial">Arial</option>
      </select> 
      <select onchange="cmdExec('fontsize',this[this.selectedIndex].value);">
        <option selected>Size</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
      </select> </td>
  </tr>
  <tr> 
    <td><div class="cbtn" onClick="cmdExec('cut')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);"> 
        <img hspace="1" vspace=1 align=absmiddle src="<?echo IMG_PATH_EDITOR?>Cut.gif" alt="Cut"> 
      </div></td>
    <td><div class="cbtn" onClick="cmdExec('copy')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);"> 
        <img hspace="1" vspace=1 align=absmiddle src="<?echo IMG_PATH_EDITOR?>Copy.gif" alt="Copy"> 
      </div></td>
    <td><div class="cbtn" onClick="cmdExec('paste')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);"> 
        <img hspace="1" vspace=1 align=absmiddle src="<?echo IMG_PATH_EDITOR?>Paste.gif" alt="Paste"> 
      </div></td>
    <td><div class="cbtn" onClick="cmdExec('bold')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);"> 
        <img hspace="1" vspace=1 align=absmiddle src="<?echo IMG_PATH_EDITOR?>Bold.gif" alt="Bold"> 
      </div></td>
    <td><div class="cbtn" onClick="cmdExec('italic')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);"> 
        <img hspace="1" vspace=1 align=absmiddle src="<?echo IMG_PATH_EDITOR?>Italic.gif" alt="Italic"> 
      </div></td>
    <td><div class="cbtn" onClick="cmdExec('underline')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);"> 
        <img hspace="1" vspace=1 align=absmiddle src="<?echo IMG_PATH_EDITOR?>Under.gif" alt="Underline"> 
      </div></td>
    <td><div class="cbtn" onClick="cmdExec('justifyleft')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);"> 
        <img hspace="1" vspace=1 align=absmiddle src="<?echo IMG_PATH_EDITOR?>Left.gif" alt="Justify Left"> 
      </div></td>
    <td><div class="cbtn" onClick="cmdExec('justifycenter')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);"> 
        <img hspace="1" vspace=1 align=absmiddle src="<?echo IMG_PATH_EDITOR?>Center.gif" alt="Center"> 
      </div></td>
    <td><div class="cbtn" onClick="cmdExec('justifyright')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);"> 
        <img hspace="1" vspace=1 align=absmiddle src="<?echo IMG_PATH_EDITOR?>Right.gif" alt="Justify Right"> 
      </div></td>
    <td><div class="cbtn" onClick="cmdExec('insertorderedlist')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);"> 
        <img src="<?echo IMG_PATH_EDITOR?>numlist.gif" alt="Ordered List" width="23" height="22" hspace="2" vspace=1 align=absmiddle> 
      </div></td>
    <td><div class="cbtn" onClick="cmdExec('insertunorderedlist')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);"> 
        <img src="<?echo IMG_PATH_EDITOR?>bullist.gif" alt="Unordered List" width="23" height="22" hspace="2" vspace=1 align=absmiddle> 
      </div></td>
    <td><div class="cbtn" onClick="cmdExec('outdent')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);"> 
        <img hspace="2" vspace=1 align=absmiddle src="<?echo IMG_PATH_EDITOR?>deindent.gif" alt="Decrease Indent"> 
      </div></td>
    <td><div class="cbtn" onClick="cmdExec('indent')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);"> 
        <img hspace="2" vspace=1 align=absmiddle src="<?echo IMG_PATH_EDITOR?>inindent.gif" alt="Increase Indent"> 
      </div></td>
    <td><div class="cbtn" onClick="foreColor()" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">
        <img hspace="2" vspace=1 align=absmiddle src="<?echo IMG_PATH_EDITOR?>fgcolor.gif" alt="Forecolor"> 
      </div></td>
    <td>
<div class="cbtn" onClick="cmdExec('createLink')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);"> 
        <img hspace="2" vspace=1 align=absmiddle src="<?echo IMG_PATH_EDITOR?>Link.gif" alt="Link"> 
      </div></td>
    <td>
	  </td>
    
  </tr>
</table>
