<?php
class connectOtherDataBase
{
	var $connectionMade;
	var $server;
	var $dataBase;
	
	function makeConnection($server, $user, $password, $dataBase)
	{
		$this-> $serve = $server;
		$this-> $dataBase = $dataBase;
		
		if ((@$db=mysql_connect($server, $user, $password)) == FALSE)
			log_exit( "<b>Sorry:</b> Could not connect to MySQL Database server $SERVER ", __LINE__, __FILE__);
	
	  if(!mysql_select_db($dataBase, $db))
		   log_exit( "<b>Sorry:</b> Could not connect to database DATABASE", __LINE__, __FILE__);
		   
		 $this-> $connectionMade = $db;
	    return $db;	
	}
	
	
	function closeConnection()
	{
	   mysql_close($connectionMade);
	}
	
	
	
	function selectOneRecord($Querry_Sql)
	{ //echo DEBUG . $Querry_Sql;
	        
		if((@$result = mysql_query ($Querry_Sql))==FALSE)
	   	{
			if (DEBUG=="True")
			{
				echo mysql_message($Querry_Sql);		
			}	
		}   
		else
	 	{	
			if ($check=mysql_fetch_array($result))
	   		{
	      		return $check;
	   		}
				return false;	
		}
	}
	
	function SelectMultiple($Querry_Sql)
	{   
		if((@$result = mysql_query ($Querry_Sql))==FALSE)
	   	{
		
			if (DEBUG=="True")
			{
				echo mysql_message($Querry_Sql);		
			}	
		}   
		else
	 	{	
		    $count = 0;
			$data = array();
			while ( $row = mysql_fetch_array($result)) 
			{
				$data[$count] = $row;
				$count++;
			}
				return $data;
		}
	}
	
	
	
	function insertInto($Querry_Sql)
	{	//echo $Querry_Sql;
	   
	
	   if((@$result = mysql_query ($Querry_Sql))==FALSE)
	   	{
			if (DEBUG=="True")
			{
				echo mysql_message($Querry_Sql);		
			}	
		}   
		else
	 	{	
	 		$inserted = @mysql_insert_id();
			return $inserted ;	
		}
	}
	function update($Querry_Sql)
	{
	        
		if((@$result = mysql_query ($Querry_Sql))==FALSE)
	   	{
			if (DEBUG=="True")
			{
			echo mysql_message($Querry_Sql);		
			}	
		}   
		else
	 	{	
			return true;
	   	}
	}
	
	
		function updateAgentAccount($agentID, $amount, $transID, $type, $description, $actAs, $today='0000-00-00')
		{
			if($today == '0000-00-00')
				$today = date("Y-m-d");  
			$loginID  = $_SESSION["loggedUserData"]["userID"];
			
			$agentQuery = selectFrom("select balance, isCorrespondent from ".$dataBase.".admin where userID = '".$agentID."'");
			$isCorresspondent = $agentQuery["isCorrespondent"];
			
			if($isCorresspondent != 'N')
			{
				$agentType = "Distributor";
				
				if(CONFIG_AnD_ENABLE == '1' && $isCorresspondent == 'Y')
				{
					$agentType = "Both";///AnD
				}	
			}elseif($isCorresspondent == 'N')
			{
				$agentType = "Agent";
			}
			
			if($agentType == 'Agent')
			{
				$accountQuery = "insert into ".$dataBase.".agent_account 
				(agentID, dated, type, amount, modified_by, TransID, description) values
				('$agentID', '$today', '$type', '$amount', '$loginID', '". $transID ."', '$description')";	
			}elseif($agentType == 'Distributor')
			{
				$accountQuery = "insert into ".$dataBase.".bank_account
				(bankID, dated, type, amount, modified_by, TransID, description) values
				('$agentID', '".$today."', '$type', '$amount', '$loginID', '". $transID."', '$description')";
			}elseif($agentType == "Both")
			{
				$accountQuery = "insert into ".$dataBase.".agent_Dist_account 
				(agentID, dated, type, amount, modified_by, TransID, description, actAs) values
				('".$agentID."', '$today', '$type', '$amount', '$loginID', '". $transID."', '$description','$actAs')";
			}
			$q = insertInto($accountQuery);
			
			$this-> agentSummaryAccount($agentID, $type, $amount); 
			
			$currentBalance = $agentQuery["balance"];
			if($type == 'DEPOSIT')
			{
				$currentBalance += $amount;
			}else{
				$currentBalance -= $amount;
				}
			update("update ".$dataBase.".admin set balance = $currentBalance where userID = '".$agentID."'");											
			
		return $q;		
		}
		
		
		function updateSubAgentAccount($agentID, $amount, $transID, $type, $description, $actAs, $today='0000-00-00', $note='')
		{
			if($today == '0000-00-00')
				$today = date("Y-m-d");  
			$loginID  = $_SESSION["loggedUserData"]["userID"];
			
			$agentQuery = selectFrom("select balance, isCorrespondent from ".$dataBase.".admin where userID = '".$agentID."'");
			$isCorresspondent = $agentQuery["isCorrespondent"];
			
			if($isCorresspondent != 'N')
			{
				$agentType = "Distributor";
				
				if(CONFIG_AnD_ENABLE == '1' && $isCorresspondent == 'Y')
				{
					$agentType = "Both";///AnD
				}	
			}elseif($isCorresspondent == 'N')
			{
				$agentType = "Agent";
			}
			
			if($agentType == 'Agent')
			{
				$accountQuery = "insert into ".$dataBase.".sub_agent_account 
				(agentID, dated, type, amount, modified_by, TransID, description) values
				('$agentID', '$today', '$type', '$amount', '$loginID', '". $transID ."', '$description')";	
			}elseif($agentType == 'Distributor')
			{
				$accountQuery = "insert into ".$dataBase.".sub_bank_account
				(bankID, dated, type, amount, modified_by, TransID, description) values
				('$agentID', '".$today."', '$type', '$amount', '$loginID', '". $transID."', '$description')";
			}elseif($agentType == "Both")
			{
				$accountQuery = "insert into ".$dataBase.".sub_agent_Dist_account 
				(agentID, dated, type, amount, modified_by, TransID, description, actAs, note) values
				('".$agentID."', '$today', '$type', '$amount', '$loginID', '". $transID."', '$description','$actAs', '".$note."')";
			}
			$q = $this->insertInto($accountQuery);
			$this-> subAgentSummaryAccount($agentID, $type, $amount); 
			
			$currentBalance = $agentQuery["balance"];
			if($type == 'DEPOSIT')
			{
				$currentBalance += $amount;
			}else{
				$currentBalance -= $amount;
				}
			update("update ".$dataBase.".admin set balance = $currentBalance where userID = '".$agentID."'");											
			
			
		return $q;	
				
		}
		
		
	function agentSummaryAccount($agentID, $type, $amount)
	{
		 $today = date("Y-m-d"); 
			$agentContents = selectFrom("select id, user_id, dated, opening_balance, closing_balance from ".$dataBase.".account_summary where user_id = '$agentID' and dated = '$today'");
	
	
			if($agentContents["user_id"] != "")
			{
				
				$balance = $agentContents["closing_balance"];
				
				if($type == 'DEPOSIT')
				{
					$balance = $balance + $amount;	
				}else{
					$balance = $balance - $amount;	
				}
				
			 update("update ".$dataBase.".account_summary set closing_balance = '$balance' where id = '".$agentContents["id"]."'");			
		}else{
			$datesLast = selectFrom("select  Max(dated) as dated from ".$dataBase.".account_summary where user_id = '$agentID'");
			 $agentContents = selectFrom("select user_id, dated, opening_balance, closing_balance from ".$dataBase.".account_summary where user_id = '$agentID' and dated = '".$datesLast["dated"]."'");
			
				
				$balance = 0;
				
				$balance += $agentContents["closing_balance"];
					
				
				
				$openingBalance = $balance;
				
				if($type == 'DEPOSIT')
				{
					$closingBalance = $balance + $amount;	
				}else{
					$closingBalance = $balance - $amount;	
				}
				
			 $this-> insertInto("insert into ".$dataBase.".account_summary (dated, user_id, opening_balance, closing_balance) values('$today', '$agentID', '$openingBalance', '$closingBalance')");
			}	
			
				
	}
	
	function subAgentSummaryAccount($agentID, $type, $amount)
	{
		 $today = date("Y-m-d"); 
			$agentContents = selectFrom("select id, user_id, dated, opening_balance, closing_balance from ".$dataBase.".sub_account_summary where user_id = '$agentID' and dated = '$today'");
	
	
			if($agentContents["user_id"] != "")
			{
				
				$balance = $agentContents["closing_balance"];
				
				if($type == 'DEPOSIT')
				{
					$balance = $balance + $amount;	
				}else{
					$balance = $balance - $amount;	
				}
				
			 update("update ".$dataBase.".sub_account_summary set closing_balance = '$balance' where id = '".$agentContents["id"]."'");			
		}else{
			$datesLast = selectFrom("select  Max(dated) as dated from ".$dataBase.".sub_account_summary where user_id = '$agentID'");
			 $agentContents = selectFrom("select user_id, dated, opening_balance, closing_balance from ".$dataBase.".sub_account_summary where user_id = '$agentID' and dated = '".$datesLast["dated"]."'");
			
				
				$balance = 0;
				
				$balance += $agentContents["closing_balance"];
					
				
				
				$openingBalance = $balance;
				
				if($type == 'DEPOSIT')
				{
					$closingBalance = $balance + $amount;	
				}else{
					$closingBalance = $balance - $amount;	
				}
				
			 $this-> insertInto("insert into ".$dataBase.".sub_account_summary (dated, user_id, opening_balance, closing_balance) values('$today', '$agentID', '$openingBalance', '$closingBalance')");
			}	
			
				
	}
	
	
	
	
	
	///////////update Ledger Function///////////
	function remoteTransStatusChange($transId, $newStatus, $username, $remarks, $refundFee='Yes')
{
		$updateTrans = "update " . TBL_TRANSACTIONS . " set transStatus= '".$newStatus."'";
		
		
		$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID, AgentComm, createdBy from " . TBL_TRANSACTIONS . " where transID = '".$transId."'");
		
		$transBalance = $amount["transAmount"];		
		$commission = $amount["AgentComm"];
		$balance = $amount["totalAmount"];
		$agent = $amount["custAgentID"];
		$bank = $amount["benAgentID"];
		
			$agentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$agent."'");
		
		
		
		if($newStatus == 'Authorize')
		{
				$updateTrans .= " , authorisedBy = '".$username."',
														authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."'";
			  $descript = "Transaction Authorized successfully.";									
			  
			  
			  
			  
			  
			  
			if(CONFIG_LEDGER_AT_CREATION != "1")
			{
				if($amount["createdBy"] != 'CUSTOMER')
					{
						$payin = selectFrom("select balance, payinBook, customerID from " . TBL_CUSTOMER . " where customerID = '". $amount["customerID"]."'");
						if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")
						{
							
							
							if(CONFIG_EXCLUDE_COMMISSION == '1')
							{
								$balance = $amount["totalAmount"] - $commission;
							}else{
								$balance = $amount["totalAmount"];
							}
							
							$agentEntryType = "WITHDRAW";
						  $agentEntryDesc = "Transaction Authorized";
						 
							$updateAgentAccount = true;
							
						}elseif($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == "1")
						{
							//For these customer, ledger is created in the start
								/*insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
							 values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction Authorized','WITHDRAW','".$balance."','".$transId."','".$_SESSION["loggedUserData"]["userID"]."')");
							 
					
							$newBalance = $payin["balance"] - $balance ; 
							$update_Balance = "update ".TBL_CUSTOMER." set balance = '".$newBalance."' where customerID= '".$payin["customerID"]."'";
							update($update_Balance);*/
						}
					}elseif($amount["createdBy"] == 'CUSTOMER')
					{
						$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
						 values('".$amount["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction Authorized','WITHDRAW','".$amount["totalAmount"]."','".$amount["refNumberIM"]."'
						 )";
						 $this-> insertInto($strQuery);
						 
						if(CONFIG_ONLINE_AGENT == '1')
						{	
							$descript = "Transaction Authorized";
							updateAgentAccount($agentContents["userID"], $amount["totalAmount"], $imReferenceNumber, "WITHDRAW", $descript, 'Agent');	
						}	
					}
				}
				
				if(CONFIG_POST_PAID != '1')
			{
				$dist = $amount["benAgentID"];
				$benAgentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '$dist'");
				$currentBalance = $benAgentContents["balance"];
							
				
				$currentBalance =  $currentBalance - $transBalance;
				update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$bank'");
					
				$distributorEntryType = "WITHDRAW";
				$distributorEntryDesc = "Transaction Authorized";
						 
				$updateBankAccount = true;
								
	
			}
		
		$printingMessage = "Transaction Authorized successfully";
				
				if(CONFIG_SHARE_OTHER_NETWORK == '1' && CONFIG_REMOTE_LEDGER_AT_CREATE != '1')
				{
					$benAgentInfo = selectFrom("select fromServer from " . TBL_ADMIN_USERS . " where userID = '".$amount["benAgentID"]."'");
					if($benAgentInfo["fromServer"] != '')
					{
						$transactionDescription = "Transaction Authorized";
						include("createOtherNetworkTrans.php");		
						
						
					}
						
				}
		}
		////////////////////////////Transaction has been Authorized////////////////
		///////////////////////////////////////Now Start Transaction Cancelled Or Failed////////////
		if($newStatus == 'Cancelled')
		{
		
			$updateTrans .= " , cancelledBy = '".$username."',
														cancelDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."',
														refundFee = '".$refundFee."',
														remarks		= '".$remarks."'
														";
			  $descript = "Transaction is Cancelled";									
			  
			  
		
		
		//if(CONFIG_LEDGER_AT_CREATION == "1")
		//{
					if($amount["createdBy"] != 'CUSTOMER')
					{
						$payin = selectFrom("select balance, payinBook, customerID from " . TBL_CUSTOMER . " where customerID = '". $amount["customerID"]."'");
						if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")
						{
							
							if(CONFIG_EXCLUDE_COMMISSION == '1')
							{
								$balance = $amount["totalAmount"] - $commission;
							}else{
								$balance = $amount["totalAmount"];
							}
							
							$agentEntryType = "DEPOSIT";
						  $agentEntryDesc = "Transaction Cancelled";
						 
							$updateAgentAccount = true;
							
							
							$currentBalance = $balance + $currentBalance;
							update("update " . TBL_ADMIN_USERS . " set balance = $currentBalance where userID = '$agent'");
							
							
	
						}elseif($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == "1")
						{
							$this-> insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
							 values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Cancelled','DEPOSIT','".$balance."','".$_GET["transID"]."','".$_SESSION["loggedUserData"]["userID"]."')");
							 
					
							$newBalance = $payin["balance"] + $balance ; 
							$update_Balance = "update ".TBL_CUSTOMER." set balance = '".$newBalance."' where customerID= '".$payin["customerID"]."'";
							update($update_Balance);
							
							if(CONFIG_PAYIN_CUST_AGENT == '1')
							{
								$agentPayinContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".CONFIG_PAYIN_AGENT_NUMBER."'");
								if($agentPayinContents["agentType"] == 'Sub')
								{
									updateSubAgentAccount($agentPayinContents["userID"], $balance, $_GET["transID"], 'DEPOSIT', "Transaction Cancelled", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
									$q = updateAgentAccount($agentPayinContents["parentID"], $balance, $_GET["transID"], 'DEPOSIT', "Transaction Cancelled", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
								}else{
									$q = updateAgentAccount($agentPayinContents["userID"], $balance, $_GET["transID"], 'DEPOSIT', "Transaction Cancelled", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
								}	
							}
						
						}
					}elseif($amount["createdBy"] == 'CUSTOMER')
					{
						$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
						 values('".$amount["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Cancelled','DEPOSIT','$balance','".$amount["refNumberIM"]."'
						 )";
						 $this-> insertInto($strQuery);
						 
						 if(CONFIG_ONLINE_AGENT == '1')
						{	
							
							$descript = "Transaction is Cancelled";
							updateAgentAccount($agent, $balance, $amount["refNumberIM"], "DEPOSIT", $descript, 'Agent');	
						}	
					}
					
					if(CONFIG_POST_PAID != '1')
					{
						
										
						$dist = $amount["benAgentID"];
						$benAgentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '$dist'");
						$currentBalance = $benAgentContents["balance"];
								
						$currentBalance =  $currentBalance + $transBalance;
						update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$bank'");
							
						$distributorEntryType = "DEPOSIT";
						$distributorEntryDesc = "Transaction Cancelled";
								 
						$updateBankAccount = true;
						
						}
		
			
		//}
		
		$printingMessage = "Transaction is Cancelled ";
		
	}
//////////////////////////////////////////////Cancelled transactions Ended////////////////////////////
//////////////////////////////////////////////Transaction Failed Or Rejected////////////////////

				if($newStatus == "Failed")////Deposit Agent		
					{
						$updateTrans .= " , deliveredBy = '".$username."',
														failedDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."'";
			  		$descript = "Transaction is Failed";							
						
					
						if($amount["createdBy"] != 'CUSTOMER')
						{
							$payin = selectFrom("select payinBook, customerID, balance from " . TBL_CUSTOMER . " where customerID = '". $amount["customerID"]."'");
							if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")
							{
								
								if(CONFIG_EXCLUDE_COMMISSION == '1')
								{
									$balance = $balance - $commission;
								}else{
									$balance = $balance;
									}
								
									$agentEntryType = "DEPOSIT";
						  		$agentEntryDesc = "Transaction Failed";
						 
									$updateAgentAccount = true;
							
								
								$agentContents = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from " . TBL_ADMIN_USERS . " where userID = '".$amount["custAgentID"]."'");
								$currentBalance = $agentContents["balance"];
								
								$currentBalance = $balance + $currentBalance;
								update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$agent'");
																			
							}elseif($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == "1")
							{
								$newBalance = $payin["balance"] + $balance ; 
								$update_Balance = "update ".TBL_CUSTOMER." set balance = '".$newBalance."' where customerID= '".$payin["customerID"]."'";
								update($update_Balance);
								
								$this-> insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
							 values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction Failed','DEPOSIT','".$balance."','".$_GET["transID"]."','".$agent."'
							 )");
							 
							  if(CONFIG_PAYIN_CUST_AGENT == '1')
								{
									$agentPayinContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".CONFIG_PAYIN_AGENT_NUMBER."'");
									if($agentPayinContents["agentType"] == 'Sub')
									{
										updateSubAgentAccount($agentPayinContents["userID"], $balance,$_GET["transID"], 'DEPOSIT', "Transaction Failed", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
										$q = updateAgentAccount($agentPayinContents["parentID"], $balance, $_GET["transID"], 'DEPOSIT', "Transaction Failed", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
									}else{
										$q = updateAgentAccount($agentPayinContents["userID"], $balance, $_GET["transID"], 'DEPOSIT', "Transaction Failed", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
									}	
								}
							 
							}
						}elseif($amount["createdBy"] == 'CUSTOMER')
						{
							$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
							 values('".$amount["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Failed','DEPOSIT','$balance','".$amount["refNumberIM"]."'
							 )";
							 $this-> insertInto($strQuery);
						}
					if(CONFIG_POST_PAID != '1')
					{
						
						$benAgentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$bank."'");
						//$checkBalance2 = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$bank."'");						
						$currentBalance = $benAgentContents["balance"];
								
						$currentBalanceBank =  $currentBalance + $transBalance;
						update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '$bank'");
							
						
						
						
						
						
							$distributorEntryType = "DEPOSIT";
							$distributorEntryDesc = "Transaction Failed";
						 
							$updateBankAccount = true;
						
						
					  }
					}
		
		//////////////////////////////////////////////Failed transactions Ended////////////////////////////
	//////////////////////////////////////////////Transaction Picked up or Credited////////////////////
								
							if($newStatus == "Picked up" || $newStatus == "Credited")////Deposit Agent		
							{		
								$updateTrans .= " , deliveredBy = '".$username."',
														deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."'";
															
								if(CONFIG_POST_PAID == '1')
								{
									
									
									
									$benAgentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$bank."'");
									
						$currentBalance = $benAgentContents["balance"];
								
						$currentBalanceBank =  $currentBalance + $transBalance;
						update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '$bank'");
						
									
									
									
									
									
									
										$distributorEntryType = "WITHDRAW";
								if($newStatus == "Picked up")
								{
									$distributorEntryDesc = "Transaction Picked up";
									$descript = "Transaction is Picked up";
									}elseif($newStatus == "Credited")
									{
										$distributorEntryDesc = "Transaction Credited";
										$descript = "Transaction is Credited";
									}			
							
						 
								$updateBankAccount = true;
							
							}
							
							
								
								
							}
		
	//////////////////////////Updating Queries and Accounts//////////////
		if($updateAgentAccount)
		{
				if($agentContents["agentType"] == 'Sub')
				{
					updateSubAgentAccount($agentContents["userID"], $balance, $transId, $agentEntryType, $agentEntryDesc, "Agent");
					updateAgentAccount($agentContents["parentID"], $balance, $transId, $agentEntryType, $agentEntryDesc, "Agent");
				}else{
					updateAgentAccount($agentContents["userID"], $balance, $transId, $agentEntryType, $agentEntryDesc, "Agent");
				}
			
		 }
			
			if($updateBankAccount)
			{
				if($benAgentContents["agentType"] == 'Sub')
				{
					updateSubAgentAccount($benAgentContents["userID"], $transBalance, $transId, $distributorEntryType, $distributorEntryDesc, "Distributor");
					updateAgentAccount($benAgentContents["parentID"], $transBalance, $transId, $distributorEntryType, $distributorEntryDesc, "Distributor");
				}else{
					updateAgentAccount($benAgentContents["userID"], $transBalance, $transId, $distributorEntryType, $distributorEntryDesc, "Distributor");
				}
				
			}
		
		$updateTrans .= "  where transID='".$transId."'";
		if($newStatus != "Picked up" && $newStatus != "Credited")////Deposit Agent		
		{
			update($updateTrans);
		}
		
		activities($_SESSION["loginHistoryID"],"UPDATION",$transId,TBL_TRANSACTIONS,$descript);
	insertError($printingMessage);
		///////////////////////////////////////End///////////////////////////////
			
	}	
	/////////////
		
	}
?>
