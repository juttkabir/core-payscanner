<?php  
session_start();
include ("../include/config.php");
include ("security.php");
//if(isset($_REQUEST["format"]) && $_REQUEST["format"] == "csv")	
//{
	/* Export version is csv */
	$intDispatchNumber = date("dmY").str_pad($_REQUEST["recId"],2,"0",STR_PAD_LEFT);
	
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
	
	header('Content-type: application/octet-stream');
	header("Content-disposition: filename=MIC".$intDispatchNumber.".TXT");
	
	
	if(!empty($_REQUEST["recId"]))
	{
		$strSql = "
				select 
					tbd.bankName, 
					tr.transID, 
					tr.refNumberIM, 
					tr.refNumber, 
					tr.benID, 
					tr.transDate,
					tr.localAmount,
					tr.benAgentID,
					tr.remarks,
					tr.refNumber, 
					tr.dispatchNumber,
					tr.transType, 
					tr.faxNumber,
					tr.transStatus, 
					ben.firstName as bf,
					ben.middleName as bm,
					ben.lastName as bl,
					ben.Country,
					ben.IDType,
					ben.IDNumber,
					ben.userType,
					ben.CPF,
					ben.Phone as bp,
					ben.Address as ba,
					ben.Address1 as ba1,
					ben.City as bc,
					ben.State as bs,
					ben.Zip as bz,
					cust.firstName,
					cust.middleName,
					cust.lastName,
					cust.IDNumber,
					cust.Address,
					cust.Address1, 
					cust.city,
					cust.state,
					cust.zip,
					cust.country,
					tbd.accNo,
					tbd.accountType,
					tbd.branchCode,
					tbd.originalBankID
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben, 
					customer as cust
				where 
					tr.dispatchNumber = '".$_REQUEST["recId"]."' and
					tbd.transID = tr.transID and 
					tr.benID = ben.benID and
					tr.customerID = cust.customerID					
				order by 
					tbd.bankName asc";
	}
	
	if(strtolower($_REQUEST["searchTrans"]) == "old" && !empty($_REQUEST["at"]))
	{
		$arrAllTransactions = substr($_REQUEST["at"],0,strlen($_REQUEST["at"])-1);
		
		$strSql = "
				select 
					tbd.bankName, 
					tr.transID, 
					tr.refNumberIM, 
					tr.refNumber, 
					tr.benID, 
					tr.transDate,
					tr.localAmount,
					tr.benAgentID,
					tr.remarks,
					tr.refNumber, 
					tr.dispatchNumber,
					tr.transType,
					tr.faxNumber,
					tr.transStatus, 
					ben.firstName as bf,
					ben.middleName as bm,
					ben.lastName as bl,
					ben.Country,
					ben.IDType,
					ben.IDNumber,
					ben.userType,
					ben.CPF,
					ben.Phone as bp,
					ben.Address as ba,
					ben.Address1 as ba1,
					ben.City as bc,
					ben.State as bs,
					ben.Zip as bz,
					cust.firstName,
					cust.middleName,
					cust.lastName,
					cust.IDNumber,
					cust.Address,
					cust.Address1, 
					cust.city,
					cust.state,
					cust.zip,
					cust.country,
					tbd.accNo,
					tbd.accountType,
					tbd.branchCode,
					tbd.originalBankID
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben, 
					customer as cust
				where 
					tr.transID IN (".$arrAllTransactions.") and
					tbd.transID = tr.transID and 
					tr.benID = ben.benID and
					tr.customerID = cust.customerID					
				order by 
					tbd.bankName asc";
		
		
	}
	
	if(!empty($strSql))
		$fullRS = SelectMultiRecords($strSql);
	//debug($strSql);
	//debug($fullRS);
	
	$strCsvOutput = "";
	$strCsvSeparator = "\t";
		
	$cpfseperator = array("-","/",".");
	for($i=0; $i < count($fullRS); $i++)
	{
		
		$beneficiaryFullName = $fullRS[$i]["bf"]." ".$fullRS[$i]["bm"]." ".$fullRS[$i]["bl"];
		$senderFullName = $fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"];
		$strTransactionType = strpos($fullRS[$i]["transType"], "Pick");
		
		if(strpos($fullRS[$i]["transType"], "Pick") !== false)
			$strPaymentType = "B"; // Cash 
		elseif(strpos($fullRS[$i]["transType"], "Bank") !== false)
			$strPaymentType = "D"; // Bank Transfer 
		
		
		/**
		 * I suggest you guys to mask it and use just the number 
		 * of our transactions not considering the letters (1234 for MC-1234HEAD,for example)
		 */
		$strReferenceNumber = $fullRS[$i]["refNumberIM"];
		preg_match("/\-[0-9]+/",$strReferenceNumber,$arrMatches);
		$intReferenceNumberOnlyNumeric = substr($arrMatches[0],1);

		/**
		 * In Transaction Type, there are two settings, N=new and V=void. N would be for all new transactions (authorized ones) 
		 * and V would be for a cancel request. It can occur that we send an order for payment and we decide to cancel it before 
		 * it has been paid out in Brazil. We would send a new file for Banco Rendimento with the status of this transaction set
		 * as Void (V).
		 */
		if(strpos($fullRS[$i]["transStatus"], "AwaitingCancellation") !== false)
			$charTransTypeShortCode = "V";
		else
			$charTransTypeShortCode = "N";		
		 
		//$strCsvOutput .= "[ ".$fullRS[$i]["transID"]." ] ";
		$strCsvOutput .= substr(date("m/d/Y",strtotime($fullRS[$i]["transDate"])),0,20).$strCsvSeparator; // date
		$strCsvOutput .= substr(date("h:i:s",strtotime($fullRS[$i]["transDate"])),0,20).$strCsvSeparator; // time
		
		//$strCsvOutput .= substr($fullRS[$i]["faxNumber"],0,9).$strCsvSeparator; // dispatch number 
		//$strCsvOutput .= substr($fullRS[$i]["refNumberIM"],0,9).$strCsvSeparator; // updated to Transaction number
		$strCsvOutput .= substr($intReferenceNumberOnlyNumeric,0,9).$strCsvSeparator; // updated to Transaction Number  only numeric part as in #4107
		
		$strCsvOutput .= substr(str_replace($cpfseperator,"",$fullRS[$i]["CPF"]),0,20).$strCsvSeparator; // CPF number
		$strCsvOutput .= trim(substr($beneficiaryFullName,0,40)).$strCsvSeparator; // beneficiary full name
		$strCsvOutput .= substr($fullRS[$i]["bp"],0,25).$strCsvSeparator; // beneficiary phone
		$strCsvOutput .= substr($fullRS[$i]["ba"]." ".$fullRS[$i]["ba1"],0,70).$strCsvSeparator; // beneficiary address
		$strCsvOutput .= substr($fullRS[$i]["bc"],0,25).$strCsvSeparator; // beneficiary city
		$strCsvOutput .= substr($fullRS[$i]["bs"],0,2).$strCsvSeparator; // beneficiary state
		$strCsvOutput .= substr($fullRS[$i]["bz"],0,15).$strCsvSeparator; // bene zip
		$strCsvOutput .= substr($fullRS[$i]["bc"],0,18).$strCsvSeparator; // bene country
		$strCsvOutput .= substr($fullRS[$i]["originalBankID"],0,3).$strCsvSeparator;; // bene bank code / bank id 
		$strCsvOutput .= substr($fullRS[$i]["branchCode"],0,6).$strCsvSeparator; // bene branch code / bank id 
		$strCsvOutput .= substr($fullRS[$i]["accNo"],0,12).$strCsvSeparator; // ben account number
		
		$strCsvOutput .= ($fullRS[$i]["accountType"] == "Savings"?"POUP":"C/C").$strCsvSeparator; 
		
		/*sender fields*/
		$strCsvOutput .= trim(substr($senderFullName,0,40)).$strCsvSeparator;
		$strCsvOutput .= substr($fullRS[$i]["IDNumber"],0,20).$strCsvSeparator;
		$strCsvOutput .= substr($fullRS[$i]["Address"]." ".$fullRS[$i]["Address1"],0,70).$strCsvSeparator; // sender address
		$strCsvOutput .= substr($fullRS[$i]["city"],0,25).$strCsvSeparator;
		$strCsvOutput .= substr($fullRS[$i]["state"],0,2).$strCsvSeparator;
		$strCsvOutput .= substr($fullRS[$i]["zip"],0,15).$strCsvSeparator;
		$strCsvOutput .= substr($fullRS[$i]["country"],0,18).$strCsvSeparator;
		$strCsvOutput .= substr(number_format($fullRS[$i]["localAmount"], 2, ".",""),0,13).$strCsvSeparator; // local amount 
		
		$strCsvOutput .= substr("L",0,1).$strCsvSeparator; //Currency to use: D = Dollar, L = Local (for now only L) L by default
		
		$strCsvOutput .= substr($fullRS[$i]["remarks"],0,120).$strCsvSeparator; // msg from sender to beneficiary
		
		$strCsvOutput .= $charTransTypeShortCode.$strCsvSeparator; // transaction type

		$strCsvOutput .= substr($fullRS[$i]["refNumber"],0,20).$strCsvSeparator;   //Remitance internal codeRemitance internal code -> Minas Center Code
		$strCsvOutput .= $strPaymentType.$strCsvSeparator; // payment type
		$strCsvOutput .= substr("",0,25); // folio number
		
		$strCsvOutput .= "\r\n";
	}
	
	echo $strCsvOutput;
	/* End of csv version */
//}
?>