<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

if($_GET["Submit"]!= "")
{
	if($_GET["origCountry"] != "")
		$origCountry = $_GET["origCountry"];
	if($_GET["destCountry"] != "")
		$destCountry = $_GET["destCountry"];
	if($_GET["customer"] != "")
		$customer = $_GET["customer"];	
	if($_GET["feeType"] != "")
		$feeType = $_GET["feeType"];	
	if($_GET["transactionType"] != "")
		$transactionType = $_GET["transactionType"];	
		if($_GET["feeBasedOn"] != "")
		$feeBasedOn = $_GET["feeBasedOn"];	
	
	$submit = $_GET["Submit"];
}


$SQL_Qry = "select * from ".TBL_MANAGE_FEE." where manageFeeID='".$_GET["feeID"]."'";
$fees = selectFrom($SQL_Qry);
$distID = selectFrom("select cp_ida_id from ".TBL_COLLECTION." where cp_id = '".$fees["agentNo"]."'");
//debug($fees);
if($_POST["feeType"] != "")
{
	$feeType = $_POST["feeType"];
	
}else{
 $feeType = $fees["feeType"];
}

if($_POST["distributor"]!= "")
{
	$distributor = $_POST["distributor"];
	$agentNo = '';
}else{
	$distributor = $distID["cp_ida_id"];
}
if($_POST["feeType"] != "")
{
	$agentNo = $_POST["agentID"];
}else{
$agentNo = $fees["agentNo"];
}

if($_POST["interval"] != "")
{
	$interval = $_POST["interval"];
}else{
$interval = $fees["intervalUsed"];
}///////////

if($_POST["amountRangeFrom"] != "")
{
	$amountRangeFrom = $_POST["amountRangeFrom"];
}else{
$amountRangeFrom = $fees["amountRangeFrom"];
}

if($_POST["amountRangeTo"] != "")
{
	$amountRangeTo = $_POST["amountRangeTo"];
}else{
$amountRangeTo = $fees["amountRangeTo"];
}

if($_POST["Fee"] != "")
{
	$regularFee = $_POST["Fee"];
}else{
$regularFee = $fees["regularFee"];
}
//

if($_POST["payinFee"] != "")
{
	$payinFee = $_POST["payinFee"];
}else{
$payinFee = $fees["payinFee"];
}

if($_POST["feeBasedOn"] != "")
	$feeBasedOn = $_POST["feeBasedOn"];
else
	$feeBasedOn = $fees["feeBasedOn"];
$_SESSION["feeBasedOn"] = $feeBasedOn;
?>
<html>
<head>
	<title>Update Fee/Commission</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}	
	
function checkForm(theForm) {
	if(theForm.amountRangeFrom.value == "" || IsAllSpaces(theForm.amountRangeFrom.value)){
    	alert("Please provide Lower range of money.");
        theForm.amountRangeFrom.focus();
        return false;
    }
	if(theForm.amountRangeTo.value == "" || IsAllSpaces(theForm.amountRangeTo.value)){
    	alert("Please provide Upper range of money.");
        theForm.amountRangeTo.focus();
        return false;
    }
	/*if(theForm.amountRangeTo.value < theForm.amountRangeFrom.value){
    	alert("Please provide valid Upper range of money.");
        theForm.amountRangeTo.focus();
        return false;
	}*/
	 if(theForm.feeType.options.selectedIndex == 0){
		alert("Please select Fee Type.");
		theForm.feeType.focus();
		return false;
	}
	<? 
if(CONFIG_FEE_BASED_COLLECTION_POINT == 1){
?>
	if(theForm.feeType.value == "collectionPoint"){
		
		if(theForm.distributor.options.selectedIndex == 0){
			alert("Please select Distributor from the list.");
			theForm.distributor.focus();
			return false;
		}
		if(theForm.agentID.options.selectedIndex == 0){
			alert("Please select Collection Point from the list.");
			theForm.agentID.focus();
			return false;
		}
	}
<?
}
?>
	if(theForm.Fee.value == "" || IsAllSpaces(theForm.Fee.value)){
    	alert("Please provide Fee for spcified money ranges.");
        theForm.Fee.focus();
        return false;
    }
	
	<?
/**
 * Adding the payment mode based commssion
 * Ticket# 3319
 */
if(CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1")
{
?>
	if(theForm.paymentMode.options.selectedIndex == 0){
			alert("Please select the payment mode");
			theForm.paymentMode.focus();
			return false;
		}
<?
}
?>
	
	
	return true;
	
}


function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
	</script>
	<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#FFFFFF" size="2">Update <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></strong></td>
  </tr>
  <form action="add-fee-conf.php?origCountry=<?=$origCountry;?>&destCountry=<?=$destCountry;?>&customer=<?=$customer;?>&feeType=<?=$feeType;?>&Submit=<?=$submit;?>" method="post"onSubmit="return checkForm(this);">
  <input type="hidden" name="feeID" value="<?=$_GET["feeID"]; ?>">
  <tr>
      <td align="center"> 
        <table width="500" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="500" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Update 
                    <? echo $systemPre;?>Money Transfer <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
		<? if ($_GET["msg"] != ""){ ?>
		<tr bgcolor="#ededed"><td colspan="2" bgcolor="#FFFFCC"><table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td><td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; ?></td></tr></table></td></tr><? } ?>
		<tr bgcolor="#ededed">
			<td colspan="2"><a class="style2" href="fee-list.php?origCountry=<?=$origCountry;?>&destCountry=<?=$destCountry;?>&customer=<?=$customer;?>&feeType=<?=$feeType;?>&Submit=<?=$submit;?>">Go Back</a></td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center"><font color="#FF0000">* Compulsory 
              Fields</font></td>
		</tr>
        <tr bgcolor="#ededed">
            <td width="312"><font color="#005b90"><strong>Originating Country<font color="#ff0000">*</font></strong></font></td>
            <td width="183"><input type="hidden" name="origCountry" value="<?=$fees["origCountry"] ?>"><?=$fees["origCountry"] ?></td>
        </tr>
        
         <?
        if(CONFIG_FEE_CURRENCY == '1')
        {
        ?>
        <tr bgcolor="#ededed">
            <td width="312"><font color="#005b90"><strong>Originating Currency<font color="#ff0000">*</font></strong></font></td>
            <td width="183"><input type="hidden" name="currencyOrigin" value="<?=$fees["currencyOrigin"] ?>"><?=$fees["currencyOrigin"] ?></td>
        </tr>
        <?
      	}
        ?>
        
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Destination Country<font color="#ff0000">*</font></strong></font></td>
          <td><input type="hidden" name="destCountry" value="<?=$fees["destCountry"] ?>">
            <?=$fees["destCountry"] ?></td>
        </tr>
         <?
        if(CONFIG_FEE_CURRENCY == '1')
        {
        ?>
        <tr bgcolor="#ededed">
            <td width="312"><font color="#005b90"><strong>Destination Currency<font color="#ff0000">*</font></strong></font></td>
            <td width="183"><input type="hidden" name="currencyDest" value="<?=$fees["currencyDest"] ?>"><?=$fees["currencyDest"] ?></td>
        </tr>
        <?
      	}
        ?>
        <!-- <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Transaction Type<font color="#ff0000">*</font></strong></font></td>
          <td><input type="hidden" name="transactionType" value="<?=$fees["transactionType"] ?>">
            <?=$fees["transactionType"] ?></td> -->
        </tr>
        <!-- Added by Niaz Ahmad Against Ticket # 2538 at 24-10-2007-->
        <? if (CONFIG_FEE_BASED_DROPDOWN == "1") { ?>
        <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Fee Base<font color="#ff0000">*</font></strong></font></td>
            <td width="210"><SELECT name="feeBasedOn" id="feeBasedOn" style="font-family:verdana; font-size: 11px" <? if(CONFIG_FEE_BASED_DROPDOWN == 1){echo ("onChange=\"document.forms[0].action='update-fee.php?feeID=$_GET[feeID]'; document.forms[0].submit();\"");}?>>
                <OPTION value="Generic" <? if($fees["feeBasedOn"] =="Generic"){ echo("selected");}?> >Generic</OPTION>
                <OPTION value="transactionType" <? if($fees["transactionType"] != "Generic"){ echo("selected");}?> >transactionType</OPTION> 
               <? if(CONFIG_CUSTOMER_CATEGORY == 1){?>
				<OPTION value="customer" <? if($fees["feeBasedOn"] =="customer"){ echo("selected");}?>>Customer Category</option>
				<? }?>
             </SELECT>
             <script language="JavaScript">
		           	SelectOption(document.forms[0].feeBasedOn, "<?=$_SESSION["feeBasedOn"] ?>");
		       	  </script>
          </td>
      </tr>
      <? if($_SESSION["feeBasedOn"] == "transactionType") { ?>
      <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Transaction Type<font color="#ff0000">*</font></strong></font></td>
            <td width="210"><SELECT name="transactionType2" id="transactionType" style="font-family:verdana; font-size: 11px">
               <!-- <OPTION value="Pick up">Pick up</OPTION> -->
                <OPTION value="Pick up" <? if($fees["transactionType"] =="Pick up"){ echo("selected");}?> >Pick up</OPTION>
                <OPTION value="Bank Transfer" <? if($fees["transactionType"] =="Bank Transfer"){ echo("selected");}?>>Bank Transfer</OPTION>
                <OPTION value="Home Delivery" <? if($fees["transactionType"] =="Home Delivery"){ echo("selected");}?>>Home Delivery</OPTION>
    
             </SELECT>
             <script language="JavaScript">
		           	SelectOption(document.forms[0].transactionType2, "<?=$_SESSION["transactionType2"] ?>");
		       	  </script>
          </td>
      </tr>
      
      <? 
         } if($_SESSION["feeBasedOn"] == "customer"){
	 ?>

			<tr bgcolor="#ededed">
	       		<td><font color="#005b90"><strong>Select Customer Category<font color="#ff0000">*</font></strong></font></td>
		    	<td>
			 <select name="catID" id="catID" style="font-family:verdana; font-size: 11px">
		 	<option value="">-Select One-</option>
		  <?
			$custCatSql = "select id,name from ".TBL_CUSTOMER_CATEGORY." where enabled = 'Y'";
			$category = selectMultiRecords($custCatSql);
			for($k =0; $k <count($category); $k++){
		  ?>
		  <option value="<?=$category[$k]["id"]; ?>" <? echo ($fees["transactionType"]==$category[$k]["id"] ? "selected" : "")?>>
				   <?  echo($category[$k]["name"]); ?></option>	
		<? } ?>		   
		</select>
			</td>
	    </tr>		   	
	
		<?
			}
       }
     
       $refreshFlag = false;
        if(CONFIG_FEE_DENOMINATOR == 1 || CONFIG_FEE_BASED_COLLECTION_POINT == 1 || CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1 || CONFIG_FEE_DISTRIBUTOR == 1)
        {
        	$refreshFlag = true;	
        }
        ?>
        
        <tr bgcolor="#ededed">
            <td width="312"><font color="#005b90"><strong>Amount Range<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="amountRangeFrom" value="<?=$amountRangeFrom?>" size="10" maxlength="16"> To <input type="text" name="amountRangeTo" value="<?=$amountRangeTo?>" size="10" maxlength="16"></td>
        </tr>
       <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?> Type<font color="#ff0000">*</font></strong></font></td>
            <td width="210">
            	<SELECT name="feeType" id="feeType" style="font-family:verdana; font-size: 11px" <? if($refreshFlag){echo ("onChange=\"document.forms[0].action='update-fee.php?feeID=$_GET[feeID]'; document.forms[0].submit();\"");}?>>
                <OPTION value="">- Select <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?> Type -</OPTION>
								<option value="fixed" <? if($feeType =="fixed"){ echo("selected");}?>>Fixed <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<option value="percent" <? if($feeType=="percent"){ echo("selected");} ?>>Percentage of Amount</option>
								<? if(CONFIG_FEE_DENOMINATOR == 1){?>
								<option value="denominator" <? if($feeType == "denominator"){ echo("selected");} ?>>Denomination Based <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
								<? if(CONFIG_FEE_AGENT_DENOMINATOR == 1 ){// removed  (&& CONFIG_FEE_DISTRIBUTOR != 1) from the existing condition against ticket #3399 Money Talks - Fee Management Module done by khola.
								?>
								<option value="agentDenom" <? if($feeType == "agentDenom"){ echo("selected");} ?>>Agent and Denomination Based <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
								<? if(CONFIG_FEE_AGENT == 1){// removed  (&& CONFIG_FEE_DISTRIBUTOR != 1) from the existing condition against ticket #3399 Money Talks - Fee Management Module done by khola.
								?>
								<option value="agentBased" <? if($feeType == "agentBased"){ echo("selected");} ?>>Agent Based <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
								<? if(CONFIG_FEE_DISTRIBUTOR == 1){?>
								<option value="distributorBased" <? if($feeType == "distributorBased"){ echo("selected");} ?>>Distributor Based <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
								<? if(CONFIG_FEE_BASED_COLLECTION_POINT == 1){?>
								<option value="collectionPoint" <? if($feeType == "collectionPoint"){ echo("selected");} ?>>Collection Point <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
							</SELECT>
              <!--script language="JavaScript">
		         			SelectOption(document.forms[0].feeType, "<?=$feeType ?>");
		       	  </script--></td>
		    
        </tr> 
        
		
		<? if(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1 && ($feeType == "denominator" || $feeType == "agentDenom" || $feeType == "agentBased"	)) { ?>
			<tr bgcolor="#ededed">
				<td width="285"><font color="#005b90"><strong>Fee Sub Type<font color="#ff0000">*</font></strong></font></td>
				<td>
					<select name="feeSubType">
						<option value="fix" <?=($fees["feeSubType"]=="fix"?"selected":"")?>> - FIXED - </option>
						<option value="percent" <?=($fees["feeSubType"]=="percent"?"selected":"")?>> - PERCENTAGE - </option>
					</select>
				</td>
			</tr>
		
		<? } ?>
		
		
        <?
        	if(CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"):
        ?>
        	<tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Payment Mode<font color="#ff0000">*</font></strong></font></td>
            <td>
            		<select name="paymentMode">
            			<option value="">-Payment Mode-</option>
            			<option value="cash" <?=($fees["paymentMode"]=="cash"?"selected":"")?>>Cash Payments</option>
            			<option value="bank" <?=($fees["paymentMode"]=="bank"?"selected":"")?>>Bank Payments</option>
            			<option value="cheque" <?=($fees["paymentMode"]=="cheque"?"selected":"")?>>Cheque Payments</option>
            			<? 
                  	/**
                  	 * @Ticket# 3564
                  	 */
                  	if(CONFIG_MONEY_PAID_OPTION_BY_CARD == 1) { 
                  ?>
                  	<option value="card" <?=($fees["paymentMode"]=="card"?"selected='selected'":"");?>>Card Payments</option>
                  <? } ?>
            		</select>
            </td>
        </tr>
        
        <? 
        endif;
        if($feeType == "denominator" || $feeType == "agentDenom"){?>
		     		<tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Denominator[Interval]<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="interval" value="<?=$interval?>" size="15" maxlength="16"></td>
        </tr>
		     <? }?>
        
        
        <? if( $feeType == "agentDenom" || $feeType == "agentBased" || $feeType == "distributorBased")
		       		{
		       
		       ?>  
		       
       			<tr bgcolor="#ededed">
				<!--
				Below  condition $feeType == "distributorBased" was CONFIG_FEE_DISTRIBUTOR == "1" initially have changed it since if CONFIG_FEE_DISTRIBUTOR and CONFIG_FEE_AGENT both configs on then both fees were not allowed to be added parellaly. Against ticket #3399 by khola 
				-->
       				<td><font color="#005b90"><strong>Select <? if($feeType == "distributorBased"){echo("Distributor");}else{ echo("Agent");}?></strong></font></td>
	       			<td>
	       				<select name="agentID" style="font-family:verdana; font-size: 11px">
								<option value="">- Select One -</option>
								<?
									if($feeType == "distributorBased")
									{
										$agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'N' order by agentCompany");
										}else{			
										$agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'ONLY' order by agentCompany");
									}
										for ($i=0; $i < count($agents); $i++){
											
									?>
								<option value="<?=$agents[$i]["userID"]; ?>" <? if($fees["agentNo"] == $agents[$i]["userID"]){ echo(" selected ");}?>><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
								<?
										}
									?>
							  </select>
							  <script language="JavaScript">
		         			SelectOption(document.forms[0].agentID, "<?=$agentNo ?>");
		          	</script>
	       			</td>
       			</tr>
       
		       <?
		      		}
	 ?>    
        
        <? if( $feeType == "collectionPoint")
		       {
		       
		       ?>  
		       
       			<tr bgcolor="#ededed">
       				<td><font color="#005b90"><strong>Select Distributor<font color="#ff0000">*</font></strong></font></td>
	       			<td>
	       				<select name="distributor" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].action='update-fee.php?feeID=<?=$_GET["feeID"]?>'; document.forms[0].submit();">
								<option value="">- Select One -</option>
								<?
									
									$distCollectioPoint = selectMultiRecords("select distinct(a.username), a.userID as userID, a.agentCompany from ".TBL_ADMIN_USERS." as a, ".TBL_COLLECTION." as c where a.userID = c.cp_ida_id order by a.agentCompany");
									
										for ($j=0; $j < count($distCollectioPoint); $j++){
											
									?>
								<option value="<?=$distCollectioPoint[$j]["userID"]; ?>"><? echo($distCollectioPoint[$j]["username"]." [".$distCollectioPoint[$j]["agentCompany"]."]"); ?></option>
								<?
										}
									?>
							  </select>
							  <script language="JavaScript">
		         			SelectOption(document.forms[0].distributor, "<?=$distributor ?>");
		          	</script>
	       			</td>
       			</tr>
       		
	       		<?
	       		if($distributor != ""){
	       			$collection = selectMultiRecords("SELECT cp_id, cp_branch_name, cp_city FROM  ".TBL_COLLECTION." where cp_ida_id = '".$distributor."' ORDER BY cp_branch_name ");
						?>
							 <tr bgcolor="#ededed">
							  <td><font color="#005b90"><strong>Collection Point<font color="#ff0000">*</font></strong></font>
								</td>
							  <td>
									<select name="agentID">
										<option value="">-- Select Collection Point --</option>
										<?
											for ($k=0; $k < count($collection); $k++)
											{
											
													$collAdd = $collection[$k]["cp_branch_name"]."-".$collection[$k]["cp_city"];	
													$cpID  = $collection[$k]["cp_id"];							
													
												?>
												<OPTION value="<?=$cpID; ?>">
				                  <? echo $collAdd; ?>
				                  </OPTION>
												<?
											}
												?>
												</select>
												<script language="JavaScript">
													SelectOption(document.forms[0].agentID, "<?=$agentNo; ?>");
												</script>
								</td>
							  </tr>
	       
			       <?
		       		}
		      	}
	 ?>    
        
          <? if(CONFIG_PAYIN_CUSTOMER == 1){?>
        <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Regular Customer <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></strong></font></td>
            <td><input type="text" name="Fee" value="<?=$regularFee?>" size="15" maxlength="16"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Payin Book Customer <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></strong></font></td>
            <td><input type="text" name="payinFee" value="<?=$payinFee?>" size="15" maxlength="16"></td>
        </tr>
		    <? }else if (CONFIG_PAYIN_CUSTOMER == 2){?>
		    <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Regular Customer <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></strong></font></td>
            <td><input type="text" name="Fee" value="<?=$regularFee?>" size="15" maxlength="16"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Out City Customer <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></strong></font></td>
            <td><input type="text" name="payinFee" value="<?=$payinFee?>" size="15" maxlength="16"></td>
        </tr>
		     <? }else{?>
        <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?><font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="Fee" value="<?=$regularFee?>" size="15" maxlength="16"></td>
        </tr>
        <? }?>
        <!--tr bgcolor="#ededed">
            <td width="212"><font color="#005b90"><strong><? echo $systemPre;?> Fee<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="Fee" value="<?=stripslashes($fees["Fee"]); ?>" size="15" maxlength="16"></td>
        </tr-->
          <? 
         	 if(CONFIG_SHARE_OTHER_NETWORK == '1')
		       { 
		       	$jointClients = selectMultiRecords("select * from ".TBL_JOINTCLIENT." where 1 and isEnabled = 'Y' ");
		       	
		       	$sharedInfo = selectFrom("select * from ".TBL_SHARED_FEE." where localFeeId = '".$_GET["feeID"]."'");
		       	?>
		       	 	<tr bgcolor="#ededed">
						<td>
							<font color="#005b90"><strong>Available to</strong></font>
						</td>
						<td> 
							<select name="remoteAvailable" id="remoteAvailable">
								<option value=""> Select One </option>
								<?
								for($cl = 0; $cl < count($jointClients); $cl++)
								{
								?>
								
									 <OPTION value="<?=$jointClients[$cl]["clientId"]; ?>" <? echo (($sharedInfo["remoteServerId"] == $jointClients[$cl]["clientId"]) ? "selected"  : "")?>> <?=$jointClients[$cl]["clientName"]?></option>
								<?
								}
								?>
		            			
						</td>
					</tr>
		       
		       	<?
		       }
		       ?>
        
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" value=" Save ">&nbsp;&nbsp; <input type="reset" value=" Clear ">
			</td>
		</tr>
		
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>