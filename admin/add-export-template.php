<?
session_start();
include ("../include/config.php");
include ("security.php");

if ($_POST["export"] != "") {
	$export = $_POST["export"];
}

if ($_POST['tempTitle'] != "") {
	$tempTitle = $_POST['tempTitle'];	
}

if ($_POST['Format'] != "") {
	$format	= $_POST['Format'];
}

if ($_POST["Submit"] == "Save") {
	if ($_POST["tempTitle"] == '') {
		$msg2 = CAUTION_MARK . " Please provide the template title";	
	} else if ($_POST["export"] == '') {
		$msg2 = CAUTION_MARK . " Please select export type from drop down";
	} else if (!is_array($_POST["fieldsID"])) {
		$msg2 = CAUTION_MARK . " Please select some fields to export";
	} else {
		
		$filters = (is_array($_POST["filters"]) ? implode(",",$_POST["filters"]) : "");
		
		if ($_GET["tempID"] == "") {
			$Ins_Qry = "INSERT INTO " . TBL_EXPORT_TEMPLATE . " 
									SET `title`      = '".checkValues($_POST['tempTitle'])."',
											`format`     = '".$_POST['Format']."',
											`exportName` = '".$export."',
											`filters`    = '".$filters."' ";
			insertInto($Ins_Qry);
			$tempID = @mysql_insert_id();
			
			foreach ($_POST["fieldsID"] AS $key => $value) {
				$Get_title = selectFrom("SELECT `field_title` FROM " . TBL_EXPORT_DEFAULT . " WHERE `id` = '".$value."'");
				$InsFields_Qry = "INSERT INTO " . TBL_EXPORT_FIELDS . " 
													SET `tempID`          = '".$tempID."',
															`exportDefaultID` = '".$value."',
															`field_title`     = '".$Get_title["field_title"]."' ";
				insertInto($InsFields_Qry);
			}
			
			insertError("Your template is added successfully");
		} else {
			$Update_Qry = "UPDATE " . TBL_EXPORT_TEMPLATE . " 
										SET `title`  = '".checkValues($_POST['tempTitle'])."',
												`format` = '".$_POST['Format']."',
												`exportName` = '".$export."',
												`filters` = '".$filters."' 
												WHERE `id` = '".$_GET["tempID"]."' ";
			update($Update_Qry);
			
			$DEL_Qry = "DELETE FROM " . TBL_EXPORT_FIELDS . " WHERE `tempID` = '".$_GET["tempID"]."'";
			deleteFrom($DEL_Qry);
			
			foreach ($_POST["fieldsID"] AS $key => $value) {
				$Get_title = selectFrom("SELECT `field_title` FROM " . TBL_EXPORT_DEFAULT . " WHERE `id` = '".$value."'");
				$InsFields_Qry = "INSERT INTO " . TBL_EXPORT_FIELDS . " 
													SET `tempID`          = '".$_GET["tempID"]."',
															`exportDefaultID` = '".$value."',
															`field_title`     = '".$Get_title["field_title"]."'
													";
				insertInto($InsFields_Qry);
			}
			
			insertError("Your template is updated successfully");
		}
		redirect("export-files-mgt.php?msg=Y&success=Y");
	}
}

if ($_GET["tempID"] != "") {
	
	$templates  = selectFrom("SELECT * FROM " . TBL_EXPORT_TEMPLATE . " WHERE `id` = '".$_GET["tempID"]."'");
	$fieldsCont = selectMultiRecords("SELECT * FROM " . TBL_EXPORT_FIELDS . " WHERE `tempID` = '".$_GET["tempID"]."'");
	
	$export = $templates["exportName"];
	$tempTitle = $templates["title"];
	$format = $templates["format"];
}

?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><b><strong><font class="topbar_tex"><? echo ($_GET["tempID"] != "" ? "Update" : "Add New") ?> Template</font></strong></b></td>
  </tr>
  <tr>
    <td>
	<table width="70%"  border="0">
  <tr>
    <td><fieldset>
    <legend class="style2"><? echo ($_GET["tempID"] != "" ? "Update" : "Add New") ?> Template </legend>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			<form name="frmAddTemp" id="frmAddTemp" action="add-export-template.php?tempID=<? echo $_GET["tempID"] ?>" method="post">
			  <? if($msg2 != "") {
			  ?>
			  <tr align="center">
			    <td colspan="2" class="tab-r"><? echo $msg2 ?><br></td>
			    </tr>
			  <?
			  }
			  
			  ?>
		    <tr>
			 	<td width="35%" bgcolor="#DFE6EA"><a href="export-files-mgt.php" class="style2">Go Back</a></td>
			    <td width="65%" bgcolor="#DFE6EA">&nbsp;
			      </td>
			  </tr>
		    <tr>
			 	<td width="35%" align="right" bgcolor="#DFE6EA">
				  Template Title*</td>
			    <td width="65%" bgcolor="#DFE6EA">
			   <input type="text" name="tempTitle" id="tempTitle" value="<? echo $tempTitle ?>" maxlength="100">
			      </td>
			  </tr>
			  <tr>
				<td align="right" valign="top" bgcolor="#DFE6EA">Format</td>
			    <td bgcolor="#DFE6EA">
			    <input type="radio" name="Format" id="Format" value="xls" checked>
					xls
					<br>
					<input type="radio" name="Format" id="Format" value="csv" <? echo ($format == "csv" ? "checked" : "") ?>>
					csv
					<br>
					<input type="radio" name="Format" id="Format" value="txt" <? echo ($format == "txt" ? "checked" : "") ?>>
					txt
				</td>
			  </tr>
			  <tr>
				  <td align="right" valign="top" bgcolor="#DFE6EA">Select Export*</td>
			    <td bgcolor="#DFE6EA">
				<select name="export" id="export" style="font-family:verdana; font-size: 11px" onChange="document.frmAddTemp.submit();">
				<option value="">-- Select --</option>
				<option value="Transactions" <? echo ($export == "Transactions" ? "selected" : "") ?>>Transactions</option>
				</select>
				</td>
			  </tr>
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">Select Fields to Export* </td>
			    <td bgcolor="#DFE6EA">
			<?	if ($export != '') {  ?>
						Hold Ctrl key for multiple selection<br>
			    	<select name="fieldsID[]" id="fieldsID[]" style="font-family:verdana; font-size: 11px" size="10" multiple>
			 	<?
			  		$fields_Qry = "SELECT * FROM " . TBL_EXPORT_DEFAULT . " WHERE `export` = '".$export."' ORDER BY `table` DESC";
			  		$fields = selectMultiRecords($fields_Qry);
			  		for ($i = 0; $i < count($fields); $i++) {
				  		$selected = "";
							for ($k = 0; $k < count($fieldsCont); $k++) {
								if ($fields[$i]["id"] == $fieldsCont[$k]["exportDefaultID"]) {
									$selected = "selected";	
								}
							}
			  ?>
			  		<option value="<? echo $fields[$i]["id"] ?>" <?=$selected?>><? echo $fields[$i]["field_title"] ?></option>
				<?	}  ?>
						</select>
			<?	} else {  ?>
					<i>Please select export type from above drop down</i>
			<?	}  ?>
			    </td>
			    </tr>
			  <!--  This filter logic is omitted for some how, will be used in future -->
			  <!--tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">Select Filters </td>
			    <td bgcolor="#DFE6EA">Hold Ctrl key for multiple selection<br>
			    	<select name="filters[]" id="filters[]" style="font-family:verdana; font-size: 11px" size="10" multiple>
			  		<option value="Date">Date</option>
			  		<option value="transStatus">Transaction Status</option>
			  		<option value="transType">Transaction Type</option>
						</select>
			    </td>
			    </tr-->
			  <tr>
			    <td align="center" bgcolor="#DFE6EA" colspan="2"><input type="submit" name="Submit" id="Submit" class="flat" value="Save"></td>
			    </tr>
			    	</form>
			</table>
        </fieldset></td>
  </tr>
</table>

	</td>
  </tr>
</table>
</body>
</html>