<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

if ($offset == "")
	$offset = 0;
	
$limit = 20;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";

$trnsid = $_POST["trnsid"];
//echo count($trnsid);
//echo $trnsid[0];
$ttrans = $_POST["totTrans"];

/**
 *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements 
 *  This condition executes when admin manager or branch manager logs in,
 *  keeping the check of showing the logged in users only the transactions of 
 *  their associated senders.
 */
if( defined("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS") && strstr(CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS,$agentType) )
{
   /**
	*  @var $extraCondition type string
	*  This varibale is used to define a condition 
	*/
	$extraCondition  = " and ( am.userID = '".$userID."' ) ";
}

$acManagerFlagLabel = "Account Manager";
$acManagerFlag = false;
if(CONFIG_POPULATE_USERS_DROPDOWN=="1" && defined("CONFIG_ACCOUNT_MANAGER_COLUMN") && CONFIG_ACCOUNT_MANAGER_COLUMN!="0"){
	$custExtraFields = ", c.parentID as custParentID, am.name as accountManagerName ";
	$acManagerFlagLabel = CONFIG_ACCOUNT_MANAGER_COLUMN;
	$extraJoin = " LEFT JOIN ".TBL_ADMIN_USERS." as am ON am.userID = c.parentID  ";
	$acManagerFlag = true;
}

if($_POST["btnAction"] != "")
{
	for ($i=0;$i< $ttrans;$i++)
	{
		
		if(count($trnsid) >= 1)
		{
			
			if($_POST["btnAction"] != "")
			{
				 $contentTrans = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$trnsid[$i]."'");
				 $custID 	= $contentTrans["customerID"];
				 $benID 		= $contentTrans["benID"];
				 $custAgentID = $contentTrans["custAgentID"];
				 $benAgentID = $contentTrans["benAgentID"];
				 $imReferenceNumber = $contentTrans["refNumberIM"];
				 $collectionPointID = $contentTrans["collectionPointID"];
				 $transType  = $contentTrans["transType"];
				 $transDate   = $contentTrans["transDate"];
				 $transDate = date("F j, Y", strtotime("$transDate"));
			
				$fromName = SUPPORT_NAME;
				$fromEmail = SUPPORT_EMAIL;
				// getting Admin Email address
				$adminContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where username= 'admin'");
				$adminEmail = $adminContents["email"];
				// Getting customer's Agent Email
				if(CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED)
				{
				$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$custAgentID'");
				$custAgentEmail	= $agentContents["email"];
				}
				// getting BEneficiray agent Email
				if(CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED)
				{
				$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$benAgentID'");
				$benAgentEmail	= $agentContents["email"];
				}
				// getting beneficiary email
				$benContents = selectFrom("select email from ".TBL_BENEFICIARY." where benID= '$benID'");
				$benEmail = $benContents["email"];
				// Getting Customer Email
				$custContents = selectFrom("select email from " . TBL_CUSTOMER . " where customerID= '$custID'");
				$custEmail = $custContents["email"];

if($_POST["btnAction"] == "Authorize")
{

if($contentTrans["createdBy"] == 'CUSTOMER')
{
			$subject = "Status Updated";

			$custContents = selectFrom("select * from cm_customer where c_id= '". $custID ."'");
			$custEmail = $custContents["c_name"];
			$custTitle = $custContents["Title"];
			$custFirstName = $custContents["FirstName"];
			$custMiddleName = $custContents["MiddleName"];
			$custLastName = $custContents["LastName"];
			$custCountry = $custContents["c_country"];
			$custCity = $custContents["c_city"];
			$custZip = $custContents["c_zip"];
			$custLoginName = $custContents["username"];
			$custEmail = $custContents["c_email"];
			$custPhone = $custContents["c_phone"];


			$benContents = selectFrom("select * from cm_beneficiary where benID= '". $benID ."'");
			$benTitle = $benContents["Title"];
			$benFirstName = $benContents["firstName"];
			$benMiddleName = $benContents["middleName"];
			$benLastName = $benContents["lastName"];
			$benAddress  = $benContents["Address"];
			$benCountry = $benContents["Country"];
			$benCity = $benContents["City"];
			$benZip = $benContents["Zip"];
			$benLoginName = $benContents["username"];
			$benEmail = $benContents["email"];
			$benPhone = $benContents["Phone"];


			$AdmindContents = selectFrom("select * from admin where userID = '". $benAgentID ."'");
			$AdmindLoginName = $AdmindContents["username"];
			$AdmindName = $AdmindContents["name"];
			$AdmindEmail = $AdmindContents["email"];




			$Status = $_POST["btnAction"];

/***********************************************************/
$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td colspan='2'><p><strong>Subject</strong>: Your transaction is updated </p></td>
  </tr>
			
			
					  <tr>
						<td colspan='2'><p><strong>Dear</strong>  $custTitle&nbsp;$custFirstName&nbsp;$custLastName  </p></td>
					  </tr>				
			
								  <tr>
						<td width='205'>&nbsp;</td>
						<td width='295'>&nbsp;</td>
					  </tr>
							  <tr>
						<td colspan='2'><p>Thanks for choosing $company as your money transfer company. We will keep you informed through email regarding your transaction till the money reached to your beneficiary. Please see the attached your transaction detail and some important information. </p></td>
					  </tr>			
			
			
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td><p><strong>Transaction Detail: </strong></p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td colspan='2'>-----------------------------------------------------------------------------------</td>
					  </tr>
						
			
					  <tr>
						<td> Transaction Type:  ".$transType." </td>
						<td> Status: $Status </td>
					  </tr>
					  <tr>
						<td> Transaction No:   $imReferenceNumber </td>
						<td> Transaction Date:  ".$transDate."  </td>
					  </tr>
					  <tr>
						<td><p>Authorised Date: $transDate</p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td colspan='2'>-----------------------------------------------------------------------------------</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					
					
					  <tr>
						<td><p><strong>Beneficiary Detail: </strong></p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td colspan='2'>-----------------------------------------------------------------------------------</td>
					  </tr>
					  <tr>
						<td><p>Beneficiary Name:  $benTitle&nbsp;$benFirstName&nbsp;$benLastName</p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td> Address:  $benAddress </td>
						<td> Postal / Zip Code:  $benZip </td>
					  </tr>
					  <tr>
						<td> Country:   $benCountry   </td>
						<td> Phone:   $benPhone   </td>
					  </tr>
					  <tr>
						<td><p>Email:  $benEmail   </p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>";			
			if(trim($transType) == 'Bank Transfer')
			{
			$strQuery = "SELECT * FROM cm_bankdetails where benID= '". $benID ."'";
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
			$rstRow = mysql_fetch_array($nResult);
			
			$_SESSION["bankName"] = $rstRow["bankName"];
			$_SESSION["branchCode"] = $rstRow["branchCode"];
			$_SESSION["branchAddress"] = $rstRow["branchAddress"];
			$_SESSION["swiftCode"] = $rstRow["swiftCode"];
			$_SESSION["accNo"] = $rstRow["accNo"];
			
					$message.="  <tr>
						<td><p><strong>Beneficiary Bank Details </strong></p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td> Bank Name:  ".$_SESSION['bankName']."  </td>
						<td> Acc Number:  ".$_SESSION['accNo']."  </td>
					  </tr>
					  <tr>
						<td> Branch Code:  ".$_SESSION['branchCode']."  </td>
						<td> Branch Address:  ".$_SESSION['branchAddress']."  </td>
					  </tr>
					  <tr>
						<td><p>Swift Code:  ".$_SESSION['swiftCode']."  </p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					";
			$_SESSION["bankName"] = "";
			$_SESSION["branchCode"] = "";
			$_SESSION["branchAddress"] = "";
			$_SESSION["swiftCode"] = "";
			$_SESSION["accNo"] = "";
			}
			elseif(trim($transType) == "Pick up")
			{
			
			$cContents = selectFrom("select * from cm_collection_point where cp_id  = '". $collectionPointID ."'");
			$agentnamee = $cContents["cp_corresspondent_name"];
			$contactperson = $cContents["cp_corresspondent_name"];
			$company = $cContents["cp_branch_name"];
			$address = $cContents["cp_branch_address"];
			$country = $cContents["cp_country"];
			$city = $cContents["cp_city"];
			$phone = $cContents["cp_phone"];
			$tran_date = date("Y-m-d");
			$tran_date = date("F j, Y", strtotime("$tran_date"));
			
					$message .= "
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td><p><strong>Collestin Point Details </strong></p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td> Agent Name : $agentname </td>
						<td> Contact Person:  $contactperson </td>
					  </tr>
					  <tr>
						<td> Company:  $company </td>
						<td> Address:  $address </td>
					  </tr>
					  <tr>
						<td>Country:   $country</td>
						<td>City:  $city</td>
					  </tr>
					  <tr>
						<td>Phone:  $phone</td>
						<td>&nbsp;</td>
					  </tr> ";
}

$exchangerate = getExchangeRate($custCountry, $benCountry);

$exID 			= $exchangerate[0];
$exRate 		= $exchangerate[1];
$currencyFrom 	= $exchangerate[2];
$currencyTo 	= $exchangerate[3];

///Exchange rate used
$exRate 		= $contentTrans["exchangeRate"];

$amount = $contentTrans["transAmount"];
$fee = $contentTrans["IMFee"];
$localamount =($amount * $exRate);
$purpose = $contentTrans["transactionPurpose"];
$totalamount =  $amount + $fee;
$moneypaid = $contentTrans["moneyPaid"];
if(CONFIG_CASH_PAID_CHARGES_ENABLED && $moneypaid=='By Cash')
{
	$cashCharges = $contentTrans["cashCharges"];
	$totalamount =  $totalamount +$cashCharges;
	}

if($amount > 500)
{
	$tempAmount = ($amount - 500);
	$nExtraCharges =  (($tempAmount * 0.50)/100);
	$charges = ($fee + $amount + $nExtraCharges);
}


if(CONFIG_CASH_PAID_CHARGES_ENABLED && $moneypaid=='By Cash')
{
	$cashCharges = $contentTrans["cashCharges"];
	$totalamount =  $totalamount +$cashCharges;
	}
	
if($trnsid[$i] != "")
{
 update("update transactions set IMFee = '$fee',
												 totalAmount  = '$totalamount',
												 localAmount = '$localamount',
												 exchangeRate  = '$exRate'
													 where
													 transID  = '". $trnsid[$i] ."'");
}

$message .="
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Amount Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Exchange Rate:  ".$exRate."</td>
    <td> Amount:  ".$amount." </td>
  </tr>
  <tr>
    <td> ".$systemPre." Fee:  ".$fee." </td>
    <td> Local Amount:  ".$localamount." </td>
  </tr>  <tr>
    <td> Transaction Purpose:  ".$purpose." </td>
    <td> Total Amount:  ".$totalamount." </td>
  </tr>  <tr>
    <td> Money Paid:  ".$moneypaid." </td>
    <td> Bank Charges:  ".$nExtraCharges." </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
";

}
else
{

			$subject = "Status Updated";

			$custContents = selectFrom("select * from " . TBL_CUSTOMER . " where customerID = '". $custID ."'");
			$custTitle = $custContents["Title"];
			$custFirstName = $custContents["firstName"];
			$custMiddleName = $custContents["middleName"];
			$custLastName = $custContents["lastName"];
			$custCountry = $custContents["Country"];
			$custCity = $custContents["City"];
			$custZip = $custContents["Zip"];
			$custEmail = $custContents["email"];
			$custPhone = $custContents["Phone"];


			$benContents = selectFrom("select * from ".TBL_BENEFICIARY." where benID= '". $benID ."'");
			$benTitle = $benContents["Title"];
			$benFirstName = $benContents["firstName"];
			$benMiddleName = $benContents["middleName"];
			$benLastName = $benContents["lastName"];
			$benAddress  = $benContents["Address"];
			$benCountry = $benContents["Country"];
			$benCity = $benContents["City"];
			$benZip = $benContents["Zip"];
			$benLoginName = $benContents["username"];
			$benEmail = $benContents["email"];
			$benPhone = $benContents["Phone"];


			$AdmindContents = selectFrom("select * from admin where userID = '". $benAgentID ."'");
			$AdmindLoginName = $AdmindContents["username"];
			$AdmindName = $AdmindContents["name"];
			$AdmindEmail = $AdmindContents["email"];




			$Status = $_POST["btnAction"];

/***********************************************************/
$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td colspan='2'><p><strong>Subject</strong>: Your transaction is created </p></td>
  </tr>
  <tr>
    <td colspan='2'><p><strong>Dear</strong>  $custTitle&nbsp;$custFirstName&nbsp;$custLastName  </p></td>
  </tr>
  <tr>
    <td width='205'>&nbsp;</td>
    <td width='295'>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'><p>Thanks for choosing Monex Money as your money transfer company. We will keep you informed through email regarding your transaction till the money reached to your beneficiary. Please see the attached your transaction detail and some important information. </p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Transaction Detail: </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>

  <tr>
    <td> Transaction Type:  ".$transType." </td>
    <td> Status: $Status </td>
  </tr>
  <tr>
    <td> Transaction No:   $imReferenceNumber </td>
    <td> Transaction Date:  ".$transDate."  </td>
  </tr>
  <tr>
    <td><p>Authorised Date: $tran_date</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>


  <tr>
    <td><p><strong>Beneficiary Detail: </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td><p>Beneficiary Name:  $benTitle&nbsp;$benFirstName&nbsp;$benLastName</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Address:  $benAddress </td>
    <td> Postal / Zip Code:  $benZip </td>
  </tr>
  <tr>
    <td> Country:   $benCountry   </td>
    <td> Phone:   $benPhone   </td>
  </tr>
  <tr>
    <td><p>Email:  $benEmail   </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>";

if(trim($transType) == 'Bank Transfer')
{

  		$strQuery = "SELECT * FROM cm_bankdetails where benID= '". $benID ."'";
		$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
		$rstRow = mysql_fetch_array($nResult);

		$_SESSION["bankName"] = $rstRow["bankName"];
		$_SESSION["branchCode"] = $rstRow["branchCode"];
		$_SESSION["branchAddress"] = $rstRow["branchAddress"];
		$_SESSION["swiftCode"] = $rstRow["swiftCode"];
		$_SESSION["accNo"] = $rstRow["accNo"];

$message.="  <tr>
    <td><p><strong>Beneficiary Bank Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Bank Name:  ".$_SESSION['bankName']."  </td>
    <td> Acc Number:  ".$_SESSION['accNo']."  </td>
  </tr>
  <tr>
    <td> Branch Code:  ".$_SESSION['branchCode']."  </td>
    <td> Branch Address:  ".$_SESSION['branchAddress']."  </td>
  </tr>
  <tr>
    <td><p>Swift Code:  ".$_SESSION['swiftCode']."  </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
";
		$_SESSION["bankName"] = "";
		$_SESSION["branchCode"] = "";
		$_SESSION["branchAddress"] = "";
		$_SESSION["swiftCode"] = "";
		$_SESSION["accNo"] = "";
}
elseif(trim($transType) == "Pick up")
{

$cContents = selectFrom("select * from cm_collection_point where cp_id  = '". $collectionPointID ."'");
$agentnamee = $cContents["cp_corresspondent_name"];
$contactperson = $cContents["cp_corresspondent_name"];
$company = $cContents["cp_branch_name"];
$address = $cContents["cp_branch_address"];
$country = $cContents["cp_country"];
$city = $cContents["cp_city"];
$phone = $cContents["cp_phone"];
$tran_date = date("Y-m-d");
$tran_date = date("F j, Y", strtotime("$tran_date"));

$message .= "
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Collestin Point Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Agent Name : $agentname </td>
    <td> Contact Person:  $contactperson </td>
  </tr>
  <tr>
    <td> Company:  $company </td>
    <td> Address:  $address </td>
  </tr>
  <tr>
    <td>Country:   $country</td>
    <td>City:  $city</td>
  </tr>
  <tr>
    <td>Phone:  $phone</td>
    <td>&nbsp;</td>
  </tr> ";

}

$exchangerate = getExchangeRate($custCountry, $benCountry);

$exID 			= $exchangerate[0];
$exRate 		= $exchangerate[1];
$currencyFrom 	= $exchangerate[2];
$currencyTo 	= $exchangerate[3];

///Exchange rate used 
$exRate = $contentTrans["exchangeRate"];

$amount = $contentTrans["transAmount"];
$fee = $contentTrans["IMFee"];
$localamount =($amount * $exRate);
$purpose = $contentTrans["transactionPurpose"];
$totalamount =  $amount + $fee;
$moneypaid = $contentTrans["moneyPaid"];
if(CONFIG_CASH_PAID_CHARGES_ENABLED && $moneypaid=='By Cash')
{
	$cashCharges = $contentTrans["cashCharges"];
	$totalamount =  $totalamount +$cashCharges;
	}


if($amount > 500)
{
	$tempAmount = ($amount - 500);
	$nExtraCharges =  (($tempAmount * 0.50)/100);
	$charges = ($fee + $amount + $nExtraCharges);
}
if($trnsid[$i] != "")
{
 update("update transactions set IMFee = '$fee',
												 totalAmount  = '$totalamount',
												 localAmount = '$localamount',
												 exchangeRate  = '$exRate'
													 where
													 transID  = '". $trnsid[$i] ."'");
}
$message .="
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Amount Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Exchange Rate:  ".$exRate."</td>
    <td> Amount:  ".$amount." </td>
  </tr>
  <tr>
    <td> ".$systemPre." Fee:  ".$fee." </td>
    <td> Local Amount:  ".$localamount." </td>
  </tr>  <tr>
    <td> Transaction Purpose:  ".$purpose." </td>
    <td> Total Amount:  ".$totalamount." </td>
  </tr>  <tr>
    <td> Money Paid:  ".$moneypaid." </td>
    <td> Bank Charges:  ".$nExtraCharges." </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
";

}
}
/**********************************************************/

				
			}
//#9672: Speedmoney: Need to Stop Auto emails.
if(CONFIG_STOP_WHOLE_EMAIL_SYSTEM_SPEEDMONEY!="1")
	sendMail($custEmail, $subject, $message, $fromName, $fromEmail);

			if($_POST["btnAction"] == "Cancel")
			{
				$tranStat = selectFrom("select transStatus, transID, totalAmount, custAgentID, benAgentID from ". TBL_TRANSACTIONS." where transID='". $trnsid[$i] ."'");
				$stat = $tranStat["transStatus"];
				$id=$tranStat["transID"];
				if($stat == 'Pending' || $stat == 'Processing')
					{
					$balance = $tranStat["totalAmount"];
					$agent = $tranStat["custAgentID"];
					$userID  = $_SESSION["loggedUserData"]["userID"];
					$today = date("Y-m-d");

					//insertInto("insert into agent_account values('', '$agent', '$today', 'DEPOSIT', '$balance', '$userID', '$id', 'Transaction Cancelled')");
						confirmCancelled($id,$username);
					}
				else
				{
					waitCancellation($id,$username);
					}
				
			}

			if($_POST["btnAction"] == "Recall")
			{
				update("update ". TBL_TRANSACTIONS." set transStatus = 'recalled', recalledBy = '$username', recalledDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i] ."'");
				insertError("Transaction recalled successfully.");
				//echo "HERE";
			}
			if($_POST["btnAction"] == "Verify")
			{
				update("update ". TBL_TRANSACTIONS." set transStatus = 'Processing', verifiedBy='$username' where transID='". $trnsid[$i] ."'");
				insertError("Transaction Verified successfully.");
				//echo "HERE";
			}
			if($_POST["btnAction"] == "Hold" || $_POST["btnAction"] == "hold")
			{
				
				update("update ". TBL_TRANSACTIONS." set transStatus = 'Hold', holdedBy='$username' where transID='". $trnsid[$i] ."'");
				insertError("Transaction Holded successfully.");
				//echo "HERE";
			}
			if($_POST["btnAction"] == "Unhold")
			{
				update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy='$username' where transID='". $trnsid[$i] ."'");
				
				/*$today = date("Y-m-d");;  
				$insertQuery = "insert into bank_account values('','".$contentTrans["benAgentID"]."', '$today', 'WITHDRAW', '".$contentTrans["totalAmount"]."', '$username', '". $trnsid[$i] ."','Transaction Unholded')";
				$q=mysql_query($insertQuery);*/
				
				$today = date("Y-m-d");  
				if($contentTrans["createdBy"] != 'CUSTOMER')
				{
						$typeAgent = selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." where userID = '".$contentTrans["custAgentID"]."'");
						$type = $typeAgent["agentType"];
						if($type=="Sub")
						{
							$agent=$typeAgent["parentID"];
						}else{
							$agent=$contentTrans["custAgentID"];
						}
						$insertQuery = "insert into agent_account values('','$agent', '$today', 'WITHDRAW', '".$contentTrans["totalAmount"]."', '$userID', '". $trnsid[$i] ."', 'Transaction Unholded')";
						$q=mysql_query($insertQuery);
					}
				insertInto("insert into bank_account(aaID, bankID, dated, type, amount, currency, modified_by, TransID, description) values('', '".$contentTrans["benAgentID"]."', '$today', 'WITHDRAW', '".(DEST_CURR_IN_ACC_STMNTS == "1" ? $contentTrans["localAmount"] : $contentTrans["transAmount"])."', '".(DEST_CURR_IN_ACC_STMNTS == "1" ? $contentTrans["currencyTo"] : $contentTrans["currencyFrom"])."', '$userID', '". $trnsid[$i] ."', 'Transaction Unholded')");
				
				
				
				insertError("Transaction Unholded successfully.");
				//echo "HERE";
			}

			if($_POST["btnAction"] == "Authorize")
			{
				// Update agents account limit;
				update("update " . TBL_ADMIN_USERS . " set limitUsed  = (limitUsed  + '".$contentTrans["totalAmount"]."') where userID = '". $contentTrans["custAgentID"] ."'");
				update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='$username', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i] ."'");				
				
				/*$today = date("Y-m-d");;  
				$insertQuery = "insert into bank_account values('','".$contentTrans["benAgentID"]."', '$today', 'WITHDRAW', '".$contentTrans["totalAmount"]."', '$username', '". $trnsid[$i] ."','Transaction Authorized')";
				$q=mysql_query($insertQuery);*/
				
				$today = date("Y-m-d");  
				if($contentTrans["createdBy"] != 'CUSTOMER')
				{
						$typeAgent = selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." where userID = '".$contentTrans["custAgentID"]."'");
						$type = $typeAgent["agentType"];
						if($type=="Sub")
						{
							$agent=$typeAgent["parentID"];
						}else{
							$agent=$contentTrans["custAgentID"];
						}
						$insertQuery = "insert into agent_account values('','$agent', '$today', 'WITHDRAW', '".$contentTrans["totalAmount"]."', '$userID', '". $trnsid[$i] ."', 'Transaction Authorized')";
						$q=mysql_query($insertQuery);
				}
				insertInto("insert into bank_account(aaID, bankID, dated, type, amount, currency, modified_by, TransID, description) values('', '".$contentTrans["benAgentID"]."', '$today', 'WITHDRAW', '".(DEST_CURR_IN_ACC_STMNTS == "1" ? $contentTrans["localAmount"] : $contentTrans["transAmount"])."', '".(DEST_CURR_IN_ACC_STMNTS == "1" ? $contentTrans["currencyTo"] : $contentTrans["currencyFrom"])."', '$userID', '". $trnsid[$i] ."', 'Transaction Authorized')");
				
				
				insertError("Transaction Authorized successfully.");
				//echo "HERE";
				
				/*$strQuery = "SELECT * FROM admin where username = 'IMBAL'";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
				$rstRow = mysql_fetch_array($nResult);				
				$IMBAL = $rstRow["userID"];
				
				
				$strQuery = "SELECT * FROM admin where username = 'ChequePoint'";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
				$rstRow = mysql_fetch_array($nResult);				
				$ChequePoint = $rstRow["userID"];				
				
				$strQuery = "SELECT * FROM transactions where transID='". $trnsid[$i] ."'";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
				$rstRow = mysql_fetch_array($nResult);				
				$benAgentID  = $rstRow["benAgentID"];
				if($benAgentID == $ChequePoint || $benAgentID == $IMBAL)
				{
					$transID = $trnsid[$i];
					include "imbal-swift-file.php";
				}*/
				
			}

		}
	}
redirect("manage-transactions.php?msg=Y&action=". $_GET["action"]);
}
$query = "select * from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1".$extraCondition;
$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1".$extraCondition;

if($_POST["Submit"] =="Search")
{

	
	if($_POST["transType"]!="")
		$_SESSION["transType"] = $_POST["transType"];
	else 
		$_SESSION["transType"]="";
	if($_POST["transStatus"]!="")
		$_SESSION["transStatus"] = $_POST["transStatus"];
	else
		$_SESSION["transStatus"]="";
	
	if($_POST["transID"] != "")
	{
		//$query = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c, ". TBL_BENEFICIARY ." as b, ". TBL_CM_CUSTOMER ." as cm, ". TBL_CM_BENEFICIARY ." as cb where ((t.customerID = c.customerID and t.benID = b.benID) OR (t.customerID = cm.c_id and t.benID = cb.benID))";
		//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c, ". TBL_BENEFICIARY ." as b, ". TBL_CM_CUSTOMER ." as cm, ". TBL_CM_BENEFICIARY ." as cb where ((t.customerID = c.customerID and t.benID = b.benID) OR (t.customerID = cm.c_id and t.benID = cb.benID and cm.c_id = cb.customerID))";
$query = "select * from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".$extraJoin." where 1 ".$extraCondition;
$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".$extraJoin." where 1 ".$extraCondition;
			
$queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm, ". TBL_CM_BENEFICIARY ." as cb where t.customerID =  cm.c_id and t.benID =  cb.benID ";
$queryonlineCnt = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm, ". TBL_CM_BENEFICIARY ." as cb where t.customerID =  cm.c_id and t.benID =  cb.benID ";
							
		switch($_POST["searchBy"])
		{
			case 0:
			{		
				$query .= " and (t.transID='".$_POST["transID"]."' OR t.refNumber = '".$_POST["transID"]."' OR t.refNumberIM = '".$_POST["transID"]."') ";
				$queryCnt .= " and (t.transID='".$_POST["transID"]."' OR t.refNumber = '".$_POST["transID"]."' OR t.refNumberIM = '".$_POST["transID"]."') ";
				break;
			}
			case 1:
			{
				
				$queryonline .= " and (cm.FirstName like '".$_POST["transID"]."%' OR cm.LastName like '".$_POST["transID"]."%')";
				$queryonlineCnt .= " and (cm.FirstName like '".$_POST["transID"]."%' OR cm.LastName like '".$_POST["transID"]."%')";
								
				
				$query .= "  and (c.firstName like '".$_POST["transID"]."%' OR c.lastName like '".$_POST["transID"]."%')";
				$queryCnt .= "  and (c.firstName like '".$_POST["transID"]."%' OR c.lastName like '".$_POST["transID"]."%')";
				break;
			}
			case 2:
			{
				$queryonline .= " and (cb.firstName like '".$_POST["transID"]."%' OR cb.lastName like '".$_POST["transID"]."%')";		
				$queryonlineCnt .= " and (cb.firstName like '".$_POST["transID"]."%' OR cb.lastName like '".$_POST["transID"]."%')";
				
				
				$query .= " and (b.firstName like '".$_POST["transID"]."%' OR b.lastName like '".$_POST["transID"]."%')";
				$queryCnt .=" and (b.firstName like '".$_POST["transID"]."%' OR b.lastName like '".$_POST["transID"]."%')";
				break;
			}
		}
//		$query .= " and (t.transID='".$_POST["transID"]."' OR t.refNumber = '".$_POST["transID"]."' OR  t.refNumberIM = '".$_POST["transID"]."') ";
	//	$queryCnt .= " and (t.transID='".$_POST["transID"]."' OR t.refNumber = '".$_POST["transID"]."' OR  t.refNumberIM = '".$_POST["transID"]."') ";
			}
}
	if($_SESSION["transType"] != "")
	{
		$query .= " and (t.transType='".$_SESSION["transType"]."')";
		$queryCnt .= " and (t.transType='".$_SESSION["transType"]."')";
		if($_POST["transID"] != "")
		{
			$queryonline .= " and (t.transType='".$_SESSION["transType"]."')";
		  $queryonlineCnt .= " and (t.transType='".$_SESSION["transType"]."')";
			}
		
	}
	if($_SESSION["transStatus"] != "")
	{
		if($_GET["action"]!="")
		{
		$_SESSION["transStatus"]="";
		}
		else{
		$query .= " and (t.transStatus='".$_SESSION["transStatus"]."')";
		$queryCnt .= " and (t.transStatus='".$_SESSION["transStatus"]."')";
		
		if($_POST["transID"] != "")
		{
			$queryonline .= " and (t.transStatus='".$_SESSION["transStatus"]."')";
		  $queryonlineCnt .= " and (t.transStatus='".$_SESSION["transStatus"]."')";
			}
		}
	}

if($_GET["action"] == "Cancel" || $_GET["action"] == "cancel")
{
	/*if($agentType!='admin')
	{
		$query .= " and t.transStatus ='Pending' and t.transDate > date_sub('".getCountryTime(CONFIG_COUNTRY_CODE)."', interval 1 hour) ";
		$queryCnt .= " and t.transStatus ='Pending' and t.transDate > date_sub('".getCountryTime(CONFIG_COUNTRY_CODE)."', interval 1 hour) ";
	}
	else
		{*/
		$query .= " and (t.transStatus ='Pending' or t.transStatus ='Authorize' or t.transStatus ='Processing' or t.transStatus ='Amended')";
		$queryCnt .= " and (t.transStatus ='Pending' or t.transStatus ='Authorize' or t.transStatus ='Processing' or t.transStatus ='Amended')";
		//}
}

if($_GET["action"] == "Recall")
{	//and transStatus !='Picked up' and transStatus !='Credited'
	//$query .= " and (t.transStatus = 'Pending' OR t.transStatus = 'authorize' OR t.transStatus = 'Processing' OR t.transStatus ='Cancelled') ";
	//$queryCnt .= " and (t.transStatus = 'Pending' OR t.transStatus = 'authorize' OR t.transStatus = 'Processing' OR t.transStatus ='Cancelled') ";///Changed on 14 Dec.
	/*if($agentType!='admin')
	{
		$query .= " and (t.transStatus !='Picked up' and t.transStatus !='Credited' and transDate > date_sub('".getCountryTime(CONFIG_COUNTRY_CODE)."', interval 1 hour)) ";
		$queryCnt .= " and (t.transStatus !='Picked up' and t.transStatus !='Credited' and transDate > date_sub('".getCountryTime(CONFIG_COUNTRY_CODE)."', interval 1 hour)) ";
	}else{*/
	$query .= " and (t.transStatus !='Picked up' and t.transStatus !='Credited') ";
	$queryCnt .= " and (t.transStatus !='Picked up' and t.transStatus !='Credited') ";
	//} 
	
}

if($_GET["action"] == "Verify")
{
	$query .= " and (t.transStatus ='Pending') ";
	$queryCnt .= " and (t.transStatus ='Pending') ";
}

if($_GET["action"] == "hold")
{
	if(CONFIG_VERIFY_TRANSACTION_ENABLED)
	{
		$query .= " and (t.transStatus = 'Processing') ";
		$queryCnt .= " and (t.transStatus = 'Processing') ";
		}else{	
		$query .= " and (t.transStatus ='Pending') ";
		$queryCnt .= " and (t.transStatus ='Pending') ";
	}
	
}

if($_GET["action"] == "unhold")
{
	$query .= " and (t.transStatus = 'Hold') ";
	$queryCnt .= " and (t.transStatus = 'Hold') ";
}

if($_GET["action"] == "authorize")
{
	if(CONFIG_VERIFY_TRANSACTION_ENABLED)
	{
		$query .= " and (t.transStatus = 'Processing') ";
		$queryCnt .= " and (t.transStatus = 'Processing') ";
	}else{	
		$query .= " and (t.transStatus ='Pending') ";
		$queryCnt .= " and (t.transStatus ='Pending') ";
	}
}

switch ($agentType)
{
	case "SUPA":
	case "SUPAI":
	case "SUBA":
	case "SUBAI":
		$query .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."')";
		$queryCnt .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."')";
	if($_POST["transID"] != "")
		{	
		$queryonline .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."')";
		$queryonlineCnt .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."')";
	}
}
$query .= " order by t.transDate DESC";
//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS."";
 $query .= " LIMIT $offset , $limit";
$contentsTrans = selectMultiRecords($query);
$allCount = countRecords($queryCnt );
		/*if($_POST["transID"] != "")
		{	
			$queryonline .= " order by t.transDate DESC";
			//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS."";
		echo	$queryonline .= " LIMIT $offset , $limit";
			$onlinecustomer = selectMultiRecords($queryonline);
			$onlinecustomerCount = countRecords($queryonlineCnt );
			echo $allCount = $allCount + $onlinecustomerCount;
		}*/
//debug($query);		
?>
<html>
<head>
	<title>Add Promotional Code</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2"><? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?> 
      Transactions</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="manage-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
            <td nowrap> Search Transactions 
              <input name="transID" type="text" id="transID">
		  <select name="transType" style="font-family:verdana; font-size: 11px; width:100">
          <option value=""> - Type - </option>
          <option value="Pick up">Pick up</option>
          <option value="Bank Transfer">Bank Transfer</option>
          <option value="Home Delivery">Home Delivery</option>
        </select><script language="JavaScript">SelectOption(document.forms[0].transType, "<?=$_SESSION["transType"]; ?>");</script>
		<select name="transStatus" style="font-family:verdana; font-size: 11px; width:100">
		  <option value=""> - Status - </option>

		  <option value="Pending">Pending</option>
		  <option value="Processing">Processing</option>
		  <option value="Recalled">Recalled</option>
		  <option value="Rejected">Rejected</option>
		  <option value="Cancelled">Cancelled</option>
		  <option value="Cancelled">Awaiting Cancellation</option>		  
		  <option value="Authorize">Authorized</option>
		  <option value="Failed">Undelivered</option>
		  <option value="Delivered">Delivered</option>
		  <option value="Out for Delivery">Out for Delivery</option>
		  <option value="Picked up">Picked up</option>
		  <option value="Credited">Credited</option>
        </select>
		<script language="JavaScript">SelectOption(document.forms[0].transStatus, "<?=$_SESSION["transStatus"]; ?>");</script>
		<select name="searchBy" >
			<option value=""> - Search By - </option>
			<option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
			<option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By Sender Name</option>
			<option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].searchBy, "<?=$_SESSION["searchBy"]; ?>");</script>
		<input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
      <form action="manage-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">


          <?
			if ($allCount > 0){
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo (count($contentsTrans) + count($onlinecustomer))?> records to <? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?>.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0 || $onlinecustomerCount > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?></span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			  <td><span class="style1">Sender Name </span></td>
			  <td><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1">Created By </span></td>
			  <td width="146" align="center"><? if ($_GET["action"] != ""&&$_GET["action"] != "Recall") { ?><input name="All" type="checkbox" id="All" onClick="CheckAll();"><? } ?></td>
		      <td width="74" align="center">&nbsp;</td>
			  <td width="74" align="center">&nbsp;</td>
			</tr>
		    <? for($i=0;$i<count($contentsTrans);$i++)
			{
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["totalAmount"] .  " " . $contentsTrans[$i]["currencyFrom"]?></td>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>
					
				  <?
				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>


				  <td align="center" bgcolor="#FFFFFF"><? if ($_GET["action"] != "" && $_GET["action"] != "Recall") {  $tempTransId = $contentsTrans[$i]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId'>";?><? } ?></td>
				  <? if($_GET["action"] != "Recall")
					  {
					  ?>
					  <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $contentsTrans[$i]["transID"]?>" class="style2">Enguiry</a></td>
					  <?
					  }
					  else 
					  {
						  ?>
						  <td align="center" bgcolor="#FFFFFF"><a href="add-transaction.php?transID=<? echo $contentsTrans[$i]["transID"]?>" class="style2">Update</a></td>
						  <?
					  }
					  ?>
				</tr>
				<?
			}
			?>
			
			<? /*for($i=0;$i< $onlinecustomerCount;$i++)
			{
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $onlinecustomer[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["totalAmount"] .  " " . $onlinecustomer[$i]["currencyFrom"]?></td>
				  <? if($onlinecustomer[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$onlinecustomer[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$onlinecustomer[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$onlinecustomer[$i]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$onlinecustomer[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>
					
				  <?
				  if($onlinecustomer[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$onlinecustomer[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>


				  <td align="center" bgcolor="#FFFFFF"><? if ($_GET["action"] != ""&&$_GET["action"] != "Recall") {  $tempTransId = $onlinecustomer[$i]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId'>";?><? } ?></td>
				  <? if($_GET["action"] != "Recall")
					  {
					  ?>
					  <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $onlinecustomer[$i]["transID"]?>" class="style2">Enguiry</a></td>
					  <?
					  }
					  else 
					  {
						  ?>
						  <td align="center" bgcolor="#FFFFFF"><a href="add-transaction.php?transID=<? echo $onlinecustomer[$i]["transID"]?>" class="style2">Update</a></td>
						  <?
					  }
					  ?>
				</tr>
				<?
			}*/
			?>
			
			<tr bgcolor="#FFFFFF">
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td bgcolor="#FFFFFF">&nbsp;</td>
			  <td width="100" align="center">&nbsp;</td>
			  <td align="center"><input type='hidden' name='totTrans' value='<?echo $i?>'>
			    <? if ($_GET["action"] != "") { ?><input name="btnAction" type="submit"  value="<? echo ucfirst($_GET["action"])?>"><? } ?></td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			</tr>
			<?
			} // greater than zero
			?>
          </table></td>
        </tr>


          <tr>
            <td  bgcolor="#000000"> <table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if (count($contentsTrans) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>

          <?
			} else {
		?>
          <tr>
            <td  align="center"> No Transaction found in the database.
            </td>
          </tr>
          <?
			}
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>
