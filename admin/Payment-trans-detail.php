<?php

	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	
	$customerID 	=  	$_GET["TransIDS"];
	$custAgentID 	= 	$_GET["custAgentID"];
	$fromDate 		=  	$_GET["fromDate"];
	$toDate 		=  	$_GET["toDate"];
	$currency		=	$_GET["currency"];
	$amount			=	$_GET["amount"];
	$amountType		=	$_GET["amountType"];
	$amountCriteria	=	$_GET["amountCriteria"];
	
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	
	if ($offset == "")
	{
		$offset = 0;
	}

	$limit=200;
	
	if ($_GET["newOffset"] != "") 
	{
		$offset = $_GET["newOffset"];
	}
	
	if($offset == 0)
	{
		$_SESSION["dgrandTotal"]="";
		$_SESSION["dCurrentTotal"]="";	
		
		$_SESSION["dgrandTotal2"]="";
		$_SESSION["dCurrentTotal2"]="";		
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	$sortBy = $_GET["sortBy"];
	
	if ($sortBy == "")
		$sortBy = " transDate";

	

	$strQuery = "SELECT transType, transID, transDate, refNumberIM, custAgentID, customerID, createdBy, benID,transAmount, currencyFrom, totalAmount, transStatus 
				FROM ".TBL_TRANSACTIONS." as t 
					WHERE t.transID IN (".$customerID.")
						AND (t.transStatus !='Cancelled') ";

	$strQueryCnt = "SELECT count(transID) FROM " . TBL_TRANSACTIONS . " as t 
					WHERE t.transID IN (".$customerID.")
						AND (t.transStatus !='Cancelled') ";   	

			
	
	$strstrQuery .= " Order By transDate LIMIT $offset , $limit";                      
	$contentsTrans = selectMultiRecords($strQuery);
	$allCount = countRecords($strQueryCnt);
	//debug($contentsTrans);


?>            
<html>        
<head>
<title>View Transaction Details</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="./styles/admin.js"></script>

<style type="text/css">
	.style2 
	{
		color: #6699CC;
		font-weight: bold;
	}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>

<body>
	<table width="100%" border="0" cellspacing="1" cellpadding="5" >
  		<tr> 
    		<td class="topbar"><b><font color="#000000" size="2">View transaction details.</font></b></td>
		</tr>
 		<?php 
		if ($allCount > 0)
		{
			?>
			<tr>
				<td colspan="11" align="center">
					<form action="view-detailed-agent-transaction.php" method="post" name="trans">
					<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
          				<tr>
				            <td width="477">
              					<?php 
								if (count($contentsTrans) > 0) 
								{	?>
									Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans));?></b>
									of
									<?=$allCount; ?>
									<?php } ;?>
									</td>
									<?php if ($prv >= 0) { ?>
									<td width="50"> <a href="<?php print $PHP_SELF ."?customerID=" . $customerID . "&newOffset=0&sortBy=".$_GET["sortBy"]."&total=first&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo&amount=$amount&amountCriteria=$amountCriteria&amountType=$amountType&currency=$currency";?>"><font color="#005b90">First</font></a>
									</td>
									<td width="53" align="right"> <a href="<?php print $PHP_SELF ."?customerID=" . $customerID . "&newOffset=$prv&sortBy=".$_GET["sortBy"]."&total=pre&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo&amount=$amount&amountCriteria=$amountCriteria&amountType=$amountType&currency=$currency"?>"><font color="#005b90">Previous</font></a>
									</td>
									<?php } ?>
									<?php
									if ( ($nxt > 0) && ($nxt < $allCount) ) {
									$alloffset = (ceil($allCount / $limit) - 1) * $limit;
									?>
									<td width="50" align="right"> <a href="<?php print $PHP_SELF ."?customerID=" . $customerID . "&newOffset=$nxt&sortBy=".$_GET["sortBy"]."&total=next&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo&amount=$amount&amountCriteria=$amountCriteria&amountType=$amountType&currency=$currency";?>"><font color="#005b90">Next</font></a>&nbsp;
									</td>
									<td width="82" align="right"> <!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&total=last";?>"--><font color="#005b90">&nbsp;</font><!--/a-->&nbsp;
									</td>
									<?php }
								   ?>
				          </tr>
			        </table>		
					</form>  
				</td>
			</tr> 
		<?php
		}
		?>
			<tr> 
			<td align="center">
				<table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#CCCCCC">
					<tr class="style1"> 
						<td>Dated</td>
						<td>Reference Number</td>
						<td>Agent Name</td>
						<td>Sender Name</td>
						<td>Beneficiary Name</td>
						<? if(CONFIG_SHOW_TRANS_AMOUNT=="1") {?>
							<td>Transaction Amount</td>
						<? }?>
						<td>Total Amount</td>
						<td>Transaction Type</td>
						<td>Status</td>
					</tr>
					<?php
					if(count($contentsTrans) > 0)
					{
						$transTotal = 0;
						$total = 0;
						$totalLocal = 0;
						for($i = 0; $i < count($contentsTrans); $i++)
						{      
							// Get Agent Information 
							$getAgentInfo = selectFrom("SELECT userID,	name FROM ".TBL_ADMIN_USERS." 
															WHERE userID='".$contentsTrans[$i]["custAgentID"]."'"); 
	
							// Get Coustomer Information 
							if($contentsTrans[$i]["createdBy"] != 'CUSTOMER')
								$strCTableName = TBL_CUSTOMER;
							elseif($contentsTrans[$i]["createdBy"] == 'CUSTOMER')
								$strCTableName = TBL_CM_CUSTOMER;
								
							$getCustInfo = selectFrom("SELECT Title, firstName,	middleName,	lastName 	
															FROM ".$strCTableName." WHERE customerID='".$contentsTrans[$i]["customerID"]."'");
	
							// Get Benif Information 
							if($contentsTrans[$i]["createdBy"] != 'CUSTOMER')
								$strBTableName = TBL_BENEFICIARY;
							elseif($contentsTrans[$i]["createdBy"] == 'CUSTOMER')
								$strBTableName = TBL_CM_BENEFICIARY;
	
							$getBenfInfo = selectFrom("SELECT Title,firstName,middleName,lastName
															FROM ".$strBTableName." WHERE benID='".$contentsTrans[$i]["benID"]."'"); 
															
							$fontColor = '';
						//	if(is_first_sender_transaction($contentsTrans[$i]["transID"],$contentsTrans[$i]["customerID"]))
							//	$fontColor = "#00CC00";								
							?>
							<tr> 
								<td style="color:<?=$fontColor?>"><?php echo dateFormat($contentsTrans[$i]["transDate"],"2"); ?></td>
								<td><?php echo $contentsTrans[$i]["refNumberIM"]; ?></td>
								<td><?php echo $getAgentInfo["name"];?></td>
								<td><?php echo $getCustInfo["firstName"]." ".$getCustInfo["middleName"]." ".$getCustInfo["lastName"];?></td>
								<td><?php echo $getBenfInfo["firstName"]." ".$getBenfInfo["middleName"]." ".$getBenfInfo["lastName"];?></td>
								
								<? if(CONFIG_SHOW_TRANS_AMOUNT=="1") {?>
									<td align="right"><?php echo(number_format($contentsTrans[$i]["transAmount"],2,'.',',')); ?>
									<span style='color:#0000FF'><?php echo $contentsTrans[$i]["currencyFrom"]; ?> </span></td>
								<? }?>
								<td align="right"><?php echo(number_format($contentsTrans[$i]["totalAmount"],2,'.',',')); ?></td>
								<td><?php echo $contentsTrans[$i]["transType"]?></td>
								<td><?php echo $contentsTrans[$i]["transStatus"]; ?></td>
							</tr>
							<?php 
								$transTotal +=$contentsTrans[$i]["transAmount"];
								$total = $total + $contentsTrans[$i]["totalAmount"]; 
						}
					}?>
					<tr>
						<td colspan="11" border="0" bordercolor="#FFFFFF">&nbsp;</td>
					</tr>
					<tr bgcolor="#CCCCCC" class="style1">
						<td colspan="5">Total</td>
						<? if(CONFIG_SHOW_TRANS_AMOUNT=="1") {?>
							<td align="right"><?php  echo number_format($transTotal,2,'.',','); ?></td>	
						<? }?>	
						<td align="right"><?php  echo(number_format($total,2,'.',',')." ".$contentsCurrency["currencyTo"]); ?></td>	
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<?php
						if($_GET["total"]=="first")
						{
							$_SESSION["dgrandTotal"] = $totalLocal;
							$_SESSION["dCurrentTotal"]=$totalLocal;
						}
						elseif($_GET["total"]=="pre")
						{
							$_SESSION["dgrandTotal"] -= $_SESSION["dCurrentTotal"];			
							$_SESSION["dCurrentTotal"]=$totalLocal;
						}
						elseif($_GET["total"]=="next")
						{
							$_SESSION["dgrandTotal"] += $totalLocal;
							$_SESSION["dCurrentTotal"]= $totalLocal;
						}
						elseif($_GET["total"]=="")
						{
							$_SESSION["dgrandTotal"]=$totalLocal;
							$_SESSION["dCurrentTotal"]=$_SESSION["dgrandTotal"];
						}
						
						//For Destination Currency Total
						if($_GET["total"]=="first")
						{
							$_SESSION["dgrandTotal2"] = $total;
							$_SESSION["dCurrentTotal2"]=$total;
						}
						elseif($_GET["total"]=="pre")
						{
							$_SESSION["dgrandTotal2"] -= $_SESSION["dCurrentTotal2"];			
							$_SESSION["dCurrentTotal2"]=$total;
						}
						elseif($_GET["total"]=="next")
						{
							$_SESSION["dgrandTotal2"] += $total;
							$_SESSION["dCurrentTotal2"]= $total;
						}
						elseif($_GET["total"]=="")
						{
							$_SESSION["dgrandTotal2"]=$total;
							$_SESSION["dCurrentTotal2"]=$_SESSION["dgrandTotal2"];
						}
					?>
				<!--<tr>
					<td colspan="5">Total</td>	
					<td><?php  echo(number_format($_SESSION["dgrandTotal2"],2,'.',',')." ".$contentsCurrency["currencyTo"]); ?></td>	
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>-->
			</table>
		</td>
	</tr>
	<tr>
		<td align="center">
			<div align="center">
				<form name="form1" method="post" action="">   
					<div class='noPrint'>               
						<input type="submit" name="Submit2" value="Print This Transaction" onClick="print()">
					</div>
				</form>
			</div>
		</td>
	</tr>
</table>
</body>
</html>