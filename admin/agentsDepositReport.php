<?
	session_start();

	include ("../include/config.php");
	include ("security.php");
	
	/**
	 * Variables from Query String
	 */
	$print				= $_REQUEST["print"];
	$currentDate		= $_REQUEST["currentDate"];
	
	/**
	 * Some pre data preparation.
	 */
	$currentDate		= (!empty($currentDate))?$currentDate:date("Y-m-d", time());

	/**
	 * Check for data validity
	 */
	// nothing till now...
	
	/**
	 * Fetch Agents Outstanding
	 */
	$agentsArray = array();
	
	// Fetch Total Transactions Amount
	$sql = "SELECT
				a.userID, 
				a.username, 
				a.name, 
				sum( ac.amount ) AS TOTAL
			FROM
				agent_account ac, 
				admin a
			WHERE
				ac.type = 'DEPOSIT'
				AND transID=''
				
				AND ac.dated = '".$currentDate."' 
				AND ac.agentID = a.userID
				
			GROUP BY
				ac.agentID
			";
	$result = mysql_query($sql) or die(mysql_error());
	
	while($rs = mysql_fetch_array($result))
	{
		$agentsArray[$rs["userID"]]["userID"] = $rs["userID"];
		$agentsArray[$rs["userID"]]["username"] = $rs["username"];
		$agentsArray[$rs["userID"]]["name"] = $rs["name"];
		$agentsArray[$rs["userID"]]["TOTAL"] = abs($rs["TOTAL"]);
		//$agentsArray[$rs["userID"]]["RECEIVED"] = 0;
		
		
	
		
	
		
	}
	
	// Fetch Received Amount
	/** commented as per instructions by Mr. Hasan
	$sql = "SELECT
				a.userID, 
				a.username, 
				a.name, 
				sum( ac.amount ) AS RECEIVED
			FROM
				agent_account ac, 
				admin a
			WHERE
				ac.type = 'DEPOSIT'
				AND ac.TransID != ''
				AND ac.dated = '".$currentDate."' 
				AND ac.agentID = a.userID
				AND ac.agentID != ".CONFIG_PAYIN_AGENT_NUMBER."
			GROUP BY
				ac.agentID
			";
	$result = mysql_query($sql) or die(mysql_error());
	
	while($rs = mysql_fetch_array($result))
	{
		$agentsArray[$rs["userID"]]["userID"] = $rs["userID"];
		$agentsArray[$rs["userID"]]["username"] = $rs["username"];
		$agentsArray[$rs["userID"]]["name"] = $rs["name"];
		
		if(empty($agentsArray[$rs["userID"]]["TOTAL"]))
		{
			$agentsArray[$rs["userID"]]["TOTAL"] = 0;
		}
		
		$agentsArray[$rs["userID"]]["RECEIVED"] = abs($rs["RECEIVED"]);
	}
	**/
	
	$tmpTotalTransactionAmount = 0;
	//$tmpTotalReceivedAmount = 0;
	$tmpTotalOutstandingAmount = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Agents Outstanding Report</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
    <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="center" class="reportHeader">Agents Outstanding Report <br />
                    <span class="reportSubHeader">
                        <?=date("l, d F, Y", strtotime($currentDate))?>
                        <br />
                        <br />
                    </span></td>
            </tr>
        </table>
                <br />
                    <table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
                        <tr>
                            <td width="70%" class="columnTitle">Agent </td>
                            <td width="30%" align="right" class="columnTitle">Payment Recieved  </td>
                            
                        </tr>
						<?
							foreach($agentsArray as $key)
							{
						?>
								<tr>
									<td>
										<?=$key["name"]." [".$key["username"]."]"?>
									</td>
									<td align="right"><?=number_format(abs($key["TOTAL"]), 2, ".", ",")?></td>
									<!-- td align="right"><?=number_format($key["RECEIVED"], 2, ".", ",")?></td -->
									
								</tr>
						<?	
								$tmpTotalTransactionAmount += (abs($key["TOTAL"]));
								//$tmpTotalReceivedAmount += (abs($key["RECEIVED"]));
						
							}
						?>
						
						<tr>
                            <td>&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <!-- td align="right">&nbsp;</td -->
                            <td align="right">&nbsp;</td>
                        </tr>
						
                        <tr>
                            <td class="netBalance">Totals</td>
                            <td align="right" class="netBalance"><strong>
                                <?=number_format($tmpTotalTransactionAmount, 2, ".", ",")?>
                            </strong></td>
                            <!-- td align="right" class="netBalance"><strong>
                                <? //number_format($tmpTotalReceivedAmount, 2, ".", ",") ?>
                            </strong></td -->
                          
							
                        </tr>
                    </table>
                    <br />
                    <?
				if($print != "Y")
				{
			?>
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td width="50%" class="reportToolbar"><input name="btnPrint" type="button" id="btnPrint" value=" Print " onclick="javascript: print();" /></td>
                            <td width="50%" align="right" class="reportToolbar">&nbsp;</td>
                        </tr>
                    </table>
                    <?
				}
			?>
		</td>
    </tr>
</table>
<?
	if($print == "Y")
	{
?>
		<script type="text/javascript">
			print();
		</script>
<?
	}
?>
</body>
</html>
