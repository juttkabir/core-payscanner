<?
session_start();
include ("../include/config.php");
$tran_date = date("Y-m-d");
$tran_date = date("F j, Y", strtotime("$tran_date"));
$date_time = date('d-m-Y  h:i:s A');
require_once './cm_xls/Excel/reader.php';
include ("security.php");
$modifyby = $_SESSION["loggedUserData"]["userID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

/* This file created by Naiz Ahmad @4513 at 20-02-2009 */
$msgFlag = true;
$date_time = date('Y-m-d h:i:s');
if($_POST["Submit"] != "")
{


	if ($_FILES["csvFile"]["name"]=='none')
	{
		$msg = CAUTION_MARK . " Please select file to import.";
	}
	$ext = strtolower(strrchr($_FILES["csvFile"]["name"],"."));
	if ($ext == ".txt" || $ext == ".csv" || $ext == ".xls" )
	{
		if (is_uploaded_file($_FILES["csvFile"]["tmp_name"]))
		{
				
			$Pict="$username-" . date("Y-m-d") . $ext;
			$filename = $_FILES["csvFile"]["tmp_name"];
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('CP1251');
			$data->read($filename);
			error_reporting(E_ALL ^ E_NOTICE);
			$counter = 0;
			
			for ($i = 11; $i <= $data->sheets[0]['numRows']; $i++)
			{ 
			
		            $data1[1]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][1]);  //branchName
					$data1[2]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][2]);  //bankId
					$data1[3]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][3]);  //branchCode
					$data1[4]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][4]);  //branchAddress
					$data1[5]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][5]);  //extra1 (Address2)
					$data1[6]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][6]);  //	extra2 (District)
					$data1[7]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][7]);  //	extra3 (state)
					$data1[8]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][8]);  //	country
					
					
					if($data1[2] == "SBI")
					{
					 	$bankName = "STATE BANK OF INDIA";
					}elseif($data1[2] == "IOB")
					{
						$bankName = "INDIAN OVERSEAS BANK";
					
					}elseif($data1[2] == "SBT")
					{
						$bankName = "STATE BANK OF TRAVANCORE";
					}elseif($data1[2] == "PNB")
					{
						$bankName = "PUNJAB NATIONAL BANK";
					}elseif($data1[2] == "FBL")
					{
						$bankName = "FEDERAL BANK LTD";
					}elseif($data1[2] == "CAN")
					{
						$bankName = "CANARA BANK";
					}elseif($data1[2] == "SIB")
					{
						$bankName = "SOUTH INDIAN BANK";
					}elseif($data1[2] == "ICICI")
					{
						$bankName = "ICICI BANK";
					}
					
					
				 $pepData = selectFrom("select 
				 								id,
												bankId,
												name,
												country,
												branchCode,
												branchAddress,
												extra1,
												extra2,
												extra3 
											from
												 banks
											where 
					                       		branchName = '".$data1[1] ."' and
												name = '".$bankName."' and
												bankId='".$data1[2] ."' and 
												branchCode = '".$data1[3] ."' 
												and branchAddress = '".$data1[4] ."' and
										   		extra1 = '".$data1[5] ."' and  
												extra2 = '".$data1[6] ."' and  
												extra3 = '".$data1[7] ."' and 
												country = '".$data1[8] ."'");
					
					if($pepData["id"] !='' ){
						
						 $updatPep= "update banks
						 						set
													 branchName = '". $data1[1] ."',
													 name = '".$bankName."',
													 bankId = '". $data1[2] ."',
													 branchCode = '". $data1[3] ."',
													 branchAddress = '". $data1[4] ."',
													 extra1 = '". $data1[5] ."',
													 extra2 = '". $data1[6] ."',
													 extra3 = '". $data1[7] ."',
												 	 country = '". $data1[8] ."'
						 						where 
													id='".$pepData["id"]."' ";
				
					update($updatPep);
					$counter ++;
					$msg = " $counter Bank list is Updated Successfully ";
					
						}else{
	     			
				    $Querry_Sqls = "INSERT INTO banks
				   									(
													branchName,
													name,
													bankId,
													branchCode,
													branchAddress,
													extra1,
													extra2,
													extra3,
													country
													) 
												VALUES 
				   									(
													'". $data1[1] ."',
													'".$bankName."',
													'". $data1[2] ."',
													'". $data1[3] ."',
													'". $data1[4] ."',
													'". $data1[5] ."',
													'". $data1[6] ."',
													'". $data1[7] ."',
													'". $data1[8] ."'
													)";
				     
				 
			     insertInto($Querry_Sqls);
			 	$pepID = @mysql_insert_id();
				
				$counter ++;
				$msg = " $counter Bank List is Imported Successfully ";
				
			  }
			
	    }
			
	}
		else
		{
			$msg = CAUTION_MARK . " Your file is not uploaded due to some error.";
			$msgFlag = false;
		}
	}
	else
	{
		$msg = CAUTION_MARK . " File format must be .txt, .csv or .dat";
		$msgFlag = false;
	}	
}


?>


<html>
<head>
	<title>Import Muthoot Bank List</title>
</head>
<body>
	<table width="400" border="0" align="center" cellpadding="5" cellspacing="1">
  	<form name="bankList" action="import-bank-muthoot.php" method="post" enctype="multipart/form-data"> 
 
  <? 
  
		   if($msg!= "" && $msgFlag == true) {
  ?>
  <tr align="center">
   
	 <td colspan="2"><b><font color= "<? echo SUCCESS_COLOR ?>" ><? echo $msg?></font></b><br /></td>
  </tr>
  <?
			  }else{
			 	?>
  <tr align="center">
     <td colspan="2"><b><font color= "<? echo CAUTION_COLOR ?>" ><? echo $msg?></font></b><br /></td>
  </tr>
  <? } ?>
  <tr>
    <td bgcolor="#DFE6EA" colspan="2">Select File Name </td>
  </tr>
    <tr>
				<td width="25%" align="right" bgcolor="#DFE6EA">Select File </td>
			    <td width="75%" bgcolor="#DFE6EA"><input name="csvFile" type="file" id="csvFile" size="15"></td>
  </tr>
<tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">&nbsp;</td>
			    <td align="center" bgcolor="#DFE6EA"><input name="Submit" type="submit" class="flat" value="Submit"></td>
  </tr>
  </form>
</table>

</body></html>
