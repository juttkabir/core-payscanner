<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];

	$strMainCaption = "Manage Currency Exchange Fee Records"; 

	$cmd = $_REQUEST["cmd"];

	//debug($_REQUEST);
	if(!empty($cmd))
	{
		$bolDataSaved = false;
		/* Request to store or update the data of company */
		if($cmd == "ADD")
		{
			/**
			 * If the Id is empty than the record is new one
			 */
			$strCreatedBy = $userID."|".$agentType;
			$strInsertSql = "insert into currency_exchange_fee 
								(type, currencyF, currencyT, fee, created_by, created_on, updated_on)
							 values
								('".$_REQUEST["fee_type"]."','".$_REQUEST["currencyF"]."','".$_REQUEST["currencyT"]."',
								 '".$_REQUEST["cheque_fee"]."','".$strCreatedBy."','".time()."','".time()."')";
			
			if(mysql_query($strInsertSql))
				$bolDataSaved = true;
			else
				$bolDataSaved = false;

			$cmd = "";
			$fdi = "";
			$fee_type = "";
			$currencyF = "";
			$currencyT = "";
			$cheque_fee = "";
			
			activities($_SESSION["loginHistoryID"],"INSERT",0,"currency_exchange_fee","New currency Fee Added");
		}
		
		if($cmd == "UPDATE")
		{
			/**
			 * Record is not new and required the updation
			 */
			if(!empty($_REQUEST["fdi"]))
			{
				$strUpdateSql = "update currency_exchange_fee 
								 set
									type = '".$_REQUEST["fee_type"]."',
									currencyF = '".$_REQUEST["currencyF"]."',
									currencyT = '".$_REQUEST["currencyT"]."',
									fee = '".$_REQUEST["cheque_fee"]."',
									updated_on = '".time()."'
								 where
									fee_id = '".$_REQUEST["fdi"]."'
							"; 
				if(update($strUpdateSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
					
				$fdi = "";
				$fee_type = "";
				$currencyF = "";
				$currencyT = "";
				$cheque_fee = "";
				$cmd = "";
				
				activities($_SESSION["loginHistoryID"],"UPDATE",$_REQUEST["fdi"],"currency_exchange_fee","Currency Exchange Fee updated");
			}	
		}
		
		
		
		if($cmd == "EDIT")
		{
		
			if(!empty($_REQUEST["fdi"]))
			{
				$strGetDataSql = "select * from currency_exchange_fee where fee_id='".$_REQUEST["fdi"]."'";
				$arrCompanyData = selectFrom($strGetDataSql);
				//debug($arrCompanyData);
				
				$fdi = $arrCompanyData["fee_id"];
				$fee_type = $arrCompanyData["type"];
				$currencyF = $arrCompanyData["currencyF"];
				$currencyT = $arrCompanyData["currencyT"];
				$cheque_fee = $arrCompanyData["fee"];
								
				$cmd = "UPDATE";
				
				activities($_SESSION["loginHistoryID"],"VIEW",$_REQUEST["fdi"],"currency_exchange_fee","Currency Exchange Fee Viewed");
			}
		}
	
		if($cmd == "DEL")
		{
			if(!empty($_REQUEST["fdi"]))
			{
				$strDelSql = "delete from currency_exchange_fee where fee_id='".$_REQUEST["fdi"]."'";
				if(mysql_query($strDelSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
				
				activities($_SESSION["loginHistoryID"],"DELETE",$_REQUEST["fdi"],"currency_exchange_fee","Currency Exchange Fee Deleted");
			}
			else
				$bolDataSaved = false;
			$cmd = "";
		}
	}
	
	//$arrCompaniesRows = selectMultiRecords("select company_id, name from company order by name");
	
	if(empty($cmd))
		$cmd = "ADD";
	
	/* The default amount in "amount from" field should be '1' */
	/*if(empty($amount_from))
		$amount_from = 1;*/
	
	/* Fetching the list of fees to display at the bottom */	
	$arrAllFeesData = selectMultiRecords("Select * from currency_exchange_fee order by updated_on desc");

		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Manage Currency Exchange Fee Record</title>
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script language="javascript" src="jquery.cluetip.js"></script>
<script>
	$(document).ready(function() {
		$("#submitFee").validate({
			rules: {
				fee_type: "required",
				currencyF: "required",
				currencyT: "required",
				cheque_fee: {
					required: true,
					number: true,
					min: 0//,
					//min: "#amount_from"+1
				}/*,
				company_val: {
				   required: function(element) {
						return $("#fee_type").val() == "C"
					  }
				}*/

			},
			messages: {
				fee_type: "<br />Please select the fee type from drop down.",
				currencyF:"<br />Please select operational currency",
				currencyT:"<br />Please select buy/sell currency",
				cheque_fee: { 
					required: "<br />Please enter the Fee(Minimum value zero).",
					number: "<br />This is not valid fee.",
					min: "<br />Fee can not be less than 0"//,
					//min: "<br />Value should be greater than "+ $("#amount_from").val()
				}
			}
		});
		
		/*$("#fee_type").click(function(){
			if($(this).val() == "C")
				$("#companyRow").show();
			else
				$("#companyRow").hide();
		});*/
		
		<? if(!empty($_REQUEST["fdi"])) { ?>
			$("#fee_type").click();
		<? } ?>
		
		$('img').cluetip({splitTitle:'|'});
		
	});
</script>
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style>
td{ 
	font-family:Arial, Helvetica, sans-serif;
	size:8px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.feeList td {
	text-align:center;
	font-size:10px;
	font-family:Arial, Helvetica, sans-serif;
}

.feeList th {
	text-align:center;
	font-size:14px;
	font-family: Arial, Helvetica, sans-serif;
	color:#ffffff;
	background-color:#005666;
}


.firstRow td {
	background-color:#999999;
	font-size:12px;
	color: #000000;
}
.secondRow td {
	background-color: #cdcdcd;
	font-size:12px;
	color: #000000;
}
</style>
</head>
<body>
<form name="submitFee" id="submitFee" action="<?=$_SERVER["PHP_SELF"]?>" method="post">
	<table width="50%" border="0" cellspacing="1" cellpadding="2" align="center" id="mainTble">
		<tr>
			<td colspan="2" align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong>
			</td>
		</tr>
		<? if($bolDataSaved) { ?>
			<tr bgcolor="#000000">
				<td colspan="2" align="center" style="text-align:center; color:#EEEEEE; font-size:13px; font-weight:bold">
					<img src="images/info.gif" title="Information|Command Executed Successfully." />&nbsp;Command Executed Successfully!
				</td>
			</tr>
		<? } ?>
		
		<tr bgcolor="#ededed">
			<td colspan="2" align="center" class="tdDefination" style="text-align:center">
				All fields mark with <font color="red">*</font> are compulsory Fields.
			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="40%" class="tdDefination"><b>Fee Type:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="60%">
				<select name="fee_type" id="fee_type">
					<option value="">Select Fee Type</option>
					<option value="F" <?=($fee_type=="F"?'selected="selected"':'')?>>Fixed Amount</option>
					<option value="P" <?=($fee_type=="P"?'selected="selected"':'')?>>Percentage Of Cheque Amount</option>
				</select>
			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="40%" class="tdDefination"><b>Select Operating Currency:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="60%">

				<select name="currencyF" id="currencyF">
					<?php /*?><option value="">Select Operating Currency</option><?php */?>
					<option value="GBP" >GBP</option>
				</select>
			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="40%" class="tdDefination"><b>Select Buy/Sell Currency:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="60%">
			<?php
				$currDataArr = SelectMultiRecords("select cID,country,currencyName,count(currencyName) as inter_curr 
														from currencies 
														where cID IN 
														(select buysellCurrency 
														from curr_exchange order by id DESC) 
														group by currencyName");
			?>
				<select name="currencyT" id="currencyT">
					<option value="">Select Buy/Sell Currency</option>
                    <?php foreach($currDataArr as $k=>$v){?>
						<option value="<?php echo $v["currencyName"]?>" <?=($currencyT==$v["currencyName"]?'selected="selected"':'')?>><?php echo $v["currencyName"]?></option>
					<?php }?>
				</select>
			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="40%" class="tdDefination"><b>Fee:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="60%"><input type="text" name="cheque_fee" id="cheque_fee" size="20" value="<?=$cheque_fee?>" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" name="storeData" value="Submit" />
				&nbsp;&nbsp;
				<!--<input type="reset" value="Clear Fields" />-->
				<input type="hidden" name="fdi" value="<?=$fdi?>" />
				<input type="hidden" name="cmd" value="<?=$cmd?>" />
				<? if(!empty($fdi)) { ?>
					&nbsp;&nbsp;<a href="<?=$_SERVER['PHP_SELF']?>">Add New One</a>
				<? } ?>
			</td>
		</tr>
	</table>
</form>
<br /><br />
<fieldset>
	<legend style="color:#666666; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:18px; font-weight:bold">
		&nbsp;AVAILABLE CHEQUE ORDER FEES&nbsp;
	</legend>
	<table width="95%" align="center" border="0" class="feeList" cellpadding="2" cellspacing="2">
		<tr>
			<th width="20%">Fee Type</th>
			<th width="10%">Operational Currency</th>
			<th width="10%">Buy/Sell Currency</th>
			<th width="15%">Fee</th>
			<th width="10%">Created By</th>
			<th width="10%">Created On</th>
			<th width="15%">Updated On</th>
			<th>Actions</th>
		</tr>
		<?php
			foreach($arrAllFeesData as $feeVal)
			{
				$userData = explode("|",$feeVal["created_by"]);
				$arrUserResultSet = array();
				if(!empty($userData[0]))
					$arrUserResultSet = selectFrom("select name, username from admin where userid='".$userData[0]."'");
					
				$strFeeType = "";
				if($feeVal["type"] == "F")
					$strFeeType = "Fixed Amount";
				elseif($feeVal["type"] == "P")
					$strFeeType = "Percentage of Amount";
				/*elseif($feeVal["type"] == "C")
				{
					$strFeeType = "Company Based Fee";
					if(!empty($feeVal["type_value"]))
					{
						$arrCompanyNameResultSet = selectFrom("select name from company where company_id='".$feeVal["type_value"]."'");
						$strFeeType .= "<br /><b>".$arrCompanyNameResultSet["name"]."</b>";
					}
				}*/
				
				if($strClass == "firstRow")
					$strClass = "secondRow";
				else
					$strClass = "firstRow";
		?>
		<tr class="<?=$strClass?>">
			<td><?=$strFeeType?></td>
			<td><?=$feeVal["fee"]?></td>
			<td><?=$feeVal["currencyF"]?></td>
			<td><?=$feeVal["currencyT"]?></td>
			<td><?=$arrUserResultSet["name"]." [".$arrUserResultSet["username"]."] / ".$userData[1]?></td>
			<td><?=date("d/m/Y",$feeVal["created_on"])?></td>
			<td><?=date("d/m/Y H:i:s",$feeVal["updated_on"])?> GMT</td>
			<td>
				<a href="<?=$_SERVER['PHP_SELF']?>?fdi=<?=$feeVal["fee_id"]?>&cmd=EDIT">
					<img src="images/edit_th.gif" border="0" title="Edit Fee| You can edit the record by clicking on this thumbnail." />
				</a>&nbsp;&nbsp;
				<a href="<?=$_SERVER['PHP_SELF']?>?fdi=<?=$feeVal["fee_id"]?>&cmd=DEL">
					<img src="images/remove_tn.gif" border="0" title="Remove Fee|By clicking on this the fee will be no longer available." />
				</a>
			</td>
		</tr>
				
		<?
			}
		?>
	</table>
</fieldset>

</body>
</html>