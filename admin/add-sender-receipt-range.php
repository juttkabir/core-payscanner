<?
session_start();
include ("../include/config.php");
$date_time = date("Y-m-d h:i:s");
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

 $thisPage = "add-sender-receipt-range.php?msg=Y&flag=".$_GET["flag"];

$lower  = "";
$upper  = "";
$prefix = "";
$errMsg = "";

$_SESSION["agentID"] = $_POST["agentID"];
$_SESSION["prefix"] = $_POST["prefix"];
if($_GET["refID"] != '')
{
	if($_GET["update"] == 'Y')
	{
		
		$refInfo  = selectFrom("select * from ".TBL_RECEIPT_RANGE." where id = '".$_GET["refID"]."'");
		$agentID = $refInfo["agentID"];	
		$lower  =  $refInfo["rangeFrom"];
		$upper  =  $refInfo["rangeTo"];
		$prefix =  $refInfo["prefix"];
	}	
	
}
if ($_POST["Submit"] == "Save") {
	$agentID = $_POST["agentID"];	
	$lower = trim($_POST["receiptRangeFrom"]);
	$upper = trim($_POST["receiptRangeTo"]);
	$prefix =trim($_POST["prefix"]);
	if($_GET["refID"] != '')
	{
		
			
				
				
			if (isExist("SELECT `id` FROM ".TBL_RECEIPT_RANGE." WHERE agentID = '".$agentID."' AND id != '".$_GET["refID"]."' AND ((`rangeFrom` > '".$lower."' and `rangeFrom` < '".$upper."') OR (`rangeFrom` = '".$lower."' OR `rangeFrom` = '".$upper."' OR `rangeTo` = '".$lower."' OR `rangeTo` = '".$upper."') OR (`rangeFrom` < '".$lower."' and `rangeTo` > '".$lower."'))")) {
				$errMsg = "Range already exists for the selected sender";
				
			} else {
			$qInsert = "UPDATE ".TBL_RECEIPT_RANGE." 
									SET `rangeFrom` = '".$lower."', 
											`rangeTo`   = '".$upper."', 
											`prefix`   = '".$prefix."', 
											`created`   = '".$date_time."', 
											`userType` = 'sender'
											where id = '".$_GET["refID"]."'";
				update($qInsert);
				insertError(ARR4);
				$thisPage = "view-receipt-ranges.php?msg=Y";
				$thisPage .= "&success=Y&agentID=$agentID";
				redirect($thisPage);
				}
			
		}else{
				
						
					
					
					
					
					
				if (isExist("SELECT `id` FROM ".TBL_RECEIPT_RANGE." WHERE agentID = '".$agentID."' AND ((`rangeFrom` > '".$lower."' and `rangeFrom` < '".$upper."') OR (`rangeFrom` = '".$lower."' OR `rangeFrom` = '".$upper."' OR `rangeTo` = '".$lower."' OR `rangeTo` = '".$upper."') OR (`rangeFrom` < '".$lower."' and `rangeTo` > '".$lower."'))")) {
					$errMsg = "Range already exists for the selected sender";
				} else {
					$qInsert = "INSERT INTO ".TBL_RECEIPT_RANGE." 
										SET `agentID`   = '".$agentID."', 
										`rangeFrom` = '".$lower."', 
										`rangeTo`   = '".$upper."', 
										`prefix`   = '".$prefix."', 
										`created`   = '".$date_time."', 
										`userType` = 'sender'";
				insertInto($qInsert);
				insertError(ARR1);
				$thisPage .= "&success=Y";
				redirect($thisPage);
		}
	
	}
}

?>
<html>
<head>
	<title><? if($_GET["refID"] != ''){ ?>Update <? }else{ ?>Add<? } ?> Sender's Receipt Number Range</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
-->
</script>
<script language="javascript">
<!--

function checkForm(theForm) {
	
	if (theForm.agentID.options.selectedIndex == 0) {
		alert("Please select the Sender.");
		theForm.agentID.focus();
		return false;
	}

	if (theForm.prefix.options.selectedIndex == 0) {
		alert("Please select Prefix.");
		theForm.prefix.focus();
		return false;
	}


	if (theForm.receiptRangeFrom.value == "" || IsAllSpaces(theForm.receiptRangeFrom.value)) {
  	alert("Please provide Lower Range of Reference number");
    theForm.receiptRangeFrom.focus();
    return false;
  } else if (!isNumeric(trim(theForm.receiptRangeFrom.value))) {
  	alert("Please provide Lower Range of Reference number in Numbers Only.");
    theForm.receiptRangeFrom.focus();
    return false;
  }
  
	if (theForm.receiptRangeTo.value == "" || IsAllSpaces(theForm.receiptRangeTo.value)) {
  	alert("Please provide Upper Range of Reference number");
    theForm.receiptRangeTo.focus();
    return false;
  } else if (!isNumeric(trim(theForm.receiptRangeTo.value))) {
  	alert("Please provide Upper Range of Reference number in Numbers Only.");
    theForm.receiptRangeTo.focus();
    return false;
  }
  
  if (parseInt(theForm.receiptRangeFrom.value) > parseInt(theForm.receiptRangeTo.value)) {
  	alert("Lower Range should not be greater than Upper Range.");
    theForm.receiptRangeFrom.focus();
    return false;
  }
  
	return true;
}

function isNumeric(sText) {
	var ValidChars = "0123456789";
  var IsNumber = true;
  var Char;
  for (i = 0; i < sText.length && IsNumber == true; i++) {
		Char = sText.charAt(i);
    if (ValidChars.indexOf(Char) == -1)	{
    	IsNumber = false;
    }
	}
  return IsNumber;   
}

function trim(sString) {
	while (sString.substring(0,1) == ' '){
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ') {
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
}

function IsAllSpaces(myStr) {
	while (myStr.substring(0,1) == " ")	{
  	myStr = myStr.substring(1, myStr.length);
  }
  if (myStr == "")	{
  	return true;
  }
  return false;
}

// end of javascript -->
</script>
	<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><? if($_GET["refID"] != ''){ ?>Update <? }else{?>Add <? } ?> Sender Reciept Number Range</td>
  </tr>
  <form action="add-sender-receipt-range.php?flag=<?=$_GET["flag"]?>&refID=<?=$_GET["refID"]?>" method="post" onSubmit="return checkForm(this);">
  <tr>
      <td align="center"> 
        <table width="500" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="500" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Add Sender Reciept Number Range</strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
    <? if ($errMsg != "") {  ?>
		<tr bgcolor="#ededed">
            <td colspan="2" bgcolor="#EEEEEE">
<table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo CAUTION_COLOR; ?>"><b><i><? echo CAUTION_MARK;?></i></b></font></td>
	<td width="635"><? echo "<font color='" . CAUTION_COLOR . "'><b>".$errMsg."</b><br></font>"; ?>
		  </td>
		 </tr>
		</table>
	 </td>
	</tr>
		<? } else if ($_GET["msg"] != "") { ?>
		<tr bgcolor="#ededed">
            <td colspan="2" bgcolor="#EEEEEE">
<table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
	<td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br></font>"; $_SESSION['error']=""; ?>
		  </td>
		 </tr>
		</table>
	 </td>
	</tr>
		<? } 
		if ($_GET["flag"]!="")
    {
    ?>
    	<tr bgcolor="#ededed">
      	<td height="19" colspan="2"><a class="style2" href="view-receipt-ranges.php?agentID=<?=$agentID?>">Go Back</a></td>
    	</tr>
    <? 
    } 
    ?>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center"><font color="#FF0000">* Compulsory 
              Fields</font></td>
		</tr>
        <tr bgcolor="#ededed">
            <td width="152"><font color="#005b90"><strong>Select Sender</strong></font></td>
            <td width="343">
            	<?
		        		if($_GET["refID"] != ''){
		        				$updateAgentInfo = selectFrom("select customerID,accountname, agentID,firstName from ".TBL_CUSTOMER." where customerID = '".$agentID."'");	        		
		        				echo($updateAgentInfo["firstName"]." [".$updateAgentInfo["accountName"]."]");
		        				?>
		        						<input type="hidden" name="agentID" value="<?=$updateAgentInfo["userID"]?>">
		        						
		        				<?
		        		}else{    
			            	
			            
			            	?>
			                <select name="agentID" style="font-family:verdana; font-size: 11px">
			                  <option value="">- select sender-</option>
										<?
					          
						          		 $agentQuery  = " select customerID,accountname, agentID,firstName from ".TBL_CUSTOMER." where accountname != '' ";
													
												
												$agentQuery .= " order by customerID ";
												$agents = selectMultiRecords($agentQuery);
												for ($i=0; $i < count($agents); $i++) {
										?>
					            	   <option value="<?=$agents[$i]["customerID"]; ?>" <? echo ($agents[$i]["customerID"] == $_POST["customerID"] ? "selected" : "") ?>><? echo($agents[$i]["firstName"]." [".$agents[$i]["accountname"]."]"); ?></option>
					          <?	}  ?>
					                </select>
					                    <script language="JavaScript">
         				SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID"]; ?>");
              </script>
				             <?
				             
		            }
		            ?>   
						</td>
        </tr>
    
        
        
        
       
        <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong><? if(CONFIG_DIST_REF_NUMBER == '1'){?>Reference Number<? }else{?>Receipt<? }?> Range*</strong></font></td>
            <td>
            	<input type="text" name="receiptRangeFrom" value="<? echo $lower; ?>" size="10" maxlength="16" <? echo($_GET["refID"] != '' ? "readonly": "");?>> To <input type="text" name="receiptRangeTo" value="<? echo $upper; ?>" size="10" maxlength="16">
            </td>
        </tr>

       
	        <tr bgcolor="#ededed">
	            <td width="285"><font color="#005b90"><strong>Prefix</strong></font></td>
	            <td>
	            	<select name="prefix" style="font-family:verdana; font-size: 11px">
			                  <option value="">- Select Prefix -</option>
			                  <option value="agentNumber">- Agent Number -</option>
			                  <option value="senderNumber">- Sender Number -</option>
			           </select>
			               <script language="JavaScript">
         				SelectOption(document.forms[0].prefix, "<?=$_SESSION["prefix"]; ?>");
              </script>
	            	
	            </td>
	        </tr>
	      
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" name="Submit" value="Save">&nbsp;&nbsp; <input type="reset" value="Clear">
			</td>
		</tr>
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>