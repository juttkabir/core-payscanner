<?php 


$date_time = date('d-m-Y  h:i:s A');
	
	include ("../include/config.php");
	include ("security.php");
	
//debug($_REQUEST);
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");

	$response = new Services_JSON();
	
	$page = $_REQUEST['page']; // get the requested page 
	$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord = $_REQUEST['sord']; // get the direction 
	
	if(!$sidx && $_REQUEST['getGrid']!='transList')
	{
		$sidx = 'importedOn DESC, id ';
		//$sidx = 'transDate DESC, id ';
	}elseif(!$sidx)
	{
		$sidx = 1;
	}
	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
			
	if($limit == 0)
	{
		$limit=50;
	}
	
	$SubmitJS		= "";
	$fromJS     	= "";
	$toJS			= "";
	$refNumberJS	="";
	$amount1JS		= "";
	$descJS	= "";
$tranactionSource = $_REQUEST["tranactionSource"];


$from=explode("/", $_SESSION["fromDate"]);

$fromDate_=$from[2]."-".$from[1]."-".$from[0];
$to=explode("/", $_SESSION["toDate"]);
$toDate_=$to[2]."-".$to[1]."-".$to[0];


//$to=str_replace("/","-",$_SESSION["toDate"]);

//$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];

	//$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
$queryDate = " and (transDate >= '$fromDate_ 00:00:00' and transDate <= '$toDate_ 23:59:59')";

$cancelDate = " and (cancelDate >= '$fromDate_ 00:00:00' and cancelDate <= '$toDate_ 23:59:59')";


if($_REQUEST['transType']=='payexSimple')
{
$transType='Payex';
$query ="SELECT SUM(transAmount) as transAmount, count(transID) as transCount ,currencyFrom,cancelDate,date_format(transDate, '%Y-%m-%d') as simpleDate, date_format(cancelDate, '%Y-%m-%d') as cancelDate,trans_source,IMFee FROM ".TBL_TRANSACTIONS." Where 1 ".$queryDate." AND trans_source !='O' GROUP by currencyFrom";
}else if($_REQUEST['transType']=='onlineSimple') {
$transType='Online';
$query ="SELECT SUM(transAmount) as transAmount, count(transID) as transCount ,currencyFrom,cancelDate,date_format(transDate, '%Y-%m-%d') as simpleDate, date_format(cancelDate, '%Y-%m-%d') as cancelDate,trans_source,IMFee FROM ".TBL_TRANSACTIONS." Where 1 ".$queryDate." AND trans_source ='O' GROUP by currencyFrom ";
} else if($_REQUEST['transType']=='onlineCancel') {

$transType='Online';
$query ="SELECT SUM(transAmount) as transAmount, count(transID) as transCount ,currencyFrom,cancelDate,date_format(transDate, '%Y-%m-%d') as simpleDate, date_format(cancelDate, '%Y-%m-%d') as cancelDate,trans_source,IMFee FROM ".TBL_TRANSACTIONS." Where 1 ".$cancelDate." AND trans_source ='O' GROUP by currencyFrom ";

} else if($_REQUEST['transType']=='payexCancel') {
$transType='Payex';
$query ="SELECT SUM(transAmount) as transAmount, count(transID) as transCount ,currencyFrom,cancelDate,date_format(transDate, '%Y-%m-%d') as simpleDate, date_format(cancelDate, '%Y-%m-%d') as cancelDate,trans_source,sum(IMFee) as IMFee  FROM ".TBL_TRANSACTIONS." Where 1 ".$cancelDate." AND trans_source !='O' GROUP by currencyFrom ";

}


//debug($query);

//debug($query);

if($_REQUEST["getGrid"] == "transList"){
$rowT = selectMultiRecords($query, MYSQL_ASSOC);

$strDes ='';
	$strBAc ='';
	$batchArray =array();
	$counter = 0;
	$formatArray = array();

	//$batchArray = getBatchNumber($rowT);
	//debug($formatArray);
	
	$countT = count($rowT);
	
	if($countT > 0)
	{
		$total_pages = ceil($countT / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	if($start < 0)
	{
		$start = 0;
	}
	$query .= " Order by transDate DESC";
	$query .= " LIMIT $start , $limit";			
	//echo $queryT;
	//debug($queryT);
	$rowT = selectMultiRecords($query, MYSQL_ASSOC);
	
	
	//debug($batchArray);
	
	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $countT;

	
}
//debug($query);
$formatArray = selectMultiRecords($query);
for($k =0;$k < count($formatArray);$k++)
	{
	
	$response->rows[$k]['id'] = $k;
               
			   if(($_REQUEST['transType']=='payexCancel')||($_REQUEST['transType']=='onlineCancel')) {		
				$strImportDate = $formatArray[$k]['cancelDate']; 
				}else {
				$strImportDate = $formatArray[$k]['simpleDate']; 
				}
				
				$strDes= $transType;
				$strBAc= $formatArray[$k]['transCount'];
				$strAmount= $formatArray[$k]['transAmount']." ".$formatArray[$k]['currencyFrom'];
				$strCommision = $formatArray[$k]['IMFee'];
				
			
	
	$response->rows[$k]['cell'] = array(
									$strImportDate,
									$strDes,
									$strBAc,
									$strAmount,
									$strCommision
									
								);		
				
				
				$linkVal = '';
		$urlTrans = '';
	
	
		$i++;
					}
						echo $response->encode($response); 

?>