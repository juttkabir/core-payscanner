<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

// define page navigation parameters
if ($offset == "")
	$offset = 0;
$limit=50;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
$SQL_Qry = "select * from ".TBL_FAQS;
$faqs = SelectMultiRecords($SQL_Qry);
$allCount = count($faqs);
if ($sortBy !="")
	$SQL_Qry = $SQL_Qry. " order by $sortBy ASC ";
$SQL_Qry = $SQL_Qry. " LIMIT $offset , $limit";
//echo $SQL_Qry;
$faqs = SelectMultiRecords($SQL_Qry);
?>
<html>
<head>
	<title>Categories List</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="include/Main.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
	function checkForm(theForm) {
		var a = false;
		var none = true;
		var j = 2; 
		// < (theForm.elements.length - 11)
		for(i =0; i < <?=count($faqs); ?>; i++){
			if(theForm.elements[j].checked == true){
				a = confirm("Are you sure you want to delete the checked FAQ(s) from the database?");
				none = false;
				break;
			}
			j++;
		}
		if (none) {
			a = false;
			alert("No FAQ(s) is checked for deletion.")
		}
		return a;
	}
	function checkAllFn(theForm, field) {
		if (field == 1) {
			if (theForm.del.value == 0)
				theForm.del.value = 1;
			else
				theForm.del.value = 0;
			if (theForm.del.value == 1){
				j=2;
				for(i=0; i < <?=count($faqs);?>; i++) {
					theForm.elements[j].checked = true;
					j++;
				}
			} else {
				j = 2;
				for(i=0; i< <?=count($faqs);?>; i++) {
					theForm.elements[j].checked = false;
					j++;
				}
			}
		} else {
			if (theForm.app.value == 0)
				theForm.app.value = 1;
			else
				theForm.app.value = 0;
			if (theForm.app.value == 1){
				j = 3;
				for(i=0; i<<?=count($faqs);?>; i++) {
					theForm.elements[j].checked = true;
					j = j+2;
				}
			} else {
				j = 3;
				for(i=0; i<<?=count($faqs);?>; i++) {
					theForm.elements[j].checked = false;
					j = j+2;
				}
			}
		}
	}
	// end of javascript -->
	</script>
	<style>
		.bak2top {
				font-family: Arial;
				font-size: 10px;
				font-weight: bold;
				color: #0000FF;
				background-color: #CCCCFF;
				border: 1px solid;
				border-color: #0000FF;
				padding: 2px;
			}
	</style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">FAQs Listing</td>
 </tr>
  <form action="delete-faqs.php" method="post" onSubmit="return checkForm(this);">
  <input type="hidden" name="del" value="0">
  <input type="hidden" name="app" value="0">
  <tr>
    <td align="center">
		<table width="527" border="0" cellspacing="1" cellpadding="1" align="center">
          <?
			if ($allCount > 0){
		?>
          <tr> 
            <td colspan="4" bgcolor="#000000"> <table width="600" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($faqs) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($faqs));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
          <tr bgcolor="#DFE6EA"> 
            <td width="44"><a href="#" onClick="checkAllFn(document.forms[0], 1);"><font color="#005b90"><strong>Delete</strong></font></a></td>
            <!--td width="67"><a href="<?php print $PHP_SELF . "?sortBy=type";?>"><font color="#005b90"><strong>Type</strong></font></a></td-->
            <td width="193"><a href="<?php print $PHP_SELF . "?sortBy=question";?>"><font color="#005b90"><strong>Question</strong></font></a></td>
            <td width="287"><a href="<?php print $PHP_SELF . "?sortBy=answer";?>"><font color="#005b90"><b>Answer</b></font></a></td>
          </tr>
          <?
			for ($i=0; $i<count($faqs); $i++){
		?>
          <tr valign="top" bgcolor="#eeeeee"> 
            <td align="center"> <input type="checkbox" name="faqID[<?=$i;?>]" value="<?=$faqs[$i]["faqID"]; ?>"></td>
            <!--td><?=stripslashes($faqs[$i]["type"]); ?></td-->
            <td><a href="update-faq.php?faqID=<?=$faqs[$i]["faqID"];?>"><font color="#005b90"><? echo stripslashes($faqs[$i]["question"]); ?></font></a></td>
            <td> 
              <?=stripslashes($faqs[$i]["answer"]); ?>
            </td>
          </tr>
          <?
			}
		?>
          <tr> 
            <td colspan="4" bgcolor="#000000"> <table width="600" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($faqs) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($faqs));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
          <tr> 
            <td colspan="4" align="center"> <input type="submit" value="Delete FAQs"> 
            </td>
          </tr>
          <?
			} else {
		?>
          <tr> 
            <td colspan="4" align="center"> No FAQs found in the database. </td>
          </tr>
          <?
			}
		?>
        </table>
	</td>
  </tr>
</form>
</table>
	<script language="JavaScript" src="./javascript/bak2top.js"></script>
</body>
</html>
