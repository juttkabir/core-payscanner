<?
	session_start();
	include ("../include/config.php");
	include ("security.php");
	$agentType = getAgentType();
	$parentID = $_SESSION["loggedUserData"]["userID"];
	
	$cmd = $_REQUEST["cmd"];
	
	if($cmd == "ADD")
	{
			$title = $_REQUEST["title"];
			$insertSql = "insert into ".TBL_CATEGORY."(description) values('$title')";
			insertInto($insertSql);
			$success = true;
			$msg = "Category with '$title' has been created.";
			
			$title = "";
			$cmd = "";
	}
	
	
	if($cmd == "UPDATE")
	{
		if(!empty($_REQUEST["id"]))
		{
			$updateSql = "update ".TBL_CATEGORY." set description='".$_REQUEST["title"]."' where id=".$_REQUEST["id"];
			update($updateSql);
			$success = true;
			$msg = "Category with '".$_REQUEST["title"]."' has been updated.";
			$cmd = "";
			$title = "";
		}
		else
		{
			$success = false;
			$msg = "Invalid request command.";
		}
	}
	
	if($cmd == "EDIT")
	{
		if(!empty($_REQUEST["id"]))
		{
			$editData = selectFrom("select description from ".TBL_CATEGORY." where id=".$_REQUEST["id"]);
			$title = $editData["description"];
			$cmd = "UPDATE";
		}
		else
		{
			$success = false;
			$msg = "Invalid request command.";
		}
	}
	
	if($cmd == "DEL")
	{
		if(!empty($_REQUEST["id"]))
		{
			$editData = "delete from ".TBL_CATEGORY." where id=".$_REQUEST["id"];
			$result = mysql_query($editData) or die(mysql_error());
			$cmd = "";
			$success = true;
			$msg = "Record has been deleted.";
		}
		else
		{
			$success = false;
			$msg = "Invalid request command.";
		}
	}
	
	if(empty($cmd))
		$cmd = "ADD";


?>
<html>
<head>
	<title>Add / Edit Categories</title>
	<script language="javascript" src="./javascript/functions.js"></script>
	<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
		function checkForm(theForm) 
		{
			if(document.getElementById("title").value == "")
			{
		    	alert("Please enter the document category title.");
		      document.getElementById("title").focus();
		      return false;
		  }
			return true;
		} 
	-->
	</script>
	<style>
		a {
		font-family:Arial, Helvetica, sans-serif;
		color:#666666;
		font-weight:bold;
		text-decoration:none;
		}
		a.hover{
		text-decoration:underline;
		}
	</style>
</head>
<body align="center">
	<table width="80%" border="0" cellspacing="1" cellpadding="5" align="center">
		<tr>
		    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Add / Edit Document Category</font></strong></td>
		</tr>
		<form name="theForm" action="documentCategories.php" method="post" onSubmit="return checkForm(this);">
			<tr>
	      <td align="center"> 
	      	<table width="686" border="0" cellspacing="1" cellpadding="2" align="center">
	          <? if (!empty($msg)){ ?>
		          <tr> 
		            <td colspan="2" bgcolor="#EEEEEE">
									<table width="100%" cellpadding="5" cellspacing="0" border="0">
		                <tr bgcolor="#cdcdcd">
		                  <td width="40" align="center">
		                  	<font size="5" color="<? echo ($success  ? SUCCESS_COLOR : CAUTION_COLOR); ?>">
		                  		<b><i><? echo ($success ? SUCCESS_MARK : CAUTION_MARK);?></i></b>
		                  	</font>
		                  </td>
		                  <td>
		                  	<? 
		                  		echo "<font color='" . ($success ? SUCCESS_COLOR : CAUTION_COLOR) . "'>
		                  		<b>$msg</b><br><br></font>"; 
		                  	?>
		                  </td>
		                </tr>
		              </table>
		            </td>
		          </tr>
	          <? } ?>
						<tr bgcolor="#ededed"> 
	            <td width="17%" height="20" align="right"><font color="#005b90"><b>Category Title</b></font></td>
	            <td width="26%">
	            	<input name="title" id="title" size="30" style="WIDTH: 140px;HEIGHT: 18px;" value="<?=$title?>" />
	            </td>
	        </tr>
	      </table>
			</td>
		</tr>
	  <tr> 
	    <td colspan="2" align="center" bgcolor="#ededed"> 
	      <input name="cmd" type="hidden" value="<?=$cmd?>" />
	      <input name="id" type="hidden" value="<?=$_REQUEST["id"]?>" />
	      <input name="Save" type="submit" value=" Save ">
	      &nbsp;&nbsp; 
	      <input name="reset" type="reset" value=" Clear "> 
	    </td>
  	</tr>
	</form>
	</table>
	<table width="85%" border="0" cellspacing="4" cellpadding="5" align="center">
		<tr>
	    <td colspan="2" bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Document Categories List</font></strong></td>
		</tr>
		<tr>
				<th width="75%" align="left">Document Category Title</th>
				<th width="25%" align="center">Actions</th>
		</tr>
		<?
			$selectSql = "select * from ".TBL_CATEGORY." order by description asc";
			$categoriesData = selectMultiRecords($selectSql);
			for($i=0 ; $i < count($categoriesData); $i++)
			{
		?>
		<tr bgcolor="#<?=($i%2==0?"ffffff":"cccccc")?>">
				<td width="75%" align="left"><?=$categoriesData[$i]["description"]?></th>
				<td width="25%" align="center">
					<a href="?cmd=EDIT&id=<?=$categoriesData[$i]["id"]?>">EDIT</a>
					&nbsp;|&nbsp;
					<a href="?cmd=DEL&id=<?=$categoriesData[$i]["id"]?>">DELETE</a>
				</td>
		</tr>
		<?
			}
		?>
	</table>
</body>
</html>
