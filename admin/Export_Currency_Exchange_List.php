<?php
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
include("connectOtherDataBase.php");
$username  = $_SESSION["loggedUserData"]["username"];
$date_time = date('d-m-Y-h-i-s-A');
$fileName = trim($username.$date_time);
$cdate = date("d-m-y");

$fileNamed = "Currency_Exchange_Stock_".$username."_".$date_time;


	header ("Content-type: application/x-msexcel"); 
	header ("Content-Disposition: attachment; filename=$fileNamed.xls" ); 
	header ("Content-Description: PHP/INTERBASE Generated Data" );

	$arrAllCurrenyStockData = selectMultiRecords("Select * from ".TBL_CURRENCY_STOCK_LIST." order by currency asc");
	
	
	$data = "<table width='800' border='1' cellspacing='0' cellpadding='0' bordercolor='#000000'>"; 
 	 	 	 	 	 	 	 	
	$data .="<tr align='center'>
			<td><font face='Verdana' color='#000000' size='2'><b>Currency</b></font></td>
			<td><font face='Verdana' color='#000000' size='2'><b>Symbol</font></b></td>
			<td><font face='Verdana' color='#000000' size='2'><b>Amount</font></b></td>
			<td><font face='Verdana' color='#000000' size='2'><b>System Buying Rate</font></b></td>
			<td><font face='Verdana' color='#000000' size='2'><b>GBP Value</font></b></td>
			<td><font face='Verdana' color='#000000' size='2'><b>Created By</font></b></td>
			<td><font face='Verdana' color='#000000' size='2'><b>Created On</font></b></td>
			<td><font face='Verdana' color='#000000' size='2'><b>Updated By</font></b></td>
			<td><font face='Verdana' color='#000000' size='2'><b>Updated On</font></b></td>
			</tr>";
	$count=1;
	$total=0;
	$i=0;
	foreach($arrAllCurrenyStockData as $currVal)
	{
		$strCurrency = $currVal["currency"];
		$strCurrencySymbol = $currVal["currencySymbol"];
		$strAmount = customNumberFormat($currVal["amount"],2,true);
		$arrCreatedUserData = explode("|",$currVal["addedBy"]);
		$strCreatedUserName = strtoupper($arrCreatedUserData[0]);
		$strCreatedOn = date("d/m/Y",strtotime($currVal["created_on"]));
		$arrUpdatedUserData = explode("|",$currVal["updatedBy"]);
		$strUpdatedUserName = strtoupper($arrUpdatedUserData[0]);
		$strUpdatedOn = date("d/m/Y H:i:s",strtotime($currVal["updated_on"]));
		$cur_sql="select cID from currencies where currencyName ='$strCurrency'";
		$buying_rate_arr_id = selectFrom($cur_sql); 
		$cur_sql="select buying_rate from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."' and created_at = (select max(created_at) from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."')";
		$buying_rate_arr = selectFrom($cur_sql); 
		$buying_rate=number_format($buying_rate_arr[0],4,'.','');
		$GBP_value =number_format($strAmount/$buying_rate,4,'.',NULL);
/*	while($contentTrans = mysql_fetch_array($result))
	{*/
		
			

						
		$data .= " <tr align='center'>
		
		<td><font color='#000000'  size='2' face='Verdana'>$strCurrency</font></td>
		<td><font color='#000000'  size='2' face='Verdana'>$strCurrencySymbol</font></td>
		<td style='mso-number-format:0\.00'><font color='#000000'  size='2' face='Verdana'>$strAmount</font></td>
		<td style='mso-number-format:0\.0000'><font color='#000000'  size='2' face='Verdana'>$buying_rate</font></td>
		<td style='mso-number-format:0\.0000'><font color='#000000'  size='2' face='Verdana'>$GBP_value</font></td>
		<td><font color='#000000'  size='2' face='Verdana'>$strCreatedUserName</font></td>
		<td><font color='#000000'  size='2' face='Verdana'>$strCreatedOn</font></td>
		<td><font color='#000000'  size='2' face='Verdana'>$strUpdatedUserName</font></td>
		<td><font color='#000000'  size='2' face='Verdana'>$strUpdatedOn GMT</font></td></tr>";
		$total += $strAmount/$buying_rate;
		$count++;
	}
	$data .= " <tr>
		
		<td align='center'><font color='#000000'  size='2' face='Verdana'></font></td>
		<td align='left'><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td>
		<td align='center'><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td></tr>";
	$data .= " <tr>
		
		<td align='center'><font color='#000000'  size='2' face='Verdana'></font></td>
		<td align='left'><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td></tr>";
			
	$data .= " <tr>
		
		<td align='center'><font color='#000000'  size='2' face='Verdana'></font></td>
		<td align='left'><font color='#000000'  size='2' face='Verdana'></font></td>
		<td align='center' colspan=2><font color='#000000'  size='2' face='Verdana'><b>Total GBP Value</b></font></td>
		<td align='right' style='mso-number-format:0\.0000'><font color='#000000'  size='2' face='Verdana'><b>".number_format($total,4,'.',',')."</b></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td>
		<td><font color='#000000'  size='2' face='Verdana'></font></td></tr>";
	if(empty($arrAllCurrenyStockData)){
	
/*		$data .= " <tr>
		
		<td align='center'><font color='#000000'  size='1' face='Verdana'>$refNumberIM</font></td>
		<td align='left'><font color='#000000'  size='1' face='Verdana'>$transDate</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>$sql</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>".print_r($arrBanksList[0])."</font></td>
		<td align='center'><font color='#000000'  size='1' face='Verdana'></font></td>
		<td><font color='#000000'  size='1' face='Verdana'>$customerAddress</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>$placeOfBirth</font></td></tr>";*/
	$data .= " <tr>
		
		<td align='center' colspan=7 size='3'><font color='red'  size='1' face='Verdana'>No data found for Currency Exchange Stock.</font></td></tr>";
	}
	$data.="</table>";
	
	echo $data;


?>