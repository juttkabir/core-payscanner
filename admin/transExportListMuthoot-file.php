<?php

	session_start();
	
	include ("../include/config.php");
	include ("security.php");

$filters = "";
$agentName = $_REQUEST["agentName"];
/**
 * Date filter
 */
$fromDate = $_REQUEST["fromDate"];
$toDate = $_REQUEST["toDate"];
$transType = "Bank Transfer";
$exportType = $_REQUEST["exportType"];
if($_REQUEST["searchTrans"] == "old")
	$strTransAge = " AND tr.dispatchNumber != '0'";
else
	$strTransAge = " AND tr.dispatchNumber = '0' ";
if($exportType!=""){
	$filters .= " (tr.transDate BETWEEN '$fromDate 00:00:00' AND '$toDate 23:59:59')";
	$filters .= " AND transType='$transType'";
	if($agentName!="" && $agentName!="all")	
		$filters .= " AND custAgentID='$agentName' ";
	$sql = "select 
					tbd.bankName, 
					tbd.branchAddress,
					tr.transID, 
					tr.refNumberIM, 
					tr.benID, 
					tr.transDate,
					tr.localAmount,
					tr.custAgentID,
					tr.transType,
					ben.firstName as fnb,
					ben.middleName as fnm, 
					ben.lastName fnl,
					ben.Country as ctb,
					ben.Phone phb,
					ben.IDType,
					ben.IDNumber,
					ben.userType,
					ben.CPF,
					cust.firstName as fnc,
					cust.middleName as mnc, 
					cust.lastName as lnc,
					cust.profession as occc,
					cust.Address as addc,
					cust.Zip as zipc,
					cust.Phone as phc,
					cust.dob as dobc,
					tbd.accNo,
					tbd.accountType,
					tbd.branchCode
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben,
					customer cust,
					admin ad
				where
					tbd.transID = tr.transID and
					tr.benID = ben.benID and
					tr.custAgentID = ad.userID and
					tr.customerID  = cust.customerID and 
					(tr.transStatus  = 'Authorize' || tr.transStatus  = 'Amended') and 
					". $filters ." 
					".$strTransAge." 
				order by 
					tbd.bankName asc
					";
}
$amountUnderOneBank = 0;
$totalAmount = 0;
$totalTrasactions = 0;

if(!empty($sql))
	$fullRS = SelectMultiRecords($sql);

	if(count($fullRS)>0){
		$arrIds = array();
		for($s=0;$s<count($fullRS);$s++)
		{
			$arrIds[] = $fullRS[$s]["transID"];
		}
		if(count($arrIds) > 0)
		{
			$strDispatchNumSql = "select Max(dispatchNumber) as mdn from transactions";
			$arrDispatchNumData = selectFrom($strDispatchNumSql);
			$intDispatchNumber = $arrDispatchNumData["mdn"];
			$intDispatchNumber++;
			
			$strSqlSer = addslashes($sql);
			
			$strDispatchSql = "insert into dispatch_book 
									(dispatchNumber, created, reportQuery)
								values
									('".$intDispatchNumber."','".date("Y-m-d h:i:s")."','".$strSqlSer."')";

			insertInto($strDispatchSql);
			$intDispatchId = mysql_insert_id();

			foreach($arrIds as $key => $val)
			{
				if(!empty($val))
				{
					$intDistributorTransactionNumber++;
					$strDispatchNumUpdateSql = "update transactions 
												set 
													dispatchNumber='".$intDispatchNumber."', 
													dispatchDate='".date("Y-m-d")."'
												where transID='".$val."'";
					update($strDispatchNumUpdateSql);
				}
			}
		}	
	}
	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	if($exportType == "xls")
	{
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=bankTransactionsMT-".time().".xls"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
	}
	elseif($exportType == "csv")
	{

		header("Content-Disposition: attachment; filename=bankTransactionsMT-".time().".csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "";
		$strColumnEnd   = ",";

	}
	else
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Bank Transactions</title>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;

	$strFullHtml .= $strMainStart;
	$strFullHtml .= $strColumnStart.$strBoldStart."Bank Transactions 'Our A/C No.: 10757001 Sort Code: 60-01-59'".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strRowEnd;
		
	$strFullHtml .= $strMainStart;
	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStart.$strBoldStart."TRANS REF #".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."DATE ".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."SENDER NAME".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."OCCUPATION".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."SENDER ADDRESS".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."SENDER POST CODE".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."SENDER PHONE".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."SENDER DOB".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."BENEFICIARY NAME".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."BENEFICIARY PHONE".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."FOREIGN AMOUNT".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."BENEFICIARY COUNTRY".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."TRANSACTION TYPE".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."BANK NAME".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."ACCOUNT #".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."BRANCH ADDRESS".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strRowEnd;

	$strFullHtml .= $strRowStart;
	for($s=0;$s<count($fullRS);$s++)
	{

		$agentCodeQuery = selectFrom("Select username from admin where userID = '".$fullRS[$s]["custAgentID"]."'");
		$agentCode		= strtoupper($agentCodeQuery["username"]);


		if($exportType == "csv")
			$strFullHtml .= "";
		else
			$strFullHtml .= "<tr class='".$fullRS[$s]["status"]."'>";

		$tids .= $fullRS[$s]["transID"].",";
		
		$lastBankName = $fullRS[$s-1]["bankName"];
		$nextBankName = $fullRS[$s+1]["bankName"];
		$totalAmount += $fullRS[$s]["localAmount"];
		$totalTrasactions += 0;
		if((!empty($lastBankName) && $lastBankName != $fullRS[$s]["bankName"]) || $s == 0)
		{
			$noOfOrders = 0;
			$amountUnderOneBank = 0;
			
			$strFullHtml .= $strColumnStart.$fullRS[$s]["bankName"].$strColumnEnd;
			$strFullHtml .= $strRowEnd;
		}
		$noOfOrders += 1;
		$amountUnderOneBank += $fullRS[$s]["localAmount"];
			
		$strFullHtml .= $strColumnStart.$fullRS[$s]["refNumberIM"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$fullRS[$s]["transDate"].($exportType=="csv"?"         ":"").$strColumnEnd;

		$strFullHtml .= $strColumnStart.$fullRS[$s]["fnc"]." ".$fullRS[$s]["mnc"]." ".$fullRS[$s]["lnc"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$fullRS[$s]["occc"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$fullRS[$s]["addc"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$fullRS[$s]["zipc"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$fullRS[$s]["phc"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$fullRS[$s]["dobc"].$strColumnEnd;
		
		$strFullHtml .= $strColumnStart.$fullRS[$s]["fnb"]." ".$fullRS[$s]["mnb"]." ".$fullRS[$s]["lnb"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.(!empty($fullRS[$s]["phb"])? $fullRS[$s]["phb"] : "" ).$strColumnEnd;
		$strFullHtml .= $strColumnStart.(!empty($fullRS[$s]["localAmount"])? $fullRS[$s]["localAmount"] : " " ).$strColumnEnd;
		$strFullHtml .= $strColumnStart.(!empty($fullRS[$s]["ctb"])? $fullRS[$s]["ctb"] : "" ).$strColumnEnd;
		$strFullHtml .= $strColumnStart.$fullRS[$s]["transType"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.(!empty($fullRS[$s]["bankName"])? $fullRS[$s]["bankName"] : " " ).$strColumnEnd;
		$strFullHtml .= $strColumnStart.(!empty($fullRS[$s]["accNo"])? $fullRS[$s]["accNo"] : " " ).$strColumnEnd;
		$strFullHtml .= $strColumnStart.(!empty($fullRS[$s]["branchAddress"])? $fullRS[$s]["branchAddress"] : " " ).$strColumnEnd;
		$strFullHtml .= $strRowEnd;

		if((!empty($nextBankName) && $nextBankName != $fullRS[$s]["bankName"]) || $s == count($fullRS)-1)
		{
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."Number of Transactions:".$strColumnEnd;
			$strFullHtml .= $strColumnStart.$noOfOrders.$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."Total Bank".$strColumnEnd;
			$strFullHtml .= $strColumnStart.($exportType!="csv"? number_format($amountUnderOneBank,2,".",","):number_format($amountUnderOneBank,2,".","")).$strColumnEnd;
			$strFullHtml .= $strColumnStart." ".$strColumnEnd;
			$strFullHtml .= $strColumnStart." ".$strColumnEnd;
			$strFullHtml .= $strColumnStart." ".$strColumnEnd;
			$strFullHtml .= $strColumnStart." ".$strColumnEnd;
			$strFullHtml .= $strColumnStart." ".$strColumnEnd;
			$strFullHtml .= $strRowEnd;
		}
		
	}
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strRowEnd;


			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."Number of Transactions:".$strColumnEnd;
			$strFullHtml .= $strColumnStart.$s.$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."Foreign Total".$strColumnEnd;
			$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($totalAmount,2,".",",") : number_format($totalAmount,2,".","")).$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strRowEnd;

	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlClosing;

	echo $strFullHtml;
?>