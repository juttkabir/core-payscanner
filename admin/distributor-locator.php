<?
	session_start();
	include ("../include/config.php");
	include ("security.php");

	$agentType = getAgentType();
	$agentID = $_SESSION["loggedUserData"]["userID"];

	// If there is a search request
	if ( isset( $_REQUEST["searchSubmit"] ) )
	{
		$whereClause = "";
		$errorMessage = "";
		$searchResultInfo = "";

		$q = "select * from " . TBL_COLLECTION . $whereClause;
		$countQuery = "select count(cp_id) as numberOfRecords from " . TBL_COLLECTION . $whereClause;


		if ( !empty( $_REQUEST["searchText"] ) && !empty( $_REQUEST["filterField"] ) )
		{
			$filterField = "cp_" . $_REQUEST["filterField"];
			$whereClause .= " where " . $filterField . " like '%" . $_REQUEST["searchText"] . "%' ";
		}
		else
		{
			if ( empty( $_REQUEST["searchText"] ) )
				$errorMessage = "Please provide search text ";
			if ( empty( $_REQUEST["filterField"] ) )
				$errorMessage .= (!empty($errorMessage) ? "and select a filter type" : "Please select a filter type");

			$errorMessage .= (!empty($errorMessage) ? "<br />Showing all records" : "");
		}

		/***** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents *****/
		if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
			&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1 )
		{
			if ( strstr($agentType, "SUPA") || strstr($agentType, "SUBA") )
			{
				// Limit the collection points to associated distributors if an agent is accessing this page.
				$userID = $_SESSION["loggedUserData"]["userID"];
				if ( !empty( $userID ) )
				{
					
					// Select associated distributors
					$assocDistQuery = "select associated_distributors from admin where userID='" . $userID . "'";
					$userData = selectFrom( $assocDistQuery );
	
					if ( !empty($userData["associated_distributors"]) )
					{
						// If it is not empty, it means that there are some distributors associated with this agent.
						// So, only those collection points should be shown which are related to those associated distributors.
						// If $userData["associated_distrbutors"] is found to be empty, it means that there is no particular assoication of this
						// agent with any distributor. All the collection will be shown in this case as it will be treated as the current agent is associated with
						// all distributors.
						$assocDistIDs = explode( ",", $userData["associated_distributors"] );
						foreach( $assocDistIDs as $distID )
							$assocDistIDsClause .= "'" . $distID . "', ";
	
						$assocDistIDsClause = substr( $assocDistIDsClause, 0, strlen( $assocDistIDsClause ) - 2 );
						$whereClause .= (!empty($whereClause) ? " and " : " where ") . " cp_ida_id in(" . $assocDistIDsClause . ") ";
					}
					
				}
				
			}
		}
		/***** End of code against #3428: Connect Plus - Select Distributors for Agents *****/

		$countQuery .= $whereClause;
		$countData = selectFrom( $countQuery );

		/********** Start of code block to manage paging. **********/
		$limitPerPage = 30;
	
		// Count total number of records
			$totalRecords = $countData["numberOfRecords"];
			$searchResultInfo = $totalRecords . " result(s) found.";
	
		$totalPages = ceil( $totalRecords / $limitPerPage );
		if ($totalPages == 0)
			$totalPages = 1;
		$lastPage = $totalPages;
		$currentPage = !empty($_REQUEST["pageNo"]) ? $_REQUEST["pageNo"] : 1;
		
		$currentOffset = $_REQUEST["currentOffset"];
		$pageState = $_REQUEST["optionClicked"];
		
		if($currentPage == 1){
		$currentOffset = 0; // Its because offsetting in Database is Zero based.
	}else{
			if($pageState == "previous"){
				
						$currentOffset =$currentOffset -  $limitPerPage;
			
			}elseif($pageState == "last"){
						$lastOffset = (($totalPages -1)* $limitPerPage);
				
						$currentOffset =  $lastOffset;
				
				}else{
						$currentOffset =$currentOffset +  $limitPerPage;
			}
		}
		
		$isFirstPage = ( $currentPage <= 1 ? true : false );
		$isLastPage = ( $currentPage >= $lastPage ? true : false );
		/********** End of code block to manage paging. **********/

		 $q .= $whereClause . " limit " . $currentOffset . ", " . $limitPerPage;
		$collPoints = selectMultiRecords( $q );

		$numberOfCPs = count( $collPoints );

		for( $i = 0; $i < $numberOfCPs; $i++)
		{
			$subQuery = "select agentCompany, distAlias, username from " . TBL_ADMIN_USERS
						 . " where "
								. " userID = '" . $collPoints[$i]["cp_ida_id"] . "'";
			$distributor = selectFrom( $subQuery );
			$collPoints[$i]["agentCompany"] = $distributor["agentCompany"];
			$collPoints[$i]["distAlias"] = $distributor["distAlias"];
			$collPoints[$i]["username"] = $distributor["username"];
		}
	}
?>
<html>
<head>
	<title>Distributor Locator</title>
	<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="./javascript/functions.js"></script>

	<? include ("javaScript.php"); ?>

	<script language="javascript">
		searchText = "<?=( !empty($_REQUEST["searchText"]) ? $_REQUEST["searchText"] : "" );?>";
		fieldForSearch = "<?=( !empty($_REQUEST["filterField"]) ? $_REQUEST["filterField"] : "" );?>";

		function loadPage( requestedPage,optionClicked )
		{
			var searchFrm = document.searchForm;
			searchFrm.pageNo.value = requestedPage;
			searchFrm.optionClicked.value = optionClicked;
			searchFrm.searchText.value = searchText;
			searchFrm.filterField.value = fieldForSearch;
			
			//searchForm = document.getElementById("searchForm");
			/*
			document.getElementById("pageNo").value = requestedPage;
			document.getElementById("optionClicked").value = optionClicked;
			document.getElementById("searchText").value = searchText;
			document.getElementById("filterField").value = fieldForSearch;
			*/
			
			/*
			searchForm.pageNo.value = requestedPage;
			searchForm.optionClicked.value = optionClicked;
			searchForm.searchText.value = searchText;
			searchForm.filterField.value = fieldForSearch;
			*/
			searchForm.submit();
		}

		function selectFilterFieldByValue( val )
		{
			//document.
		}

		function CheckAll()
		{
			
		}
	</script>
<style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color:#005B90;font-weight:bold;}
-->
</style>
</head>
<body>
<table width="" border="0" cellspacing="1" cellpadding="5" align="center">
	<tr>
		<td class="topbar"><strong><font class="topbar_tex">Distributor Locator</font></strong></td>
	</tr>
	<tr>
		<td align="left"><br />
			<table border="1" cellpadding="5" bordercolor="#6699CC" align="center">
			  <tr>
				  <td nowrap bgcolor="#6699CC"><span class="tab-u"><strong>Search</strong></span></td>
				</tr>
				<tr>
				<td nowrap>
					<form id="searchForm" action="distributor-locator.php" method="get" name="searchForm">
						<?
							$filterField = "";
							if ( !empty( $_REQUEST["filterField"] ) )
								$filterField = $_REQUEST["filterField"];
						?>
						<input name="currentOffset" type="hidden" value="<?=$currentOffset ?>">
						Search Text: 
						<input name="searchText" id="searchText" type="text" value="<?=( !empty($_REQUEST["searchText"]) ? $_REQUEST["searchText"] : "" );?>">

						<select id="filterField" name="filterField" style="font-family:verdana; font-size: 11px; width:200px;">
							<option value="" >---Select Filter Type---</option>
							<option value="city" <?=($filterField == "city" ? "selected='selected'" : "" );?>>City</option>
							<option value="state" <?=($filterField == "state" ? "selected='selected'" : "" );?>>State</option>
							<option value="country" <?=($filterField == "country" ? "selected='selected'" : "" );?>>Country</option>
							<option value="phone" <?=($filterField == "phone" ? "selected='selected'" : "" );?>>Phone</option>
							<option value="branch_name" <?=($filterField == "branch_name" ? "selected='selected'" : "" );?>>Collection Point Name</option>
							<option value="branch_address" <?=($filterField == "branch_address" ? "selected='selected'" : "" );?>>Collection Point Address</option>
							<option value="contact_person_name" <?=($filterField == "contact_person_name" ? "selected='selected'" : "" );?>>Contact Person</option>
						</select>

						<input type="submit" value="Search" />
						<input type="hidden" name="searchSubmit" value="1" />
						<input type="hidden" name="pageNo" id="pageNo" value="" />
						<input type="hidden" name="optionClicked" id="optionClicked" value="" />
					</form>
				</td>
			  </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<span style="color:#333399; font-weight:bold;">
			<?
				echo $errorMessage;
				echo "<br />" . $searchResultInfo;
			?>
			</span>
		</td>
	</tr>
	<? 
		if ( !empty($collPoints) && count($collPoints) > 0 )
		{

		?>
			<tr>
				<td>
					<table width="" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
						<tr>
						  <td width="150">
							Showing page <?=$currentPage;?> of <?=$totalPages?>
						  </td>
						  <td width="50">
						  	<?
								if ( !$isFirstPage )
								{
									?>
								  		<a href="javascript:;" onClick="loadPage(1,'none');"><font color="#005b90">First</font></a>
									<?
								}
							?>
						  </td>
						  <td width="50" align="right">
						 	<?
								if ( !$isFirstPage )
								{
									?>
										<a href="javascript:;" onClick="loadPage(<?=$currentPage - 1;?>,'previous');"><font color="#005b90">Previous</font></a>
									<?
								}
							?>
						  </td>
						  <td colspan="10" width="">&nbsp;</td>
						  <td width="50" align="left">
						  	<?
								if ( !$isLastPage )
								{
									?>
										<a href="javascript:;" onClick="loadPage(<?=$currentPage + 1;?>,'next');"><font color="#005b90">Next</font></a>
									<?
								}
							?>
						  </td>
						  <td width="50" align="left">
						  	<?
								if ( !$isLastPage )
								{
									?>
										<a href="javascript:;" onClick="loadPage(<?=$lastPage;?>,'last');"><font color="#005b90">Last</font></a>
									<?
								}
							?>
						  </td>
						 <td colspan="8" width="">&nbsp;</td>
						</tr>
					  </table>
				</td>
			</tr>
			<tr>
				<td width="" height="25" nowrap bgcolor="#6699CC"><span class="tab-u"><strong>&nbsp;Search Results</span></td>
			</tr>
			<tr>
				<td nowrap bgcolor="#EFEFEF">
					<table width="" bordercolor="#EFEFEF" style="border:1px solid blue;">
						<tr bgcolor="#FFFFFF">
							<td><span class="style1"><b><strong>Collection Point Name</strong> </span></td>
							<?
								if ( !defined("CONFIG_HIDE_DISTRIBUTOR_CERTAIN_FIELDS")
									 || !strstr(CONFIG_USERS_PROHIBITTED_TO_SEE_CERTAIN_FIELDS, $agentType) )
								{
									?>
										<td><span class="style1"><strong>Distributor</strong></span></td>
									<?
								}

								if (defined("CONFIG_SHOW_DISTRIBUTOR_ALIAS_COLUMN") && CONFIG_SHOW_DISTRIBUTOR_ALIAS_COLUMN)
								{
							?>
									<td><span class="style1"><strong>Distributor Alias</strong></span></td>
							<?
								}

								if ( !defined("CONFIG_HIDE_DISTRIBUTOR_CERTAIN_FIELDS")
									 || !strstr(CONFIG_USERS_PROHIBITTED_TO_SEE_CERTAIN_FIELDS, $agentType) )
								{
									?>
										<td bgcolor="#FFFFFF"><span class="style1"><strong>Bank Name </strong></span></td>
									<?
								}
							?>
							<td bgcolor="#FFFFFF"><span class="style1"><strong>State </strong></span></td>
							<td bgcolor="#FFFFFF"><span class="style1"><strong>City</strong></span></td>
							<td><span class="style1"><strong>Address</strong></span></td>
							<td align="center"><span class="style1"><strong>Phone</strong></span></td>
						    <td align="center"><span class="style1"><strong>Status</strong></span></td>
							<?
								if ( !defined("CONFIG_HIDE_DISTRIBUTOR_CERTAIN_FIELDS")
									 || !strstr(CONFIG_USERS_PROHIBITTED_TO_SEE_CERTAIN_FIELDS, $agentType) )
								{
									?>
									    <td align="center" width="98">&nbsp;</td>
									<?
								}
							?>
						</tr>
						<?
							foreach($collPoints as $collPoint)
							{
								?>
									<tr bgcolor="#FFFFFF">
										<td width="136" bgcolor="#FFFFFF"><?=$collPoint["cp_branch_name"];?></td>
										<?
											if ( !defined("CONFIG_HIDE_DISTRIBUTOR_CERTAIN_FIELDS")
												 || !strstr(CONFIG_USERS_PROHIBITTED_TO_SEE_CERTAIN_FIELDS, $agentType) )
											{
												?>
													<td width="97" bgcolor="#FFFFFF"><?=$collPoint["agentCompany"];?> [<?=$collPoint["username"];?>]</td>
												<?
											} 

											if (defined("CONFIG_SHOW_DISTRIBUTOR_ALIAS_COLUMN") && CONFIG_SHOW_DISTRIBUTOR_ALIAS_COLUMN)
											{
										?>
												<td width="106" bgcolor="#FFFFFF"><?=$collPoint["distAlias"];?></td>
										<?
											}

											if ( !defined("CONFIG_HIDE_DISTRIBUTOR_CERTAIN_FIELDS")
												 || !strstr(CONFIG_USERS_PROHIBITTED_TO_SEE_CERTAIN_FIELDS, $agentType) )
											{
												?>
													<td width="81" bgcolor="#FFFFFF"><?=$collPoint["cp_corresspondent_name"];?></td>
												<?
											}
										?>
										<td width="81" bgcolor="#FFFFFF"><?=$collPoint["cp_state"];?></td>
										<td width="48" bgcolor="#FFFFFF"><?=$collPoint["cp_city"];?></td>
										<td width="139" bgcolor="#FFFFFF"><?=$collPoint["cp_branch_address"];?></td>
										<td align="center" bgcolor="#FFFFFF"><?=$collPoint["cp_phone"];?></td>
									    <td align="center" bgcolor="#FFFFFF"><?=$collPoint["cp_active"];?></td>
										<?
											if ( !defined("CONFIG_HIDE_DISTRIBUTOR_CERTAIN_FIELDS")
											 || !strstr(CONFIG_USERS_PROHIBITTED_TO_SEE_CERTAIN_FIELDS, $agentType) )
											{
												?>
													<td align="center" bgcolor="#FFFFFF">
														<a href="add-collectionpoint.php?page=distributor-locator.php&qs=<?=$qs;?>&agentCity=<?=$collPoint["cp_city"]?>&benCountry=<?=$collPoint["cp_country"]?>&distributorID=<? echo $collPoint["cp_ida_id"]?>&collectionPointID=<? echo $collPoint["cp_id"]?>&" class="style2">Edit</a>
														 | <a href="javascript:;" onClick="window.open('disable_collection_point.php?cp_id=<?=$collPoint["cp_id"];?>&agentCity=<?=$collPoint["cp_city"]?>&reloadOpener=1','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=200,width=460');" class="style2"><?=($collPoint["cp_active"] == "Active" ? "Disable" : "Enable" )?></a>
													</td>
												<?
											}
										?>
									</tr>
								<?
							}
						?>
					</table>
				</td>
			</tr>
			<?
		}
	?>
</table>
</body>
</html>
