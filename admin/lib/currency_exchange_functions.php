<?
session_start();
$date_time = date('d-m-Y  h:i:s A');

function currency_totals($arguments)
{
	//debug_print_backtrace();
	
	$buyAmount ='';
	$buyRate ='';
	$buyCostInGBP ='';
	$sellAmount ='';
	$sellRate ='';
	$sellCostInGBP ='';
	$pl ='';
	$margin ='';
	
	$roundRateLevel  = $arguments["roundRateLevel"];
	$roundAmountLevel  = $arguments["roundAmountLevel"];
	
	$buy_sell    = $arguments["buy_sell"];
	$totalAmount = $arguments["totalAmount"];
	$rate        = $arguments["rate"];
	$lastBuyRate = $arguments["lastBuyRate"];
	$localAmount = $arguments["localAmount"];

	$arrReturn	 = array();		
	
	if($buy_sell == 'B')
	{
		if(!empty($totalAmount))
			$buyAmount = stripslashes(number_format($totalAmount,$roundAmountLevel,'.',''));
		
		if(!empty($rate)){
			$buyRate = stripslashes(number_format($rate,$roundRateLevel,'.',''));	
		$grandBuyRate +=$buyRate;
		$buyRateCounter++;
		}	
		
		$lastBuyRate = $buyRate;	
			
		$buyCostInGBP = number_format($localAmount,$roundAmountLevel,'.','');
		$grandBuy +=$buyAmount;
		$grandBuyGBP+=$buyCostInGBP;
	}
	elseif($buy_sell == 'S')
	{
		if(!empty($totalAmount))
			$sellAmount = stripslashes(number_format($totalAmount,$roundAmountLevel,'.',''));
		
		if(!empty($rate)){
			$sellRate = stripslashes(number_format($rate,$roundRateLevel,'.',''));
			$grandSellRate +=$sellRate;
			$sellRateCounter++;
		}		
			
		$sellCostInGBP = number_format($localAmount,$roundAmountLevel,'.','');
		$grandSellGBP +=$sellCostInGBP;
		if(!empty($lastBuyRate))
		{
			//debug($sellCostInGBP .'-('.$sellAmount .'/'. $lastBuyRate.')');
			$pl = $sellCostInGBP - ($sellAmount / $lastBuyRate );
			if(!empty($pl))
				$pl = stripslashes(number_format($pl,$roundAmountLevel,'.',''));
			
			
			//debug($pl."-->".$sellCostInGBP);
			$margin = ($pl / $sellCostInGBP) * 100;
			if(!empty($margin))
				$margin = stripslashes(number_format($margin,$roundAmountLevel,'.',''));
		}
		$grandSell +=$sellAmount;
		$grandMargin +=$margin;	
		$grandProfitLoss +=$pl;
	
	}
	
	$arrReturn = array(
	"buyAmount" 	=> number_format($buyAmount,$roundAmountLevel,'.',''),
	"buyRate"		=> number_format($buyRate,$roundRateLevel,'.',''),
	"lastBuyRate"	=> number_format($lastBuyRate,$roundRateLevel,'.',''),
	"buyCostInGBP" 	=> number_format($buyCostInGBP,$roundAmountLevel,'.',''),
	"sellAmount" 	=> number_format($sellAmount,$roundAmountLevel,'.',''),
	"sellRate" 		=> number_format($sellRate,$roundRateLevel,'.',''),
	"sellCostInGBP" => number_format($sellCostInGBP,$roundAmountLevel,'.',''),
	"profit_loss" 	=> number_format($pl,$roundAmountLevel,'.',''),
	"margin" 		=> number_format($margin,$roundAmountLevel,'.','')
	);
	//debug($arrReturn);
	return $arrReturn;
}
function currency_grand_totals($currencyData,$roundAmountLevel = '2',$roundRateLevel = '4')
{
	//debug_print_backtrace();
	$grandBuy  = 0;
	$grandBuyGBP  = 0;
	$grandSell = 0;
	$grandSellGBP = 0;
	$grandBuyRate =0;
	$grandSellRate =0;
	$buyRateCounter = 0;
	$sellRateCounter = 0;
	$grandProfitLoss = 0;
	$grandMargin =0;
	$cashInHand =0;
	
	for ($i=0; $i < count($currencyData); $i++)
	{
	
	$buyAmount ='';
	$buyRate ='';
	$buyCostInGBP ='';
	$sellAmount ='';
	$sellRate ='';
	$sellCostInGBP ='';
	$pl ='';
	$margin ='';
		if($currencyData[$i]["buy_sell"] == 'B')
		{
			//debug('B='.$currencyData[$i]["totalAmount"]);
			if(!empty($currencyData[$i]["totalAmount"]))
				$buyAmount = stripslashes(number_format($currencyData[$i]["totalAmount"],$roundAmountLevel,'.',''));
			
			if(!empty($currencyData[$i]["rate"])){
				$buyRate = stripslashes(number_format($currencyData[$i]["rate"],$roundRateLevel,'.',''));	
			$grandBuyRate +=$buyRate;
			$buyRateCounter++;
			}	
			
			$lastBuyRate = $buyRate;	
				
			$buyCostInGBP = number_format($currencyData[$i]["localAmount"],$roundAmountLevel,'.','');
			$grandBuy +=$buyAmount;
			$grandBuyGBP+=$buyCostInGBP;
		}
		elseif($currencyData[$i]["buy_sell"] == 'S')
		{
			//debug('S='.$currencyData[$i]["totalAmount"]);
			if(!empty($currencyData[$i]["totalAmount"]))
				$sellAmount = stripslashes(number_format($currencyData[$i]["totalAmount"],$roundAmountLevel,'.',''));
			
			if(!empty($currencyData[$i]["rate"])){
				$sellRate = stripslashes(number_format($currencyData[$i]["rate"],$roundRateLevel,'.',''));
				$grandSellRate +=$sellRate;
				$sellRateCounter++;
			}		
				
			$sellCostInGBP = number_format($currencyData[$i]["localAmount"],$roundAmountLevel,'.','');
			$grandSellGBP +=$sellCostInGBP;
			if(!empty($lastBuyRate))
			{
				$pl = $sellCostInGBP - ($sellAmount / $lastBuyRate );
				if(!empty($pl))
					$pl = stripslashes(number_format($pl,$roundAmountLevel,'.',''));
				
				
				//debug($pl."-->".$sellCostInGBP);
				$margin = ($pl / $sellCostInGBP) * 100;
				if(!empty($margin))
					$margin = stripslashes(number_format($margin,$roundAmountLevel,'.',''));
			}
			$grandSell +=number_format($sellAmount,$roundAmountLevel,'.','');
			$grandMargin +=number_format($margin,$roundAmountLevel,'.','');
			$grandProfitLoss +=number_format($pl,$roundAmountLevel,'.','');
		}
	} //end for loop
	
	
	if(!empty($grandBuyRate) && !empty($buyRateCounter))
	{
		$averageBuyRate = $grandBuyRate / $buyRateCounter;
		$averageBuyRate = stripslashes(number_format($averageBuyRate,$roundRateLevel,'.',''));
	}
	if(!empty($grandSellRate) && !empty($sellRateCounter))
	{
		$averageSellRate = $grandSellRate / $sellRateCounter;
		$averageSellRate = stripslashes(number_format($averageSellRate,$roundRateLevel,'.',''));
	}
	$cashInHand = $grandBuy - $grandSell;
	
	$arrReturn = array(
	"grandBuy" 			=> number_format($grandBuy,$roundAmountLevel,'.',','),
	"averageBuyRate" 	=> number_format($averageBuyRate,$roundRateLevel,'.',','),
	"grandBuyGBP" 		=> number_format($grandBuyGBP,$roundAmountLevel,'.',','),
	"grandSell"			=> number_format($grandSell,$roundAmountLevel,'.',','),
	"averageSellRate"   => number_format($averageSellRate,$roundRateLevel,'.',','),
	"grandSellGBP"		=> number_format($grandSellGBP,$roundAmountLevel,'.',','),
	"grandProfitLoss"	=> number_format($grandProfitLoss,$roundAmountLevel,'.',','),
	"grandMargin"		=> number_format($grandMargin,$roundAmountLevel,'.',','),
	"cashInHand"		=> number_format($cashInHand,$roundAmountLevel,'.',',')
	);
	//debug($arrReturn);
	
	return $arrReturn;
}


?>