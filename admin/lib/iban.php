<?php

/**
 * To check the validity of the IBAN number based on alogorithm 
 * can be found at http://www.europebanks.info/ibanguide.htm#6
 * Further examples of european valid IBAN numbers are to be found at
 * http://www.europebanks.info/ibanguide.htm
 */

function validateIban($ibanToValidate) {

    /**
     * IBAN FORMAT
     * DE89 37040044 0532013000
     *
     * Country Code: DE 
     * Control Digit: 89 
     * Bank Code: 37040044 
     * Account number: 0532013000 
     */
     
    $iban = $ibanToValidate;
    $iban = str_replace(" ","",$iban);
    
    /**
     * IBAN length depends upon the country. 
     * NOTE: there is no need to pad account with zeros. It should 
     * already have been done in the generation of the IBAN
     */

    $countryLength = array(
        "AD" => 24, /* Andorra */
        "AT" => 20, /* Austria */
        "BE" => 16, /* Belgium */
        "CY" => 28, /* Cyprus */
        "CZ" => 24, /* Czech Republic */
        "DK" => 18, /* Denmark */
        "EE" => 20, /* Estonia */
        "FI" => 18, /* Finland */
        "FR" => 27, /* France */
        "DE" => 22, /* Germany */
        "GI" => 23, /* Gibraltar */
        "GR" => 27, /* Greece */
        "HU" => 28, /* Hungary */
        "IS" => 26, /* Iceland */
        "IE" => 22, /* Ireland */
        "IT" => 27, /* Italy */
        "LV" => 21, /* Latvia */
        "LT" => 20, /* Lithuania */
        "LU" => 20, /* Luxembourg */
        "NL" => 18, /* Netherlands*/
        "NO" => 15, /* Norway*/
        "PL" => 28, /* Poland */
        "PT" => 25, /* Portugal */
        "SK" => 24, /* Slovak Republic */
        "SI" => 19, /* Slovenia */
        "ES" => 24, /* Spain */
        "SE" => 24, /* Sweden */
        "CH" => 21, /* Switzerland */
        "GB" => 22, /* United Kingdom */
    );

    $ibanCharReplace = array(
        "A" => 10,
        "B" => 11,
        "C" => 12,
        "D" => 13,
        "E" => 14,
        "F" => 15,
        "G" => 16,
        "H" => 17,
        "I" => 18,
        "J" => 19,
        "K" => 20,
        "L" => 21,
        "M" => 22,
        "N" => 23,
        "O" => 24,
        "P" => 25,
        "Q" => 26,
        "R" => 27,
        "S" => 28,
        "T" => 29,
        "U" => 30,
        "V" => 31,
        "W" => 32,
        "X" => 33,
        "Y" => 34,
        "Z" => 35 );

    /* If the country code is not one we recognise */
    $countryCode = substr($iban,0,2);
    if (!$countryCode[$countryLength]) {
        return false;
    }

    /* If the IBAN string is not correct for the country */
    if (strlen($iban) != $countryLength[$countryCode]) {
        return false;
    }

    $lead4 = substr($iban,0,4);
    $tail = substr($iban,4);
    $newIban = $tail.$lead4;

    $numericIban = '';
    for ($i=0; $i<strlen($newIban); $i++) {
        $char = $newIban[$i];
        if ($ibanCharReplace[strtoupper($char)]) {
            $numericIban .= $ibanCharReplace[strtoupper($char)];
        }
        else {
            $numericIban .= $char;
        }
    }

    /* Strip leading zero's from the numericIban */
    while ($numericIban[0] == 0) {
        $numericIban = substr($numericIban,1);
    }

    $ibanNum = gmp_mod($numericIban , 97);
    /**
     * If mod is 1 then the given IBAN is valid
     */
    if ( $ibanNum == 1 ) {
        return true;
    }
    else {
        return false;
    }
}

?>
