<?php 
	require_once('config/lang/eng.php');
	require_once('tcpdf.php');
	
	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator('Mirza Arslan Baig');
	
	$pdf->SetAuthor('Mirza Arslan Baig');
	$pdf->SetTitle('Premier Exchange Deal Contract');
	$pdf->SetSubject('Deal Contract');
	
	// set default header data
	//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 014', PDF_HEADER_STRING);
	
	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	//set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	
	//set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
	// IMPORTANT: disable font subsetting to allow users editing the document
	$pdf->setFontSubsetting(false);
	
	// set font
	$pdf->SetFont('helvetica', '', 10, '', false);

	// add a page
	$pdf->AddPage();
	
	// set default form properties
	$pdf->setFormDefaultProp(array('lineWidth'=>1, 'borderStyle'=>'solid', 'fillColor'=>array(255, 255, 200), 'strokeColor'=>array(255, 128, 128)));

	$pdf->SetFont('helvetica', 'BI', 18);
	$pdf->Cell(0, 5, 'Form Practice By Mirza Arslan Baig', 0, 1, 'C');
	$pdf->Ln(10);

	$pdf->SetFont('helvetica', '', 12);
	
	$pdf->Cell(35, 5, 'First name:');
	$pdf->TextField('firstname', 50, 5);
	$pdf->Ln(6);
	
	// Button to validate and print
	$pdf->Button('print', 30, 10, 'Print', 'Print()', array('lineWidth'=>2, 'borderStyle'=>'beveled', 'fillColor'=>array(128, 196, 255), 'strokeColor'=>array(64, 64, 64)));

	// Reset Button
	$pdf->Button('reset', 30, 10, 'Reset', array('S'=>'ResetForm'), array('lineWidth'=>2, 'borderStyle'=>'beveled', 'fillColor'=>array(128, 196, 255), 'strokeColor'=>array(64, 64, 64)));

	// Submit Button
	$pdf->Button('submit', 30, 10, 'Submit', array('S'=>'SubmitForm', 'F'=>'http://localhost/test/pdf_test/tcpdf/printvars.php', 'Flags'=>array('ExportFormat')), array('lineWidth'=>2, 'borderStyle'=>'beveled', 'fillColor'=>array(128, 196, 255), 'strokeColor'=>array(64, 64, 64)));
	
	$pdf->Output('form_test.pdf', 'I');
?>