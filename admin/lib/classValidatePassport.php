<?php
	/* Class: passportCheck
	 * created on: 27th May 2009
	 * Author: Niaz Ahmad
	 * Filename: passportValidate.class.php
	 *
	 * UK Passport Check
	 * Validates checksums and calculates birthdaty, expiration date and age
	 * Stores last check results
	 *
	 * Update 27th May 2009: UNIX-Timestamp only works with Dates beginning with the 1st Jan 1970
	 *			to get the birthdate in that case, use the byear, bmonth, bday fields of the result array
	 *
	 * This class is freeware and comes with no warranty at all.
	 * Please leave the copyright note unchanged.
	 */

class passportCheck {
	var $lastresult = array();

	function pak_check($strPass) {
		/* Check the numbers in $aArr and returns $res - Array
		 * Params:	$aArr[0] : (int) with 10 digits - living area (4) + serial (5) + checksum (1)
		 *			$aArr[1] : (int) with 7 digits - Birthdate (yymmdd) + checksum (1)
		 *			$aArr[2] : (int) with 7 digits - Valid until date (yymmdd) + checksum (1)
		 *			$aArr[3] : (int) with 1 digit - overall checksum (1)
		 * Result:	$res["valid"] : (bool)
		 *			$res["birthday"] : (int) - UNIX Timestamp of birthdate or -1 if birthyear < 1970
		 *			$res["byear"] : (int) - Year of birth
		 *			$res["bmonth"] : (int) - Month of birth
		 *			$res["bday"] : (int) - Day of birth
		 *			$res["age"] : (int) - todays age from birthdate
		 *			$res["validto"] : (int) - UNIX Timestamp of valid until date
		 *			$res["error"] : (int) - 0 (everything okay),
		 *									-1 (expired, Data available),
		 *									 1 (Code or checksum wrong, no data available) 
		 */
		$strPass = trim($strPass);
		$CodeA = substr($strPass, 0, 10); // get Passport Number first 10 digit
		//$CodeB = substr($strPass, 13, 7); // get Date of Birth from 13 to 19 digit
		//$CodeC = substr($strPass, 21, 7); // get Expiry Date from 21 to 27 digit
		//$CodeD = substr($strPass, 42, 2); // get Final Check Digit 21 to 27 digit
		
		/*debug($CodeA);
		debug($CodeB);
		debug($CodeC);
		debug($CodeD);
		debug(strlen($CodeA));
		debug(strlen($CodeB));
		debug(strlen($CodeC));
		debug(strlen($CodeD));*/
		
		$res = array("valid" => false, "birthday" => 0, "age" => "", "validto" => 0, "error" => 1);
		/*$CodeA = trim($aArr[0]);
		$CodeB = trim($aArr[1]);
		$CodeC = trim($aArr[2]);
		$CodeD = trim($aArr[3]);
		if (strlen($CodeA) > 10) $CodeA = substr($CodeA, 0, 10);
		if (strlen($CodeB) > 7) $CodeB = substr($CodeB, 0, 7);
		if (strlen($CodeC) > 7) $CodeC = substr($CodeC, 0, 7);
		if (strlen($CodeD) > 1) $CodeD = substr($CodeD, 0, 1);*/
		
		if ((strlen($CodeA) != 10)) return $res;
		/*if ((strlen($CodeA) != 10) || (strlen($CodeB) != 7) || 
			(strlen($CodeC) != 7)) return $res;	*/
		
		if ($this->pak_getchecksum(substr($CodeA,0,-1)) != substr($CodeA, -1))return $res;
		/*if ($this->pak_getchecksum(substr($CodeB,0,-1)) != substr($CodeB, -1)) return $res;
		if ($this->pak_getchecksum(substr($CodeC,0,-1)) != substr($CodeC, -1)) return $res;
		if ($this->pak_getchecksum($CodeA.$CodeB.$CodeC) != $CodeD) return $res;*/
	
		$res['valid'] = true;
		
	/*	$year = (int)(substr($CodeB,0,2));
		if (substr($CodeB,0,6) <= strftime("%y%m%d")) $year += 2000;
		else $year += 1900;
		$res['byear'] = $year;
		$res['bmonth'] = (int)(substr($CodeB,2,2));
		$res['bday'] = (int)(substr($CodeB,4,2));
		if ($year >= 1970) $res['birthday'] = mktime(0,0,0,$res['bmonth'], $res['bday'], $year);
		else $res['birthday'] = -1;
		$res['validto'] = mktime(0,0,0,(int)(substr($CodeC,2,2)), (int)(substr($CodeC,4,2)), (int)(substr($CodeC,0,2)));
		$res['age'] = date('Y')-$year;
		if (date('m') < $res['bmonth']) $res['age']--;
		else if ((date('m') == $res['bmonth']) && (date('d') < $res['bday'])) $res['age']--;
		if (time() > ($res['validto'] + (60*60*24))) {
			$res['valid'] = false;
			$res['error'] = -1;
		} else $res['error'] = 0;*/
		$this->lastresult = $res;
		return $res;
	}

	// private function
	function pak_getchecksum($aCode) {
		// Calculate and return the checksum
		//debug($aCode);
		$multi = array(7,3,1);
		$i = 0;
		$sum = 0;
		for ($c = 0; $c < strlen($aCode); $c++) {
			$sum += $multi[$i] * (int)(substr($aCode,$c,1));
			//debug($sum);
			$i++;
			if ($i > 2) $i = 0;
		}
		$res = $sum%10;
		//debug($res);
		return ($sum%10);
	}

}
?>