<?php
//include_once($_SERVER['DOCUMENT_ROOT']."/compliance/server.php");
/**
 * This function will check the basic compliance checks on a transaction with help of amount and personas
 * name validation functions.
 * author Niaz Ahmad
 *  
 */
	function complainceVerify($arrParams)
	{ 
		$returnFlag = false;
		//debug($arrParams);
		if(!empty($arrParams['transid']))
		{ 
			$arrAmountReturn = amountRules($arrParams);
			$arrUserReturn = nameRules($arrParams);
		
			//debug($arrAmountReturn);
			//debug($arrUserReturn);
			
			$arrFinalResult = array();
			foreach($arrAmountReturn as $key => $val)
					$arrFinalResult[$key] = $val;
			
			foreach($arrUserReturn as $key => $val)
					$arrFinalResult[$key] = $val;		
					
			//debug($arrFinalResult,true);
			$transData = selectFrom("SELECT transStatus,customerID,benID FROM transactions WHERE transID = '".$arrParams['transid']."'");
			//debug($transData);
	
			$strMessage = 'Transaction is held due to ';
			$holdFlag = false;
			$autoBlockCustFlag = false;
			$autoBlockBenFlag = false;
			$strCustBlockMessage = 'Customer is Blocked due to ';
			$strBenBlockMessage  = 'Beneficiary is Blocked due to ';
			//debug($arrFinalResult,true);
			foreach($arrFinalResult as $key => $val)
			{
				$strMessage .=$val["M"]."<br>";
				if($val["S"] == 'Y')
					$holdFlag = true;
				
				if($val["B"] == 'Y')
				{
					if($val["U"] == 'sender')
					{
						$autoBlockCustFlag = true;
						$strCustBlockMessage .=$val["M"]."<br>";
					}
					elseif($val["U"] == 'beneficiary')
					{
						$autoBlockBenFlag = true;
						$strBenBlockMessage .=$val["M"]."<br>";
					}
				}
					
			}
			//debug($strMessage);
			
			if($autoBlockCustFlag)
			{
				$sql = "update 
							".TBL_CUSTOMER."
							set
								customerStatus = 'Disable',
								compliance_note = '".$strCustBlockMessage."'
							where
								customerID = '".$transData["customerID"]."'	
							";
					//debug($sql);
					 if(update($sql))
					 {
						activities($_SESSION["loginHistoryID"],"UPDATION",$transData["customerID"],TBL_CUSTOMER,$strCustBlockMessage);
						//$returnFlag = true;
					 }		
			}
			
			if($autoBlockBenFlag)
			{
				$sql = "update 
							".TBL_BENEFICIARY."
							set
								status = 'Disable',
								beneficiaryStatus = 'Disable',
								compliance_note = '".$strBenBlockMessage."'
							where
								benID = '".$transData["benID"]."'	
							";
					//debug($sql);
					 if(update($sql))
					 {
						activities($_SESSION["loginHistoryID"],"UPDATION",$transData["benID"],TBL_BENEFICIARY,$strBenBlockMessage);
						//$returnFlag = true;
					 }		
			}
			if($holdFlag)
			{
				$strUpdate = "update 
									". TBL_TRANSACTIONS." 
								set 
									transStatus = 'Hold',
									hold_trans_status = '".$transData["transStatus"]."',
									holdedBy = '".$_SESSION["loggedUserData"]["userID"]."',
									held_reason = '".$strMessage."',
									holdDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."'
								WHERE 
									transID='".$arrParams['transid']."'";
				//debug($strUpdate,true);
				 if(update($strUpdate))
				 {
					activities($_SESSION["loginHistoryID"],"UPDATION",$arrParams['transid'],TBL_TRANSACTIONS,$strMessage);
					$returnFlag = true;
				 }
			}
			//debug($returnFlag,true);
			 return $returnFlag;
		}	
		
		else
		{
			if($arrParams["ruleFor"] == 'customer' || $arrParams["ruleFor"] == 'beneficiary'){
				$arrUserReturn = nameRules($arrParams);
				return $arrUserReturn;
			}elseif($arrParams["ruleFor"] == 'amount'){
				$arrAmountReturn = amountRules($arrParams);
				return $arrAmountReturn;
			}
			//return $arrUserReturn;
		}
	   //debug($returnFlag,true);
	   //return $returnFlag;
	}
	
	function amountRules($arrParams)
	{
		//check on transaction type  

		$agentType = getAgentType();
		$callFrom  = $arrParams["callFrom"];
		$action    = trim($arrParams["action"]);

		
		if(empty($callFrom))
			$callFrom = 'import';	
		
		if(empty($action))
			$action = 'send';	
		
		$extraFields = '';
		$condition = '';
		$condition2 = '';
		$condition3 = '';
		$conditionTransType ='';
		$headerFlag = true; 
	
		$beneficiaryStructureFlag  = true;
		
		
		if(!empty($arrParams['transid']))
		{
			$strDataCollectSql = "
								 SELECT 
										transID, 
										customerID,
										benID, 
										transType,
										transAmount, 
										localAmount, 
										totalAmount, 
										transStatus,
										fromCountry AS senderCountry, 
										fromCountry AS origenCountry, 
										toCountry AS destCountry, 
										currencyFrom AS originCYY, 
										currencyTo AS destCYY
								 FROM 
									transactions
								 WHERE
									transid = '".$arrParams['transid']."'
								";
		
			$arrOrderData = selectFrom($strDataCollectSql);
			//debug($arrOrderData);
			$transID 			= trim($arrOrderData["transID"]);
			$customerID 		= trim($arrOrderData["customerID"]);
			$benID 				= trim($arrOrderData["benID"]);
			$transType 			= trim($arrOrderData["transType"]);
			$transAmount 		= trim($arrOrderData["transAmount"]);
			$localAmount 		= trim($arrOrderData["localAmount"]);
			$totalAmount 		= trim($arrOrderData["totalAmount"]);
			$originCountryName 	= trim($arrOrderData["origenCountry"]);
			$destCountryName 	= trim($arrOrderData["destCountry"]);
			$originCYY 			= trim($arrOrderData["originCYY"]);
			$destCYY 			= trim($arrOrderData["destCYY"]);
			$transStatus 		= trim($arrOrderData["transStatus"]);
			
			// get origin country id agains origin country name
			if(!empty($originCountry)){
				$sqlOrigCountry = selectFrom("select countryId  from countries where countryName = '".trim($originCountryName)."'");
				$originCountryId = $sqlOrigCountry["countryId"];
				
			}	
			// get destination country id agains destination country name
			if(!empty($destCountry)){
				$sqlDestCountry = selectFrom("select countryId  from countries where countryName = '".trim($destCountryName)."'");
				$destCountryId = $sqlDestCountry["countryId"];
				
			}	
			
	    }
		else
		{
			$customerID 		= trim($arrParams["customerID"]);
			$benID 				= trim($arrParams["benID"]);
			$transType 			= trim($arrParams["transType"]);
			$transAmount 		= trim($arrParams["transAmount"]);
			$localAmount 		= trim($arrParams["localAmount"]);
			$totalAmount 		= trim($arrParams["totalAmount"]);
			$originCountryId 	= trim($arrParams["origenCountry"]);
			$destCountryId 		= trim($arrParams["destCountry"]);
			$originCYY 			= trim($arrParams["originCYY"]);
			$destCYY 			= trim($arrParams["destCYY"]);
			//debug($arrParams);
		}
		
		function getHeader()
		{
		
			$columns = NULL;
			
			$columns .= '<table align="center" cellpadding="4" cellspacing="1" width="100%" border="0" bgcolor="#FFFFFF">';
			$columns .= '<tr bgcolor="#F0F0F0">
								<td align="left" valign="middle" width="15%"><strong>Country</strong></td>
								<td align="left" valign="middle" width="15%"><strong>Amount Type</strong></td>
								<td align="left" valign="middle" width="15%"><strong>Amount</strong></td>
								<td align="left" valign="middle" width="10%"><strong>Condition</strong></td>
								<td align="left" valign="middle" width="40%"><strong>Message</strong></td>
								<td align="left" valign="middle" width="20%"><strong>Documents</strong></td>
						  </tr>';
		return $columns;
		}


		if($action == 'view')
		{
			
			$viewData = NULL;
			
			$complaincetRuleQuery = "SELECT *
										FROM ".TBL_COMPLIANCE_LOGIC." 
											WHERE 
												applyAt = 'Create Transaction' AND
												(applyTrans = 'Current Transaction' OR 	
												applyTrans = 'Accumulative Transaction')AND
												country = '".$originCountryId."' AND
												isEnable = 'Y' 
												ORDER BY ruleID DESC";
								
								//debug($complaincetRuleQuery);
			$compliancetRule = selectMultiRecords($complaincetRuleQuery);
			//debug(count($compliancetRule));
			if(count($compliancetRule)>0){
			$viewData = getHeader();
			}
			for($p=0;$p<count($compliancetRule);$p++)
			{
				if($compliancetRule[$p]["applyTrans"] == 'Current Transaction'){
					$amountType = 'Transaction Amount';
				}elseif($compliancetRule[$p]["applyTrans"] == 'Accumulative Transaction'){
					$amountType = 'Accumulative Amount';
				}	
				
						
				$strMessage 	= $compliancetRule[$p]["message"];
				$strUserType 	= $compliancetRule[$p]["userType"];
				$strMatchCriteria = $compliancetRule[$p]["matchCriteria"];
				$strAmount 		= $compliancetRule[$p]["amount"];
				
				if(!empty($compliancetRule[$p]["country"]))
					$strCountry = getCountryName($compliancetRule[$p]["country"]);
					
				$strDocument	= $compliancetRule[$p]["CTR"];
				
				if($strDocument !='N' && $strDocument !=''){
					$arrRegulator = regulatorForm($strCountry);
					if(!empty($arrRegulator)){
					$arrForm = explode("|",$arrRegulator);
					$document_name = $arrForm[0];
					$document_path = $arrForm[1];
				}
			  }
		
			   
				$viewData .= '<tr bgcolor="#F0F0F0">
								<td>'.$strCountry.'</td>
								<td>'.$amountType.'</td>
								<td>'.$strAmount.'</td>
								<td>'.$strMatchCriteria.'</td>
								<td>'.$strMessage.'</td>';
								
				$viewData .=	'<td>';
								  if($strDocument != 'N' && $strDocument != '')
				$viewData .=	    '<a href="'.$document_path.'" target="_blank"><strong>'.$document_name.'</strong></a>';
				$viewData .=  '</td>';
				
				$viewData .=   '</tr>';
			
			}
			echo $viewData;
		}
		
		
		if($action == 'send')
		{
			$strReturnResponse = NULL;
			$strCumulativeResponse = NULL;
	
		 	if($callFrom == 'import')
			{
				$condition = "isHold = 'Y' AND ";
				$condition2 = "isHold = 'Y' AND ";
			}
			
			if($transType == "Cheque Cashing" || $transType == "Currency Exchange")
					$conditionTransType = "ttype = '".$transType."' AND ";
					
			if(!empty($transType))
				$condition .= "ttype = '".$transType."' AND ";
			
			
			if(!empty($originCountryId))
					$condition .= "country = '".$originCountryId."' AND ";
			
			if(!empty($destCountryId))
					$condition .= "dest_country = '".$destCountryId."' AND ";
			
			if(!empty($originCYY))
				$condition .= "originCurrency  = '".$originCYY."' AND ";	
			
			if(!empty($destCYY))
				$condition .= "destCurrency  = '".$destCYY."' AND ";	
			
			if(!empty($destCountryId))
				$condition2 .= "dest_country  = '".$destCountryId."' AND ";
			
			
			if(!empty($originCountryId))
				$condition2 .= "(country = '".$originCountryId."' OR country = 'all') AND ";
			
			
			if(!empty($transType))
				$condition2 .= "(ttype = '$transType' OR ttype = 'all') AND ";
			
			if(!empty($originCYY))
				$condition2 .= "originCurrency  = '".$originCYY."' AND ";	
			
			if(!empty($destCYY))
				$condition2 .= "destCurrency  = '".$destCYY."' AND ";	
					
			
			$currentRuleQuery = "SELECT 
										*
								FROM ".TBL_COMPLIANCE_LOGIC." 
								WHERE 
									applyTrans = 'Current Transaction' AND
									applyAt = 'Create Transaction' AND
									".$condition."
									isEnable = 'Y' 
									ORDER BY ruleID DESC
								";
			//debug($currentRuleQuery);
			$currentRule = selectMultiRecords($currentRuleQuery);
			//debug($currentRule);
			$inputVal = array(
								"currentRule" =>$currentRule,
								"headerFlag"=>$headerFlag,
								"transAmount"=>$transAmount,
								"localAmount"=>$localAmount,
								"totalAmount"=>$totalAmount,
								"customerID"=>$customerID,
								"transID"=>$transID,
								"benID"=>$benID,
								"callFrom"=>$callFrom
							);
			$strReturnResponse = checkCurrenctRule($inputVal);
			//debug($strReturnResponse);
			/* if country base not found then find rules of where country is set empty or all in configuration*/
			if(empty($strReturnResponse))
			{
					
				$currentRuleQuery = "SELECT *
										FROM ".TBL_COMPLIANCE_LOGIC." 
										WHERE 
											applyTrans = 'Current Transaction' AND
											applyAt = 'Create Transaction' AND
											".$condition2."
											".$conditionTransType."
											isEnable = 'Y' 
											ORDER BY ruleID DESC
											";
				//debug($currentRuleQuery);
				$currentRule = selectMultiRecords($currentRuleQuery);
				//debug($currentRule);
				$inputVal = array(
									"currentRule" =>$currentRule,
									"headerFlag"=>$headerFlag,
									"transAmount"=>$transAmount,
									"localAmount"=>$localAmount,
									"totalAmount"=>$totalAmount,
									"customerID"=>$customerID,
									"transID"=>$transID,
									"benID"=>$benID,
									"callFrom"=>$callFrom
								);
				$strReturnResponse = checkCurrenctRule($inputVal);
				//debug($strReturnResponse);
			}
			//debug($currentRule);
			$complianceQuery = " SELECT 
										*
										FROM ".TBL_COMPLIANCE_LOGIC." 
										WHERE 
	
											applyTrans = 'Accumulative Transaction' AND 
											applyAt = 'Create Transaction' AND
											".$condition."
											isEnable = 'Y'
											ORDER BY ruleID DESC
											";
			//debug($complianceQuery);						
			$cumulativeRule = selectMultiRecords($complianceQuery);
			//debug($cumulativeRule);
			
			$inputVal2 = array(
								"cumulativeRule" =>$cumulativeRule,
								"headerFlag"=>$headerFlag,
								"transAmount"=>$transAmount,
								"localAmount"=>$localAmount,
								"totalAmount"=>$totalAmount,
								"beneficiaryStructureFlag"=>$beneficiaryStructureFlag,
								"customerID"=>$customerID,
								"transID"=>$transID,
								"benID"=>$benID,
								"agentType"=>$agentType,
								"callFrom"=>$callFrom
							);
			$strCumulativeResponse = checkCumulativeRule($inputVal2);
			//$strReturnResponse .= $strCumulativeResponse;
			//debug($strCumulativeResponse);
			
			if(empty($strCumulativeResponse))	
			{
				$complianceQuery = " SELECT 
											*
											FROM ".TBL_COMPLIANCE_LOGIC." 
											WHERE 
												applyTrans = 'Accumulative Transaction' and 
												applyAt = 'Create Transaction' AND
												".$condition2."
												".$conditionTransType."
												isEnable = 'Y' 
												ORDER BY ruleID DESC
												";
				//debug($complianceQuery);						
				$cumulativeRule = selectMultiRecords($complianceQuery);
				//debug($cumulativeRule);
				$inputVal2 = array(
									"cumulativeRule" =>$cumulativeRule,
									"headerFlag"=>$headerFlag,
									"transAmount"=>$transAmount,
									"localAmount"=>$localAmount,
									"totalAmount"=>$totalAmount,
									"beneficiaryStructureFlag"=>$beneficiaryStructureFlag,
									"strReturnResponse"=>$strReturnResponse,
									"customerID"=>$customerID,
									"transID"=>$transID,
									"benID"=>$benID,
									"agentType"=>$agentType,
									"callFrom"=>$callFrom
							);
				$strCumulativeResponse = checkCumulativeRule($inputVal2);
				//debug($strCumulativeResponse);
			}
	
			
			foreach($strCumulativeResponse as $key => $val)
				$strReturnResponse[$key] = $val;
			
			//debug($strReturnResponse);
			
		 return $strReturnResponse;
		
		} // end action send if condition 		
	}
	
	function getUserAccumulativeAmount($arguments)
	{
			//debug_print_backtrace();
			$userType = $arguments["userType"];
			$userId = $arguments["userId"];
			$fromDate = $arguments["fromDate"];
			$toDate = $arguments["toDate"];
			$transID = $arguments["transID"];
			$calculateAmount = $arguments["calculateAmount"];
			$calculateOn =$arguments["calculateOn"];
			$callFrom =$arguments["callFrom"];
				
			
			$beneficiaryStructureFlag = $arguments["beneficiaryStructureFlag"];
			if($userType == 'beneficiary')
				$fieldName = 'benID';
			else
				$fieldName = 'customerID';
		
			if($beneficiaryStructureFlag)
			{
				$benTransAmountSum = '';
				if($userType == 'beneficiary'){
						$strBeneficiary = "SELECT 
											  b.benID,
											  b.firstName,
											  b.middleName,
											  b.lastName,
											  b.Address,
											  b.Address1,
											  i.id_number,
											  i.expiry_date
										  FROM 
											 ".TBL_BENEFICIARY." as b,
											   user_id_types as i 
										  WHERE 
											  benID = '".$userId."' AND 
											  user_type = 'B' AND
											  user_id = benID";
						
						//debug($strBeneficiary);
						$beneData = selectFrom($strBeneficiary);
						
						if($beneData["benID"]!='')
						{
							$strDuplicateBen = "SELECT 
													  b.benID
												  FROM 
													 ".TBL_BENEFICIARY." as b,
													   user_id_types as i 
												  WHERE 
													  firstName  = '".$beneData["firstName"]."'  AND 
													  middleName = '".$beneData["middleName"]."' AND 
													  lastName   = '".$beneData["lastName"]."'   AND 
													  Address    = '".$beneData["Address"]."'    AND 
													  id_number  = '".$beneData["id_number"]."'  AND
													  benID     != '".$beneData["benID"]."'      AND
													  user_type  = 'B' AND
													  user_id    = benID";
							//debug($strDuplicateBen);  
							$benDuplicateData = selectMultiRecords($strDuplicateBen);
						}
						if(!empty($benDuplicateData))
						{
							for($d=0; $d<count($benDuplicateData);$d++){  
								$strBenTrans = "SELECT 
														SUM(transAmount) AS benTotalAmount
												 
													  FROM 
														 ".TBL_TRANSACTIONS." 
													  WHERE 
														  benID = '".$benDuplicateData[$d]["benID"]."' AND
														  transDate BETWEEN '$fromDate' AND '$toDate' AND
														  transStatus != 'Cancelled'";
						//debug($strBenTrans);
						$benTransData = selectFrom($strBenTrans);	
						$benTransAmountSum .= $benTransData["benTotalAmount"];
						//debug($benTransAmountSum);
						}
					}
				 }
			} 
			/**
			 Fetch Accumulated amount of sender
			**/
			$senderAccumulativeAmountReturn = 0;
			
			$sqlCountTrans = "
								select 
										count(transID) as transCount, 
										sum($calculateOn) as totalAmount 
								from 
										".TBL_TRANSACTIONS."
								where 
										".$fieldName." = '".$userId."' and 
										transDate between '$fromDate' and '$toDate'  
										AND transStatus != 'Cancelled'";
			//debug($sqlCountTrans);
			$countTrans = selectFrom($sqlCountTrans); 
			//debug($countTrans);
			$intTotalCustomerTransactions = $countTrans["transCount"];
			$senderAccumulativeAmountReturn	  = $countTrans["totalAmount"];
			$intBackDays = $arrGroupRightViewData["backDays"];
			//debug($senderAccumulativeAmountReturn);
			if(!empty($transID)){
				$strTransAmount = selectFrom("select $calculateOn from ".TBL_TRANSACTIONS." where transID = '".$transID."'");
				$senderAccumulativeAmount =  $senderAccumulativeAmount - $strTransAmount["transAmount"];
			}
			//debug($calculateAmount);
			$senderAccumulativeAmountReturn += $calculateAmount;
			if($beneficiaryStructureFlag)
				$senderAccumulativeAmountReturn += $benTransAmountSum;
		
		 // debug($senderAccumulativeAmountReturn);
		
			return $senderAccumulativeAmountReturn;
		}

	function regulatorForm($country)
	{
		$strComp = "SELECT id,document_name,document_path FROM regulator_form 
								WHERE document_path !='' AND document_name!='' AND 	country = '".$country."'";
		$complianceDocuments = selectFrom($strComp);
								
		if(!empty($complianceDocuments["id"]))
		{
			$document_name = $complianceDocuments["document_name"];
			$document_path = $url.$complianceDocuments["document_path"];
			//debug($document_name."-->".$document_path);
			return $document_name."|".$document_path;
		}
	}

	function checkCurrenctRule($arguments)
	{
		//debug_print_backtrace();
		//debug($arguments);
		$currentRule = $arguments["currentRule"];
		$headerFlag = $arguments["headerFlag"];
		$transAmount = $arguments["transAmount"];
		$localAmount = $arguments["localAmount"];
		$totalAmount = $arguments["totalAmount"];
		$transID = $arguments["transID"];
	    $customerID = $arguments["customerID"];
	    $benID = $arguments["benID"];
		$callFrom =$arguments["callFrom"];
		$beneficiaryStructureFlag = $arguments["beneficiaryStructureFlag"];
		$amountCheck = 0;
		$returnCurrentRuleData = array();
		$strAllPromptCurrentRule = "";
		//debug($currentRule);
		
		for($k = 0 ; $k < count($currentRule) ; $k++)
		{
			if($currentRule[$k]["ruleID"]!='') 
			{
				$fromAmount = 0;
				$toAmount = 0;
				$userType = '';
				
				if(empty($transAmount))
					$transAmount = 0;
					$custDocumentFlag = false;
				if($currentRule[$k]["matchCriteria"] == "BETWEEN")
				   {
					  $betweenAmount= explode("-",$currentRule[$k]["amount"]);
					  $fromAmount=$betweenAmount[0];   
					  $toAmount=$betweenAmount[1]; 
					  $matchCriteria = $currentRule[$k]["matchCriteria"];
					  $message = $currentRule[$k]["message"];
					}else{
					 $fromAmount = $currentRule[$k]["amount"];
					 $matchCriteria = $currentRule[$k]["matchCriteria"];
					 $message = $currentRule[$k]["message"];
					
					}
					$userType = $currentRule[$k]["userType"];
					$currentCTR = $currentRule[$k]["CTR"];
				
					if(!empty($currentRule[$k]["doc_provided"]))
					 {
						 $custDocProvidedFlag = false;
						 $arrDocID = unserialize($currentRule[$k]["doc_provided"]);
						 if(!empty($arrDocID)){
								$custDocProvidedFlag = true;
							 // $arrDocID[1] = Y;
							 // $arrDocID[3] = Y;
							 $custDocumentFlag = complianceUploadDoc($arrDocID,$customerID);
						 }
					 }
					
					   if(!empty($strAllPromptCurrentRule))
						 $strAllPromptCurrentRule .="<br>";
				
				 if($currentRule[$k]["amountType"] == 'trans')
					   $amountCheck = $transAmount;
				  elseif($currentRule[$k]["amountType"] == 'destination')  
						$amountCheck = $localAmount;
				  elseif($currentRule[$k]["amountType"] == 'total')  
						$amountCheck = $totalAmount;
				
				if(empty($amountCheck))
					 $amountCheck = $transAmount;
	
				  $inputCurrentArry = array(
						 "matchCriteria"       => $matchCriteria,
						 "fromAmount"          => $fromAmount,
						 "transAmount"         => $amountCheck,
						 "message"             => $message,
						 "toAmount"            => $toAmount,
						 "custDocumentFlag"    => $custDocumentFlag,
						 "custDocProvidedFlag" => $custDocProvidedFlag,
						 "userType"            => $userType,
						 "ctrValue"            => $currentCTR,
						 "currentRuleId"       => $currentRule[$k]["ruleID"]
						 );
				 
				 $strAllPromptCurrentRule = returnPromptForCurrentAmount($inputCurrentArry);
				//debug($strAllPromptCurrentRule);
				 //debug($callFrom);
				 if(!empty($strAllPromptCurrentRule))
				 {
					$document_name = '';
					$document_path = '';
					$arrRegulator ='';
					
					/* to check whther to hold transaction or not*/
					if($currentRule[$k]["isHold"] == 'Y' && $callFrom == 'import'){
						
						$returnCurrentRuleData[$currentRule[$k]["ruleID"]]['S'] = $currentRule[$k]["isHold"];
						$returnCurrentRuleData[$currentRule[$k]["ruleID"]]['M'] = $currentRule[$k]["message"];
						//$returnCurrentRuleData = $currentRule[$k]["ruleID"]."|".'Hold'."|".$currentRule[$k]["message"];
						//return $returnCurrentRuleData;//$strReturnResponse = 'hold'; //if hold is found than immediatly hold the transaction
					}
					elseif($callFrom == 'ntm')
					{
						$returnCurrentRuleData[] = $currentRule[$k]["ruleID"];
						//debug($returnCurrentRuleData);
					}
	
					$headerFlag = false;
				 }	
					
					//debug($arrCurrentRule);
			}
			 
	}	
	return $returnCurrentRuleData;
	//return $strReturnResponse;
	}
	
	function checkCumulativeRule($arguments)	
	{
		$cumulativeRule = $arguments["cumulativeRule"];
		$headerFlag = $arguments["headerFlag"];
		$transAmount = $arguments["transAmount"];
		$localAmount = $arguments["localAmount"];
		$totalAmount = $arguments["totalAmount"];
		$callFrom =$arguments["callFrom"];
		$amountCheck =0;
		//$userId = $arguments["transAmount"];
		
		$transID = $arguments["transID"];
		$customerID = $arguments["customerID"];
		$benID = $arguments["benID"];
		$returnCumulativeRuleData = array();
		//$strReturnResponse= $arguments["strReturnResponse"];
		//debug($cumulativeRule);
		$strAllPromptAccumulativeRule = "";
		$ctrValue = '';	
		$strGroupRightViewSql = "select * from oldTransactionGroupRight where `group`='".$agentType."' and `haveRight`='Y'"; 
		$arrGroupRightViewData = selectFrom($strGroupRightViewSql);
		$to = getCountryTime(CONFIG_COUNTRY_CODE);
		for($j = 0 ; $j < count($cumulativeRule) ; $j++){
		
			if($cumulativeRule[$j]["ruleID"]!='') {
			 $fromAmount = 0;
			 $toAmount = 0;
			 $senderAccumulativeAmount = '';
			 $periodFlag = true;
			
				if($cumulativeRule[$j]["matchCriteria"] == "BETWEEN")
				   {
					  $betweenAmount= explode("-",$cumulativeRule[$j]["amount"]);
					  $fromAmount=$betweenAmount[0];   
					  $toAmount=$betweenAmount[1]; 
					  $matchCriteria = $cumulativeRule[$j]["matchCriteria"];
					  $message = $cumulativeRule[$j]["message"];
					}else{
					 $fromAmount = $cumulativeRule[$j]["amount"];
					 $matchCriteria = $cumulativeRule[$j]["matchCriteria"];
					 $message = $cumulativeRule[$j]["message"];
					} 
					$userType = $cumulativeRule[$j]["userType"];
					
					/* set user ID bydefault user id set customer id*/
					if($cumulativeRule[$j]["userType"] == 'beneficiary')
						$userId = $benID;
					elseif($cumulativeRule[$j]["userType"] == 'sender' ||  $cumulativeRule[$j]["userType"] == '')
						$userId = $customerID;
					
					if($cumulativeRule[$j]["period"] != ''){
						$arrPeriod = explode("|",$cumulativeRule[$j]["period"]);
						if($arrPeriod[0] != '' ||  $arrPeriod[1] != ''){
							$fromDate = getPeriodFromDate($arrPeriod);
							$periodFlag = false;
							}
					}
					if(is_array($arrGroupRightViewData) && $periodFlag)
					{
						//$to = getCountryTime(CONFIG_COUNTRY_CODE);
						$fromData = strtotime("-".$arrGroupRightViewData["backDays"]." days");
						$fromDate = date("Y",$fromData)."-".date("m",$fromData)."-".date("d",$fromData)." 00:00:00";
					}
		
						 
					  if($cumulativeRule[$j]["amountType"] == 'trans'){
							$amountCheck = $transAmount;
							$calculateOn = 'transAmount';
					  }elseif($cumulativeRule[$j]["amountType"] == 'destination'){  
							$amountCheck = $localAmount;
							$calculateOn = 'localAmount';
					  }elseif($cumulativeRule[$j]["amountType"] == 'total'){  
							$amountCheck = $totalAmount;
							$calculateOn = 'totalAmount';
					  }	
					  else
					  {
						$calculateOn = 'transAmount';
					  }	
							
					/* set parameters for calling functions*/			
					$arguments = array
					(
						"userType" =>$userType,
						"userId" => $userId,
						"fromDate" =>$fromDate,
						"toDate" =>$to,
						"transID" =>$transID,
						"beneficiaryStructureFlag" =>$beneficiaryStructureFlag,
						"calculateOn"=>$calculateOn,
						"calculateAmount"=>$amountCheck,
					);
					/* call function to get accumualtive amount */
					$senderAccumulativeAmount = getUserAccumulativeAmount($arguments);
					$ctrValue = $cumulativeRule[$j]["CTR"];
					
					
					if(empty($amountCheck))
						 $amountCheck = $transAmount;
					 
						 $inputArry = array(
							 "matchCriteria"            => $matchCriteria,
							 "fromAmount" 				=> $fromAmount,
							 "senderAccumulativeAmount" => $senderAccumulativeAmount,
							 "message" 					=> $message,
							 "toAmount" 				=> $toAmount,
							 "custDocumentFlag" 		=> $custDocumentFlag,
							 "custDocProvidedFlag"       => $custDocProvidedFlag,
							 "ctrValue"         		=> $ctrValue,
							 "userType" 				=> $userType,
							 "accumulativeId" 			=> $cumulativeRule[$j]["ruleID"]
						 );
			
				
					$strAllPromptAccumulativeRule = returnPromptForAccumulativeAmount($inputArry);
					//debug($strAllPromptAccumulativeRule);
					if(!empty($strAllPromptAccumulativeRule)){
					 $document_name = '';
					 $document_path = '';
					 $arrRegulator ='';
						//$arrCurrentRule[$k] = $strAllPromptCurrentRule;
					 if($cumulativeRule[$j]["CTR"] !='N' && $cumulativeRule[$j]["CTR"] !=''){
								$arrRegulator = regulatorForm($cumulativeRule[$j]["country"]);
								if(!empty($arrRegulator)){
								$arrForm = explode("|",$arrRegulator);
								$document_name = $arrForm[0];
								$document_path = $arrForm[1];
							}
						  }
						
						 if($headerFlag){
						   //$strReturnResponse .= getHeader();
						   $strReturnResponse .= '<tr bgcolor="#FF0000"><td colspan="6">';
						   $strReturnResponse .= '<input type="button" name="proceed" id="proceed" value="Proceed" onclick="show_confirmation_page(\'proceed\');" />';
						   $strReturnResponse .= '</td></tr>';
						 } 
						 if(!empty($cumulativeRule[$j]["country"]))
							$originCountryAR = getCountryName($cumulativeRule[$j]["country"]);
						$strReturnResponse .= '<tr bgcolor="#FF0000">
								<td>'.$originCountryAR.'</td>
								<td>'.$cumulativeRule[$j]["applyTrans"].'</td>
								<td>'.$cumulativeRule[$j]["amount"].'</td>
								<td>'.$cumulativeRule[$j]["matchCriteria"].'</td>
								<td>'.$cumulativeRule[$j]["message"].'</td>';
										
						$strReturnResponse .=	'<td>';
										  if($cumulativeRule[$j]["CTR"] != 'N' && $cumulativeRule[$j]["CTR"] != '')
						$strReturnResponse .=	    '<a href="'.$document_path.'" target="_blank"><strong>'.$document_name.'</strong></a>';
						$strReturnResponse .=  '</td>';
						
						$strReturnResponse .=   '</tr>';
					
						/* to check whther to hold transaction or not*/
						if($cumulativeRule[$j]["isHold"] == 'Y' && $callFrom == 'import'){
							
							$returnCumulativeRuleData[$cumulativeRule[$j]["ruleID"]]['S'] = $cumulativeRule[$j]["isHold"];
							$returnCumulativeRuleData[$cumulativeRule[$j]["ruleID"]]['M'] = $cumulativeRule[$j]["message"];
							//$returnCumulativeRuleData = 'Hold'."|".$cumulativeRule[$j]["ruleID"]."|".$cumulativeRule[$j]["message"]; 
							//$strReturnResponse .='<input type="hidden" name="compliance_hold_transaction" id="compliance_hold_transaction" value="Y" />';
						}
						elseif($callFrom == 'ntm')
						{
							$returnCumulativeRuleData[] = $cumulativeRule[$j]["ruleID"];
							//debug($returnCurrentRuleData);
						}
						
						$headerFlag = false;
				}
				//debug($strAllPromptAccumulativeRule);
			 }	
		  }	
		// return  $strReturnResponse;
		  return  $returnCumulativeRuleData;
	
	}

	function getPeriodFromDate($arrInput)
	{
		// this may be Monthly etc. now get start of existing month
		//debug_print_backtrace();
		$format = 'Y-m-d H:i:s';
		$date = date ($format);
		$toDate = date ($format);
		if(is_array($arrInput)){
		 if($arrInput[0] == "days"){
			$input = $arrInput[0];
			$days = $arrInput[1];
		  }else{
			$input = $arrInput[0];
			}	
		}	
		$input = strtolower($input);			
		
		if($input == "daily"){
			$date_case = date("Y-m-d")." 00:00:00";
		}elseif($input == "weekly"){
			$getWeekDay = weekday(date('Y'), date('m'), date('d'));
			$date_case = date("Y-m-").$getWeekDay." 00:00:00";
		}elseif($input == "monthly"){
			$date_case = date("Y-m")."-01 00:00:00";
		}elseif($input == "annualy"){
			$date_case = date("Y")."-01-01 00:00:00";
		}elseif($input == "quarterly"){
			$date_case2 = strftime( '%Y%m%d', strtotime( strftime( '%Y' ) . '-' . ( ceil( strftime( '%m' )/3 )*3-2 ).'-1' ) );
			$date_case = date($format,strtotime($date_case2));
		}elseif($input == "days"){
		$string = '-'.$days." "."day";
		   $date_case = date ($format, strtotime ( $string . $date ) ); 
		}
		return $date_case;
	}
	
	function weekday($fyear, $fmonth, $fday) //0 is monday
	{
		return (((mktime ( 0, 0, 0, $fmonth, $fday, $fyear) - mktime ( 0, 0, 0, 7, 17, 2006))/(60*60*24))+700000) % 7; 
	}
	 
	function complianceUploadDoc($arrDocID,$customerID)
	{
		//debug_print_backtrace();
		$docFlag    = false;
		if(!empty($arrDocID))
			$arrDocID = array_keys($arrDocID); // array(1,3);
		if(!empty($arrDocID[0]))
			$arrDocStr = "'".implode("','",$arrDocID)."'"; // '1','3'
		//debug($arrDocStr,true); 
		if(!empty($customerID)){
			if(!empty($arrDocStr)){
				$strCustSql = "SELECT id_type_id  FROM user_id_types WHERE user_id = '".$customerID."' 
				AND user_type='C' AND id_type_id IN($arrDocStr) ORDER BY id_type_id";
				$strCustDoc = selectMultiRecords($strCustSql);
			}
		}
		for($d=0;$d<count($strCustDoc);$d++)
		{
			$arrDoc[$d]=$strCustDoc[$d]["id_type_id"]; 
		} 
		if(!empty($arrDocID) && !empty($arrDoc))
			$returnArr = array_diff_assoc($arrDocID,$arrDoc);
		
		if(!empty($returnArr))
			$docFlag = true;
		if(!empty($arrDocID) && empty($arrDoc))
			$docFlag = true;
		
		return $docFlag;		
	} // end of functions
	
	function returnPromptForCurrentAmount($arrCurrentArguments) 
	{
		//debug_print_backtrace();
		//debug($arrCurrentArguments);
		$matchCriteria    = $arrCurrentArguments["matchCriteria"];
		$fromAmount       = $arrCurrentArguments["fromAmount"];
		$transAmount      = $arrCurrentArguments["transAmount"];
		$message 		  = $arrCurrentArguments["message"];
		$toAmount 	      = $arrCurrentArguments["toAmount"];
		$custDocumentFlag = $arrCurrentArguments["custDocumentFlag"];
		$custDocProvidedFlag = $arrCurrentArguments["custDocProvidedFlag"];
		$userType         = $arrCurrentArguments["userType"];
		$currentCTR       = $arrCurrentArguments["currentCTR"];
		$currentRuleId    = $arrCurrentArguments["currentRuleId"];
						 
		$strPrompt = "";
		$btnDisabled = "";

		if($matchCriteria == "BETWEEN")
		 {
			
			if($fromAmount <= $transAmount && $transAmount <= $toAmount)
				{
					if($custDocumentFlag && $custDocProvidedFlag){
						$strPrompt = $message;
					}elseif(!$custDocumentFlag && !$custDocProvidedFlag){
						$strPrompt = $message;	
					}	
					
				}	
		  }	
		  else
		  {
			switch($matchCriteria)
			{
			  case "==":
				if($transAmount == $fromAmount){
					if($custDocumentFlag && $custDocProvidedFlag){
						$strPrompt = $message;
					}elseif(!$custDocumentFlag && !$custDocProvidedFlag){
						$strPrompt = $message;	
					}
				  }	
				 break;
			  
			  case "<":
				if($transAmount < $fromAmount){
					if($custDocumentFlag && $custDocProvidedFlag){
						$strPrompt = $message;
					}elseif(!$custDocumentFlag && !$custDocProvidedFlag){
						$strPrompt = $message;	
					}	
				}	
					break;
						
			  case ">":
				if($transAmount > $fromAmount){
					if($custDocumentFlag && $custDocProvidedFlag){
						$strPrompt = $message;
					}elseif(!$custDocumentFlag && !$custDocProvidedFlag){
						$strPrompt = $message;
					}	
				 }	
					break;
			  case "<=":
				if($transAmount <= $fromAmount){
					if($custDocumentFlag && $custDocProvidedFlag){
						$strPrompt = $message;
					}elseif(!$custDocumentFlag && !$custDocProvidedFlag){
						$strPrompt = $message;
					}
				}		
					break;
			  case ">=":
				if($transAmount >= $fromAmount){
					if($custDocumentFlag && $custDocProvidedFlag){
						$strPrompt = $message;
					}elseif(!$custDocumentFlag && !$custDocProvidedFlag){
						$strPrompt = $message;
					}	
				}		
					break;
			  case "!=":
				if($transAmount != $fromAmount){
					if($custDocumentFlag && $custDocProvidedFlag){
						$strPrompt = $message;
					}elseif(!$custDocumentFlag && !$custDocProvidedFlag){
						$strPrompt = $message;
					}
				}	
					break;
				default:
						$strPrompt = "";
			}
		  }
		return $strPrompt;     
	}
	
	function returnPromptForAccumulativeAmount($arrData)
	{
		//debug_print_backtrace();
		$matchCriteria = $arrData["matchCriteria"];
		$fromAmount = $arrData["fromAmount"];
		$senderAccumulativeAmount = $arrData["senderAccumulativeAmount"];
		$message = $arrData["message"];
		$toAmount = $arrData["toAmount"];
		$custDocumentFlag =  $arrData["custDocumentFlag"];
		$ctrValue =  $arrData["ctrValue"];
		$accumulativeId =  $arrData["accumulativeId"];
		$userType =  $arrData["userType"];
		$custDocProvidedFlag = $arrData["custDocProvidedFlag"];
		$strPromptAccumulative = "";
		$btnDisabled = "";
	
			
		if($matchCriteria == "BETWEEN")
		 {
			if($fromAmount <= $senderAccumulativeAmount && $senderAccumulativeAmount <= $toAmount)
				{
					if($custDocumentFlag && $custDocProvidedFlag)
						$strPromptAccumulative = $message;
					elseif(!$custDocumentFlag && !$custDocProvidedFlag)
						$strPromptAccumulative = $message;	
				}	
		  }	
		  else
		  {
			switch($matchCriteria)
			{
			  case "==":
				if($senderAccumulativeAmount == $fromAmount){
					if($custDocumentFlag && $custDocProvidedFlag)
						$strPromptAccumulative = $message;
					elseif(!$custDocumentFlag && !$custDocProvidedFlag)
						$strPromptAccumulative = $message;
				 }		
				 break;
			  
			  case "<":
				if($senderAccumulativeAmount < $fromAmount){
					if($custDocumentFlag && $custDocProvidedFlag)
						$strPromptAccumulative = $message;
					elseif(!$custDocumentFlag && !$custDocProvidedFlag)
						$strPromptAccumulative = $message;	
					}	
					break;
						
			  case ">":
				if($senderAccumulativeAmount > $fromAmount){
					if($custDocumentFlag && $custDocProvidedFlag)
						$strPromptAccumulative = $message;
					elseif(!$custDocumentFlag && !$custDocProvidedFlag)
						$strPromptAccumulative = $message;	
					}
					break;
			  case "<=":
				if($senderAccumulativeAmount <= $fromAmount){
					if($custDocumentFlag && $custDocProvidedFlag)
						$strPromptAccumulative = $message;
					elseif(!$custDocumentFlag && !$custDocProvidedFlag)
						$strPromptAccumulative = $message;
					}
					break;
			  case ">=":
				if($senderAccumulativeAmount >= $fromAmount){
					if($custDocumentFlag && $custDocProvidedFlag)
						$strPromptAccumulative = $message;
					elseif(!$custDocumentFlag && !$custDocProvidedFlag)
						$strPromptAccumulative = $message;	
					}	
					break;
			  case "!=":
				if($senderAccumulativeAmount != $fromAmount){
					if($custDocumentFlag && $custDocProvidedFlag)
						$strPromptAccumulative = $message;
					elseif(!$custDocumentFlag && !$custDocProvidedFlag)
						$strPromptAccumulative = $message;	
				}		
					break;
				default:
						$strPromptAccumulative = "";
			}
		 }
		return $strPromptAccumulative; 	
	}
	
	function getCountryName($countryId)	
	{
		$strCountrySql 	= selectFrom("SELECT countryName FROM countries WHERE countryId ='".$countryId."'");
		return $strCountrySql["countryName"];
	}	

	function nameRules($arrParams, $value="")
	{
		// debug_print_backtrace();
		$returnValue = array();
		$strMessage = "";
		$date_time = date("Y-m-d h:i:s"); 
		$arrReturnCustExact = array();
		$arrReturnBentExact = array();
		$arrReturnCustPartial = array();
		$arrReturnBenPartial = array();
		$arrFinalReturn = array();
	   	
		$callFrom  = $arrParams["callFrom"];

		
		if(empty($callFrom))
			$callFrom = 'import';	
		
		 if(!empty($arrParams['transid']))
		 {
		   $sqlUsers = "select 
						 c.firstName as custFN,
						 c.middleName as custMN,
						 c.lastName as custLN,
						 b.firstName as benFN,
						 b.middleName as benMN,
						 b.lastName as benLN
					from
						transactions as t
						LEFT JOIN beneficiary as b ON t.benID = b.benID
						LEFT JOIN customer as c ON t.customerID = c.customerID
					where
						t.transID = '".$arrParams['transid']."'	";
				//debug($sqlUsers);		
				
				$arrUsersData = selectFrom($sqlUsers);
				//debug($arrUsersData,true);
		   $custFirstName = $arrUsersData["custFN"];
		   $custMiddleName = $arrUsersData["custMN"];
		   $custLastName = $arrUsersData["custLN"];
		   
		   $benFirstName = $arrUsersData["benFN"];
		   $benMiddleName = $arrUsersData["benMN"];
		   $benLastName = $arrUsersData["benLN"];
		   
		   	/* trim name*/
			  $_arrCustName = trimName($custFirstName,$custMiddleName,$custLastName);
			  $_arrBenName = trimName($benFirstName,$benMiddleName,$benLastName);
			 // debugCompliance($_arrName);
			 
			 
		    /* sender exact rules*/
			$arrCustExactRules = usersRules('sender','Exact Match');
			//debug($arrCustExactRules);
			
		  /* sender partial rules*/
			$arrCustPartialRules = usersRules('sender','Partial Match');
			//debug($arrCustPartialRules,true);
		  
		  /* beneficairy exact rules*/
			$arrBenExactRules = usersRules('beneficiary','Exact Match');
			//debug($arrBenExactRules);
			
		  /* beneficiary partial rules*/
			$arrBenPartialRules = usersRules('beneficiary','Partial Match');
			//debug($arrBenPartialRules);
			
		 }	
		 elseif($arrParams['ruleFor'] == 'customer')
		 {
			 if(!empty($arrParams['customerID']))
			 {
				$sqlUsers = "select 
							 c.firstName as custFN,
							 c.middleName as custMN,
							 c.lastName as custLN
						from
							customer as c
						where
							c.customerID = '".$arrParams['customerID']."'  ";
					//debug($sqlUsers);		
					
					$arrUsersData = selectFrom($sqlUsers);
			   
			   $custFirstName = $arrUsersData["custFN"];
			   $custMiddleName = $arrUsersData["custMN"];
			   $custLastName = $arrUsersData["custLN"];
			 }
			 elseif(empty($arrParams['customerID']) && (!empty($arrParams['custFN']) || !empty($arrParams['custLN'])))
			 {
			   $custFirstName = $arrParams["custFN"];
			   $custMiddleName = $arrParams["custMN"];
			   $custLastName = $arrParams["custLN"];
			   //debug($custFirstName."-->".$custMiddleName."-->".$custLastName);
			 }
				/* trim name*/
				$_arrCustName = trimName($custFirstName,$custMiddleName,$custLastName);
				/* sender exact rules*/
				$arrCustExactRules = usersRules('sender','Exact Match');
				//debug($arrCustExactRules);
				
				/* sender partial rules*/
				$arrCustPartialRules = usersRules('sender','Partial Match');
				//debug($arrCustPartialRules);
		 }	
		 elseif($arrParams['ruleFor'] == 'beneficiary')
		 {
			 if(!empty($arrParams['benID']))
			 {
				$sqlUsers = "select 
							  b.firstName as benFN,
							  b.middleName as benMN,
							  b.lastName as benLN
						from
							beneficiary as b
						where
							b.benID = '".$arrParams['benID']."'  ";
					//debug($sqlUsers);		
					
					$arrUsersData = selectFrom($sqlUsers);
				   
				   $benFirstName = $arrUsersData["benFN"];
				   $benMiddleName = $arrUsersData["benMN"];
				   $benLastName = $arrUsersData["benLN"];
			 }
			 elseif(empty($arrParams['benID']) && (!empty($arrParams['benFN']) ||!empty($arrParams['benLN'])))
			 {
			   $benFirstName = $arrParams["benFN"];
			   $benMiddleName = $arrParams["benMN"];
			   $benLastName = $arrParams["benLN"];
			 }
				/* trim name*/
				$_arrBenName = trimName($benFirstName,$benMiddleName,$benLastName);
				
				/* beneficairy exact rules*/
				$arrBenExactRules = usersRules('beneficiary','Exact Match');
				//debug($arrBenExactRules);
				
				/* beneficiary partial rules*/
				$arrBenPartialRules = usersRules('beneficiary','Partial Match');
				//debug($arrBenPartialRules);
		}	
			
			/////////////exact match /////////////////////////////
				/*
						Letter represents
						S = hold
						M = Message
						B = auto block
						U = user type e.g sender or beneficairy
						L = list of ids of match user, its contain list name and primary key of list table
						T = type match e.g exact or partial
					*/	
				
				/* customer exact match result*/
				foreach($arrCustExactRules as $ceKey => $ceVal)
				{
					//debug($ceVal["listName"]."-->".$ceVal["matchCriteria"]);
					$arguments = array("listName"=>$ceVal["listName"],"listID"=>$ceVal["listID"]);
					$returnRs = checkExactRules($_arrCustName,$arguments);
					
					if(!empty($returnRs))
					//if($returnFlag)
					{
					  $arrReturnCustExact[$ceVal["ruleID"]]['S'] = $ceVal["isHold"];
					  $arrReturnCustExact[$ceVal["ruleID"]]['M'] = $ceVal["message"];
					  $arrReturnCustExact[$ceVal["ruleID"]]['B'] = $ceVal["autoBlock"];
					  $arrReturnCustExact[$ceVal["ruleID"]]['U'] = $ceVal["userType"];
					  $arrReturnCustExact[$ceVal["ruleID"]]['T'] = 'exact';
					  $arrReturnCustExact[$ceVal["ruleID"]]['L'] = $returnRs;
					}
					
				}
				
				/* beneficiary exact match result*/
				foreach($arrBenExactRules as $beKey => $beVal)
				{
					//debug($ceVal["listName"]."-->".$ceVal["matchCriteria"]);
					$arguments = array("listName"=>$beVal["listName"],"listID"=>$beVal["listID"]);
					$returnBenRs = checkExactRules($_arrBenName,$arguments);
					if(!empty($returnBenRs))
					{
					 $arrReturnBentExact[$beVal["ruleID"]]['S'] = $beVal["isHold"];
					  $arrReturnBentExact[$beVal["ruleID"]]['M'] = $beVal["message"];
					  $arrReturnBentExact[$beVal["ruleID"]]['B'] = $beVal["autoBlock"];
					  $arrReturnBentExact[$beVal["ruleID"]]['U'] = $beVal["userType"];
					  $arrReturnBentExact[$beVal["ruleID"]]['L'] = $returnBenRs;
					}
					
				}
				
				/* customer partial match result*/
				if(empty($arrReturnCustExact))
				{
					foreach($arrCustPartialRules as $cpKey => $cpVal)
					{
						//debug($cpVal["listName"]."-->".$cpVal["matchCriteria"]);
						$arguments = array("listName"=>$cpVal["listName"],"listID"=>$cpVal["listID"]);
						$returnRs = checkPartialRules($_arrCustName,$arguments);
						//debug($returnRs);
						if(!empty($returnRs))
						{
						  $rule_id = $cpVal["ruleID"];
						 
						  $arrReturnCustPartial[$cpVal["ruleID"]]['S'] = $cpVal["isHold"];
						  $arrReturnCustPartial[$cpVal["ruleID"]]['M'] = $cpVal["message"];
						  $arrReturnCustPartial[$cpVal["ruleID"]]['B'] = $cpVal["autoBlock"];
						  $arrReturnCustPartial[$cpVal["ruleID"]]['U'] = $cpVal["userType"];
						  $arrReturnCustPartial[$cpVal["ruleID"]]['T'] = 'partial';
						  $arrReturnCustPartial[$cpVal["ruleID"]]['L'] = $returnRs;
						}
						
					}
				 }
				 /* beneficiary partial match result*/
				if(empty($arrReturnBentExact))
				{
					foreach($arrBenPartialRules as $bpKey => $bpVal)
					{
						//debug($bpVal["listName"]."-->".$bpVal["matchCriteria"]."-->".$bpVal["listID"]);
						$arguments = array("listName"=>$bpVal["listName"],"listID"=>$bpVal["listID"]);
						$returnBenRs = checkPartialRules($_arrBenName,$arguments);
						if(!empty($returnBenRs))
						{
						  $arrReturnBenPartial[$bpVal["ruleID"]]['S'] = $bpVal["isHold"];
						  $arrReturnBenPartial[$bpVal["ruleID"]]['M'] = $bpVal["message"];
						  $arrReturnBenPartial[$bpVal["ruleID"]]['B'] = $bpVal["autoBlock"];
						  $arrReturnBenPartial[$bpVal["ruleID"]]['U'] = $bpVal["userType"];
						  
						   $arrReturnBenPartial[$bpVal["ruleID"]]['L'] = $returnBenRs;
						}
						
					}
				}
				
			foreach($arrReturnCustExact as $key => $val)
				$arrFinalReturn[$key] = $val;
			
			foreach($arrReturnBentExact as $key => $val)
				$arrFinalReturn[$key] = $val;	
			
			foreach($arrReturnCustPartial as $key => $val)
				$arrFinalReturn[$key] = $val;	
			
			foreach($arrReturnBenPartial as $key => $val)
				$arrFinalReturn[$key] = $val;		
				
			/*debug($arrReturnCustExact);
			debug($arrReturnBentExact);
			debug($arrReturnCustPartial);
			debug($arrReturnBenPartial);
			debug($arrFinalReturn);*/
		
	  return $arrFinalReturn;
	}
	
	function usersRules($type,$match)
	{
		$arrReturn = array();
		
		if(empty($type))
			$type = 'sender';
		
		if(empty($match))	
			$match = 'Exact Match';
		
		/* exact match rules*/  
		  $sqlRules =   "select 
							   c.*,
							   l.listName as listName
							 from 
								".TBL_COMPLIANCE_LOGIC." as c
								LEFT JOIN ".TBL_COMPLIANCE_LIST." as l ON c.listID = l.listID
							 where 
								c.userType ='".$type."' and
								c.applyAt='User Creation' and 
								c.matchCriteria = '".$match."' and 
								c.isEnable = 'Y'
								";
			// debug($sqlRules);
			 $arrReturn = selectMultiRecords($sqlRules);					
			//debug($arrReturn);
		
		  return $arrReturn;	  
	}
	
	function trimName($firstName,$middleName,$lastName)
	{
	   //debug_print_backtrace();
	   $_arrReturnData = '';
	   $firstName =  trim($firstName);
	   $middleName = trim($middleName);
	   $lastName =   trim($lastName);
	
		$firstName = str_replace("'", "", $firstName);
		$middleName = str_replace("'", "", $middleName);
		$lastName = str_replace("'", "", $lastName);
		 
		$firstName = stripslashes($firstName);
		$middleName = stripslashes($middleName);
		$lastName = stripslashes($lastName);
		
		if($firstName !='' &&  $middleName!='' &&  $lastName!=''){
			$full_name = $firstName.",".$middleName.",".$lastName;
		  }elseif($firstName !='' &&  $middleName!=''){
			$full_name = $firstName.",".$middleName;
		  }elseif($firstName !='' &&  $lastName!=''){
			$full_name = $firstName.",".$lastName;
		  }elseif($middleName!='' &&  $lastName){
			$full_name = $middleName.",".$lastName;
		  }elseif($firstName !=''){
			$full_name = $firstName;
		  }elseif($middleName!=''){
			$full_name = $middleName;
		  }elseif($lastName!=''){
		   $full_name = $lastName;
		 }	
		 
		$_arrReturnData = array("firstName"=>$firstName,"middleName"=>$middleName,"lastName"=>$lastName,"full_name"=>$full_name);
		
		return  $_arrReturnData;
			
	}
	
	function checkExactRules($arrName,$arguments)
	{
				
		$listName   = trim($arguments["listName"]);
		$listId     = $arguments["listID"];	
		$firstName  = $arrName["firstName"];	
		$middleName = $arrName["middleName"];	
		$lastName   = $arrName["lastName"];	
		$full_name  = $arrName["full_name"];
		$full_name_with_space  = str_replace(","," ",$full_name);
		$complianceFlag = false;
		$returnData = array();
			
		
		if($listName == 'OFAC')
		{
			$queryExactData = "select 
									compliancePersonID as id,
									firstName,
									middleName,
									lastName,
									listID,
									CONCAT(firstName,' ',middleName,' ',lastName) as fullName,
									sdn_title
								 from 
									".TBL_COMPLIANCE_PERSON."
								 where 
									listID= '".$listId."' "; 
			
			
			if($firstName!='')
				$queryExactData .= " and (firstName ='".$firstName."')"; 
			
			if($middleName!='')
				$queryExactData .= " and (middleName ='".$middleName."')"; 
			
			if($lastName!='')
				$queryExactData .= " and (lastName ='".$lastName."')"; 
			//debug($queryExactData );
			
			$rs_exact = mysql_query($queryExactData);
			
			$compExactSubRecord = mysql_num_rows($rs_exact);
			
			if($compExactSubRecord < 1 )
			{
				$queryExactData = "select 
										alt_id as id,
										compliancePersonID,
										alt_name as sdn_title,
										listID 
									from 
										".TBL_COMPLIANCE_OFAC_ALT." 
				 
				 where listID= '".$listId."' "; 
			
				if($firstName!='' || $middleName !='' || $lastName!='')
					$queryExactData .= " and  alt_name = '".$full_name."'"; 
			
			}
		}
		
		if($listName == 'UNSC')
		{
			$queryExactData = "select 
									unsc_id as id,
									fullName,
									alias 
								from 
									".TBL_COMPLIANCE_UNSC_LIST." 
								where 
									listID= '".$listId."' "; 
			
			if($firstName!='' || $middleName !='' || $lastName !='')
				$queryExactData .= " and  fullName =('".trim($full_name_with_space)."')"; 
			//debug($queryExactData );
		
			$rs_exact = mysql_query($queryExactData);
		
			$compExactSubRecord = mysql_num_rows($rs_exact);
			
			if($compExactSubRecord < 1 )
			{
				$queryExactData = "select 
										unsc_id as id,
										fullName,
										alias 
									from 
										".TBL_COMPLIANCE_UNSC_LIST."
									where 
										listID= '".$listId."' "; 
			
			if($firstName!='' || $middleName !='' || $lastName !='')
				$queryExactData .= " and  alias = '".$full_name."'"; 
			}
		}
		
		
		if($listName == 'payex')
		{
		
			$queryExactData = "select 
									compliancePersonID as id,
									listID,
									CONCAT (firstName,' ',middleName,' ',lastName) as fullName,
									sdn_title
								from 
									".TBL_COMPLIANCE_PERSON."
								where 
									listID= '".$listId."'"; 
			
			if($firstName!='')
				$queryExactData .= " and (firstName ='".$firstName."')"; 
			
			if($middleName!='')
				$queryExactData .= " and (middleName ='".$middleName."')"; 
			
			if($lastName!='')
				$queryExactData .= " and (lastName ='".$lastName."')"; 
		
		}
		
		if($listName == 'PEP')
		{
		
			$queryExactData = "select 
									comp_PEPID as id,
									listID,
									fullName,
									position as sdn_title,
									portfolio 
								from 
									compliancePEP 
								where 
									listID= '".$listId."' "; 
			
			if($firstName!='' || $middleName !='' || $lastName !='')
				$queryExactData .= " and  fullName =('".$full_name."')"; 
		} 
		
		if($listName == 'HM Treasury')
		{
		
			$queryExactData = "select
									 hm_id as id,
									 list_id as listID,
									 full_name as fullName
								from 
									compliance_hm_treasury 
								where 
									list_id= '".$listId."' ";
			
			if($firstName!='' || $middleName !='' || $lastName !='')
			{
				$queryExactData .= " and  full_name =('".$full_name."')"; 
				$queryExactData .=" AND group_type = 'Individual' " ;
			}		  
		}
		//debug($queryExactData);
		$result = mysql_query($queryExactData);
		$i=0;
		while($compPersonRecord = mysql_fetch_array($result))
		{
			//debug($compPersonRecord["id"]);
			//debug($compPersonRecord);
			if(!empty($compPersonRecord["id"]))
			{
				$returnData[$listName][$compPersonRecord["id"]][] = $compPersonRecord["fullName"];
				$complianceFlag = true;
			}	
			$i++;
		}
		//debug($returnData,true);	
		return $returnData;
	}	

	function checkPartialRules($arrName,$arguments)
	{
		//debug_print_backtrace();
		$listName   = trim($arguments["listName"]);
		$listId     = $arguments["listID"];	
		$firstName  = $arrName["firstName"];	
		$middleName = $arrName["middleName"];	
		$lastName   = $arrName["lastName"];	
		$full_name  = $arrName["full_name"];
		$full_name_with_space  = str_replace(","," ",$full_name);
		$compPartialFlag = false;
		$returnData = array();
		
		if($listName == 'OFAC')
		{
				$query = "select 
								compliancePersonID as id,
								firstName,
								middleName,
								lastName,
								listID,
								CONCAT(firstName,' ',middleName,' ',lastName) as fullName,
								sdn_title
							 from 
								".TBL_COMPLIANCE_PERSON." 
							 where listID= '".$listId."' "; 
			
				$query  .= "and (";
				
				if($firstName!='')
					$query .= " firstName ='".$firstName."'"; 
			
				if($middleName!='')
					$query .= " or middleName ='".$middleName."'"; 
				
				if($lastName!='')
					$query .= " or lastName ='".$lastName."'"; 
					
					$query .= ")  Limit 5";
			
			//debug($queryExactData );
			$rs_exact = mysql_query($query);
			
			$compExactSubRecord = mysql_num_rows($rs_exact);
		
			if($compExactSubRecord < 1 )
			{
				$query = "select 
								alt_id as id,
								compliancePersonID,
								alt_name as sdn_title,
								listID 
							from 
								".TBL_COMPLIANCE_OFAC_ALT." 
							where 
								listID= '".$listId."' "; 
			
			if($firstName!='' || $middleName !='' || $lastName!='')
				$query .= " and  alt_name like '%".$full_name."%'  Limit 5"; 
			
			}
			//debug($query);
		}
		
		if($listName == 'UNSC')
		{
			$query = "select 
							unsc_id as id,
							fullName,
							alias 
					   from 
							".TBL_COMPLIANCE_UNSC_LIST." 
						where 
							listID= '".$listId."' "; 
			
			$query .= "and (";
			
			if($firstName!='')
				$query .= " fullName LIKE '%".$firstName."%'"; 
			
			if($middleName!='')
				$query .= " or fullName LIKE '%".$middleName."%'"; 
			
			if($lastName!='')
				$query .= " or fullName LIKE '%".$lastName."%'"; 
				
				$query .= ")  Limit 5";
			
			//debug($queryExactData );
			$rs_exact = mysql_query($query);
			
			$compExactSubRecord = mysql_num_rows($rs_exact);
			
			if($compExactSubRecord < 1 )
			{
				$query = "select 
								unsc_id as id,
								fullName,
								alias 
						   from 
								".TBL_COMPLIANCE_UNSC_LIST." 
						   where 
								listID= '".$listId."' "; 
				
				if($firstName!='' || $middleName !='' || $lastName!='')
				{
				
				$query .= "and (";
				
				if($firstName!='')
					$query .= " alias like '%,".$firstName.",%'"; 
				
				if($middleName!='')
					$query .= " or alias like '%,".$middleName.",%'"; 
				
				if($lastName!='')
					$query .= " or alias like '%,".$lastName.",%'"; 
				
					$query .= " or alias like '%,".$full_name_with_space.",%'"; 
				
					$query .= ")  Limit 5"; 
				}
			
			}
		}
		
		if($listName == 'payex')
		{
		
			$query = "select 
							compliancePersonID as id,
							listID,
							CONCAT(firstName,' ',middleName,' ',lastName) as fullName,
							sdn_title
						from 
							".TBL_COMPLIANCE_PERSON."
						where 
							listID= '".$listId."'"; 
			
			$query .= "and (";
			
			if($firstName!='')
				$query .= " firstName LIKE '%".$firstName."%' "; 
			
			if($middleName!='')
				$query .= " or middleName LIKE '%".$middleName."%'"; 
			
			if($lastName!='')
				$query .= " or lastName LIKE '%".$lastName."%'"; 
			
			$query .= ")  Limit 5";	
		}
		
		if($listName == 'PEP')
		{
			$query = "select 
							comp_PEPID as id,
							listID,
							fullName as fullName,
							position as sdn_title,
							portfolio 
						from 
							compliancePEP
						where 
							listID= '".$listId."' "; 
			
			if($firstName!='' || $middleName !='' || $lastName !='')
				$query .= " and  fullName like '%".trim($full_name)."%'  Limit 5"; 
			
		} 
		if($listName == 'HM Treasury')
		{
				$query = "select 
								hm_id as id,
								list_id as listID,
								full_name as fullName 
							from 
								compliance_hm_treasury
							where 
								list_id= '".$listId."' ";
			
			$query .= "and (";
			
			  if($firstName!='')
			 	 $query .= "  full_name LIKE '%".$firstName."%'"; 
		              	
			  if($middleName!='')
			  	$query .= "  OR  full_name LIKE '%".$middleName."%'"; 
			 	
			 if($lastName!='')
			  	$query .= "  OR full_name  LIKE '%".$lastName."%'"; 
				
				$query .= ")";	
				
				$query .=" AND group_type = 'Individual'  Limit 5 " ;
			
		//debug($query);
		}
		//debug($query);
		$result = mysql_query($query);
		while($compPersonRecord = mysql_fetch_array($result))
		{
			//debug($compPersonRecord["id"]);
			if(!empty($compPersonRecord["id"]))
			{
				//$returnData[$listName][] = $compPersonRecord["id"];
				//$returnData[$compPersonRecord["id"]][] = $compPersonRecord["fullName"];
				//$returnData[$listName][$compPersonRecord["id"]][] = $compPersonRecord["lastName"];
				//$complianceFlag = true;
				
				$returnData[$listName][$compPersonRecord["id"]][] = $compPersonRecord["fullName"];
				$complianceFlag = true;
			}	
		}
		//debug($returnData);
		return $returnData;
		
	}
	
	function nameRulesChecking($arrParams)
	{
		
		
		$strDataCollectSql = "
							 SELECT 
									customerID,
									benID,
							 FROM 
								transactions
							 WHERE
								transid = '".$arrParams['transid']."'
							";
	
		$arrOrderData = selectFrom($strDataCollectSql);
		
		
		
		
		
		$arrFieldsToDisplay = array();
		
		$arrFieldsToDisplay['OFAC'] = array(
			"alt_name" => "Alternate Name",
			"alt_type" => "Alternate Name Type",
			"alt_remarks" => "Remarks"
		);
		
		
		
		
		$arrFieldsToDisplay['HM'] = array(
			"title" => "Title", 
			"full_name" => "Full Name",
			"full_address" => "Full Address",
			"nationality1" => "Nationality",
			"dob1" => "Date of Birth",
			"dob_country" => "Birth Country",
			"last_updated" => "Last Listing Update"
		);
		
		
		foreach($_POST as $strKey => $strVal)
			$$strKey = $strVal;
			
		
		//print_r($_POST);
		
		$arrParams = array();
		$arrParams['firstName'] = $_POST['strFirstName']; 
		$arrParams['middleName'] = $_POST['strMiddleName']; 
		$arrParams['lastName'] = $_POST['strLastName']; 
		
		$arrReturn = payexCompliance($arrParams);
		
		//print_r($arrReturn);
		//debugCompliance($arrReturn);
		
		if(empty($arrReturn)){
			echo 'false';
			exit;
		}
		
		$strOutputHtml = '';
		
		foreach($arrReturn as $strList => $arrValues)
		{
			//if(in_array($strList, $arrFieldsToDisplay))
			//{
				$strEntryKey = $strList;
			
				///* browsing PARTIAL and EXACT match list
				foreach($arrValues as $strListMatchType => $arrMatchValues)
				{
					$bolAnyMatch = false;
					foreach($arrMatchValues as $intIndex => $arrFoundValues)		
					{
						foreach($arrFoundValues as $strMatchListAttribKey => $strMatchListAttribVal)
						{
							if($arrFieldsToDisplay[$strEntryKey][$strMatchListAttribKey])
								$strOutputHtml .= "<b>".$arrFieldsToDisplay[$strEntryKey][$strMatchListAttribKey]."</b>: ".$strMatchListAttribVal." <br />";
							
							$bolAnyMatch = true;
						}
						$strOutputHtml .= "<hr />";	
					}
					
					
					if($bolAnyMatch && strpos($strListMatchType, 'EXACT') !== false)
						$strOutputHtml .= "<input type='hidden' id='is_exact_match' name='is_exact_match' value='Y' />";
					
					$strOutputHtml .= "<input type='hidden' id='is_partial_match' name='is_partial_match' value='Y' />";
				}
			//}
		}
		
		/*
		echo "<hr />";
		//$arrReturnSubArray = getSubArray($arrReturn);
		//print_r($arrReturnSubArray);
		
		function getSubArray($arrParam)
		{
			if(is_array($arrParam))
				return getSubArray($arrParam);
			else
				return $arrParam;
		}
		*/
		
		$strOutputHtml .= "<input type='hidden' name='comp_detail' id='comp_detail' value='".base64_encode($strOutputHtml)."' />";
		
		echo $strOutputHtml;
		return $strOutputHtml;
	}
