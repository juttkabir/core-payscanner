<?php

/**
 * This file includes all the functions and logic that will be used to keep audit trail regarding the edit 
 * action on input data.
 *
 */


/**
 * This function will get the data of the row which is about the edit or edited
 * @param string $strDbTable		Table name of the database to get data
 * @param string $strPkColumnName	Name of primary key column in above table
 * @param int 	 $intPkId			Primary Key id value of above column
 * @return array $arrGetData		This array will contain full row data againt above 3 params
 */
function getRowData($strDbTable, $strPkColumnName, $intPkId)
{
	if(empty($strDbTable) || empty($strPkColumnName) || empty($intPkId))
		return false;
	
	$strGetSql = "SELECT * FROM ".$strDbTable." WHERE ".$strPkColumnName." = ".$intPkId;
	$arrGetData = selectFrom($strGetSql, MYSQL_ASSOC);
	
	return $arrGetData;
}


/**
 * To maintain the change of old data set and new dataset of same row this function will be used
 * @param array	$arrBeforeEditRecordSet	Array (associated) of the row which is before editing the record
 * @param array $arrAfterEditRecordSet	Array (associated) of the row which is after editing the record
 * @return array $arrActualDiff			Associated array of the change set with respect to before and after editing the record
 *
 * TODO: Mapping of field name to the actual Label in application
 *		 If foreign key id is modified than get associated name of foreign key id
 */
function maintainAudit($arrBeforeEditRecordSet, $arrAfterEditRecordSet)
{
	$arrActualDiff = array();
	
	if(empty($arrBeforeEditRecordSet) || empty($arrAfterEditRecordSet))
		return false;
			
	foreach($arrBeforeEditRecordSet as $strKey => $strVal)
	{
		//check if the before or after value is modified, only maintain record if new value is changed
		if($strVal != $arrAfterEditRecordSet[$strKey])
		{
		$arrActualDiff[$strKey]['OV'] = $strVal; //old value
		$arrActualDiff[$strKey]['NV'] = $arrAfterEditRecordSet[$strKey]; //new value
		}	
	}
	
	return $arrActualDiff;
}


/**
 * saveAuditLog function will save all the calculated difference in pre-defined format for reporting purposes
 * @param array 	$arrDiff 				Associated array that is return of maintainAudit function
 * @param int		$intUfserIdEditingRecord	User id of user who edited the record
 * @param string	$strDbTable				database table name of which record is edited
 * @param int		$intPkId				Primaary key of the associated database table in above param
 * 
 */

function saveAuditLog($arrDiff, $intUserIdEditingRecord, $strDbTable, $intPkId, $intHistoryId)
{
	if($strDbTable == "transactions")
		{
			$result = selectFrom("SELECT refNumberIM FROM transactions where transID = '".$intPkId."' ") ;
			
			$strRefno = $result['refNumberIM'];
		}
	
	/**
	*Ticket No:10507 
	* Cliend : Global Exchange
	*account Number of Customer will be assign to ref Number table;
	
	**/
	
	if($strDbTable == "customer")
		{
			$result = selectFrom("SELECT accountName FROM customer where customerID = '".$intPkId."' ") ;
			
				$strRefno = $result['accountName'];
		}
		
	if($strDbTable == "accounts")
		{
			$result = selectFrom("SELECT accounNumber FROM accounts where id = '".$intPkId."' ") ;
			$strRefno = $result['accounNumber'];
		}
	
	$strChangeLog = addslashes(serialize($arrDiff));

	if($strDbTable == "transactions")
	{
 
		$strInsertSql = "INSERT INTO ".TBL_AUDIT_MODIFY_HISTORY." 
						(pk_id, table_name, change_log, userid, history_id, datetime, ref_no)
						VALUES
						('$intPkId', '$strDbTable', '$strChangeLog', '$intUserIdEditingRecord', $intHistoryId, '".date("Y-m-d H:i:s")."', '".$strRefno."')";
	
	 }
	 else if($strDbTable == "accounts")
	{
	
	
	$querActivityId1 =  "select activity_id from login_activities where 	login_history_id = '".$intHistoryId."'	AND	action_for = '".$intPkId."'	order by `activity_id` DESC limit 1	";
	$querActivityId = mysql_query($querActivityId1);
	$getActivityId = mysql_fetch_array($querActivityId);	
	
		$strInsertSql = "INSERT INTO ".TBL_AUDIT_MODIFY_HISTORY." 
						(pk_id, table_name, change_log, userid, history_id, datetime, ref_no,id_activity_id)
						VALUES
						('$intPkId', '$strDbTable', '$strChangeLog', '$intUserIdEditingRecord', $intHistoryId, '".date("Y-m-d H:i:s")."', '".$strRefno."', '".$getActivityId['activity_id']."')";
	
	}  
	else if($strDbTable == "account_details")
	{
	
	
	$querActivityId1 =  "select activity_id from login_activities where 	login_history_id = '".$intHistoryId."'	AND	action_for = '".$intPkId."'	order by `activity_id` DESC limit 1	";
	$querActivityId = mysql_query($querActivityId1);
	$getActivityId = mysql_fetch_array($querActivityId);	
	
		$strInsertSql = "INSERT INTO ".TBL_AUDIT_MODIFY_HISTORY." 
						(pk_id, table_name, change_log, userid, history_id, datetime, ref_no,id_activity_id)
						VALUES
						('$intPkId', '$strDbTable', '$strChangeLog', '$intUserIdEditingRecord', $intHistoryId, '".date("Y-m-d H:i:s")."', '".$strRefno."', '".$getActivityId['activity_id']."')";
	 
	} 
	else
	{
	
	
		$strInsertSql = "INSERT INTO ".TBL_AUDIT_MODIFY_HISTORY." 
						(pk_id, table_name, change_log, userid, history_id, datetime,ref_no)
						VALUES
						('$intPkId', '$strDbTable', '$strChangeLog', '$intUserIdEditingRecord', $intHistoryId, '".date("Y-m-d H:i:s")."', '".$strRefno."')";
	
	}
	if(insertInto($strInsertSql))
		return true;
	return false;
	
}
 
/**
 * This function will be called from final interface and will called other sub function available in this interface
 * @param array 	$arrOldData 			Associated array that contain before edit record set
 * @param int		$intUserIdEditingRecord	User id of user who edited the record
 * @param string	$strDbTable				database table name of which record is edited
 * @param string 	$strPkColumnName		Name of primary key column in above table
 * @param int		$intPkId				Primaary key of the associated database table in above param
 * @param int 		$intHistoryId			History Id of the login history table
 */
function logChangeSet($arrOldData, $intUserIdEditingRecord, $strDbTable, $strPkColumnName, $intPkId, $intHistoryId)
{
	//debug_print_backtrace(); 

	$arrLatestData = getRowData($strDbTable, $strPkColumnName, $intPkId);
	if(!$arrLatestData)
		return false;
	 
	$arrDifference = maintainAudit($arrOldData, $arrLatestData);
	
	if(empty($arrDifference))
		return false;
 
	saveAuditLog($arrDifference, $intUserIdEditingRecord, $strDbTable, $intPkId, $intHistoryId);
	 
}

function saveAuditViewLog($intHistoryId, $activity, $actionFor,$strDbTable, $description)
{
	if($strDbTable == "transactions")
			{
				$result = selectFrom("SELECT refNumberIM FROM transactions where transID = '".$actionFor."' ") ;
				
					$ref_no = $result['refNumberIM'];
				
							
			$strInsertSql = "INSERT INTO audit_view_history 
									(login_history_id, activity, activity_time,action_for,table_name, description)
									VALUES
									('".$intHistoryId."','".$activity."', '".date("Y-m-d H:i:s")."', '".$ref_no."','".$strDbTable."','".$description."' )";
			}
			else{

$strInsertSql = "INSERT INTO audit_view_history 
						(login_history_id, activity, activity_time,action_for,table_name, description)
						VALUES
						('".$intHistoryId."','".$activity."', '".date("Y-m-d H:i:s")."', '".$actionFor."','".$strDbTable."','".$description."' )";
				}
						insertInto($strInsertSql);

} 