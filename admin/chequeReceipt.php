<?php

	if(empty($_REQUEST["oi"]))
	{
		echo "<h1>Invalid Request!</h1>";
		exit;
	}
	
	
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];

	$arrOrderData = selectFrom("select o.*, firstname, middlename, lastname from cheque_order o, customer c where order_id = '".$_REQUEST["oi"]."' and c.customerID = o.customer_id");
	
	$strOrderStatusLine = "";
	if($arrOrderData["status"] == "NG" || $arrOrderData["status"] == "AR")
	{
		$strOrderStatusLine = "<b>Cheque will be clear on</b><br />".$arrOrderData["estimated_date"];
	}
	elseif($arrOrderData["status"] == "ID")
	{
		$arrPaidDateTime = "";
		if(!empty($arrOrderData["paid_by"]))
		{
			$creator = explode("|",$arrOrderData["paid_by"]);
			$arrCreatorDetail = selectFrom("select username from admin where userid='".$creator[0]."'");
		}
		
		$strOrderStatusLine = "<b>Cheque is paid on</b><br />".date("d/m/Y",strtotime($arrOrderData["paid_on"]))."<br />";
		$strOrderStatusLine .= "<b>Teller:</b>&nbsp;".$arrCreatorDetail["username"]." / <b>Time:</b> ".date("H:i",strtotime($arrOrderData["paid_on"]));
	}
	else
	{
		$strOrderStatusLine = "<b>Cheque Order Status</b><br />".$arrChequeStatues[$arrOrderData["status"]];
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cheque Order Receipt</title>
<script language="javascript" src="jquery.js"></script>
<script>
	$(document).ready(function(){
		$("#printbtn").click(function(){
			$(this).hide();
			print();
		});
	});
</script>
<style>
td{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#000000;
}
</style>
</head>
<body>
<table border="1" bordercolor="#4C006F" cellspacing="0" cellpadding="0" width="225">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="1" cellpadding="3" id="table">
				<tr>
				   <td colspan="4" align="center" bgcolor="#FFFFFF">
						<img height='50' alt='' src='<?=CONFIG_LOGO?>' width='100' />
				   </td>
				</tr>
				<tr>
					<td colspan="4" align="center" bgcolor="#FFFFFF">
						<h2>Exchange Cheque</h2>
						<b><?=$arrOrderData["cheque_ref"].":".date("d/m/Y H:i")?></b>
						<br /><br />
					</td>
				</tr>
		
				<tr>
					<td scope="col"><b>Date:</b></td>
					<td scope="col"><?=date("d/m/Y",strtotime($arrOrderData["created_on"]))?></td>
				</tr>
				<tr>
					<td scope="col"><b>Teller:</b></td>
					<td scope="col"><?=$_SESSION["loggedUserData"]["username"]?></td>
				</tr>
				<tr>
					<td colspan="2"><hr width="100%" style="border-style:dotted" /></td>
				</tr>
				
				<tr>
					<td scope="col" colspan="2">
						<i><b>Customer</b></i><br />
						&nbsp;&nbsp;<?=$arrOrderData["firstname"]." ".$arrOrderData["middlename"]." ".$arrOrderData["lastname"]?>
					</td>
				</tr>
				
				<tr>
					<td scope="col"><i><b>Bank:</b></i></td>
					<td scope="col"><?=$arrOrderData["bank_name"]?></td>
				</tr>
		
				<tr>
					<td scope="col"><i><b>Sort Code:</b><i></td>
					<td scope="col"><?=$arrOrderData["branch"]?></td>
				</tr>
				<tr>
					<td scope="col"><i><b>Cheque Number:</b></i></td>
					<td scope="col"><?=$arrOrderData["cheque_no"]?></td>
				</tr>
				<tr><td><br /><br /><br /></td></tr>
				<tr>
					<td align="right"><b>Cheque Value:</b></td>
					<td align="right"><?=$arrOrderData["cheque_amount"]." ".$arrOrderData["cheque_currency"]?></td>
				</tr>
				<tr>
					<td align="right"><b>Fee:</b></td>
					<td align="right"><?=$arrOrderData["fee"]." ".$arrOrderData["cheque_currency"]?></td>
				</tr>
				<tr>
					<td colspan="2" align="right" valign="bottom"><hr width="80%" /></td>
				</tr>
				<tr>
					<td align="right"><b>Total:</b></td>
					<td align="right"><?=$arrOrderData["paid_amount"]." ".$arrOrderData["cheque_currency"]?></td>
				</tr>
				
				
				<tr>
					<td align="center" colspan="2">
						<br /><br />
						<?=$strOrderStatusLine?>
						<br />
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<br /><br />
						<hr noshade="noshade" size="1" width="100%" />
						<b>Customer Signature</b>
						<br /><br />
						<hr noshade="noshade" size="1" width="100%" />
						<b>Authorized</b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<input type="button" id="printbtn" value="Print Order Receipt" />

</body>
</html>