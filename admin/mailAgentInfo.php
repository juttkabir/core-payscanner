<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
dbConnect();
$username=loggedUser();
$agentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$_GET["userID"]."'");
if ($_GET["caller"] != ""){
	$supperAgentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$agentDetails["parentID"]."'");
}
?>
<html>
<head>
	<title>View Agent/Distributor Details</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">
		<table width="100%"><tr><td><strong><font color="#000000" size="2">View <? if ($_GET["caller"] == "") echo "Super"; else echo "Sub";?> Agent/Correspondent Details</font></strong></td><td><a href="javascript:window.close()"><font size="4" color="#ffffff"><b>X</b></font></a>&nbsp;</td></tr></table></td>
  </tr>
  <tr>
    <td align="center">
		<table width="426" border="0" cellspacing="1" cellpadding="2" align="center">
		<?
			/*if($_GET["caller"] != ""){
		?>
		<tr>
			<td colspan="2" bgcolor="#006699" height="20"><font color="#FFFFFF"><b>Super Agent</b></font></td>
		</tr>
       <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Branch Manager</strong></font></td>
            <?
            $branchMngr = selectFrom("select username from ".TBL_ADMIN_USERS." where userID='".$supperAgentDetails["parentID"]."'");
            ?>
            <td width="256"><?=stripslashes($branchMngr["username"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Agent Company Name*</strong></font></td>
            <td width="256"><?=stripslashes($supperAgentDetails["agentCompany"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Agent Contact Person*</strong></font></td>
            <td><?=stripslashes($supperAgentDetails["agentContactPerson"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Company Director*</strong></font></td>
            <td><?=stripslashes($supperAgentDetails["agentCompDirector"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Currency*</strong></font></td>
            <td><?=$agentDetails["supperAgentCurrency"]; ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Account Limit*</strong></font></td>
            <td><?=stripslashes($supperAgentDetails["agentAccountLimit"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            
          <td width="159"><font color="#005b90"><strong>Commission Percentage/Fee*</strong></font></td>
            <td><?=stripslashes($supperAgentDetails["agentCommission"]); ?></td>
        </tr>
		<tr>
			<td colspan="2" bgcolor="#006699" height="20"><font color="#FFFFFF"><b>Sub Agent</b></font></td>
		</tr>
		<?
			}
		if ($_GET["caller"] == ""){ */
		?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Branch Manager</strong></font></td>
            <td width="256"><?=stripslashes($agentDetails["agentCompany"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Company Name</strong></font></td>
            <td width="256"><?=stripslashes($agentDetails["agentCompany"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Agent Contact Person</strong></font></td>
            <td><?=stripslashes($agentDetails["agentContactPerson"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Address Line 1</b></font></td>
            <td><?=stripslashes($agentDetails["agentAddress"]); ?></td>
        </tr><? if (trim($agentDetails["agentAddress2"]) != ""){ ?>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Address Line 2</b></font></td>
            <td><?=stripslashes($agentDetails["agentAddress2"]); ?></td>
        </tr><? } ?>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Country</b></font></td>
            <td><?=$agentDetails["agentCountry"]; ?></td>
        </tr>
		<? if ($agentDetails["Country"] != "United States" && $agentDetails["Country"] != "Canada"){ ?>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>City</b></font></td>
            <td><?=$agentDetails["agentCity"];?></td>
        </tr><? } else {
		?><tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>
              <? if ($agentDetails["Country"] == "United States") echo "Zip"; else echo "Postal"; ?>
              Code</b></font></td>
            <td><?=stripslashes($agentDetails["agentZip"]); ?></td>
        </tr><?
		} ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Phone</strong></font></td>
            <td><?=stripslashes($agentDetails["agentPhone"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Fax</strong></font></td>
            <td><?=stripslashes($agentDetails["agentFax"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Email</strong></font></td>
            <td><?=stripslashes($agentDetails["email"]); ?></td>
        </tr><? if ($_GET["caller"] == ""){ ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Company URL</strong></font></td>
            <td><?=stripslashes($agentDetails["agentURL"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>MSB Number</strong></font></td>
            <td><?=stripslashes($agentDetails["agentMSBNumber"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>MSB Expiry</strong></font></td>
            <td><? echo $agentDetails["agentMCBExpiry"]; ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Company Registration Number</strong></font></td>
            <td><?=stripslashes($agentDetails["agentCompRegNumber"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Company Director</strong></font></td>
            <td><?=stripslashes($agentDetails["agentCompDirector"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Director Address</strong></font></td>
            <td><?=stripslashes($agentDetails["agentDirectorAdd"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><strong>Director Proof ID</strong></font></td>
            <td valign="top"><?=$agentDetails["agentProofID"]; ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>ID Expiry</strong></font></td>
            <td><?=$agentDetails["agentIDExpiry"]; ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong> Document Provided</strong></font></td>
            <td><?=$agentDetails["agentDocumentProvided"]; ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Bank</strong></font></td>
            <td><?=stripslashes($agentDetails["agentBank"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Account Name</strong></font></td>
            <td><?=stripslashes($agentDetails["agentAccountName"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Account Number</strong></font></td>
            <td><?=stripslashes($agentDetails["agentAccounNumber"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Branch Code</strong></font></td>
            <td><?=stripslashes($agentDetails["agentBranchCode"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Account Type</strong></font></td>
            <td><?=$agentDetails["agentAccountType"];?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Currency*</strong></font></td>
            <td><?=$agentDetails["agentCurrency"]; ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Account Limit</strong></font></td>
            <td><?=stripslashes($agentDetails["agentAccountLimit"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            
          <td width="159"><font color="#005b90"><strong>Commission Percentage/Fee</strong></font></td>
            <td><?=stripslashes($agentDetails["agentCommission"]); ?></td>
        </tr>
		<? } ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Status</strong></font></td>
            <td><?=$agentDetails["agentStatus"];?></td>
        </tr>
	<?	if ($agentDetails["agentStatus"] == 'Disabled') {  ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Disable Reason</strong></font></td>
            <td><?=$agentDetails["disableReason"];?></td>
        </tr>
	<?	} else if ($agentDetails["agentStatus"] == 'Suspended') {  ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Suspension Reason</strong></font></td>
            <td><?=$agentDetails["suspensionReason"];?></td>
        </tr>
	<?	}  ?>
      </table>
	</td>
  </tr>
</table>
</body>
</html>
