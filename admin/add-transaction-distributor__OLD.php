<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
$agentID = $_SESSION["loggedUserData"]["userID"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
///////////////////////History is maintained via method named 'activities'

$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
	$offset = 0;
		
if($limit == 0)
	$limit=50;
	
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
	
$nxt = $offset + $limit;
$prv = $offset - $limit;

	
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";



$transType   = "";
$transStatus = "";
$Submit      = "";
$transID     = "";
$by          = "";



	
if($_REQUEST["Submit"]!="")
	$Submit = $_REQUEST["Submit"];
		
if($_REQUEST["transID"]!="")
	$transID = $_REQUEST["transID"];


if($_REQUEST["searchBy"]!="")
	$by = $_REQUEST["searchBy"];

$trnsid = $_REQUEST["trnsid"];


$strForeignAmountCurrency = $_REQUEST["foreignAmountCurrency"];
$strLocalAmountCurrency = $_REQUEST["localAmountCurrency"];



$ttrans = $_REQUEST["totTrans"];
if($_REQUEST["btnAction"] != "")
{
	/**
	 * updatiung the transactions with the distributor(s)
	 */
	
	if(!empty($_REQUEST["distribut"]) && count($_REQUEST["transBenIds"]) > 0)
	{
		/**
		 * Getting the parent Id of the agent to store in transactions table
		 */	
		$benParentIdRs = selectFrom("select parentID from ".TBL_ADMIN_USERS." where userID = ".$_REQUEST["distribut"]);
	
		foreach ($_REQUEST["transBenIds"] as $dk => $dv)
		{
			/**
			 * Cheking which trasactions are assigned to distributors
			 * If they contain any distributor then update the transaction 
			 */
			if(!empty($dv))	
			{
				
				$updateTransSql = "Update ".TBL_TRANSACTIONS." set benAgentID=".$_REQUEST["distribut"].", benAgentParentID = ".$benParentIdRs["parentID"] ." where transID=".$dk;
				update($updateTransSql);
				
				activities($_SESSION["loginHistoryID"],"UPDATION",$dk,TBL_TRANSACTIONS,"Updating the distributor to ".$_REQUEST["distribut"]);	
			}
		}
	}
}

$query = "select * from ". TBL_TRANSACTIONS . " as t, bankDetails as d where 1";
$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, bankDetails as d where 1";

if($Submit == "Search")
{
	if($transID != "")
	{
		switch($by)
		{
			case 0:
			{		
				$query = "select * from ". TBL_TRANSACTIONS . " as t, bankDetails as d where 1 ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, bankDetails as d where 1 ";
				
				$query .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
				$queryCnt .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
				break;
			}
			case 1:
			{
				$query = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c, bankDetails as d where t.customerID = c.customerID and t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c, bankDetails as d where t.customerID = c.customerID and t.createdBy != 'CUSTOMER' ";
							
				$query .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
				$queryCnt .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
				break;
			}
			case 2:
			{
				$query = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b, bankDetails as d where t.benID = b.benID and t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b, bankDetails as d where t.benID = b.benID and t.createdBy != 'CUSTOMER' ";
					
				$query .= " and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
				$queryCnt .=" and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
				break;
			}
		}
	}
}

	$fromDate = $_REQUEST["fYear"] . "-" . $_REQUEST["fMonth"] . "-" . $_REQUEST["fDay"];
	$toDate = $_REQUEST["tYear"] . "-" . $_REQUEST["tMonth"] . "-" . $_REQUEST["tDay"];
	$filters = " and (t.transDate BETWEEN '$fromDate 00:00:00' AND '$toDate 23:59:59')";
	
	
	
	
	/* Filter option for Total Amount */
	if(!empty($_REQUEST["totalAmount"]))
		$filters .= " and t.totalAmount = '".trim($_REQUEST["totalAmount"])."' ";
	/* Filter Option for Local Total Amount */
	if(!empty($_REQUEST["totalLocalAmount"]))
		$filters .= " and t.localAmount = '".trim($_REQUEST["totalLocalAmount"])."' ";
	/* bank Name search filter */
	if(!empty($_REQUEST["bankName"]))
		$filters .= " and d.bankName LIKE '".trim($_REQUEST["bankName"])."' ";
	/* Filter option for created by */
	if(!empty($_REQUEST["createdBy"]))
		$filters .= " and t.addedBy = '".trim($_REQUEST["createdBy"])."' ";
	
	
	/**
	 *  Changing the logic to assign distributor 
	 *  Now in this queue only transaction which are in authorize status will appear
	 *	$query .= " and (t.transStatus ='Pending' and t.transType = 'Bank Transfer' and t.benAgentID = 0) ";
	 *	$queryCnt .= " and (t.transStatus ='Pending' and t.transType = 'Bank Transfer' and t.benAgentID = 0) ";
	 */
	$query .= " and ((t.transStatus ='Authorize' or t.transStatus ='Amended') and t.transType = 'Bank Transfer' and t.benAgentID = 0) ";
	$queryCnt .= " and ((t.transStatus ='Authorize' or t.transStatus ='Amended') and t.transType = 'Bank Transfer' and t.benAgentID = 0) ";

	/**
	 * Inserting the local and foreign currency 
	 */
	$query .= " and t.currencyFrom ='$strLocalAmountCurrency' and t.currencyTo = '$strForeignAmountCurrency' ";
	$queryCnt .= " and t.currencyFrom ='$strLocalAmountCurrency' and t.currencyTo = '$strForeignAmountCurrency' ";
	
	/* Search Filters */
	$query .= $filters;
	$queryCnt .= $filters;
	
	$query .= " order by t.transDate DESC";
	
	$query .= " LIMIT $offset , $limit";
	
	//debug($query);
	
	$contentsTrans = selectMultiRecords($query);
	
	$allCount = countRecords($queryCnt);
	
	if($transID != "" && $by != 0)
	{	
		$allCount = $allCount;
		$other = $offset + $limit;
		if($other > count($contentsTrans))
		{
			if($offset < count($contentsTrans))
			{
				$offset2 = 0;
				$limit2 = $offset + $limit - count($contentsTrans);	
			}
			elseif($offset >= count($contentsTrans))
			{
				$offset2 = $offset - $countOnlineRec;
				$limit2 = $limit;
			}
		}
	}
	
	$strUrlParam = "&totalAmount=".trim($_REQUEST["totalAmount"]).
				   "&totalLocalAmount=".trim($_REQUEST["totalLocalAmount"]).
				   "&bankName=".trim($_REQUEST["bankName"]).
				   "&createdBy=".trim($_REQUEST["createdBy"]).
				   "&fYear=".$_REQUEST["fYear"].
				   "&fMonth=".$_REQUEST["fMonth"].
				   "&fDay=".$_REQUEST["fDay"].
				   "&tYear=".$_REQUEST["tYear"].
				   "&tMonth=".$_REQUEST["tMonth"].
				   "&tDay=".$_REQUEST["tDay"];
				   
?>
<html>
<head>
<title>Add Distributor to Transactions</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script src="jquery.js"></script>
<script language="javascript">
<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function showTotalAmounts(cbid, foreign, local)
{
	var currenctFAmount = Number($("#tfa").val());
	var currenctLAmount = Number($("#tla").val());

	if($("#transBenIds"+cbid).attr("checked"))
	{
		currenctFAmount += foreign;
		currenctLAmount += local;
	}
	else
	{
		currenctFAmount -= foreign;
		currenctLAmount -= local;
	}
	
	currenctFAmount = currenctFAmount.toFixed(4);
	currenctLAmount = currenctLAmount.toFixed(4)
	
	document.getElementById("tfa").value = currenctFAmount;	
	document.getElementById("tla").value = currenctLAmount;	
	
	initTotal();
	
}

function initTotal()
{
	if(document.getElementById("tfa"))
		document.getElementById("totalForeignAmount").innerHTML = document.getElementById("tfa").value;
	if(document.getElementById("tla"))
		document.getElementById("totalLocalAmount").innerHTML = document.getElementById("tla").value;
}

function checkAllCbs()
{
	var checkBoxIdStr = document.getElementById("ca").value;
	var checkBoxArray = checkBoxIdStr.split(",");

	if($("#checkAll").attr("checked"))
	{
		for(i = 0; i < checkBoxArray.length; i++)
		{
			if(document.getElementById("transBenIds"+checkBoxArray[i]))
			{
				if(!$("#transBenIds"+checkBoxArray[i]).attr("checked"))
					document.getElementById("transBenIds"+checkBoxArray[i]).click();
			}
		}
	}
	else
	{
		for(i = 0; i < checkBoxArray.length; i++)
		{
			if(document.getElementById("transBenIds"+checkBoxArray[i]))
			{
				if($("#transBenIds"+checkBoxArray[i]).attr("checked"))
					document.getElementById("transBenIds"+checkBoxArray[i]).click();
			}
		}
	}

}

function checkInput()
{
	
	if(document.getElementById("distribut").selectedIndex == 0)
	{	
		alert("Please select any distributor.");
		document.getElementById("distribut").focus();
		return false;
	}
	
	return true;
}
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
-->
</script>
<style type="text/css">
<!--
	.style1 {color: #005b90}
	.style2 {color: #005b90; font-weight: bold; }
-->
</style>
</head>
<body onLoad="initTotal();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Assign Transactions</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="<?=$PHP_SELF?>?action=<? echo $_GET["action"]?>" method="get" name="search">
      <tr>
         <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
      </tr>
      <tr>
  		<td nowrap align="center"> 
			<b>From </b>
			<? 
			$month = date("m");
			
			$day = date("d");
			$year = date("Y");
			?>
			
			<SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
			<?
					for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
			?>
			</select>
			<script language="JavaScript">
				SelectOption(document.search.fDay, "<?=(!empty($_POST["fDay"])?$_POST["fDay"]:"")?>");
			</script>
					<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
			  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
			  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
			  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
			  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
			  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
			  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
			  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
			  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
			  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
			  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
			  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
			  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			</SELECT>
			<script language="JavaScript">
				SelectOption(document.search.fMonth, "<?=(!empty($_POST["fMonth"])?$_POST["fMonth"]:"")?>");
			</script>
			<SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
			  <?
						$cYear=date("Y");
						for ($Year=2004;$Year<=$cYear;$Year++)
							{
								echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
							}
					  ?>
			</SELECT> 
					<script language="JavaScript">
				SelectOption(document.search.fYear, "<?=(!empty($_POST["fYear"])?$_POST["fYear"]:"")?>");
			</script>
			<b>To	</b>
			<SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
			  <?
				for ($Day=1;$Day<32;$Day++)
					{
						if ($Day<10)
						$Day="0".$Day;
						echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
					}
			  ?>
			</select>
					<script language="JavaScript">
				SelectOption(document.search.tDay, "<?=(!empty($_POST["tDay"])?$_POST["tDay"]:"")?>");
			</script>
					<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
	
			  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
			  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
			  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
			  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
			  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
			  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
			  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
			  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
			  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
			  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
			  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
			  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			</SELECT>
					<script language="JavaScript">
				SelectOption(document.search.tMonth, "<?=(!empty($_POST["tMonth"])?$_POST["tMonth"]:"")?>");
			</script>
			<SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
			<?
				$cYear=date("Y");
				for ($Year=2004;$Year<=$cYear;$Year++)
				{
					echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
				}
			?>
			</SELECT>
					<script language="JavaScript">
			SelectOption(document.search.tYear, "<?=(!empty($_POST["tYear"])?$_POST["tYear"]:"")?>");
			</script>
			<br /><br />
			<input name="transID" type="text" id="transID" value="<?=$transID?>">
			&nbsp;&nbsp;		  
			<select name="searchBy" >
				<option value=""> - Search By - </option>
				<option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
				<option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By Sender Name</option>
				<option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
			</select>
			<script language="JavaScript">SelectOption(document.forms[0].searchBy, "<?=$by?>");</script>
			<br /><br />
			<b>Local Amount Currency</b>
			&nbsp;&nbsp;
			<?
				$strLacSql = "select distinct(currencyFrom) from ".TBL_TRANSACTIONS;
				$arrLacData = selectMultiRecords($strLacSql);
			?>
			<select name="localAmountCurrency">
			<? 
				foreach($arrLacData as $strKey => $strVal) 
			   	{
					if($strVal["currencyFrom"] == $_REQUEST["localAmountCurrency"])
					{
			?>
						<option value="<?=$strVal["currencyFrom"]?>" selected="selected"><?=$strVal["currencyFrom"]?></option>
			<?
					}
					else
					{
			?>
						<option value="<?=$strVal["currencyFrom"]?>"><?=$strVal["currencyFrom"]?></option>
			<? 
					}			
			   } 
			?>
			</select>
			&nbsp;&nbsp;
			<b>Foreign Amount Currency</b>
			&nbsp;&nbsp;
			<?
				$strFacSql = "select distinct(currencyTo) from ".TBL_TRANSACTIONS;
				$arrFacData = selectMultiRecords($strFacSql);
			?>
			<select name="foreignAmountCurrency">
			<? 
				foreach($arrFacData as $strKey => $strVal) 
				{
					if($strVal["currencyTo"] == $_REQUEST["foreignAmountCurrency"])
					{
			?>
						<option value="<?=$strVal["currencyTo"]?>" selected="selected"><?=$strVal["currencyTo"]?></option>
			<?
					}
					else
					{
			?>
						<option value="<?=$strVal["currencyTo"]?>"><?=$strVal["currencyTo"]?></option>
			<? 
					}
				} 
			?>
			</select>
			<br />
			<br />
			
			<b>Total Amount</b>&nbsp;<input type="text" name="totalAmount" value="<?=$_REQUEST["totalAmount"]?>" />
			&nbsp;&nbsp;&nbsp;
			<b>Total Local Amount</b>&nbsp;<input type="text" name="totalLocalAmount" value="<?=$_REQUEST["totalLocalAmount"]?>" />
			<br /><br />
			<b>Bank Name</b>&nbsp;<input type="text" name="bankName" value="<?=$_REQUEST["bankName"]?>" />
			&nbsp;&nbsp;&nbsp;
			<b>Created By</b>&nbsp;<input type="text" name="createdBy" value="<?=$_REQUEST["createdBy"]?>" />
			<br /><br />
			<input type="submit" name="Submit" value="Search"></td>
	      </tr>
	  </form>
    </table>
	<br>
	<br>
	<table width="725" border="1" cellpadding="0" bordercolor="#666666">
      <form action="<?=$PHP_SELF?>?action=<? echo $_GET["action"]?>" method="post" name="trans" onSubmit="return checkInput();">
       <? if ($allCount > 0){ ?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>&foreignAmountCurrency=<?=$_REQUEST["foreignAmountCurrency"]?>&localAmountCurrency=<?=$_REQUEST["localAmountCurrency"].$strUrlParam?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$prv&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>&foreignAmountCurrency=<?=$_REQUEST["foreignAmountCurrency"]?>&localAmountCurrency=<?=$_REQUEST["localAmountCurrency"].$strUrlParam?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$nxt&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>&foreignAmountCurrency=<?=$_REQUEST["foreignAmountCurrency"]?>&localAmountCurrency=<?=$_REQUEST["localAmountCurrency"].$strUrlParam?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <!--a href="<?php print $PHP_SELF . "?"."transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>&foreignAmountCurrency=<?=$_REQUEST["foreignAmountCurrency"]?>&localAmountCurrency=<?=$_REQUEST["localAmountCurrency"].$strUrlParam?>"><font color="#005b90">Last</font></a -->&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo count($contentsTrans)?> records to <? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?>.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0)
		{
			$allIds = "";
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="725" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?></span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			   <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Local Amount </span></td>
			  <td><span class="style1">Sender Name </span></td>
			  <td><span class="style1">Beneficiary Name </span></td>
			  <td><span class="style1">Bank Name</span></td>
			  <td width="100"><span class="style1">Created By </span></td>
			  <td width="146" align="center">
			  	<input type="checkbox" title="Check All" value="Y" name="checkAll" id="checkAll" onClick="checkAllCbs();" />
			  </td>
			</tr>
		    <? for($i=0;$i<count($contentsTrans);$i++)
			{
				$allIds = $allIds . ",".$contentsTrans[$i]["transID"];
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["totalAmount"] .  " " . $contentsTrans[$i]["currencyFrom"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["localAmount"] .  " " . $contentsTrans[$i]["currencyTo"]?></td>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<? 
					}else {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  <? } ?>
					
				<td width="100" bgcolor="#FFFFFF">
				<? 
					$bankNameSql = "select bankName from bankDetails where transID =".$contentsTrans[$i]["transID"];
					$bankNameData = selectFrom($bankNameSql);
					echo ucfirst($bankNameData["bankName"]);
				?>
				</td>
				  <?
				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>


				  <td align="center" bgcolor="#FFFFFF">
				   <input type="checkbox" id="transBenIds<? echo $contentsTrans[$i]["transID"]?>" name="transBenIds[<? echo $contentsTrans[$i]["transID"]?>]" value="Y" onClick="showTotalAmounts(<?=$contentsTrans[$i]["transID"]?>,<?=$contentsTrans[$i]["totalAmount"]?>, <?=$contentsTrans[$i]["localAmount"]?>);" />
				  </td>  
				</tr>
				<?
			}
			?>
			
				<tr bgcolor="#FFFFFF">
					<td align="center" colspan="7">
						Total Amount =
						<span id="totalForeignAmount"></span>&nbsp;<?=$_REQUEST["localAmountCurrency"]?>
						&nbsp;|&nbsp;
						Total Local Amount = 
						<span id="totalLocalAmount"></span>&nbsp;<?=$_REQUEST["foreignAmountCurrency"]?>
						&nbsp;
						<input type="hidden" name="tfa" id="tfa" value="0" />
						<input type="hidden" name="tla" id="tla" value="0" />
						
						<input type="hidden" name="ca" id="ca" value="<?=$allIds?>" />							
					</td>
					<td align="right" colspan="4">
						
						<select name="distribut" id="distribut" style="font-family:verdana; font-size: 11px; width:150">	
							<option value=""> - Select One - </option>
							<?
								$transType = "Bank Transfer";
								$distributors = "select * from admin where parentID > 0 and adminType='Agent' and isCorrespondent != 'N'  and isCorrespondent != '' and PPType = '' and agentStatus = 'Active' order by userID";				
								$ida = selectMultiRecords($distributors);
								
								
								for ($j=0; $j < count($ida); $j++)
								{
									$authority = $ida[$j]["authorizedFor"];
									$toCountry = $ida[$j]["IDAcountry"];
									$isDefault = $ida[$j]["defaultDistrib"];
						
									if($ida[$j]["userID"] == $_REQUEST["distribut"])
									{
								?>
									<option value="<? echo $ida[$j]["userID"]; ?>" selected="selected">
									<? echo $ida[$j]["username"]." [".$ida[$j]["name"]."]"; ?>
									</option>
								 <?
								 	}
									else
									{
								?>
									<option value="<? echo $ida[$j]["userID"]; ?>">
									<? echo $ida[$j]["username"]." [".$ida[$j]["name"]."]"; ?>
									</option>
								<?	
									}
								}
							?>
						</select>
						
					
						<input type="hidden" name="foreignAmountCurrency" value="<?=$_REQUEST["foreignAmountCurrency"]?>" />
						<input type="hidden" name="localAmountCurrency" value="<?=$_REQUEST["localAmountCurrency"]?>" />
						<!--input name="transID" type="hidden" id="transID" value="<?=$_REQUEST["transID"]?>" />
						<input name="searchBy" type="hidden" id="searchBy" value="<?=$_REQUEST["searchBy"]?>" /-->
						
						<input type='hidden' name='totTrans' value='<?echo $i?>'>
						<input name="btnAction" type="submit"  value="Assign Distributors">
					</td>
				</tr>
				
				<tr bgcolor="#FFFFFF">
					<td align="center" colspan="11">&nbsp;
												
					</td>
				</tr>
			<?
			} 
			?>
          </table></td>
        </tr>


          <tr>
            <td  bgcolor="#000000"> 
            	<table width="725" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if (count($contentsTrans) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+ count($contentsTrans) );?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>&foreignAmountCurrency=<?=$_REQUEST["foreignAmountCurrency"]?>&localAmountCurrency=<?=$_REQUEST["localAmountCurrency"].$strUrlParam?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$prv&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>&foreignAmountCurrency=<?=$_REQUEST["foreignAmountCurrency"]?>&localAmountCurrency=<?=$_REQUEST["localAmountCurrency"].$strUrlParam?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$nxt&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>&foreignAmountCurrency=<?=$_REQUEST["foreignAmountCurrency"]?>&localAmountCurrency=<?=$_REQUEST["localAmountCurrency"].$strUrlParam?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <!--a href="<?php print $PHP_SELF . "?"."transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>&foreignAmountCurrency=<?=$_REQUEST["foreignAmountCurrency"]?>&localAmountCurrency=<?=$_REQUEST["localAmountCurrency"].$strUrlParam?>"><font color="#005b90">Last</font></a -->&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
            </td>
          </tr>

          <?
			} else {
		?>
          <tr>
            <td  align="center"> No Transaction found in the database having no distributor.
            </td>
          </tr>
          <?
			}
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>