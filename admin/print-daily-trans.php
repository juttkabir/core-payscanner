<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();

//include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$agentID = $_SESSION["loggedUserData"]["userID"];
$agentType = getAgentType();

$arr_transID = $_POST['checkbox'];
foreach ($arr_transID as $transID) {
	$arr = explode('/', $transID);
	?>
	<script type="text/javascript">
		window.open('print-confirmed-transaction-payscanner.php?customerID=<?=$arr[1]?>&transID=<?=$arr[0]?>', '_blank');
	</script>
	<?php
}

if(CONFIG_RECEIPT_PRINT_TRANSACTIONS_CUSTOMIZE_PAGE == "1")
{
	$recieptPage = CONFIG_RECEIPT_PRINT_TRANSACTIONS;
}elseif(CONFIG_CUSTOM_RECIEPT == "1"){
	$recieptPage = CONFIG_RECIEPT_PRINT;
} 
else {
	$recieptPage = "print-trans-details.php";
}
//$recieptPage = "print-daily-trans.php";

$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS_FOR_PRINT;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
	$limit=20;
	
	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";
	
$Submit      = "";
$transID     = "";
$by          = "";
	
if($_POST["Submit"]!="")
		$Submit = $_POST["Submit"];
	elseif($_GET["search"]!="") 
		$Submit=$_GET["search"];

//debug($_POST["checkbox"]);
//debug($_POST["transID"]);
		
if($_REQUEST["transID"]!="")
		$transID  = $_REQUEST["transID"];
	
	

if($_POST["searchBy"]!="")
		$by = $_SESSION["searchBy"] = $_POST["searchBy"];
	elseif($_GET["searchBy"]!="") 
		$by = $_SESSION["searchBy"] = $_GET["searchBy"];

	//$OR = "";
	
session_register("dfMonth");
session_register("dfDay");
session_register("dfYear");
session_register("dtMonth");
session_register("dtDay");
session_register("dtYear");


if($_POST["Submit"]=="Search")
{		
	$_SESSION["dfMonth"]="";
	$_SESSION["dfDay"]="";
	$_SESSION["dfYear"]="";
	
	$_SESSION["dtMonth"]="";
	$_SESSION["dtDay"]="";
	$_SESSION["dtYear"]="";
	
	
	$_SESSION["dfMonth"]=$_POST["fMonth"];
	$_SESSION["dfDay"]=$_POST["fDay"];
	$_SESSION["dfYear"]=$_POST["fYear"];
	
	$_SESSION["dtMonth"]=$_POST["tMonth"];
	$_SESSION["dtDay"]=$_POST["tDay"];
	$_SESSION["dtYear"]=$_POST["tYear"];
}

/**
 *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements 
 *  This condition executes when admin manager or branch manager logs in,
 *  keeping the check of showing the logged in users only the transactions of 
 *  their associated senders.
 */
if( defined("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS") && strstr(CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS,$agentType) )
{
   /**
	*  @var $extraCondition type string
	*  This varibale is used to define a condition 
	*/
	$extraCondition  = " and ( am.userID = '".$agentID."' ) ";
}

$acManagerFlagLabel = "Account Manager";
$acManagerFlag = false;
if(CONFIG_POPULATE_USERS_DROPDOWN=="1" && defined("CONFIG_ACCOUNT_MANAGER_COLUMN") && CONFIG_ACCOUNT_MANAGER_COLUMN!="0"){
	$custExtraFields = ", c.parentID as custParentID, am.name as accountManagerName ";
	$acManagerFlagLabel = CONFIG_ACCOUNT_MANAGER_COLUMN;
	$extraJoin = " LEFT JOIN ".TBL_ADMIN_USERS." as am ON am.userID = c.parentID  ";
	$acManagerFlag = true;
}


if($Submit =="Search")
{
	if($transID != "" || $_POST["transID"]!= "")
	{
		switch($by)
		{
			case 0:
			{
				$query = "select * from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where (t.refNumber = '".$transID."' OR  t.refNumberIM = '".$transID."') ".$extraCondition;
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where (t.refNumber = '".$transID."' OR  t.refNumberIM = '".$transID."') ".$extraCondition;
				
				//$contentsTrans = selectMultiRecords($query);
				//$allCount = countRecords($queryCnt);
				
				break;
			}
			case 1:
			{
				$queryonline = "select *  from ". TBL_TRANSACTIONS . " as t, ".TBL_CM_CUSTOMER." as cm where t.customerID =  cm.c_id and createdBy like 'CUSTOMER' ";
				$queryonlineCnt = "select *  from ". TBL_TRANSACTIONS . " as t, ".TBL_CM_CUSTOMER." as cm where t.customerID =  cm.c_id and createdBy like 'CUSTOMER' ";
				$queryonline .= " and (cm.firstName like '%".$transID."%' OR cm.lastName like '%".$transID."%')";
				$queryonlineCnt .= " and (cm.firstName like '%".$transID."%' OR cm.lastName like '%".$transID."%')";
				$queryonline .= " order by t.transDate";
				$queryonlineCnt .= " order by t.transDate";
				//$queryonline .= " LIMIT $offset , $limit";
				//$queryonlineCnt .= " LIMIT $offset , $limit";
				$onlinecustomer = selectMultiRecords($queryonline);
			
				$query = "select *  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where (c.firstName like '%".$transID."%' OR c.lastName like '%".$transID."%')".$extraCondition;
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where (c.firstName like '%".$transID."%' OR c.lastName like '%".$transID."%')".$extraCondition;
				
				//$contentsTrans = selectMultiRecords($query);
				//$allCount = countRecords($queryCnt);
				break;
			}
			case 2:
			{
				$queryonline = "select *  from ". TBL_TRANSACTIONS . " as t, ".TBL_CM_BENEFICIARY." as cb where t.benID =  cb.benID and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%') and createdBy like 'CUSTOMER'";
				$queryonlineCnt = "select *  from ". TBL_TRANSACTIONS . " as t, ".TBL_CM_BENEFICIARY." as cb where t.benID =  cb.benID and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%') and createdBy like 'CUSTOMER'";
				$queryonline .= " order by t.transDate";
				$queryonlineCnt .= " order by t.transDate";
				//$queryonline .= " LIMIT $offset , $limit";
				//$queryonlineCnt .= " LIMIT $offset , $limit";
				$onlinecustomer = selectMultiRecords($queryonline);
				$query = "select *  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".$extraJoin." where (b.firstName like '%".$transID."%' OR b.lastName like '%".$transID."%')".$extraCondition;
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".$extraJoin." where (b.firstName like '%".$transID."%' OR b.lastName like '%".$transID."%')".$extraCondition;

				
				//$contentsTrans = selectMultiRecords($query);
				//$allCount = countRecords($queryCnt);
				break;
			}
			case 3:
			{
				$query = "select * from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_ADMIN_USERS ." as u ON t.custAgentID =  u.userID ".$extraJoin." where u.username ='".$transID."' ".$extraCondition;
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_ADMIN_USERS ." as u ON t.custAgentID =  u.userID ".$extraJoin." where u.username ='".$transID."' ".$extraCondition;
				
				//$contentsTrans = selectMultiRecords($query);
				//$allCount = countRecords($queryCnt);
				break;
			}

		}

		/*$sambaCheck = selectFrom("select * from sambaida_check");
		if($sambaCheck["IDA_id"] == $agentID)
		{
			/*	if($agentType == "SUPI" || $agentType == "SUPAI" || $agentType == "SUPA")
				{
					$query .= " and custAgentID = '".$_SESSION["loggedUserData"]["userID"]."'";
				}
				elseif($agentType == "SUBI" || $agentType == "SUBAI" || $agentType == "SUBA")
				{
					$query .= " and (benAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR custAgentID = '".$_SESSION["loggedUserData"]["userID"]."') ";
				}/
				$contentsTrans = selectMultiRecords($query);
				$found = ($contentTrans["transID"] > 0 ? "Y" : "N");		
		}
		else
		{*/
				/*if($agentType == "SUPI" || $agentType == "SUPAI" || $agentType == "SUPA")
				{
					$query .= " and custAgentID = '".$_SESSION["loggedUserData"]["userID"]."'";
				}
				elseif($agentType == "SUBI" || $agentType == "SUBAI" || $agentType == "SUBA")
				{
					$query .= " and (benAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR custAgentID = '".$_SESSION["loggedUserData"]["userID"]."') ";
				}*/
			
				
				
		//}		
	}else{
		
		$query = "select *  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$extraCondition;
		$queryCnt = "select count(*)  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$extraCondition;
		}
			
				if($agentType == 'SUPA' || $agentType == 'SUBA' || $agentType == 'SUBAI' || $agentType == 'SUPAI'){
					
					$query .= "  and t.custAgentId =  $agentID";
					$queryCnt .= "  and t.custAgentId =  $agentID";
					
					}elseif($agentType == 'Branch Manager')
				{
					
				   $query .= "  and (t.benAgentParentID =  $agentID OR  custAgentParentID = '".$_SESSION["loggedUserData"]["userID"]."')";
					$queryCnt .= "  and (t.benAgentParentID =  $agentID OR  custAgentParentID = '".$_SESSION["loggedUserData"]["userID"]."')";
			 
			 		
						
				}
				
				if($_GET["fromDate"]!= ""){
				$fromDate = $_GET["fromDate"];
				}else{
				$fromDate = $_SESSION["dfYear"] . "-" . $_SESSION["dfMonth"] . "-" . $_SESSION["dfDay"];
				}
				if($_GET["toDate"]!= ""){
				$toDate = $_GET["toDate"];
				}else{
				$toDate = $_SESSION["dtYear"] . "-" . $_SESSION["dtMonth"] . "-" . $_SESSION["dtDay"];
  			}
			
			
	
	if( CONFIG_VIEW_ASSOCIATED_AGENT_TRANS == 1 )
	{
		$agentLinkQuery = selectFrom("select linked_Agent from ".TBL_ADMIN_USERS." where userID = '".$agentID."'");
		$linkedAgentArray = explode(",", $agentLinkQuery["linked_Agent"]);
		for($i = 0; $i < count($linkedAgentArray); $i++){
    		if(!empty($linkedAgentArray[$i]))
			{
				$agentIDArrayQuery = selectFrom("select userID from admin where username = '".$linkedAgentArray[$i]."'");
				$agentIDArray[$i] = $agentIDArrayQuery["userID"];
				$agentIDArray[$i] = "'".$agentIDArray[$i]."'";
  			}
		}
  
		if(count($agentIDArray) > 0){
			
			$query .= " and (";
			$queryCnt .= " and ( ";
			for($j = 0; $j < count($agentIDArray); $j++){
			
				if($j> 0){
			
					$query .= " or ";
					$queryCnt .= " or ";
				}
			
				$query .= "  t.custAgentID = (".$agentIDArray[$j].") ";
				$queryCnt .= "  t.custAgentID = (".$agentIDArray[$j].") ";
			}
			
			$query .= ")";
			$queryCnt .= ")";
		}
		
	}			
  /* This config added by Niaz Ahmad at 05-03-2009 @3168 Family Express*/
if(CONFIG_DISTRIBUTOR_ASSOCIATED_MANAGEMENT == "1"){
	$distLinkQuery = selectFrom("select linked_Distibutor from ".TBL_ADMIN_USERS." where userID = '".$agentID."'");
		$linkedDestArray = explode(",", $distLinkQuery["linked_Distibutor"]);
		
		for($i = 0; $i < count($linkedDestArray); $i++){
    		if(!empty($linkedDestArray[$i]))
			{
			   $linkDistId= selectFrom("select userID from ".TBL_ADMIN_USERS." where username = '".$linkedDestArray[$i]."'");
			  
				$destIDArray[$i] = $linkDistId["userID"];
				 $destIDArray[$i] = "'".$destIDArray[$i]."'";
				 //debug($agentIDArray[$i]);
  			}
		}
		//$linkedDistributorID = implode(",", $agentIDArray);
	if(count($destIDArray) > 0){
			
			$query .= " and (";
			$queryCnt .= " and ( ";
			for($k = 0; $k < count($destIDArray); $k++){
			
				if($k> 0){
			
					$query .= " or ";
					$queryCnt .= " or ";
				}
			
				$query .= "  t.benAgentID = (".$destIDArray[$k].") ";
				$queryCnt .= "  t.benAgentID = (".$destIDArray[$k].") ";
			}
			
			$query .= ")";
			$queryCnt .= ")";
		}
	
}

/* This config added by Niaz Ahmad at 20-03-2009 @4721 Family Express*/
if(CONFIG_SUPI_MANAGER_ADMIN_STAFF == "1" && COMPANY_NAME == "Muthoot Global"){
 if($agentType == "SUPI Manager" ){
  			
			$query 	  .= " and (t.transStatus = 'Authorize' or t.transStatus = 'Amended' or t.transStatus = 'Credited')";
			$queryCnt .= " and (t.transStatus = 'Authorize' or t.transStatus = 'Amended' or t.transStatus = 'Credited')";
			$query 	  .= " and transType = 'Bank Transfer' ";
			$queryCnt .= "and transType = 'Bank Transfer' ";
 
 }
 
} 			
			$queryDate = " (t.transDate >= '$fromDate 00:00:00' and t.transDate <= '$toDate 23:59:59')";

			//if($_POST["searchBy"] != 0 ){
				$query .=  " and $queryDate";
				$queryCnt .=  " and $queryDate";		
			//}
				$query .= " order by t.transDate";
			$queryCnt .= " order by t.transDate";
				
					$query .= " LIMIT $offset , $limit";
				//$queryCnt .= " LIMIT $offset , $limit";

				$contentsTrans = selectMultiRecords($query);
				$allCount = countRecords($queryCnt);
				
				//$found = ($contentTrans["transID"] > 0 ? "Y" : "N");
				
				//$onlinecustomerCount = count(selectMultiRecords($queryonlineCnt ));
				
				//$allCount = $allCount + $allCount;
}
//debug($query);
?>
<html>
<head>
	<title>Print Transactions</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.printTrans;
	var len = m.elements.length;
	if (m.All.checked==true)
  {
	    for (var i = 0; i < len; i++)
			{
	     	m.elements[i].checked = true;
	   	}
	}
	else
	{
	    for (var i = 0; i < len; i++)
		  {
		   	m.elements[i].checked=false;
	    }
	}
}
function msg()
{
	var m = document.printTrans;
	var len = m.elements.length;
	var flag = 0;
	
	for (var i = 0; i < len; i++)
	{
		if (m.elements[i].checked == true)
		{
			flag = 1;	
		}
	}
	if (flag == 1)
	{
		return true;
	}	
	else
	{
		alert("Please select transaction(s) first. Then click Print");
		return false;
	}	
}
-->
</script>

    <style type="text/css">
<!--
.style2 {color: #005b90; font-weight: bold; }
.style1 {color: #005b90}
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#000000" size="2">Select Transactions 
      for Printing</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table width="255" border="1" cellpadding="5" bordercolor="#666666">
        <form action="print-daily-trans.php" method="post" name="Search">
      <tr>
            <td width="286" bgcolor="#C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
        <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$_SESSION["dfMonth"]; ?>");
        </script>
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$_SESSION["dfDay"]; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$_SESSION["dfYear"]; ?>");
        </script>
        To	<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$_SESSION["dtMonth"]; ?>");
        </script>
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$_SESSION["dtDay"]; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$_SESSION["dtYear"]; ?>");
        </script>
      </td>
     </tr>
        <tr>
        <td align="center" nowrap>
		    <input name="transID" type="text" id="transID" size="15" value="" style="font-family:verdana; font-size: 11px; width:100"><? //echo $transID ?>
             <select name="searchBy" id="searchBy">
              <option value="0" <? echo ($_SESSION["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
              <option value="1" <? echo ($_SESSION["searchBy"] == 1 ? "selected" : "")?>>By Customer Name</option>
              <option value="2" <? echo ($_SESSION["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
<? 		if($agentType == "admin" || $agentType == "System" || $agentType == "Call" || $agentType == "Admin Manager")
{
?>
                  <option value="3" <? echo ($_POST["searchBy"] == 3 ? "selected" : "")?>>By Agent Number</option>
<?
}
?>
             </select>
             <input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
<!--	<?//$recieptPage?>?re=y	-->
		<form action="print-daily-trans.php" name="printTrans" method="post" onSubmit="return msg();">

			<table width="700" border="1" cellpadding="0" bordercolor="#666666">
				<?
				if ($allCount > 0){
				?>
				<tr>
					<td  bgcolor="#000000">
						<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
							<tr>
								<td>
									<?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
										Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
										of
										<?=$allCount; ?>
									<?php } ;?>
								</td>
								<?php if ($prv >= 0) { ?>
									<td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&search=Search&searchBy=$by&transID=$transID";?>"><font color="#005b90">First</font></a>
									</td>
									<td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&search=Search&searchBy=$by&transID=$transID";?>"><font color="#005b90">Previous</font></a>
									</td>
								<?php } ?>
								<?php
								if ( ($nxt > 0) && ($nxt < $allCount) ) {
									$alloffset = (ceil($allCount / $limit) - 1) * $limit;
									?>
									<!-- td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td -->
									<td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&search=Search&searchBy=$by&transID=$transID";?>"><font color="#005b90">Next</font></a>&nbsp;
									</td>
									<td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&search=Search&searchBy=$by&transID=$transID";?>"><font color="#005b90">Last</font></a>&nbsp;
									</td>
								<?php } ?>
							</tr>
						</table>
					</td>
				</tr>
				<tr>

					<td height="25" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>&nbsp;There
            are <? echo (count($contentsTrans)+count($onlinecustomer));?> records.</strong></span></td>
				</tr>
				<?
				if(count($contentsTrans) > 0 || count($onlinecustomer) > 0)
				{
				?>
				<tr>
					<td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
							<tr bgcolor="#FFFFFF">
								<? if(CONFIG_SINGLE_PRINT != "1"){?>
									<td><span class="style1"><input name="All" type="checkbox" id="All" onClick="CheckAll();"> Select</span></td>
								<? }?>
								<td><span class="style1">Date</span></td>
								<td><span class="style1"><? echo $systemCode;?></span></td>
								<td><span class="style1"><? echo $manualCode;?></span></td>
								<td><span class="style1">Status</span></td>
								<td width="85" bgcolor="#FFFFFF"><span class="style1">Amount </span></td>
								<td width="102"><span class="style1">Type</span></td>
								<? if(CONFIG_SINGLE_PRINT == "1"){?>
									<td width="102"><span class="style1">Print Transaction</span></td>
								<? } ?>
							</tr>
							<? for($i=0;$i<count($contentsTrans);$i++)
							{
								?>
								<tr bgcolor="#FFFFFF">
									<? if(CONFIG_SINGLE_PRINT != "1"){?>
										<td width="100" bgcolor="#FFFFFF"><input type="checkbox" name="checkbox[]" value="<? echo $contentsTrans[$i]["transID"]."/".$contentsTrans[$i]["customerID"];?>"></td>
									<? }?>
									<td width="100" bgcolor="#FFFFFF"><input type="hidden" name="transID" value="<? echo $contentsTrans[$i]["transID"];?>">
										<? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></td>

									<td width="102" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
									<td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
									<td width="98" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
									<td width="85" bgcolor="#FFFFFF"><? echo customNumberFormat($contentsTrans[$i]["totalAmount"]). " " . $contentsTrans[$i]["currencyFrom"]?></td>

									<td width="102" bgcolor="#FFFFFF" title=""><? echo ucfirst($contentsTrans[$i]["transType"])?></td>
									<? if(CONFIG_SINGLE_PRINT == "1"){?>

										<td width="102" bgcolor="#FFFFFF"  align="center"><a href="<?=CONFIG_RECIEPT_PRINT?>?transID=<? echo $contentsTrans[$i]["transID"]?>&imReferenceNumber=<?=$contentsTrans[$i]["refNumberIM"]?>&transactionDate=<?=$contentsTrans[$i]["transDate"]?>&fdate=<?=$fromDate ?>&tDate=<?=$toDate ?>&transValue=<? echo $_POST["transID"];?>&searchBy=<?=$by ?>&pageID=2&success=Y" class="style1">Print</a></td>
									<? } ?>
								</tr>
								<?
							}
							?>


							<? for($i=0;$i<count($onlinecustomer);$i++)
							{
								?>
								<tr bgcolor="#FFFFFF">
									<td width="100" bgcolor="#FFFFFF"><input type="checkbox" name="checkbox[]" value="<? echo $onlinecustomer[$i]["transID"];?>"></td>
									<td width="100" bgcolor="#FFFFFF"><? echo dateFormat($onlinecustomer[$i]["transDate"], "2")?></td>
									<td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumberIM"]?></td>
									<td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumber"]?></td>
									<td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["transStatus"]?></td>
									<td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($onlinecustomer[$i]["totalAmount"]).  " " . $onlinecustomer[$i]["currencyFrom"]?></td>
									<td width="102" bgcolor="#FFFFFF" title=""><? echo ucfirst($onlinecustomer[$i]["transType"])?></td>
								</tr>
								<?
							}
							?>

							<tr bgcolor="#FFFFFF">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td bgcolor="#FFFFFF">&nbsp;</td>
								<td width="102" align="center">&nbsp;</td>
								<td align="center">&nbsp;</td>
							</tr>
							<?
							} // greater than zero
							?>

							<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
								<tr>
									<td>
										<?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
											Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
											of
											<?=$allCount; ?>
										<?php } ;?>
									</td>
									<?php if ($prv >= 0) { ?>
										<td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&search=Search&searchBy=$by&transID=$transID";?>"><font color="#005b90">First</font></a>
										</td>
										<td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&search=Search&searchBy=$by&transID=$transID";?>"><font color="#005b90">Previous</font></a>
										</td>
									<?php } ?>
									<?php
									if ( ($nxt > 0) && ($nxt < $allCount) ) {
										$alloffset = (ceil($allCount / $limit) - 1) * $limit;
										?>
										<td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&search=Search&searchBy=$by&transID=$transID";?>"><font color="#005b90">Next</font></a>&nbsp;
										</td>
										<td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&search=Search&searchBy=$by&transID=$transID";?>"><font color="#005b90">Last</font></a>&nbsp;
										</td>
									<?php } ?>
								</tr>
							</table>

						</table></td>
				</tr>
			</table>
			<p>
				<? if(CONFIG_SINGLE_PRINT != "1"){?>
			<table align="center"> <tr><td>
						<input type="submit" name="Submit" value="Print">
					</td></tr></table> <? } ?>
			</p>
			<?
			} else if (!isset($allCount)){
				?>
				<?
			} else {
			?>
	<tr>
		<td  align="center"> No transaction found in the database.
		</td>
	</tr>
	<?
	}
	?>
	</form>
	</td>
	</tr>

</table>
</body>
</html>