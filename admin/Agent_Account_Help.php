<?
include ("../include/config.php");
?>
<HTML>
<head></head>
	<body>
	<p><b>Agent Account Report</b></p>
		<p>
				This report provides us information about the accounts of an agent during a specified time period.
			<BR>On click to this Link, user will see a screen with the search filter's option of Date and Agent Name.
			<BR>If no agent is specified or no data is found for the specified filters a screen with the message appear describing as <b>No 
			data found. Please select the proper filters from given above.</b>
			<BR>On Selection of Data, Maximum 20 records are displayed per page.
			<BR>If more than 20 records are there to be displayed, you may use <b>Next</b>, <b>Previous</b> or <b>First</b> Links to view 
			required pages.
			<BR><b>Cumulative Total</b>  is also shown on the top bar. A message is displayed based on Cumulative Total  to
			show whether Agent is to pay the <b><?=COMPANY_NAME?></b> or his account is going fine. 
				
		</p>
</body>
</HTML>