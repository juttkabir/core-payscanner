<?
/**
 * @package Transaction
 * @subpackage New Registered Senders List
 * This page Shows the Search filters for the search date filters and records
 */

session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
//include ("javaScript.php");
//$arrIDTypeData	= selectMultiRecords("SELECT id, title FROM id_types WHERE 1 ORDER BY title ASC");
?>
<html>
<head>
<title>Sender Lists</title>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee/grid.css" title="coffee" media="screen" />
<!--<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />-->
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />
<link href="css/datePicker.css" rel="stylesheet" type="text/css" />

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script src="javascript/date.js" type="text/javascript"></script>
<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
<script type="text/javascript" src="javascript/jqGrid/js/grid.locale-en.js"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<!--<script src="javascript/jqGrid/js/grid.celledit.js" type="text/javascript"></script>-->
<!--<script src="javascript/jqGrid/js/grid.inlineedit.js" type="text/javascript"></script>-->
<!--<script src="jquery.cluetip.js" type="text/javascript"></script>-->
<!--<script language="javascript" src="javascript/jquery.autocomplete.js"></script>-->
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var extraParams;
	jQuery(document).ready(function(){
		// Date Picker
		Date.format = 'yyyy-mm-dd';
		$('#fdate').datePicker({clickInput:true});
		$('#fdate').dpSetStartDate('1990-01-01');
		
		$('#tdate').datePicker({clickInput:true});
		$('#tdate').dpSetStartDate('1990-01-01');
		var lastSel;
		jQuery("#getList").jqGrid({
			url:'new_senders_list_conf.php?getGrid=getList',
			datatype: "json",
			height: 210,
			width: 700, 
			colNames:[
				'Sr.', 
				'Customer Name', 
				'Reference Number',
				'Status',
				'Creation Date',
				'Country',
				'Edit',
				'Action'
			],
			colModel:[
				{name:'sr',	index:'sr', width:25,	align:"center",sortable:false},
				{name:'customerName',index:'customerName', width:100,align:"left",editable:false },
				{name:'accountName',index:'accountName',width:100,align:"left",editable:false},
				{name:'status',	index:'status',	width:50, align:"left"},
				{name:'created', index:'created', width:80, align:"left"},
				{name:'Country', index:'Country', width:80, align:"left", editable:false},
				{name:'Edit', index:'Edit', width:80, align:"left", sortable:false},
				{name:'Action',index:'Action',width:80,align:"left",sortable:false}
			],
			imgpath:gridimgpath, 
			rowNum: 10,
			rowList: [5,10,20,50,100,200],
			pager: jQuery('#pagernavT'),
			multiselect:true,
			sortname: "customerName",
			sortorder: "ASC",
			viewrecords: true,
			loadonce: false,
			multiselect:false,
			loadui: "block",
			loadtext: "Loading ...",
			caption: "New Senders List"
		}).navGrid('#pagernavT',
				{edit:false, add:false, del:false, search:false},
				{afterSubmit: editMessage, closeAfterEdit:true}, 	// edit options
      			{}, 	// add options
      			{} 		// del options
			);
			
		$('#Submit').click(
			function(){
				if($('#fdate').val() == ''){
					alert("Please give the from Date");
					$('#fdate').click();
					return;
				}
				if($('#tdate').val() == ''){
					alert("Please give to Date");
					$('#tdate').click();
					return;
				}
				gridReload('getList');
			}
		);
		
		$('#export').click(
			function(){
				gridReload('export');
			}
		);
		
		$('#print').click(
			function(){
				gridReload('print');
			}
		);
		
		<?php 
			if($_REQUEST['refresh']=='1'){
		?>
			setTimeout("gridReload('getList')",900);
			
		<?php 
			}
		?>
		
	});
	
	// define handler function for 'afterSubmit' event.
	var editMessage = function(response, postdata){
		var json   = response.responseText;		// response text is returned from server.
		var result = JSON.parse(json);			// convert json object into javascript object.
		return [result.status,result.message,null]; 
	}
	
	
	
	
	function gridReload(grid)
	{
		var theUrl = "new_senders_list_conf.php";		
		var extraParam='';

		var fdate	= jQuery("#fdate").val();
		var tdate	= jQuery("#tdate").val();
		var status 	= jQuery("#status").val();
		var customerNumber 		= jQuery("#custNum").val();
		extraParam = "?getGrid="+grid+"&fdate="+fdate+"&tdate="+tdate+"&status="+status+"&custNum="+customerNumber+"&Submit=Search";
		
		if(grid == 'print' || grid == 'export')
		{
			exportList(extraParam);
		}
		else if(grid=='getList')
		{
			jQuery("#getList").setGridParam({
				url: theUrl+extraParam,
				page:1
			}).trigger("reloadGrid");
		}
	}
	
	function exportList(extraParam){
		window.open("new_senders_list_conf.php"+extraParam,'ExportDailyTransaction','height=600,width=1000,location=1,status=1,scrollbars=1');
	}
	
	function customerEdit(senderID, type){
		if(type == 'company')
			window.open("add-company.php?customerID="+senderID, '_blank', 'scrollbars=yes,toolbar=no, location =no,status=no,menubar=no,resizeable=yes,height=300,width=800');
	}
	
	function customerAction(senderID){
		window.open("disable_sender.php?customerID="+senderID+'&page=new_senders_list', '_blank', 'scrollbars=no,toolbar=no,location=no,status=no,menubar=no,resizeable=yes,height=300,width=800');
	}
	
</script>
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000}
.style1 {color: #005b90}
.style4 {color: #005b90; font-weight: bold; }

#searchTable
{
	background-color:#ededed;
	border:1px solid #000000;
}
#searchTable tr td
{
	border-top:1px solid #FFFFFF;
}
.dp-choose-date{
	margin-left:5px;
}
div.scroll span{
	line-height:17px;
	margin-left:5px;
}
-->
</style>
<script language="javascript">
/*
function clearDates()
{
	document.frmSearch.from.value = "";
	document.frmSearch.to.value = "";
	return true;
}
*/
</script>
</head>
<body>
<div style=" margin:5px; padding-left:5px; height:30px; line-height:30px;background-color:#6699cc; color:#FFFFFF"><strong>GB Compliance List<strong></div>
	<form name="frmSearch" id="frmSearch">
	<table width="100" border="0" align="center">
		<tr>
			<td>
				<table id="searchTable" align="center" border="0" cellpadding="5" bordercolor="#666666">
					<tr>
						<th align="left"colspan="4" bgcolor="#6699cc"><span class="tab-u">Search Senders for Compliance</span></th>
					</tr>
					<tr>
						<td align="center" colspan="2">
							From <input type="text" readonly="readonly" name="fdate" id="fdate" value="<?php if($_REQUEST['refresh']=='1') echo $_SESSION['fdate']; ?>"/>
							To 	<input type="text" readonly="readonly" name="tdate" id="tdate" value="<?php if($_REQUEST['refresh']=='1') echo $_SESSION['tdate']; ?>"/>
						</td>
					</tr>
					<tr>
						<td nowrap align="right">Status</td>
						<td>
						<select style="font-family:verdana; font-size: 11px; width:100;" name="status" id="status">
							  <option value=""> - Status - </option>
							  <option value="All" <?php if($_SESSION['status'] == 'All' && $_REQUEST['refresh']=='1') echo "selected='selected'"; ?>>All</option>
							  <option value="New" <?php if($_SESSION['status'] == 'New' && $_REQUEST['refresh']=='1') echo "selected='selected'"; ?>>New</option>
							  <option value="Enable" <?php if($_SESSION['status'] == 'Enable' && $_REQUEST['refresh']=='1') echo "selected='selected'"; ?>>Enable</option>
							  <option value="Disable" <?php if($_SESSION['status'] == 'Disable' && $_REQUEST['refresh']=='1') echo "selected='selected'"; ?>>Disable</option>
						</select>
						</td>
					</tr>
					<tr>
						<td nowrap align="right">Reference Number</td>
						<td>
							<input id="custNum" name="custNum" style="WIDTH: 140px;HEIGHT: 18px; " value="<?php if(isset($_REQUEST['refresh'])) echo $_SESSION['customerNumber']; ?>"/>
						</td>
					</tr>
					<tr>
						<td colspan="4" nowrap align="center">
							<input id="Submit" type="button" name="Submit" value="Search" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table id="getList" border="0" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernavT" class="scroll" style="text-align:center;"></div>
			</td>
		</tr>
		<tr>
			<td align="center">
				<input type="button" name="Submit" id="print" value="Print All" />
				<input type="button" name="Submit" id="export" value="Export All" />
			</td>
		</tr>
	</table>
</form>
</body>
</html>