<?php
// Session Handling
session_start();
///////////////////////History is maintained via method named 'activities'

// Including files
include ("../include/config.php");
include ("security.php");

$agentType = getAgentType();

if ($_POST["userID"] == ""){
	$backURL = "add-admin-opal.php?msg=Y";
} else {
	$backURL = "update-admin-opal.php?userID=$_POST[userID]&msg=Y&Submit=$Submit&adminCode=$adminCode&searchBy=$searchBy";
}
/*	#5441 - AMB Exchange
	Operator ID is added in Admin Staff form.
	to be displayed in Western Union Output export file.
	by Niaz Ahmad
*/
$operatorFlag = false;
if(CONFIG_OPERATOR_ID_AnD=="1"){
	$operatorFlag = true;
}
if($_GET["Submit"]!="")
{
	$Submit = $_GET["Submit"];	
}
if($_GET["adminCode"]!="")
{
	$adminCode = $_GET["adminCode"];	
}
if($_GET["searchBy"]!="")
{
	$searchBy = $_GET["searchBy"];	
}

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

	session_register("loginName");
	session_register("password1");
	session_register("password2");
	session_register("name");
	session_register("rights");
	session_register("email");
	session_register("adminType");
	session_register("agentStatus");
	session_register("custCountries");
	session_register("IDAcountry");
	session_register("linkedAgent");
	session_register("linkedDistributor");
	session_register("accessFromIP");
	if($operatorFlag)
		session_register("OperatorID");

	$_SESSION["loginName"] = $_POST["loginName"];
	$_SESSION["password1"] = $_POST["password1"];
	$_SESSION["password2"] = $_POST["password2"];
	$_SESSION["name"] = $_POST["name"];
	$_SESSION["email"] = $_POST["email"];
	
	$_SESSION["rights"] = (is_array($_POST["adminRights"]) ? implode(", ", $_POST["adminRights"]) :"");
 	$_SESSION["adminType"] = $_POST["adminType"];
	$_SESSION["agentStatus"] = $_POST["agentStatus"];
	$_SESSION["limitTransAmount"] = $_POST["limitTransAmount"];
	$_SESSION["limitChequeAmount"] = $_POST["limitChequeAmount"];
	$_SESSION["limitCurrExchange"] = $_POST["limitCurrExchange"];
	$_SESSION["limitDailyExpense"] = $_POST["limitDailyExpense"];
	if($operatorFlag)
		$_SESSION["OperatorID"] = $_REQUEST["OperatorID"];
	if (CONFIG_CUST_COUNTRIES == "1") {
		$_SESSION["custCountries"] = (is_array($_POST["custCountries"]) ? implode(",", $_POST["custCountries"]) : "");
	} else {
		$_SESSION["custCountries"] = "";	
	}
	$_SESSION["IDAcountry"] = (is_array($_POST["IDAcountry"]) ? implode(",", $_POST["IDAcountry"]) :"");
	$_SESSION["accessFromIP"] = $_POST["accessFromIP"];
	
	if (CONFIG_ADMIN_ASSOCIATE_AGENT == "1") /* excluded from Ticket #4384  && strstr(CONFIG_ASSOCIATED_ADMIN_TYPE, $_POST['adminType'].",") */
	{
		$_SESSION["linkedAgent"] = (is_array($_POST["linkedAgent"]) ? implode(",", $_POST["linkedAgent"]) : "");
		
		if($_SESSION["linkedAgent"]{strlen($_SESSION["linkedAgent"])-1} != ",") 
			$_SESSION["linkedAgent"] = $_SESSION["linkedAgent"].",";
		
	}
	else
		$_SESSION["linkedAgent"] = "";	
	
	if (CONFIG_ADMIN_ASSOCIATE_DISTRIBUTOR == "1" && $_POST["linkedDistributor"] != "") {
		$_SESSION["linkedDistributor"] = (is_array($_POST["linkedDistributor"]) ? implode(",", $_POST["linkedDistributor"]) : "");
		
		if($_SESSION["linkedDistributor"]{strlen($_SESSION["linkedDistributor"])-1} != ",") {
		$_SESSION["linkedDistributor"] = $_SESSION["linkedDistributor"].",";
		}
		
	}else{
		$_SESSION["linkedDistributor"] = "";	
	}
	
$backDays = "";

if (strstr($_SESSION["rights"], 'Backdated')) {
	if ($agentType == 'admin') {
		if ($_POST['backDays'] == "") {
			$backDays	= '1';
		} else {
			$backDays = $_POST['backDays'];
		}
	}
}

if (trim($_POST["loginName"]) == ""){
	insertError(AG24);
	redirect($backURL);
}
if (trim($_POST["password1"]) == ""){
	insertError(AG25);
	redirect($backURL);
}
if (trim($_POST["password2"]) == ""){
	insertError(AG26);
	redirect($backURL);
}
if (trim($_POST["password2"]) != trim($_POST["password1"])){
	insertError(AG27);
	redirect($backURL);
}
if ($_SESSION["rights"] == "" && ($_POST["adminType"] == "Admin")){
	insertError(AG28);
	redirect($backURL);
}
if (trim($_POST["email"]) == ""){
	insertError(AG29);
	redirect($backURL);
}
if (CONFIG_CUST_COUNTRIES == "1") {

//print_r($_POST['custCountries']); exit;
	if ($_POST['custCountries'] == "") {
		insertError(AG45);
		redirect($backURL);
	}	
}
if($_SESSION["adminType"]!= "MLRO"){
if ($_POST["IDAcountry"] == ""){
	insertError(AG41);
	redirect($backURL);
}
}
if ($_POST["userID"] == "") {
	
	if (isExist("SELECT `username` FROM " . TBL_ADMIN_USERS . " WHERE `username` = '".checkValues($_POST["loginName"])."'")) {
		insertError(AG32);
		redirect($backURL);
	}
	if (isExist("SELECT `username` FROM " . TBL_CM_CUSTOMER . " WHERE `username` = '".checkValues($_POST["loginName"])."'")) {
		insertError(AG32);
		redirect($backURL);
	}
	if (isExist("SELECT `accountName` FROM " . TBL_CUSTOMER . " WHERE `accountName` = '".checkValues($_POST["loginName"])."'")) {
		insertError(AG32);
		redirect($backURL);
	}
	if (isExist("SELECT `loginName` FROM " . TBL_TELLER . " WHERE `loginName` = '".checkValues($_POST["loginName"])."'")) {
		insertError(AG32);
		redirect($backURL);
	}

	/* $Querry_Sqls = "INSERT INTO ".TBL_ADMIN_USERS." (username, password, changedPwd, name, rights, backDays, email, created, adminType, agentStatus, IDAcountry, custCountries, accessFromIP, linked_Agent) VALUES 
	('".checkValues($_POST["loginName"])."', '".$_POST["password1"]."', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".checkValues($_POST["name"])."', '".$_SESSION["rights"]."', '".$backDays."', '".checkValues($_POST["email"])."', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$_POST["adminType"]."', '".$_POST["agentStatus"]."', '".$_SESSION["IDAcountry"]."', '".$_SESSION["custCountries"]."', '".$_SESSION["accessFromIP"]."','".$_SESSION["linkedAgent"]."')";*/
	
	$Querry_Sqls = "INSERT INTO ".TBL_ADMIN_USERS." (username, password,  name, rights, backDays, email, created, adminType, agentStatus, IDAcountry, custCountries, accessFromIP, linked_Agent, linked_Distributor) VALUES 
	('".checkValues($_POST["loginName"])."', '".$_POST["password1"]."','".checkValues($_POST["name"])."', '".$_SESSION["rights"]."', '".$backDays."', '".checkValues($_POST["email"])."', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$_POST["adminType"]."', '".$_POST["agentStatus"]."', '".$_SESSION["IDAcountry"]."', '".$_SESSION["custCountries"]."', '".$_SESSION["accessFromIP"]."','".$_SESSION["linkedAgent"]."','".checkValues($_SESSION["linkedDistributor"])."')";
	insertInto($Querry_Sqls);
	
	//echo "   ADMIN Rights <br> ".$_SESSION["rights"]."<br>";
	
	$insertedID = @mysql_insert_id();
	
	if(CONFIG_ADMIN_STAFF_AMOUNT_LIMIT_ALERT == "1"){ 
	$strUpdateSql = "update
						".TBL_ADMIN_USERS."
					set
						adminLimitTransAmount = '".$_SESSION["limitTransAmount"]."',
						adminLimitCheque = '".$_SESSION["limitChequeAmount"]."',
						adminLimitCurrExchange = '".$_SESSION["limitCurrExchange"]."',
						limitDailyExpense = '".$_SESSION["limitDailyExpense"]."'
					where
							userID = '".$insertedID."'	
						";
					update($strUpdateSql);	
	//debug($strUpdateSql);
	}
	if(isset($_POST["OperatorID"]) && $operatorFlag){
			update("update " . TBL_ADMIN_USERS. " set OperatorID='".$_POST["OperatorID"]."' where userID='".$insertedID."'");
	}	
	$fromName = SUPPORT_NAME;
	$fromEmail = SUPPORT_EMAIL;
	$subject = $_POST["loginName"]."-Your Account created on $company Payex";
	$message = "Dear ".$_POST["name"].",<br>
		Your Admin account has been created at $company. Your login information is as follows:<br>
		Login: ".$_POST["loginName"]."<br>
		Password: ".$_POST["password1"]."<br>
		Name: ".$_POST["name"]."<br>
		To access your account please <a href=\"".ADMIN_ACCOUNT_URL."\">click here</a><br>

		$company Support";
if(CONFIG_MAIL_GENERATOR == '1'){
	sendMail($_POST["email"], $subject, $message, $fromName, $fromEmail);
}
	////To record in History
	$descript ="Admin staff is added";
	activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,TBL_ADMIN_USERS,$descript);
	$_SESSION["loginName"] = "";
	$_SESSION["password1"] = "";
	$_SESSION["password2"] = "";
	$_SESSION["name"] = "";
	$_SESSION["rights"] = "";
	$_SESSION["email"] = "";
	$_SESSION["adminType"] = "";
	$_SESSION["agentStatus"] = "";
	$_SESSION["custCountries"] = "";
	$_SESSION["IDAcountry"] = "";
	$_SESSION["accessFromIP"] = "";
	$_SESSION["linkedAgent"] = "";
	$_SESSION["linkedDistributor"] = "";
	if($operatorFlag)
		$_SESSION["OperatorID"] = "";
		
	insertError(AG30);
	$backURL .= "&success=Y";
} else {
	if (isExist("select username from ".TBL_ADMIN_USERS." where username = '".checkValues($_POST["loginName"])."' and userID<>'".$_POST[userID]."'")){
		insertError(AG32);
		redirect($backURL);
	}
	if (checkValues($_POST["loginName"]) != checkValues($_POST["loginName2"])) {
		if (isExist("SELECT `username` FROM " . TBL_CM_CUSTOMER . " WHERE `username` = '".checkValues($_POST["loginName"])."'")) {
			insertError(AG32);
			redirect($backURL);
		}
		if (isExist("SELECT `accountName` FROM " . TBL_CUSTOMER . " WHERE `accountName` = '".checkValues($_POST["loginName"])."'")) {
			insertError(AG32);
			redirect($backURL);
		}
		if (isExist("SELECT `loginName` FROM " . TBL_TELLER . " WHERE `loginName` = '".checkValues($_POST["loginName"])."'")) {
			insertError(AG32);
			redirect($backURL);
		}
	}
	$Querry_Sqls = "update ".TBL_ADMIN_USERS." set username='".checkValues($_POST["loginName"])."',
	name ='".checkValues($_POST["name"])."',
	 email ='".checkValues($_POST["email"])."', 
	 password ='".$_POST["password1"]."', 
	 rights ='".$_SESSION["rights"]."', 
	 backDays ='".$backDays."', 
	 agentStatus ='".checkValues($_POST["agentStatus"])."',
	 custCountries = '".$_SESSION["custCountries"]."',
	 IDAcountry ='".$_SESSION["IDAcountry"]."',
	 accessFromIP ='".$_POST["accessFromIP"]."',
	 linked_Agent = '".$_SESSION["linkedAgent"]."',
	 linked_Distributor = '".$_SESSION["linkedDistributor"]."',
	 adminType='".checkValues($_POST["adminType"])."'  where userID='".$_POST[userID]."'";
	update($Querry_Sqls);
	
if(CONFIG_ADMIN_STAFF_AMOUNT_LIMIT_ALERT == "1"){ 
	$strUpdateSql = "update
						".TBL_ADMIN_USERS."
					set
						adminLimitTransAmount = '".$_SESSION["limitTransAmount"]."',
						adminLimitCheque = '".$_SESSION["limitChequeAmount"]."',
						adminLimitCurrExchange = '".$_SESSION["limitCurrExchange"]."',
						limitDailyExpense = '".$_SESSION["limitDailyExpense"]."'
					where
							userID = '".$_POST["userID"]."'	
						";
					update($strUpdateSql);	
	
	}
if(!empty($_POST["userID"]) && isset($_POST["OperatorID"]) && $operatorFlag){
			update("update " . TBL_ADMIN_USERS. " set OperatorID='".$_POST["OperatorID"]."' where userID='".$_POST["userID"]."'");
	}	
	
	////To record in History
	$descript ="Admin staff is updated";
	activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["userID"],TBL_ADMIN_USERS,$descript);
	$_SESSION["loginName"] = "";
	$_SESSION["password1"] = "";
	$_SESSION["password2"] = "";
	$_SESSION["name"] = "";
	$_SESSION["rights"] = "";
	$_SESSION["email"] = "";
	$_SESSION["adminType"] = "";
	$_SESSION["agentStatus"] = "";
	$_SESSION["custCountries"] = "";
	$_SESSION["IDAcountry"] = "";
	$_SESSION["linkedAgent"] = "";
	$_SESSION["linkedDistributor"] = "";
	$_SESSION["accessFromIP"] = "";
	$_SESSION["limitTransAmount"] =  "";
	$_SESSION["limitChequeAmount"] = "";
	$_SESSION["limitCurrExchange"] = "";
	$_SESSION["limitDailyExpense"] = "";
	if($operatorFlag)
		$_SESSION["OperatorID"] = "";
	insertError(AG31);
	
	$fromName = SUPPORT_NAME;
	$fromEmail = SUPPORT_EMAIL;
	$subject = $_POST["loginName"]."-Your Account has been updated on $company Payex";
	$message = "Dear ".$_POST["name"].",<br>
		Your Admin account has been updated at $company. Your updated login information is as follows:<br>
		Login: ".$_POST["loginName"]."<br>
		Password: ".$_POST["password1"]."<br>
		Name: ".$_POST["name"]."<br>
		To access your account please <a href=\"".ADMIN_ACCOUNT_URL."\">click here</a><br>

		$company Support";
if(CONFIG_MAIL_GENERATOR == '1'){
	sendMail($_POST["email"], $subject, $message, $fromName, $fromEmail);
}
	
	$backURL .= "&success=Y";
}
redirect($backURL);
?>