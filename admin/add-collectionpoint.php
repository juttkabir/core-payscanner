<?
session_start();
include ("../include/config.php");
//include ("config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$countryBasedFlag = false;
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}

$locationString = "Location ID";
if(defined("CONFIG_COLLECTION_LOCATION_ID_TITLE") && CONFIG_COLLECTION_LOCATION_ID_TITLE!="0"){
	$locationString = CONFIG_COLLECTION_LOCATION_ID_TITLE;
}
if($_REQUEST["ajaxCity"] == "get")
{
	//$strCities = '<select name="city" id="city">';
	$strCities = '';
	if(!empty($_REQUEST["cntry"]))
	{
		$cityQuery="select distinct(city) from  ".TBL_CITIES." where country = '".$_REQUEST["cntry"]."' order by city";
		$cityRS=selectMultiRecords($cityQuery);
		for($c=0;$c<count($cityRS);$c++)
		{
			$strCities .= '<option value="'.$cityRS[$c]["city"].'">'.$cityRS[$c]["city"].'</option>';
		}
    }
	else
	{
		$strCities .= '<option value="">- Select City -</option>';
	}
	//$strCities .= '</select>';

	echo $strCities;
	exit;
}


$collectionPointID = $_GET["collectionPointID"];
$distributorID = $_GET["distributorID"];
$page = $_GET["page"];
$qs = isset($_REQUEST["qs"]) ? $_REQUEST["qs"] : "";

if ($_GET["collectionPointID"] != ""){
$Mode = "Update";
$city = $_GET["agentCity"];
$collectionpoint = selectFrom("select * from cm_collection_point where cp_id = $collectionPointID");
}
else{
$Mode = "Add";
}

?>
<html>
<head>
	<title>Add Collection Point</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script src="jquery.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
	
function checkForm(theForm) {
	if(theForm.agentID.options.selectedIndex == 0){
    	alert("Please select a Distributor.");
        theForm.agentID.focus();
        return false;
    }
	<? if(CONFIG_NON_MANDATORY_BANK_NAME_AT_COLLECTION_POINT !="1"){?>
	if(theForm.corresspondentName.value == "" || IsAllSpaces(theForm.corresspondentName.value)){
    	alert("Please provide the Bank Name.");
        theForm.corresspondentName.focus();
        return false;
    }
	<? } ?>
	if(theForm.name.value == "" || IsAllSpaces(theForm.name.value)){
    	alert("Please provide the Collection Point Name.");
        theForm.name.focus();
        return false;
    }
	
	if(document.getElementById("locationId").value == "" || IsAllSpaces(document.getElementById("locationId").value))
	{
		alert("Please provide the location ID or branch number.");
        document.getElementById("locationId").focus();
        return false;
	}
	
  if(theForm.sCountry.options.selectedIndex == 0){
    	alert("Please select a Country.");
        theForm.sCountry.focus();
        return false;
    }
	if(theForm.address.value == "" || IsAllSpaces(theForm.address.value)){
    	alert("Please provide the Address.");
        theForm.address.focus();
        return false;
    }
	<? if(CONFIG_BENEFICIARY_CITY_DROPDOWN == "1") { ?>
		if(document.getElementById("city").value == "")
		{
			alert("Please select a City.");
			document.getElementById("city").focus();
			return false;
	    }
	<? }  else { ?>
	if(theForm.city.value == "")
	{
    	alert("Please provide the City Name.");
        theForm.city.focus();
        return false;
    }	
 	<? } ?>
<?
if(CONFIG_isSAMEDAY_OPTION =='1'){   
	?>
    if((theForm.wday.value == "" && theForm.oTime.value == "" && theForm.cTime.value == "")||(theForm.wday.value != "" && theForm.oTime.value != "" && theForm.cTime.value != "") ){
    	
    }else{
    	alert("Please make sure to enter values for working days, closing time and opening time.");
    	return false;
    }
    
    if((theForm.wwday.value == "" && theForm.woTime.value == "" && theForm.wcTime.value == "")||(theForm.wwday.value != "" && theForm.woTime.value != "" && theForm.wcTime.value != "") ){
    	
    }else{
    	alert("Please make sure to enter values for weekend working days, weekend closing time and weekend opening time.");
    	return false;
    } 		     
 <? 
 }
 ?>
	return true;
}


function shorProvinces()
{
if(document.addAdmin.operatesWeekend.selected)
	{
	document.getElementById("Yes").style.display = '';
	document.getElementById("No").style.display = 'none';
	}
	else 
	{
	document.getElementById("Yes").style.display = 'none';
	document.getElementById("No").style.display = '';
	
	}
}

function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
   


	function fnBody_OnLoad() 
	{
		addAdmin.corresspondentName.focus();
		var oForm = document.forms['addAdmin'] ;
		oForm.agentID.value = "<? echo $distributorID ?>";
		
		<? if(CONFIG_BENEFICIARY_CITY_DROPDOWN == "1" && !empty($_REQUEST["collectionPointID"])) { ?>
	 		//$("#cityDiv").load("add-collectionpoint.php?ajaxCity=get&cntry="+document.getElementById("country").value);
			$.ajax({
			   type: "GET",
			   url: "add-collectionpoint.php",
			   data: "ajaxCity=get&cntry="+document.getElementById("country").value,
			   success: function(msg){
					document.getElementById("cityDiv").innerHTML = msg;
					SelectOption(document.forms[0].city, "<?=$collectionpoint["cp_city"]; ?>");
			   }
			 });
			
		<? } ?>
	}
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}



	// end of javascript -->
	</script>
</head>
<body onLoad="fnBody_OnLoad();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	
  <tr>
    <td class="topbar"><strong><font class="topbar_tex"><?=$Mode?> Collection Point</font></strong></td>
  </tr>
  <form action="add-collectionpoint-conf.php?page=<?=$page?>&qs=<?=urlencode($qs); ?>&collectionPointID=<?=$collectionPointID?>" method="post" onSubmit="return checkForm(this);" name="addAdmin">
 <input type="hidden" name="agentCity" value="<?=$_GET["agentCity"];?>">
	<input type="hidden" name="benCountry" value="<?=$_GET["benCountry"];?>">
	
  <tr>
    <td align="center">
		<table width="448" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr> 
            <td colspan="2" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong><?=$Mode?> Collection Point</strong></font></td>
                </tr>
              </table></td>
          </tr>
          <? if ($_GET["msg1"] != ""){ ?>
          <tr bgcolor="#EEEEEE">
            <td colspan="2" bgcolor="#EEEEEE"><table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr>
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>Your Collection point Added successfully. </b><br><br></font>"; ?></td>
                </tr>
              </table></td>
          </tr>
          <? } ?>
          <tr bgcolor="#ededed"> 
            <td height="19" colspan="2" align="center"><font color="#FF0000">* 
              Compulsory Fields</font></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Related Distributor<font color="#ff0000">*</font></strong></font></td>
            <td><select name="agentID" style="font-family:verdana; font-size: 11px">
				<option value="">- Select Distributor -</option>
				<!--option value="All">- All Banks -</option-->
				<?
						$agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'N' and agentStatus='Active' and fromServer='' order by agentCompany");
						for ($i=0; $i < count($agents); $i++){
					?>
				<option value="<?=$agents[$i]["userID"]; ?>"><? echo $agents[$i]["agentCompany"] . " [" . $agents[$i]["username"] . "]" ; ?></option>
				<?
						}
					?>
			  </select>
			  
			  
					  </td>
					</tr>
          
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Bank Name</strong></font>
			<!-- this config added @5281 AMB-->
			<? if(CONFIG_NON_MANDATORY_BANK_NAME_AT_COLLECTION_POINT !="1"){?>
			<strong><font color="#ff0000">*</font></strong>
			<? } ?>
			</td>
            <td><input type="text" name="corresspondentName" value="<?=$collectionpoint["cp_corresspondent_name"] ?>"  size="35" maxlength="32"></td>
          </tr>
          
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Collection Point Name<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="name" value="<?=$collectionpoint["cp_branch_name"] ?>"  size="35" maxlength="32"></td>
          </tr>
           <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong><?=$locationString?>/Branch Number&nbsp;<font color="red">*</font></strong></font></td>
            <td><input type="text" name="locationId" id="locationId" value="<?=$collectionpoint["cp_branch_no"] ?>"  size="35" maxlength="32"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Contact Person Name</strong></font></td>
            <td><input type="text" name="contactPersonName" value="<?=$collectionpoint["cp_contact_person_name"] ?>"  size="35" maxlength="32"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Country<font color="#ff0000">*</font></strong></font></td>
           <td align="left">	
           	  <? $Country = $collectionpoint["cp_country"]; ?>				
					<select name="sCountry" id="country" style="WIDTH: 140px;HEIGHT: 18px; " onChange='$("#cityDiv").load("add-collectionpoint.php?ajaxCity=get&cntry="+this.value);'>
					<option>Select a Country</option>
						<?
						
							$countryTypes = " and countryType like '%destination%' ";
							
						 if(CONFIG_COUNTRY_SERVICES_ENABLED){
							$serviceTable = TBL_SERVICE;
							if($countryBasedFlag){
								$serviceTable = TBL_SERVICE_NEW;
							}
							$strQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".$serviceTable." where  countryId = toCountryId  $countryTypes order by countryName";
							}
					else
						{
							$strQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName";
						}
						//$strQuery = "SELECT distinct  country    FROM cities   ORDER BY  country ";
						$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
				
						while($rstRow = mysql_fetch_array($nResult))
						{
							$strCountry = $rstRow["countryName"];
							// strtoupper function added in the condition below against ticket #3540: Connect Plus - Import Collection Point Issues
							if(strtoupper($Country) == strtoupper($strCountry) )
								echo "<option value='$strCountry' selected>".$strCountry."</option>";			
							else
							echo "<option value='$strCountry'>".$strCountry."</option>";	
						}
						
						?>
					</select>
					</select><script language="JavaScript">SelectOption(document.forms[0].sCountry, "<?=$Country;?>");</script>
					&nbsp;<font  face="Verdana"><b></font>&nbsp;</td>
		  		</tr>
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Address<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="address" value="<?=$collectionpoint["cp_branch_address"];?>" size="35" maxlength="254"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144" valign="top"><font color="#005b90"><b>City<font color="#ff0000">*</font></b></font></td>
            <td>
				<? if(CONFIG_BENEFICIARY_CITY_DROPDOWN == "1") { ?>
						<select name="city" id="cityDiv">
							<option value="">- Select City-</option>
						</select>
						<script language="JavaScript">
				         	SelectOption(document.forms[0].city, "<?=$collectionpoint["cp_city"]; ?>");
                        </script>
				<? } else { ?>
					<input type="text" name="city" value="<?=$collectionpoint["cp_city"];?>" size="35" maxlength="255">
				<? } ?>
			</td>
          </tr>
         
         
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>State</strong></font></td>
            <td><input type="text" name="state" value="<?=$collectionpoint["cp_state"];?>" size="35" maxlength="255"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Phone</strong></font></td>
            <td><input type="text" name="phone" value="<?=$collectionpoint["cp_phone"];?>" size="35" maxlength="255"></td>
          </tr>
           <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Fax</strong></font></td>
            <td><input type="text" name="fax" value="<?=$collectionpoint["cp_fax"];?>" size="35" maxlength="255"></td>
          </tr>
          
            <? if(CONFIG_isSAMEDAY_OPTION == '1'){?>
           <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Sameday</strong></font></td>
           
            <td><SELECT name="isSameday" style="font-family:verdana; font-size: 11px">
                  <OPTION value="Y" <? echo($collectionpoint["sameday"]=="N" ? "" : "selected");?>>Yes</OPTION>
                  <OPTION value="N" <? echo($collectionpoint["sameday"]=="Y" ? "" : "selected");?>>No</OPTION>
                </SELECT>
            	</td>           	
          </tr>  
          
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Working days</strong></font></td>
            <td><input type="text" name="wday" value="<?=$collectionpoint["workingDays"];?>" size="35" maxlength="255"></td>
          </tr>

<tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Opening Time</strong></font></td>
            <td><input type="text" name="oTime" value="<? if($collectionpoint["openingTime"]!= '00:00:00'){ echo $collectionpoint["openingTime"];}?>" size="35" maxlength="255">&nbsp;<i>24 hr format</i></td>
          </tr>

<tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Closing Time</strong></font></td>
            <td><input type="text" name="cTime" value="<? if($collectionpoint["closingTime"]!= '00:00:00'){ echo $collectionpoint["closingTime"];}?>" size="35" maxlength="255">&nbsp;<i>24 hr format</i></td>
          </tr>
          
          
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Operates on weekend</strong></font></td>
           
            <td><SELECT name="operatesWeekend" style="font-family:verdana; font-size: 11px" onChange="ShowReg(this.selected)">
                  <OPTION value="Y" <? echo($collectionpoint["operatesWeekend"]=="N" ? "" : "selected");?>>Yes</OPTION>
                  <OPTION value="N" <? echo($collectionpoint["operatesWeekend"]=="Y" ? "" : "selected");?>>No</OPTION>
                </SELECT>
            	</td>           	
          </tr>  
          
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Weekend working days</strong></font></td>
            <td><input type="text" name="wwday" value="<?=$collectionpoint["weekendWorkingDays"];?>" size="35" maxlength="255"></td>
          </tr>

<tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Weekend opening Time</strong></font></td>
            <td><input type="text" name="woTime" value="<? if($collectionpoint["weekendOpeningTime"]!= '00:00:00'){ echo $collectionpoint["weekendOpeningTime"];}?>" size="35" maxlength="255">&nbsp;<i>24 hr format</i></td>
          </tr>

<tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Weekend closing Time</strong></font></td>
            <td><input type="text" name="wcTime" value="<? if($collectionpoint["weekendClosingTime"]!= '00:00:00'){ echo $collectionpoint["weekendClosingTime"];}?>" size="35" maxlength="255">&nbsp;<i>24 hr format</i></td>
          </tr>
          
                 
          	<? }?>
          	
          	
          	
            </table>
            </td>
          </tr>
          
          <tr bgcolor="#ededed"> 
            <td colspan="2" align="center"> <input type="submit" value=" <?=$Mode?>">
        </td>
        <?
			if($Mode!="Add")
			{
				if ( $page == "distributor-locator.php" )
					$backUrl = $_SERVER['HTTP_REFERER'];
				else
					$backUrl = "manage_collection_point.php?agentCity=" . $city;
		?>
        <td>
            <a href="<?=$backUrl;?>"><strong><font color="#000000">Go Back </font></strong></a>
        </td>
      <? } ?>
             </tr>
        </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>
