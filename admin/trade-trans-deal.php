<?php
session_start();
/**
*
* @package Trading
* @subpackage deal traded to non traded 
*
* Short Description
* This page is used to concile the trades with deals.
*
*/
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("javaScript.php");
include_once ("secureAllPages.php");
$modifyby = $_SESSION["loggedUserData"]["userID"];

//$currencyies = selectMultiRecords("SELECT distinct `currency` FROM `exchangerate` as ex join currencies as c where ex.currency = c.currencyName");

$currenciesFrom = selectMultiRecords("SELECT distinct currencyFrom FROM transactions as trans join currencies as c where trans.currencyFrom = c.currencyName");

$currenciesTo = selectMultiRecords("SELECT distinct currencyTo FROM transactions as trans join currencies as c where trans.currencyTo = c.currencyName");

$currenciesBuy = selectMultiRecords("SELECT distinct currencyBuy FROM account_details as acDetails join currencies as c where acDetails.currencyBuy = c.currencyName");

$currenciesSell = selectMultiRecords("SELECT distinct currencySell FROM `account_details` as acDetails join currencies as c where acDetails.currencySell = c.currencyName");


?>
<html>
<head>
<title>Deal Traded to Not-Traded</title>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="./javascript/functions.js"></script>
<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/jqGrouping/css/jquery-ui-custom.css" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/jqGrouping/css/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/jqGrouping/css/ui.multiselect.css" />
<script src="javascript/jqGrid/jqGrouping/jq/jquery.min.js"></script>
<script src="javascript/jqGrid/jqGrouping/jq/grid.locale-en.js" type="text/javascript"></script>
<script src="javascript/jqGrid/jqGrouping/jq/jquery.jqGrid.min.js" type="text/javascript"></script>

<!--
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee/grid.css" title="coffee" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />
-->

<!--<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/grid.celledit.js" type="text/javascript"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script src="toolbar.js" type="text/javascript"> </script>-->


<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var allCurrencies = [];
	var allCurrencies1 = [];
	
	/* // Note:for associative array  declare as object
	var allCurrencies = {};
	var allCurrencies1 = {}; */
	//var subgridimgpath = 'javascript/jqGrid/themes/green/images';
	var tAmt = 0;
	var lAmt = 0;
	var totalAmtA =0 ;
	var totalAmtB =0 ;
	var totalAmtA1 =0 ;
	var totalAmtB1 =0 ;
	
	var currencyB1 =0 ;
	var currencyB2 =0 ;
	var currencyA1 =0 ;
	var currencyA2 =0 ;
	var extraParams;
	//var comSubGrid = 'comSubGrid';
	jQuery(document).ready(function(){
		var lastSel;
		var maxRows = 50;
		
		jQuery("#payList").jqGrid({
			url:'trade-trans-action.php?getGrid=payList&nd='+new Date().getTime(),
			datatype: "json",
			jsonReader: {
				tradenumber: 'Stradenumber',
				repeatitems : true
            },
			sortable: true,
			height: 400,
			//height: 'auto', 
			colNames:[
				'Trade#',
				'Date', 
				'Amount', 
				'CCY',
				'Remaining',
				'Resolved',
				'Ex.Rate',
				'Amount',
				'CCY',
				'Remaining',
				'Resolved'
			],
			colModel:[
				{name:'tradenumber',index:'tradenumber', width:70,sortable: true, align:"left",sortable: true, sorttype:'int',summaryType:'count', summaryTpl : 'Total Trades:({0})'},
				{name:'created',index:'created', width:70, height:30,sortable: true, sorttype:'date', datefmt:'d-m-Y'},
				{name:'amountA',index:'amountA', width:70,sortable: true, align:"right" ,sorttype:'number_format',formatter:'number', summaryType:'sum'},
				{name:'currencyBuy',index:'currencyBuy',sortable: true, width:30, align:"center"},
				{name:'amountA1',index:'amountA1', width:70,sortable: true, align:"right" ,sorttype:'number_format',formatter:'number', summaryType:'sum',cellattr:function(rowId, value, rowObject, colModel, arrData) {
				return 'class=amountAP'+rowId+'';
				}},
				{name:'amountA2',index:'amountA2', width:70, align:"right" ,sorttype:'number_format',formatter:'number', summaryType:'sum'},
				{name:'newRateP',index:'newRateP',sortable: true, width:50, align:"right",sorttype:'number_format',formatter:'number', formatoptions: { decimalSeparator: ".", thousandsSeparator: " ", decimalPlaces: 4, defaultValue: '0.0000' },cellattr:function(rowId, value, rowObject, colModel, arrData) {
				return 'class=newRateP'+rowId+'';
				}},
				{name:'amountB',index:'amountB', width:60,sortable: true, align:"right" ,sorttype:'number_format',formatter:'number', summaryType:'sum',cellattr:function(rowId, value, rowObject, colModel, arrData) {
				return 'class=amountBP'+rowId+'';
				}},
				{name:'currencySell',index:'currencySell', width:30,sortable: true, align:"center"},
				{name:'amountB1',index:'amountB1', width:60,sortable: true, align:"right" ,sorttype:'number_format',formatter:'number', summaryType:'sum'},
				{name:'amountB2',index:'amountB2', width:60,sortable: true, align:"right" ,sorttype:'number_format',formatter:'number', summaryType:'sum'}
			],
			
			imgpath:gridimgpath, 
			rowNum: maxRows,
			rowList: [10,20,50,100,200],
			pager: jQuery('#pagernav'),
			//sortname: 'created',
			viewrecords: true,
			hoverrows: true,
            scrollTimeout: 0,
            altRows: true,
			multiselect:true,
			sortorder: 'created',
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			caption: "Trades",
			footerrow:true,
			userDataOnFooter:true,
			onSelectRow: function(rowId) { handleSelectedRow1(rowId); },
			grouping: true,
			groupingView : { groupField : ['created','currencyBuy','currencySell'],
			groupColumnShow : [true, true, true],
			//groupText : ['<b>{0}</b>', '{0}'],
			groupText : ['<b>{0} - {1} Trade(s)</b>'],
			 // formatDisplayField: [formatDisplay, formatDisplay, formatDisplay],
			groupCollapse : false,
			groupOrder: ['desc', 'asc','asc'],
			groupSummary : true },
				
	loadComplete: function () {
	var exchangeFinalValue = new Array();
	var globalRemaining = new Array();
	var globalAmount = new Array();
	
 $('table#payList tr:last').each(function(i, row){
  //var trId = ($(row).attr('id'));
  var td = $(this).find('td').each(function(){
  

var amountValues = $(this).attr('class');

if(amountValues == "amountAPundefined"){
$("."+amountValues).each(function(){
globalRemaining.push($(this).text());


});
}


if(amountValues == "amountBPundefined"){

$("."+amountValues).each(function(){
globalAmount.push($(this).text());
//alert(amountValue)


});
}

for(var i=0;i<globalAmount.length;i++){
var total = (parseFloat(globalAmount[i].replace(',', ''))/parseFloat(globalRemaining[i].replace(',', '')));


//var total = (parseFloat(globalAmount[i])/parseFloat(globalRemaining[i]));
exchangeFinalValue.push(total.toFixed(4));
//exchangeFinalValue.push(total);

}

//for(var j=0;j<exchangeFinalValue.length;j++){
//alert(exchangeFinalValue[j]);
$(".newRatePundefined").each(function(i){

$(this).text(exchangeFinalValue[i]);

});



 // exchangeFinalValue = (globalAmount)/(globalRemaining);
// alert(globalAmount+"   "+globalRemaining);
 // }
  });

  
});
}
				/* loadComplete: function () {
 $('table#payList tr').each(function(i, row){
  var trId = ($(row).attr('id'));
  if(typeof(trId) != 'undefined'){ 
    if(trId.search('payListghead_1_') > -1){
        $("#"+trId).hide();
    }  
  }
});
   } */	
			});
			jQuery("#payList").jqGrid('navGrid','#pagernav',{add:false,edit:false,del:false}); 
			
			jQuery("#payList").jqGrid('setGroupHeaders', { useColSpanStyle: true,
			groupHeaders:[ {startColumnName: 'amountA', numberOfColumns: 4, titleText: '<b>Sold<b>'},
			{startColumnName: 'amountB', numberOfColumns: 4, titleText: '<b>Bought<b>'} ] });
			jQuery("#payList").jqGrid('setFrozenColumns');
		
		var counter1 = 0;
	Object.defineProperty(Object.prototype, "associativeIndexOf", { 
    value: function(value) {
        for (var key in this) if (this[key] == value) return key;
        return -1;
    }
});
		
function handleSelectedRow1(id) {
		
        var amountA     = jQuery('#payList').getCell(id, 'amountA');
		var payExchange = jQuery('#payList').getCell(id, 'newRateP');
		var amountB     = jQuery('#payList').getCell(id, 'amountB'); 
		var currencyA1 = jQuery('#payList').getCell(id, 'currencyBuy');
		var currencyA2 = jQuery('#payList').getCell(id, 'currencySell');
        var cbIsChecked = $('#jqg_payList_'+id).is(':checked');
		//var cbIsUnchecked = $('#jqg_payList_'+id).is(':unchecked');
	
	//var arraysize = allCurrencies1.length;
	  var arraysize = Object.keys(allCurrencies1).length;
	  if(arraysize>0)
	  {
		  for (var i = 0; i < arraysize; i += 1) 
		  {
		//alert(allCurrencies[i]);
					 //var a = allCurrencies1.indexOf(currencyA1+'/'+currencyA2);
					 var a = allCurrencies1.associativeIndexOf(currencyA1+'/'+currencyA2);
					  if(a==-1){
						
							alert('Currencies are not matched');
							$('#jqg_payList_'+id).attr('checked', false);  
							return false;
							
						}
						 
			}
			 
		}
	  
	  
	   //allCurrencies1[id] = currencyA1+'/'+currencyA2;
	    allCurrencies1['jqg_payList_'+id] = currencyA1+'/'+currencyA2;
	    counter1 ++;
	 
	
		if(cbIsChecked==true)
         {
            if(amountA!=null)
                {
					totalAmtA = parseFloat(totalAmtA) + parseFloat(amountA);
					totalAmtB = parseFloat(totalAmtB) + parseFloat(amountB);
					var totalPayExchange = totalAmtB / totalAmtA ;					
                }
         }
		 else
             {
	delete allCurrencies1['jqg_payList_'+id];
             if(amountA!=null)
                {
                    totalAmtA = parseFloat(totalAmtA) - parseFloat(amountA);
					totalAmtB = parseFloat(totalAmtB) - parseFloat(amountB);
                }
             }
	
          jQuery('#payList').jqGrid('footerData','set',{created:'Total Amount:',amountA:totalAmtA,newRateP:totalPayExchange,amountB:totalAmtB});
console.log(allCurrencies1);
        } 

		
		
		jQuery("#transList").jqGrid({
			url:'trade-trans-action.php?getGrid=transList&nd='+new Date().getTime(),
			datatype: "json",
			jsonReader: {
				refNumberIM: 'SrefNumberIM',
				repeatitems : true
            },
			sortable: true,
			height: 400,
			//height: 'auto', 
			colNames:[
				'Ref#', 
				'Date',
				'Transaction Type',
				'Transaction Status',
				'Amount',
				'CCY',
				'Remaining',
				'Resolved',
				'Ex.Rate',
				'Amount',
				'CCY',
				'Remaining',
				'Resolved'
				
			],
					
			 colModel:[
				{name:'refNumberIM',index:'refNumberIM', width:80, align:"center",sortable: true, sorttype:'int',summaryType:'count', summaryTpl : ' Total Trans:({0})'},
				{name:'transDate',index:'transDate', width:70,sortable: true, height:30, sorttype:'date', datefmt:'d-m-Y'},
				{name:'transType',index:'transType', width:100,sortable: true, height:30, sorttype:'date', datefmt:'d-m-Y'},
				{name:'transStatus',index:'transStatus', width:100,sortable: true, height:30, sorttype:'date', datefmt:'d-m-Y'},
				//{name:'amountA',index:'amountA', width:70,sortable: true, align:"right", sorttype:'number',formatter:'number',summaryType:'sum'},
				{name:'totalAmount',index:'totalAmount', width:70,sortable: true, align:"right",sorttype:'number_format',formatter:'number',summaryType:'sum',cellattr:function(rowId, value, rowObject, colModel, arrData) {
				return 'class=totalAmount'+rowId;
				}},
				{name:'currencyFrom',index:'currencyFrom', width:30, sortable: true, align:"center"},
				{name:'amountA1',index:'amountA1', width:70, sortable: true,align:"right", sorttype:'number_format',formatter:'number',summaryType:'sum',cellattr:function(rowId, value, rowObject, colModel, arrData) {
				return 'class=amount'+rowId;
				}},
				{name:'amountA2',index:'amountA2', width:70,sortable: true, align:"right", sorttype:'number_format',formatter:'number',summaryType:'sum'},
				{name:'newRate',index:'newRate', width:50,sortable: true, align:"right", sorttype:'number_format',formatter:'number', formatoptions: { decimalSeparator: ".", thousandsSeparator: " ", decimalPlaces: 4, defaultValue: '0.0000' } ,cellattr:function(rowId, value, rowObject, colModel, arrData) {
				return 'class=newRate'+rowId+'';
				}},
  				
				
				{name:'amountB',index:'amountB', width:60,sortable: true, align:"right", sorttype:'number_format',formatter:'number',summaryType:'sum',cellattr:function(rowId, value, rowObject, colModel, arrData) {
				return 'class=amountB'+rowId;} },
				{name:'currencyTo',index:'currencyTo', width:30, sortable: true, align:"center"},
				{name:'amountB1',index:'amountB1', width:60,sortable: true, align:"right", sorttype:'number_format',formatter:'number',summaryType:'sum'},
				{name:'amountB2',index:'amountB2', width:60,sortable: true, align:"right", sorttype:'number_format',formatter:'number',summaryType:'sum'}
			],
			 
			imgpath:gridimgpath, 
			rowNum: maxRows,
			//height: 'auto',
			rowList: [10,20,50,100,200],
			pager: jQuery('#pagernavT'),
			//sortname: 'transDate',
			viewrecords: true,
			hoverrows: true,
            scrollTimeout: 0,
            altRows: true,
			multiselect:true,
			//sortorder: "desc",
			sortorder: 'transDate',
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			caption: "Transactions",
			footerrow:true,
			userDataOnFooter:true,
			onSelectRow: function(rowId) { handleSelectedRow(rowId); },
			
			grouping: true,
			groupingView : { groupField : ['transDate','currencyFrom','currencyTo'],
			groupColumnShow : [true,true,true],
			//groupText : ['<b>{0}</b>', '</b>{0}</b>'],
			groupText : ['<b>{0} - {1} Transaction(s)</b>'],
			 // formatDisplayField: [formatDisplay, formatDisplay, formatDisplay],
			groupCollapse : false,
			groupOrder: ['desc', 'asc','asc'],
			groupSummary : true},
				
	loadComplete: function () {
	var exchangeFinalValue = new Array();
	var globalRemaining = new Array();
	var globalAmount = new Array();
	
 $('table#transList tr:last').each(function(i, row){
  //var trId = ($(row).attr('id'));
  var td = $(this).find('td').each(function(){
  

var amountValues = $(this).attr('class');

if(amountValues == "amountundefined"){
$("."+amountValues).each(function(){
globalRemaining.push($(this).text());


});
}


if(amountValues == "amountBundefined"){

$("."+amountValues).each(function(){
globalAmount.push($(this).text());
//alert(amountValue)


});
}

for(var i=0;i<globalAmount.length;i++){
//var total = (parseFloat(globalAmount[i])/parseFloat(globalRemaining[i]));
//alert (globalAmount[i].replace(',', ''))
var total = (parseFloat(globalAmount[i].replace(',', ''))/parseFloat(globalRemaining[i].replace(',', '')));

exchangeFinalValue.push(total.toFixed(4));

}

//for(var j=0;j<exchangeFinalValue.length;j++){
//alert(exchangeFinalValue[j]);
$(".newRateundefined").each(function(i){

$(this).text(exchangeFinalValue[i]);

});



 // exchangeFinalValue = (globalAmount)/(globalRemaining);
// alert(globalAmount+"   "+globalRemaining);
 // }
  });

  
});
}
			
			});
		
			   /* function formatDisplay(currentValue, sourceValue, option, groupIndex, groupObject) {
			  
			   if(groupIndex== 2){
       
		
        if(option === undefined) {
            return "option of value \"" + currentValue + "\" is undefined!";
        } else {
            return groupObject.lastvalues[1]+" - "+currentValue;
        }
		}else if(groupIndex== 1){
		//$("#transListghead_"+groupIndex+"_").css("display", "none");
		return currentValue;
		}else
		{
		return currentValue;
		}
    } */
			jQuery("#transList").jqGrid('navGrid','#pagernavT',{add:false,edit:false,del:false}); 
			
			jQuery("#transList").jqGrid('setGroupHeaders',
			{ useColSpanStyle: true, groupHeaders:[ {startColumnName: 'totalAmount',
			numberOfColumns: 4, titleText: '<b>Sold<b>'},
			{startColumnName: 'amountB', numberOfColumns: 4, titleText: '<b>Bought<b>'} ]

			});
			jQuery("#transList").jqGrid('setFrozenColumns');
			
	
		
		var counter = 0;
/* Object.defineProperty(Object.prototype, "associativeIndexOf", { 
    value: function(value) {
        for (var key in this) if (this[key] == value) return key;
        return -1;
    }
}); */
		function handleSelectedRow(id) {
		
        var amountA     = jQuery('#transList').getCell(id, 'totalAmount');
		var transExchangeRate = jQuery('#transList').getCell(id, 'newRate');
		var amountB     = jQuery('#transList').getCell(id, 'amountB');
		var currencyB1 = jQuery('#transList').getCell(id, 'currencyFrom');
		var currencyB2 = jQuery('#transList').getCell(id, 'currencyTo');
        var cbIsChecked = $('#jqg_transList_'+id).is(':checked');
		//var cbIsUnChecked = $('#jqg_transList_'+id).is(':unchecked');
		
	 //var arraysize = allCurrencies.length;
	  var arraysize = Object.keys(allCurrencies).length;
	  // alert(arraysize);
	  if(arraysize>0){
		  for (var i = 0; i < arraysize; i += 1) {
 //alert(allCurrencies[i]);
			 //var a = allCurrencies.indexOf(currencyB1+'/'+currencyB2);
			 var a = allCurrencies.associativeIndexOf(currencyB1+'/'+currencyB2);
			 //alert(a);
				  if(a==-1 ){
				  alert('currencies are not matched');
				  $('#jqg_transList_'+id).attr('checked', false); 
				  return false;
				  }  
			 }
	  }
	  
	   allCurrencies['jqg_transList_'+id] = currencyB1+'/'+currencyB2;
	    counter ++;
	
		if(cbIsChecked == true )
         {
            if(amountA!=null)
                {
                    totalAmtA1 = parseFloat(totalAmtA1) + parseFloat(amountA);
					totalAmtB1 = parseFloat(totalAmtB1) + parseFloat(amountB);
					totalExchange = totalAmtB1 / totalAmtA1;
                }
         }
		 else
             {
delete allCurrencies['jqg_transList_'+id];
             if(amountA!=null)
                {
                   totalAmtA1 = parseFloat(totalAmtA1) - parseFloat(amountA);
				   totalAmtB1 = parseFloat(totalAmtB1) - parseFloat(amountB);
				   
                }
             }

            jQuery('#transList').jqGrid('footerData','set',{transDate:'Total Amount:',totalAmount:totalAmtA1,amountB:totalAmtB1,newRate:totalExchange});

console.log(allCurrencies);
        } 	

		jQuery("#sendBtn").click( function(){
			if($(this).val()=="Deal"){
				var payIDs = jQuery("#payList").getGridParam('selarrrow');
				var transIDs = jQuery("#transList").getGridParam('selarrrow');
				if(payIDs!='' && transIDs!=''){
					if(reconcileCheck(payIDs,transIDs,'')==false){
						return false;
					}
					var confirmMsg = confirm("Are you sure to Proceed?");
					if(confirmMsg==false)
						return false;
					var allPayIds = $("#allPayIdsV").val();
					var allPayIdsArr = allPayIds.split(",");
					var allTransIDs = $("#allTransIDsV").val();
					var allTransIdsArr = allTransIDs.split(",");
					var userType = $("#userType").val();

					$("#payListMatchData").load("trade-trans-action.php?btnAction=makeDeal",
					{ 'payIDs[]': allPayIdsArr,'transIDs[]': allTransIdsArr,'userTypes': userType},
						 function(){
						alert($("#payListMatch").val());
					});
				}
				else{
					alert("Please Select Rows from both sides");
					$("#allPayIdsV").val('');
					$("#allTransIDsV").val('');
					return false;
				}

				$("#userType").val("customer");
				gridReload('bothList');
				//gridReload('bothList');
			}
		});
		
		jQuery("#resolvebtn").click( function(){
			if($(this).val()=="Resolve"){
				var transIDs = jQuery("#transList").getGridParam('selarrrow');
				//alert(transIDs);
				$("#allTransIDsV").val(transIDs);
if(transIDs!=''){

//var transarr=transIDs.split(",");
//alert(transarr);
var check=false;
var checkArr =new Array();
checkArr =[];
$("#transList tr" ).each(function() {
var id = this.id;
ifchecked = $("#jqg_transList_"+id).is(":checked");
if(ifchecked){
var status=$(this).find('td').eq(4).text();
var transIDArr = id+",";
if(status=="Credited")
{
checkArr.push(1);
check=true;
}else{
checkArr.push(0);	
	
}						

}
						
						
						// $(transIDs).each(function() {
						
						// if(transIDs==id && status=="Credited")
						// {
							// checkArr.push(1);
							// check=true;
						// }
						
						// });					
					
					});
					
if(checkArr.indexOf(0) ==-1){
			
if(checkArr.length >0){
					
						var confirmMsg = confirm("Are you sure to Proceed?");
						if(confirmMsg==false){
				

							
							return false;
						}
							

									var allTransIDs = $("#allTransIDsV").val();
						var allTransIdsArr = allTransIDs.split(",");
						var userType = $("#userType").val();
				
						$("#payListMatchData").load("trade-trans-action.php?btnAction=resolve",
						{'transIDs[]': allTransIdsArr,'userTypes': userType},
							 function(){
							alert($("#payListMatch").val());
						});
					}
					else
					{
						alert("Please Select Credited transactions.");
					}
}else{
	alert("Please Select Only Credited transactions.");
}
				}
				else{
					alert("Please Select Rows from transaction.");
					$("#allTransIDsV").val('');
					return false;
				}

				$("#userType").val("customer");
				gridReload('bothList');
				//gridReload('bothList');
			}
		});
		
		
		jQuery("#searchTradeBtn").click( function(){
		location.reload();
/*			$("#payListMatchData").load("trade-trans-action.php?btnAction=loadDeals",
						 function(msg){
						alert(msg);
					});*/
			/* $.ajax({
			   type: "GET",
			   url: "trade-trans-action.php?btnAction=loadDeals",
			   //data: "iban_num="+document.getElementById("iban").value,
			   beforeSend: function(XMLHttpRequest){
					var transIDsR = jQuery("#transList").getGridParam('selarrrow');
					if(transIDsR!=''){
						if(reconcileCheck(transIDsR,transIDsR,'T')==false){
							return false;
						}
					}
			   },
			   success: function(msg){
			   	//alert("Response is :: "+msg);
			   }
			 }); */
		});
		
	});
	function deletePayments(pids){
		var amountPay   =0;
		var pids = pids.toString();
		$("#allPayIdsV").val(pids);
		$("#allTransIDsV").val(tids);
	}
    function reconcileCheck(pids,tids,tradeFlag){
		//alert(pids+','+tids+','+tradeFlag);
		var transListID = "transList";
		var payListID = "payList";
		if(tradeFlag=="T"){
			payListID = transListID = "transList";
		}
		var amountPay   =0;
		var amountTrans =0;
		//var amountPay1   =0;
		//var amountTrans1 =0;
		var pids = pids.toString();
		var tids = tids.toString();
		$("#allPayIdsV").val(pids);
		$("#allTransIDsV").val(tids);
		var allPayIds	=new Array();
		var allTransIds	=new Array();
		var allPayCurrA = new Array();
		var allTransCurrA = new Array();
		var allPayCurrB = new Array();
		var allTransCurrB = new Array();
	
		if(pids!=null)
			allPayIds   = pids.split(",");
		if(tids!=null)
			allTransIds = tids.split(",");
		//alert('payList = '+allPayIds);
		//alert('transList = '+allTransIds);
		for(var p=0;p<allPayIds.length;p++){
			var payG	= jQuery("#"+payListID).getRowData(allPayIds[p]);
			amountPay	= parseFloat(amountPay) + parseFloat(payG.amountB);
			//amountPay1	= parseFloat(amountPay1) + parseFloat(payG.amountA);
			allPayCurrA[p]  = payG.currencyBuy;
			allPayCurrB[p]  = payG.currencySell;
		}
//alert("amountPay1.length="+amountPay1.length);

		for(var t=0;t<allTransIds.length;t++){
			var transG	= jQuery("#"+transListID).getRowData(allTransIds[t]);
			amountTrans = parseFloat(amountTrans) + parseFloat(transG.amountB);
			//amountTrans1 = parseFloat(amountTrans1) + parseFloat(transG.totalAmount);
			allTransCurrA[t]  = transG.currencyFrom;
			allTransCurrB[t]  = transG.currencyTo;
		}
//alert("amountTrans1.length="+amountTrans1.length);
//alert("allTransCurr.length="+allTransCurr.length);
		
		for(var c1=0;c1<allTransCurrA.length;c1++){
			for(var c2=0;c2<allPayCurrA.length;c2++){
				var transCurr = allTransCurrA[c1];
				
				
				//alert("allPayCurr[c2]="+allPayCurr[c2]);
				var payCurr = allPayCurrA[c2];
				
				if(transCurr.toString()!=payCurr.toString()){
					alert("First Currencies do not match.Please select same currency Amounts.");
					$("#allPayIdsV").val('');
					$("#allTransIDsV").val('');
					return false;
				}
			}
		}
		for(var c1=0;c1<allTransCurrB.length;c1++){
			for(var c2=0;c2<allPayCurrB.length;c2++){
				var transCurr = allTransCurrB[c1];
				
				
//alert("allPayCurrB[c2]="+allPayCurr[c2]);
				var payCurr = allPayCurrB[c2];
				if(transCurr.toString()!=payCurr.toString()){
					alert("Currency sold to customer is different than bought from bank.");
					$("#allPayIdsV").val('');
					$("#allTransIDsV").val('');
					return false;
				}
			}
		}
		if(amountPay<amountTrans){
		alert("Currency sold is less than bought from bank.\nTotal Trade Amounts = "+amountPay+"\nTotal Transaction Amount = "+amountTrans+"\nPlease Select again or Add more trade.");
			$("#allPayIdsV").val('');
			$("#allTransIDsV").val('');
		//alert(amountPay+"-1-"+amountTrans);
		return false;
		/* alert("Currency sold is less than bought from bank.\nTotal Trade Amounts = "+amountPay+"\nTotal Transaction Amount = "+amountTrans+"\nPlease Select again or Add more trade.<?='<a href=\'#\' onClick=\'popup(http://'.$_SERVER['SERVER_NAME'].'/'.$_SERVER['SCRIPT_NAME'].')\'>Click here</a>'?>");
			return true; */
		 
		}
	/*  if(amountPay1<amountTrans1){
		//alert(amountPay1+"-2-"+amountTrans1);
		
			alert("Currency sold is less than bought from bank.\nTotal Trade Amounts = "+(amountPay1)+"\nTotal Transaction Amount = "+parseFloat(amountTrans1)+"\nPlease Select again or Add more trade.");
			$("#allPayIdsV").val('');
			$("#allTransIDsV").val('');
			return false; 
		} 
		*/
	}
	function gridReload(grid)
	{
		var theUrl = "trade-trans-action.php";
	
		//empty checked value on both grid on reload the grid.
		allCurrencies = [];
		allCurrencies1 = [];
		
		/* allCurrencies.splice(0, allCurrencies.length);
		allCurrencies1.splice(0, allCurrencies1.length); */
			
		if(grid=='payList' || grid=='bothList' || grid=='deletePayList'){
			var extraParam='?';
			var from = jQuery("#from").val();
			var to = jQuery("#to").val();
			var amount1 = jQuery("#amount1").val();
			var tradenumber1 = jQuery("#tradenumber1").val();
			var desc = jQuery("#desc").val();
			var currencyTrade = jQuery("#currencyTrade").val();
			var originCurrencyTrade = jQuery("#originCurrencyTrade").val();
			if(grid=='payList'){
				extraParam = extraParam + "from="+from+"&to="+to+"&desc="+desc+"&amount1="+amount1+"&currencyTrade="+currencyTrade+"&originCurrencyTrade="+originCurrencyTrade+"&tradenumber1="+tradenumber1+"&Submit=SearchPay&getGrid=payList&nd="+new Date().getTime();
			}
			else{
				extraParam = extraParam + "getGrid=payList&nd="+new Date().getTime();
			}
			jQuery("#payList").setGridParam({
				url: theUrl+extraParam,
				page:1
			}).trigger("reloadGrid");
		}
	
		if(grid=='transList' || grid=='bothList'){
			var extraParam='?';
			var fromT = jQuery("#fromT").val();
			var toT = jQuery("#toT").val();
			var amountT = jQuery("#amountT").val();
			var searchBy = jQuery("#searchType").val();
			var searchName = jQuery("#searchName").val();
			var currencyTrans = jQuery("#currencyTrans").val();
			var originCurrencyTrans = jQuery("#originCurrencyTrans").val();
			var agentUsername = jQuery("#agentUsername").val();
			var customerName = jQuery("#customerName").val();
			if(grid=='transList'){
				$("#userType").val('customer');
				extraParam = extraParam + "fromT="+fromT+"&toT="+toT+"&amountT="+amountT+"&searchBy="+searchBy+"&searchName="+searchName+"&currencyTrans="+currencyTrans+"&originCurrencyTrans="+originCurrencyTrans+"&agentUsername="+agentUsername+"&customerName="+customerName+"&Submit=SearchTrans&getGrid=transList&nd="+new Date().getTime();
			}
			else{
				extraParam = extraParam + "getGrid=transList&nd="+new Date().getTime();
			}
			jQuery("#transList").setGridParam({
				url: theUrl+extraParam,
				page:1
			
			}).trigger("reloadGrid");
		}
		
		
	}
	function reconciledOptions(id){
		var ret 	= jQuery("#payListList").getRowData(id);
		var recOptIds  = jQuery("#recOptIds").val();
		var recArr = new Array();
		if(recOptIds!=''){
			recArr = recOptIds.split(',');
			//alert('id='+id);
			//alert('Length of Array = '+recArr.length +' index[0]='+recArr[0]+' index[1]='+recArr[1]);
			//alert('Exists = '+recArr.indexOf(id));
			if(recArr.indexOf(id)==-1){
				recArr.push(id);
				recArr.join(',');
				jQuery("#recOptIds").val(recArr);
			}
		}
		else{
			jQuery("#recOptIds").val(id);
		}
		alert('After recOptIds='+jQuery("#recOptIds").val());
		//+" Reconcile="+ret.mark+" Username="+ret.username+" UserType="+ret.userType+" Notes="+ret.notes
	}
	function uniqueArr(arrayName)
	{
		var newArray=new Array();
		label:for(var i=0; i<arrayName.length;i++ )
		{  
			for(var j=0; j<newArray.length;j++ )
			{
				if(newArray[j]==arrayName[i]) 
					continue label;
			}
			newArray[newArray.length] = arrayName[i];
		}
		return newArray;
	}
	
</script>
 
</head>
<!--<body style="font-size: 50px !important;" font-size: 14px !important; >-->
<body>
	<form name="frmSearch" id="frmSearch">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc" colspan="2"><b><strong><font color="#FFFFFF">Deal Traded to Not-Traded</font></strong></b></td>
  </tr>
  <tr>
	<td valign='top'>
	<table  border="0" style="float:left; vertical-align:top;" >
    <tr>
    <td align="center">
      <table border="1" cellpadding="5" bordercolor="#666666">
      <tr>
            <td nowrap bgcolor="#6699cc"><span class="tab-u"><strong>Transactions Search Filter</strong></span></td>
        </tr>
        <tr>
		  <td nowrap align="center">From Date 
				<input name="fromT" type="text" id="fromT" readonly >
				&nbsp;<a href="javascript:show_calendar('frmSearch.fromT');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp; &nbsp;To Date&nbsp;
				<input name="toT" type="text" id="toT" readonly >
				&nbsp;<a href="javascript:show_calendar('frmSearch.toT');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
				<!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->			</td>
        </tr>
        <tr>
	      <td nowrap align="center"> 

           	  Transaction Reference Number 
              <input name="searchName" type="text" id="searchName" >		</td>
      </tr>
	  
	     <tr>
	      <td nowrap align="center"> 
          Agent's Username
		  <input name="agentUsername" type="text" id="agentUsername" >	
			  &nbsp;&nbsp;&nbsp;
           	Customer Name 
              <input name="customerName" type="text" id="customerName" >		</td>
      </tr>
	  
	  
	  <tr>
		  <td nowrap="nowrap" align="center">
	  	  Amount 
            <input name="amountT" type="text" id="amountT" >
			&nbsp;&nbsp;&nbsp;
              Destination Currency
              <select name="currencyTrans" id="currencyTrans">
				  <option value="">Select Currency</option>
			  	  <? if(count($currenciesFrom)>0){?>
					  <? for($ct=0;$ct<count($currenciesFrom);$ct++){?>
					  <option value="<?=$currenciesFrom[$ct]["currencyFrom"]?>"><?=$currenciesFrom[$ct]["currencyFrom"]?></option>
					  <? }?>
				  <? }?>
			  </select> 

			 <!-- 
			 <select name="currencyTrans" id="currencyTrans">
				  <option value="">Select Currency</option>
			  	  <?// if(count($currencyies)>0){?>
					  <?// for($ct=0;$ct<count($currencyies);$ct++){?>
					  <option value="<?//=$currencyies[$ct]["currency"]?>"><?//=$currencyies[$ct]["currency"]?></option>
					  <?// }?>
				  <?// }?>
			  </select>-->

			  Origin Currency
              <select name="originCurrencyTrans" id="originCurrencyTrans">
				  <option value="">Select Currency</option>
			  	  <? //echo count($OriginCurrencies);   
				  if(count($currenciesTo)>0){?>
					  <? for($oct=0;$oct<count($currenciesTo);$oct++){?>
					  <option value="<?=$currenciesTo[$oct]["currencyTo"]?>"><?=$currenciesTo[$oct]["currencyTo"]?></option>
					  <? }?>
				  <? }?>
			  </select>
			  &nbsp;&nbsp;&nbsp;
			<input type="button" name="Submit" value="Search Transactions" onClick="gridReload('transList')">		  </td>
	  </tr>
    </table>  	</td>
	</tr>
    
    <tr><td>&nbsp;</td></tr>
	<tr>
    <td valign="top">
    			<table id="transList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
				
				<div id="pagernavT" class="scroll" style="text-align:center;"></div>	
				
				<table id="list4"><tr><td>&nbsp;</td></tr></table>
				</td>
    </tr>
	  <tr bgcolor="#FFFFFF">
		<td align="center"><input name="searchTradeBtn" id="searchTradeBtn" type="button"  value="Reload All">&nbsp;&nbsp;&nbsp;<input name="resolvebtn" id="resolvebtn" type="button"  value="Resolve"></td>
	  </tr>
</table></td>
	<td valign='top'>
	<table  border="0" style="float:left; vertical-align:top" >
    <tr>
    <td align="center">
      <table border="1" cellpadding="5" bordercolor="#666666">
      <tr>
            <td nowrap bgcolor="#6699cc"><span class="tab-u"><strong>Trade Search Filter<? echo $from2 . $to;?></strong></span></td>
        </tr>
        <tr>
				
                  <td nowrap align="center">From Date 
                  	<input name="from" type="text" id="from" readonly >
                  	<!--input name="from" type="text" id="from" value="<?=$from1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('frmSearch.from');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp; &nbsp;To Date&nbsp;
		    		    <input name="to" type="text" id="to" readonly >
		    		    <!--input name="to" type="text" id="to" value="<?=$to1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('frmSearch.to');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
		    		    <!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->		    		    </td>
                </tr>
        <tr>
            <td nowrap align="center"><!--input name="refNumber" type="text" id="refNumber" value=<?=$refNumber?>-->
		  				Amount 
              <input name="amount1" type="text" id="amount1" >
              <!--input name="amount1" type="text" id="amount1" value=<?=$amount1?>-->
			  &nbsp;&nbsp;&nbsp;
              Description 
              <input name="desc" type="text" id="desc" >
              <!--input name="desc" type="text" id="desc" value=<?=$desc?>-->		</td>
      </tr>
	  <tr>
         <td nowrap align="center">
		 Trade# 
              <input name="tradenumber1" type="text" id="tradenumber1" >
              <!--input name="amount1" type="text" id="amount1" value=<?=$amount1?>-->
			  &nbsp;&nbsp;&nbsp;
              Destination Currency
              <select name="currencyTrade" id="currencyTrade">
				  <option value="">Select Currency</option>
			  	  <? if(count($currenciesBuy)>0){?>
					  <? for($cp=0;$cp<count($currenciesBuy);$cp++){?>
					  <option value="<?=$currenciesBuy[$cp]["currencyBuy"]?>"><?=$currenciesBuy[$cp]["currencyBuy"]?></option>
					  <? }?>
				  <? }?>
			  </select> 
			  
			  <!--<select name="currencyTrade" id="currencyTrade">
				  <option value="">Select Currency</option>
			  	  <? //if(count($currencyies)>0){?>
					  <? //for($cp=0;$cp<count($currencyies);$cp++){?>
					  <option value="<?//=$currencyies[$cp]["currency"]?>"><?//=$currencyies[$cp]["currency"]?></option>
					  <?// }?>
				  <?// }?>
			  </select>-->
			  Origin Currency
              <select name="originCurrencyTrade" id="originCurrencyTrade">
				  <option value="">Select Currency</option>
			  	  <? if(count($currenciesSell)>0){?>
					  <? for($oc=0;$oc<count($currenciesSell);$oc++){?>
					  <option value="<?=$currenciesSell[$oc]["currencySell"]?>"><?=$currenciesSell[$oc]["currencySell"]?></option>
					  <? }?>
				  <? }?>
			  </select>
			  &nbsp;&nbsp;&nbsp;
					 </td>
					 <tr><td nowrap align="center"><input type="button" name="Submit" value="Search Trade" onClick="gridReload('payList')"></td></tr>
    </table>  	</td>
	</tr>
    
    <tr><td>&nbsp;</td></tr>
	<tr>
    <td valign="top">
    			<table id="payList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernav" class="scroll" style="text-align:center;"></div>
				<div id="payListMatchData" style="visibility:hidden;"></div>
				<div id="payListDeleteData" style="visibility:hidden;"></div>	
				<table id="list4"><tr><td>&nbsp;</td></tr></table>
				
				
				</td>
    </tr>
	  <tr bgcolor="#FFFFFF">
		<td align="center">
		<input name="sendBtn" id="sendBtn" type="button"  value="Deal">
		<input type='hidden' name='totTrans' id='totTrans'>
		<input type='hidden' name='recOptIds' id='recOptIds' value="">
		<input type="hidden" name="allPayIdsV" id="allPayIdsV" value="">
		<input type="hidden" name="allTransIDsV" id="allTransIDsV" value="">
		<input type="hidden" name="userType" id="userType" value="customer"></td>
	  </tr>
</table>	</td>
  </tr>
</table>
</form>
</body>
</html>