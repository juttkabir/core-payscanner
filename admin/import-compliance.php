<?
session_start();
include ("../include/config.php");
$tran_date = date("Y-m-d");
$date_time = date('d-m-Y  h:i:s A');
require_once './cm_xls/Excel/reader.php';
include ("security.php");
$modifyby = $_SESSION["loggedUserData"]["userID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;


if($_REQUEST["Submit"] != "")
{
	
		 if($_REQUEST["listName"] == "sdn"){
		     include "import-ofac-sdn.php";
		 }elseif($_REQUEST["listName"] == "add"){
				include "import-ofac-add.php";
		 }elseif($_REQUEST["listName"] == "alt"){
				include "import-ofac-alt.php";
		}elseif($_REQUEST["listPEP"] == "peps"){
	      include "import-comp-pep.php";
		}elseif($_REQUEST["hm_treasury"] == "hm"){
		 include "import-compliance-hm-treasury.php";
		}
}		

?>
<script language="javascript" src="jquery.js"></script>
<script language="javascript" type="text/javascript">

function updateGUI()
{
	if($("#listName").attr('checked'))
	{
		$("#ofac_list").show();
		$("#PEP_list").hide();
		$("#hm").hide();
			
	}
	else
	{
		$("#ofac_list").hide();
			
	}
if($("#listPEP").attr('checked'))
	{
		$("#PEP_list").show();
		$("#ofac_list").hide();
		$("#hm").hide();
			
	}
	else
	{
		$("#PEP_list").hide();
				
	}
if($("#hm_treasury").attr('checked'))	
	{
		$("#hm").show();
		$("#ofac_list").hide();
		$("#PEP_list").hide();
	}
	else
	{
		$("#hm").hide();
	}
 if($("#listName").attr('checked')||$("#listPEP").attr('checked') || $("#hm_treasury").attr('checked'))
   {
    $("#button").show();
   }
   else
   {
    $("#button").hide();
   }	
	
}
</script>

<html>
<link href="images/interface.css" rel="stylesheet" type="text/css">
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000}
-->
</style>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
<form name="addComplaint" action="import-compliance.php" method="post" enctype="multipart/form-data">
  <tr>
    <td bgcolor="#6699cc"><b><strong><font color="#FFFFFF">Import Compliance List </font></strong></b></td>
  </tr>
  <tr>
    <td>
	<table width="265"  border="0">
  	 <tr>
    	<td width="259" valign="top"><fieldset>
    		<legend class="style2">Import Compliance List </legend>
    		<br>
	 <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
			
			  <? 
			   if($msg!= "" && $flag == "N") {
			  ?>
			  <tr align="center">
			    <td colspan="2"><b><font color= <? echo CAUTION_COLOR ?> ><? echo $msg?></font></b><br></td>
			    </tr>
			  
			  <?
			  }else{
			 	?>
				<tr align="center">
			    <td colspan="2"><b><font color= <? echo SUCCESS_COLOR ?> ><? echo $msg?></font></b><br></td>
			    </tr>
				<? } ?>
			  	<tr>
						<td bgcolor="#DFE6EA" colspan="2">Select File Name </td>
			  	</tr>
		 <tr>
		   <td width="20%" bgcolor="#DFE6EA" align="left">
				<input type="checkbox" name="listName" id ="listName" 
				value="OFAC" <?=$_REQUEST["listName"] == "OFAC"?'checked="checked"':'' ?> onClick="updateGUI();">
			</td>
			<td width="80%"  bgcolor="#DFE6EA">OFAC (<a href="https://www.treasury.gov/resource-center/sanctions/SDN-List/Pages/sdn_data.aspx" style="color: red">Source Online</a>)</td>
		</tr>
				
		<tr>
		  <td width="20%" bgcolor="#DFE6EA" align="left">
		 	<input type="checkbox" name="listPEP" id ="listPEP" 
			value="PEP"<?=$_REQUEST["listPEP"] == "PEP"?'checked="checked"':'' ?> onClick="updateGUI();"> 
		  </td>
	 	<td width="80%"  bgcolor="#DFE6EA">PEPs</td>
	</tr>
	<tr>
	  <td bgcolor="#DFE6EA" align="left">
	  		<input type="checkbox" name="hm_treasury" id ="hm_treasury" 
		   	value="hm_treasury"<?=$_REQUEST["hm_treasury"] == "hm_treasury"?'checked="checked"':'' ?> onClick="updateGUI();">
	  </td>
	   <td  bgcolor="#DFE6EA">HM Treasury (<a href="http://hmt-sanctions.s3.amazonaws.com/sanctionsconlist.xls" style="color: red">Source Online</a>) </td>
	  </tr>	
	  <tr id="hm" style="display:none">
	    <td width="20%" bgcolor="#DFE6EA" align="left">&nbsp;</td>
	  	<td width="80%" bgcolor="#DFE6EA" align="left">
			<input type="radio" name="hm_treasury" value="hm">&nbsp;&nbsp; HM Treasury
		</td>
	  </tr>	
	</table>   
			 
			
	<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1" id="ofac_list" style="display:none">
		 <tr>
		   <td width="20%" bgcolor="#DFE6EA" align="left">&nbsp;</td>
		   <td width="80%" bgcolor="#DFE6EA" align="left">
			 <input type="radio" name="listName" value="sdn">&nbsp;&nbsp; SDN.del 
		   </td>
		 </tr>
		 <tr>
		  <td width="20%" bgcolor="#DFE6EA" align="left">&nbsp;</td>
		  <td width="80%" bgcolor="#DFE6EA" align="left">
			<input type="radio" name="listName" value="add">&nbsp;&nbsp; ADD.del
		  </td>
		</tr>
		<tr>
		   <td width="20%" bgcolor="#DFE6EA" align="left">&nbsp;</td>
		   <td width="20%" bgcolor="#DFE6EA" align="left">
			<input type="radio" name="listName" value="alt">&nbsp;&nbsp; ALT.del 
		   </td>
		</tr>
 </table>
 <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1" id="PEP_list" style="display:none">
		 <tr>
		 	<td width="20%" bgcolor="#DFE6EA" align="left">&nbsp;</td>
			<td width="80%" bgcolor="#DFE6EA" align="left">
				<input type="radio" name="listPEP" value="peps">&nbsp;&nbsp;  PEPs
			</td>
		 </tr>
  </table>
			
  <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1" id="button" style="display:none">
	   <tr>
	     <td width="25%" align="right" bgcolor="#DFE6EA">Select File </td>
		 <td width="75%" bgcolor="#DFE6EA">
		 	<input name="csvFile" type="file" id="csvFile" size="15">
		  </td>
		</tr>
		<tr>
		  <td align="right" valign="top" bgcolor="#DFE6EA">&nbsp;</td>
		  <td align="center" bgcolor="#DFE6EA">
		   	<input name="Submit" type="submit" class="flat" value="Submit">
		   </td>
		     </tr>
	</table>		
    <br>
   </fieldset>
     </td>
    </tr>
</table>
 
    </td>
  </tr>
   </form>	
</table>
</body>
</html>