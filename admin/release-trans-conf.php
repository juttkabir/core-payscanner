<?
session_start();
include ("../include/config.php");
include ("ledgersUpdation.php");
include ("security.php");

$date_time = date('d-m-Y  h:i:s A');
$limit_date = date('Y-m-d');

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$agentType = getAgentType();
$username = $_SESSION["loggedUserData"]["username"];
$loggedUserID = $_SESSION["loggedUserData"]["userID"];
$root_path = $_SERVER['DOCUMENT_ROOT'];
$payOutFlag = true;
$_strNotPay = '';

if($agentType == "TELLER")
{
	$teller = " by Teller";
	$userID = $_SESSION["loggedUserData"]["tellerID"];
	$parantID = $_SESSION["loggedUserData"]["cp_ida_id"];
}else
{
	$teller = " ";
//$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$userID = $_SESSION["loggedUserData"]["userID"];
}
$changingPerson = "$userID"."$agentType";

if($_GET["id"] != "")
{
	$id = $_GET["id"];
}else{
	$id = "";
}
$qryString .= "&id=".$id;   	    
	
if($_GET["searchBy"] != "")
{
	$by = $_GET["searchBy"];
}else{
	$by = "";
}
$qryString .= "&searchBy=".$by; 

if($_GET["fromDate"]!=""){
	$fromDate = $_GET["fromDate"];
}else{
  $fromDate = "";
}
$qryString .= "&fromDate=".$fromDate;   	    

if($_GET["toDate"]!=""){
	$toDate = $_GET["toDate"];
}else{
  $toDate = "";
}
$qryString .= "&toDate=".$toDate;   

if($_GET["transType"] != "")
{
	$type = $_GET["transType"];
}else
{	
	$type = "";
}
$qryString .= "&transType=".$type;

if(!empty($_REQUEST["srcUrl"]))
	$strBackUrl = $_REQUEST["srcUrl"];
else
	$strBackUrl = "release-trans.php";

$trnsid = $_POST["trnsid"];

if(defined("CONFIG_API_TRANSACTIONS") && CONFIG_API_TRANSACTIONS == "CMT")
	require(LOCAL_DIR."api/gopi/api_functions.php");
$strXmlResponseString = "";

function writeOutOnSftp($sftp, $strXml)
{
	if(!empty($strXml))
	{
		$strXmlResponse = '<?xml version="1.0" encoding="UTF-8" ?>
						<TMTPayoutUpdate xmlns="tmt" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'.$strXml.'
						</TMTPayoutUpdate>';
		
		// validate the file, before transmitting to the remote server 
		//if(validateXmlFromFile($strXml, TMTPUP))
		//{	
			$strFileName = PID."TMTPUP_".date("dmYHis").".xml";
			do{
				$bolFileNotWritten = false;
				if(!writeFileOnSftp($sftp, $strFileName, $strXmlResponse))
				{
					// If file not written than try it again with a sllep time of 10 seconds 
					$bolFileNotWritten = true;
					echo "Unable to write '$strFileName' file, on client system, trying it again in next 5 seconds.<br />";
					sleep(5);
				}
			}while($bolFileNotWritten);
			
			//debug($strXmlResponse);
		//}
		//else
		//{
		//	debug($strXml);
		//	debug("Not A valid document.",true);
		//}
		
	}
}

function callApiModif($intTransId, $_arrCountries)
{
	$strXmlResponse = '';
	if(defined("CONFIG_API_TRANSACTIONS") && !empty($intTransId))
	{
		
		$strTransactionDetSql = "select sending_agent, rejectDate, deliveryDate, refNumber, benAgentID, partner_id, Address, City, Country, Phone, IDType, IDNumber from transactions t, beneficiary b where transid = '".$intTransId."' and from_server = '".CONFIG_API_TRANSACTIONS."' and is_parsed = 'Y' and b.benID = t.benID";
		$arrTransactionDet = selectFrom($strTransactionDetSql);
		
		//debug($arrTransactionDet);
		
		$intStatus = "01";
		$strUpdateDate = str_replace(" ","T",$arrTransactionDet["deliveryDate"]);
		if($_REQUEST["transactionStatus"]  == "Rejected")
		{
			$intStatus = "02";
			$strUpdateDate = str_replace(" ","T",$arrTransactionDet["rejectDate"]);
		}

		$_arrIdTypes = array(
			 "Passport" => "01",
			 "National ID" => "02",
			 "Driver�s License" => "03",
			 "Other Government ID" => "04",
			 "Other" => "06"
		);
		
		$strCountry = explode(" (",$arrTransactionDet["Country"]);
		$strIsoCountry = '';
		
		while ($countryName = current($_arrCountries)) 
		{
		   if (strtoupper($countryName) == $strCountry[0])
			   $strIsoCountry =  key($_arrCountries);
		   next($_arrCountries);
		}

		$intReceiverId = $_arrIdTypes[$arrTransactionDet["IDType"]];
		if(empty($intReceiverId))
			$intReceiverId = "7";
		$strReceiverIdNumber = $arrTransactionDet["IDNumber"];
		if(empty($strReceiverIdNumber))
			$strReceiverIdNumber = "Any photo id";

		//<tmtTransactionNumber>'.$arrTransactionDet["refNumber"].'</tmtTransactionNumber>
		//if(!empty($arrTransactionDet["IDNumber"]))			
		//{
			$strXmlResponse = '<Transaction>
										<partnerID>'.$arrTransactionDet["partner_id"].'</partnerID>
										<agentID>'.$arrTransactionDet["sending_agent"].'</agentID>
										<tmtTransactionNumber>'.$arrTransactionDet["refNumber"].'</tmtTransactionNumber>
										<updateDateTime>'.$strUpdateDate.'</updateDateTime>
										<updateType>'.$intStatus.'</updateType>
										<userId>'.$_SESSION["loggedUserData"]["username"].'</userId>
										<payoutAgentId>'.$arrTransactionDet["benAgentID"].'</payoutAgentId>
										<receiverAddress1>'.$arrTransactionDet["Address"].'</receiverAddress1>
										<receiverCity>'.$arrTransactionDet["City"].'</receiverCity>
										<receiverCountry>'.$strIsoCountry.'</receiverCountry>
										<receiverPhoneNumber>'.$arrTransactionDet["Phone"].'</receiverPhoneNumber>
										<receiverIdType>'.$intReceiverId.'</receiverIdType>
										<receiverIdProvided>'.$strReceiverIdNumber.'</receiverIdProvided>
										<messageDateTime>'.date("Y-m-d\TH:i:s").'</messageDateTime>
									</Transaction>';
		//}
	}
	return $strXmlResponse;
}

if($_POST["btnAction"] != "")
{
	if(count($trnsid) >= 1)
	{		
			for ($i=0;$i< count($trnsid);$i++)
			{

			 	$contentTrans = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$trnsid[$i]."'");
				//if(!empty($_REQUEST["transactionStatus"]))
				//{
					/*** Transction Status to Failed Starts ***/
					if( $_REQUEST["transactionStatus"]  == "Failed")////Deposit Agent		
					{
						transStatusChange($trnsid[$i], "Failed", $username, $_REQUEST["remarks"]);
						if(CONFIG_SHARE_OTHER_NETWORK == '1')
						{
							$remoteTransInfo = selectFrom("select * from " . TBL_SHARED_TRANSACTIONS . " where localTrans = '".$trnsid[$i]."'");
							if($remoteTransInfo["remoteTrans"] != '')
							{
								$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$remoteTransInfo["remoteServerId"]."' and isEnabled = 'Y'");
								if($jointClient["clientId"] != '')
								{
									$otherClient = new connectOtherDataBase();
									$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
									
									$otherTransaction = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".transactions where transID = '".$remoteTransInfo["remoteTrans"]."' and (transStatus = 'Authorize' OR transStatus = 'Amended')");		
									
									if($otherTransaction["transID"] != '')
									{						
										$otherClient->update("update ".$jointClient["dataBaseName"].".transactions set 
													transStatus = 'Failed', 
													deliveredBy = '".$username."',
													failedDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."',
													
													remarks		= '".$_REQUEST["remarks"]."'
													where transID = '".$otherTransaction["transID"]."'
													");	
										
										$remoteCustAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["custAgentID"]."'");											
										
										if($remoteCustAgent["agentType"] == 'Sub')
										{
											$otherClient-> updateSubAgentAccount($remoteCustAgent["userID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
											$q = $otherClient-> updateAgentAccount($remoteCustAgent["parentID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
										}
										else
										{
											$q = $otherClient-> updateAgentAccount($remoteCustAgent["userID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
										}				
										if(CONFIG_REMOTE_DISTRIBUTOR_POST_PAID != '1')
										{
											$remoteBenAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["benAgentID"]."'");
												
											if($remoteBenAgent["agentType"] == 'Sub')
											{
												$otherClient-> updateSubAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
												$q = $otherClient-> updateAgentAccount($remoteBenAgent["parentID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
											}
											else
											{
												$q = $otherClient-> updateAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
											}	
										}
									}
									$otherClient->closeConnection();	
									dbConnect();	
								}
							}
						}
					}
					/*** Transaction Status Failed Ends ***/
					
					/*** Suspicious transaction starts ***/
					elseif( $_REQUEST["transactionStatus"]  == "Suspicious")
					{
						update("update " . TBL_TRANSACTIONS . " set transStatus= 'Suspicious', remarks = '". $_REQUEST["remarks"]."', suspeciousDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i]."'");
						activities($_SESSION["loginHistoryID"],"UPDATION",$trnsid[$i],TBL_TRANSACTIONS,"Transaction is Suspicious");		 
					}
					/*** Suspicious ends ***/
					
					/*** Rejected transaction starts ***/
					
					elseif($_REQUEST["transactionStatus"]  == "Rejected")///Deposit Agent//Cancelled		
					{
						
						////////Remote transaction status Rejected////
						if(CONFIG_SHARE_OTHER_NETWORK == '1')
						{
							
							
							$remoteTransInfo = selectFrom("select * from " . TBL_SHARED_TRANSACTIONS . " where localTrans = '".$trnsid[$i]."'");
							
							if($remoteTransInfo["remoteTrans"] != '')
							{
								
								$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$remoteTransInfo["remoteServerId"]."' and isEnabled = 'Y'");
								if($jointClient["clientId"] != '')
								{
									$otherClient = new connectOtherDataBase();
									$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
									
									
									$otherTransaction = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".transactions where transID = '".$remoteTransInfo["remoteTrans"]."' and (transStatus = 'Authorize' OR transStatus = 'Amended')");		
									
									if($otherTransaction["transID"] != '')
									{						
										$otherClient->update("update ".$jointClient["dataBaseName"].".transactions set 
										transStatus = 'Rejected', 
										deliveredBy = '".$username."',
										failedDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."',
										
										remarks		= '".$_REQUEST["remarks"]."'
										where transID = '".$otherTransaction["transID"]."'
										");	
										
										$remoteCustAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["custAgentID"]."'");											
										
										
										if($remoteCustAgent["agentType"] == 'Sub')
										{
										$otherClient-> updateSubAgentAccount($remoteCustAgent["userID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
										$q = $otherClient-> updateAgentAccount($remoteCustAgent["parentID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
										}else{
										$q = $otherClient-> updateAgentAccount($remoteCustAgent["userID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
										}				
										if(CONFIG_REMOTE_DISTRIBUTOR_POST_PAID != '1')
										{
										$remoteBenAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["benAgentID"]."'");
										
										if($remoteBenAgent["agentType"] == 'Sub')
										{
										$otherClient-> updateSubAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
										$q = $otherClient-> updateAgentAccount($remoteBenAgent["parentID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
										}else{
										$q = $otherClient-> updateAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
										}	
										}
									}
									
									$otherClient->closeConnection();		
									dbConnect();
								}
							}
						}
						
						//////////////////Remote transaction Rejected ended/////
						
						
						
						$amount = selectFrom("select totalAmount, custAgentID, transAmount,localAmount, customerID, createdBy, refNumberIM, benAgentID, AgentComm,currencyFrom,currencyTo from " . TBL_TRANSACTIONS . " where transID = '". $trnsid[$i]."'");
						
						
						$balance = $amount["totalAmount"];
						$agent = $amount["custAgentID"];
						$bank = $amount["benAgentID"];
						$transBalance = $amount["transAmount"];
						$agentComm = $amount["AgentComm"];
						$currecyFromLedgerAgent = $amount["currencyFrom"];
						
						if(DEST_CURR_IN_ACC_STMNTS == "1" || CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == "1"){
						$amountToLedgerDist = $amount["localAmount"];
						$currecyToLedgerDist = $amount["currencyTo"];
						}else{		
						$amountToLedgerDist = $amount["transAmount"];
						$currecyToLedgerDist = $amount["currencyFrom"];
						}
						
						if(CONFIG_EXCLUDE_COMMISSION == '1')
						{
						$agentBalance = $balance - $agentComm;
						}else{
						$agentBalance = $balance;
						}
						
						
						update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_REQUEST["transactionStatus"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_REQUEST["remarks"]."', rejectDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i]."'");
						activities($_SESSION["loginHistoryID"],"UPDATION",$trnsid[$i],TBL_TRANSACTIONS,"Transaction is Rejected");	
						if($amount["createdBy"] != 'CUSTOMER')
						{
						$payin = selectFrom("select payinBook, customerID from " . TBL_CUSTOMER . " where customerID = '". $amount["customerID"]."'");
						if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")
						{
						///
						if(CONFIG_EXCLUDE_COMMISSION == '1')
						{
						$agentBalance = $balance - $commission;
						}else{
						$agentBalance = $balance;
						}
						
						$agentContents = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from " . TBL_ADMIN_USERS . " where userID = '$agent'");
						$currentBalance = $agentContents["balance"];
						
						
						$currentBalance = $agentBalance + $currentBalance;
						update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$agent'");
						
						////////////////Either to insert into Bank or A&D Account////////
						if($agentContents["agentType"]=="Sub")
						{
						$agent=$agentContents["parentID"];
						$q = updateAgentAccount($agent, $agentBalance, $trnsid[$i], "DEPOSIT", "Transaction Rejected $teller", "Agent",$note,$currecyFromLedgerAgent);
						updateSubAgentAccount($agentContents["userID"], $agentBalance, $trnsid[$i], "DEPOSIT", "Transaction Rejected $teller", "Agent","");
						}else{
						$agent=$agentContents["userID"];
						$q = updateAgentAccount($agent, $agentBalance, $trnsid[$i], "DEPOSIT", "Transaction Rejected $teller", "Agent",$note,$currecyFromLedgerAgent);
						}
						
						
						}elseif($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == "1")
						insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
						values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Rejected','DEPOSIT','".$balance."','".$trnsid[$i]."','".$agent."'
						)");
						}elseif($amount["createdBy"] == 'CUSTOMER')
						{
						$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
						values('".$amount["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Rejected','DEPOSIT','$balance','".$amount["refNumberIM"]."'
						)";
						insertInto($strQuery);
						}
						
						if(CONFIG_POST_PAID != '1')
						{
						$checkBalance2 = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$bank."'");						
						
						$currentBalanceBank = $checkBalance2["balance"]	+ $transBalance;				
						update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '".$bank."'");
						
						////////////////Either to insert into Bank or A&D Account////////
						
						
						
						if($checkBalance2["agentType"]=="Sub")
						{
						$dist=$checkBalance2["parentID"];
						$q = updateAgentAccount($dist, $amountToLedgerDist,$trnsid[$i], "DEPOSIT", "Transaction Rejected $teller", "Distributor",$note,$currecyToLedgerDist);
						updateSubAgentAccount($checkBalance2["userID"], $transBalance, $trnsid[$i], "DEPOSIT", "Transaction Rejected $teller", "Distributor","");
						}else{
						$dist=$checkBalance2["userID"];
						$q = updateAgentAccount($dist, $amountToLedgerDist,$trnsid[$i], "DEPOSIT", "Transaction Rejected $teller", "Distributor",$note,$currecyToLedgerDist);
						}
						
						
						}
						
					}
					/*** Rejected transaction ends   ***/
				//}
				else
				{
				$contentTrans = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$trnsid[$i]."'");
//=======
				//if(!empty($_REQUEST["transactionStatus"]))
				//{
					/*** Transction Status to Failed Starts ***/
					if( $_REQUEST["transactionStatus"]  == "Failed")////Deposit Agent		
					{
						transStatusChange($trnsid[$i], "Failed", $username, $_REQUEST["remarks"]);
						if(CONFIG_SHARE_OTHER_NETWORK == '1')
						{
							$remoteTransInfo = selectFrom("select * from " . TBL_SHARED_TRANSACTIONS . " where localTrans = '".$trnsid[$i]."'");
							if($remoteTransInfo["remoteTrans"] != '')
							{
								$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$remoteTransInfo["remoteServerId"]."' and isEnabled = 'Y'");
								if($jointClient["clientId"] != '')
								{
									$otherClient = new connectOtherDataBase();
									$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
									
									$otherTransaction = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".transactions where transID = '".$remoteTransInfo["remoteTrans"]."' and (transStatus = 'Authorize' OR transStatus = 'Amended')");		
									
									if($otherTransaction["transID"] != '')
									{						
										$otherClient->update("update ".$jointClient["dataBaseName"].".transactions set 
													transStatus = 'Failed', 
													deliveredBy = '".$username."',
													failedDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."',
													
													remarks		= '".$_REQUEST["remarks"]."'
													where transID = '".$otherTransaction["transID"]."'
													");	
										
										$remoteCustAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["custAgentID"]."'");											
										
										if($remoteCustAgent["agentType"] == 'Sub')
										{
											$otherClient-> updateSubAgentAccount($remoteCustAgent["userID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
											$q = $otherClient-> updateAgentAccount($remoteCustAgent["parentID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
										}
										else
										{
											$q = $otherClient-> updateAgentAccount($remoteCustAgent["userID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
										}				
										if(CONFIG_REMOTE_DISTRIBUTOR_POST_PAID != '1')
										{
											$remoteBenAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["benAgentID"]."'");
												
											if($remoteBenAgent["agentType"] == 'Sub')
											{
												$otherClient-> updateSubAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
												$q = $otherClient-> updateAgentAccount($remoteBenAgent["parentID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
											}
											else
											{
												$q = $otherClient-> updateAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
											}	
										}
									}
									$otherClient->closeConnection();	
									dbConnect();	
								}
							}
						}
					}
					/*** Transaction Status Failed Ends ***/
					
					/*** Suspicious transaction starts ***/
					elseif( $_REQUEST["transactionStatus"]  == "Suspicious")
					{
						update("update " . TBL_TRANSACTIONS . " set transStatus= 'Suspicious', remarks = '". $_REQUEST["remarks"]."', suspeciousDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i]."'");
						activities($_SESSION["loginHistoryID"],"UPDATION",$trnsid[$i],TBL_TRANSACTIONS,"Transaction is Suspicious");		 
					}
					/*** Suspicious ends ***/
					
					/*** Rejected transaction starts ***/
					
					elseif($_REQUEST["transactionStatus"]  == "Rejected")///Deposit Agent//Cancelled		
					{
						
						////////Remote transaction status Rejected////
						if(CONFIG_SHARE_OTHER_NETWORK == '1')
						{
							
							
							$remoteTransInfo = selectFrom("select * from " . TBL_SHARED_TRANSACTIONS . " where localTrans = '".$trnsid[$i]."'");
							
							if($remoteTransInfo["remoteTrans"] != '')
							{
								
								$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$remoteTransInfo["remoteServerId"]."' and isEnabled = 'Y'");
								if($jointClient["clientId"] != '')
								{
									$otherClient = new connectOtherDataBase();
									$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
									
									
									$otherTransaction = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".transactions where transID = '".$remoteTransInfo["remoteTrans"]."' and (transStatus = 'Authorize' OR transStatus = 'Amended')");		
									
									if($otherTransaction["transID"] != '')
									{						
										$otherClient->update("update ".$jointClient["dataBaseName"].".transactions set 
										transStatus = 'Rejected', 
										deliveredBy = '".$username."',
										failedDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."',
										
										remarks		= '".$_REQUEST["remarks"]."'
										where transID = '".$otherTransaction["transID"]."'
										");	
										
										$remoteCustAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["custAgentID"]."'");											
										
										
										if($remoteCustAgent["agentType"] == 'Sub')
										{
										$otherClient-> updateSubAgentAccount($remoteCustAgent["userID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
										$q = $otherClient-> updateAgentAccount($remoteCustAgent["parentID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
										}else{
										$q = $otherClient-> updateAgentAccount($remoteCustAgent["userID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
										}				
										if(CONFIG_REMOTE_DISTRIBUTOR_POST_PAID != '1')
										{
										$remoteBenAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["benAgentID"]."'");
										
										if($remoteBenAgent["agentType"] == 'Sub')
										{
										$otherClient-> updateSubAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
										$q = $otherClient-> updateAgentAccount($remoteBenAgent["parentID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
										}else{
										$q = $otherClient-> updateAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
										}	
										}
									}
									
									$otherClient->closeConnection();		
									dbConnect();
								}
							}
						}
						
						//////////////////Remote transaction Rejected ended/////
						
						
						
						$amount = selectFrom("select totalAmount, custAgentID, transAmount,localAmount, customerID, createdBy, refNumberIM, benAgentID, AgentComm,currencyFrom,currencyTo from " . TBL_TRANSACTIONS . " where transID = '". $trnsid[$i]."'");
						
						
						$balance = $amount["totalAmount"];
						$agent = $amount["custAgentID"];
						$bank = $amount["benAgentID"];
						$transBalance = $amount["transAmount"];
						$agentComm = $amount["AgentComm"];
						$currecyFromLedgerAgent = $amount["currencyFrom"];
						
						if(DEST_CURR_IN_ACC_STMNTS == "1" || CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == "1"){
						$amountToLedgerDist = $amount["localAmount"];
						$currecyToLedgerDist = $amount["currencyTo"];
						}else{		
						$amountToLedgerDist = $amount["transAmount"];
						$currecyToLedgerDist = $amount["currencyFrom"];
						}
						
						if(CONFIG_EXCLUDE_COMMISSION == '1')
						{
						$agentBalance = $balance - $agentComm;
						}else{
						$agentBalance = $balance;
						}
						
						
						update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_REQUEST["transactionStatus"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_REQUEST["remarks"]."', rejectDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i]."'");
						activities($_SESSION["loginHistoryID"],"UPDATION",$trnsid[$i],TBL_TRANSACTIONS,"Transaction is Rejected");	
						if($amount["createdBy"] != 'CUSTOMER')
						{
						$payin = selectFrom("select payinBook, customerID from " . TBL_CUSTOMER . " where customerID = '". $amount["customerID"]."'");
						if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")
						{
						///
						if(CONFIG_EXCLUDE_COMMISSION == '1')
						{
						$agentBalance = $balance - $commission;
						}else{
						$agentBalance = $balance;
						}
						
						$agentContents = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from " . TBL_ADMIN_USERS . " where userID = '$agent'");
						$currentBalance = $agentContents["balance"];
						
						
						$currentBalance = $agentBalance + $currentBalance;
						update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$agent'");
						
						////////////////Either to insert into Bank or A&D Account////////
						if($agentContents["agentType"]=="Sub")
						{
						$agent=$agentContents["parentID"];
						$q = updateAgentAccount($agent, $agentBalance, $trnsid[$i], "DEPOSIT", "Transaction Rejected $teller", "Agent",$note,$currecyFromLedgerAgent);
						updateSubAgentAccount($agentContents["userID"], $agentBalance, $trnsid[$i], "DEPOSIT", "Transaction Rejected $teller", "Agent","");
						}else{
						$agent=$agentContents["userID"];
						$q = updateAgentAccount($agent, $agentBalance, $trnsid[$i], "DEPOSIT", "Transaction Rejected $teller", "Agent",$note,$currecyFromLedgerAgent);
						}
						
						
						}elseif($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == "1")
						insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
						values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Rejected','DEPOSIT','".$balance."','".$trnsid[$i]."','".$agent."'
						)");
						}elseif($amount["createdBy"] == 'CUSTOMER')
						{
						$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
						values('".$amount["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Rejected','DEPOSIT','$balance','".$amount["refNumberIM"]."'
						)";
						insertInto($strQuery);
						}
						
						if(CONFIG_POST_PAID != '1')
						{
						$checkBalance2 = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$bank."'");						
						
						$currentBalanceBank = $checkBalance2["balance"]	+ $transBalance;				
						update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '".$bank."'");
						
						////////////////Either to insert into Bank or A&D Account////////
						
						
						
						if($checkBalance2["agentType"]=="Sub")
						{
						$dist=$checkBalance2["parentID"];
						$q = updateAgentAccount($dist, $amountToLedgerDist,$trnsid[$i], "DEPOSIT", "Transaction Rejected $teller", "Distributor",$note,$currecyToLedgerDist);
						updateSubAgentAccount($checkBalance2["userID"], $transBalance, $trnsid[$i], "DEPOSIT", "Transaction Rejected $teller", "Distributor","");
						}else{
						$dist=$checkBalance2["userID"];
						$q = updateAgentAccount($dist, $amountToLedgerDist,$trnsid[$i], "DEPOSIT", "Transaction Rejected $teller", "Distributor",$note,$currecyToLedgerDist);
						}
						
						
						}
						
						/**
						 * If the transaction of the API than send the response back to the Sending Server
						 */
						$strXmlResponseString .= callApiModif($trnsid[$i], $_arrCountries);
					}
					/*** Rejected transaction ends   ***/
				//}
				else
				{
				$contentTrans = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$trnsid[$i]."'");
/* merge-right.r4909 */
				
				/* #5088-AMB Exchange-- Distributor Deal Sheet*/
					if(CONFIG_DISTRIBUTION_DEAL_SHEET == "1"){
						if(strstr(CONFIG_DEAL_SHEET_VIEW_USERS,$agentType))
						{
							$action = "payout";
							$msg ="";
							$_arrReturnFlag = array();
							include_once("$root_path/deal_sheet/server.php");
							$arrInput = array("action" =>$action,"transID"=>$trnsid[$i],"agentType"=>$agentType,"loggedUserID"=>$loggedUserID);
							//debug( $arrInput);
							$payOutFlag = payOut($arrInput);
						}
					
					if(!$payOutFlag)
							$_strNotPay .= $contentTrans["refNumberIM"]." ,";
					}
			if($payOutFlag){	
				if($contentTrans["transType"] == "Bank Transfer")
				{
					$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '".$trnsid[$i]."' and (transStatus='Authorize' or transStatus='Amended')");
					$balance = $amount["totalAmount"];
					$bank = $amount["benAgentID"];
					$transBalance = $amount["transAmount"];		
					
											
					if($agentType == "TELLER")
					{
						$tellerInfo = selectFrom("select balance from " . TBL_TELLER . " where tellerID = '$userID'");		
						$currentTellerBalance = $tellerInfo["balance"] - $transBalance;				
						update("update " . TBL_TELLER . " set balance  = $currentTellerBalance where tellerID = '$userID'");
						insertInto("insert into ".TBL_TELLER_ACCOUNT."(tellerID, dated, type, amount, modified_by, TransID, description) values('$userID', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 'WITHDRAW', '$transBalance', '$userID', '".$trnsid[$i]."', 'Transaction Credited $teller')");
					}
					if(CONFIG_POST_PAID == '1')
					{
							$checkBalance2 = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$bank."'");
							$currentBalanceBank = $checkBalance2["balance"]	- $transBalance;
							update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '".$bank."'");
								
							insertInto("insert into bank_account(bankID,dated,type,amount,modified_by,TransID,description,settlementCurrency)values('$bank',
							'".getCountryTime(CONFIG_COUNTRY_CODE)."','WITHDRAW', '$transBalance', '$loggedUserID','".$trnsid[$i]."', 'Transaction Credited', '$currency')");
					
							////////////////Either to insert into Bank or A&D Account////////
							if($checkBalance2["agentType"]=="Sub")
							{
								$dist=$checkBalance2["parentID"];
								$q = updateAgentAccount($dist, $transBalance, $trnsid[$i], "WITHDRAW", "Transaction Credited $teller", "Distributor");
									 updateSubAgentAccount($checkBalance2["userID"], $transBalance, $trnsid[$i], "WITHDRAW", "Transaction Credited $teller", "Distributor","");
							}else{
								$dist=$checkBalance2["userID"];
								$q = updateAgentAccount($dist, $transBalance, $trnsid[$i], "WITHDRAW", "Transaction Credited $teller", "Distributor",'','');
							}
					}
						
					update("update " . TBL_TRANSACTIONS . " set transStatus= 'Credited', trackingNum = '', remarks = '". $_REQUEST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."', deliveredBy = '$changingPerson' where transID='".$trnsid[$i]."'");
					$descript = "Transaction is Credited";
					activities($_SESSION["loginHistoryID"],"UPDATION",$trnsid[$i],TBL_TRANSACTIONS,$descript);		 
					
				}
				else
				{
					// this is enhancement of system that assign the daily limit to teller.
					
					if($agentType == "TELLER"){
						$tellerInfo = selectFrom("select * from " . TBL_TELLER . " where tellerID = '$userID'");		
						//echo ("select * from " . TBL_TELLER_LIMIT . " where teller_id = '$userID' and limit_date = '$limit_date'");				
						//echo $limit_date;
						$checklimit = selectFrom("select * from " . TBL_TELLER_LIMIT . " where teller_id = '$userID' and limit_date = '$limit_date'");
						$amount2 = selectFrom("select  transAmount, customerID from " . TBL_TRANSACTIONS . " where transID = '". $trnsid[$i]."'");
					}	
					//echo("account Limit ".$checklimit["account_limit"]);
					if($checklimit["account_limit"])
					{							
						$transBalance = $amount2["transAmount"];
					
						$limit = $checklimit["used_limit"]+ $transBalance;
					
					 	$limit_alert = $tellerInfo["account_limit"] - $limit;
									
						if ($limit <= $tellerInfo["account_limit"])
						{						
							update("update " . TBL_TELLER_LIMIT . " set used_limit = '" . $limit . "' where teller_id ='$userID'and limit_date = '$limit_date'");
				
					
							// send email if limit is less then limit allert amount.					
					
							if ($limit_alert <= $tellerInfo["limit_alert"])
							{
								$emailToParant=selectFrom("select email from admin  where userID = '$parantID'");
								//echo("select email from admin  where userID = '$parantID'");
								$adminEmail=$emailToParant["email"];
								$subject = "Please extend my daily account limit";
								$fromName = $tellerInfo["loginName"];
								$fromEmail = $tellerInfo["loginName"];
								$message="
								<table>  
									<tr>
							    	<td>Please extend my daily accunt limit</td>
							    	<td>&nbsp;</td>
							  	</tr>
								</table>
								";
								
								sendMail($adminEmail, $subject, $message, $fromName, $fromEmail);
						
							}			
							// end mail send
					
							$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $trnsid[$i]."' and (transStatus='Authorize' or transStatus='Amended')");
							$balance = $amount["totalAmount"];
							$bank = $amount["benAgentID"];
							$transBalance = $amount["transAmount"];		
						
							if($agentType == "TELLER")
							{
								$tellerInfo = selectFrom("select balance from " . TBL_TELLER . " where tellerID = '$userID'");		
								$currentTellerBalance = $tellerInfo["balance"] - $transBalance;				
								update("update " . TBL_TELLER . " set balance  = $currentTellerBalance where tellerID = '$userID'");
								insertInto("insert into ".TBL_TELLER_ACCOUNT." (tellerID, dated, type, amount, modified_by, TransID, description) values('$userID', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 'WITHDRAW', '$transBalance', '$userID', '".$trnsid[$i]."', 'Transaction Picked up $teller')");
							}
							if(CONFIG_POST_PAID == '1')
							{
								transStatusChange($trnsid[$i], "Picked up", $username, '');
							
							}
						
							update("update " . TBL_TRANSACTIONS . " set transStatus= 'Picked up', trackingNum = '', remarks = '". $_REQUEST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."', deliveredBy = '$changingPerson' where transID='".$trnsid[$i]."'");
							$descript = "Transaction is Picked up";
							activities($_SESSION["loginHistoryID"],"UPDATION",$trnsid[$i],TBL_TRANSACTIONS,$descript);		 
					
						}
						else
						{
							insertError(AG40);
							redirect($strBackUrl."?msg=Y&dist=$_GET[dist]&act=".$_GET['act']."&submit=".$_GET['submit']."$qryString");	
						
						}
					}else{					
					
						$limitQuery = "insert into  " . TBL_TELLER_LIMIT . " (teller_id, limit_date, account_limit, used_limit, limit_alert) values ( '$userID' , '$limit_date', '".$tellerInfo["account_limit"]."','".$amount2["transAmount"]."', '".$tellerInfo["limit_alert"]."')";
						insertInto($limitQuery);
						
						$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $trnsid[$i]."' and (transStatus='Authorize' or transStatus='Amended')");
						$balance = $amount["totalAmount"];
						$bank = $amount["benAgentID"];
						$transBalance = $amount["transAmount"];		
												
						if($agentType == "TELLER")
						{
							$tellerInfo = selectFrom("select balance from " . TBL_TELLER . " where tellerID = '$userID'");		
							$currentTellerBalance = $tellerInfo["balance"]	- $transBalance;				
							update("update " . TBL_TELLER . " set balance  = $currentTellerBalance where tellerID = '$userID'");
							insertInto("insert into ".TBL_TELLER_ACCOUNT." (tellerID, dated, type, amount, modified_by, TransID, description) values('$userID', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 'WITHDRAW', '$transBalance', '$userID', '".$trnsid[$i]."', 'Transaction Picked up $teller')");
						}
						if(CONFIG_POST_PAID == '1')
						{
								transStatusChange($trnsid[$i], "Picked up", $username, '');
						}
							
							update("update " . TBL_TRANSACTIONS . " set transStatus= 'Picked up', trackingNum = '', remarks = '". $_REQUEST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."', deliveredBy = '$changingPerson' where transID='".$trnsid[$i]."'");
							$descript = "Transaction is Picked up";
							activities($_SESSION["loginHistoryID"],"UPDATION",$trnsid[$i],TBL_TRANSACTIONS,$descript);
					}
				}
			}	// end deal sheet check
			
				//include ("email.php");
			//$msg="Operation performed Successfully";
			

				/**
				 * If the transaction of the API than send the response back to the Sending Server
				 */
				$strXmlResponseString .= callApiModif($trnsid[$i], $_arrCountries);
			
			}
		}
	}// end for loop 
	   if(!$payOutFlag && CONFIG_DISTRIBUTION_DEAL_SHEET == "1")
		 {
			$msg ="Following Transactions are not Pay Out: ".$_strNotPay;
			$msg .= " Please Add Deal Sheet Then try to pay out Transaction
			<a href='add_deal_sheet.php' target=_blank style='text-decoration:underline; color:blue'>[Add New Deal Sheet]</a>";
			insertError($msg);
			redirect($strBackUrl."?msg=Y&dist=$_GET[dist]&success=$success&act=".$_GET['act']."&submit=".$_GET['submit']."$qryString");	
		 }
	}
	writeOutOnSftp($sftp, $strXmlResponseString);
	//debug($strXmlResponseString, true);
	if(!empty($_REQUEST["srcUrl"]))
		redirect($_REQUEST["srcUrl"]."?act=".$_GET['act']."&submit=".$_GET['submit']."$qryString");
	else
		redirect("release-trans.php?act=".$_GET['act']."&submit=".$_GET['submit']."$qryString");
	
	//debug($strBackUrl,true);	
	//redirect($strBackUrl."?act=".$_GET['act']."&submit=".$_GET['submit']."&dist=".$_REQUEST["dist"]."$qryString");
	//redirect("release-trans.php?act=".$_GET['act']."&submit=".$_GET['submit']."$qryString");
}
?>