<?php
	/** Display Report Viewed History (Report Logs)
	 *  @pakage Report
	 *  @subpaakge Report View History
	 *  @author Kalim ul Haq
	 *  copyrigth(c) HBS Technology (Pvt.) Ltd.
	 */
	session_start();
	include("../include/config.php");
	include("security.php");
	include ("javaScript.php");
$currdate=date('Y-m-d h:i:s');
	
	$systemCode	= SYSTEM_CODE;
	$company	= COMPANY_NAME;
	$systemPre	= SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$agentType	= getAgentType();
	$date_time	= date('d-m-Y  h:i:s A');
	
	$parentID	= $_SESSION["loggedUserData"]["userID"];
	
	/**	maintain report logs 
	 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
	 *	If not exist enter it in order to maintain report logs
	 */
	include_once("maintainReportsLogs.php");
	maintainReportLogs($_SERVER["PHP_SELF"]);
	
	// Set limit, records per page 
	$limit	= 20;
	if($offset == "")
		$offset = 0;
	if($_GET["newOffset"] != "")
		$offset = $_GET["newOffset"];
		
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
/*	
	if(!empty($_REQUEST["fromDate"]))
		$fromDate	= $_REQUEST["fromDate"];
	else
		$fromDate	= $currDate;
		
	if(!empty($_REQUEST["toDate"]))
		$toDate	= $_REQUEST["toDate"];
	else
		$toDate	= $currDate;
	*/
	
	
	
if($_GET["fromDate"]!="")
$_POST["fromDate"] = $_GET["fromDate"] ;
if($_GET["toDate"]!="")
 $_POST["toDate"] = $_GET["toDate"] ;

$fromDatevalue=$_POST["fromDate"];
  $toDatevalue=$_POST["toDate"];



  if(!empty($fromDatevalue)){
  $fromDatevalue = str_replace('/', '-', $fromDatevalue);
  $fromDate= date("Y-m-d",strtotime($fromDatevalue));
 }
 else
  $fromDate=$currdate;
 if(!empty($toDatevalue)){
  $toDatevalue = str_replace('/', '-', $toDatevalue);
  $toDate= date("Y-m-d",strtotime($toDatevalue));
 }
 else
  $toDate=$currdate;
  
    $fromDateReport=date("Y-m-d",strtotime($fromDate));
  $toDateReport=date("Y-m-d",strtotime($toDate));
	// Query to fatch data from DB
	$strQuery	= "SELECT
						lh.login_time,
						lh.login_name,
						lh.access_ip,
						lh.login_type,
						rh.id,
						rh.report_title,
						rh.count,
						rh.actionCount
					FROM
						viewReportHistory as rh
						LEFT JOIN " .TBL_LOGIN_HISTORY . " as lh
						ON rh.login_history_id = lh.history_id
					WHERE 1" ;

	// All Records Count Query
	$strQueryCnt	= "	SELECT
							COUNT(rh.id)
						FROM
							viewReportHistory as rh
							LEFT JOIN " .TBL_LOGIN_HISTORY . " as lh
							ON rh.login_history_id = lh.history_id
						WHERE 1
					   ";
					
	if(!empty($_REQUEST["userID"]) && $_REQUEST["userID"] != "all")
		$strQueryWhere .= " AND lh.user_id='".$_REQUEST["userID"]."' ";
		
	$strQueryWhere .= " AND (login_time >= '$fromDate 00:00:00' and login_time <= '$toDate 23:59:59') ";				
	
	$strQuery	.= $strQueryWhere;
	$strQuery	.= " ORDER BY lh.login_time DESC";
	$strQuery 	.= " LIMIT $offset , $limit";
	$contentsTrans = selectMultiRecords($strQuery);
	//debug($strQuery);repor
	
	// Count all records
	$strQueryCnt	.= $strQueryWhere;
	$allCount 		= countRecords($strQueryCnt);	

	// Set conditions for User Drop Down Menu
	$orderBy = " ORDER BY CONCAT(adminType, agentContactPerson) ";
	
	
	//work  by Mudassar Ticket #11425	
		if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){	
					$extraCondition = " AND agentStatus = 'Active' ";
					$arrArg = array("orderBy"=>$orderBy,"extraCondition"=>$extraCondition,"all"=>true);
					}
					
		else if (CONFIG_AGENT_WITH_ALL_STATUS==1){	
	
			$arrArg = array("orderBy"=>$orderBy,"all"=>true);
					}
							
	//work end by Mudassar Ticket #11425
	
	$strUserDropDownMenu	= getUserDropDown($arrArg);
	
	// Query String for pagination
	$strShowingOf	= "Showing <strong>".($offset+1)." - ".($offset+count($contentsTrans))."</strong> Of ".$allCount." ";
	$strQueryString = "&Submit=".$_REQUEST["Submit"]."&userID=".$_REQUEST["userID"]."&fromDate=".$fromDate."&toDate=".$toDate."";
?>
<html>
<head>
<title>View Reports Logs</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<script type="text/javascript" src="javascript/jquery.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript">
/*$(document).ready(function(){
	Date.format = 'yyyy-mm-dd';
	$("#fromDate").datePicker({
		startDate: '2000-01-01'
	});
	$("#toDate").datePicker({
		startDate: '2000-01-01'
	});
	*/
	$("#printReport").click(function(){
		// Maintain report print logs 
		$.ajax({
			url: "maintainReportsLogs.php",
			type: "post",
			data:{
				maintainLogsAjax: "Yes",
				pageUrl: "<?=$_SERVER['PHP_SELF']?>",
				action: "P"
			}
		});
		
		$("#searchTable").hide();
		$("#pagination").hide();
		$("#actionBtn").hide();
		$(".actionField").hide();
		print();
		$("#searchTable").show();
		$("#pagination").show();
		$("#actionBtn").show();
		$(".actionField").show();
	});
});
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
</script>
<style type="text/css">
#searchTable{
	border:1px solid #000066;
}
#displayTable, #displayTable td, #displayTable th{
	border:1px solid #88C9EE;
	border-collapse:collapse;
}
.headingBar{
	margin:5px;
	height:20px;
	line-height:20px;
	padding-left:5px;
	background-color:#6699CC;
	color:#FFFFFF;
	font-weight:bold;
}
input, select { padding:3px;}
</style>
</head>
<body>
<div class="headingBar">View Reports Logs</div>
<table border="0" cellspacing="3" width="500" align="center" id="searchTable">
    <form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="Search">
    <tr>
        <th align="left" class="headingBar">Search Filters</th>
    </tr>
    <tr>
    <td align="center">
          From Date 
                  	<input name="fromDate" type="text" id="from" value="<?=$fromDateReport?>" readonly >
                  	<!--input name="from" type="text" id="from" value="<?=$from1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('Search.fromDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" title="Select Date From|Please select date from calendar"><img src="images/show-calendar.gif" width=24 height=15 border=0> </a>&nbsp; &nbsp;To Date&nbsp;
		    		    <input name="toDate" type="text" id="to" value="<?=$toDateReport?>" readonly >
		    		    <!--input name="to" type="text" id="to" value="<?=$to1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('Search.toDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" title="Select Date To|Please select date from calendar"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
		    		    <!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->		    		    
               </td>
    </tr>
    <tr>
        <td align="center">UserType/User Name: <?=$strUserDropDownMenu;?></td>
        <script type="text/javascript">SelectOption(document.Search.userID, "<?=$_REQUEST["userID"]?>");</script>
    </tr>
    <tr>
        <td align="center"><input type="submit" name="Submit" value="Search"></td>
    </tr>
    </form>
</table>
<br />
<br />
<?php
if(count($contentsTrans) > 0)
{?>
    <table width="1000" id="pagination" align="center" style="margin:2px auto; border:1px solid #000066">
        <tr> 
            <td align="left"><?=("Showing <strong>".($offset+1)." - ".($offset+count($contentsTrans))."</strong> Of ".$allCount." ")?></td>
            <?php if ($prv >= 0) { ?>
            <td width="50" align="center"><a href="<?php echo $_SERVER['PHP_SELF']."?newOffset=0".$strQueryString;?>"><font color="#005b90">First</font></a></td>
            <td width="50" align="center"><a href="<?php echo $_SERVER['PHP_SELF']."?newOffset=$prv".$strQueryString;?>"><font color="#005b90">Previous</font></a></td>
            <?php } ?>
            <?php 
            if( ($nxt > 0) && ($nxt < $allCount) ) {
            $alloffset = (ceil($allCount / $limit) - 1) * $limit;
            ?>
            <td width="50" align="center"><a href="<?php echo $_SERVER['PHP_SELF']."?newOffset=$nxt".$strQueryString;?>"><font color="#005b90">Next</font></a></td>
            <td width="50" align="center"><a href="<?php echo $_SERVER['PHP_SELF']."?newOffset=$alloffset".$strQueryString;?>"><font color="#005b90">Last</font></a></td>
            <?php } ?>
        </tr>
	</table>
    <table width="1000" align="center" id="displayTable" cellpadding="5">
        <tr bgcolor="#A6C9E2"> 
            <th align="left" width="150">Login Time</th>
            <th align="left" width="150">User Name</th>
            <th align="left" width="100">User Type</th>
            <th align="left" width="100">IP Address</th>
            <th align="left" width="300">Link Name</th>
            <th align="left" width="50" class="actionField">Action</th>
            <th align="left" width="50">Count</th>
        </tr>
        <?php
		$totalCount	= 0;
        foreach($contentsTrans as $key=>$val)
        {
			?>
            <tr> 
                <td><?=date("d-m-Y H:i:s",strtotime($val["login_time"]));?></td>
                <td><?=$val["login_name"];?></td>
                <td><?=$val["login_type"];?></td>
                <td><?=$val["access_ip"];?></td>
                <td><?=$val["report_title"];?></td>
                <td align="center" class="actionField"><a href="#" onClick="javascript:window.open('reportActionLogs.php?id=<?=$val["id"]?>&count=<?=$val["count"];?>','view Action Details', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=220,width=400')" style="color:#0000CC">Details</a></td>
                <td align="center"><?=$val["count"];?></td>
            </tr>
            <?php
            $totalCount += $val["count"];
        }?>
        <tr bgcolor="#DFEFFC">
        	<td colspan="7" style="border-bottom:2px solid #88C9EE;"><strong>Total:</strong></td>
        </tr>
        <tr bgcolor="#C5DBEC">
            <td align="left">&nbsp;</td>
            <td align="left">&nbsp;</td>
            <td align="left">&nbsp;</td>
            <td align="left">&nbsp;</td>
            <td align="left">&nbsp;</td>
            <td align="left" class="actionField">&nbsp;</td>
            <td align="center"><strong><?=$totalCount;?></strong></td>
        </tr>
    </table>
    <br />
    <div align="center" id="actionBtn">
        <form id="exportForm" action="exportReportsLogs.php" method="post">
        	<input type="hidden" name="fromDate" value="<?=$fromDate?>" />
            <input type="hidden" name="toDate" value="<?=$toDate?>" />
            <input type="hidden" name="userID" value="<?=$userID?>" />
            <input type="hidden" name="pageUrl" value="<?=$_SERVER['PHP_SELF']?>" />
        	<input type="submit" name="Submit" id="exportReport" value="Export" />
            <input type="button" id="printReport" value="Print" />
        </form>
    </div>
<?php
}
else
{?>
    <div style="background-color:#FFFFCC; color:#FF0000" align="center">No data to view. Select Proper Filters from Above</div>
<?php
}?>
</body>
</html>