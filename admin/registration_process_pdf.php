<?php 
	/**
	 * @package sender Registration
	 * @Subpackage Sender Registration from Premier Exchange Website
	 * This page is used for Registering Sender From the Premier Exchange Web Site
	 */

	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	
	
	/**** This code is for ajax validation of email from database ****/
	/*if($_POST['email']){
		$chkEmail = $_POST['email'];
		$query = "SELECT email FROM ".TBL_CUSTOMER." WHERE email = '$chkEmail'";
		$records = selectMultiRecords($query);
		$intFlagEmail = count($records);
		if($_POST['chkEmailID'] == '1'){
			if($intFlagEmail > 0 )
				echo '<em style="color:#FF4B4B;">'.$chkEmail.' is already registered.</em>';
			else
				echo '<em style="color:#4B9D05">Available.</em>';
			exit;
		}
	}*/
	/**** This code is for registration process ****/
	/*if($_POST['register'] != 'Register'){
		echo '<em style="color:#FF4B4B;font-size:13px;">Sender can not be registered. Please try again.</em>';
		exit;
	}
	if($intFlagEmail > 0){
		echo '<em style="color:#FF4B4B;font-size:13px;">This Email address is already registered.</em>';
		exit;
	}*/
	
	/******** Data From Form ********/
	$mailto = $_POST['email'];
	if(!empty($mailto))
		$custEmail = $mailto;
	
	$ipAddress = $_POST['ipAddress'];
	$title = $_POST['title'];	
	$forename = $_POST['forename'];
	$surname = $_POST['surname'];
	$fullName = $forename.' '.$surname;
	$dob = $_POST['dob'];
	$gender = $_POST['gender'];
	$birCountry = $_POST['birthCountry'];
	$resCountry = $_POST['residenceCountry'];
	$postCode = $_POST['postcode'];
	$buildingNumber = $_POST['buildingNumber'];
	$buildingName = $_POST['buildingName'];
	$street = $_POST['street'];
	$city = $_POST['city'];
	$town = $_POST['town'];
	$province = $_POST['province'];
	$landline = $_POST['landline'];
	$mobile = $_POST['mobile'];
	
	$passportNumber1 = $_POST['passportNum1'];
	$passportNumber2 = $_POST['passportNum2'];
	$passportNumber3 = $_POST['passportNum3'];
	$passportNumber4 = $_POST['passportNum4'];
	$passportNumber5 = $_POST['passportNum5'];
	$passportNumber6 = $_POST['passportNum6'];
	
	$NIC = $_POST['nic'];
	$email = $_POST['email'];
	
	
	$address = $buildingNumber.", ".$buildingName.",".$street.", ".$town.", ".$province.", ".$resCountry;
	
	
	/** API Proccess **/
	
	
	
	require_once "../api/gbgroup/autheticatebyprofile.php";
	require_once "../api/gbgroup/get_itemchecksbyprofile.php";
	
	
	
	/***** Setting Values of variable from Database ****/

	$customerNumber = selectFrom("select MAX(customerNumber) from ".TBL_CUSTOMER." where 1 ".$custWhr);
	$customerNumber_value = $customerNumber[0]+1;
	//debug($customerNumber_value);
	if(defined("CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX"))
		$strCustomerPrefix = CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX;
	else
		$strCustomerPrefix = "C-";
	
	$accountName = $strCustomerPrefix.$customerNumber_value;
	//debug($accountName);
	
	/***************  START PDF ATTACHMENT DOCUMENT GENERATION ***************/

	include ('../pdfClass/class.ezpdf.php');
	$pdf =& new Cezpdf();
	$pdf->selectFont('../pdfClass/fonts/Helvetica.afm');
	
	
	// image can be created only if GD library is installed in server
	$image = imagecreatefromjpeg("images/premier/logo.jpg");
	$pdf->addImage($image,50,700,200);
	
	$pdf->ezSetMargins(0,0,80,80);
	for($i=0;$i<=8;$i++)
		$pdf->ezText('',12);

	$pdf->ezText('',12);
	$pdf->ezText('<b><u>Registration</u></b>',12,array("justification" => "center"));
	$pdf->ezText('',12);
	$pdf->ezText('Registration Details are given Below:',12,array("justification" => 'full'));	
	$pdf->ezText('',12);
	
	$pdf->ezText('',12);
	$pdf->ezText('',12);
	
	
	$data = array(
		array(
			'Title',
			"$title"
		),
		array(
			'Forename',
			"$forename"
		),
		array(
			'Surname',
			"$surname"
		),
		array(
			'Reference Number',
			"$accountName"
		),
		array(
			'Date of Birth',
			"$dob"
		),
		array(
			'Gender',
			"$gender"
		),
		array(
			'Country of Birth',
			"$birCountry"
		),
		array(
			'Country of Residence',
			"$recCountry"
		),
		array(
			'Postcode / Zipcode',
			"$postCode"
		),
		array(
			'Your Address',
			"$address"
		),
		array(
			'Telephone - Landline',
			"$landline"
		),
		array(
			'Telephone - Mobile',
			"$mobile"
		),
		array(
			'Passport',
			"$passport"
		),
		array(
			'National ID Card',
			"$NIC"
		),
		array(
			'Email',
			"$email"
		),
	);
	
	$pdf->ezTable($data,'' , '',array('showHeadings'=>0,'shaded'=>0,'showLines'=>2));
	
	for($i=0;$i<=1;$i++)
		$pdf->ezText('',12);
	
	
	for($i=0;$i<=4;$i++)
		$pdf->ezText('',12);
	
	

	$pdfcode = $pdf->output();
	//$pdf->ezStream();
	
	// write pdf 
	$fPDFname = tempnam('/tmp','PDF_Registration Details').'.pdf';
	$fp = fopen($fPDFname,'w');
	fwrite($fp,$pdfcode);
	
	$fpPDF = fopen($fPDFname, "rb");
	$filePDF = fread($fpPDF, 102460);
	fclose($fp);
	$filePDF = chunk_split(base64_encode($filePDF));

		
	$file_name = 'Registeration Data.pdf';
	$file_path = $fPDFname;

/***************  END PDF ATTACHMENT DOCUMENT GENERATION ***************/


/***************  Database operations ***************/

	
$query = "INSERT INTO customer (firstName, lastName, customerName, accountName,gender, email, Address, city, Zip, Phone, Mobile, dob, customerNumber, customerStatus, Country, 	placeOfBirth, passport_number, IDNumber) VALUES('$forename', '$surname', '$fullName', '$accountName',$gender, '$email', '$address', '$town', '$postCode', '$landline', '$mobile', '$dob', '$customerNumber_value', 'Disable', '$resCountry', '$birCountry', '$passport', '$NIC')";
mysql_query($query);
//$customerID = @mysql_insert_id();



/***************  START EMAIL BODY ***************/
$timeStamp = time();
$num = md5( time() );
$messageT = "\n<br />Dear Mirza Arslan Baig,\n<br /><br />Please find attached Registration info for Premier Exchange.  Please check all details are correct and if possible please e-mail/fax back a SIGNED\ncopy to us.\nThank you very much for dealing with Premier FX. <br /><br />Regards,<br /> ";



$messageT.="<table>
				<tr>
					<td align='left' width='30%'>
					<img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width='20%'>&nbsp;</td>
					<td align='left'><font size='2px'><font color='#F7A30B'>LONDON OFFICE</font>\n<br>11 th Floor City Tower 40 Basinghall St London EC2V 5DE \n<br><font color='#F7A30B'> PORTUGAL HEAD OFFICE</font> \n<br> Rua Sacadura Cabral Edificio Golfe 1A, 8135-144 Almancil \n<br> <font color='#F7A30B'>Tel:</font> UK +44 (0) 845 021 2370 | <font color='#F7A30B'>Int:</font> +351 289 358 511 \n<br> <font color='#F7A30B'>FAX:</font> +351 289 358 513 \n <br> <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font></td>
				</tr>
			</table>";
			
/***************  END EMAIL BODY ***************/



// Prepare Email for Sender and Account Manger

$subject = "Registration Confirmation";
$headers  = 'MIME-Version: 1.0' . "\r\n";
// This two steps to help avoid spam   
$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
$headers .= "X-Mailer: PHP v".phpversion()."\r\n";         

$fromName  = SYSTEM;
$fromEmail = "Reply-To: noreply@premierfx.com";


require_once("lib/phpmailer/class.phpmailer.php");
$sendermailer = new PHPMailer();

$sendermailer->FromName = $agentmailer->FromName = $fromName;
$sendermailer->From = $agentmailer->From = $fromEmail; 
$sendermailer->AddAddress($custEmail,'');
$sendermailer->IsHTML(true);

// attachements
if($fpPDF){
	$sendermailer->AddAttachment($file_path, $file_name,"base64");
}
else{
	$attachementInfo = '<br/>Deal Contract could not be attached with email';
}

// Subject
$sendermailer->Subject = $subject;

// Body
$sendermailer->Body = $messageT;


	if($custEmail){
		$custEmailFlag = $sendermailer->Send();//@mail($custEmail, $subject, $messageT, $headers); 
	}
	else{
		$msg = "Email is not sent to customer (may not exist or email functionality is disabled)";

		echo "<h5 style='color:##FF4B4B;font-style:italic;'>".$msg."</h5>";
	}	

if ($custEmailFlag ) {
	if($custEmailFlag){
		$msg = "Email has been sent to sender.";
	}
	$msg.=$attachementInfo."<br/>";
	echo "<h5 style='color:#4B9D05;font-style:italic;'>".$msg."</h5>";
}
/**** Informative Message ***/

?>