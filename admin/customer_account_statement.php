<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

$totalAmount="";

session_register("agrandTotal");
session_register("aCurrentTotal");

session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");

session_register("agrandTotal");
session_register("aCurrentTotal");

if($_POST["Submit"] =="Search")
{
	$_SESSION["agrandTotal"]="";
	$_SESSION["aCurrentTotal"]="";

	$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];
	
	$_SESSION["customer"] = "";
}


if($_POST["customer"]!="")
	$_SESSION["customer"] =$_POST["customer"];
		

if ($offset == "")
	$offset = 0;
$limit=40;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
?>
<html>
<head>
	<title>Customer Account</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="javascript/jquery.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript">
$(document).ready(function(){
	$("#printReport").click(function(){
		// Maintain report print logs 
		$.ajax({
			url: "maintainReportsLogs.php",
			type: "post",
			data:{
				maintainLogsAjax: "Yes",
				pageUrl: "<?=$_SERVER['PHP_SELF']?>",
				action: "P"
			}
		});
		
		$("#searchTable").hide();
		$("#pagination").hide();
		$("#actionBtn").hide();
		print();
		$("#searchTable").show();
		$("#pagination").show();
		$("#actionBtn").show();
	});
});
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
</script>
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">	
	<tr>
    <td class="topbar"><?=__("Sender");?> Account Balance </td>
  </tr>	
</table>
<table width="83%" border="0" cellspacing="1" cellpadding="5" align="center">
  <form action="customer_account_statement.php" method="post" name="Search">
  <tr>
  
  <td width="800">
  			<table width="400" align="center" border="0" id="searchTable">
          <tr>
					<td>
			
			From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        <script language="JavaScript">
         	SelectOption(document.forms[0].fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.forms[0].fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.forms[0].fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
        To	<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </script>
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

		 <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tDay, "<?=$_SESSION["tDay"]; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tYear, "<?=$_SESSION["tYear"]; ?>");
        </script><br>
              
            <? /* if($agentType=="admin" || $agentType == "Admin Manager"  || $agentType == "Branch Manager"){?>
              <select name="agentID" style="font-family:verdana; font-size: 11px">
                <option value="">- Select Agent-</option>
				<option value="all">All Agents</option>
                <?
                $agentQuery = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY'";
					
								if($agentType == "Branch Manager")
								{
									$agentQuery .= " and parentID = '$changedBy'";				
									
								}
								$agentQuery .= "order by agentCompany";
								$agents = selectMultiRecords($agentQuery);
					for ($i=0; $i < count($agents); $i++){
				?>
                <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
                <?
					}
				?>
             </select>
			 <script language="JavaScript">
         	SelectOption(document.forms[0].agentID, "<?=$_SESSION["abank"]; ?>");
                                </script>
        <? } */?>
        	</td>
		</tr>		
		<tr>
			<td align="center">
        		Payin Book or <?=__("Sender")?> Reference <input type="text" name="cusotmer">
        	</td>
		</tr>
		<tr>
			<td align="center">
      			<input type="submit" name="Submit" value="Search">
	  		</td>
		</tr>
			</table>					
		</td>
		</tr>
		<tr>
		<td>
		
      <?
	 if($_POST["Submit"] =="Search"||$_GET["search"] == "search")
	 {
	 	
		$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
		$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
		
	 	if($_POST["cusotmer"]!="")
		{
	 		/*$Balance="";
			$_SESSION["abank"] = $_POST["agentID"];
			if($agentType != "admin" && $agentType != "Admin Manager" )
			{
				$_SESSION["abank"] = $_SESSION["loggedUserData"]["userID"];
				}	
	 //		$slctID=$_POST["agentID"];
		}
		else 
				$_SESSION["abank"] = $_GET["agentID"];
			//$slctID=$_GET["agentID"];
		
	//	if($_SESSION["abank"]!="")
		{
			if($_SESSION["abank"]=="all"){
				
				$accountQuery="Select distinct agentID from agent_account  where 1 ";
				}
			elseif
			
			($_SESSION["abank"]!=""){*/
				
				$accountQuery = "Select customerID, firstName, Address from customer where accountName = '".$_POST["cusotmer"]."'";
				$accountQueryCnt = "Select count(customerID) from customer where accountName = '".$_POST["cusotmer"]."'";

			}
			else
			{
				$accountQuery= "Select c.customerID, firstName, Address from customer as c,agents_customer_account as a where 1 and c.customerID = a.customerID";
				$accountQueryCnt = "Select count(c.customerID) from customer as c,agents_customer_account as a where 1 and c.customerID = a.customerID";
				$queryDate = "(a.Date >= '$fromDate 00:00:00' and a.Date <= '$toDate 23:59:59')";
				
				$accountQuery .=  " and $queryDate";
		        $accountQueryCnt .=  " and $queryDate";

			}
					
					//$accountQuery="Select  agentID from agent_account where agentID = '".$_SESSION["loggedUserData"]["userID"]."' "; 
			
		/*
		 $accountQuery .=  " and $queryDate";
		 $accountQueryCnt .=  " and $queryDate";
		 */
		// $accountQuery .= " and status != 'Provisional' ";
			
	
			//$contentsAcc = selectMultiRecords($accountQuery);
			//$allCount = countRecords($contentsAcc);

			//$contentsAcc = selectMultiRecords($accountQuery);
			//$allCount = countRecords($contentsAcc);

			$allAmount=0;
				

	

				/*for($k=0;$k < $allCount;$k++)

				{
					$ids=$contentsAcc[$k]["customerID"];
					$allAmount+=calculateBalanceDateCustomer($contentsAcc[$k]["customerID"], $fromDate, $toDate);
				}*/
			//echo $queryBen;
		
		
		
		//echo	$accountQuery .= " LIMIT $offset , $limit";
		//echo $accountQuery;
	
			$contentsAcc = selectMultiRecords($accountQuery);
			$allCount = countRecords($accountQueryCnt); 
			
		//echo $allCount;
			
     /*	if($slctID!="all")
			$accountQuery="Select name, balance, agentStatus  from ".TBL_ADMIN_USERS." where userID = $slctID and parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' order by agentCompany";
		else 
			$accountQuery="Select name, balance, agentStatus  from ".TBL_ADMIN_USERS." where 1 and parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' order by agentCompany";

		$contentsAcc = selectMultiRecords($accountQuery);
		$allCount = count($contentsAcc);
		//echo $queryBen;
		$accountQuery .= " LIMIT $offset , $limit";
		$contentsAcc = selectMultiRecords($accountQuery);*/
	?>	
		
        <table width="715" height="284" border="1" align="center" cellpadding="0" bordercolor="#666666">
          <tr> 
            <td height="25" nowrap bgcolor="#6699CC"> 
			<table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF" id="pagination">
                <tr> 
                  <td width="256" align="left">
                    <?php if (count($contentsAcc) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&agentID=".$_SESSION["customer"]."&search=search&total=first";?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="77" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&agentID=".$_SESSION["customer"]."&search=search&total=pre";?>"><font color="#005b90">Previous</font></a> 
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="61" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&agentID=".$_SESSION["customer"]."&search=search&total=next";?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <!--td width="50" align="right"><a href="<?php //print $PHP_SELF . "?agentID=".$slctID."&search=search&total=last";?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td-->
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
      <tr>
            <td height="25" bgcolor="#C0C0C0" bordercolor="#000000"><span class="tab-u"><strong>&nbsp;Report 
              Date - <? if($_SESSION["fMonth"]!="" && $fromDate < $toDate){echo "From ".$_SESSION["fMonth"]."-".$_SESSION["fDay"]."-".$_SESSION["fYear"]." To ".$_SESSION["tMonth"]."-".$_SESSION["tDay"]."-".$_SESSION["tYear"];}?></strong></span></td>
        </tr>
        <tr>
            <td height="25" nowrap bgcolor="#C0C0C0" align="center"><span class="tab-u"><strong> <?=__("Sender")?> Account Current Balance </strong></span></td>
        </tr>                      
       
          <?
		if(count($contentsAcc) > 0)
		{
		?>
          <tr> 
            <td nowrap bgcolor="#EFEFEF"> 
				<table border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#C0C0C0"> 
                  <td>Cumulative Total</td>
                  <td><? 
                  	$allAmount = 0;
				 $cummulativeTotalQuery = "Select sum(amount) as TotalAmount, Type from agents_customer_account where 1 and (Date >= '$fromDate 00:00:00' and Date <= '$toDate 23:59:59') ";
				 
				 if($_POST["cusotmer"]!=""){
 					$cummulativeTotalQuery .= " and customerID = '".$contentsAcc[$i]["customerID"]."'";
 				}
 				
 				$cummulativeTotalQuery .= "  group by Type ";
 				
				  $cummulativeTotal = selectMultirecords($cummulativeTotalQuery);
				  for($i=0;$i < count($cummulativeTotal);$i++)		
						{
							if($cummulativeTotal[$i]["Type"] == "DEPOSIT")
							{
							  	$allAmount = $allAmount+$cummulativeTotal[$i]["TotalAmount"];
							}
							  	
							if($cummulativeTotal[$i]["Type"] == "WITHDRAW")
							{
							  $allAmount = $allAmount-$cummulativeTotal[$i]["TotalAmount"];
							}
						}	

								echo number_format($allAmount,2,'.',',');
				  ?></td>
                  <td>&nbsp; </td>
                </tr>
                <tr bgcolor="#FFFFFF"> 
                  <td width="166"><span class="style1"><?=__("Sender")?> Name</span></td>
                  <td width="175"><span class="style1">Balance Amount</span></td>
                  <td width="446"><span class="style1"><?=__("Sender")?> Address</span></td>
                </tr>
                <? for($i=0;$i < count($contentsAcc);$i++)
			{
			//$BeneficiryID = $contentsBen[$i]["benID"];agentStatus 
				?>
                <tr bgcolor="#FFFFFF"> 
                  <td><? 
				  //$q=selectFrom("SELECT * FROM ".TBL_CUSTOMER." WHERE customerID ='".$contentsAcc[$i]["customerID"]."'"); echo $q["firstName"];
				 echo $contentsAcc[$i]["firstName"];
				  ?> </td>
                  <td>
                    <? 
					$id=$contentsAcc[$i]["customerID"];
					$Bal=calculateBalanceDateCustomer($id, $fromDate, $toDate);
					echo number_format($Bal,2,'.',',');
					$Balance+=$Bal;
					 
			  
			  //		echo number_format($contentsAcc[$i]["balance"],2,'.',',');
					//$Balance = $Balance+$contentsAcc[$i]["balance"];
			  
			  	?>
                  </td>
                  <td><?  echo $contentsAcc[$i]["Address"]; ?> </td>
                </tr>
                <?
			}
			?>
                <tr bgcolor="#FFFFFF"> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr bgcolor="#FFFFFF"> 
                  <td><b>Current Balance</b></td>
                  <td><? echo number_format($Balance,2,'.',','); ?></td>
                  <td>&nbsp;</td>
                </tr>
                <tr bgcolor="#FFFFFF"> 
                  <td height="37" colspan="12"> </td>
                </tr>
              </table>
              <?
			} // greater than zero
			else
			{
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Date and Cusotmer </font>
				</td>
			</tr>
          <tr> 
            <td nowrap bgcolor="#EFEFEF"> 
			<table border="0" bordercolor="#EFEFEF">
                <tr bgcolor="#FFFFFF"> 
                   <td width="166"><span class="style1">Sender Name</span></td>
                  <td width="175"><span class="style1">Balance Amount</span></td>
                  <td width="446"><span class="style1">Sender Address</span></td>
                </tr>
                <tr bgcolor="#FFFFFF"> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
          </tr>
          <?
			}
			?>
          <tr> 
            <td class="style3" align="left" colspan="4"> Running Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
              <?
				if($_GET["total"]=="first")
				{
				$_SESSION["agrandTotal"] = $Balance;
				$_SESSION["aCurrentTotal"]=$Balance;
				}elseif($_GET["total"]=="pre"){
					$_SESSION["agrandTotal"] -= $_SESSION["aCurrentTotal"];			
					$_SESSION["aCurrentTotal"]=$Balance;
				}elseif($_GET["total"]=="next"){
					$_SESSION["agrandTotal"] += $Balance;
					$_SESSION["aCurrentTotal"]= $Balance;
				}else
					{
					$_SESSION["agrandTotal"] = $Balance;
					$_SESSION["aCurrentTotal"]=$_SESSION["agrandTotal"];
					}
				echo number_format($_SESSION["agrandTotal"],2,'.',',');
				?>
            </td>
          </tr>
		  <tr id="actionBtn">
		  	<td> 
				<div align="center">
					<input type="button" name="Submit2" value="Print This Report" id="printReport">
				</div>
			</td>
		  </tr>
        </table>
	   
	<?	
	 }?>	 
	 </td>
	</tr>
	</form>
</table>
</body>
</html>