<?php
	session_start();
	
	/** 
	 *  Short Description 
	 *  This page provides the interface to add trade
	 *  @package Trading 
	 *  @subpackage Add Trade
	 *  @copyright HBS Tech. (Pvt) Ltd.
	 */
	include ("../include/config.php");
	include ("security.php");
	//include_once "../premier_fx_online_module/includes/functions.php";
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	$payinAmount = CONFIG_PAYIN_FEE;
	$company = COMPANY_NAME;
	$date_time = date('Y-m-d  h:i:s');
	$created_date = date("Y-m-d");
	include "lib/audit-functions.php";
	 
			/**
			 * Add new record
			 */
	
	if(!empty($_REQUEST["from_date"]))
	{
		$dDate = explode("/",$_POST["from_date"]);
		$time = date('h:i:s');
		if(count($dDate) == 3)
		{
			$dDate = $dDate[2]."-".$dDate[1]."-".$dDate[0];
			$dateTime = $dDate." ".$time;
		}	
		//debug($dateTime,true);
	}
	
	//$query_TradeNo = "Select Max(sys_Trade_No) as max_val from ". TBL_ACCOUNT_DETAILS ." ";
	
	$query_TradeNo = "SELECT MAX(CAST(SUBSTRING(sys_Trade_No, 7, length(sys_Trade_No)-6) AS UNSIGNED)) as max_val FROM ". TBL_ACCOUNT_DETAILS ." ";
	
	//debug($query_TradeNo);
	$Sys_TradeNo = selectFrom($query_TradeNo);
	if (!empty($Sys_TradeNo['max_val'])){
	
		//print_r(explode('-',$Sys_TradeNo));
		/* $Sys_TradeNo1= 	explode('-',$Sys_TradeNo['max_val']);
		debug($Sys_TradeNo); 
		$number= $Sys_TradeNo1[1]+1;  */ 
		
		$number= $Sys_TradeNo['max_val']+1;
		$newNumber = 'Trade-'.$number;
		}
	
	else{ 
	$newNumber = 'Trade-1';
	}
	
	
	if(!empty($_REQUEST["saveInputs"]))
	{
			
		/**
		* If the Id is empty than the record is new one
		*/
		if(empty($_REQUEST["acid"]))
		{
			
			$strInsertSql = "INSERT INTO account_details 
											(
											  currencyBuy,
											  currencySell,
											  amountBuy,
											  amountSell,
											  accountSell,
											  accountBuy,
											  exchangeRate,
											  description,
											  transNumber,
											  created,
											  status,
											  sys_Trade_No,
											  created_by
											)
									 values
										   (
											 '".$_REQUEST["currencyBuy"]."',
											 '".$_REQUEST["currencySell"]."',
											 '".$_REQUEST["amountBuy"]."',
											 '".$_REQUEST["amountSell"]."',
											 '".$_REQUEST["accountDr"]."',
											 '".$_REQUEST["accountCr"]."',
											 '".$_REQUEST["rate"]."',
											 '".$_REQUEST["desc"]."',
											 '".$_REQUEST["tradeNo"]."',
											 '".$dateTime."',
											 'TN',
											 '".$newNumber."',
											 '".$userID."'
											)";
//debug($strInsertSql);
				if(mysql_query($strInsertSql))
				{
					$intLastId = mysql_insert_id();	
						if(!empty($intLastId))
							$msg = "The record is added successfully. System trade no is ". $newNumber;
						else
							$msg = "There is some error, Please try again.";
				}
//debug($strInsertSql);		
		}
		else
		{
			/**
			 * Else the record is not new and required the updation
			 */
			 
			 $arrOldData = selectFrom("select * from account_details where
							 	acid = '".$_REQUEST["acid"]."' ");
			$strUpdateSql = "update account_details 
							 set
							 	currencyBuy = '".$_REQUEST["currencyBuy"]."',
								currencySell = '".$_REQUEST["currencySell"]."',
								amountBuy = '".$_REQUEST["amountBuy"]."',
								amountSell = '".$_REQUEST["amountSell"]."',
								exchangeRate = '".$_REQUEST["rate"]."',
								description = '".$_REQUEST["desc"]."',
								transNumber = '".$_REQUEST["tradeNo"]."',
								status = 'TE',
								updated='".$dateTime."'
							 where
							 	acid = '".$_REQUEST["acid"]."'
						"; 
//debug($strUpdateSql);
				
			if(update($strUpdateSql)){
			
			 
		logChangeSet($arrOldData,$userID,"account_details","acid",$_REQUEST["acid"],$_SESSION["loginHistoryID"]);
			
			
			
			$strDataSql = "SELECT 
							   sys_Trade_No
						  FROM 
						  	 account_details 
						  WHERE 
						    acid = '".$_REQUEST["acid"]."'";
					$systemTradeNo = selectFrom($strDataSql);
					$system_TradeNo = $systemTradeNo["sys_Trade_No"];
//debug($systemTradeNo);
				$msg = "The record is updated successfully.
				System trade no is ". $system_TradeNo;
			}			
         }	
		 header( 'Location: update-trade.php' ) ;
	}	 
	/**
	 * If the record is for editing 
	 * For this check if the company_id field is empty or not, if not than request will be for editing the record
	 */
	$strMainCaption = "Add Trade"; 
	if(!empty($_REQUEST["acid"]))
	{
	
		$strGetDataSql = "SELECT 
							   currencyBuy,
							   currencySell,
							   amountBuy,
							   amountSell,
							   accountBuy,
							   accountSell,
							   exchangeRate,
							   description,
							   transNumber,
							   created,
							   sys_Trade_No
						  FROM 
						  	 account_details 
						  WHERE 
						    acid = '".$_REQUEST["acid"]."'";
//debug($strGetDataSql);
		$arrCompanyData = selectFrom($strGetDataSql);
		$currencyBuy = $arrCompanyData["currencyBuy"];
		$currencySell = $arrCompanyData["currencySell"];
		
		/**
		 *	Condition to set the decimal format of the amount 
		 *	to 2 and exchange rate to 4 if the below config is
		 *	enabled 
		 */
		if( defined("CONFIG_ROUND_NUMBER_TRADING") && CONFIG_ROUND_NUMBER_TRADING != "0" )
		{
			$amountBuy = number_format($arrCompanyData["amountBuy"],2);
			$amountSell = number_format($arrCompanyData["amountSell"],2);
			$rate = number_format($arrCompanyData["exchangeRate"],4);	
		}
		else
		{
			$amountBuy = $arrCompanyData["amountBuy"];
			$amountSell = $arrCompanyData["amountSell"];
			$rate = $arrCompanyData["exchangeRate"];
		}
		
		$desc = $arrCompanyData["description"];
		$tradeNo = $arrCompanyData["transNumber"];
		$accountBuy = $arrCompanyData["accountBuy"];
		$accountSell  = $arrCompanyData["accountSell"];
		$created  = date('d/m/Y',strtotime($arrCompanyData["created"]));
		$strMainCaption = "Modify Existing Trade"; 
	}
	elseif($_REQUEST["currencySell"]!="" && $_REQUEST["amountSell"]!=""){
		$currencySell = $_REQUEST["currencySell"];
		$amountSell = $_REQUEST["amountSell"];
	}
//debug($arrCompanyData);
//debug($tradeNo);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Add Trade</title>

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>

<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<script>
	$(document).ready(function() {
		
		$("#submitCompany").validate({
			rules: {
					currencyBuy: "required",
					amountBuy: "required",
					currencySell:  "required",
					amountSell: "required",
					//rate: "required",
					accountCr: "required",
					accountDr: "required"
			},
			messages: {
					currencyBuy: "<br />Please select currency Buy",
					amountBuy: "<br />Please enter amount buy",
					currencySell: "<br />Please select currency sell.",
					amountSell: "<br />Please enter amount sell.",
					//rate: "<br />Please enter rate.",
					accountCr: "<br />Please select credit account.",
					accountDr: "<br />Please select debit account."
			}
		});
	<?php 
		/**
		 *	Condition to set the decimal format of the amount 
		 *	to 2 and exchange rate to 4 if the below config is
		 *	enabled 
		 */
		if( defined("CONFIG_ROUND_NUMBER_TRADING") && CONFIG_ROUND_NUMBER_TRADING != "0" )
		{?>
			$("#amountBuy").blur(function(){
				var amountBuy = parseFloat($("#amountBuy").val());
				if(amountBuy != '')
				{
					$("#amountBuy").val(amountBuy.toFixed(2));
				}
			});
			$("#amountSell").blur(function(){
				var amountSell = parseFloat($("#amountSell").val());
				if(amountSell != '')
				{
					$("#amountSell").val(amountSell.toFixed(2));
				}
			});
			/*$("#rate").blur(function(){
			
				var rate = parseFloat($("#rate").val());
				if(rate != '')
				{
					$("#rate").val(rate.toFixed(4));
				}
			});*/
	<?php }?>
		$("#closeWindow").click(function(){
			close();
		});
		$("#from_date").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
		});
		
		/** $("#rate").blur(function(){
			var roundDecimal = 2;
			var amountBuy = parseFloat($("#amountBuy").val());
			var amountSell = parseFloat($("#amountSell").val());
			//alert(amountBuy);
			//alert(amountSell);
			var rate = $("#rate").val();
			var rs;
			
			if(rate != '') 
			{
				if(amountBuy)
				{
					rs = rate * amountBuy
					$("#amountSell").val(roundNumber(rs,roundDecimal));
				//$("#amountSell").val(rs);
				}
				else
				{
					rs = amountSell / rate;
					$("#amountBuy").val(roundNumber(rs,roundDecimal));
				}
			}
		}); */
	currencyBuyFn();
	currencySellFn();	
	$("#currencyBuy").change(function(){
				currencyBuyFn();
		});
	$("#currencySell").change(function(){
	      var currencyBuy = $('#currencyBuy').val();
		  var amountBuy = $('#amountBuy').val();
		  if(currencyBuy == '' || amountBuy == '0.00'){
		    //alert('Please ADD Currency By');
    		// $('#currencySell').val(''); return false;
		  }
	           rate_val= getrateBuy();


				currencySellFn();
		});	
		
		$("#currencySell").change(function(){
				currencySellFn();
		});
		$("#currencyBuy").change(function(){
	      var currencySell = $('#currencySell').val();
		  var amountSell = $('#amountSell').val();
		  if(currencySell == '' || amountSell == '0.00'){
		   // alert('Please ADD Currency Sell');
    		  //$('#currencyBuy').val(''); return false;
		  }
	           rate_val= getrateSell();
				
			
				currencyBuyFn();
		});	
		
	$("#accountCr").change(function(){
			var crAccount = $("#accountCr").val()
			if(crAccount != '')
				accountBuyFn();
		});
	$("#accountDr").change(function(){
			var drAccount = $("#accountDr").val()
			if(drAccount != '')
				accountSellFn();
	//	});
	});
	});
	function callfuncSell(val){
	  var currencySell = $('#currencySell').val();
	 var currencyBuy =  $("#currencyBuy").val();
	 if(currencySell !='' && currencyBuy !='' ){
	      
				var rate_val =  $("#rate").val();
			
			var amountSell = parseFloat(val)*parseFloat(rate_val);
			
			$('#amountSell').val(amountSell.toFixed(2));
	 
	   }
	
	}
	function getrateBuy(){
	var amountBuy = $('#amountBuy').val();
	$.ajax({
		   type: "POST",
		   url: "get_rate_ajex.php",
		   
		   data :{
		   action : "getCurrRate",
		   currencyB : $("#currencyBuy").val(),
		   currencyS : $("#currencySell").val()
		   },
		   beforeSend: function(XMLHttpRequest){
			//document.getElementById('validate').disabled = true;
			//$("#accountCr").html("&nbsp;");
		   },
		   success: function(msg){
		 
			if(msg !=''){
				//$("#rate").val(msg.toFixed(4));
				$("#rate").val(msg);
				var rate_val = msg;
			
			var amountSell = parseFloat(amountBuy)*parseFloat(rate_val);
			//alert(msg+'-----'+amountSell+'----'+amountBuy);
			$('#amountSell').val(amountSell.toFixed(2));
				}
			
		   }
		}); 
		
	}
	
	function callfuncBuy(val){
	  var currencySell = $('#currencySell').val();
	 var currencyBuy =  $("#currencyBuy").val();
	 if(currencySell !='' && currencyBuy !='' ){
	      
				var rate_val =  $("#rate").val();
			
			var amountBuy = parseFloat(val)*parseFloat(rate_val);
			
			$('#amountBuy').val(amountBuy.toFixed(2));
	 
	   }
	
	}
	

	function getrateSell(){
	var amountSell = $('#amountSell').val();
	$.ajax({
		   type: "POST",
		   url: "get_rate_ajex.php",
		   
		   data :{
		   action : "getCurrRate",
		   currencyB : $("#currencyBuy").val(),
		   currencyS : $("#currencySell").val()
		   },
		   beforeSend: function(XMLHttpRequest){
			//document.getElementById('validate').disabled = true;
			//$("#accountCr").html("&nbsp;");
		   },
		   success: function(msg){
		 
			if(msg !=''){
				//$("#rate").val(msg.toFixed(4));
				$("#rate").val(msg);
				var rate_val = msg;
			//rs = amountSell / rate;
			var amountBuy = parseFloat(amountSell)/parseFloat(rate_val);
			//alert(msg+'-----'+amountBuy+'----'+amountSell);
			$('#amountBuy').val(amountBuy.toFixed(2));
				}
			
		   }
		}); 
		
	}

	function roundNumber(rnum, rlength) { // Arguments: number to round, number of decimal places
	  var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
	  return newnumber; 
	}
	
	function currencyBuyFn()
	{
		$.ajax({
		   type: "GET",
		   url: "add-trade-conf.php",
		   data: "action=getAccountBuy&currencyB="+$("#currencyBuy").val()+"&accountB="+$("#accountCr").val(),
		   beforeSend: function(XMLHttpRequest){
			//document.getElementById('validate').disabled = true;
			//$("#accountCr").html("&nbsp;");
		   },
		   success: function(msg){
		 //  var dataMsg = msg.split("|");
			if(msg !='')
				$("#accountCr").html(msg);
			else
				$("#accountCr").html("<option>--select account--</option>");	
		   }
		}); 
	}	
	function currencySellFn()
	{
		$.ajax({
		   type: "GET",
		   url: "add-trade-conf.php",
		   data: "action=getAccountSell&currencyS="+$("#currencySell").val()+"&accountS="+$("#accountDr").val(),
		   beforeSend: function(XMLHttpRequest){
			//document.getElementById('validate').disabled = true;
			//$("#accountCr").html("&nbsp;");
		   },
		   success: function(msg){
		 //  var dataMsg = msg.split("|");
			if(msg !='')
				$("#accountDr").html(msg);
			else
				$("#accountDr").html("<option>--select account--</option>");	
		   }
		}); 
	}	
	function accountBuyFn()
	{
		$.ajax({
		   type: "GET",
		   url: "add-trade-conf.php",
		   data: "action=get&currencyB="+$("#currencyBuy").val()+"&currencyS="+$("#currencySell").val()+"&account="+$("#accountCr").val(),
		   beforeSend: function(XMLHttpRequest){
			//document.getElementById('validate').disabled = true;
			$("#crBalance").html("&nbsp;");
		   },
		   success: function(msg){
		 //  var dataMsg = msg.split("|");
			$("#crBalance").html(msg);
		   }
		}); 
	}	
	function accountSellFn()
	{
		$.ajax({
		   type: "GET",
		   url: "add-trade-conf.php",
		   data: "action=get&currencyB="+$("#currencyBuy").val()+"&currencyS="+$("#currencySell").val()+"&account="+$("#accountDr").val(),
		   beforeSend: function(XMLHttpRequest){
			//document.getElementById('validate').disabled = true;
			$("#drBalance").html("&nbsp;");
		   },
		   success: function(msg){
		 //  var dataMsg = msg.split("|");
			$("#drBalance").html(msg);
		   }
		}); 
	}	
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}

   
 // function will clear input elements on each form  
    function clearFormCustomize(){  
    	  // declare element type  
      var type = null;  
      // loop through forms on HTML page  
      for (var x=0; x<document.forms.length; x++){  
        // loop through each element on form  
        for (var y=0; y<document.forms[x].elements.length; y++){  
          // define element type  
         type = document.forms[x].elements[y].type  
         // alert before erasing form element  
         //alert('form='+x+' element='+y+' type='+type);  
         // switch on element type  
         switch(type){  
           case "text":  
           case "textarea":  
           case "password":  
           //case "hidden":  
             document.forms[x].elements[y].value = "";  
             break;  
           case "radio":  
           case "checkbox":  
             document.forms[x].elements[y].checked = "";  
             break;  
           case "select-one":  
             document.forms[x].elements[y].options[0].selected = true;  
             break;  
           case "select-multiple":  
             for (z=0; z<document.forms[x].elements[y].options.length; z++){  
               document.forms[x].elements[y].options[z].selected = false;  
             }  
           break; 
         }  
       }  
     }  
   }  	
</script>
<style>
td{ font-family:Verdana, Arial, Helvetica, sans-serif;
size:8px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.remarks
{
	font-size:12px;
	text-align:center;
	font-weight:bold;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.goback
{
	font-size:10px;
	text-align:left;
	font-weight:bold;
}
</style>
</head>
<body>
<form name="submitCompany" id="submitCompany" action="<?=$_SERVER["PHP_SELF"]?>" method="post">
	<table width="50%" border="0" cellspacing="1" cellpadding="2" align="center">
		<tr>
			<? if(!empty($_REQUEST["acid"])){?>
			<td bgcolor="#DFE6EA" class="goback">
				<a href="update-trade.php">Go Back</td>
				<td  align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong></td>
			<? }else{ ?>
			<td  colspan="2" align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong></td>
			<? } ?>	
		</tr>
		<? if(!empty($msg)) { ?>
			<tr bgcolor="#000000">
				<td colspan="2" align="center" style="text-align:center; color:#EEEEEE; font-size:10px;">
					<img src="images/info.gif" />&nbsp;<?=$msg?>				</td>
			</tr>
		<? } ?>
		
		<tr bgcolor="#ededed">
			<td colspan="2" align="center" class="tdDefination" style="text-align:center">
				All fields mark with <font color="red">*</font> are compulsory Fields.			</td>
		</tr>
		<tr bgcolor="#ededed">
		<!--<font color="red">*</font> -->
			<td width="25%" class="tdDefination"><b>Date:&nbsp;</b></td>
			<td align="left"><input type="text" name="from_date" id="from_date" value="<?=$created?>" readonly="" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Currency Buy:&nbsp;<font color="red">*</font></b></td>
			<td align="left">
			<?
			//$strQuery = "SELECT DISTINCT(currencyName), description  FROM  currencies ORDER BY currencyName ";
			$sql = "SELECT DISTINCT(currency) FROM accounts WHERE status = 'AC' ORDER BY currency";
			//debug($sql);
			$currencyRs = mysql_query($sql) or die('invalid query'.mysql_error());
			
			while($row = mysql_fetch_array($currencyRs))
			{
				$currency[] = $row["currency"];
			}
			?>
				<select name="currencyBuy" id="currencyBuy">
				<option value="">--select currency--</option>
				 <?
					foreach($currency as $key){
			   			$buySelected = '';
						if($currencyBuy == $key)
							$buySelected = 'selected="selected"';
				        echo "<option value='$key' $buySelected>".$key."</option>";
					}	
		        ?>
				</select>
				&nbsp; <input type="text" name="amountBuy" id="amountBuy" value="<?=number_format($amountBuy,2)?>" onblur="callfuncSell(this.value);">			</td>
		</tr>
		<tr bgcolor="#ededed">
		  <td class="tdDefination"><b>Rate:&nbsp;<font color="red">*</font></b></td>
		  <td align="left"><input type="text" name="rate" id="rate" value="<?=number_format($rate,2)?>"/></td>
		  <!--<td align="left"><input type="text" name="rate" id="rate" value="<?//=$_REQUEST["rate"]?>"/></td> -->
	  </tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Currency Sell:&nbsp;<font color="red">*</font></b></td>
			<td align="left">
				<select name="currencySell" id="currencySell">
					<option value="">--select currency--</option>
				 <?
					foreach($currency as $key){
						$sellSelected = '';
				       if($currencySell == $key)
							$sellSelected = 'selected="selected"';
					    echo "<option value='$key' $sellSelected>".$key."</option>";
					}	
		        ?>
				</select>
				&nbsp; <input type="text" name="amountSell" id="amountSell" value="<?=number_format($amountSell,2)?>" onblur="callfuncBuy(this.value);">			</td>
		</tr>
		<tr bgcolor="#ededed">
		  <td colspan="2" class="tdDefination">&nbsp;</td>
	  </tr>
		<tr bgcolor="#ededed">
		  <td class="tdDefination"><b>Credit Account:&nbsp;<font color="red">*</font></b></td>
		  <td align="left" id="accountCrRow">
		  	<?
			$sqlAccount = "SELECT id,accounNumber FROM accounts WHERE status = 'AC'";
			$accountCrSql = mysql_query($sqlAccount) or die('invalid query'.mysql_error());
			?>
		  <select name="accountCr" id="accountCr">
					<option value="">--select Account--</option> 
				 <?
		   			/*while($row = mysql_fetch_array($accountCrSql,MYSQL_ASSOC))
					{
					$crSelected = '';
					$id = $row["id"];
					$accounNumber = $row["accounNumber"];
					 if($accountBuy == $accounNumber)
						 	$crSelected = 'selected="selected"';
				        echo "<option value='$accounNumber' $crSelected >".$accounNumber."</option>";
					}	*/
		        ?>
			</select>
			<span id="crBalance"><strong>&nbsp;</strong></span>
			 </td>
	 
	   </tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Debit Account:&nbsp;<font color="red">*</font></b></td>
			<td align="left">
		  <select name="accountDr" id="accountDr">
					<option value="">--select Account--</option>
			<?
			/*$accountDrSql = mysql_query($sqlAccount) or die('invalid query'.mysql_error());
			
				while($row2 = mysql_fetch_array($accountDrSql))
				{
					$drSelected = '';
					$id = $row2["id"];
					$accounNumberDr = $row2["accounNumber"];
					 if($accountSell == $accounNumberDr)
						 	$drSelected = 'selected="selected"';
					echo "<option value='$accounNumberDr' $drSelected>".$accounNumberDr."</option>";
				}*/

			?>
			  </select>
			  	<span id="drBalance"><strong>&nbsp;</strong></span>
			  </td>
		</tr>
		<tr bgcolor="#ededed">
          <td class="tdDefination"><b>Description :&nbsp;</b></td>
		  <td align="left">
		  	<input type="text" name="desc" id="desc" size="40" maxlength="100" value="<?=$desc?>" />		  </td>
	  </tr>
		<tr bgcolor="#ededed">
		  <td class="tdDefination"><b>Trade Number :&nbsp; </b></td>
		  <td align="left"><input type="text" name="tradeNo" id="tradeNo" size="40" maxlength="100" value="<?=$tradeNo?>" /></td>
	  </tr>
		<tr bgcolor="#ededed">
		  <td class="tdDefination">&nbsp;</td>
		  <td align="left">&nbsp;</td>
	  </tr>
		
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" name="storeData" value="Submit" />
				&nbsp;&nbsp;
				<input type="reset" value="Clear Fields" />
				<input type="hidden" name="acid" value="<?=$_REQUEST["acid"]?>" />
				<input type="hidden" name="from" value="<?=$_REQUEST["from"]?>" />
				<input type="hidden" name="agentID" value="<?=$_REQUEST["agentID"]?>" />
				<input type="hidden" name="transID" value="<?=$_REQUEST["transID"]?>" />
				<!--<input type="hidden" name="tradeNo" value="<?//=$_REQUEST["tradeNo"]?>" />-->
				<input type="hidden" name="saveInputs" value="Y" />
				&nbsp;&nbsp;
				<input name="btnPrint" type="button" id="btnPrint" value="Print" onclick="print();" />			</td>
		</tr>
	</table>
</form>
</body>
</html>