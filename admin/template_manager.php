<?php 
	/**
	 * @package : Online Module
	 * @subpackage: Template Manager
	 * @author: Mirza Arslan Baig
	 */
	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	
	$userID  = $_SESSION["loggedUserData"]["userID"];
//$agentType = getAgentType();
 $where_sql="";
if($_SESSION["loggedUserData"]['adminType']=='Agent')
{

 $where_sql="   AND agentID='".	$userID."' ";
}


	if(isset($_POST['search'])){
		$strEvent = $_POST['event'];
		$strStatus = $_POST['status'];
		$strQuerySearch = "SELECT id, refNumber, SUBSTRING(body, 1, 40) AS bodylim,body, status, eventID,agentID FROM ".TBL_EMAIL_TEMPLATES." WHERE ";
		$strCondition = "eventID = '$strEvent' ";
		if($strStatus != 'all' && !empty($strStatus)){
			$strCondition .= "AND status = '$strStatus' ";
		}
		$strQuerySearch .= $strCondition;
		
		
		$strQuerySearch .=  $where_sql ;
		
		print_r($strQuerySearch);
		//debug($strQuerySearch);
		$arrTemplates = selectMultiRecords($strQuerySearch);
		//debug($arrTemplates);
		$intTotalTemplates = count($arrTemplates);
	}
	
	if(isset($_GET['templateID']) && !empty($_GET['templateID']) && isset($_GET['action'])){
		if($_GET['action'] == 'delete'){
			$intTemplateID = trim($_GET['templateID']);
			$strAction = trim($_GET['action']);
			$strQueryDeleteTemplate = "DELETE FROM ".TBL_EMAIL_TEMPLATES." WHERE id = '$intTemplateID' LIMIT 1";
			//debug($strQueryDeleteTemplate);
			if(deleteFrom($strQueryDeleteTemplate))
				$strMsg = "Template is deleted successfully!!";
		}
		
		if($_GET['action'] == 'chStatus'){
			$strNewStatus = trim($_GET['newStatus']);
			$intTemplateID = trim($_GET['templateID']);
			$intEventID = trim($_GET['event']);
			if($strNewStatus == 'Enable' || $strNewStatus == 'Disable'){
				if($strNewStatus == 'Enable'){
					$strQueryChkEnable = "SELECT count(id) AS records FROM ".TBL_EMAIL_TEMPLATES." WHERE eventID = '$intEventID' AND status = 'Enable'";
					//debug($strQueryChkEnable);
					$arrCountEnable = selectFrom($strQueryChkEnable);
					if($arrCountEnable['records'] <= 0){
						$strQueryChangeStatus = "UPDATE ".TBL_EMAIL_TEMPLATES." SET `status` = '$strNewStatus' WHERE id = '$intTemplateID'";
						update($strQueryChangeStatus);
					}else{
						$strError = "A template is Already enable for this event!!";
					}
				}else{
					$strQueryChangeStatus = "UPDATE ".TBL_EMAIL_TEMPLATES." SET `status` = '$strNewStatus' WHERE id = '$intTemplateID'";
					update($strQueryChangeStatus);
				}
			}
		}
		
		if(isset($_GET['search'])){
		$strEvent = $_GET['event'];
		$strStatus = $_GET['status'];
		$strQuerySearch = "SELECT id, refNumber, SUBSTRING(body, 1, 40) AS bodylim,body, status, eventID FROM ".TBL_EMAIL_TEMPLATES." WHERE ";
		$strCondition = "eventID = '$strEvent' ";
		if($strStatus != 'all' && !empty($strStatus)){
			$strCondition .= "AND status = '$strStatus'";
		}
		$strQuerySearch .= $strCondition;
		//debug($strQuerySearch);
		$arrTemplates = selectMultiRecords($strQuerySearch);
		//debug($arrTemplates);
		$intTotalTemplates = count($arrTemplates);
	}
		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/email_template.css"/>
	<script src="javascript/jquery-1.7.2.min.js" type="text/javascript"></script>
	<title>Template Manager</title>
	</head>

	<body>
		<div class="main">
			<h3>Template Manager</h3>
			<br />
			<form id="filters" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
				<table class="table cust_width">
					<tr>
						<td colspan="2">
							<u>Please choose the event you want to view the templates for</u>
						</td>
					</tr>
					
					<tr>
						<th>Agent For</th>
				 
<td colspan="7">
<?php $agentQuery  = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' AND agentStatus = 'Active'   $where_sql  ";


 
 
 ?>
 <?php  if($_SESSION["loggedUserData"]['adminType']!='Agent')
{
?>				<select  name="agentName" id="agentName" >
					<option value="">Select One</option>
 	          		<?php 
					
							$agentQuery .= "order by agentCompany";
								 $agents = selectMultiRecords($agentQuery);
					for ($i=0; $i < count($agents); $i++){
 	    		        		?>
 	            				<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
 	            				<?
 	                			}
								?>
 	          			</select>
						
						<?php } else {?>
						<input  type="hidden" name="agentName" id="agentName" value="<?=$userID?>"><?=$_SESSION["loggedUserData"]['name']?>
						<?php }?>

</td>					
					</tr>
					
					<tr>
						<td>Events<font color="#FF0000">* </font>:</td>
						<td>
							<select name="event" class="input-large">
								<option value="">- Select an event -</option>
								<?php 
									$strQueryEvents = "SELECT Distinct(name) AS name, id  FROM ".TBL_EVENTS;
									$arrEvents = selectMultiRecords($strQueryEvents);
									$intTotalEvents = count($arrEvents);
									if($intTotalEvents > 0){
										for($x = 0;$x < $intTotalEvents; $x++){
											echo "<option value='".$arrEvents[$x]['id']."'>".$arrEvents[$x]['name']."</option>";
										}
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Status:</td>
						<td>
							<select name="status" class="input-small">
								<option value="all" >ALL</option>
								<option value="Enable">Enable</option>
								<option value="Disable">Disable</option>
							</select>
						</td>
					</tr>
					<tr>
						<td  colspan="2" class="align_center">
							<input name="search" id="search" type="submit" class="btn btn-primary input-small" value="Search"/>
						</td>
					</tr>
					<?php 
						if(isset($strMsg)){
					?>
					<tr class="success">
						<td class="success align_center" colspan="2"><?php echo $strMsg; ?></td>
					</tr>
					<?php 
						}
						if(isset($strError)){
					?>
					<tr class="error">
						<td class="error align_center" colspan="2"><?php echo $strError; ?></td>
					</tr>
					<?php 
						}
					?>
				</table>
			</form>
			<?php 
				if($intTotalTemplates > 0){
			?>
			<table class="table table-striped table-bordered">
				<tr>
					<th>
					Template Reference Number
					</th>
					<th>
					Preview
					</th>
					<th>
					Current Status
					</th>
					<th colspan="3" class="align_center">
					Action
					</th>
				</tr>
				<?php 
					for($x = 0;$x < $intTotalTemplates;$x++){
				?>
				<tr>
					<td><?php echo $arrTemplates[$x]['refNumber']; ?></td>
					<td>
                    <?php $fullBody = selectFrom("select body,refNumber from ".TBL_EMAIL_TEMPLATES." where refNumber = '".$arrTemplates[$x]['refNumber']."' and eventID = '$strEvent' ");?>
                    <div class="tooltipcontainer">
                    		
                            <div class="tooltip">
								<? echo $fullBody["body"]; ?></div>        
                     		</div>
					<a href="#"><?php echo $arrTemplates[$x]['bodylim']."..."; ?>
                        
                    </a>
                    
					
					</td>
					<td><?php echo $arrTemplates[$x]['status']; ?></td>
					<?php 
						if($arrTemplates[$x]['status'] == 'Enable'){ 
					?>
					<td><a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=chStatus&templateID=<?php echo $arrTemplates[$x]['id']; ?>&newStatus=Disable&event=<?php echo $arrTemplates[$x]['eventID']; ?>&oldStatus=<?php echo $_POST['status']; ?>&search=search">Disable</a></td>
					<?php 
						}else{
					?>
					<td><a href="<?php echo $_SERVER['PHP_SELF']; ?>?event=<?php echo $arrTemplates[$x]['eventID']; ?>&action=chStatus&templateID=<?php echo $arrTemplates[$x]['id']; ?>&newStatus=Enable&oldStatus=<?php echo $strStatus; ?>&search=search">Enable</a></td>
					<?php 
						}
					?>
					<td><a href="email_template_composer.php?templateID=<?php echo $arrTemplates[$x]['id']; ?>&action=edit" target="_blank">Edit</a></td>
					<!-- <td><a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=delete&templateID=<?php echo $arrTemplates[$x]['id']; ?>&event=<?php echo $arrTemplates[$x]['eventID']; ?>&oldStatus=<?php echo $strStatus; ?>&search=search" onClick="return confirm('Are you sure to delete this template?');">Delete</a></td> -->
				</tr>
				<?php 
					}
				?>
			</table>
			<?php 
				}else{
			?>
			<table class="table">
				<tr>
					<th class="align_center">There are no templates to display.</th>
				</tr>
			</table>
			<?php 
				}
			?>
		</div>
		<script type="text/javascript">
			function SelectOption(OptionListName, ListVal){
				for (i=0; i < OptionListName.length; i++)
				{
					if (OptionListName.options[i].value == ListVal)
					{
						OptionListName.selectedIndex = i;
						break;
					}
				}
			}
			<?php 
				if(isset($_POST['event'])){
			?>
				SelectOption(document.forms[0].event, "<?php echo $_POST['event']; ?>");
			<?php 
				}elseif(isset($_GET['event'])){
			?>
				SelectOption(document.forms[0].event, "<?php echo $_GET['event']; ?>" );
			<?php
				}
				
				if(isset($_POST['status'])){
			?>
				SelectOption(document.forms[0].status, "<?php echo $_POST['status']; ?>");
			<?php 
				}
			?>
		</script>
	</body>
</html>
