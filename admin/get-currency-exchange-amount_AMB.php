<?
// Session Handling
session_start();
// Including files
include ("../include/config.php");
include ("security.php");
include("connectOtherDataBase.php");
include("ledgersUpdation.php");

$agentType = getAgentType();
$cid = $_REQUEST['cur_id'];
if(!empty($cid)){
$res= selectFrom("select amount,currency from currency_stock where currency in (select currencyName from currencies where cID=$cid)");
/*$res1 = selectFrom("select * from currencies where currencyName='GBP'");
debug($res1);
$res2= selectFrom("select * from currency_stock ");
debug($res2);*/
//debug($res);
	$strAccountChart = "SELECT 
						   id,
						   accountName,
						   accountNumber,
						   description,
						   currency 
					  FROM 
						 accounts_chart
					  WHERE 
						status = 'AC'
					  ";
				 //debug($strAccountChart);
				$accountChartRs = selectMultiRecords($strAccountChart);
	//debug($accountChartRs);			
	$drSum = 0;
	$crSum = 0;
	$type= '';
	$sum = 0;
	for($i=0; $i < count($accountChartRs); $i++){
	if($accountChartRs[$i]["accountNumber"] == 11223344){
	 $strAccount = "SELECT 
					   id,
					   accounNumber,
					   accountName,
					   currency,
					   balanceType,
					   balance 
				  FROM 
					 accounts
				  WHERE 
					status = 'AC' AND
					accounType = '".$accountChartRs[$i]["accountNumber"]."' AND
					currency = 'GBP'
				  ";
	 //debug($strAccount);
	 $accountRs = selectMultiRecords($strAccount);
	 //debug($accountRs);	
		if(count($accountRs) > 0 ) { 
		
			for($k=0; $k < count($accountRs); $k++){ 
				$drBalance = 0;
				$crBalance = 0;
				$date_from ='';
				$date_to ='';
				$strClosingBalance = "
								SELECT 
								id,
								closing_balance,
								dated,
								accountNumber 
								FROM 
								".TBL_ACCOUNT_SUMMARY." 
								WHERE
								accountNumber = '".$accountRs[$k]["accounNumber"]."' AND
								dated = (select MAX(dated) as dated FROM ".TBL_ACCOUNT_SUMMARY." 
								WHERE accountNumber = '".$accountRs[$k]["accounNumber"]."')
								";
				$closingBalance = selectFrom($strClosingBalance);
				//debug($closingBalance);
				if($accountRs[$k]['balanceType'] == 'Dr')
					$drBalance = abs($closingBalance["closing_balance"] - $accountRs[$k]['balance']);
				elseif($accountRs[$k]['balanceType'] == 'Cr')
					$crBalance = abs($closingBalance["closing_balance"] + $accountRs[$k]['balance']);
				//debug($drBalance." -- ".$crBalance);
				$drSum += $drBalance;
				$crSum += $crBalance;
				$type =$accountRs[$k]['balanceType'];
				} 
			}
			//debug($type);
		if($type == 'Dr')
			$sum= $drSum;
		elseif($type == 'Cr')
			$sum= $crSum;		
		}	
		
	}
	$strAjaxReturn=number_format($res[0],2,'.',NULL)."|".$res[1]."|".number_format($sum,2,'.',NULL);	
if(defined("CONFIG_DECIMAL_POINT_CURRENCY_EX_STOCK") && CONFIG_DECIMAL_POINT_CURRENCY_EX_STOCK=='1')
	$strAjaxReturn=customNumberFormat($res[0],2,true)."|".$res[1]."|".number_format($sum,2,'.',NULL);
	
echo $strAjaxReturn;
}
exit;


?>