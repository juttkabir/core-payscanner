<?php
// Session Handling
session_start();
///////////////////////History is maintained via method named 'activities'

// Including files
include ("../include/config.php");
include ("security.php");
include("connectOtherDataBase.php");
include ("../compliance/server.php");
$agentType2 = getAgentType();
$parentID2 = $_SESSION["loggedUserData"]["userID"];
$commaSeperatedUserIDs = "";

define("PIC_SIZE","51200");
/*if ($logo_name != "")
	{
		$Ext = strrchr($logo_name,".");
		if (is_uploaded_file($logo))
		{
			$Pict = strtolower($agentCode.$Ext);
			move_uploaded_file($logo, "../logos/" . strtolower($agentCode . $Ext) );
			update("update " . TBL_ADMIN_USERS. " set logo='$Pict' where username='$agentCode'");
		}
	}
*/

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$logo = $_FILES["logo"]["tmp_name"];
$logo_name = $_FILES["logo"]["name"];

$validationFlag = true;
if(CONFIG_UPDATE_AGENT_INTRODUCER_FIELDS == '1'){
	if($_REQUEST[ida]!='Y' && $agentType2 == 'SUPA')
		$validationFlag = false;
}

/**
 * #6747:Premier Exchange
 * Agent User type is displayed on agent form.
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
$agentUserTypeFlag = false;
if(CONFIG_AGENT_USER_TYPE == "1"){
	$agentUserTypeFlag = true;
}
$agentIDNumberFlag = false;
if(defined("CONFIG_SUP_AGENT_DIST_ID_NUMBER") && CONFIG_SUP_AGENT_DIST_ID_NUMBER=="1"){
	$agentIDNumberFlag = true;
}
if ($_GET["userID"]!="")
{
	$userID = $_GET["userID"];
}

if($_GET["searchBy"] != "")
{
	$searchBy = $_GET["searchBy"];
}

if ($_GET["Country"] != ""){
	$Country = $_GET["Country"];
}
if ($_GET["agentStatus"] != ""){
	$agentStatus = $_GET["agentStatus"];
}
if ($_GET["agentCode"] != ""){
	$agentCode = $_GET["agentCode"];
}
if ($_GET["type"] != ""){
	$typeV = $_GET["type"];
}
if ($_GET["ida"] != ""){
	$ida = $_GET["ida"];
}
$pageID = $_POST["pageID"];
$alertID = $_POST["alertID"];

$pageID = $_POST["pageID"];
$alertID = $_POST["alertID"];
if ($_REQUEST["mode"] != ""){
	$mode = $_REQUEST["mode"];
}
if ($_FILES["file"]["error"] > 0)
{
	echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
}
else
{/*
echo "Upload: " . $_FILES["file"]["name"] . "<br />";
echo "Type: " . $_FILES["file"]["type"] . "<br />";
echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />";*/
	if( isset($_POST["agentAddress"])&&isset($_POST["manualAgentNum"])&&isset($_POST["agentCompany"])&&isset($_POST["agentContactPerson"])&&isset($_POST["Country"])&&isset($_POST["agentPhone"]))
	{
		move_uploaded_file($_FILES["file"]["tmp_name"],"../files/customer/" . $_FILES["file"]["name"]);
		//update("update document_upload set path='$_FILES["file"]["name"]' where userId='$agentCode'");
		// echo "Stored in: " . "upload/" . $_FILES["file"]["name"];

	}
}
switch(trim($_POST["Country"])){
	case "United States": $ccode = "US"; break;
	case "Canada": $ccode = "CA"; break;
	default:
		$countryCode = selectFrom("select countryCode from ".TBL_COUNTRY." where countryName='".trim($_POST["Country"])."'");
		$ccode = $countryCode["countryCode"];
		break;
}
$_SESSION["IDAcountry"] = (is_array($_POST["IDAcountry"]) ? implode(",", $_POST["IDAcountry"]) :"");
$_SESSION["authorizedFor"] = (is_array($_POST["authorizedFor"]) ? implode(",", $_POST["authorizedFor"]) :"");

$backDays = "";

if ($_POST["rights"] == 'Backdated') {
	if ($agentType == 'admin') {
		if ($_POST['backDays'] == "") {
			$backDays	= '1';
		} else {
			$backDays = $_POST['backDays'];
		}
	}
}

/*
if($_POST["supAgentID"] != '' )
	$supAgentID = $_SESSION["supAgentID"];
else*/
$supAgentID = $_POST["supAgentID"];

$_SESSION["parentID"]="";

$_SESSION["parentID"]= $_POST["supAgentID"];

$_SESSION["isOnline"] 			= $_POST["isOnline"];
$_SESSION["defaultDistrib"] = $_POST["defaultDistrib"];
/**
 * @Ticket# 3506
 */
$_SESSION["replaceDefaultDistributor"] = $_POST["replaceDefaultDistributor"];
/* End #3506*/
$_SESSION["remoteAvailable"] = $_POST["remoteAvailable"];
$_SESSION["hasClave"] = $_POST["hasClave"];
$_SESSION["rateCountries"] = (is_array($_POST["rateCountries"]) ? implode(",", $_POST["rateCountries"]) :"");
$_SESSION["unlimitedRate"] = $_POST["unlimitedRate"];
$_SESSION["agentUserType"] = $_POST["agentUserType"];
$notes = $_SESSION["notes"] = $_POST["notes"];


$agentParentID = $_POST["agentParentID"];

if ($_SESSION["parentID"]==""){

	if($agentType2 == 'Branch Manager' || $agentType2 == 'admin' || $agentType2 == 'Admin Manager' )
	{

		$_SESSION["parentID"] = $parentID2;

	}elseif($_SESSION["parentID"] == "")
	{

		$_SESSION["parentID"] = $agentParentID;

	}
}








if(CONFIG_PAYIN_AGENT == '1' && $ida!= 'Y')
{

	if (trim($_POST["ch_payinBook"]) != "" && trim($_POST["payinBook"]) == ""){


		insertError(AG46);
		redirect($backURL);

	}


	if (trim($_POST["payinBook"]) != ""){

		if (trim($_POST["ch_payinBook"]) == ""){
			insertError(AG47);
			redirect($backURL);
		}

		if (!is_numeric($_POST["payinBook"])){
			insertError(AG48);
			redirect($backURL);
		}

		$payinBookNumber = strlen(trim($_POST["payinBook"]));

		if ($payinBookNumber < CONFIG_AGENT_PAYIN_NUMBER ){
			insertError(AG49." ".CONFIG_AGENT_PAYIN_NUMBER);
			redirect($backURL);
		}

		$payinBookNumber = strlen(trim($_POST["payinBook"]));
		if ($payinBookNumber > CONFIG_AGENT_PAYIN_NUMBER ){
			insertError(AG50." ".CONFIG_AGENT_PAYIN_NUMBER);
			redirect($backURL);
		}

		if ($_POST["payinBook"] != ""){
			if (isExist("select payinBook from ".TBL_CUSTOMER." where payinBook = '".$_POST["payinBook"]."' ")){
				insertError(AG52);

				redirect($backURL);
			}
		}

	}
}










/*echo $supAgentID;
echo "<br>";

exit();*/
//echo  implode(",", $_POST["IDAcountry"]);
//echo implode(" ", $arr );
//exit;
if ($_POST["userID"] == ""){
	session_register("manualAgentNum");
	session_register("agentCompany");
	session_register("agentContactPerson");
	session_register("agentAddress");
	session_register("agentAddress2");
	session_register("postcode");
	session_register("agentCountry");
	session_register("agentCity");
	session_register("distAlias");
	session_register("agentZip");
	session_register("agentPhone");
	session_register("agentFax");
	session_register("email");
	session_register("agentURL");
	session_register("agentMSBNumber");
	session_register("msbMonth");
	session_register("msbDay");
	session_register("msbYear");
	session_register("agentCompRegNumber");
	session_register("agentCompDirector");
	session_register("agentDirectorAdd");
	session_register("agentProofID");
	session_register("otherProofID");
	session_register("dDate");
	session_register("idMonth");
	session_register("idDay");
	session_register("idYear");
	session_register("agentDocumentProvided");
	session_register("agentBank");
	session_register("agentAccountName");
	session_register("agentAccounNumber");
	session_register("agentBranchCode");
	session_register("agentAccountType");
	session_register("agentCurrency");
	session_register("agentCommCurrency");
	session_register("agentAccountLimit");
	session_register("agentCommission");
	session_register("agentStatus");
	session_register("parentID");
	session_register("source");
	if($agentIDNumberFlag){
		session_register("agentIDNumber");
	}

	$_SESSION["manualAgentNum"] = $_POST["manualAgentNum"];
	$_SESSION["agentCompany"] = $_POST["agentCompany"];
	$_SESSION["agentContactPerson"] = $_POST["agentContactPerson"];
	$_SESSION["agentAddress"] = $_POST["agentAddress"];
	$_SESSION["agentAddress2"] = $_POST["agentAddress2"];
	$_SESSION["postcode"] = $_POST["postcode"];
	$_SESSION["Country"] = $_POST["Country"];
	$agentCountry = $_POST["Country"];
	$_SESSION["City"] = $_POST["City"];
	$_SESSION["distAlias"] = $_POST["distAlias"];
	$_SESSION["agentZip"] = $_POST["postcode"];
	$_SESSION["agentPhone"] = $_POST["agentPhone"];
	$_SESSION["agentFax"] = $_POST["agentFax"];
	$_SESSION["email"] = $_POST["email"];
	$_SESSION["agentURL"] = $_POST["agentURL"];
	$_SESSION["agentHouseNumber"] = $_POST["agentHouseNumber"];
	$_SESSION["agentMSBNumber"] = $_POST["agentMSBNumber"];
	$_SESSION["msbMonth"] = $_POST["msbMonth"];
	$_SESSION["msbDay"] = $_POST["msbDay"];
	$_SESSION["msbYear"] = $_POST["msbYear"];
	$_SESSION["agentCompRegNumber"] = $_POST["agentCompRegNumber"];
	$_SESSION["agentCompDirector"] = $_POST["agentCompDirector"];
	$_SESSION["agentDirectorAdd"] = $_POST["agentDirectorAdd"];
	$_SESSION["agentProofID"] = $_POST["agentProofID"];
	$_SESSION["otherProofID"] = $_POST["otherProofID"];
	$_SESSION["dDate"] = $_POST["dDate"];
	$_SESSION["idMonth"] = $_POST["idMonth"];
	$_SESSION["idDay"] = $_POST["idDay"];
	$_SESSION["idYear"] = $_POST["idYear"];
	$_SESSION["agentDocumentProvided"] = (is_array($_POST["agentDocumentProvided"]) ? implode(",",$_POST["agentDocumentProvided"]) : "");
	$_SESSION["agentBank"] = $_POST["agentBank"];
	$_SESSION["agentAccountName"] = $_POST["agentAccountName"];
	$_SESSION["agentAccounNumber"] = $_POST["agentAccounNumber"];
	$_SESSION["agentBranchCode"] = $_POST["agentBranchCode"];
	$_SESSION["agentAccountType"] = $_POST["agentAccountType"];
	$_SESSION["agentCurrency"] = $_POST["agentCurrency"];
	$_SESSION["agentCommCurrency"] = $_POST["agentCommCurrency"];
	$_SESSION["agentAccountLimit"] = $_POST["agentAccountLimit"];
	$_SESSION["agentCommission"] = $_POST["agentCommission"];
	$_SESSION["agentStatus"] = $_POST["agentStatus"];
	$_SESSION["isCorrespondent"] = $_POST["correspondent"];
	$_SESSION["payinBook"] = $_POST["payinBook"];
	$_SESSION["ch_payinBook"] = $_POST["ch_payinBook"];
	$_SESSION["source"] = $_POST["source"];
	$currencyOrigin=$_POST["currencyOrigin"];
	$_SESSION["currencyOrigin"] = $_POST["currencyOrigin"];
	if($agentIDNumberFlag){
		$_SESSION["agentIDNumber"]	=	$_POST["agentIDNumber"];
	}

	////$_GET["ida"] == "Y"

	//$_SESSION["IDAcountry"] = implode(",", $_POST["IDAcountry"]);
	if($_POST["correspondent"] == "ONLY")
	{
		$backURL = "add-agent.php?msg=Y&type=$typeV&ida=$ida&userID=$userID&agentCode=$agentCode&searchBy=$searchBy&Country=$Country&agentStatus=$agentStatus";
	}else{
		$backURL = "add-agent.php?msg=Y&type=$typeV&ida=$ida&userID=$userID&agentCode=$agentCode&searchBy=$searchBy&Country$Country&agentStatus=$agentStatus";
	}
} else {

	if($_POST["correspondent"] == "ONLY")
	{
		$backURL = "update-agent.php?userID=$_POST[userID]&msg=Y&ida=$ida&type=$typeV&userID=$userID&agentCode=$agentCode&searchBy=$searchBy&Country=$Country&agentStatus=$agentStatus";
	}else{
		$backURL = "update-agent.php?userID=$_POST[userID]&msg=Y&ida=$ida&type=$typeV&userID=$userID&agentCode=$agentCode&searchBy=$searchBy&Country=$Country&agentStatus=$agentStatus";
	}
}
$uploadImageVar = $_POST["uploadImage"];
$backURL .= "&pageID=$pageID&alertID=$alertID";

if (CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y" || CONFIG_MANUAL_DISTRIBUTOR_NUMBER == "1" && $_GET["ida"] == "Y") {
	if (trim($_POST["manualAgentNum"]) == "") {
		insertError(AG42);
		redirect($backURL);
	} else {
		if ($_POST["userID"] == "") {
			$qUserName = "SELECT `username` FROM ".TBL_ADMIN_USERS." WHERE `username` = '".trim($_POST["manualAgentNum"])."'";
			$qUserRes = selectFrom($qUserName);
			if ($qUserRes["username"] != "") {
				insertError(AG43) ;
				redirect($backURL) ;
			}
		} else {
			$qUserName = "SELECT `username` FROM ".TBL_ADMIN_USERS." WHERE `userID` = '".$_POST["userID"]."'" ;
			$qUserRes = selectFrom($qUserName) ;
			if ($qUserRes["username"] != trim($_POST["manualAgentNum"])) {
				$qUserName2 = "SELECT `username` FROM ".TBL_ADMIN_USERS." WHERE `username` = '".trim($_POST["manualAgentNum"])."'" ;
				$qUserRes2 = selectFrom($qUserName2) ;
				if ($qUserRes2["username"] != "") {
					insertError(AG43) ;
					redirect($backURL) ;
				}
			}
		}
	}
}

//check if agent with the same country exists
if(CONFIG_ONLINE_AGENT == '1' && $_SESSION["isOnline"] == 'Y' && !defined("CONFIG_USE_VIRTUAL_AGENT")){
	$agentCountryQuery = selectFrom("SELECT userId,username FROM ".TBL_ADMIN_USERS." WHERE agentCountry = '".$agentCountry."' and isOnline = 'Y'" );
	$usernameOnline = $agentCountryQuery["username"];
}


if (trim($_POST["agentCompany"]) == ""){
	insertError(AG1);
	redirect($backURL);
}

if ($usernameOnline != "" && CONFIG_ONLINE_AGENT == '1' && $_SESSION["isOnline"] == 'Y'){

	insertError(AG53.$usernameOnline.AG54);
	redirect($backURL);
}

if (trim($_POST["agentContactPerson"]) == ""){
	insertError(AG2);
	redirect($backURL);
}
if (trim($_POST["agentAddress"]) == ""){
	insertError(AG3);
	redirect($backURL);
}
if (trim($_POST["Country"]) == ""){
	insertError(AG4);
	redirect($backURL);
}

/*
if (trim($_POST["City"]) == ""){
	insertError(AG5);
	redirect($backURL);
}
if (trim($_POST["agentZip"]) == ""){
	insertError(AG6);
	redirect($backURL);
}*/
if (trim($_POST["agentPhone"]) == ""){
	insertError(AG7);
	redirect($backURL);
}


if($_SESSION["defaultDistrib"] == 'Y')
{
	/**
	 * If want to replace the existing distributor
	 * @Ticket# 3506
	 */
	if($_SESSION["replaceDefaultDistributor"] == 'Y')
	{
		foreach ($_POST["IDAcountry"] as $countryValue)
		{
			//echo("select userID, username from admin where IDAcountry like '%$countryValue%'");
			$existingIDA = selectFrom("select userID, username from admin where IDAcountry like '%$countryValue%' and defaultDistrib = 'Y'");
			if($existingIDA["username"] != "")
			{
				/**
				 * Overwriting the default distributor, by setting it to 'N'
				 */
				$strOverwriteSql = "update admin set defaultDistrib = 'N' where userID=".$existingIDA["userID"];
				update($strOverwriteSql);

				insertError("Default Distributor ".$existingIDA["username"]." for $countryValue has been updated to non default");
			}
		}
	}
	else
	{
		/**
		 * Just to know, which distributor is difault
		 */
		foreach ($_POST["IDAcountry"] as $countryValue)
		{
			//echo("select userID, username from admin where IDAcountry like '%$countryValue%'");
			$existingIDA = selectFrom("select userID, username from admin where IDAcountry like '%$countryValue%' and defaultDistrib = 'Y'");
			if($existingIDA["username"] != "")
			{
				insertError("Already Exist Default Distributor ".$existingIDA["username"]." for $countryValue");
				redirect($backURL);
			}
		}
	}
}

if (CONFIG_FREETEXT_IDFIELDS == '1') {
	$maxFutureYear = date("Y") + CONFIG_IDEXPIRY_YEAR_DELTA;
	if ($_POST["dDate"] != "") {
		//$dDate = explode("-", $_POST["dDate"]);
		$dyear = substr($_POST["dDate"],6,4);
		// Pass an array of date into this function and it returns true or false...
		if (isValidDate($_POST["dDate"])) {
			if ($dyear <= $maxFutureYear) {
				$idExpiryDate = substr($_POST["dDate"],6,4) . "-" . substr($_POST["dDate"],3,2)	. "-" . substr($_POST["dDate"],0,2);
				if ($idExpiryDate <= date("Y-m-d")) {
					insertError(AG44);
					redirect($backURL);
				}
			} else {
				insertError("ID expiry year should not be greater than " . $maxFutureYear . ". If so, then contact to your admin.");
				redirect($backURL);
			}
		} else {
			insertError("Invalid Date Format");
			redirect($backURL);
		}
	} else {
		$idExpiryDate = "";
	}
} else if (CONFIG_IDEXPIRY_WITH_CALENDAR == "1") {
	if ($_POST["dDate"] != "") {
		$dDate = explode("/", $_POST["dDate"]);
		$idExpiryDate = $dDate[2] . "-" . $dDate[1]	. "-" . $dDate[0];
		if ($idExpiryDate <= date("Y-m-d")) {
			insertError(AG44);
			redirect($backURL);
		}
	} else {
		$idExpiryDate = "";
	}
} else {
	$idExpiryDate = $_POST["idYear"] . "-" . $_POST["idMonth"] . "-" . $_POST["idDay"];
}


if ($logo_name != "")
{
	if ($logo_size>PIC_SIZE)
	{
		insertError("Image size must be less than 50K");
		redirect($backURL);
	}
	if (extension($logo_name)!=1)
	{
		insertError("Invalid file format. Only gif, jpg and png are allowed");
		redirect($backURL);
	}
}

if ($_POST["agent_type"]== "")
	$agent_type = 'Supper';
else
	$agent_type = $_POST["agent_type"];

if(CONFIG_NEW_INTRODUCER_ASSOCIATION==1 && $_POST["userID"] != "")
{
	if($_POST["IsByDefault"])
		$IsDefault='Y';
	else
		$IsDefault='N';
	$FieldUpdate = " IsDefault='".$IsDefault."', ";


}

if($_POST["IsByDefault"]==true && CONFIG_NEW_INTRODUCER_ASSOCIATION==1)
{
	if($_POST["userID"] == "")
	{
		$FieldLabel = " ,IsDefault ";
		$FieldValue = " ,'Y' ";
	}

	//Making IsDefault=N for previously linked agents


	$Link_agent = selectFrom(" select linked_Agent from ".TBL_ADMIN_USERS." where userID ='".$_SESSION["parentID"]."'");
	$str_linkd_agnt=$Link_agent["linked_Agent"];

	$str_linkd_agnt = substr($str_linkd_agnt, 0, strlen($str_linkd_agnt)-1);
	$arr_Linkd_agnt = explode(',',$str_linkd_agnt);


	for ($i=0; $i < count($arr_Linkd_agnt); $i++)
	{
		$Link_agent_id = selectFrom(" select userID, agentCompany, username from ".TBL_ADMIN_USERS." where username ='".$arr_Linkd_agnt[$i]."'");
		update("update ".TBL_ADMIN_USERS." set IsDefault = 'N' where userID='".$Link_agent_id["userID"]."'");
	}
}

if ($_POST["userID"] == ""){


	$password = createCode();
	////



	///
	/*if($supAgentID != "")
	{
		$agentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID = $supAgentID");
		$agentNumber = selectFrom("select MAX(subagentNum) from ".TBL_ADMIN_USERS." where parentID='".$_POST["supAagentID"]."'");
		$agentCode = $agentDetails["username"]."-".($agentNumber[0]+1);


		 $Querry_Sqls = "INSERT INTO ".TBL_ADMIN_USERS." (username, password, changedPwd, name, email, created,
		 parentID, subagentNum, agentCompany, agentContactPerson, agentAddress, agentAddress2,  agentCity, agentZip, agentCountry,
		 agentCountryCode, agentPhone, agentFax, agentURL, agentMSBNumber, agentMCBExpiry, agentCompRegNumber, agentCompDirector,
		  agentDirectorAdd, agentProofID, agentIDExpiry, agentDocumentProvided, agentBank, agentAccountName, agentAccounNumber,
		  agentBranchCode, agentAccountType, agentCurrency, agentAccountLimit, commPackage ,agentCommission, agentStatus,
		  isCorrespondent, IDAcountry, agentType, accessFromIP, logo, authorizedFor, postCode) VALUES
		('$agentCode', '$password', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["email"])."',
		'".getCountryTime(CONFIG_COUNTRY_CODE)."', $supAgentID, '".($agentNumber[0]+1)."', '".checkValues($_POST["agentCompany"])."',
		'".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["agentAddress"])."',
		'".checkValues($_POST["agentAddress2"])."', '".$_POST["City"]."', '".$_POST["postcode"]."', '".$_POST["Country"]."',
		'$ccode', '".checkValues($_POST["agentPhone"])."', '".checkValues($_POST["agentFax"])."', '".checkValues($_POST["agentURL"])."',
		 '".checkValues($_POST["agentMSBNumber"])."', '".$_POST["msbYear"]."-".$_POST["msbMonth"]."-".$_POST["msbDay"]."',
		 '".checkValues($_POST["agentCompRegNumber"])."', '".checkValues($_POST["agentCompDirector"])."',
		 '".checkValues($_POST["agentDirectorAdd"])."', '".checkValues($_POST["agentProofID"])."',
		 '".$_POST["idYear"]."-".$_POST["idMonth"]."-".$_POST["idDay"]."', '".$_SESSION["agentDocumentProvided"]."',
		 '".checkValues($_POST["agentBank"])."',
		'".checkValues($_POST["agentAccountName"])."', '".checkValues($_POST["agentAccounNumber"])."',
		'".checkValues($_POST["agentBranchCode"])."', '".$_POST["agentAccountType"]."', '".$_POST["agentCurrency"]."',
		'".checkValues($_POST["agentAccountLimit"])."', '".checkValues($_POST["commPackage"])."',
		'".checkValues($_POST["agentCommission"])."', '".$_POST["agentStatus"]."', '".$_POST["correspondent"]."',
		'".$_SESSION["IDAcountry"]."', '$agent_type', '".checkValues($_POST["accessFromIP"])."', '$Pict',
		'".checkValues($_SESSION["authorizedFor"])."','".$_POST["postcode"]."')";

	}
	else
	{*/
	$agentNumber = selectFrom("select MAX(agentNumber) from ".TBL_ADMIN_USERS." where agentCountry='".$_POST["Country"]."'");
	if (CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y" || CONFIG_MANUAL_DISTRIBUTOR_NUMBER == "1" && $_GET["ida"] == "Y") { // Changed by Jamshed 17-Nov 07
		$agentCode = trim($_POST["manualAgentNum"]) ;
	} else {
		$agentCode = "".$systemPre."".$ccode.($agentNumber[0]+1);///Changed Kashi 11_Nov
	}
	if (isExist("SELECT `username` FROM " . TBL_ADMIN_USERS . " WHERE `username` = '".$agentCode."'")) {
		insertError(AG43);
		redirect($backURL);
	}
	if (isExist("SELECT `username` FROM " . TBL_CM_CUSTOMER . " WHERE `username` = '".$agentCode."'")) {
		insertError(AG43);
		redirect($backURL);
	}
	if (isExist("SELECT `accountName` FROM " . TBL_CUSTOMER . " WHERE `accountName` = '".$agentCode."'")) {
		insertError(AG43);
		redirect($backURL);
	}
	if (isExist("SELECT `loginName` FROM " . TBL_TELLER . " WHERE `loginName` = '".$agentCode."'")) {
		insertError(AG43);
		redirect($backURL);
	}




	$strQuery = "select max(userID) as maxid from admin";
	$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
	$rstRow = mysql_fetch_array($nResult);
	$supAgentID = ($rstRow['maxid']+1);
	$insertionKeys = "";
	$insertionValues = "";
	if(CONFIG_UNLIMITED_EX_RATE_OPTION == '1')
	{
		$insertionKeys .= " ,rateCountries, unlimitedRate";
		$insertionValues .= " ,'".$_SESSION["rateCountries"]."', '".$_POST["unlimitedRate"]."'";
	}
	if($agentUserTypeFlag){
		$insertionKeys .= " ,agentUserType ";
		$insertionValues .= " ,'".$_POST["agentUserType"]."' ";
	}
	/*** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents ***/
	$assocDistDBField = "";
	$csvAssocDistIDs = "";
	$extraUpdate = "";

	if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX") && CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1 )
	{
		if ( !empty($_REQUEST["assocDistributors"]) )
		{
			$assocDistDBField = "associated_distributors";
			$csvAssocDistIDs = implode( ",", $_REQUEST["assocDistributors"] );
			$extraUpdate = " associated_distributors='" . $csvAssocDistIDs . "', ";
		}
		else
		{
			$extraUpdate = " associated_distributors='', ";
		}
	}
	/***** End of code against #3428: Connect Plus - Select Distributors for Agents *****/
	/* #4926 FaithExchange
		Bank charges added for specific distributor.
	*/
	if(isset($_POST["mobile"]) || isset($_POST["source"])){
		$insertionKeys .=",mobile,source";
		$insertionValues .=",'".$_POST["mobile"]."','".$_POST["source"]."'";
	}

	/* compliance check by Niaz Ahmad start code*/

	if (trim($_POST["agentContactPerson"])!= ""){
		$arrAgentName = explode(" ",$_POST["agentContactPerson"]);
		//debug($arrAgentName);
		$agentFirstName  = $arrAgentName[0];
		$agentLastName   = $arrAgentName[1];
		$agentMiddleName = $arrAgentName[2];
		$strComplianceMatch = '';
		$strComplianceMatchType = array();
		$complianceMessage = '';
		$exctMatchLists ='';
		$arrInputs = array("firstName"=>$agentFirstName,"middleName"=>$agentMiddleName,"lastName"=>$agentLastName);
		$arrResponse = payexCompliance($arrInputs);
		//debug($arrResponse);
		if(is_array($arrResponse))
		{
			//debug(count($arrResponse));
			foreach ($arrResponse as $key=>$value){
				if(!empty($value))
					$strComplianceMatch .= $key.",";
				if (is_array($value)){

					foreach($value as $key2=>$value2){
						if($key2 == 'OFACEXACT' || $key2 == 'HMEXACT' || $key2 == 'PEPEXACT' || $key2 == 'PAYEXEXACT')
							$exctMatchLists .= $key.",";
						//$strComplianceMatchType[$key][$key2] = $key2;
						//debug($value);

					} // end second foreach loo[
				} // end second if condition
			} // end first foreach loop
		}// end first if condition
		if(!empty($exctMatchLists)){
			$complianceMessage = 'User exact match found in Following Lists: '.$exctMatchLists;
			insertError($complianceMessage);
			redirect($backURL);
		}
	}
	//debug($_POST["agentContactPerson"],true);
	/* compliance check by Niaz Ahmad end code*/

	if (isset($_POST["associatedAgent"])) {
		$commaSeperatedUserIDs = "";
		for ($i=0;$i<count($_POST["associatedAgent"]);$i++) {
			$associatedAgentValue=$_POST["associatedAgent"][$i];
			$array = explode("(", $associatedAgentValue);
			$name = $array[0];
			$userIDs = selectFrom("select userID from admin where name like '%$name%'");
			if ($i===count($_POST["associatedAgent"])-1)
				$commaSeperatedUserIDs .= $userIDs["userID"];
			else
				$commaSeperatedUserIDs .= $userIDs["userID"].", ";
		}

		$Querry_Sqls = "INSERT INTO ".TBL_ADMIN_USERS." (linked_Agent, username, password, changedPwd, name, email, created, rights, backDays, parentID,
		agentNumber, agentCompany, agentContactPerson, agentAddress, agentAddress2, agentCity, agentZip, agentCountry, 
		agentCountryCode, agentPhone, agentFax, agentURL, agentMSBNumber,  agentHouseNumber,agentMCBExpiry, agentCompRegNumber, agentCompDirector,
		 agentDirectorAdd, agentProofID, agentIDExpiry, agentDocumentProvided, agentBank, agentAccountName, agentAccounNumber,
		  agentBranchCode, agentAccountType, agentCurrency, agentAccountLimit, " . (!empty($assocDistDBField) ? $assocDistDBField . ", " : "") . " commPackage ,agentCommission, agentStatus, 
		  isCorrespondent, IDAcountry, custCountries, agentType, accessFromIP, authorizedFor, postCode,payinBook, isOnline, defaultDistrib, paymentMode, settlementCurrency, hasClave,distAlias,notes $insertionKeys $FieldLabel) VALUES 
		('$commaSeperatedUserIDs', '$agentCode', '$password', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["email"])."',
		 '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$_POST["rights"]."', '".$backDays."', '".$parentID2."', '".($agentNumber[0]+1)."', '".checkValues($_POST["agentCompany"])."',
		  '".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["agentAddress"])."', 
		  '".checkValues($_POST["agentAddress2"])."', '".$_POST["City"]."', '".$_POST["agentZip"]."', '".$_POST["Country"]."', 
		  '$ccode', '".checkValues($_POST["agentPhone"])."', '".checkValues($_POST["agentFax"])."', 
		  '".checkValues($_POST["agentURL"])."', '".checkValues($_POST["agentMSBNumber"])."','".checkValues($_POST["agentHouseNumber"])."',
		   '".$_POST["msbYear"]."-".$_POST["msbMonth"]."-".$_POST["msbDay"]."', '".checkValues($_POST["agentCompRegNumber"])."', 
		   '".checkValues($_POST["agentCompDirector"])."', '".checkValues($_POST["agentDirectorAdd"])."', 
		   '".checkValues($_POST["agentProofID"])."', '".$idExpiryDate."', 
		   '".$_SESSION["agentDocumentProvided"]."', '".checkValues($_POST["agentBank"])."', 
		'".checkValues($_POST["agentAccountName"])."', '".checkValues($_POST["agentAccounNumber"])."', 
		'".checkValues($_POST["agentBranchCode"])."', '".$_POST["agentAccountType"]."', '".$_POST["agentCurrency"]."', 
		'".checkValues($_POST["agentAccountLimit"])."', " . (!empty($csvAssocDistIDs) ? " '" . $csvAssocDistIDs . "', " : "") . "'".checkValues($_POST["commPackage"])."', 
		'".checkValues($_POST["agentCommission"])."', '".$_POST["agentStatus"]."', '".$_POST["correspondent"]."', 
		'".$_SESSION["IDAcountry"]."', '".$_POST["Country"]."', '$agent_type', '".checkValues($_POST["accessFromIP"])."', '".checkValues($_SESSION["authorizedFor"])."','".$_POST["postcode"]."','".$_POST["payinBook"]."', '".$_SESSION["isOnline"]."', '".$_SESSION["defaultDistrib"]."', '".$_POST["agentPaymentMode"]."','".$_POST["currencyOrigin"]."', '".$_POST["hasClave"]."','".$_POST["distAlias"]."','".$notes."' $insertionValues $FieldValue)";

		insertInto($Querry_Sqls);
		$userID = @mysql_insert_id();

		$userIDs = explode(", ", $commaSeperatedUserIDs);
		for ($i=0;$i<count($userIDs);$i++) {
			$associatedAgentValue = $userIDs[$i];
			$existingLinkedDistributors = selectFrom("select linked_Distributor from admin where userID=".$associatedAgentValue);
			$linkedDistributor = $existingLinkedDistributors["linked_Distributor"];
			if ($linkedDistributor == "") {
				$linkedDistributor .= $userID;
			} else {
				$linkedDistributor .= ", ".$userID;
			}
			$strOverwriteSql = "update admin set linked_Distributor = '".$linkedDistributor."' where userID=".$associatedAgentValue;
			update($strOverwriteSql);
		}
	} else if (isset($_POST["associatedDistributor"])) {
		$commaSeperatedUserIDs = "";
		for ($i=0;$i<count($_POST["associatedDistributor"]);$i++){
			$associatedDistributorValue=$_POST["associatedDistributor"][$i];
			$array = explode("(", $associatedDistributorValue);
			$name = $array[0];
			$userIDs = selectFrom("select userID from admin where name like '%$name%'");
			if ($i===count($_POST["associatedDistributor"])-1)
				$commaSeperatedUserIDs .= $userIDs["userID"];
			else
				$commaSeperatedUserIDs .= $userIDs["userID"].", ";
		}

		$Querry_Sqls = "INSERT INTO ".TBL_ADMIN_USERS." (linked_Distributor, username, password, changedPwd, name, email, created, rights, backDays, parentID,
		agentNumber, agentCompany, agentContactPerson, agentAddress, agentAddress2, agentCity, agentZip, agentCountry, 
		agentCountryCode, agentPhone, agentFax, agentURL, agentMSBNumber,  agentHouseNumber,agentMCBExpiry, agentCompRegNumber, agentCompDirector,
		 agentDirectorAdd, agentProofID, agentIDExpiry, agentDocumentProvided, agentBank, agentAccountName, agentAccounNumber,
		  agentBranchCode, agentAccountType, agentCurrency, agentAccountLimit, " . (!empty($assocDistDBField) ? $assocDistDBField . ", " : "") . " commPackage ,agentCommission, agentStatus, 
		  isCorrespondent, IDAcountry, custCountries, agentType, accessFromIP, authorizedFor, postCode,payinBook, isOnline, defaultDistrib, paymentMode, settlementCurrency, hasClave,distAlias,notes $insertionKeys $FieldLabel) VALUES 
		('$commaSeperatedUserIDs', '$agentCode', '$password', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["email"])."',
		 '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$_POST["rights"]."', '".$backDays."', '".$parentID2."', '".($agentNumber[0]+1)."', '".checkValues($_POST["agentCompany"])."',
		  '".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["agentAddress"])."', 
		  '".checkValues($_POST["agentAddress2"])."', '".$_POST["City"]."', '".$_POST["agentZip"]."', '".$_POST["Country"]."', 
		  '$ccode', '".checkValues($_POST["agentPhone"])."', '".checkValues($_POST["agentFax"])."', 
		  '".checkValues($_POST["agentURL"])."', '".checkValues($_POST["agentMSBNumber"])."','".checkValues($_POST["agentHouseNumber"])."',
		   '".$_POST["msbYear"]."-".$_POST["msbMonth"]."-".$_POST["msbDay"]."', '".checkValues($_POST["agentCompRegNumber"])."', 
		   '".checkValues($_POST["agentCompDirector"])."', '".checkValues($_POST["agentDirectorAdd"])."', 
		   '".checkValues($_POST["agentProofID"])."', '".$idExpiryDate."', 
		   '".$_SESSION["agentDocumentProvided"]."', '".checkValues($_POST["agentBank"])."', 
		'".checkValues($_POST["agentAccountName"])."', '".checkValues($_POST["agentAccounNumber"])."', 
		'".checkValues($_POST["agentBranchCode"])."', '".$_POST["agentAccountType"]."', '".$_POST["agentCurrency"]."', 
		'".checkValues($_POST["agentAccountLimit"])."', " . (!empty($csvAssocDistIDs) ? " '" . $csvAssocDistIDs . "', " : "") . "'".checkValues($_POST["commPackage"])."', 
		'".checkValues($_POST["agentCommission"])."', '".$_POST["agentStatus"]."', '".$_POST["correspondent"]."', 
		'".$_SESSION["IDAcountry"]."', '".$_POST["Country"]."', '$agent_type', '".checkValues($_POST["accessFromIP"])."', '".checkValues($_SESSION["authorizedFor"])."','".$_POST["postcode"]."','".$_POST["payinBook"]."', '".$_SESSION["isOnline"]."', '".$_SESSION["defaultDistrib"]."', '".$_POST["agentPaymentMode"]."','".$_POST["currencyOrigin"]."', '".$_POST["hasClave"]."','".$_POST["distAlias"]."','".$notes."' $insertionValues $FieldValue)";

		insertInto($Querry_Sqls);
		$userID = @mysql_insert_id();

		$userIDs = explode(", ", $commaSeperatedUserIDs);
		for ($i=0;$i<count($userIDs);$i++){
			$associatedDistributorValue = $userIDs[$i];
			$existingLinkedAgents = selectFrom("select linked_Agent from admin where userID=".$associatedDistributorValue);
			$linkedAgent = $existingLinkedAgents["linked_Agent"];
			if ($linkedAgent == "") {
				$linkedAgent .= $userID;
			} else {
				$linkedAgent .= ", ".$userID;
			}
			$strOverwriteSql = "update admin set linked_Agent = '".$linkedAgent."' where userID=".$associatedDistributorValue;
			update($strOverwriteSql);
		}
	}

	//}

	///
	if(defined("CONFIG_NEW_INTRODUCER_ASSOCIATION") && CONFIG_NEW_INTRODUCER_ASSOCIATION==1)
	{
		$arrLinkedAgent = selectFrom("select linked_Agent FROM ".TBL_ADMIN_USERS." WHERE  userID ='".$_SESSION["parentID"]."'");


		if(!empty($arrLinkedAgent["linked_Agent"]))
		{
			$arrLinkedAgent["linked_Agent"] .=$agentCode.",";

			update("update ".TBL_ADMIN_USERS." set linked_Agent = '".$arrLinkedAgent["linked_Agent"]."' where userID='".$_SESSION["parentID"]."'");

		}
	}
	///
	$arrLastInsertId = selectFrom("select LAST_INSERT_ID() as li");
	$insertedID = $arrLastInsertId["li"];
	$userID = $insertedID;
	if(defined("CONFIG_DISTRIBUTOR_BANK_CHARGES") && CONFIG_DISTRIBUTOR_BANK_CHARGES=="1"){
		update("update ".TBL_ADMIN_USERS." set bankCharges = '".$_POST["distributorBankcharges"]."' where userID='".$insertedID."'");
	}
	if($agentIDNumberFlag){
		update("update ".TBL_ADMIN_USERS." set agentIDNumber = '".$_POST["agentIDNumber"]."' where userID='".$insertedID."'");
	}
	if($agentCommCurrency){
		update("update ".TBL_ADMIN_USERS." set agentCommCurrency = '".$_POST["agentCommCurrency"]."' where userID='".$insertedID."'");
	}
	$_SESSION["supAgentID"] = '';
	/****************/
	if ($_FILES["file"]["error"] > 0)
	{
		echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
	}
	else
	{/*
echo "Upload: " . $_FILES["file"]["name"] . "<br />";
echo "Type: " . $_FILES["file"]["type"] . "<br />";
echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />";*/
		if(defined("CONFIG_ADD_INTRO_UPLOAD_DOCS") && CONFIG_ADD_INTRO_UPLOAD_DOCS=="1"){
			if( isset($_POST["agentAddress"])&&isset($_POST["manualAgentNum"])&&isset($_POST["agentCompany"])&&isset($_POST["agentContactPerson"])&&isset($_POST["Country"])&&isset($_POST["agentPhone"]))
			{
				$path='../files/customer/' . $_FILES["file"]["name"];
				move_uploaded_file($_FILES["file"]["tmp_name"],"../files/customer/" . $_FILES["file"]["name"]);
				//update("update document_upload set path='$path' where userId='$userID'");
				$insertLogo = "INSERT INTO document_upload (path,userId) values ('".$path."','".$userID."')";
				insertInto($insertLogo);
				// echo "Stored in: " . "upload/" . $_FILES["file"]["name"];

			}
		}
	}
	/********************/
	// PICTURE UPLOAD CODE
	if ($logo_name != "")
	{
		$Ext = strrchr($logo_name,".");
		if (is_uploaded_file($logo))
		{
			$Pict = strtolower($agentCode.$Ext);
			move_uploaded_file($logo, "../logos/" . strtolower($agentCode . $Ext) );
			update("update " . TBL_ADMIN_USERS. " set logo='$Pict' where username='$agentCode'");
		}
	}

	$fromName = SUPPORT_NAME;
	$fromEmail = SUPPORT_EMAIL;

	$subject = "Admin Account creation at $company";
	$message = "Congratulations! ".$_POST["name"].",<br>
		Your agent account has been created at $company agent module. Your login information is as follows:<br>
		Login: ".$agentCode."<br>
		Password: ".$password."<br>
		Name: ".$_POST["agentContactPerson"]."<br>
		To access your account please <a href=\"http://".$_SERVER['SERVER_NAME']."\">click here</a><br>

		$company Support";
	if(CONFIG_MAIL_GENERATOR == '1'){
		$optionalEmail = true;
		if($_POST['emailFlag'] == true && $_POST['sendAgentEmail'] == false)
			$optionalEmail = false;
		if($optionalEmail){
			if(!empty($_POST["supAgentID"]) && $_POST["agentUserType"]=='Introducer')
			{
				$agentEmailAdress = selectFrom("SELECT email from ".TBL_ADMIN_USERS." where userID='".$_POST["supAgentID"]."'");
			}


			sendMail($_POST["email"], $subject, $message, $fromName, $fromEmail,'Enabled');
			//Email tp Admin Manager
			if(!empty($agentEmailAdress['email']))
			{
				sendMail($agentEmailAdress['email'], $subject, $message, $fromName, $fromEmail,'Enabled');

			}

		}
	}

	if(CONFIG_SHARE_OTHER_NETWORK == '1')
	{
		include("sharedUserEntry.php");

	}
	////To record in History
	$descript = "Agent is added";
	activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,TBL_ADMIN_USERS,$descript);



	$_SESSION["manualAgentNum"] = "";
	$_SESSION["agentCompany"] = "";
	$_SESSION["agentContactPerson"] = "";
	$_SESSION["agentAddress"] = "";
	$_SESSION["agentAddress2"] = "";
	$_SESSION["postcode"] = "";
	$_SESSION["Country"] = "";
	$_SESSION["City"] = "";
	$_SESSION["distAlias"] = "";
	$_SESSION["agentZip"] = "";
	$_SESSION["agentPhone"] = "";
	$_SESSION["agentFax"] = "";
	$_SESSION["email"] = "";
	$_SESSION["agentURL"] = "";
	$_SESSION["agentMSBNumber"] = "";
	$_SESSION["agentHouseNumber"] = "";
	$_SESSION["msbMonth"] = "";
	$_SESSION["msbDay"] = "";
	$_SESSION["msbYear"] = "";
	$_SESSION["agentCompRegNumber"] = "";
	$_SESSION["agentCompDirector"] = "";
	$_SESSION["agentDirectorAdd"] = "";
	$_SESSION["agentProofID"] = "";
	$_SESSION["otherProofID"] = "";
	$_SESSION["dDate"] = "";
	$_SESSION["idMonth"] = "";
	$_SESSION["idDay"] = "";
	$_SESSION["idYear"] = "";
	$_SESSION["agentDocumentProvided"] = "";
	$_SESSION["agentBank"] = "";
	$_SESSION["agentAccountName"] = "";
	$_SESSION["agentAccounNumber"] = "";
	$_SESSION["agentBranchCode"] = "";
	$_SESSION["agentAccountType"] = "";
	$_SESSION["agentCurrency"] = "";
	$_SESSION["agentCommCurrency"] = "";
	$_SESSION["agentAccountLimit"] = "";
	$_SESSION["IDAcountry"] = "";
	$_SESSION["authorizedFor"] = "";
	$_SESSION["agentCommission"] = "";
	$_SESSION["agentStatus"] = "";
	$_SESSION["isOnline"] = "";
	$_SESSION["defaultDistrib"] = "";
	$_SESSION["payinBook"] = "";
	$_SESSION["ch_payinBook"] = "";
	$_SESSION["remoteAvailable"] = "";
	$_SESSION["hasClave"] = "";
	$_SESSION["rateCountries"] = "";
	$_SESSION["unlimitedRate"] = "";
	$_SESSION["notes"] = "";
	$_SESSION["agentUserType"] = "";
	if($agentIDNumberFlag){
		$_SESSION["agentIDNumber"] = "";
	}
	if($_POST["supAagentID"] != "")
	{
		if($_POST["correspondent"] == "ONLY")
		{
			insertError(eregi_replace("Super",'Sub', eregi_replace("Agent",'Distributor', AG16)). " The login name is <b>$agentCode</b> and password is <b>$password</b>");
		}
		else
		{
			insertError(eregi_replace("Super",'Sub', AG16). " The login name is <b>$agentCode</b> and password is <b>$password</b>");
		}
	}
	else
	{
		if($_POST["correspondent"] == "ONLY")
		{
			insertError(eregi_replace("Agent",'Distributor', AG16). " The login name is <b>$agentCode</b> and password is <b>$password</b>");
		}
		else
		{
			insertError(AG16 . " The login name is <b>$agentCode</b> and password is <b>$password</b>");
		}
	}

	$strConfirmMessage =  ($_POST["correspondent"] == "ONLY" ? eregi_replace("Agent",'Distributor', AG16) : AG16). " The login name is <b>$agentCode</b> and password is <b>$password</b>";

	if(CONFIG_USE_VIRTUAL_AGENT == "1" && !empty($_POST["isOnline"]) && $_POST["isOnline"] == "Y")
		$strConfirmMessage .= "<br />Virtual Agent URL: <a target='_blank' href='".CONFIG_VIRTUAL_AGENT_URL_PREFIX."/user/?va=".$insertedID."' style=\"color:#669933\">".CONFIG_VIRTUAL_AGENT_URL_PREFIX."/user/?va=".$insertedID."</a>";

	//insertError( ($_POST["correspondent"] == "ONLY" ? eregi_replace("Agent",'Distributor', AG16) : AG16). " The login name is <b>$agentCode</b> and password is <b>$password</b>");

	insertError($strConfirmMessage);

	$backURL .= "&success=Y";
} else {
	if (trim($_POST["oldCountry"]) != trim($_POST["Country"])){
		$agentNumber = selectFrom("select MAX(agentNumber) from ".TBL_ADMIN_USERS." where agentCountry='".$_POST["Country"]."'");
		if (CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y" || CONFIG_MANUAL_DISTRIBUTOR_NUMBER == "1" && $_GET["ida"] == "Y") { // Changed by Jamshed 17-Nov 07
			if (trim($_POST["manualAgentNum"]) != trim($_POST["manualAgentNum2"])) {
				if (isExist("SELECT `username` FROM " . TBL_ADMIN_USERS . " WHERE `username` = '".trim($_POST["manualAgentNum"])."'")) {
					insertError(AG43);
					redirect($backURL);
				}
				if (isExist("SELECT `username` FROM " . TBL_CM_CUSTOMER . " WHERE `username` = '".trim($_POST["manualAgentNum"])."'")) {
					insertError(AG43);
					redirect($backURL);
				}
				if (isExist("SELECT `accountName` FROM " . TBL_CUSTOMER . " WHERE `accountName` = '".trim($_POST["manualAgentNum"])."'")) {
					insertError(AG43);
					redirect($backURL);
				}
				if (isExist("SELECT `loginName` FROM " . TBL_TELLER . " WHERE `loginName` = '".trim($_POST["manualAgentNum"])."'")) {
					insertError(AG43);
					redirect($backURL);
				}
			}
			$agentCode = trim($_POST["manualAgentNum"]);
		} elseif($validationFlag) {
			$agentCode = "".$systemPre."".$ccode.($agentNumber[0]+1);///Changed Kashi 11_Nov
			if (isExist("SELECT `username` FROM " . TBL_ADMIN_USERS . " WHERE `username` = '".$agentCode."'")) {
				insertError(AG43);
				redirect($backURL);
			}
			if (isExist("SELECT `username` FROM " . TBL_CM_CUSTOMER . " WHERE `username` = '".$agentCode."'")) {
				insertError(AG43);
				redirect($backURL);
			}
			if (isExist("SELECT `accountName` FROM " . TBL_CUSTOMER . " WHERE `accountName` = '".$agentCode."'")) {
				insertError(AG43);
				redirect($backURL);
			}
			if (isExist("SELECT `loginName` FROM " . TBL_TELLER . " WHERE `loginName` = '".$agentCode."'")) {
				insertError(AG43);
				redirect($backURL);
			}
		}
		$agentAcountUpdate = "username='$agentCode', agentNumber='".($agentNumber[0]+1)."', ";
	} else {
		if (CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y" || CONFIG_MANUAL_DISTRIBUTOR_NUMBER == "1" && $_GET["ida"] == "Y") {
			if (trim($_POST["manualAgentNum"]) != trim($_POST["manualAgentNum2"])) {
				if (isExist("SELECT `username` FROM " . TBL_ADMIN_USERS . " WHERE `username` = '".trim($_POST["manualAgentNum"])."'")) {
					insertError(AG43);
					redirect($backURL);
				}
				if (isExist("SELECT `username` FROM " . TBL_CM_CUSTOMER . " WHERE `username` = '".trim($_POST["manualAgentNum"])."'")) {
					insertError(AG43);
					redirect($backURL);
				}
				if (isExist("SELECT `accountName` FROM " . TBL_CUSTOMER . " WHERE `accountName` = '".trim($_POST["manualAgentNum"])."'")) {
					insertError(AG43);
					redirect($backURL);
				}
				if (isExist("SELECT `loginName` FROM " . TBL_TELLER . " WHERE `loginName` = '".trim($_POST["manualAgentNum"])."'")) {
					insertError(AG43);
					redirect($backURL);
				}
			}
			$agentCode = trim($_POST["manualAgentNum"]);
			$agentAcountUpdate = "username='$agentCode', ";
		}
	}
	$_SESSION["agentDocumentProvided"] = (is_array($_POST["agentDocumentProvided"]) ? implode(",",$_POST["agentDocumentProvided"]) : "");


	if(CONFIG_UNLIMITED_EX_RATE_OPTION == '1'  && $validationFlag)
	{
		$agentAcountUpdate .= " rateCountries = '".$_SESSION["rateCountries"]."', unlimitedRate = '".$_POST["unlimitedRate"]."',";
	}
	if($agentUserTypeFlag  && $validationFlag){
		$agentAcountUpdate .= " agentUserType = '".$_POST["agentUserType"]."',";
	}
	/*** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents ***/
	$extraUpdate = "";

	if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX") && CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1  && $validationFlag)
	{
		if ( !empty($_REQUEST["assocDistributors"]) )
		{
			$csvAssocDistIDs = implode( ",", $_REQUEST["assocDistributors"] );
			$extraUpdate = " associated_distributors='" . $csvAssocDistIDs . "', ";
		}
		else
		{
			$extraUpdate = " associated_distributors='', ";
		}
	}
	/***** End of code against #3428: Connect Plus - Select Distributors for Agents *****/
	if(isset($_POST["mobile"]))
		$extraUpdate .=" mobile='".$_POST["mobile"]."', ";
	if($validationFlag)
	{
		if(CONFIG_DISTRIBUTOR_UPDATE_SHOW != "1")
		{
			$extraUpdate .=" source='".$_POST["source"]."',";
		}
	}

	if($validationFlag){
		$extraUpdate .= "
			rights='".$_POST["rights"]."',
			backDays='".$backDays."',
			agentURL='".checkValues($_POST["agentURL"])."', 
			agentMSBNumber='".checkValues($_POST["agentMSBNumber"])."', 
			agentHouseNumber='".checkValues($_POST["agentHouseNumber"])."', 
			agentMCBExpiry='".$_POST["msbYear"]."-".$_POST["msbMonth"]."-".$_POST["msbDay"]."', 
			agentCompRegNumber='".checkValues($_POST["agentCompRegNumber"])."', 
			agentCompDirector='".checkValues($_POST["agentCompDirector"])."', 
			agentDirectorAdd='".checkValues($_POST["agentDirectorAdd"])."', 
			agentProofID='".checkValues($_POST["agentProofID"])."', 
			agentIDExpiry='".$idExpiryDate."', 
			agentDocumentProvided='".$_SESSION["agentDocumentProvided"]."', 
			agentBank='".checkValues($_POST["agentBank"])."', 
			agentAccountName='".checkValues($_POST["agentAccountName"])."', 
			agentAccounNumber='".checkValues($_POST["agentAccounNumber"])."', 
			agentBranchCode='".checkValues($_POST["agentBranchCode"])."', 
			agentAccountType='".$_POST["agentAccountType"]."', 
			agentCurrency='".$_POST["agentCurrency"]."', 
			agentAccountLimit='".checkValues($_POST["agentAccountLimit"])."',
			commPackage = '".$_POST["commPackage"]."', 
			agentCommission='".checkValues($_POST["agentCommission"])."', 
			agentStatus='".$_POST["agentStatus"]."', 
			isCorrespondent = '".$_POST["correspondent"]."',
			IDAcountry = '".$_SESSION["IDAcountry"]."',
			accessFromIP = '".checkValues($_POST["accessFromIP"])."',
			authorizedFor = '".checkValues($_SESSION["authorizedFor"])."',
			parentID = '".$_SESSION["parentID"]."',
			isOnline = '".$_SESSION["isOnline"]."',
			defaultDistrib = '".$_SESSION["defaultDistrib"]."',
			paymentMode = '".$_POST["agentPaymentMode"]."',
			hasClave = '".$_POST["hasClave"]."',	
			distAlias = '".$_POST["distAlias"]."',
			payinBook = '".$_POST["payinBook"]."',
			settlementCurrency = '".$_POST["currencyOrigin"]."',
			notes = '".$notes."', ";
	}
	$Querry_Sqls = "update ".TBL_ADMIN_USERS." set $agentAcountUpdate 
	 name='".$_POST["agentContactPerson"]."', 
	 email='".checkValues($_POST["email"])."', 
	 agentCompany='".checkValues($_POST["agentCompany"])."', 
	 agentContactPerson='".checkValues($_POST["agentContactPerson"])."', 
	 agentAddress='".checkValues($_POST["agentAddress"])."', 
	 agentAddress2='".checkValues($_POST["agentAddress2"])."',	 
	 agentCity='".$_POST["City"]."', 
	 agentZip='".$_POST["agentZip"]."', 
	 agentCountry='".$_POST["Country"]."', 
	 agentCountryCode='$ccode', 
	 agentPhone='".checkValues($_POST["agentPhone"])."', 
	 $extraUpdate
	 $FieldUpdate 
	 agentFax='".checkValues($_POST["agentFax"])."', 
	 postCode='".checkValues($_POST["agentZip"])."'
	 where userID='".$_POST["userID"]."'";





	//debug($Querry_Sqls,true);
	if ($logo_name != "" && $validationFlag)
	{
		$Ext = strrchr($logo_name,".");
		if (is_uploaded_file($logo))
		{
			$Pict=strtolower($_POST["usern"].$Ext);
			move_uploaded_file($logo, "../logos/" . strtolower($_POST["usern"] . $Ext) );
			echo "  update " . TBL_ADMIN_USERS. " set logo='$Pict' where username='".$_POST["usern"]."'";
			update("update " . TBL_ADMIN_USERS. " set logo='$Pict' where username='".$_POST["usern"]."'");
		}
	}

	update($Querry_Sqls);
	if($validationFlag){
		if(defined("CONFIG_DISTRIBUTOR_BANK_CHARGES") && CONFIG_DISTRIBUTOR_BANK_CHARGES=="1"){
			update("update ".TBL_ADMIN_USERS." set bankCharges = '".$_POST["distributorBankcharges"]."' where userID='".$_POST["userID"]."'");
		}
		if($agentIDNumberFlag){
			update("update ".TBL_ADMIN_USERS." set agentIDNumber = '".$_POST["agentIDNumber"]."' where userID='".$_POST["userID"]."'");
		}
		if($agentCommCurrency){
			update("update ".TBL_ADMIN_USERS." set agentCommCurrency = '".$_POST["agentCommCurrency"]."' where userID='".$_POST["userID"]."'");
		}
	}
	if(CONFIG_SHARE_OTHER_NETWORK == '1')
	{
		include("sharedUserEntry.php");

	}
	////To record in History
	$descript ="Agent is updated";
	activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["userID"],TBL_ADMIN_USERS,$descript);


	$_SESSION["isOnline"] = "";
	$_SESSION["defaultDistrib"] = "";
	$_SESSION["hasClave"] = "";
	$_SESSION["notes"] = "";
	$_SESSION["remoteAvailable"] =  "";
	//echo("\n\n Query is updated");
	insertError(AG17);
	$backURL .= "&success=Y";
}

// Added by Niaz Ahmad at 02-05-2009 @4816 Faith Exchange Upload Document
if(CONFIG_UPLOAD_DOCUMNET_ADD_SUPPER_AGENT	== "1"){
	if($uploadImageVar == "Y"){
		$backURL = "uploadCustomerImage.php?type=".$typeV."&from=add_supper_agent&mode=".$mode."&pageID=add_agent&ida=".$ida."";
		if(!empty($insertedID)){
			$backURL .="&customerID=".$insertedID;
		}else{
			$backURL .="&customerID=".$_REQUEST["userID"];
		}
	}
}

//$userID = "";
//for ($i=0;$i<count($_POST["associatedAgent"]);$i++)
//{
//	$associatedAgentValue=$_POST["associatedAgent"][$i];
//	$array = explode("(", $associatedAgentValue);
//	$name = $array[0];
//	$userIDs = selectFrom("select userID from admin where name like '%$name%'");
//	if ($i===count($_POST["associatedAgent"])-1)
//		$userID .= $userIDs["userID"];
//	else
//		$userID .= $userIDs["userID"].", ";
//}
//$existingIDA = selectFrom("select userID from admin where userID = (select max(userID) from admin)");
//$strOverwriteSql = "update admin set linked_Agent = '$userID' where userID=".$existingIDA["userID"];
//update($strOverwriteSql);

//$userIDs = explode(", ", $commaSeperatedUserIDs);
//for ($i=0;$i<count($userIDs)-1;$i++) {
//	$associatedAgentValue = $userIDs[$i];
//	$existingLinkedDistributors = selectFrom("select linked_Distributor from admin where userID=".$associatedAgentValue);
//	$linkedDistributor = $existingLinkedDistributors["linked_Distributor"];
//	$linkedDistributor .= ", ".$existingIDA["userID"];
//	$strOverwriteSql = "update admin set linked_Distributor = '".$linkedDistributor."' where userID=".$associatedAgentValue;
//	update($strOverwriteSql);
//}

//$userID = "";
//for ($i=0;$i<count($_POST["associatedDistributor"]);$i++){
//	$associatedDistributorValue=$_POST["associatedDistributor"][$i];
//	$array = explode("(", $associatedDistributorValue);
//	$name = $array[0];
//	$userIDs = selectFrom("select userID from admin where name like '%$name%'");
//	if ($i===count($_POST["associatedDistributor"])-1)
//		$userID .= $userIDs["userID"];
//	else
//		$userID .= $userIDs["userID"].", ";
//}
//$existingIDA = selectFrom("select userID from admin where userID = (select max(userID) from admin)");
//$strOverwriteSql = "update admin set linked_Distributor = '$userID' where userID=".$existingIDA["userID"];
//update($strOverwriteSql);
//
//$userIDs = explode(", ", $userID);
//for ($i=0;$i<count($userIDs)-1;$i++){
//	$associatedDistributorValue = $userIDs[$i];
//	$existingLinkedDistributors = selectFrom("select linked_Distributor from admin where userID=".$associatedDistributorValue);
//	$linkedDistributor = $existingLinkedDistributors["linked_Distributor"];
//	$linkedDistributor .= ", ".$existingIDA["userID"];
//	$strOverwriteSql = "update admin set linked_Agent = '".$linkedDistributor."' where userID=".$associatedDistributorValue;
//	update($strOverwriteSql);
//}

///////////////////////////////////
if (isset($_GET['userID'])) {
	if (isset($_POST["associatedAgent"])) {
		$userID = "";
		$arrayUserID="";
		for ($i = 0; $i < count($_POST["associatedAgent"]); $i++) {
			$associatedAgentValue = $_POST["associatedAgent"][$i];
			$array = explode("(", $associatedAgentValue);
			$name = $array[0];
			$userIDs = selectFrom("select userID from admin where name like '%$name%'");
			if ($i === count($_POST["associatedAgent"]) - 1)
				$userID .= $userIDs["userID"];
			else
				$userID .= $userIDs["userID"] . ", ";
			$arrayUserID[$i] = $userIDs["userID"];
		}
		$strOverwriteSql = "update admin set linked_Agent = '$userID' where userID=" . $_GET["userID"];
		update($strOverwriteSql);

		for ($i=0;$i<count($arrayUserID);$i++) {
			$associatedAgentValue = $arrayUserID[$i];
			$existingLinkedDistributors = selectFrom("select linked_Distributor from admin where userID=".$associatedAgentValue);
			$linkedDistributor = $existingLinkedDistributors["linked_Distributor"];
			if ($linkedDistributor == "") {
				$linkedDistributor .= $_GET['userID'];
			} else {
				$array = explode(", ", $linkedDistributor);
				$flag = true;
				foreach ($array as $userID) {
					if ($userID == $_GET['userID']) {
						$flag = false;
						break;
					}
				}
				if ($flag)
				$linkedDistributor .= ", ".$_GET['userID'];
			}
			$strOverwriteSql = "update admin set linked_Distributor = '".$linkedDistributor."' where userID=".$associatedAgentValue;
			update($strOverwriteSql);
		}
	} else if (isset($_POST["associatedDistributor"])) {
		$userID = "";
		$arrayUserID="";
		for ($i = 0; $i < count($_POST["associatedDistributor"]); $i++) {
			$associatedDistributorValue = $_POST["associatedDistributor"][$i];
			$array = explode("(", $associatedDistributorValue);
			$name = $array[0];
			$userIDs = selectFrom("select userID from admin where name like '%$name%'");
			if ($i === count($_POST["associatedDistributor"]) - 1)
				$userID .= $userIDs["userID"];
			else
				$userID .= $userIDs["userID"] . ", ";
			$arrayUserID[$i] = $userIDs["userID"];
		}
		$strOverwriteSql = "update admin set linked_Distributor = '$userID' where userID=" . $_GET["userID"];
		update($strOverwriteSql);

		for ($i=0;$i<count($arrayUserID);$i++) {
			$associatedAgentValue = $arrayUserID[$i];
			$existingLinkedAgents = selectFrom("select linked_Agent from admin where userID=" . $associatedAgentValue);
			$linkedAgent = $existingLinkedAgents["linked_Agent"];
			if ($linkedAgent == "") {
				$linkedAgent .= $_GET['userID'];
			} else {
				$array = explode(", ", $linkedAgent);
				$flag = true;
				foreach ($array as $userID) {
					if ($userID == $_GET['userID']) {
						$flag = false;
						break;
					}
				}
				if ($flag)
					$linkedAgent .= ", " . $_GET['userID'];
			}
			$strOverwriteSql = "update admin set linked_Agent = '" . $linkedAgent . "' where userID=" . $associatedAgentValue;
			update($strOverwriteSql);
		}
	}
}
///////////////////////////////////
$services = "";
for ($i=0;$i<count($_POST["services"]);$i++)
{
	$servicesValue=$_POST["services"][$i];
	if ($i===count($_POST["services"])-1)
		$services .= $servicesValue;
	else
		$services .= $servicesValue.", ";
}

$strOverwriteSql = "update admin set services = '$services' where userID=".$_GET["userID"];
update($strOverwriteSql);
///////////
redirect($backURL);
?>