<?php

	session_start();
	include ("../include/config.php");
	include ("security.php");
	if(!defined("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE") || CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE!="1"){
		echo "<h2>Invalid Request.You have no rights to Access this Page !</h2>";
		exit;
	}
	$titleReciept = "TT Transaction Reciept";
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
		//TT Transaction status array
	$status_arr=array("1"=>"Pending","2"=>"Hold","3"=>"Cancelled","4"=>"Incomplete");
	$TransDetails = selectFrom("select 
									tt.id,
									tt.sender_account_number,
									tt.sending_currency,
									tt.sending_amount,
									tt.receiving_account_number,
									tt.receiving_currency,
									tt.receiving_amount,
									tt.exchange_rate,
									tt.created,
									tt.status,
									
									cust.firstName as CustFName,
									cust.lastName as CustLName, 
									ben.firstName as BenFName,
									ben.lastName as BenLName 
								from 
									tt_transactions as tt,
									customer as cust,
									beneficiary as ben
								where 
									tt.id = '".$_REQUEST["TransId"]."'
								AND
									tt.customer_Id 	= cust.customerID
								AND
									tt.ben_Id     	=ben.benID
									
								");


		
							
								
					
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$titleReciept?></title>
<script language="javascript" src="jquery.js"></script>
<script>
	$(document).ready(function(){
		$("#printbtn").click(function(){
			$(this).hide();
			print();
		});
	});
</script>
<style>
td{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#000000;
}
.amounts_right{
	width: 70px; 
	text-align: right; 
	float:left;
} 
</style>
</head>
<body>
<table border="1" bordercolor="#4C006F" cellspacing="0" cellpadding="0" width="320">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="1" cellpadding="3" id="table">
				<tr>
				   <td colspan="4" align="center" bgcolor="#FFFFFF">
						<img height='50' alt='' src='<?=CONFIG_LOGO?>' width='100' /><br />
					<span style="font-size:11px;">
					87,Edgware Road, London, W2 2HX <br />
					Tel: 0207 723 3622, Fax: 0207 723 2984
					</span>
						</td>
				</tr>
				<tr> 
				  <td colspan="4" align="center" bgcolor="#FFFFFF">
					  <h2><?=$titleReciept?></h2>
						<br />					</td>
				</tr>
				<tr>
					<td width="37%" scope="col"><b>Date:</b></td>  <!--date("d/m/Y H:i:s A",strtotime($TransDetails["created"]))-->
					<td width="63%" scope="col"><?=date('F j, Y, g:i a',strtotime($TransDetails["created"]))?></td>
				</tr> 
				
				<tr>
					<td width="37%" scope="col"><b>Transaction:</b></td> 
					<td width="63%" scope="col"><?="TT-".$TransDetails["id"]?></td>
				</tr> 
				<tr>
					<td width="37%" scope="col"><b>Transaction Status:</b></td> 
					<td width="63%" scope="col"><?=$status_arr[$TransDetails["status"]]?></td>
				</tr> 
				
				<tr bgcolor="#ededed">
					<td width="37%" class="tdDefination" style="font-size:14px" colspan="5"><b>Sender Details</b></td>
						
				</tr>
						
					
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination" style="font-size:10px"><b>Sender Name:&nbsp;</b></td>
							<td align="left" colspan="3"><?=$TransDetails["CustFName"]." ".$TransDetails["CustLName"]?></td>
						</tr>
						
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination" style="font-size:10px"><b>Sender A/C #:&nbsp;</b></td>
							<td align="left" colspan="3"><?=$TransDetails["sender_account_number"]?></td>
						</tr>
						
						
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination" style="font-size:10px"><b>Sending Amount:&nbsp;</b></td>
							<td align="left" colspan="3"><?=number_format($TransDetails["sending_amount"],2,'.',',')?></td>
						</tr>
						
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination" style="font-size:10px"><b>Sending Currency:&nbsp;</b></td>
							<td align="left" colspan="3"><?=$TransDetails["sending_currency"]?></td>
						</tr>
						
						
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination" style="font-size:10px"><b>Exchange Rate:&nbsp;</b></td>
							<td align="left" colspan="3"><?=number_format($TransDetails["exchange_rate"],4,'.',',')?></td>
						</tr>
				
				<tr bgcolor="#ededed">
							<td width="37%" class="tdDefination" style="font-size:14px" colspan="5"><b>Receiving  Details</b></td>
						
				</tr>
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination" style="font-size:10px"><b>Receiver Name:&nbsp;</b></td>
							<td align="left" colspan="3"><?=$TransDetails["BenFName"]." ".$TransDetails["BenLName"]?></td>
						</tr>
						
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination" style="font-size:10px"><b>Receiver A/C #:&nbsp;</b></td>
							<td align="left" colspan="3"><?=$TransDetails["receiving_account_number"]?></td>
						</tr>
						
						
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination" style="font-size:10px"><b>Receiving Amount:&nbsp;</b></td>
							<td align="left" colspan="3"><?=number_format($TransDetails["receiving_amount"],2,'.',',')?></td>
						</tr>
						
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination" style="font-size:10px"><b>Receiving Currency:&nbsp;</b></td>
							<td align="left" colspan="3"><?=$TransDetails["receiving_currency"]?></td>
						</tr>
						
						
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination" style="font-size:10px">&nbsp;</td>
							<td align="left" colspan="3">&nbsp;</td>
						</tr>
				
				
				<tr>
					<td colspan="2" align="center">
						<br /><br />
						<hr noshade="noshade" size="1" width="100%" />
						<b>Customer Signature</b>
						<br /><br />
						<hr noshade="noshade" size="1" width="100%" />
						<b>Authorized</b>					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<input type="button" id="printbtn" value="Print Receipt" />

</body>
</html>