<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];

	$strMainCaption = "Manage Cheque Order Fee Records"; 

	$cmd = $_REQUEST["cmd"];

	//debug($_REQUEST);
	if(!empty($cmd))
	{
		$bolDataSaved = false;
		/* Request to store or update the data of company */
		if($cmd == "ADD")
		{
			/**
			 * If the Id is empty than the record is new one
			 */
			$strCreatedBy = $userID."|".$agentType;
			$strInsertSql = "insert into cheque_order_fee 
								(type, type_value, amount_from, amount_upto, fee, created_by, created_on, updated_on)
							 values
								('".$_REQUEST["fee_type"]."','".$_REQUEST["company_val"]."','".$_REQUEST["amount_from"]."','".$_REQUEST["amount_upto"]."',
								 '".$_REQUEST["cheque_fee"]."','".$strCreatedBy."','".time()."','".time()."')";
			
			if(mysql_query($strInsertSql))
				$bolDataSaved = true;
			else
				$bolDataSaved = false;

			$cmd = "";
			$fdi = "";
			$fee_type = "";
			$company_id = "";
			$amount_from = "";
			$amount_upto = "";
			$cheque_fee = "";
			
			activities($_SESSION["loginHistoryID"],"INSERT",0,"cheque_order_fee","New Fee Added");
		}
		
		if($cmd == "UPDATE")
		{
			/**
			 * Record is not new and required the updation
			 */
			if(!empty($_REQUEST["fdi"]))
			{
				$strUpdateSql = "update cheque_order_fee 
								 set
									type = '".$_REQUEST["fee_type"]."',
									type_value = '".$_REQUEST["company_val"]."',
									amount_from = '".$_REQUEST["amount_from"]."',
									amount_upto = '".$_REQUEST["amount_upto"]."',
									fee = '".$_REQUEST["cheque_fee"]."',
									updated_on = '".time()."'
								 where
									fee_id = '".$_REQUEST["fdi"]."'
							"; 
				if(update($strUpdateSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
					
				$fdi = "";
				$fee_type = "";
				$company_id = "";
				$amount_from = "";
				$amount_upto = "";
				$cheque_fee = "";
				$cmd = "";
				
				activities($_SESSION["loginHistoryID"],"UPDATE",$_REQUEST["fdi"],"cheque_order_fee","Cheque Order Fee updated");
			}	
		}
		
		
		
		if($cmd == "EDIT")
		{
		
			if(!empty($_REQUEST["fdi"]))
			{
				$strGetDataSql = "select * from cheque_order_fee where fee_id='".$_REQUEST["fdi"]."'";
				$arrCompanyData = selectFrom($strGetDataSql);
				//debug($arrCompanyData);
				
				$fdi = $arrCompanyData["fee_id"];
				$fee_type = $arrCompanyData["type"];
				$company_id = $arrCompanyData["type_value"];
				$amount_from = $arrCompanyData["amount_from"];
				$amount_upto = $arrCompanyData["amount_upto"];
				$cheque_fee = $arrCompanyData["fee"];
				
				if($amount_upto == "0.00")
					$amount_upto = "";
				
				$cmd = "UPDATE";
				
				activities($_SESSION["loginHistoryID"],"VIEW",$_REQUEST["fdi"],"cheque_order_fee","Cheque Order Fee Viewed");
			}
		}
	
		if($cmd == "DEL")
		{
			if(!empty($_REQUEST["fdi"]))
			{
				$strDelSql = "delete from cheque_order_fee where fee_id='".$_REQUEST["fdi"]."'";
				if(mysql_query($strDelSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
				
				activities($_SESSION["loginHistoryID"],"DELETE",$_REQUEST["fdi"],"cheque_order_fee","Cheque Order Fee Deleted");
			}
			else
				$bolDataSaved = false;
			$cmd = "";
		}
	}
	
	$arrCompaniesRows = selectMultiRecords("select company_id, name from company order by name");
	
	if(empty($cmd))
		$cmd = "ADD";
	
	/* The default amount in "amount from" field should be '1' */
	if(empty($amount_from))
		$amount_from = 1;
	
	/* Fetching the list of fees to display at the bottom */	
	$arrAllFeesData = selectMultiRecords("Select * from cheque_order_fee order by updated_on desc");

		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Manage Cheque Fee Record</title>
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script language="javascript" src="jquery.cluetip.js"></script>
<script>
	$(document).ready(function() {
		$("#submitFee").validate({
			rules: {
				fee_type: "required",
				cheque_fee: {
					required: true,
					number: true,
					min: 0
				},
				amount_from: {
					required: true,
					number: true,
					min: 1
				},
				amount_upto: {
					number: true//,
					//min: "#amount_from"+1
				},
				company_val: {
				   required: function(element) {
						return $("#fee_type").val() == "C"
					  }
				}

			},
			messages: {
				fee_type: "<br />Please select the fee type from drop down.",
				cheque_fee: { 
					required: "<br />Please enter valid fee",
					number: "<br />The fee entered is invalid.",
					min: "<br />The fee cannot be in nagative."
				},
				amount_from: { 
					required: "<br />The amount from is required.",
					number: "<br />This is not valid amount.",
					min: "<br />Value should be greater than or equal to 1"
				},
				amount_upto: { 
					number: "<br />This is not valid amount."//,
					//min: "<br />Value should be greater than "+ $("#amount_from").val()
				},
				company_val : "<br />Please select any company to continue."
			}
		});
		
		$("#fee_type").click(function(){
			if($(this).val() == "C")
				$("#companyRow").show();
			else
				$("#companyRow").hide();
		});
		
		<? if(!empty($_REQUEST["fdi"])) { ?>
			$("#fee_type").click();
		<? } ?>
		
		$('img').cluetip({splitTitle:'|'});
		
	});
</script>
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style>
td{ 
	font-family:Arial, Helvetica, sans-serif;
	size:8px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.feeList td {
	text-align:center;
	font-size:10px;
	font-family:Arial, Helvetica, sans-serif;
}

.feeList th {
	text-align:center;
	font-size:14px;
	font-family: Arial, Helvetica, sans-serif;
	color:#ffffff;
	background-color:#000066;
}


.firstRow td {
	background-color:#999999;
	font-size:12px;
	color: #000000;
}
.secondRow td {
	background-color: #cdcdcd;
	font-size:12px;
	color: #000000;
}
</style>
</head>
<body>
<form name="submitFee" id="submitFee" action="<?=$_SERVER["PHP_SELF"]?>" method="post">
	<table width="50%" border="0" cellspacing="1" cellpadding="2" align="center" id="mainTble">
		<tr>
			<td colspan="2" align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong>
			</td>
		</tr>
		<? if($bolDataSaved) { ?>
			<tr bgcolor="#000000">
				<td colspan="2" align="center" style="text-align:center; color:#EEEEEE; font-size:13px; font-weight:bold">
					<img src="images/info.gif" title="Information|Command Executed Successfully." />&nbsp;Command Executed Successfully!
				</td>
			</tr>
		<? } ?>
		
		<tr bgcolor="#ededed">
			<td colspan="2" align="center" class="tdDefination" style="text-align:center">
				All fields mark with <font color="red">*</font> are compulsory Fields.
			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="40%" class="tdDefination"><b>Fee Type:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="60%">
				<select name="fee_type" id="fee_type">
					<option value="">Select Fee Type</option>
					<option value="F" <?=($fee_type=="F"?'selected="selected"':'')?>>Fixed Amount</option>
					<option value="P" <?=($fee_type=="P"?'selected="selected"':'')?>>Percentage Of Cheque Amount</option>
					<option value="C" <?=($fee_type=="C"?'selected="selected"':'')?>>Company Based Fee</option>
				</select>
			</td>
		</tr>
		<tr bgcolor="#ededed" id="companyRow" style="display:none">
			<td width="40%" class="tdDefination"><b>Company:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="60%">
				<select name="company_val" id="company_val">
				<?
					foreach($arrCompaniesRows as $comKey => $comVal)
					{
						$strCompanySelectedOption = '';
						if($company_id == $comVal["company_id"])
							$strCompanySelectedOption = 'selected="selected"';
				?>
					<option value="<?=$comVal["company_id"]?>" <?=$strCompanySelectedOption?>><?=$comVal["name"]?></option>
				<? } ?>
				</select>
			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="40%" class="tdDefination">Amount From:&nbsp;<font color="red">*</font></td>
			<td align="left" width="60%"><input type="text" name="amount_from" id="amount_from" size="20" value="<?=$amount_from?>" /></td>
		</tr>
			<tr bgcolor="#ededed">
			<td width="40%" class="tdDefination">Amount Upto:&nbsp;</td>
			<td align="left" width="60%">
				<input type="text" name="amount_upto" id="amount_upto" size="20" value="<?=$amount_upto?>" />
				&nbsp;
				<img src="images/info.gif" border="0" title="Amount Upto | If you want to setup the fee range for which there should be a specific fee than use this field. Otherwise leave it blank. Please note that the fee having the range has the highest priority over the fee without 'Amount Upto' field entry, on the same 'Amount From' value." />
				</td>
		</tr>

		<tr bgcolor="#ededed">
			<td width="40%" class="tdDefination"><b>Fee:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="60%"><input type="text" name="cheque_fee" id="cheque_fee" size="20" value="<?=$cheque_fee?>" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" name="storeData" value="Submit" />
				&nbsp;&nbsp;
				<input type="reset" value="Clear Fields" />
				<input type="hidden" name="fdi" value="<?=$fdi?>" />
				<input type="hidden" name="cmd" value="<?=$cmd?>" />
				<? if(!empty($fdi)) { ?>
					&nbsp;&nbsp;<a href="<?=$_SERVER['PHP_SELF']?>">Add New One</a>
				<? } ?>
			</td>
		</tr>
	</table>
</form>
<br /><br />
<fieldset>
	<legend style="color:#666666; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:18px; font-weight:bold">
		&nbsp;AVAILABLE CHEQUE ORDER FEES&nbsp;
	</legend>
	<table width="95%" align="center" border="0" class="feeList" cellpadding="2" cellspacing="2">
		<tr>
			<th width="20%">Fee Type</th>
			<th width="10%">Fee</th>
			<th width="10%">Amount From</th>
			<th width="15%">Amount Upto</th>
			<th width="10%">Created By</th>
			<th width="10%">Created On</th>
			<th width="15%">Updated On</th>
			<th>Actions</th>
		</tr>
		<?php
			foreach($arrAllFeesData as $feeVal)
			{
				$userData = explode("|",$feeVal["created_by"]);
				$arrUserResultSet = array();
				if(!empty($userData[0]))
					$arrUserResultSet = selectFrom("select name, username from admin where userid='".$userData[0]."'");
					
				$strFeeType = "";
				if($feeVal["type"] == "F")
					$strFeeType = "Fixed Amount";
				elseif($feeVal["type"] == "P")
					$strFeeType = "Percentage of Amount";
				elseif($feeVal["type"] == "C")
				{
					$strFeeType = "Company Based Fee";
					if(!empty($feeVal["type_value"]))
					{
						$arrCompanyNameResultSet = selectFrom("select name from company where company_id='".$feeVal["type_value"]."'");
						$strFeeType .= "<br /><b>".$arrCompanyNameResultSet["name"]."</b>";
					}
				}
				
				if($strClass == "firstRow")
					$strClass = "secondRow";
				else
					$strClass = "firstRow";
		?>
		<tr class="<?=$strClass?>">
			<td><?=$strFeeType?></td>
			<td><?=$feeVal["fee"]?></td>
			<td><?=$feeVal["amount_from"]?></td>
			<td><?=$feeVal["amount_upto"] == 0.00 ?"No upper limit defined.":$feeVal["amount_upto"]?></td>
			<td><?=$arrUserResultSet["name"]." [".$arrUserResultSet["username"]."] / ".$userData[1]?></td>
			<td><?=date("d/m/Y",$feeVal["created_on"])?></td>
			<td><?=date("d/m/Y H:i:s",$feeVal["updated_on"])?> GMT</td>
			<td>
				<a href="<?=$_SERVER['PHP_SELF']?>?fdi=<?=$feeVal["fee_id"]?>&cmd=EDIT">
					<img src="images/edit_th.gif" border="0" title="Edit Fee| You can edit the record by clicking on this thumbnail." />
				</a>&nbsp;&nbsp;
				<a href="<?=$_SERVER['PHP_SELF']?>?fdi=<?=$feeVal["fee_id"]?>&cmd=DEL">
					<img src="images/remove_tn.gif" border="0" title="Remove Fee|By clicking on this the fee will be no longer available." />
				</a>
			</td>
		</tr>
				
		<?
			}
		?>
	</table>
</fieldset>

</body>
</html>