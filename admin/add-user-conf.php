<?php
// Session Handling
session_start();

// Including files
include ("../include/config.php");
include ("security.php");
///////////////////////History is maintained via method named 'activities'

if ($_POST["userID"] == ""){
	session_register("group");
	session_register("username");
	session_register("name");
	$_SESSION["username"] = $_POST["username"];
	$_SESSION["name"] = $_POST["name"];
	$_SESSION["group"] = $_POST["group"];
	$backURL = "add-user.php?msg=Y";
} else {
	$backURL = "update-user.php?userID=$_POST[userID]&msg=Y";
}
if (trim($_POST["username"]) == ""){
	insertError(GR5);
	redirect($backURL);
}
if (trim($_POST["password"]) == "" || trim($_POST["cpassword"]) == ""){
	insertError(GR6);
	redirect($backURL);
}
if (trim($_POST["password"]) != trim($_POST["cpassword"])){
	insertError(GR7);
	redirect($backURL);
}
if ($_POST["userID"] == ""){
	$Querry_Sqls = "insert into ".TBL_ADMIN_USERS." (groupID, username, name, password) values ('".$_POST["group"]."', '".checkValues($_POST["username"])."', '".checkValues($_POST["name"])."', '".$_POST["password"]."')";
	insertInto($Querry_Sqls);
	
	$insertedID = @mysql_insert_id();
	$descript = "User is added with the name ".$_POST["username"]; 
	activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,TBL_ADMIN_USERS,$descript);
	
	$_SESSION["group"] = "";
	$_SESSION["username"] = "";
	$_SESSION["name"] = "";
	insertError(GR8);
} else {
	if ($_POST["oldpassword"] == $_POST["password"])
		$Querry_Sqls = "update ".TBL_ADMIN_USERS." set groupID='".$_POST["group"]."', username='".checkValues($_POST["username"])."', name='".checkValues($_POST["name"])."', password='".$_POST["password"]."' where userID='".$_POST["userID"]."'";
	else
		$Querry_Sqls = "update ".TBL_ADMIN_USERS." set groupID='".$_POST["group"]."', username='".checkValues($_POST["username"])."', name='".checkValues($_POST["name"])."', password='".$_POST["password"]."' where userID='".$_POST["userID"]."'";
	update($Querry_Sqls);
	
	$descript = "User is updated with the name ".$_POST["username"]; 
	activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["userID"],TBL_ADMIN_USERS,$descript);
	insertError(GR9);
}
redirect($backURL);
?>