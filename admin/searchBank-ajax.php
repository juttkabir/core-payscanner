<?php
	
	//ini_set('display_errors','On');
	error_reporting(1);
	include ("../include/config.php");
	include ("security.php");
	//debug("a");
	
	if($_REQUEST["getDetails"] == "banks")
	{ 	
		if(!empty($_REQUEST["bankId"]))
			$bankId=$_REQUEST["bankId"];
				
		if($_REQUEST["searchTypeV"]==0)
		{
		
				$bankSQL = "select 
									bnk.id as bnkId, 
									bnk.name, 
									
									benBank.*
							FROM 
									banks as bnk, 
									ben_banks as benBank
							 WHERE 
									bnk.id='".$bankId."' 
									AND bnk.id=benBank.bankId 
									AND benBank.benId='".$_REQUEST["BenID"]."'
									AND benBank.id='".$_REQUEST["BenAccNo"]."'";
				/*if(!empty($_REQUEST["TTtransId"]))
				{
					$ttAcccNo =selectFrom(" SELECT  
												accNo 
										  	FROM
												TTbankDetails
											WHERE
												transID='".$_REQUEST["TTtransId"]."'");
					$bankSQL .=" AND benBank.accountNo='".$ttAcccNo["accNo"]."'";	
				}*/
				//debug($bankSQL,true);	 
		}
			
		else
		{
			/*$bankSQL = "select 
							bnk.id, 
							bnk.name, 
							
							benBank.*
						FROM banks as bnk 
						LEFT JOIN ben_banks as benBank ON bnk.id=benBank.bankId 
						WHERE bnk.id='".$bankId."' ";	*/
			$bankSQL = "select 
							bnk.id, 
							bnk.name
						FROM banks as bnk 
						WHERE bnk.id='".$bankId."' ";	
		}
		
					
		$arrBanksData = selectFrom($bankSQL);
		//debug($bankSQL,TRUE);
		//debug($arrBanksData);
		
		
		?>
		<table width="100%" border="0" cellspacing="1" cellpadding="2" align="center" style="font-size:10px">
			<tr bgcolor="#ededed">
				<td align="left" style="font-size:11px"><b>Bank Name:&nbsp;</b></td>
				<td align="left" style="font-size:10px"><input type="text" name="BankNam" id="BankNam"
				value="<?=trim($arrBanksData["name"])?>"  readonly="readonly"/></td>
			</tr>
			
			<tr bgcolor="#ededed">
				<td align="left" style="font-size:11px"><b>Branch Code:&nbsp;</b></td>
				<td align="left" style="font-size:10px"><input type="text" name="BrCode" id="BrCode"
				value="<? if($_REQUEST["BenID"] ==$arrBanksData["benId"])
							echo $arrBanksData["branchCode"]?>" <?php //echo $readonly?> autocomplete="off"/></td>
				
			</tr>
			
			
			<tr bgcolor="#ededed">
				<td align="left" style="font-size:11px"><b>Branch Address:&nbsp;</b></td>
				<td align="left" style="font-size:10px"><input type="text" name="BrAdd" id="BrAdd"
				value="<? if($_REQUEST["BenID"] ==$arrBanksData["benId"]) 
							echo $arrBanksData["branchAddress"]?>" <?php //echo $readonly?> autocomplete="off"/></td>
				
			</tr>
			
			
			<tr bgcolor="#ededed">
				<td align="left" style="font-size:11px"><b>Account Number:&nbsp;</b></td>
				<td align="left" style="font-size:10px"><input type="text" name="AccNum" id="AccNum" 
				value="<? if($_REQUEST["BenID"] ==$arrBanksData["benId"])
							echo $arrBanksData["accountNo"]?>" <?php //echo $readonly?> autocomplete="off"/></td>
				
			</tr>
			<?php if((empty($_REQUEST["BenAccNo"]) || (!empty($_REQUEST["BenAccNo"]) && $_REQUEST["BenID"] !=$arrBanksData["benId"]))) { //debug($_REQUEST["notBenBank"]."~".$_REQUEST["BenID"]."**".$arrBanksData["benId"]);
			?>
			<tr bgcolor="#ededed">
				<td align="center" id="addBank">
				<a class="addUpdateBank" href="#" onclick="AddBank('<?=$_REQUEST["BenID"]?>','<?=$_REQUEST["bankId"]?>')" id="addEvent">ADD ACCOUNT</a>
				</td>
				<td align="right" id="addBankMsg" style="display:none; color:#0033FF; font-size:10px">
				Account Information Added!
				</td>	
				<td align="center" id="upBankMsg" style="display:none; color:#0033FF; font-size:10px">
					<a class="addUpdateBank" href="#" onclick="UpdateBank('<?=$arrBanksData["id"]?>')" id="upBankEvent" style="display:none">UPDATE ACCOUNT</a>
				</td>
				
		
			</tr>
		<?php }
		else
		{?>
		
		<tr bgcolor="#ededed">
			<td align="center" id="updateBank">
				<a  class="addUpdateBank" href="#" onclick="UpdateBank('<?=$arrBanksData["id"]?>')">UPDATE ACCOUNT</a>
			</td>
			
			<td align="right" id="upBankMsg" style="display:none; color:#0033FF; font-size:10px">
				Account Information Updated!
			</td>	
		</tr>
		<?php }?>
		<input type="hidden" name="benAccount" id="benAccount" value="<?=$_REQUEST["benAccount"]?>" />
		</table>
		
		<?php
	   
		exit;
		
	 }
	 
	if($_REQUEST["addBank"]=="Y")
	{ 
	 
		 //Duplicate check
			$qrSQL = "select 
							bnk.bankId
						FROM 
							ben_banks as bnk 
						WHERE 
							bnk.benId='".$_REQUEST["BenID"]."' AND
							bnk.bankId='".$_REQUEST["bankid"]."' AND
							bnk.accountNo='".$_REQUEST["AccNum"]."' AND
							bnk.branchCode='".$_REQUEST["BrCode"]."'";	
		
		
					
			$arrDuplicateAccCheck = selectFrom($qrSQL);
			if(empty($arrDuplicateAccCheck) && !empty($_REQUEST["AccNum"]))
			{
			//debug($_REQUEST);
				$arrBankData = array();
				$arrBankData["benId"]        		  		= $_REQUEST["BenID"];
				$arrBankData["bankId"] 		    		  	= $_REQUEST["bankid"];
				$arrBankData["accountNo"]      				= $_REQUEST["AccNum"];
				
				$arrBankData["branchCode"]           		= $_REQUEST["BrCode"];
				
				$arrBankData["branchAddress"]             	= $_REQUEST["BrAdd"]; 
				//$arrBankData["IBAN"]   					= $_REQUEST["receivingAc"];
				
				
				$NewBankId = dataInsertionOperation($arrBankData, 'ben_banks');
			
				echo $NewBankId; 
			}
		exit;
		
	}		
	 
	if($_REQUEST["updateBank"]=="Y")
	{ 
	
		$BenBankID=$_REQUEST["BenBankID"];

		$arrBankData = array();

		$arrBankData["accountNo"]      				= $_REQUEST["AccNum"];
		
		$arrBankData["branchCode"]           		= $_REQUEST["BrCode"];
		
		$arrBankData["branchAddress"]             	= $_REQUEST["BrAdd"]; 
		
		
	
		$UpBankId = dataInsertionOperation($arrBankData, 'ben_banks', '',"UPDATE", "id='$BenBankID'");
		//debug($NewBankId); 
		echo $UpBankId;
		exit;
		
	}				
		
	$searchType = strtolower($_REQUEST['searchType']);
	$benID		= strtolower($_REQUEST['benID']);
	//debug($_REQUEST);
	$q = strtolower($_REQUEST["q"]);
	if (!$q) return;
	
	$whrClause =  " AND b.name like '".$q."%' ";
	
	// for existing beneficiary banks::::changed bb.bankId as bankId to bb.id as bankId,
	if($searchType == 0){
		$sql 	= "select 
						b.id,  
						b.name ,
						
						bb.id as bankId,
						bb.accountNo
					FROM
		 				 banks as b, 
						ben_banks as bb  
					WHERE 1 
					AND bb.benId = '".$benID."' 
					AND b.id=bb.bankId ";
		$sql	.= $whrClause;
		//debug($sql);
		$arrBankRows = selectMultiRecords($sql);
			//debug($arrBankRows);
	}		
	else{
		// for new beneficiary banks
		$sql = "select 
						b.id,  
						b.name
					FROM
						 banks as b 
					WHERE 1 ";
		$sql	.= $whrClause;
		$arrBankRows = selectMultiRecords($sql);
	}
	
	//debug($sql);
	   
	
	//debug($arrBankRows);
	foreach($arrBankRows as $key => $val) {
		if($searchType == '0')
			$retStr=trim($val["name"]."[".$val["accountNo"]."]")."|".$val["id"]."|".$val["bankId"];			
		else
			$retStr=trim($val["name"])."|".$val["id"];	
		echo $retStr."\r\n";		
	}
	
	exit;
//////////////
