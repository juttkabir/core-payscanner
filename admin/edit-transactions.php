<?
//session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$agentID = $_SESSION["loggedUserData"]["userID"];
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
	$offset = 0;
	
if($limit == 0)
	$limit=50;

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}

$_SESSION["back"] = "";

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";
	
$moneyPaid = "";
/**
 *  @var $flag type boolean
 *  This varibale is used to check the status ( enable,disable ) of the config
 *  CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS 
 */
$flag = false ;
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

			$_SESSION["transType"] 		= "";
			$_SESSION["benAgentID"] 	= "";
			$_SESSION["customerID"] 	= "";
			$_SESSION["benID"] 			  = "";
			$_SESSION["moneyPaid"] 		= "";
			$_SESSION["transactionPurpose"] = "";
			$_SESSION["other_pur"] = "";
			$_SESSION["fundSources"] 	= "";
			$_SESSION["refNumber"] 		= "";
	
			$_SESSION["transAmount"] 	= "";
			$_SESSION["totalAmount"] 	= "";
			$_SESSION["exchangeRate"] 	= "";
			$_SESSION["exchangeID"] 	= "";
			$_SESSION["localAmount"] 	= "";
			$_SESSION["IMFee"] 			= "";
	
			// resetting Session vars for trans_type Bank Transfer
			$_SESSION["bankName"] 		= "";
			$_SESSION["branchCode"] 	= "";
			$_SESSION["branchAddress"] 	= "";
			$_SESSION["swiftCode"] 		= "";
			$_SESSION["accNo"] 			= "";
			$_SESSION["ABACPF"] 		= "";
			$_SESSION["IBAN"] 			= "";
			
			$_SESSION["question"] 			= "";
			$_SESSION["answer"]     = "";	
			$_SESSION["tip"]     = "";	
			$_SESSION["currencyCharge"] = "";
			$_SESSION["transDate"]="";
	
/**
 *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements 
 *  This condition executes when admin manager or branch manager logs in,
 *  keeping the check of showing the logged in users only the transactions of 
 *  their associated senders.
 */
if( defined("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS") && strstr(CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS,$agentType) )
{
   /**
	*  @var $flag type boolean
	*  This varibale is used to check the status ( enable,disable ) of the config
	*  CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS 
	*/
	$flag      = true;
   /**
	*  @var $extraCondition type string
	*  This varibale is used to define a condition 
	*/
	$extraCondition  = " and ( am.userID = '".$agentID."' ) ";
}
	
$acManagerFlagLabel = "Account Manager";
$acManagerFlag = false;
if(CONFIG_POPULATE_USERS_DROPDOWN=="1" && defined("CONFIG_ACCOUNT_MANAGER_COLUMN") && CONFIG_ACCOUNT_MANAGER_COLUMN!="0"){
	$custExtraFields = ", c.parentID as custParentID, am.name as accountManagerName ";
	$acManagerFlagLabel = CONFIG_ACCOUNT_MANAGER_COLUMN;
	$extraJoin = " LEFT JOIN ".TBL_ADMIN_USERS." as am ON am.userID = c.parentID  ";
	$acManagerFlag = true;
}

$chequeAmountField = "";
if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1 )
{
	$chequeAmountFieldWitTableAlias = "t.chequeAmount,";
	$chequeAmountField = "chequeAmount,";
}	



if($_POST["Submit"] == "Search" || $_GET["search"] == "Search")
{
	$queryCnt = "select count(transID) 
		from ". 
			TBL_TRANSACTIONS . " as t
			LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
			$extraJoin."  
			WHERE 1 ".$extraCondition;

	$query = "select t.transID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.trans_source,t.transAmount,t.totalAmount,t.custAgentID, " . $chequeAmountFieldWitTableAlias . " t.benAgentID ".$custExtraFields."
		from ". 
			TBL_TRANSACTIONS . " as t
			LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
			$extraJoin."  
			WHERE 1 ".$extraCondition;
	$search = "Search";
	if($_POST["transID"] != "")
	{
		if($_POST["transID"] != "")
			{
				$id = $_POST["transID"];
			}
		if($_POST["searchBy"] != "")
			{
			$by = $_POST["searchBy"];
			}
	}
	
	if ($_POST["moneyPaid"] != "") {
		$moneyPaid = $_POST["moneyPaid"];
	} else if ($_GET["moneyPaid"] != "") {
		$moneyPaid = $_GET["moneyPaid"];
	}
	
	if($_REQUEST["transID"] != "")
	{
		$id = $_REQUEST["transID"];
	}
	if($_REQUEST["searchBy"] != "")
	{
		$by = $_REQUEST["searchBy"];
	}
	if($_REQUEST["nameType"] != "")
	{
		$nameType = $_REQUEST["nameType"];
	}
	if($_REQUEST["transID"] != "")
	{
		$transID = $_REQUEST["transID"];
	}
	if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
	{
		if($_REQUEST["transSource"]!="")
		{
			$transSource = $_REQUEST["transSource"];
		
		}
	}

if(!empty($_REQUEST["customerParentID"]))
	$customerParentID = $_REQUEST["customerParentID"];
	
	
	if($id != "")
	{
		switch($by)
		{
			case 0:
			{
				$query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.trans_source,t.transAmount,t.totalAmount, " . $chequeAmountFieldWitTableAlias . " t.benAgentID ".$custExtraFields." 
				from 
					". TBL_TRANSACTIONS . " as t
					LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
					$extraJoin."  
					 where (t.refNumber = '$id' OR  t.refNumberIM = '$id') ".$extraCondition;
				$queryCnt = "select count(transID)  
					from 
						". TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
						$extraJoin."  
						 where (t.refNumber = '$id' OR  t.refNumberIM = '$id') ".$extraCondition;
				break;
			}
			case 1:
			{
				
				
				if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){
					
					
								if($nameType == "fullName"){
									$name = split(" ",$transID);
									$nameClause = "c.firstName LIKE '$name[0]' and c.lastName LIKE  '$name[1]' ";
									$nameClauseOn = "firstName LIKE '$name[0]' and lastName LIKE  '$name[1]' ";
								}else{
									$nameClause = " c.".$nameType." LIKE '".$transID."%'";
									$nameClauseOn = " ".$nameType." LIKE '".$transID."%'";
								}
					
						 $query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,
						 t.trans_source,t.transStatus,t.transAmount,t.totalAmount, " . $chequeAmountFieldWitTableAlias . " t.benAgentID ".$custExtraFields."
								from 
									". TBL_TRANSACTIONS . " as t
									LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
									$extraJoin."  WHERE 1 
									 and ".$nameClause." ".$extraCondition;
						$queryCnt = "select count(transID) 
								from 
									". TBL_TRANSACTIONS . " as t
									LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
									$extraJoin."  WHERE 1 
									 and ".$nameClause." ".$extraCondition;
				
					}else{
				
				// searchNameMultiTables function start (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="c";
			 $q=searchNameMultiTables($id,$fn,$mn,$ln,$alis);
			  	
			  // searchNameMultiTables function end (Niaz)
			  
				//$query = "select *  from ". TBL_TRANSACTIONS . " t, ". TBL_CUSTOMER ." c where t.customerID =  c.customerID and (c.firstName like '$id%' OR c.lastName like '$id%') and t.createdBy != 'CUSTOMER' ";
				//$queryCnt = "select count(*)  from ". TBL_TRANSACTIONS . " t, ". TBL_CUSTOMER ." c where t.customerID =  c.customerID and (c.firstName like '$id%' OR c.lastName like '$id%') and t.createdBy != 'CUSTOMER' ";
				
			 $query = "select t.transID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.trans_source,
			 			t.transAmount,t.totalAmount,t.custAgentID, " . $chequeAmountFieldWitTableAlias . " t.benAgentID ".$custExtraFields."
							from 
								". TBL_TRANSACTIONS . " as t
								LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
									$extraJoin."  WHERE 1  
								 ".$q." ".$extraCondition;
	 
			$queryCnt = "select count(transID) 
							from 
								". TBL_TRANSACTIONS . " as t
								LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
									$extraJoin."  WHERE 1 
								 ".$q." ".$extraCondition;
			}
				
				
				break;
			}
			case 2:
			{
				
				
				if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){

					if($nameType == "fullName"){
						$name = split(" ",$transID);
						$nameClause = "b.firstName LIKE '$name[0]' and b.lastName LIKE  '$name[1]' ";
						$nameClauseOn = "firstName LIKE '$name[0]' and lastName LIKE  '$name[1]' ";
					}else{
						$nameClause = " b.".$nameType." LIKE '".$transID."%'";
						$nameClauseOn = " ".$nameType." LIKE '".$transID."%'";
					}
					
						$query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.trans_source
							t.transAmount,t.totalAmount,t.benID, " . $chequeAmountFieldWitTableAlias . " t.benAgentID ".$custExtraFields."
								from 
									". TBL_TRANSACTIONS . " as t
									LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
									LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
									$extraJoin."  WHERE 1 
									 and ".$nameClause." ".$extraCondition;
						$queryCnt = "select count(transID) 
									from 
										". TBL_TRANSACTIONS . " as t
										LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
										LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
										$extraJoin."  WHERE 1 
										 and ".$nameClause." ".$extraCondition;
				
					}else{
				
				// searchNameMultiTables function start (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="b";
			 $q=searchNameMultiTables($id,$fn,$mn,$ln,$alis);
			 
				
				$query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.trans_source,t.transAmount,t.totalAmount, " . $chequeAmountFieldWitTableAlias . " t.benAgentID ".$custExtraFields."
								from 
									". TBL_TRANSACTIONS . " as t
									LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
									LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
									$extraJoin."  WHERE 1  
									".$q." ".$extraCondition;
				$queryCnt = "select count(transID) 
								from 
									". TBL_TRANSACTIONS . " as t
									LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
									LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
									$extraJoin."  WHERE 1 
									".$q." ".$extraCondition;
				
				//$query = "select *  from ". TBL_TRANSACTIONS . " t, ". TBL_BENEFICIARY ." b where t.benID =  b.benID and (b.firstName like '$id%' OR b.lastName like '$id%') and t.createdBy != 'CUSTOMER' ";
				//$queryCnt = "select count(*)  from ". TBL_TRANSACTIONS . " t, ". TBL_BENEFICIARY ." b where t.benID =  b.benID and (b.firstName like '$id%' OR b.lastName like '$id%') and t.createdBy != 'CUSTOMER' ";
			}
				
				
				break;
			}
			case 3:
			{
				/*$query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.totalAmount, " . $chequeAmountFieldWitTableAlias . " t.benAgentID  from ". TBL_TRANSACTIONS . " t, ". TBL_ADMIN_USERS ." u where t.custAgentID =  u.userID and u.name like '$id%' ".$extraCondition;
*/				
				$query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.trans_source,t.transAmount,t.totalAmount, " . $chequeAmountFieldWitTableAlias . " t.benAgentID ".$custExtraFields."
								from 
									". TBL_TRANSACTIONS . " as t
									LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
									LEFT JOIN ". TBL_ADMIN_USERS ." as u ON t.custAgentID =  u.userID ".
									$extraJoin."  
									WHERE u.name like '$id%' ".$extraCondition;
				$queryCnt = "select count(transID) 
								from 
									". TBL_TRANSACTIONS . " as t
									LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
									LEFT JOIN ". TBL_ADMIN_USERS ." as u ON t.custAgentID =  u.userID ".
									$extraJoin."  
									WHERE u.name like '$id%' ".$extraCondition;
				break;
			}

		}
		 
			 		
	}
	// Forcefully not displaying Transactions of Online Sender
	$query .= " and t.createdBy != 'CUSTOMER' ";
	$queryCnt .= " and t.createdBy != 'CUSTOMER' ";
	
	if($agentType == "Branch Manager"){
		$query .= " and (custAgentParentID ='$agentID' OR t.benAgentParentID =  $agentID )";
		$queryCnt .= " and (custAgentParentID ='$agentID' OR t.benAgentParentID =  $agentID) ";
	}
	
switch ($agentType)
{
	case "SUPA":
	case "SUPAI":
	case "SUBA":
	case "SUBAI":
		$query .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		$queryCnt .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
}
	
	if ($moneyPaid != "") {
		
		$query .= " and (t.moneyPaid = '".$moneyPaid."')";	
		$queryCnt .= " and (t.moneyPaid = '".$moneyPaid."')";	
	}
	
$query .= " and (t.transStatus ='Pending' or t.transStatus ='Processing')";	
$queryCnt .= " and (t.transStatus ='Pending' or t.transStatus ='Processing')";	

if(!empty($customerParentID)){
	$query .= " and (c.parentID='".$customerParentID."')";
	$queryCnt .= " and (c.parentID='".$customerParentID."')";
}
if($transSource == "O")
{
	$query.= " and trans_source = '".$transSource."' ";
	$queryCnt.= " and trans_source = '".$transSource."' ";

}
elseif($transSource != "O" && $transSource != "")
{
	$query.= " and trans_source != 'O' ";
	$queryCnt.= " and trans_source != 'O' ";

}


$query .= "  order by t.transDate DESC";

$countRec = countRecords($queryCnt);
$totalContent = $countRec;
$query .= " LIMIT $offset , $limit";

$contentsTrans = selectMultiRecords($query);
if($_GET["allCount"] == ""){
	$allCount = $countRec;
}else{
	$allCount = $_GET["allCount"];
}

$rangeOffset = count($contentsTrans)+$offset;

//echo(" Agents are -->".count($totalContent)."--");
/* 
 $other = $offset + $limit;
 if($other > $countOnlineRec)
 {
	if($offset < $countOnlineRec)
	{
		$offset2 = 0;
		$limit2 = $offset + $limit - $countOnlineRec;	
	}elseif($offset >= $countOnlineRec)
	{
		$offset2 = $offset - $countOnlineRec;
		$limit2 = $limit;
	}
  $query .= " LIMIT $offset2 , $limit2";
	$contentsTrans = selectMultiRecords($query);
 }*/
}else{
	$queryCnt = "select count(transID) 
								from 
									". TBL_TRANSACTIONS . " as t
									LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
									$extraJoin."  
									WHERE 1 ".$extraCondition;
	$query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.trans_source
						,t.totalAmount, " . $chequeAmountField . " t.benAgentID  ".$custExtraFields." 
								from 
									". TBL_TRANSACTIONS . " as t
									LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
									$extraJoin."  
									WHERE 1 ".$extraCondition;
	
	
	if($agentType == "Branch Manager")
	{
    $query .= " and (custAgentParentID ='$agentID' OR benAgentParentID =  $agentID) ";
    $queryCnt .= " and (custAgentParentID ='$agentID' OR benAgentParentID =  $agentID) ";
		   
	}
	switch ($agentType)
	{
		case "SUPA":
		case "SUPAI":
		case "SUBA":
		case "SUBAI":
			$query .= " and (custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
			$queryCnt .= " and (custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		
	}
	if($transSource == "O")
	{
		$query.= " and trans_source = '".$transSource."' ";
		$queryCnt.= " and trans_source = '".$transSource."' ";
	
	}
	elseif($transSource != "O" && $transSource != "")
{
	$query.= " and trans_source != 'O' ";
	$queryCnt.= " and trans_source != 'O' ";

}
	$query .= " and (createdBy != 'CUSTOMER') ";
	$queryCnt .= " and (createdBy != 'CUSTOMER') ";
	
	$queryCnt .= " and (transStatus ='Pending' or t.transStatus ='Processing') ";	
	$query .= " and (transStatus ='Pending' or t.transStatus ='Processing')";	
	
	$allCount = countRecords($queryCnt);
	
	$query .= " order by transDate DESC";
  	$query .= " LIMIT $offset , $limit";

	$contentsTrans = selectMultiRecords($query);
	$rangeOffset = count($contentsTrans)+$offset;	
}

?>
<html>
<head>
	<title>Update Transactions</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}	
-->
</script>


<script language="JavaScript">
<!--

function nameTypeCheck(){
	
	
			
			
			
			if(document.getElementById('searchBy').value == '1' || document.getElementById('searchBy').value == '2'){
					
				
				document.getElementById('nameTypeRow').style.display = '';
			}	else{
				
				
				document.getElementById('nameTypeRow').style.display = 'none';
				}
			
}

-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {
	color: #005B90;
	font-weight: bold;
}
-->
    </style>
</head>
<body onLoad="nameTypeCheck();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong ><font color="#FFFFFF" size="2">Update Transactions</font></strong></td>
  </tr>
  <tr>
		<td>
			<table width="255" border="1" cellpadding="5" bordercolor="#666666" align="center">
        <form action="edit-transactions.php" method="post" name="Search">
          <tr>
            <td width="286" bgcolor="#C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
          </tr>
          <tr>
            <td align="center" nowrap>
            	<input name="transID" type="text" id="transID" value="<?=$id?>" size="15" style="font-family:verdana; font-size: 11px; width:100"> 
              <select name="searchBy" id="searchBy" onChange="nameTypeCheck();">
                  <option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
                  <option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By <?=__("Sender");?> Name</option>
                  <option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
						<?	if($agentType == "admin" || $agentType == "System" || $agentType == "Call" || $agentType == "Admin Manager")
							{
							?>
							 	<option value="3" <? echo ($_POST["searchBy"] == 3 ? "selected" : "")?>>By <?=__("Agent");?> Name</option>
							<?
							}
						?>
              </select>
		<?	if (CONFIG_PAYMENT_MODE_FILTER == "1") {  ?>
			<br>Payment Mode
      <select name="moneyPaid" id="moneyPaid" style="font-family:verdana; font-size: 11px;">
          <option value="">- Select Mode -</option>
          <option value="By Cash" <? echo ($moneyPaid == "By Cash" ? "selected" : "") ?>>By Cash</option>
          <option value="By Cheque" <? echo ($moneyPaid == "By Cheque" ? "selected" : "") ?>>By Cheque</option>
          <option value="By Bank Transfer" <? echo ($moneyPaid == "By Bank Transfer" ? "selected" : "") ?>>By Bank Transfer</option>
		  </select>
		<?	}  ?>
		 <? if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
		{ 
		
		?>
            <select name="transSource">
                <option value=""> - Transaction Source - </option>
                <option value="O">Online</option>
                <option value="P">Payex</option>
                
            
            </select>
            <script language="JavaScript">SelectOption(document.forms[0].transSource, "<?=$transSource?>");</script>
            <? } ?>
		
			</td>
</tr>
	   <?php
	   	/** 
		 *  This condition executes when CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS is 
		 *  not defined,disabled or the logged in user is not the one defined in this config 
		 */
		 

//work by Mudassar Ticket #11425
//debug(CONFIG_POPULATE_USERS_DROPDOWN_SENDER);	
	if(	$flag == false )
		{
			
	//work  by Mudassar Ticket #11425	
			if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){	
					$agentStatusQuery = selectFrom("select agentStatus from ".TBL_ADMIN_USERS." where agentStatus = 'Active' ");
					$arguments = array("flag"=>"populateUsers","selectedID"=>$customerParentID,"hideSelectMain"=>true,"agentStatusQuery"=>$agentStatusQuery);
					}
					
			else if (CONFIG_AGENT_WITH_ALL_STATUS==1){	
	
					$arguments = array("flag"=>"populateUsers","selectedID"=>$customerParentID,"hideSelectMain"=>true);
					}
							
	//work end by Mudassar Ticket #11425
			
			$populateUser = gateway("CONFIG_POPULATE_USERS_DROPDOWN_SENDER",$arguments,CONFIG_POPULATE_USERS_DROPDOWN_SENDER);
			if(!empty($populateUser)){
		?>
			<tr align="center"><td>Select <?=$acManagerFlagLabel?>&nbsp;&nbsp;
				<select name="customerParentID" id="customerParentID" style="font-family:verdana; font-size: 11px">
				<option value="">-- Select <?=$acManagerFlagLabel?> --</option>
					<?php echo $populateUser;?>
				  </select>&nbsp;&nbsp;
				 </td></tr>
			<? 
			}
	//debug($populateUser);
	//work end by Mudassar Ticket #11425	
		}
		if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){ ?>
   	
			   	<tr align="center" id="nameTypeRow"><td>
			   		Name Type: <select name="nameType">
			   			<option value="firstName" <? echo ($nameType == "firstName" ? "selected" : "")?>>First Name</option>
			   			<option value="lastName" <? echo ($nameType == "lastName" ? "selected" : "")?>>Last Name</option>
			   			<option value="fullName" <? echo ($nameType == "fullName" ? "selected" : "")?>>First Name & Last Name</option>
						</select>
   		
  	</td></tr>
  	<? } ?>
		<tr>
			<td align="center">
		
                <input type="submit" name="Submit" value="Search"></td>
          </tr>
        </form>
      </table>
		</td>
	</tr>
  
  
  <tr>
    <td align="center">

      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
        <form action="edit-transactions.php?" method="post" name="trans">
			<tr>
    		<td>
    			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td width="250">
                    <?php if (count($contentsTrans) > 0) {?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($rangeOffset);?></b>
                    of
                    <?=$allCount; ?>
                  </td>
                <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&search=$search&transID=$id&moneyPaid=$moneyPaid&transSource=$transSource&searchBy=$by&allCount=$allCount&nameType=".$nameType."&customerParentID=$customerParentID&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&search=$search&transID=$id&moneyPaid=$moneyPaid&transSource=$transSource&searchBy=$by&nameType=".$nameType."&customerParentID=$customerParentID&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>
                  </td>
				  				
                <?php } ?>
                <?php
								if ( ($nxt > 0) && ($nxt < $allCount) ) {
									$alloffset = (ceil($allCount / $limit) - 1) * $limit;
								?>
                  <td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&search=$search&transID=$id&moneyPaid=$moneyPaid&transSource=$transSource&searchBy=$by&allCount=$allCount&nameType=".$nameType."&customerParentID=$customerParentID&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&search=$search&transID=$id&moneyPaid=$moneyPaid&transSource=$transSource&searchBy=$by&allCount=$allCount&nameType=".$nameType."&customerParentID=$customerParentID&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
				 					
                  <?php } 
                	}
                  ?>
                </tr>
              </table>
    		</td>
    	</tr>
	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo(count($contentsTrans))?> records to Update.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?> </span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Transaction Source</span></td>
			  <td><span class="style1">Status</span></td>
				<?
					if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
					{
						?>
							<td><span class="style1">Cash Amount</span></td>
							<td><span class="style1">Cheque Amount</span></td>
						<?
					}
				?>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			  <td width="100"><span class="style1"><?=__("Agent");?> Name</span></td>
			<?php if($acManagerFlag){?>
				<td><span class="style1"><?=__("Sender");?> Name </span></td>
				<td width="80"><span class="style1"><b><?=$acManagerFlagLabel?></b> </span></td>
			<?php }?>
			  <td width="50" align="center">&nbsp;</td>
			  </tr>
		    <? for($i=0;$i < count($contentsTrans);$i++)
			{
				?>
				<? 
				if(CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER == "1"){
					$customerDetails = selectFrom("select customerStatus from ".TBL_CUSTOMER." where customerID = ".$contentsTrans[$i]["customerID"]."");
						$onlineCustomerDetails = selectFrom("select status from cm_customer where c_id = ".$contentsTrans[$i]["customerID"]."");
				
								if ($customerDetails["customerStatus"] == "Disable"){
									
														$fontColor = "#CC000";
														$custStatus = "Disable";
									}elseif ($customerDetails["customerStatus"] == "Enable" || $customerDetails["customerStatus"]== ""){
										
														$fontColor = "#006699";
														$custStatus = "Enable";
										
										}
											
									/*	if ($onlineCustomerDetails["status"] == "Disable"){
									
																$fontColor = "#CC000";
																$custStatus = "Disable";
									}elseif ($onlineCustomerDetails["status"] == "Enable" || $onlineCustomerDetails["status"]=""){
										
																$fontColor = "#006699";
																$custStatus = "Enable";
										
									}*/
									
								}else{
									$fontColor = "#006699";
									}
				
				?>
				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')"><strong><font color="<? echo $fontColor; ?>"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
				  <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
				  <?php if($contentsTrans[$i]["trans_source"] == "O")
					{
					$showTransSource = "Online";
					}
					else
					{
						$showTransSource = "Payex";
					}
					?>
				  <td width="75" bgcolor="#FFFFFF" ><? echo $showTransSource; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				 	<?
						if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
						{
							$cashAmount = $contentsTrans[$i]["totalAmount"] - $contentsTrans[$i]["chequeAmount"];
							$chequeAmt = ($contentsTrans[$i]["chequeAmount"] > 0 ? $contentsTrans[$i]["chequeAmount"] : 0);
							?>
								<td><?=number_format($cashAmount, 4) .  " " . $contentsTrans[$i]["currencyFrom"]; ?></td>
								<td><?=number_format($chequeAmt, 4) .  " " . $contentsTrans[$i]["currencyFrom"];?></td>
							<?
						}
					?>
				  <td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($contentsTrans[$i]["totalAmount"]);?></td>
				  <? $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo ucfirst($agentContent["name"])?></td>
			<?php if($acManagerFlag){?>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<?
				  }else
				  {
				  	if(CONFIG_SHARE_OTHER_NETWORK == '1')
            {    
             $sharedTrans = selectFrom("select * from ".TBL_SHARED_TRANSACTIONS." where localTrans = '".$contentsTrans[$i]["transID"]."' and generatedLocally = 'N'");   
            }   
						if(CONFIG_SHARE_OTHER_NETWORK == '1' && $sharedTrans["remoteTrans"] != '')
						{
							$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$sharedTrans["remoteServerId"]."'");	
							$otherClient = new connectOtherDataBase();
							$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
							
							$customerContent = $otherClient->selectOneRecord("select firstName, lastName from ".$jointClient["dataBaseName"].".customer where customerID = '".$contentsTrans[$i]["customerID"]."'");		
							$beneContent = $otherClient->selectOneRecord("select firstName, lastName from ".$jointClient["dataBaseName"].".beneficiary where benID = '".$contentsTrans[$i]["benID"]."'");		
							$otherClient->closeConnection();
							dbConnect();
						}else{
							$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
							$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
						}
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  	<?
				  	}
				  ?>
				<td width="80"><?=ucfirst($contentsTrans[$i]["accountManagerName"])?></td>
			<?php }?>
				  <? if(CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER && $custStatus == "Disable"){ ?>
												<td width="50" align="center" bgcolor="#FFFFFF">&nbsp;</td>
									<? }else{ ?>
				  							<td width="50" align="center" bgcolor="#FFFFFF"><a href="<?=$transactionPage?>?transID=<? echo $contentsTrans[$i]["transID"]?>&back=edit-transactions.php" class="style2">Update</a></td>
				 	 <? }} ?>
				 	 
			    </tr>
				<?
			}
			?>
					<? for($i=0;$i < count($onlinecustomer);$i++)
					{
					?>
               <tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $onlinecustomer[$i]["transID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumberIM"]?></td>
				  <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($onlinecustomer[$i]["transAmount"])?></td>
				  
				  <? $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$i]["custAgentID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo ucfirst($agentContent["name"])?></td>
				<?php if($acManagerFlag){?>
				  <?
				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
					  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
					  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
					  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
					  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>
					<td width="80"><?=ucfirst($createdBy)?></td>
					<td width="80"><?=ucfirst($contentsTrans[$i]["accountManagerName"])?></td>
				<?php }?>
				  <? if($onlinecustomer[$i]["createdBy"] == "CUSTOMER") {?>
				  	  <td width="50" align="center" bgcolor="#FFFFFF"><a href="add-customer-transaction.php?transID=<? echo $onlinecustomer[$i]["transID"]?>&back=edit-transactions.php" class="style2"><strong><font color="#006699">Update</strong></font></a></td>
					<? }else{ ?>
						<td width="50" align="center" bgcolor="#FFFFFF"><a href="<?=$transactionPage?>?transID=<? echo $onlinecustomer[$i]["transID"]?>&back=edit-transactions.php"><strong><font color="#006699">Update</strong></font></a></td>
						<? 
						}
						?>
			    </tr>
          <?
					}
					?> 
			<tr bgcolor="#FFFFFF">
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td bgcolor="#FFFFFF">&nbsp;</td>
			  <td width="100" align="center">&nbsp;</td>
			  <td width="50" align="center">&nbsp;</td>
			<?php if($acManagerFlag){?>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			<?php }?>
			  <td width="50" align="center">&nbsp;</td>
			  </tr>
			<?
			//} // greater than zero
			?>
          </table></td>
        </tr>
		</form>
    </table></td>
  </tr>

</table>
</body>
</html>
