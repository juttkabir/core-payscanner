<?php

session_start();

include ("../include/config.php");
include ("security.php");

$filters = "";
$agentName = $_REQUEST["agentID"];
/**
 * Date filter
 */
$queryCurrency = $_REQUEST["currency"];
$fromDate = $_REQUEST["fromDate"];
$toDate = $_REQUEST["toDate"];
if($fromDate!="" && $toDate!="")
	$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
$exportType = $_REQUEST["exportType"];
$queryFlag = $_REQUEST["queryFlag"];

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
if(!empty($_REQUEST["pageUrl"]))
	maintainReportLogs($_REQUEST["pageUrl"],'E');
	

if($exportType!=""){
	 	$Balance="";
	 	
	 	if($agentName == "allSUPA")
	 	{
	 		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType='Agent' and isCorrespondent = 'N' and agentType = 'Supper' ";
	 		
			//$accountQuery .= " and $queryDateSUP";				
	 		
	 	}elseif($agentName == "allSUPI")
	 	{
	 		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType='Agent' and isCorrespondent = 'ONLY' and agentType = 'Supper' ";
			//$accountQuery .= " and $queryDateSUP";	
	 		
	 	}elseif($agentName == "allSUBA")
	 	{
	 		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType='Agent' and isCorrespondent = 'N' and agentType = 'Sub' ";
			//$accountQuery .= " and $queryDateSUP";	
	 		
	 	}elseif($agentName == "allSUBI")
	 	{
	 		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType='Agent' and isCorrespondent = 'ONLY' and agentType = 'Sub' ";
			//$accountQuery .= " and $queryDateSUP";	
	 		
	 	}else{
	 		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType = 'Agent' and isCorrespondent != '' ";
		////	$accountQuery .= " and $queryDate";				
			
			if($agentName != "" && $agentName != "all")
			{
				$accountQuery .= " and  userID = '$agentName' ";	
			}
		}
		
	 		$contentsAcc = selectMultiRecords($accountQuery);
	 		$allCount = count($contentsAcc);
}
	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	if($exportType == "xls")
	{
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=AccountSummaryMT-".time().".xls"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
	}
	elseif($exportType == "csv")
	{
		header("Content-Disposition: attachment; filename=AccountSummaryMT-".time().".csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "";
		$strColumnEnd   = ",";

	}
	else
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Export Account Summary</title>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;
	$strFullHtml .= $strRowStart;
// Column Heading starts
	$strFullHtml .= $strColumnStart.$strBoldStart."Agent".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Amount Used(Dr)".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Amount Paid(Cr)".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Opening Balance".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Running Balance".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Closing Balance".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strRowEnd;
// Column Heading ends
	$strFullHtml .= $strRowStart;

	 	 if($_REQUEST["queryFlag"]!="true") { 	
	 		$totalOpeningBalance = 0;
			$totalClosingBalance = 0;
			$totalDailyBalance = 0;
	 		$totalDebit = 0;
	 		$totalCredit = 0;
	 			 
				for($i=0;$i < count($contentsAcc);$i++)
				{
			
						if($contentsAcc[$i]["agentType"] == "Supper")
						{///////Super Agent and Super Distributor
							$summaryTable = TBL_ACCOUNT_SUMMARY;	
							$distributorLedger = TBL_DISTRIBUTOR_ACCOUNT;
							$agentLedger = TBL_AGENT_ACCOUNT;	
						}else{///Sub Agent and Sub Distributor
						 	
						 	$summaryTable = TBL_SUB_ACCOUNT_SUMMARY;		
						 	$distributorLedger = TBL_SUB_DISTRIBUTOR_ACCOUNT;
							$agentLedger = TBL_SUB_AGENT_ACCOUNT;
						 	
						}
								/////////////////					
								//echo "select  Max(dated) as datedFrom from " . $summaryTable . " where user_id = '".$contentsAcc[$i]["user_id"]."' and dated <= '$fromDate'<br>";
								$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . $summaryTable . " where user_id = '".$contentsAcc[$i]["user_id"]."' and dated <= '$fromDate'");
								//print_r($datesFrom);
							 $FirstDated = $datesFrom["datedFrom"];
							$datesTo = selectFrom("select  Max(dated) as datedTo from " . $summaryTable . " where user_id = '".$contentsAcc[$i]["user_id"]."' and dated <= '$toDate'");
							//echo("Date To-->select  Max(dated) as datedTo from " . $summaryTable . " where user_id = '".$contentsAcc[$i]["user_id"]."' and dated <= '$toDate'");
							//print_r($datesTo);
							$LastDated = $datesTo["datedTo"];
							 $openingBalance = 0;
							 $closingBalance = 0;
							 $dailyBalance = 0;
				 			 $Debit = 0;
				 			 $Credit = 0;
						 			
					if(!defined("CONFIG_SWAP_CLOSING_TO_OPENING") || CONFIG_SWAP_CLOSING_TO_OPENING != "1"){	 
							if($FirstDated != "")
							{
								$account1 = "Select opening_balance, closing_balance from ".$summaryTable." where 1";
								$account1 .= " and dated = '$FirstDated'";				
								$account1 .= " and  user_id = '".$contentsAcc[$i]["user_id"]."' ";								
								//echo "<br>".$account1."<br>";
								$contents1 = selectFrom($account1);
								//print_r($contents1);
								if($fromDate == $FirstDated)
									$openingBalance =  $contents1["opening_balance"];
								else
									$openingBalance =  $contents1["closing_balance"];
							}
							
							if($LastDated != "")
							{
								$account2 = "Select opening_balance, closing_balance from ".$summaryTable." where 1";
								$account2 .= " and dated = '$LastDated'";				
								$account2 .= " and  user_id = '".$contentsAcc[$i]["user_id"]."' ";								
								$contents2 = selectFrom($account2);
								
								$closingBalance =  $contents2["closing_balance"];
							}
							$dailyBalance = $closingBalance - $openingBalance;
							
							$totalOpeningBalance += $openingBalance;
							$totalClosingBalance += $closingBalance;
							$totalDailyBalance   += $dailyBalance;	
						 		//////////////////	
						 		if($contentsAcc[$i]["isCorrespondent"] == 'N')
						 		{
						 			 $transBalanceQuery = "Select sum(amount) as amount,aaID, type , description,currency,dated from ".$agentLedger." where agentID = '".$contentsAcc[$i]["user_id"]."' ";	
						 			
						 		}
								elseif($contentsAcc[$i]["adminType"] == 'Agent' and $contentsAcc[$i]["isCorrespondent"] == 'Y') /* A&D*/
								{
									 $transBalanceQuery 	= "Select sum(amount) as amount,aaID, type, description,currency,dated from ".TBL_AnD_ACCOUNT." where agentID = '".$contentsAcc[$i]["user_id"]."' ";
									 $transBalanceQuerySwap = "Select * from ".TBL_AnD_ACCOUNT." where agentID = '".$contentsAcc[$i]["user_id"]."' ";									 
						 		}	
								else /* Distributor */
								{
									$transBalanceQuery = "Select sum(amount) as amount,aaID, type, description,currency,dated from ".$distributorLedger." where bankID = '".$contentsAcc[$i]["user_id"]."' ";	
								}
									if($fromDate!="" && $toDate!=""){
							 			$transBalanceQuery .= " and $queryDate";
										$transBalanceQuerySwap .= " and $queryDate";
									}
									if($queryCurrency!=""){
							 			$transBalanceQuery .= " and currency = '$queryCurrency'";
										$transBalanceQuerySwap.= " and currency = '$queryCurrency'";
									}
										
					 			$transBalanceQuery .= " group by type, description ";
								$transBalance = selectMultiRecords($transBalanceQuery);	

							}
								else if(defined("CONFIG_SWAP_CLOSING_TO_OPENING") && CONFIG_SWAP_CLOSING_TO_OPENING == "1"){

						 		if($contentsAcc[$i]["isCorrespondent"] == 'N')
						 		{
						 			 $transBalanceQuery = "Select sum(amount) as amount,aaID, type , description,currency,dated from ".$agentLedger." where agentID = '".$contentsAcc[$i]["user_id"]."' ";	
						 			 $transBalanceQuerySwap = "Select * from ".$agentLedger." where agentID = '".$contentsAcc[$i]["user_id"]."' ";	
						 			
						 		}
								elseif($contentsAcc[$i]["adminType"] == 'Agent' and $contentsAcc[$i]["isCorrespondent"] == 'Y') /* A&D*/
								{
									 $transBalanceQuery 	= "Select sum(amount) as amount,aaID, type, description,currency,dated from ".TBL_AnD_ACCOUNT." where agentID = '".$contentsAcc[$i]["user_id"]."' ";
									 $transBalanceQuerySwap = "Select * from ".TBL_AnD_ACCOUNT." where agentID = '".$contentsAcc[$i]["user_id"]."' ";									 
						 		}	
								else /* Distributor */
								{
									$transBalanceQuery = "Select sum(amount) as amount,aaID, type, description,currency,dated from ".$distributorLedger." where bankID = '".$contentsAcc[$i]["user_id"]."' ";	
									$transBalanceQuerySwap = "Select * from ".$distributorLedger." where bankID = '".$contentsAcc[$i]["user_id"]."' ";	
								}
									if($fromDate!="" && $toDate!=""){
							 			$transBalanceQuery .= " and $queryDate";
										$transBalanceQuerySwap .= " and $queryDate";
									}
									if($queryCurrency!=""){
							 			$transBalanceQuery .= " and currency = '$queryCurrency'";
										$transBalanceQuerySwap.= " and currency = '$queryCurrency'";
									}
									$transBalance = selectMultiRecords($transBalanceQuerySwap);	
									$account3 = "Select opening_balance, closing_balance from ".$summaryTable." where $queryDate  and  user_id = '".$contentsAcc[$i]["user_id"]."' and currency = '".$_REQUEST["currency"]."' order by id asc";
									$account4 = "Select opening_balance, closing_balance from ".$summaryTable." where $queryDate  and  user_id = '".$contentsAcc[$i]["user_id"]."' and currency = '".$_REQUEST["currency"]."' order by id desc";
									$contents3 = selectFrom($account3);
									$contents4 = selectFrom($account4);
									$openingBalance = $contents3["opening_balance"];
									$closingBalance = $contents4["closing_balance"];

									$dailyBalance = $closingBalance - $openingBalance;
									$totalOpeningBalance += $openingBalance;
									$totalClosingBalance += $closingBalance;
									$totalDailyBalance   += $dailyBalance;	
									if(CONFIG_VIEW_CURRENCY_LABLES == "1"){
									 	 $currSending = " ".$transBalance[0]["currency"];
									} 
								
								}
						 		for($s = 0; $s < count($transBalance); $s++)
						 		{
						 			if($transBalance[$s]["type"] == "DEPOSIT")
									{
										if( $transBalance[$s]["description"]== 'Transaction Cancelled' || $transBalance[$s]["description"] == 'Transaction Amended')
										{
											$Debit = $Debit - $transBalance[$s]["amount"];
											}else{
									  		$Credit = $Credit + $transBalance[$s]["amount"];
										}
										
									}
									  	
									if($transBalance[$s]["type"] == "WITHDRAW")
									{
										if( $transBalance[$s]["description"]== 'Manually Withdrawn')
										{
											$Credit = $Credit - $transBalance[$s]["amount"];
										}else{
									  	$Debit = $Debit + $transBalance[$s]["amount"];
									  }
									  	
									}	
						 		}	
						 		$totalCredit += $Credit;
							  	$totalDebit += $Debit;
			
		if($exportType == "csv")
			$strFullHtml .= "";
		else
			$strFullHtml .= "<tr class='".$contentsAcc[$s]["status"]."'>";

// Data Rows Starts
		$strFullHtml .= $strColumnStart.$contentsAcc[$i]["name"]." [".$contentsAcc[$i]["username"]."]".$strColumnEnd;
		$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($Debit,2,'.',','):number_format($Debit,2,'.','')).$currSending.$strColumnEnd;
		$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($Credit,2,'.',','):number_format($Credit,2,'.','')).$currSending.$strColumnEnd;
		$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($openingBalance,2,'.',','):number_format($openingBalance,2,'.','')).$strColumnEnd;
		$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($dailyBalance,2,'.',','):number_format($dailyBalance,2,'.','')).$strColumnEnd;
		$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($closingBalance,2,'.',','):number_format($closingBalance,2,'.','')).$strColumnEnd;
		$strFullHtml .= $strRowEnd;
// Data Row Ends
	}
}
// Breakline Starts
	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strRowEnd;
// Breakline Ends

// Total Runnig Row starts
	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStart."Total : ".$strColumnEnd;
	$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($totalDebit,2,'.',','):number_format($totalDebit,2,'.','')).$currSending.$strColumnEnd;
	$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($totalCredit,2,'.',','):number_format($totalCredit,2,'.','')).$currSending.$strColumnEnd;
	$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($totalOpeningBalance,2,'.',','):number_format($totalOpeningBalance,2,'.','')).$strColumnEnd;
	$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($totalDailyBalance,2,'.',','):number_format($totalDailyBalance,2,'.','')).$strColumnEnd;
	$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($totalClosingBalance,2,'.',','):number_format($totalClosingBalance,2,'.','')).$strColumnEnd;
	$strFullHtml .= $strRowEnd;
// Total Runnig Row starts
	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlClosing;

	echo $strFullHtml;
?>