<?
session_start();
include ("../include/config.php");

$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include("connectOtherDataBase.php");
// include ("javaScript.php");
//include_once ("secureAllPages.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$recurID = $_GET["recurID"];



if($recurID != "")
{
    /* echo "Active Status ID".$recurID; */
    update("update ".TBL_RECURRING_PAYMENT." set Status ='Close' where transID='".$recurID."'");
	$descript = "Recurring Rule Stopped";	
	activities($_SESSION["loginHistoryID"],"UPDATION",$recurID,TBL_TRANSACTIONS,$descript);
}

if(CONFIG_DOUBLE_ENTRY == '1')
	include("double-entry-functions.php");
	
$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
	$limit=50;
	
	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
	
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";

$moneyPaid = "";

if ($_POST["moneyPaid"] != "") {
	$moneyPaid = $_POST["moneyPaid"];
} else if ($_GET["moneyPaid"] != "") {
	$moneyPaid = $_GET["moneyPaid"];
}

if($_POST["transType"]!="")
{
		$transType2 = $_POST["transType"];
		
	}
	elseif($_GET["transType"]!="") 
	{
		$transType2=$_GET["transType"];
		
	}
		
if($_POST["Submit"]!="")
		$Submit = $_POST["Submit"];
	elseif($_GET["Submit"]!="") 
		$Submit=$_GET["Submit"];
		
	if($_POST["transID"]!="")
		$transID = $_POST["transID"];
	elseif($_GET["transID"]!="")
		$transID = $_GET["transID"];

if($_POST["searchBy"]!="")
		$by = $_POST["searchBy"];
	elseif($_GET["searchBy"]!="") 
		$by = $_GET["searchBy"];

$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";

$trnsid = $_POST["trnsid"];
if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
{
	if($_REQUEST["transSource"]!="")
	{
		$transSource = $_REQUEST["transSource"];
	
	}
}
$createdByLabel = "Created By";
$createdByN = $_arrLabels[$createdByLabel];
if(!empty($createdByN))
	$createdByLabel = $createdByN;
$ttrans = $_POST["totTrans"];
if($_POST["btnAction"] != "")
{
	for ($i=0;$i< $ttrans;$i++)
	{
		
		if(count($trnsid) >= 1)
		{
			
			if($_POST["btnAction"] != "")
			{
				 $contentTrans = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$trnsid[$i]."'");
				 $custID 	= $contentTrans["customerID"];
				 $benID 		= $contentTrans["benID"];
				 $custAgentID = $contentTrans["custAgentID"];
				 $benAgentID = $contentTrans["benAgentID"];
				 $imReferenceNumber = $contentTrans["refNumberIM"];
				 $collectionPointID = $contentTrans["collectionPointID"];
				 $transType  = $contentTrans["transType"];
				 $transDate   = $contentTrans["transDate"];
				 $transDate = (!empty($transDate))?date("F j, Y", strtotime("$transDate")):"";
			
				$fromName = SUPPORT_NAME;
				$fromEmail = SUPPORT_EMAIL;
				// getting Admin Email address
				$adminContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where username= 'admin'");
				$adminEmail = $adminContents["email"];
				// Getting customer's Agent Email
				if(CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED)
				{
				$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$custAgentID'");
				$custAgentEmail	= $agentContents["email"];
				}
				// getting BEneficiray agent Email
				if(CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED)
				{
				$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$benAgentID'");
				$benAgentEmail	= $agentContents["email"];
				}
				// getting beneficiary email
				$benContents = selectFrom("select email from ".TBL_BENEFICIARY." where benID= '$benID'");
				$benEmail = $benContents["email"];
				// Getting Customer Email
				$custContents = selectFrom("select email from " . TBL_CUSTOMER . " where customerID= '$custID'");
				$custEmail = $custContents["email"];

/**********************************************************/

				
			}

//sendMail($custEmail, $subject, $message, $fromName, $fromEmail);

			if($_POST["btnAction"] == "Cancel")
			{
			   // echo "Cancel Ids".$trnsid[$i] ;
				// Recurring Payment Ticket 11753 (Muhammad Umair khan)
				$recurringStat= selectFrom("select Status from recurring_transactions where transID='".$trnsid[$i]."'");
				
				$recStat =$recurringStat["Status"];
                if($recStat != "")
                {
				   if($recStat =='Active')
				   {
                   // echo "Status Active";
				   ?>
				   <script>
				   var r = confirm("Do you want to cancel recurring rule?");
                    if (r == true) {
        
			$.ajax({
		
		    type: "GET",
		    url: "request_cancellation.php",
		    data:
			{
			recurID : <?=$trnsid[$i];?>
			}
		  			
			});

            } else {
                   alert("You pressed Cancel!");
                    } 
				  // echo "Recurring Rule Status".$recStat;
				   
				   </script>
				   <?
				   }
				   
				}				
				$tranStat = selectFrom("select transStatus, transID, totalAmount, custAgentID, commissionMode,benAgentID from ". TBL_TRANSACTIONS." where transID='". $trnsid[$i] ."' and transStatus !='Cancelled' and transStatus != 'AwaitingCancellation'");
				$stat = $tranStat["transStatus"];
				$id=$tranStat["transID"];
				if($id != ""){
				 if($stat == 'Pending' || $stat == 'Processing')
					{
					$balance = $tranStat["totalAmount"];
					$agent = $tranStat["custAgentID"];
					$userID  = $_SESSION["loggedUserData"]["userID"];
					$today = date("Y-m-d");

					//insertInto("insert into agent_account values('', '$agent', '$today', 'DEPOSIT', '$balance', '$userID', '$id', 'Transaction Cancelled')");
						confirmCancelled($id,$username);
					}
				else
				{
					waitCancellation($id,$username);
					}
				}
			}
		}
	}
//redirect("request_cancellation.php?msg=Y&action=". $_GET["action"]);

}

$acManagerFlagLabel = "Account Manager";
$acManagerFlag = false;
if(CONFIG_POPULATE_USERS_DROPDOWN=="1" && defined("CONFIG_ACCOUNT_MANAGER_COLUMN") && CONFIG_ACCOUNT_MANAGER_COLUMN!="0"){
	$custExtraFields = ", c.parentID as custParentID, am.name as accountManagerName ";
	$acManagerFlagLabel = CONFIG_ACCOUNT_MANAGER_COLUMN;
	$extraJoin = " LEFT JOIN ".TBL_ADMIN_USERS." as am ON am.userID = c.parentID  ";
	$acManagerFlag = true;
}
//echo("Variables are by=$by,,,transID=$transID,,,,Submit=$Submit,,,transStatus=$transStatus,,,transType=$transType2");
	$query = "select t.benID,transID,transDate,totalAmount,custAgentID, t.benAgentID,transType,transAmount,localAmount,refNumber,refNumberIM,transStatus,
					t.customerID,t.collectionPointID,createdBy, clientRef,t.custDocumentProvided  ".$custExtraFields."
				from ". 
					TBL_TRANSACTIONS . " as t
					LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
					$extraJoin."  
					WHERE 1 ";
$queryCnt = "select count(t.transID) 
				from ". 
					TBL_TRANSACTIONS . " as t
					LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
					$extraJoin."  
					WHERE 1 ";

if($Submit == "Search")
{
	if($transID != "")
	{
		switch($by)
		{
			case 0:
			{		
				$query = "select t.benID,transID,transDate,totalAmount,custAgentID, t.benAgentID,transType,transAmount,localAmount,refNumber,refNumberIM,transStatus,
						t.customerID,t.collectionPointID,createdBy, clientRef,t.custDocumentProvided  ".$custExtraFields."
					from ". 
						TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
						$extraJoin."  
						WHERE 1 ";
				$queryCnt = "select count(*)
					from ". 
						TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
						$extraJoin."  
						WHERE 1 ";
				
				$query .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
				$queryCnt .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
				break;
			}
			case 1:
			{
				$query = "select t.benID,transID,transDate,refNumber,refNumberIM,totalAmount," . $chequeAmountField . " custAgentID,transType,transAmount,localAmount,transStatus,t.customerID,t.collectionPointID,createdBy,t.custDocumentProvided ".$custExtraFields."
					from 
						". TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
						$extraJoin."  
						where t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(*) 
					from 
						". TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
						$extraJoin."  
						where t.createdBy != 'CUSTOMER' ";
							
				$queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
		
	      // searchNameMultiTables function (Niaz)
       $fn="FirstName";
			 $mn="MiddleName";
			 $ln="LastName"; 		
			 $alis="cm";
			 $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);	
		
			 $queryonline .= $q;
			 $queryonlineCnt .= $q;
			
			 // searchNameMultiTables function (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="c";
			 $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);	
			 
				$query .= $q;
				$queryCnt .= $q;
				
				//$queryonline .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
				//$queryonlineCnt .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
				//$query .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
				//$queryCnt .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
				
				break;
			}
			case 2:
			{
				$query = "select t.benID,transID,transDate,refNumber,refNumberIM,transStatus,custAgentID,transType,totalAmount," . $chequeAmountField . " transAmount,localAmount,t.customerID,b.benID,t.collectionPointID,createdBy,t.custDocumentProvided ".$custExtraFields."
				from 
					". TBL_TRANSACTIONS . " as t
					LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
					LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
					$extraJoin."  
					where t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(*) 
				from 
					". TBL_TRANSACTIONS . " as t
					LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
					LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
					$extraJoin."  
					where t.createdBy != 'CUSTOMER' ";
							
				$queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as cb where t.benID = cb.benID and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as cb where t.benID = cb.benID and t.createdBy = 'CUSTOMER' ";
				
				// searchNameMultiTables function (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="cb";
			 $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);
				
			 $queryonline .= $q;		
			 $queryonlineCnt .= $q;
			
			 $alis="b";
			 $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);
			 
			 $query .= $q;
			 $queryCnt .=$q;
				
				//$queryonline .= " and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%')";		
				//$queryonlineCnt .= " and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%')";
				//$query .= " and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
				//$queryCnt .=" and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";

				break;
			}
		}
//		$query .= " and (t.transID='".$_POST["transID"]."' OR t.refNumber = '".$_POST["transID"]."' OR  t.refNumberIM = '".$_POST["transID"]."') ";
	//	$queryCnt .= " and (t.transID='".$_POST["transID"]."' OR t.refNumber = '".$_POST["transID"]."' OR  t.refNumberIM = '".$_POST["transID"]."') ";
			}
}
	if($transType2 != "")
	{
		$query .= " and (t.transType='".$transType2."')";
		$queryCnt .= " and (t.transType='".$transType2."')";
		if($transID != "" && $by != 0)
		{
			$queryonline .= " and (t.transType='".$transType2."')";
		  $queryonlineCnt .= " and (t.transType='".$transType2."')";
			}
		
	}
	
	if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
	{
		if($transSource!= "")
		{
			if($transSource == "O")
			{
				$query.= " and trans_source = 'O' ";
				$queryCnt .= " and trans_source = 'O' ";
			}
			else if ($transSource == "P")
			{
				$query.= " and trans_source != 'O' ";
				$queryCnt .= " and trans_source != 'O' ";
			}
			
			
		}
	}


	if($_GET["action"] == "Cancel" || $_GET["action"] == "cancel")
	{
		$query .= " and (t.transStatus ='Pending' or t.transStatus ='Authorize' or t.transStatus ='Processing' or t.transStatus ='Amended')";
		$queryCnt .= " and (t.transStatus ='Pending' or t.transStatus ='Authorize' or t.transStatus ='Processing' or t.transStatus ='Amended')";
	}
	if($transID != "" && $by != 0)
	{
		$queryonline .= " and (t.transStatus ='Pending' or t.transStatus ='Authorize' or t.transStatus ='Processing' or t.transStatus ='Amended')";
	  $queryonlineCnt .= " and (t.transStatus ='Pending' or t.transStatus ='Authorize' or t.transStatus ='Processing' or t.transStatus ='Amended')";
	}
	
	if ($moneyPaid != "") {
		
		$query .= " and (t.moneyPaid = '".$moneyPaid."')";
		$queryCnt .= " and (t.moneyPaid = '".$moneyPaid."')";
		
		$queryonline .= " and (t.moneyPaid = '".$moneyPaid."')";
		$queryonlineCnt .= " and (t.moneyPaid = '".$moneyPaid."')";
		
	}



switch ($agentType)
{
	case "SUPA":
	case "SUPAI":
	case "SUBA":
	case "SUBAI":
		$query .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		$queryCnt .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
	
	break;
	case "admin":
	case "Admin Manager":
		break;
	default:
		$query .= " and (t.addedBy = '".$_SESSION["loggedUserData"]["username"]."')";
		$queryCnt .= " and (t.addedBy = '".$_SESSION["loggedUserData"]["username"]."')";
		
}
/**
 *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements 
 *  This condition executes when admin manager or branch manager logs in,
 *  keeping the check of showing the logged in users only the transactions of 
 *  their associated senders.
 */
if( defined("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS") && strstr(CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS,$agentType) )
{
   /**
	*  amending the conditions with queries
	*/
   $query     .= " and ( am.userID = '".$_SESSION["loggedUserData"]["userID"]."' ) ";
   $queryCnt  .= " and ( am.userID = '".$_SESSION["loggedUserData"]["userID"]."' ) ";
}
	$query .= " order by t.transDate DESC";
//debug($query);
//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS."";
  $query .= " LIMIT $offset , $limit";
 
  $contentsTrans = selectMultiRecords($query);
 
  $allCount = countRecords($queryCnt);


		
		if($transID != "" && $by != 0)
		{	
			
			
			$onlinecustomerCount = count(selectMultiRecords($queryonlineCnt ));
				
			$allCount = $allCount + $onlinecustomerCount;
			
			
			
					$other = $offset + $limit;
			 if($other > count($contentsTrans))
			 {
				if($offset < count($contentsTrans))
				{
					$offset2 = 0;
					$limit2 = $offset + $limit - count($contentsTrans);	
				}elseif($offset >= count($contentsTrans))
				{
					$offset2 = $offset - $countOnlineRec;
					$limit2 = $limit;
				}
				$queryonline .= " order by t.transDate DESC";
			  $queryonline .= " LIMIT $offset2 , $limit2";
				$onlinecustomer = selectMultiRecords($queryonline);
			 }
			
	  }
		//debug($query);
?>
<html>
<head>
	<title>Request Cancellation</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="javaScript.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
	
	function checkBoxCheck(field)
{
alert("Contact MLRO to Cancel this transaction");

	document.getElementById(field).checked=false;
}

	
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}




-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2"><? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?> 
      Transactions Request</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="request_cancellation.php?action=<? echo $_GET["action"]?>" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
            <td nowrap> Search Transactions 
              <input name="transID" type="text" id="transID" value="<?=$transID?>">
		  <select name="transType" style="font-family:verdana; font-size: 11px; width:100">
          <option value=""> - Type - </option>
          <option value="Pick up">Pick up</option>
          <option value="Bank Transfer">Bank Transfer</option>
          <option value="Home Delivery">Home Delivery</option>
        </select><script language="JavaScript">SelectOption(document.forms[0].transType, "<?=$transType2?>");</script>
         <? if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
		{ 
		
		?>
            <select name="transSource">
                <option value=""> - Transaction Source - </option>
                <option value="O">Online</option>
                <option value="P">Payex</option>
                
            
            </select>
            <script language="JavaScript">SelectOption(document.forms[0].transSource, "<?=$transSource?>");</script>
            <? } ?>
        	<br>
		<select name="searchBy" >
			<option value=""> - Search By - </option>
			<option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
			<option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By <?=__("Sender");?> Name</option>
			<option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].searchBy, "<?=$by?>");</script>
		<?	if (CONFIG_PAYMENT_MODE_FILTER == "1") {  ?>
			Payment Mode
      <select name="moneyPaid" id="moneyPaid" style="font-family:verdana; font-size: 11px;">
          <option value="">- Select Mode -</option>
          <option value="By Cash">By Cash</option>
          <option value="By Cheque">By Cheque</option>
          <option value="By Bank Transfer">By Bank Transfer</option>
		  </select>
		  <script language="JavaScript">
				SelectOption(document.forms[0].moneyPaid, "<?=$moneyPaid; ?>");
			</script>
		<?	}  ?>
		<input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
      <form action="request_cancellation.php?action=<? echo $_GET["action"]?>" method="post" name="trans">


          <?
			if ($allCount > 0){
				//debug($query);
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&action=".$_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&action=".$_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&action=".$_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&action=".$_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo (count($contentsTrans) + count($onlinecustomer))?> records to <? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?>.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0 || count($onlinecustomer) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?></span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			  <td><span class="style1"><?=__("Sender");?> Name </span></td>
			  <td><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1"><?=$createdByLabel;?></span></td>
			<?php if($acManagerFlag){?>
				<td width="80"><span class="style1"><b><?=$acManagerFlagLabel?></b> </span></td>
			<?php }?>
			  <td width="146" align="center"><? if ($_GET["action"] != "") { ?><input name="All" type="checkbox" id="All" onClick="CheckAll();"><? } ?></td>
		      <td width="74" align="center">&nbsp;</td>
			  <td width="74" align="center">&nbsp;</td>
			</tr>
		    <? for($i=0;$i < count($contentsTrans);$i++)
			{
				
						if(CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER == "1"){
					$customerDetails = selectFrom("select customerStatus from ".TBL_CUSTOMER." where customerID = ".$contentsTrans[$i]["customerID"]."");
						$onlineCustomerDetails = selectFrom("select status from cm_customer where c_id = ".$contentsTrans[$i]["customerID"]."");
				
								if ($customerDetails["customerStatus"] == "Disable"){
									
														$fontColor = "#CC000";
														$custStatus = "Disable";
									}elseif ($customerDetails["customerStatus"] == "Enable" || $customerDetails["customerStatus"]== ""){
										
														$fontColor = "#006699";
														$custStatus = "Enable";
										
									}
																			
									}else{
										
												 $fontColor = "#005b90";
										}
									
					if(CONFIG_SHARE_OTHER_NETWORK == '1')
				{
					$otherNetwork = selectFrom("select * from ".TBL_SHARED_TRANSACTIONS." where localTrans = '".$contentsTrans[$i]["transID"]."' and generatedLocally = 'N'");	
				}				
				
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="<? echo $fontColor ?>"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"];?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($contentsTrans[$i]["totalAmount"]) .  " " . getCurrencySign($contentsTrans[$i]["currencyFrom"])?></td>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	  
						if(CONFIG_SHARE_OTHER_NETWORK == '1' && $otherNetwork["remoteTrans"] != '')
						{
							$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$otherNetwork["remoteServerId"]."'");	
							$otherClient = new connectOtherDataBase();
							$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
							
							$customerContent = $otherClient->selectOneRecord("select firstName, lastName from ".$jointClient["dataBaseName"].".customer where customerID = '".$contentsTrans[$i]["customerID"]."'");		
							$beneContent = $otherClient->selectOneRecord("select firstName, lastName from ".$jointClient["dataBaseName"].".beneficiary where benID = '".$contentsTrans[$i]["benID"]."'");		
							$otherClient->closeConnection();
							dbConnect();
						}else{
				  	
				  		$customerContent = selectFrom("select firstName, lastName,customerStatus from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
				  		$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
				  	}
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  	}
				  ?>
					
				  <?
				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>				  </td>
				  <?php if($acManagerFlag){?>
	                  <td bgcolor="#FFFFFF"><?=ucfirst($contentsTrans[$i]["accountManagerName"])?></td>
					<?php }?> 
	<?
		if(CONFIG_SHARE_OTHER_NETWORK == "1" &&  $otherNetwork["sharedTransId"] != '')
		{
			
			?>
			<td align="center" bgcolor="#FFFFFF">&nbsp; </td>
			<?
			
		}else{
					if(CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER =="1"){
											if ($customerContent['customerStatus'] == 'Disable'){?>
											
									  <td align="center" bgcolor="#FFFFFF">  <? if ($_GET["action"] != "" && $_GET["action"] != "Recall") {  $tempTransId = $contentsTrans[$i]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId' id='$tempTransId' onClick=checkBoxCheck($tempTransId);>";?><? } ?></td>
									<? }elseif ($customerContent['customerStatus']== 'Enable' || $customerContent['customerStatus']==""){
										
										
										?>
											
										<td align="center" bgcolor="#FFFFFF">  <? if ($_GET["action"] != "" && $_GET["action"] != "Recall") {  $tempTransId = $contentsTrans[$i]["transID"]; echo "<input type=checkbox name=trnsid[] id='$tempTransId' value='$tempTransId' >";?><? } ?></td>
									<? } 
					}else{?>

				  <td align="center" bgcolor="#FFFFFF"><? if ($_GET["action"] != "" && $_GET["action"] != "Recall") {  $tempTransId = $contentsTrans[$i]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId'>";?><? } ?></td>
				<? }
				
			} 
			?>
				  <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $contentsTrans[$i]["transID"]?>" class="style2">Enquiry</a></td>
				</tr>
				<?
			}
			?>
			
			<? for($j=0;$j< count($onlinecustomer); $j++)
			{
				$i++;
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $onlinecustomer[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$j]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$j]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($onlinecustomer[$j]["totalAmount"]) .  " " . getCurrencySign($onlinecustomer[$j]["currencyFrom"])?></td>
				  <? if($onlinecustomer[$j]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$onlinecustomer[$j]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$onlinecustomer[$j]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$onlinecustomer[$j]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$onlinecustomer[$j]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>
					
				  <?
				  if($onlinecustomer[$j]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$onlinecustomer[$j]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$j]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>				  </td>


				  <td align="center" bgcolor="#FFFFFF"><? if ($_GET["action"] != "") {  $tempTransId = $onlinecustomer[$j]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId'>";?><? } ?></td>
				   <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $onlinecustomer[$j]["transID"]?>" class="style2">Enquiry</a></td>
				</tr>
				<?
			}
			?>
			
			<tr bgcolor="#FFFFFF">
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td bgcolor="#FFFFFF">&nbsp;</td>
			  <td width="100" align="center">&nbsp;</td>
			  <td align="center"><input type='hidden' name='totTrans' value='<?echo $i?>'>
			    <? if ($_GET["action"] != "") { ?><input name="btnAction" type="submit"  value="<? echo ucfirst($_GET["action"])?>"><? } ?></td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			<?php if($acManagerFlag){?>
				  <td align="center">&nbsp;</td>
			<?php }?> 
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			</tr>
			<?
			} // greater than zero
			?>
          </table></td>
        </tr>


          <tr>
            <td  bgcolor="#000000"> 
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&action=".$_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&action=".$_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&action=".$_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&action=".$_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
            </td>
          </tr>

          <?
			} else {
		?>
          <tr>
            <td  align="center"> No Transaction found in the database.
            </td>
          </tr>
          <?
			}
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>
