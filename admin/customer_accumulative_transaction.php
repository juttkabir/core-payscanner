<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
$agentID = $_SESSION["loggedUserData"]["userID"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "") {
	$offset = 0;
}
		
if ($limit == 0) {
	$limit=50;
}
	
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
	
$nxt = $offset + $limit;
$prv = $offset - $limit;

$sortBy = $_GET["sortBy"];

if ($sortBy == "") {
	$sortBy = "transDate";
}

$minAmount = "";
$custName = "";
$custIDs = "";
$currencyFrom = "";
$Submit = "";

if ($_POST["minAmount"] != "") {
	$minAmount = $_POST["minAmount"];
}	elseif ($_GET["minAmount"]!="") {
	$minAmount = $_GET["minAmount"];
}



if($_POST["customerID"] != "") {
	$customerID = $_POST["customerID"];
}	elseif ($_GET["customerID"]!="") {
	$customerID = $_GET["customerID"];
}



if ($_POST["Submit"] != "") {
	$Submit = $_POST["Submit"];
}	elseif ($_GET["Submit"]!="") {
	$Submit = $_GET["Submit"];
}

$currency = $_GET["currency"];

	

	
	
	$query = "SELECT * FROM ". TBL_TRANSACTIONS . " WHERE 1 AND `customerID` = ".$customerID." AND transStatus != 'Cancelled' ";
	
	$queryCnt = "SELECT COUNT(*)  FROM ". TBL_TRANSACTIONS . " WHERE 1 AND `customerID` = ".$customerID." AND transStatus != 'Cancelled'";


if ($currency != "") {
		$query .= " AND `currencyFrom` = '".$currency."' ";
		$queryCnt .= " AND `currencyFrom` = '".$currency."' ";
	}

$queryCustomer = "select accumulativeAmount  from ".TBL_CUSTOMER." where 1 AND `customerID` = ".$customerID."";
	
	
	$query .= " LIMIT $offset , $limit";
	
	
	//$countTransaction = selectMultiRecords($qryCountTransaction);
	$allCount = countRecords($queryCnt);

	$contentsTrans = selectMultiRecords($query);
		$Customer = selectFrom($queryCustomer);

?>
<html>
<head>
	<title>Sender AML Report</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Customer's Transactions</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
      <form action="cust-aml-report.php?action=<? echo $_GET["action"]?>" method="post" name="trans">


          <?
			if ($allCount > 0){
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo (count($contentsTrans) + count($onlinecustomer))?> records to View.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td width="100"><span class="style1">Date</span></td>
			  <?	if (CONFIG_SHOW_MANUAL_CODE == '1') {  ?>
			  <td width="100"><span class="style1"><? echo $manualCode;?></span></td>
				<?	}  ?>
			  <td width="100"><span class="style1">Reference Code </span></td>
			  
			  <td width="100"><span class="style1">Status </span></td>
			  <td width="100"><span class="style1">Total Amount </span></td>
			  <td width="100"><span class="style1">Sender Name </span></td>
			  <td width="100"><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1">Created By </span></td>
			</tr>
		    <? for($i=0;$i < count($contentsTrans);$i++)
			{
				?>
				<?	
					if (CONFIG_AML_COLOR_SCHEME == '1') {
						if ($contentsTrans[$i]["totalAmount"] > CONFIG_AMOUNT_CONTROLLER) {
							$color = "#FF0000";
						} else {
							$color = "#000000";
						}
					} else {
						$color = "#000000";	
					}
				?>
				<tr bgcolor="#FFFFFF">
					<? if($agentType == "MLRO"){ ?>
							<td bgcolor="#FFFFFF"><strong><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><font color="#006699"><?  echo dateFormat($contentsTrans[$i]["transDate"], "2") ?></font></a></strong></td>
					<? }else{ ?>
				  <td bgcolor="#FFFFFF"><strong><font color="#006699"><?  echo dateFormat($contentsTrans[$i]["transDate"], "2") ?></font></strong></td>
				  <? } ?>
				  
				  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $contentsTrans[$i]["refNumber"]?></font></td>
				 
				  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $contentsTrans[$i]["refNumberIM"]?></font></td>
				  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $contentsTrans[$i]["transStatus"]?></font></td>
				  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $contentsTrans[$i]["totalAmount"]?></font></td>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, middleName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, middleName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["middleName"]) . " " . ucfirst($customerContent["LastName"]) ?></font></td>
				  	<td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["middleName"]) . " " . ucfirst($beneContent["lastName"]) ?></font></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, middleName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, middleName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["middleName"]) . " " . ucfirst($customerContent["lastName"]) ?></font></td>
				  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["middleName"]) . " " . ucfirst($beneContent["lastName"]) ?></font></td>
				  	<?
				  	}
				  ?>
				  
				  
				  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $contentsTrans[$i]["createdBy"] ?></font></td>
				</tr>
				<? $totalAmount += $contentsTrans[$i]["totalAmount"]; ?>
				<?
			}?>
			
			<tr bgcolor="#FFFFFF" colspan="8">
			</tr>	
		<tr bgcolor="#FFFFFF">
				<td bgcolor="#FFFFFF"><font color="#006699"><b>TOTAL</b></font></td>
				<td bgcolor="#FFFFFF">&nbsp;</td>
				<td bgcolor="#FFFFFF">&nbsp;</td>
				<td bgcolor="#FFFFFF">&nbsp;</td>
				
				<td bgcolor="#FFFFFF"><? echo $totalAmount; ?></td>
					<td bgcolor="#FFFFFF">&nbsp;</td>
				<td bgcolor="#FFFFFF">&nbsp;</td>
				<td bgcolor="#FFFFFF">&nbsp;</td>
				</tr>
		
		
		<?	} // greater than zero
			
			?>
          </table></td>
        </tr>


          <tr>
            <td  bgcolor="#000000"> 
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
             </td>
          </tr>

          <?
			} else {
				if ($Submit != "") {
		?>
			<tr>
				<td align="center"><i>No transaction found according to above filter.</i></td>
			</tr>
		<?	
				} 
			}  
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>