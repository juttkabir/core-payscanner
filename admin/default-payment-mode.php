<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$userID = $_SESSION["loggedUserData"]["userID"];

if ($_POST["Submit"] == "Submit") {
	update("UPDATE " . TBL_ADMIN_USERS . " SET `defaultMoneyPaid` = '" . $_POST["moneyPaid"] . "' WHERE `userID` = '" . $userID . "'");
	if (empty($_POST["moneyPaid"])) {
		$msg = "Your default payment mode is unset successfully";
	} else {
		$msg = "Default Payment Mode is set to '" . $_POST["moneyPaid"] . "' successfully";
	}
	insertError($msg);
	redirect("default-payment-mode.php?msg=Y&success=Y");
}

$moneyPaid = selectFrom("SELECT `defaultMoneyPaid` FROM " . TBL_ADMIN_USERS . " WHERE `userID` = '" . $userID . "'");
$defMoneyPaid = $moneyPaid["defaultMoneyPaid"];

?>
<html>
<head>
	<title>Set Default Payment Mode</title>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><b><strong><font color="#FFFFFF">Set Default Payment Mode </font></strong></b></td>
  </tr>
  <tr>
    <td>
	<table width="70%"  border="0">
  <tr>
    <td><fieldset>
    <legend class="style2">Set Default Payment Mode</legend>
    <br>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			<form name="defPayMode" action="default-payment-mode.php" method="post">
          <? if ($_GET["msg"] != ""){ ?>
          <tr bgcolor="#DFE6EA">
            <td colspan="2" bgcolor="#DFE6EA"><table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr>
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; $_SESSION['error'] = ""; ?></td>
                </tr>
              </table></td>
          </tr>
          <? } ?>
			  <tr>
				<td align="right" bgcolor="#DFE6EA">Select Payment Mode</td>
			    <td bgcolor="#DFE6EA">
			    	<select name="moneyPaid" id="moneyPaid">
              <option value="">-select one-</option>
              <option value="By Cash" <? echo ($defMoneyPaid == "By Cash" ? "selected" : "") ?>>By Cash</option>
              <? if(CONFIG_REMOVE_OPTION_PAYMENT_MODE != "1"){ ?>
              <option value="By Cheque" <? echo ($defMoneyPaid == "By Cheque" ? "selected" : "") ?>>By Cheque</option>
              <? } ?>
              <option value="By Bank Transfer" <? echo ($defMoneyPaid == "By Bank Transfer" ? "selected" : "") ?>>By Bank Transfer</option>
              <!--option value="By Credit Card">By Credit Card</option>
              <option value="By Debit Card">By Debit Card</option-->
			    	</select>
			    </td>
			  </tr>
			  <tr>
			    <td align="center" colspan="2" bgcolor="#DFE6EA"><input name="Submit" type="submit" class="flat" value="Submit"></td>
		      </tr>
		      </form>
			</table>
		    <br>
        </fieldset></td>
  </tr>
</table>

	</td>
  </tr>
</table>
</body>
</html>