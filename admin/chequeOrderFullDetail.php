<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	
	$strMainCaption = "Cheque Order Detail";
	$_strDefaultCurrency = "GBP";
	
	if(empty($_REQUEST["eodi"]))
	{
		echo "<h1>Invalid Request!</h1>";
		exit;
	}
	
	$arrOrderDetails = selectFrom("select * from cheque_order where order_id = '".$_REQUEST["eodi"]."'");
	$arrCompanyData = selectFrom("select * from company where company_id = '".$arrOrderDetails["company_id"]."'");
	$arrCustomerData = selectFrom("select firstname, middlename, lastname, accountname, address, address1, city, state, zip, country, phone, email from customer where customerid = '".$arrOrderDetails["customer_id"]."'");
	
	activities($_SESSION["loginHistoryID"],"VIEW",$_REQUEST["eodi"],"cheque_order","Cheque Order Full Details Viewed");

		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cheque Order Detail</title>
<script language="javascript" src="jquery.js"></script>
<script>
	$(document).ready(function(){
		$("#printOrder").click(function(){
			$(this).hide();
			print();
		});
		
		$("#closeReport").click(function(){
			close();
		});
	});
</script>
<style>
td{ 
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:12px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.links {
	background-color:#FFFFFF; 
	font-size:10px; 
	text-decoration:none;
}
</style>
</head>
<body>
<div id="loading" style="display:none; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:12px; color: #FFFFFF; background-color:#FF0000; position:absolute; top:10px; left:10px;">&nbsp;Loading....&nbsp;</div>
	<table width="70%" border="0" cellspacing="1" cellpadding="2" align="center">
		<tr>
			<td colspan="4" align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong>
			</td>
		</tr>
		<tr>
			<td colspan="4" align="center" bgcolor="#0000CC" style="font-size:15px; background-color:#0000CC; color:#FFFFFF">
				<strong>Order Status</strong>&nbsp;
				<i style="color: #FF9900; font-size:16px; font-weight:bold"><?=$arrChequeStatues[$arrOrderDetails["status"]]?></i>
			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination" colspan="4"><b>Company Details</b></td>
		</tr>
		
		<tr bgcolor="#ededed">
			<td colspan="4" id="companyDetails">
				<table width="100%" border="0" cellspacing="1" cellpadding="2" align="center" style="font-size:10px">
					<tr bgcolor="#ededed">
						<td width="25%" class="tdDefination" style="font-size:10px"><b>Company Name:&nbsp;</b></td>
						<td align="left" colspan="3"><?=$arrCompanyData["name"]?></td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="25%" class="tdDefination" style="font-size:10px"><b>Address:&nbsp;</b></td>
						<td align="left" colspan="3"><?=$arrCompanyData["address"]?></td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="25%" class="tdDefination" style="font-size:10px"><b>Country:&nbsp;</b></td>
						<td align="left" width="25%"><?=$arrCompanyData["country"]?></td>
						<td width="25%" class="tdDefination" style="font-size:10px"><b>City:&nbsp;</b></td>
						<td align="left" width="25%"><?=$arrCompanyData["city"]?></td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="25%" class="tdDefination" style="font-size:10px"><b>Zip Code:&nbsp;</b></td>
						<td align="left" width="25%"><?=$arrCompanyData["zip"]?></td>
						<td width="50%" class="tdDefination" colspan="2">&nbsp;</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="25%" class="tdDefination" style="font-size:10px"><b>Phone:&nbsp;</b></td>
						<td align="left" width="25%"><?=$arrCompanyData["phone"]?></td>
						<td width="25%" class="tdDefination" style="font-size:10px"><b>Fax:&nbsp;</b></td>
						<td align="left" width="25%"><?=$arrCompanyData["fax"]?></td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="25%" class="tdDefination" style="font-size:10px"><b>E-Mail:&nbsp;</b></td>
						<td align="left" width="25%"><?=$arrCompanyData["email"]?></td>
						<td width="25%" class="tdDefination" style="font-size:10px"><b>Contact Person:&nbsp;</b></td>
						<td align="left" width="25%"><?=$arrCompanyData["contact_person"]?></td>
					</tr>
				</table>
			</td>
		</td>
		
		<tr bgcolor="#ededed">
			<td width="100%" class="tdDefination" colspan="4"><b>Customer Details&nbsp;</b></td>
		</tr>
		
		<tr bgcolor="#ededed">
			<td colspan="4" id="customerDetails">
				<table width="100%" border="0" cellspacing="1" cellpadding="2" align="center" style="font-size:10px">
					<tr bgcolor="#ededed">
						<td width="25%" class="tdDefination" style="font-size:10px"><b>Full Name:&nbsp;</b></td>
						<td align="left" colspan="3"><?=$arrCustomerData["firstname"]." ".$arrCustomerData["middlename"]." ".$arrCustomerData["lastname"]?></td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="25%" class="tdDefination" style="font-size:10px"><b>Address:&nbsp;</b></td>
						<td align="left" colspan="3"><?=$arrCustomerData["address"].(!empty($arrCustomerData["address1"])?" ".$arrCustomerData["address1"]:"")?></td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="25%" class="tdDefination" style="font-size:10px"><b>Zip Code:&nbsp;</b></td>
						<td align="left" width="25%"><?=$arrCustomerData["zip"]?></td>
						<td width="25%" class="tdDefination" style="font-size:10px"><b>State:&nbsp;</b></td>
						<td align="left" width="25%"><?=$arrCustomerData["state"]?></td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="25%" class="tdDefination" style="font-size:10px"><b>City:&nbsp;</b></td>
						<td align="left" width="25%"><?=$arrCustomerData["city"]?></td>
						<td width="25%" class="tdDefination" style="font-size:10px"><b>Country:&nbsp;</b></td>
						<td align="left" width="25%"><?=$arrCustomerData["country"]?></td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="25%" class="tdDefination" style="font-size:10px"><b>Phone:&nbsp;</b></td>
						<td align="left" width="25%"><?=$arrCustomerData["phone"]?></td>
						<td width="25%" class="tdDefination" style="font-size:10px"><b>E-Mail:&nbsp;</b></td>
						<td align="left" width="25%"><?=$arrCustomerData["email"]?></td>
					</tr>
				</table>
			</td>
		</td>
		
		
		<tr bgcolor="#ededed">
			<td width="100%" colspan="4" align="left">
				<fieldset>
					<legend>Cheque Details</legend>
					<table align="left" width="100%">
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Cheque No:&nbsp;</b></td>
							<td align="left" width="75%"><?=$arrOrderDetails["cheque_no"]?></td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Bank:&nbsp;</b></td>
							<td align="left" width="75%"><?=$arrOrderDetails["bank_name"]?></td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Account Number:&nbsp;</b></td>
							<td align="left" width="75%"><?=$arrOrderDetails["account_no"]?></td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Branch:&nbsp;</b></td>
							<td align="left" width="75%"><?=$arrOrderDetails["branch"]?></td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Cheque Issue Date:&nbsp;</b></td>
							<td align="left" width="75%"><?=$arrOrderDetails["cheque_date"]?></td>
						</tr>
					</table>
				</fieldset><br />
				<fieldset>
					<legend>Amount Details</legend>
					<table align="left" width="100%">
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Cheque Currency:&nbsp;</b></td>
							<td align="left" width="25%"><?=$arrOrderDetails["cheque_currency"]?></td>
							<td width="25%" class="tdDefination">&nbsp;</td>
							<td align="left" width="25%">&nbsp;</td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Cheque Amount:&nbsp;</b></td>
							<td align="left" width="25%"><?=$arrOrderDetails["cheque_amount"]?></td>
							<td width="25%" class="tdDefination"><b>Date In:&nbsp;</b></td>
							<td align="left" width="25%"><?=$arrOrderDetails["created_on"]?></td>
						</tr>
						<tr bgcolor="#ededed" valign="top">
							<td width="25%" class="tdDefination"><b>Cheque Fee:&nbsp;</b></td>
							<td align="left" width="25%">
								<?=$arrOrderDetails["fee"]?>
							</td>
							<td width="25%" class="tdDefination">&nbsp;</td>
							<td align="left" width="25%">&nbsp;</td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Amount Paid to Customer:&nbsp;</b></td>
							<td align="left" width="25%"><?=$arrOrderDetails["paid_amount"]?></td>
							<td width="25%" class="tdDefination"><b>Estimated Date:&nbsp;</b></td>
							<td align="left" width="25%">
								<?=$arrOrderDetails["estimated_date"]?>
							</td>
						</tr>
						
					</table>
				</fieldset>
			</td>
		</tr>		
		<tr bgcolor="#ededed">
			<td width="100%" colspan="4" class="tdDefination" style="text-align:center">
				<br /><b>Order Notes&nbsp;</b><br />
				<?php
					echo $arrOrderDetails["order_note"];
				?>
			</td>
		</tr>
		
		<tr bgcolor="#ededed">
			<td width="100%" colspan="4" class="tdDefination" style="text-align:center">
				<br /><b>Reason for Manual Cheque Fee&nbsp;</b><br />
				<?php
					if(!empty($arrOrderDetails["fee_id"]))
						echo "[Fee is not manual]";
					else
						echo $arrOrderDetails["manual_fee_reason"];
				?>
			</td>
		</tr>		
		<tr bgcolor="#ededed">
			<td colspan="4" align="center">
				<input type="button" id="printOrder" value="Print Order" />
				<input type="button" id="closeReport" value="Close" />
			</td>
		</tr>
	</table>
</body>
</html>