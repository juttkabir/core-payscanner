<?
session_start();

include("../include/config.php");
include("security.php");

$userDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where username='$username'");

$agentType = getAgentType();

$userID = $_SESSION["loggedUserData"]["userID"];

if(CONFIG_CUSTOM_SENDER == '1')
{
	$customerPage = CONFIG_SENDER_PAGE;
}else{
	$customerPage = "add-customer.php";
	
	}

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}



?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--
<link rel="stylesheet" href="images/interface.css" type="text/css">
-->
<script language="javascript" src="./javascript/functions.js"></script>
<!-- Collapsible tables list scripts -->
<script type="text/javascript" language="javascript">
    <!--
    var isDOM      = (typeof(document.getElementsByTagName) != 'undefined' && typeof(document.createElement) != 'undefined') ? 1 : 0;
    var isIE4      = (typeof(document.all) != 'undefined' && parseInt(navigator.appVersion) >= 4) ? 1 : 0;
    var isNS4      = (typeof(document.layers) != 'undefined') ? 1 : 0;
    var capable    = (isDOM || isIE4 || isNS4)  ? 1 : 0;
    // Uggly fix for Opera and Konqueror 2.2 that are half DOM compliant
    if (capable) {
        if (typeof(window.opera) != 'undefined') {
            capable = 0;
        }
        else if (typeof(navigator.userAgent) != 'undefined') {
            var browserName = ' ' + navigator.userAgent.toLowerCase();
            if (browserName.indexOf('konqueror') > 0) {
                capable = 0;
            }
        } // end if... else if...
    } // end if
    var fontFamily = 'verdana, helvetica, arial, geneva, sans-serif';
    var fontSize   = 'small';
    var fontBig    = 'large';
    var fontSmall  = '14px';
    var isServer   = true;
    //-->
    </script>
<script src="./javascript/menu.js" type="text/javascript" language="javascript1.2"></script>
<noscript>
<style type="text/css">
        <!--
        div {color: #000000}
        .heada {font-family: verdana, helvetica, arial, geneva, sans-serif; font-size: small; color: #000000}
        .headaCnt {font-family: verdana, helvetica, arial, geneva, sans-serif; font-size: x-small; color: #000000}
        .parent {font-family: verdana, helvetica, arial, geneva, sans-serif; color: #FF0000; text-decoration: none; font-weight:bold}
        .child {font-family: verdana, helvetica, arial, geneva, sans-serif; font-size: x-small; color: #333399; text-decoration: none}
        .item, .item:active, .item:hover, .tblItem, .tblItem:active {color: #666666; text-decoration: none}/*333399---666666*/
        .tblItem:hover {color: #FF0000; text-decoration: underline; font-size:12px}
        //-->
        </style>
</noscript>
<style type="text/css">
    <!--
    body {font-family: verdana, helvetica, arial, geneva, sans-serif; font-size: small}
	a { color: #FF0000; text-decoration: none; font-size:12px;
}

    //-->
    </style>
</head>
<body bgcolor="#C0C0C0">
<table width="95%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#006699">
  <tr>
    <td height="20" align="center" bordercolor="#CCCCCC" bgcolor="#666666"><font color="#ffffff"><strong>Main 
      Administration</strong></font></td>
  </tr>
  <tr>
    <td valign="top" bordercolor="#999999" bgcolor="#CCCCCC"><table width="100%" border="0" cellpadding="0" cellspacing="2">
        <tr>
          <td bordercolor="#FFFFFF"><strong><a href="main.php" class='tblItem' target="mainFrame">Main Menu</a></span></strong></td>
        </tr>
        <tr>
          <td bordercolor="#FFFFFF"> 
            <?
			  	if ($userDetails["isMain"] == "Y" || $agentType == "Call" || $agentType == "Admin Manager" || $agentType == "Branch Manager")
				{
					if($userDetails["isMain"] == "Y" || $agentType == "Admin Manager"   || $agentType == "Branch Manager")
					{
					?>
            <div id="el0Parent" onmouseover="if (isDOM || isIE4) {hilightBase('el0', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el0', '#D0DCE0')}" class="parent">
              <a href="#" onclick="if (capable) {expandBase('el0', true); return false;}"> 
              <img NAME="imEx" SRC="images/plus.gif" BORDER="0" ALT="+" width="9" height="9" ID="el0Img"></a>
              <a href="#" class='tblItem' onclick="if (capable) {expandBase('el0', true); return false;}" >User
              Management</a></div>
              	
               <div ID="el0Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el0', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el0', '#D0DCE0')}"> 
             	<? if($userDetails["parentID"]== "0" && $agentType == "admin" ){ ?>
              &nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add_super_admin.php">Add 
              Super Admin</a><br> 
                &nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="manage_super_admin.php">Manage 
              Super Admin</a><br>
              <? }?>           
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-agent.php">Add 
              Super Agent</a><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> 
              <a class="tblItem" target="mainFrame" HREF="agent-list.php">Manage 
              Super Agents</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-sub-agent.php?type=sub">Add Sub Agent</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent-list.php?type=sub">Manage Sub Agents</a></nobr><br>
              <?
              	if(CONFIG_AnD_ENABLE == '1')
              	{
              ?>
             	 <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-agent_Distributor.php">Add A&D </a></nobr><br>
            	 <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent_Distributor-list.php">Manage A&D</a></nobr><br>	
            	  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-sub-agent.php?type=sub&ida=ND">Add Sub A&D</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent-list.php?type=sub&ida=ND">Manage Sub A&D</a></nobr><br>		
              <?
              	}
              ?>
              <? if($agentType != "Branch Manager")
              	  { ?>
		              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> 
		              <a class="tblItem" target="mainFrame" HREF="add-agent.php?ida=Y">Add Super 
		              Distributor </a></nobr><br>
		              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> 
		              <a class="tblItem" target="mainFrame" HREF="agent-list.php?ida=Y">Manage 
		              Super Distributor</a></nobr><br>
		              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> 
		              <a class="tblItem" target="mainFrame" HREF="add-sub-agent.php?type=sub&ida=Y">Add Sub Distributor</a></nobr><br>
		              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> 
		              <a class="tblItem" target="mainFrame" HREF="agent-list.php?type=sub&ida=Y">Manage 
		              Sub Distributor</a></nobr><br>
		              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-admin.php?act=addAdmin">Add Admin Staff</a></nobr><br>
		              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="admins-list.php">Manage Admin Staff</a></nobr><br>
			            <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="new-agent-list.php">New Agents</a></nobr><nobr></nobr><br>	
			            <?	if (CONFIG_AGENT_RECEIPT_RANGE == "1") {  ?>
			            <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-receipt-range.php">Add Agents Receipt Range</a></nobr><nobr></nobr><br>	
			            <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="view-receipt-ranges.php">View Agents Receipt Ranges</a></nobr><nobr></nobr><br>	
			            <?	}  ?>
              		<? if (CONFIG_EXPORT_TABLE_MGT == "1") {//export-files-mgt.php
              			 ?>
                  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="manage_export_config.php">Manage Export Pages</a></nobr><br> 
             			<? } ?>
									<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="assign_teller.php">Assign Teller</a></nobr><nobr></nobr><br>			
									<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="teller_list.php">Manage Teller</a></nobr><br>  
		            	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-collectionpoint.php">Add Collection Point</a></nobr><br>  
		            	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="manage_collection_point.php">Manage Collection Point</a></nobr><br>
		            	<? if(CONFIG_BACK_DATED == '1'){ ?>
		            	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="backdated_trans_rights.php">Right for Backdated</a></nobr><br>
            	<?
            			}	
            	 } 
            	 if ($agentType=="admin")
            	 {
            	 if (CONFIG_CITY_SERVICE_ENABLED)
			  			{
			  				?>
			  			<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="city-services.php">Home Delivery Services</a></nobr><br>
			  			<?
			  		}
			  	}
			  	?>
			  	
			  	<?
				  	 if (CONFIG_LIMIT_TRANS_AMOUNT == '1')
			  			{
			  				
			          		///Home Delivery Services To be Added in File
			           ?>
			  				<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="limit_transactions.php">Admin Transaction Limit</a></nobr><br>
			  			<?
			  				
				  		}
				  	
				  	?>
				  	<?
		          	if($agentType=="admin" && CONFIG_USER_COMMISSION_MANAGEMENT == '1')
		          	{
		           ?>
			            <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="configure-distributor-commission.php"><? echo("Add ".CONFIG_COMMISSION_MANAGEMENT_NAME." Commission");?></a></nobr><nobr></nobr><br>	
									 <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="view-commission.php"><? echo("View ".CONFIG_COMMISSION_MANAGEMENT_NAME." Commission");?></a></nobr><nobr></nobr><br>				            
			        <?
			        }
			       ?>
			  	
            </div>


			<!-- start of Customer Admin-->
  				<? if($agentType != "Branch Manager")
           { ?>
            <div id="el5Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el5', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el5', '#D0DCE0')}"> 
              <a class="item" HREF="#" onclick="if (capable) {expandBase('el5', true); return false;}"> 
              <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el5Img"></a> 
              <a href="#" onclick="if (capable) {expandBase('el5', true); return false;}" class='tblItem'>Online Sender Admin</a> </div>
            <div ID="el5Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el5', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el5', '#D0DCE0')}"> 
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> 
              <a class="tblItem" target="mainFrame" HREF="customer-list.php">Quick 
              Sender Search</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> 
              <a class="tblItem" target="mainFrame" HREF="add-customer-transaction.php?create=Y">Create 
              Sender Transaction</a></nobr><br>
			  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="transactions-history.php">Transaction History</a></nobr><br>
			  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="curr-customer-regis.php?msg=Y">Today's Registrations</a></nobr><br>
			  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="curr-customer-transaction.php?msg=Y">Today's Transactions</a></nobr><br>
			  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="online-customer-account.php">Online Sender Account</a></nobr><br>
            </div>
						<?
						}
						?>
			<!-- end of Customer Admin  Starts Service Management -->
						<? if($agentType != "Branch Manager")
           { ?>
	            <div id="el1Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el1', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el1', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el1', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el1Img"></a> <a href="#" onclick="if (capable) {expandBase('el1', true); return false;}" class='tblItem'>Services Management</a> </div>
	            <div ID="el1Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el1', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el1', '#D0DCE0')}"> &nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-services.php">Add/Update Services</a><br>
	 						<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="amount-transfer-method.php">View Service</a><br>
	            </div>
	           <?
          	}
            ?>
            <!-- End of Services Managment -->
            <!-- Start of Commission/Fee -->
            
            <div id="el2Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el2', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el2', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el2', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el2Img"></a> <a href="#" onclick="if (capable) {expandBase('el2', true); return false;}" class='tblItem'><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?> Management</a> </div>
            <div ID="el2Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el2', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el2', '#D0DCE0')}"> <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-fee.php">Add <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?></a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="fee-list.php">Manage <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?></a></nobr><br>
			  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="fee-calander.php">View Company <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fees");}?></a></nobr><br>
            </div>
            <!-- End of Commission/Fee -->
            <!-- Start of Exchange Rate -->
            <? if($agentType != "Branch Manager")
           { ?>
            <div id="el3Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el3', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el3', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el3', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el3Img"></a> <a href="#" onclick="if (capable) {expandBase('el3', true); return false;}" class='tblItem'>Exchange Rates</a> </div>
            <div ID="el3Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el3', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el3', '#D0DCE0')}"> <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-rate.php?message='exch'">Add Rates</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="rate-list.php">Manage Exchange Rates</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="exchange-rates-agent.php">Exchange Rates</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="import-exchange-rates.php">Import Exchange Rates</a></nobr><br>
            </div>
            <?
          	}
            ?>
            <!-- End of Exchange Rate -->
            <?
			}
			if($agentType == "Call")
			{
			?>
			<!-- start of Customer Admin-->

            <div id="el5Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el5', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el5', '#D0DCE0')}"> 
              <a class="item" HREF="#" onclick="if (capable) {expandBase('el5', true); return false;}"> 
              <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el5Img"></a> 
              <a href="#" onclick="if (capable) {expandBase('el5', true); return false;}" class='tblItem'>Online Sender 
              Admin</a> </div>
            <div ID="el5Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el5', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el5', '#D0DCE0')}"> 
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> 
              <a class="tblItem" target="mainFrame" HREF="customer-list.php">Quick 
              Sender Search</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="transactions-history.php">Transaction History</a></nobr><br>
			  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="curr-customer-regis.php?msg=Y">Today Registrations</a></nobr><br>			  
            </div>

			 <!--end of Customer Admin-->
			<?
			}
			?>
            <div id="el4Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el4', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el4', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el4', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el4Img"></a> <a href="#" onclick="if (capable) {expandBase('el4', true); return false;}" class='tblItem'>Transactions</a> </div>
            <div ID="el4Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el4', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el4', '#D0DCE0')}">
			  			<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="payment-output-file.php?y=1">Detail Manage Transaction</a></nobr><br>
			  			<?
			  			if ($agentType == "admin")
			  			{
			  				if(CONFIG_SUSPEND_TRANSACTION_ENABLED == '1')
			  				{
			  				?>
			  			<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="suspend-transaction.php">Suspend Transaction</a></nobr><br>
			  				<?
			  				}
			  				?>
			  				
			  			<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="payment-mode-transactions.php">Transactions By Money Paid</a></nobr><br>
			  			<?	if (CONFIG_REFNUM_GENERATOR == '1') {  ?>
			  			<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="ref-generator.php">Reference Number Generator</a></nobr><br>
			  			<?	}  ?>
			  			<?
				  		} 
				  		?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="<?=$customerPage?>">Add Sender</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="search-edit-cust.php">Update Sender</a></nobr><br>
              <?
              	if(CONFIG_IMPORT_TRANSACTION == '1')
				{
					?>
					<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="import-transactions.php">Import Transactions</a></nobr><br>
					<?
				}
              	?>

		<?
		if(CONFIG_CUSTOMER_MOBILE_NUMBER == "1")
		{
		?>
		<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" 
target="mainFrame" HREF="customer_data_report.php">Export Mobile Customer Data</a></nobr><br>
		<?
		}
		?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="import-customer.php">Import Senders</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="export-customer.php">Export Senders</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="import-beneficiary.php">Import Beneficiary</a></nobr><br>
              <?
              if(CONFIG_PAYIN_CUSTOMER == "1")
              {
              ?>
               	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="payin_Book_Customers.php">View <? echo (CONFIG_PAYIN_CUSTOMER_LABEL_SHOW == "1" ?  CONFIG_PAYIN_CUSTOMER_LABEL : "Payin Book Sender");?></a></nobr><br>
             	<?
							}             	
             	?>
              <!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-beneficiary.php">Add Beneficiary</a></nobr><br-->
              
              <?
              if(CONFIG_SEARCH_BENEFICIARY == '1')
			  {
              ?>
              	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="create-bene-trans.php">Search Beneficiary</a></nobr><br>
              <?
              }
              ?>
        
              <? //if($agentType != "Call") //&& $agentType != "Admin Manager"   change by Imran on against tiket No 385 and with discustion of Hasan, dated 17/06/06
              {
              ?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="<?=$transactionPage?>?create=Y">Create Transaction</a></nobr><br>
              <? if(CONFIG_BATCH_TRANSACTION_ENABLED){ ?>             
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="<?=$transactionPage?>?create=Y&batch=Y">Create Batch Transactions</a></nobr><br>
             <?}?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="view_inputer_transactions.php">View Inputter Transactions</a></nobr><br>
              <? } ?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="view-transactions.php">View Transactions</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="request_cancellation.php?action=cancel">Cancellation Request</a></nobr><br>
              <?
              if($agentType != "Call")
              {
              ?>
              	 <? if(DISPLAY_EDIT_TRANSACTIONS)
           				{
           					if(DISPLAY_TRANSACTIONS_LABEL == '1')
			         			{
           				?>   
              				<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="edit-transactions.php">Edit Pending Transaction</a></nobr><br>
			              <?
										}else{
									  ?>
											<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="edit-transactions.php">Edit Transaction</a></nobr><br>	
										<?
										}		            
			            }
			              ?>
			         	<?
			         	if(DISPLAY_TRANSACTIONS_LABEL == '1')
			         	{
			         	?>     
              		<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="amendment-transactions.php"> Edit Authorized Transaction</a></nobr><br>
              	<?
              	}else{
              	?>
              		<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="amendment-transactions.php"> To Amend Transaction</a></nobr><br>
              	<?
              	}
              	?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="cancel_Transactions.php?act=cancel">Confirm Cancellation</a></nobr><br>
              <?
            	}
              ?>
              <!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="manage-transactions.php?action=cancel">Cancel Transaction</a></nobr><br-->
          <? if(CONFIG_VERIFY_TRANSACTION_ENABLED)
           {?>   
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="verify-transactions.php?action=verify">Verify Transaction</a></nobr><br>
						<? }?>
              <? if($userDetails["isMain"] == "Y" || $agentType == "Admin Manager")
				  {
				  ?>
				  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="verify_agent_account.php">Verify Agent Payments</a></nobr><br>	
				  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="verify_AnD_account.php">Verify A&D Payments</a></nobr><br>	
				  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="import-barclays.php">Reconcile Online Bank Statement</a></nobr><br>
				  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="unresolved-payments.php">Unresolved Payments</a></nobr><br>
				  <? if(CONFIG_PAYIN_CUSTOMER_ACCOUNT == '1')
				  { 
					  	?>
							<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="AgentsCustomerAccount.php"><? echo (CONFIG_PAYIN_CUSTOMER_LABEL_SHOW == "1" ?  CONFIG_PAYIN_CUSTOMER_LABEL : "Payin Book Sender");?> Account</a></nobr><br>	
							<?
					}
					?>
<!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="import-barclays-customer-module.php">Import Customer Barclays file</a></nobr><br>-->
			  <!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="import-collection-points.php">Import Collection Points</a></nobr><br-->
							
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="authorize-transactions.php?action=authorize">Authorize Transactions</a></nobr><br>
              
              <? if(IS_BRANCH_MANAGER == '1' || CONFIG_BACKDATING_PAYMENTS == '1'){ ?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="discount-transactions.php"><? if(CONFIG_TOTAL_FEE_DISCOUNT == '1'){echo(CONFIG_DISCOUNT_LINK_LABLES );}else{ ?>Fee Discount Request<? } ?></a></nobr><br>
		
<? } 
if(CONFIG_HOLD_TRANSACTION_ENABLED){ ?>              
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="hold-transactions.php?action=hold">Hold Transaction</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="unHold-transactions.php?action=unhold">Unhold Transaction</a></nobr>
   <? } ?>
             
              <?
				  }
				  ?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="trans-status.php">Transaction Status</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="export-trans.php">Export Transactions</a></nobr><br>
							<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="Distributor_Output_File.php">Distributor Output File</a></nobr><br>
							<?
	          	if(CONFIG_MERCHANT_FILE_BEACON == '1')
	          	{///Distributor Output File
	          	  ?>
								<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="Distributor_Output_File_Beacon.php">Distributor Output File for Merchant Bank</a></nobr><br>
								<?
							}
							?>
              <!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="trans-cust.php">Agent Transactions</a></nobr><br-->
         <? if(CONFIG_VERIFY_TRANSACTION_ENABLED)
          {?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="verify-transactions.php?action=verify">Pending Transaction</a></nobr><br>
           <? }?>
              <!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="pending-amounts.php">Agent Pending Amounts</a></nobr><br-->
              <!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent-comm.php">Agent Commission</a></nobr><br-->

            </div>
			
			
            <div id="el11Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el11', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el11', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el11', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el11Img"></a> <a href="#" onclick="if (capable) {expandBase('el11', true); return false;}" class='tblItem'>FAQs</a> </div>
            <div ID="el11Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el11', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el11', '#D0DCE0')}"> <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-faq.php">Add FAQ</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="faqs-list.php">FAQs Listing</a></nobr><br>
            </div>
          
		    <div id="el6Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el6', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el6', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el6', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el6Img"></a> <a href="#" onclick="if (capable) {expandBase('el6', true); return false;}" class='tblItem'>Enquiries</a> </div>
            <div ID="el6Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el6', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el6', '#D0DCE0')}"> <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-complaint.php">Make Enquiry</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="view-complaint.php?sess='Not'">View All</a></nobr><br>
            </div>		
			<?	}
				else
				{
					if($userDetails["adminType"] == "Admin" || $userDetails["adminType"] == "Supper" || $userDetails["adminType"] == "System"  || $agentType == "Branch Manager")
					{
					?>
            <!--div id="el7Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el7', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el7', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el7', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el7Img"></a> <a href="#" onclick="if (capable) {expandBase('el7', true); return false;}" class='tblItem'>Miscellaneous</a> </div>
            <div ID="el7Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el7', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el7', '#D0DCE0')}"> <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="exchange-rates-agent.php">Exchange Rates</a></nobr><br>
             
			 </div-->
			 <div id="el8Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el8', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el8', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el8', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el8Img"></a> <a href="#" onclick="if (capable) {expandBase('el8', true); return false;}" class='tblItem'>User Manager</a> </div>
			 
			  
			 
			  <?
							if(strstr($userDetails["rights"], "Create Agent"))
							{?>
				<div ID="el8Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el8', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el8', '#D0DCE0')}"> 
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-agent.php">Add Super Agent</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-sub-agent.php?type=sub">Add Sub Agent</a></nobr><br><div>
             
			  <?
							}
							if(strstr($userDetails["rights"], "Delete Agent"))
							{
								if(!strstr($userDetails["rights"], "Create Agent")){?>
				<div ID="el8Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el8', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el8', '#D0DCE0')}">
					<? }?> 
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent-list.php">Manage Super Agents</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent-list.php?type=sub">Manage Sub Agent</a></nobr><br></div>


			  <?
							}if(strstr($userDetails["rights"], "Delete Agent") || strstr($userDetails["rights"], "Create Agent"))
							{?>
							</div>
							<?	}
							?>
				
							<? if(strstr($userDetails["rights"], "Create Transaction") || strstr($userDetails["rights"], "Authorise Transaction") || strstr($userDetails["rights"], "Verify Transaction")){
									?>
									<div id="el88Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el88', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el88', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el88', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el88Img"></a> <a href="#" onclick="if (capable) {expandBase('el88', true); return false;}" class='tblItem'>Transaction</a> </div>
								<div ID="el88Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el88', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el88', '#D0DCE0')}">
									<? }
							if(strstr($userDetails["rights"], "Create Transaction"))
							{?>
								
				<!--div ID="el88Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el88', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el88', '#D0DCE0')}"> 
             
            </div-->
            
             <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="<?=$transactionPage?>?create=Y">Create Transaction</a></nobr><br>
             <? if(CONFIG_BATCH_TRANSACTION_ENABLED){ ?>             
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="<?=$transactionPage?>?create=Y&batch=Y">Create Batch Transactions</a></nobr><br>
             <?}?>
            <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="view_inputer_transactions.php>View Inputter Transactions</a></nobr><br>
            <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="request_cancellation.php?action=cancel">Cancellation Request</a></nobr><br>
			  <?
							}
							if(strstr($userDetails["rights"], "Authorise Transaction"))
							{?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="authorize-transactions.php?action=authorize">Authorize Transactions</a></nobr><br>
              <?
							}
							if(strstr($userDetails["rights"], "Verify Transaction"))
							{
								if(CONFIG_HOLD_TRANSACTION_ENABLED)
								{
								?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="hold-transactions.php?action=hold">Hold Transaction</a></nobr><br>
              <?
           			 }
							}
							if(strstr($userDetails["rights"], "Create Transaction") || strstr($userDetails["rights"], "Authorise Transaction") || strstr($userDetails["rights"], "Verify Transaction")){
								
							?>
							<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="trans-status.php">Transaction Status</a></nobr><br>
							 </div>
							<? 
							}
							?>
              
            
            <div id="el2Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el2', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el2', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el2', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el2Img"></a> <a href="#" onclick="if (capable) {expandBase('el2', true); return false;}" class='tblItem'><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?> Management</a> </div>
            <div ID="el2Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el2', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el2', '#D0DCE0')}">
			<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="fee-calander.php">View Company <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?></a></nobr><br>
            </div>

            <?
					}
				}
				// For AGENTS
				if($agentType == "SUPA" || $agentType == "SUPAI" || $agentType == "SUBA" || $agentType == "SUBAI" || $agentType == "SUPI" || $agentType == "SUBI")
				{
				?>

            <div id="el8Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el8', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el8', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el8', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el8Img"></a> <a href="#" onclick="if (capable) {expandBase('el8', true); return false;}" class='tblItem'>Transactions</a> </div>
            <div ID="el8Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el8', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el8', '#D0DCE0')}"> 
              <?
						if($agentType == "SUPA" || $agentType == "SUPAI" || $agentType == "SUBA" || $agentType == "SUBAI")
						{
						?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> 
              <a class="tblItem" target="mainFrame" HREF="<?=$customerPage?>">Add 
              Sender</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-beneficiary.php">Add Beneficiary</a></nobr><br>
              
              <?
              if(CONFIG_SEARCH_BENEFICIARY == '1')
			  {
              ?>
              	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="create-bene-trans.php">Search Beneficiary</a></nobr><br>
              <?
              }
              ?>
              
              
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="<?=$transactionPage?>?create=Y">Create Transaction</a></nobr><br>
               <? if(CONFIG_BATCH_TRANSACTION_ENABLED == "1"){ ?>             
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="<?=$transactionPage?>?create=Y&batch=Y">Create Batch Transactions</a></nobr><br>
             <?}?>
              <!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="view_inputer_transactions.php">View inputter Transactions</a></nobr><br-->
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="view-transactions.php">View Transaction</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="request_cancellation.php?action=cancel">Cancellation Request</a></nobr><br>
              <?
              if(CONFIG_HOLD_TRANSACTION_ENABLED)
              {
              ?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="hold-transactions.php?action=hold">Hold Transaction</a></nobr><br>
              <? }?>
              
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="trans-status.php">Transaction Status</a></nobr><br>
              <?
						}
						?>
              <? 
			  
			  if($agentType == "SUPAI" || $agentType == "SUBAI" || $agentType == "SUPI" || $agentType == "SUBI")
				{

				if($username == "AdminIDA")
				{
				?>
			  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="IDA-transaction-view-page.php?y=1">View All Transactions</a></nobr><br>
			  <?
			  }
			  ?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="release-trans.php">Release Transaction</a></nobr><br>
              <?
              if(CONFIG_REPORT_SUSPICIOUS == '1')
              {
              ?>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="release-trans.php">Report Suspicious Transaction</a></nobr><br>
              <?
            	}
              ?>
             <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="old-trans-search.php">Old Transaction Search</a></nobr><br>
              <?
              if(CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR == '1')
              {
              	//if(CLIENT == 'Beaconcrest' && $_SESSION["loggedUserData"]["userID"] == '100305')
              	if(CONFIG_MERCHANT_FILE_BEACON == '1' && $_SESSION["loggedUserData"]["userID"] == '100305')
	          	{///Distributor Output File
	          	  ?>
								<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="Distributor_Output_File_Beacon.php">Distributor Output File for Merchant Bank</a></nobr><br>
								<?
							}else{
	              ?>
	              	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="Distributor_Output_File.php">Distributor Output File</a></nobr><br>
	              <?
	            	}
	            }
	            ?>
              <!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="recalled-trans.php">Recalled Transactions</a></nobr><br-->
			  <?
					  $userID = $_SESSION["loggedUserData"]["userID"];
					  $agentContents = selectFrom("select * from " . TBL_ADMIN_USERS . " where userID = $userID");
					  $strIDACountry = $agentContents["IDAcountry"];

					  if($strIDACountry == 'India')
					  {
					  ?>
					  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="ida-toml-excel-sheet.php">Export New Transactions</a></nobr><br>

					  <?
					  }
				}
				?>
            </div>
           <!-- 
		   asked by sir hasan
		   <div id="el2Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el2', '#CCFFCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el2', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el2', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el2Img"></a> <a href="#" onclick="if (capable) {expandBase('el2', true); return false;}" class='tblItem'><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?> Management</a> </div>
            <div ID="el2Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el2', '#CCFFCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el2', '#D0DCE0')}">
			<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="fee-calander.php"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?> Table</a></nobr><br>
            </div>-->

            <div id="el9Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el9', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el9', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el9', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el9Img"></a> <a href="#" onclick="if (capable) {expandBase('el9', true); return false;}" class='tblItem'>Enquiries</a> </div>
            <div ID="el9Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el9', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el9', '#D0DCE0')}"> <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-complaint.php">Make Enquiry</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="view-complaint.php?sess='Not'">View All</a></nobr><br>
            </div>
            <? /*if($userDetails["isMain"] == "Y")
					  {
					  ?>
	            <div id="e20Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('e20', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('e20', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('e20', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="e20Img"></a> <a href="#" onclick="if (capable) {expandBase('e20', true); return false;}" class='tblItem'>Sender Managment</a></div>
	            <div ID="e20Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('e20', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('e20', '#D0DCE0')}"> <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="AgentsCustomerAccount.php">Agent's Sender Account</a></nobr><br></div>
	            
            <?
          	}*/
            ?>
            
            <div id="el10Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el10', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el10', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el10', true); return false;}"> 
              <?
				if($agentType == "SUPAI" || $agentType == "SUBAI")
				{
				?>
            <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el10Img"></a> <a href="#" onclick="if (capable) {expandBase('el10', true); return false;}" class='tblItem'>Miscellaneous</a> </div>
            <!--div ID="el10Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el10', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el10', '#D0DCE0')}"> <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent-comm.php">Commission Report</a></nobr><br-->
              <div ID="el10Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el10', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el10', '#D0DCE0')}">
              	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="exchange-rates-agent.php">Exchange Rates</a></nobr><br></div>
              <?
				}
				?>
            </div>
            <?
				}
		if($agentType=="admin"||$agentType=="SUPAI"||$agentType=="SUPI"||$agentType=="SUPA"||$agentType == "Admin Manager"  || $agentType == "Branch Manager")
			{
				?>
	
	<!-- Added by Kashif   admin                 SUPAI== Super Agent      SubAgent====SUBAI     Super Bank===SUPI     Sub Bank====SUBI -->
			 <div id="e29Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('e29', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('e29', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('e29', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="e29Img"></a> <a href="#" onclick="if (capable) {expandBase('e29', true); return false;}" class='tblItem'>Reports</a> </div>
            <?  if($agentType=="admin"||$agentType=="SUPAI"||$agentType=="SUPA"||$agentType == "Admin Manager"  || $agentType == "Branch Manager"){?>
			<div ID="e29Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('e29', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('e29', '#D0DCE0')}"> <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="commission_summary_report.php">Agent Commission Report</a></nobr><br>
			
				<?
				if(CONFIG_AnD_ENABLE == '1' && $agentType=="SUPAI"){
				?>
					<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent_Distributor_Account.php">A&D Account Statement</a></nobr><br>			  	
				<?
				}
				?>
			
			<? } 
			   if($agentType=="admin"||$agentType=="SUPAI"||$agentType=="SUPA"||$agentType == "Admin Manager"){
			   		if(!strstr(CONFIG_DISABLE_REPORT, "DIST_COMM"))
			   		{
			   	?>
					<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="distributor-comm-summary.php">Distributor Commission Report</a></nobr><br>
			
				<? 
					}
					if(CONFIG_PROFIT_EARNING == '1')
					{
						if($agentType=="admin" || $agentType=="SUPAI")
						{?>
						<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="profit_earning_Bayba.php">Profit Earning Report</a></nobr><br>
						<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="profit_earning_list.php">Profit Earning Report List</a></nobr><br>
						
					<?
						}
					}
					
				}   
			
				if($agentType=="SUPI"||$agentType=="SUPAI"){?>
            	 <div ID="e29Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('e29', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('e29', '#D0DCE0')}"> <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="daily-bank-transfer-report.php">Daily Distributor Report</a></nobr><br>
            	 	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="bank_Account_List.php">Distributor Statement</a></nobr><br>
            	 	<?
            	 	if(!strstr(CONFIG_DISABLE_REPORT, "DAILY_CASHIER"))
            	 	{
            	 	?>
            	 		<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="teller_daily_report.php">Daily Cashier Report</a></nobr><br>
            	 	<?
            	 	}
            	 	?>
            	 	
            	 	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="payout_center_report.php">Payout Center Report</a></nobr><br>
            	 	<?
            	 	if(!strstr(CONFIG_DISABLE_REPORT, "TELLER_ACCOUNT"))
            	 	{
            	 	?>
            	 		<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="teller_Account.php">Teller/Cashier Account</a></nobr><br>
            	 	<?
            		}
            	 	?>
            	 	<? if (CONFIG_CURR_DENOMINATION == "1") { ?>
            	 	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="DCurrency-List.php"><?= CONFIG_CURR_CAPTION ?></a></nobr><br>
            		<? } ?>
								<!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="outstanding_payout_center_report.php">Outstanding Payout Center Report</a></nobr><br-->
            	 </div>
			  <? }
			  
	    		if($agentType=="admin"||$agentType == "Admin Manager"  || $agentType == "Branch Manager"){
	    			  if($agentType != "Branch Manager"){
	    			?>
				<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="daily-bank-transfer-report.php">Daily Distributor Report</a></nobr><br>	
				<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="bank_Account_List.php">Distributor Statement</a></nobr><br>
				<?
            	if(!strstr(CONFIG_DISABLE_REPORT, "DAILY_CASHIER"))
            	{
            	 	?>
					<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="teller_daily_report.php">Daily Cashier Report</a></nobr><br>
					<?
				}
				?>
				
				
				<?
            	if(!strstr(CONFIG_DISABLE_REPORT, "TELLER_ACCOUNT"))
            	{
            	?>
					<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="teller_Account.php">Teller/Cashier Account</a></nobr><br>
					
				<? }
				}
				
				?>
				
				<?
        	 	if(!strstr(CONFIG_DISABLE_REPORT, "DAILY_AGENT"))
        	 	{
        	 	?>
					<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="daily-agent-transfer-report.php">Daily Agent Report</a></nobr><br>	
					<?
				}
				?>
				<? if (CONFIG_PAYMENT_MODE_REPORT_ENABLE == "1") { ?>
				<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="payment-mode-report.php">Payment Mode Report</a></nobr><br>	
				<? } ?>
				<?
        	 	if(!strstr(CONFIG_DISABLE_REPORT, "OUSTSTANDING_AGENT"))
        	 	{
        	 	?>
					<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="outStandingPay.php">Outstanding Agent Payments</a></nobr><br>	
				<?
				}
				?>
				
				<?
				if(CONFIG_DIALY_COMMISSION == '1')
				{
				?>
					<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="daily-commission-summary.php">Daily Commission Report</a></nobr><br>	
				<?
				}
				?>
		    <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="daily-trans.php">Daily Transaction Summary</a></nobr><br>
		  	<?	if (CONFIG_CUST_AML_REPORT == '1') {  ?> 	
		    <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="cust-aml-report.php">Sender AML Report</a></nobr><br>
		  	<?	}  ?>
     		<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="transferReport.php">Transaction Transfer Report</a></nobr><br>
     		<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="reportTransactionSummary.php">Transaction Transfer Report</a></nobr><br>
				<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="beneficiaryReport.php">Sender/Beneficiary Report</a></nobr><br>
				<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="current-regis.php">Sender Registration Report</a></nobr><br>
				<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="current-ben-regis.php">Beneficiary Registration Report</a></nobr><br>
				<?
        	 	if(!strstr(CONFIG_DISABLE_REPORT, "RATE_EARNING"))
        	 	{
        	 	?>
					<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="company_profit.php">Exchange Rate Earning Report</a></nobr><br>
					<?
				}
				
			   } 
			  if($agentType=="admin"||$agentType=="SUPA"||$agentType == "Admin Manager"  || $agentType == "Branch Manager"){?>
			  	<?
 						if(CONFIG_SALES_AS_BALANCE == '1'){
	 						?>
	 						<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="sales_Report.php">Agent Account Balance</a></nobr><br>			  	
	 						<?
 						}else{
 						?>
			  				<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="Agent_account_statement.php">Agent Account Balance</a></nobr><br>
			  				<?
			  			}
			  			?>
			  	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent_country_base_transactions.php">
			  		<?
			  		if(CONFIG_AGENT_COUNTRY_BASED == '1')
				  	{
				  		echo(CONFIG_AGENT_COUNTRY_BASED_LABEL);
				  	}else{
				  		echo("Agent Country Base Transactions");
				  		}
				  	?>
				  		</a></nobr><br>
			  	
			  	<? if(CONFIG_PAYIN_CUSTOMER_ACCOUNT == '1')
				  { 
					  	?>
			  			<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="customer_account_statement.php">Sender Account Balance</a></nobr><br>
			  			<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="payout_center_report.php">Payout Center Report</a></nobr><br>
				<!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="outstanding_payout_center_report.php">Outstanding Payout Center Report</a></nobr><br-->
			  			<?
		  		}
		  		?>
			  	<? if (CONFIG_AGENT_STATMENT_GLINK == '1'){?>
			  	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent_Account_List.php">Agent Statement</a></nobr><br>
			  <? }} 
			  		  
			  if($agentType=="admin")
			  {
			  ?>
			  	<?
			  	if(CONFIG_SALES_REPORT == '1')
			  	{
			  		?>
			  		<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="sales_Report.php">Daily Sales Report</a></nobr><br>			  	
			  		<?
			  	}
			  	?>
			  	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent_Account.php">Agent Account Statement</a></nobr><br>			  	
				<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="bank_Account.php">Distributor Account Statement</a></nobr><br>
				<?
				if(CONFIG_ACCOUNT_SUMMARY == '1'){
					?>
					<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent_Account_Summary.php">Account Summary</a></nobr><br>			  	
					<?
				}
				?>

				<?
				if(CONFIG_AnD_ENABLE == '1'){
				?>
					<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent_Distributor_Account.php">A&D Account Statement</a></nobr><br>			  	
				<?
				}
				
				
				if(CONFIG_EXPORT_ALL_TRANS == '1'){
					if($agentType=="admin"){?>
						<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="extract_data.php">Export All Transactions</a></nobr><br>			  	
					<?
					}
				}
				?>
				<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="user_audit_log_screen.php">Payex Audit Logs</a></nobr><br>			  	
				<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="Transaction_Audit.php">Transactions Audit Window</a></nobr><br>			  	
			  	<!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="transferBalance.php">Transfer Balance</a></nobr><br-->			  	
				
				<?
				
				if(CONFIG_CONSOLIDATED_SENDER_REGIS_REPORT == '1')
				{//consolidated sender registration report
					?>
					<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="consolidated-sender-regis-report.php">Consolidated Sender Registration Report</a></nobr><br>			  	
					<?
				}?>
				
			  <?
				}
			  ?>
            </div>
	<!--Added By Kashif-->
			<?
			}
			if($agentType == "TELLER")
			{?>
				
				<div id="el8Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el8', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el8', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el8', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el8Img"></a> <a href="#" onclick="if (capable) {expandBase('el8', true); return false;}" class='tblItem'>Transactions</a> </div>
            <div ID="el8Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el8', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el8', '#D0DCE0')}"> <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="old-trans-search.php">Old Transaction Search</a></nobr><br> 
            	<?
            	 	if(!strstr(CONFIG_DISABLE_REPORT, "DAILY_CASHIER"))
            	 	{
            	 	?>
            		<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="teller_daily_report.php">Daily Cashier Report</a></nobr><br>
            		<?
            		}
            		?>
            </div>			
				<div id="el3Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el3', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el3', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el3', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el3Img"></a> <a href="#" onclick="if (capable) {expandBase('el3', true); return false;}" class='tblItem'>Enquiries</a> </div>
					<div ID="el3Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el3', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el3', '#D0DCE0')}"> <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-complaint.php">Make Enquiry</a></nobr><br>		
				  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="view-complaint.php?sess='Not'">View All</a></nobr><br>
				  </div>
				<!--div id="el3Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el3', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el3', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el3', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el3Img"></a> <a href="#" onclick="if (capable) {expandBase('el3', true); return false;}" class='tblItem'>Transactions</a> </div-->
        <? if (CONFIG_CURR_DENOMINATION == "1") { ?>
       	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="DCurrency-List.php"><? echo(CONFIG_CURR_CAPTION); ?></a></nobr><br>
       	<? } ?>
       

				<? 
				}
				if($agentType == "COLLECTOR")
				{?>
				
				<div id="el3Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el3', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el3', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el3', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el3Img"></a> <a href="#" onclick="if (capable) {expandBase('el3', true); return false;}" class='tblItem'>Agent Account</a> </div>
            <div ID="el3Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el3', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el3', '#D0DCE0')}"> <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent_Account.php">View Agent Account</a></nobr><br>
 						
 							<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="Agent_account_statement.php">View Agent Balance</a></nobr><br>           	
 						
 						
 						<?
						if(CONFIG_AnD_ENABLE == '1'){
						?>
							<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent_Distributor_Account.php">A&D Account Statement</a></nobr><br>			  	
						<?
						}
						?>	
       </div>
				<? 
				}
			?>
			<!--------Add by Erfan ------->
			<?
			if($agentType == "Support"){
		  
		  ?>
			<div id="e21Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('e21', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('e21', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('e2l', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="e21Img"></a> <a href="#" onclick="if (capable) {expandBase('e21', true); return false;}" class='tblItem'>Payex Configuration</a> </div>
            <div ID="e21Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('e21', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('e21', '#D0DCE0')}"> 
            	
            	<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-country.php">Mulitcountry Payex</a></nobr><br>
            	 						
 							<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="Agent_account_statement.php">View Agent Balance</a></nobr><br>           	
 						<?
						if(CONFIG_AnD_ENABLE == '1'){
						?>
						<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent_Distributor_Account.php">A&D Account Statement</a></nobr><br>			  	
          <?
          }
        ?>
         </div>  
       <?
       }
       ?>
       <!---------------------->
          </td>
        </tr>
      </table></td>
  </tr>
</table>
<script type="text/javascript" language="javascript1.2">
    <!--
    if (isNS4) {
      firstEl  = 'el0Parent';
      firstInd = nsGetIndex(firstEl);
      nsShowAll();
      nsArrangeList();
    }
    expandedDb = '';
    //-->
</script>
</body>
