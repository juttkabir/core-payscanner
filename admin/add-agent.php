<?
session_start();
include ("../include/config.php");
include ("security.php");

 /**
 * #6747:Premier Exchange
 * Agent User type is displayed on agent form.
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
$agentUserTypeFlag = false;
if(CONFIG_AGENT_USER_TYPE == "1"){
	$agentUserTypeFlag = true;
}
$complianceFlag = false;
if(CONFIG_COMPLIANCE_FOR_ALL_USERS == '1')
	$complianceFlag = true;

if($complianceFlag){
	$url_host = $_SERVER['HTTP_HOST'];
	$server_protocol = $_SERVER['SERVER_PROTOCOL'];
	if(strstr($server_protocol,"HTTP")){
		$url_address = "HTTP://".$url_host;
	}elseif(strstr($server_protocol,"HTTPS")){
	$url_address = "HTTPS://".$url_host;
	}	
}
if($_REQUEST["ajaxCity"] == "get")
{
	$strCities = '';
	if(!empty($_REQUEST["cntry"]))
	{
		$cityQuery="select distinct(city) from  ".TBL_CITIES." where country = '".$_REQUEST["cntry"]."' order by city";
		$cityRS=selectMultiRecords($cityQuery);
		for($c=0;$c<count($cityRS);$c++)
		{
			$strCities .= '<option value="'.$cityRS[$c]["city"].'">'.$cityRS[$c]["city"].'</option>';
		}
    }
	else
	{
		$strCities .= '<option value="">- Select City -</option>';
	}

	echo $strCities;
	exit;
}
if($_REQUEST["ajaxServices"] == "get")
{
	$strServices = '';
	if(!empty($_REQUEST["cntry"]))
	{
		$countryIDRS = selectFrom("select countryId from ".TBL_COUNTRIES." where countryName = '".$_REQUEST["cntry"]."'");
		$serviceQuery="select DISTINCT(serviceAvailable) from  ".TBL_SERVICE_NEW." where toCountryId = '".$countryIDRS["countryId"]."' ";
		$serviceRS=selectMultiRecords($serviceQuery);
		for($c=0;$c<count($serviceRS);$c++)
		{
			$selected = "";
			if($c==0)
				$selected = "selected";
			$strServices .= '<option value="'.$serviceRS[$c]["serviceAvailable"].'" '.$selected.' ">'.$serviceRS[$c]["serviceAvailable"].'</option>';
		}
    }

	echo $strServices;
	exit;
}


// include ("javaScript.php");
$date_time = date('d-m-Y  h:i:s A');
$agentType2 = getAgentType();

$parentID = $_SESSION["loggedUserData"]["userID"];

$countryBasedFlag = false;
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}
$agentIDNumberFlag = false;
if(defined("CONFIG_SUP_AGENT_DIST_ID_NUMBER") && CONFIG_SUP_AGENT_DIST_ID_NUMBER=="1"){
	$agentIDNumberFlag = true;
}
// Added by Niaz Ahmad at 02-05-2009 @4816 Faith Exchange Upload Document
if(CONFIG_UPLOAD_DOCUMNET_ADD_SUPPER_AGENT	== "1"){
	if($_REQUEST["userID"]!="" && $_REQUEST["page"] == "uploadDocument"){
	$userQuery = selectFrom("select userID,username,isCorrespondent,adminType,agentType,agentType,password from admin
	where userID = '".$_REQUEST["userID"]."'");
	
	 $userName = $userQuery["username"];
	 $password = $userQuery["password"];
	 $isCorrespondent = $userQuery["isCorrespondent"];
	 $agentType = $userQuery["agentType"];
	 $adminType = $userQuery["agentType"];
	
		 if($isCorrespondent == "N" && $agentType == "Supper" && $_REQUEST["mode"] =="Add"){
			insertError(AG16." The login name is <b>$userName</b> and password is <b>$password</b>");
		 }elseif($isCorrespondent == "ONLY" && $agentType == "Supper" && $_REQUEST["mode"] =="Add"){
		 insertError("Super Distributor added successfully.The login name is <b>$userName</b> and password is <b>$password</b>");
		 }elseif($_REQUEST["mode"] =="update"){
			insertError("Record Updated Successfully");	
		 }
	}
}
session_register("agentCompany");
session_register("agentContactPerson");
session_register("agentAddress");
session_register("agentAddress2");
session_register("agentCountry");
session_register("agentCity");
session_register("distAlias");
session_register("agentZip");
session_register("agentPhone");
session_register("agentFax");
session_register("email");
session_register("agentURL");
session_register("agentMSBNumber");
session_register("msbMonth");
session_register("msbDay");
session_register("msbYear");
session_register("agentCompRegNumber");
session_register("agentCompDirector");
session_register("agentDirectorAdd");
session_register("agentProofID");
session_register("otherProofID");
session_register("dDate");
session_register("idMonth");
session_register("idDay");
session_register("idYear");
session_register("agentDocumentProvided");
session_register("agentBank");
session_register("agentAccountName");
session_register("agentAccounNumber");
session_register("agentBranchCode");
session_register("agentBranchName");  //Add by hasan for monex and created a filed in admin table as agentBranchName
session_register("agentAccountType");
session_register("agentCurrency");
session_register("agentCommCurrency");
session_register("agentAccountLimit");
session_register("agentCommission");
session_register("agentStatus");
session_register("isOnline");
session_register("currencyOrigin");
session_register("hasClave");
session_register("uploadImage");
session_register("distributorBankcharges");
if($agentIDNumberFlag){
	session_register("agentIDNumber");
}

if($_GET["type"] == "sub" && $_POST["supAgentID"] != "" && $_GET["f"] == "Y")
{
	//$supAgentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$_POST["supAgentID"]."'");
	//echo "select * from ".TBL_ADMIN_USERS." where userID='".$_POST["supAgentID"]."'";
	$_SESSION["supAgentID"] = $_POST["supAgentID"];
	$_SESSION["agentCompany"] = $supAgentDetails["agentCompany"];
		$_SESSION["agentContactPerson"] = $supAgentDetails["agentContactPerson"];
		$_SESSION["agentAddress"] = $supAgentDetails["agentAddress"];
		$_SESSION["agentAddress2"] = $supAgentDetails["agentAddress2"];
		$_SESSION["Country"] = $supAgentDetails["agentCountry"];
		$_SESSION["City"] = $supAgentDetails["agentCity"];
		$_SESSION["distAlias"] = $supAgentDetails["distAlias"];
		$_SESSION["agentZip"] = $supAgentDetails["postcode"];
		$_SESSION["agentPhone"] = $supAgentDetails["agentPhone"];
		$_SESSION["agentFax"] = $supAgentDetails["agentFax"];
		$_SESSION["email"] = $supAgentDetails["email"];
		$_SESSION["agentURL"] = $supAgentDetails["agentURL"];
		$_SESSION["agentMSBNumber"] = $supAgentDetails["agentMSBNumber"];
		$_SESSION["agentHouseNumber"] = $supAgentDetails["agentHouseNumber"];
		$_SESSION["msbMonth"] = $supAgentDetails["msbMonth"];
		$_SESSION["msbDay"] = $supAgentDetails["msbDay"];
		$_SESSION["msbYear"] = $supAgentDetails["msbYear"];
		$_SESSION["agentCompRegNumber"] 		= $supAgentDetails["agentCompRegNumber"];
		$_SESSION["agentCompDirector"]  		= $supAgentDetails["agentCompDirector"];
		$_SESSION["agentDirectorAdd"]   		= $supAgentDetails["agentDirectorAdd"];
		$_SESSION["agentProofID"] 					= $supAgentDetails["agentProofID"];
		$_SESSION["otherProofID"] 					= $supAgentDetails["otherProofID"];
		$_SESSION["idMonth"] 								= $supAgentDetails["idMonth"];
		$_SESSION["idDay"] 									= $supAgentDetails["idDay"];
		$_SESSION["idYear"] 								= $supAgentDetails["idYear"];
		$_SESSION["agentDocumentProvided"] 	= $supAgentDetails["agentDocumentProvided"];
		$_SESSION["agentBank"] 							= $supAgentDetails["agentBank"];
		$_SESSION["agentAccountName"] 			= $supAgentDetails["agentAccountName"];
		$_SESSION["agentAccounNumber"]			= $supAgentDetails["agentAccounNumber"];
		$_SESSION["agentBranchCode"] 				= $supAgentDetails["agentBranchCode"];
		$_SESSION["agentBranchCode"] 				= $supAgentDetails["agentBranchName"];  // Add by hasan for monex
		$_SESSION["agentAccountType"] 			= $supAgentDetails["agentAccountType"];
		$_SESSION["agentCurrency"] 					= $supAgentDetails["agentCurrency"];
		$_SESSION["agentAccountLimit"]			= $supAgentDetails["agentAccountLimit"];
		$_SESSION["agentCommission"] 				= $supAgentDetails["agentCommission"];
		$_SESSION["agentStatus"] 						= $supAgentDetails["agentStatus"];
		$_SESSION["commPackage"]						= $supAgentDetails["commPackage"];
		$isCorrespondent = $_SESSION["isCorrespondent"] 				= $supAgentDetails["isCorrespondent"];
		$_SESSION["IDAcountry"]     				= $supAgentDetails["IDAcountry"];
		$_SESSION["defaultDistrib"] 				= $supAgentDetails["defaultDistrib"];
		$_SESSION["hasClave"]	=	$supAgentDetails["hasClave"];
		$_SESSION["notes"]	=	$supAgentDetails["notes"];
		$_SESSION["rateCountries"] 				= $supAgentDetails["rateCountries"];
		$_SESSION["unlimitedRate"]	=	$supAgentDetails["unlimitedRate"];
		$_SESSION["agentCommCurrency"]	=	$supAgentDetails["agentCommCurrency"];
		$_SESSION["agentUserType"] = $supAgentDetails["agentUserType"];
		if($agentIDNumberFlag){
			$_SESSION["agentIDNumber"]	=	$supAgentDetails["agentIDNumber"];
		}
}
else
{
if ($_POST["agentCompany"] != "")
	$_SESSION["agentCompany"] = $_POST["agentCompany"];
if ($_POST["agentContactPerson"] != "")
	$_SESSION["agentContactPerson"] = $_POST["agentContactPerson"];
if ($_POST["agentAddress"] != "")
	$_SESSION["agentAddress"] = $_POST["agentAddress"];
if ($_POST["agentAddress2"] != "")
	$_SESSION["agentAddress2"] = $_POST["agentAddress2"];
if ($_POST["Country"] != "")
	$_SESSION["Country"] = $_POST["Country"];
if ($_POST["City"] != "")
	$_SESSION["City"] = $_POST["City"];
if ($_POST["distAlias"] != "")
	$_SESSION["distAlias"] = $_POST["distAlias"];	
if ($_POST["agentZip"] != "")
	$_SESSION["agentZip"] = $_POST["postcode"];
if ($_POST["agentPhone"] != "")
	$_SESSION["agentPhone"] = $_POST["agentPhone"];
if ($_POST["agentFax"] != "")
	$_SESSION["agentFax"] = $_POST["agentFax"];
if ($_POST["email"] != "")
	$_SESSION["email"] = $_POST["email"];
if ($_POST["agentURL"] != "")
	$_SESSION["agentURL"] = $_POST["agentURL"];
if ($_POST["agentMSBNumber"] != "")
	$_SESSION["agentMSBNumber"] = $_POST["agentMSBNumber"];
	if ($_POST["agentHouseNumber"] != "")
	$_SESSION["agentHouseNumber"] = $_POST["agentHouseNumber"];
if ($_POST["msbMonth"] != "")
	$_SESSION["msbMonth"] = $_POST["msbMonth"];
if ($_POST["msbDay"] != "")
	$_SESSION["msbDay"] = $_POST["msbDay"];
if ($_POST["msbYear"] != "")
	$_SESSION["msbYear"] = $_POST["msbYear"];
if ($_POST["agentCompRegNumber"] != "")
	$_SESSION["agentCompRegNumber"] = $_POST["agentCompRegNumber"];
if ($_POST["agentCompDirector"] != "")
	$_SESSION["agentCompDirector"] = $_POST["agentCompDirector"];
if ($_POST["agentDirectorAdd"] != "")
	$_SESSION["agentDirectorAdd"] = $_POST["agentDirectorAdd"];
if ($_POST["agentProofID"] != "")
	$_SESSION["agentProofID"] = $_POST["agentProofID"];
if ($_POST["otherProofID"] != "")
	$_SESSION["otherProofID"] = $_POST["otherProofID"];
if ($_POST["dDate"] != "")
	$_SESSION["dDate"] = $_POST["dDate"];
if ($_POST["idMonth"] != "")
	$_SESSION["idMonth"] = $_POST["idMonth"];
if ($_POST["idDay"] != "")
	$_SESSION["idDay"] = $_POST["idDay"];
if ($_POST["idYear"] != "")
	$_SESSION["idYear"] = $_POST["idYear"];
if ($_POST["agentDocumentProvided"] != "")
	$_SESSION["agentDocumentProvided"] = $_POST["agentDocumentProvided"];
if ($_POST["agentBank"] != "")
	$_SESSION["agentBank"] = $_POST["agentBank"];
if ($_POST["agentAccountName"] != "")
	$_SESSION["agentAccountName"] = $_POST["agentAccountName"];
if ($_POST["agentAccounNumber"] != "")
	$_SESSION["agentAccounNumber"] = $_POST["agentAccounNumber"];
if ($_POST["agentBranchCode"] != "")
	$_SESSION["agentBranchCode"] = $_POST["agentBranchCode"];
if ($_POST["agentAccountType"] != "")
	$_SESSION["agentAccountType"] = $_POST["agentAccountType"];
if ($_POST["agentCurrency"] != "")
	$_SESSION["agentCurrency"] = $_POST["agentCurrency"];
if ($_POST["agentCommCurrency"] != "")
	$_SESSION["agentCommCurrency"] = $_POST["agentCommCurrency"];
if ($_POST["agentAccountLimit"] != "")
	$_SESSION["agentAccountLimit"] = $_POST["agentAccountLimit"];
if ($_POST["agentCommission"] != "")
	$_SESSION["agentCommission"] = $_POST["agentCommission"];
if ($_POST["agentStatus"] != "")
	$_SESSION["agentStatus"] = $_POST["agentStatus"];
if($_POST["isOnline"] != "")	
	$_SESSION["isOnline"] = $_POST["isOnline"];
if($_POST["remoteAvailable"] != "")//remoteAvailable	
	$_SESSION["remoteAvailable"] = $_POST["remoteAvailable"];
if($_POST["defaultDistrib"] != "")
	$_SESSION["defaultDistrib"] = $_POST["defaultDistrib"];
if($_POST["currencyOrigin"]!= "")
$_SESSION["currencyOrigin"] = $_POST["currencyOrigin"];	
if($_POST["hasClave"] != "")
	$_SESSION["hasClave"] = $_POST["hasClave"];
	
if($_POST["notes"] != "")
	$_SESSION["notes"] = $_POST["notes"];


if($_POST["rateCountries"] != "")
	$_SESSION["rateCountries"] 	= $_POST["rateCountries"];
if($_POST["unlimitedRate"] != "")	
		$_SESSION["unlimitedRate"]	=	$_POST["unlimitedRate"];
if($_POST["agentUserType"] != "")	
		$_SESSION["agentUserType"]	=	$_POST["agentUserType"];
if($_POST["distributorBankcharges"] != "")	
		$_SESSION["distributorBankcharges"]	=	$_POST["distributorBankcharges"];

/**
 * If at first time than asign the default value to NO
 * @Ticket# 3506
 */
if(empty($_SESSION["replaceDefaultDistributor"]))
	$_SESSION["replaceDefaultDistributor"] = "N";

if($_POST["uploadImage"]!= "")
   $_SESSION["uploadImage"] = $_POST["uploadImage"];
}
if($agentIDNumberFlag){
	if($_POST["agentIDNumber"]!= ""){
	   $_SESSION["agentIDNumber"] = $_POST["agentIDNumber"];
	}
}

$managerLabel = "Select Branch Manager";
if(CONFIG_POPULATE_USERS_DROPDOWN=="1")
	$managerLabel = "Select Account Manager";

$arguments = array("flag"=>"populateUsers","selectedID"=>$_SESSION["supAgentID"]);
$populateUser = gateway("CONFIG_POPULATE_USERS_DROPDOWN",$arguments,CONFIG_POPULATE_USERS_DROPDOWN);
?>
<html>
<head>
	<title>Add Super Agent/Distributor</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="javaScript.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
	<?
	if($_GET["type"] == "sub" && $_GET["ida"] != "Y")
	{
	?>

	if(theForm.supAgentID.options.selectedIndex == 0){
    	alert("Please select super agent against which you want to add sub agent.");
        theForm.supAgentID.focus();
        return false;
    }
	<?
	}
	else if($_GET["type"] == "sub" && $_GET["ida"] == "Y")
	{
	?>
			if(theForm.supAgentID.options.selectedIndex == 0){
    	alert("Please select super distributor against which you want to add sub distributor.");
        theForm.supAgentID.focus();
        return false;
    }
			if(theForm.agentCompany.value == "" || IsAllSpaces(theForm.agentCompany.value)){
    	alert("Please provide the sub distributor name.");
        theForm.agentCompany.focus();
        return false;
    }
    if(theForm.agentContactPerson.value == "" || IsAllSpaces(theForm.agentContactPerson.value)){
    	alert("Please provide the sub distributor's contact person name.");
        theForm.agentContactPerson.focus();
        return false;
    }
	if(theForm.agentAddress.value == "" || IsAllSpaces(theForm.agentAddress.value)){
    	alert("Please provide the sub distributor's address line 1.");
        theForm.agentAddress.focus();
        return false;
    }
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the sub distributor's country from the list.");
        theForm.Country.focus();
        return false;
    }
   if(theForm.agentPhone.value == "" || IsAllSpaces(theForm.agentPhone.value)){
    	alert("Please provide the sub distributor's phone number.");
        theForm.agentPhone.focus();
        return false;
    }
	
    <?
  }
  else if($_GET["ida"] == "Y")
  {
  	?>
	if(theForm.agentCompany.value == "" || IsAllSpaces(theForm.agentCompany.value)){
    	alert("Please provide the distributor name.");
        theForm.agentCompany.focus();
        return false;
    }
   if(theForm.agentContactPerson.value == "" || IsAllSpaces(theForm.agentContactPerson.value)){
    	alert("Please provide the distributor's contact person name.");
        theForm.agentContactPerson.focus();
        return false;
    }
	if(theForm.agentAddress.value == "" || IsAllSpaces(theForm.agentAddress.value)){
    	alert("Please provide the distributor's address line 1.");
        theForm.agentAddress.focus();
        return false;
    }
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the distributor's country from the list.");
        theForm.Country.focus();
        return false;
    }
   if(theForm.agentPhone.value == "" || IsAllSpaces(theForm.agentPhone.value)){
    	alert("Please provide the distributor's phone number.");
        theForm.agentPhone.focus();
        return false;
    }
   <?
  }
  ?>
<?	if (CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y" || CONFIG_MANUAL_DISTRIBUTOR_NUMBER == "1" && $_GET["ida"] == "Y") {  ?>
  if(theForm.manualAgentNum.value == "" || IsAllSpaces(theForm.manualAgentNum.value)){
    	alert("Please provide the agent's login name.");
        theForm.manualAgentNum.focus();
        return false;
  }
<?	}  ?>
  if(theForm.agentCompany.value == "" || IsAllSpaces(theForm.agentCompany.value)){
    	alert("Please provide the agent's company name.");
        theForm.agentCompany.focus();
        return false;
    }
	if(theForm.agentContactPerson.value == "" || IsAllSpaces(theForm.agentContactPerson.value)){
    	alert("Please provide the agent's contact person name.");
        theForm.agentContactPerson.focus();
        return false;
    }
	if(theForm.agentAddress.value == "" || IsAllSpaces(theForm.agentAddress.value)){
    	alert("Please provide the agent's address line 1.");
        theForm.agentAddress.focus();
        return false;
    }
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the agent's country from the list.");
        theForm.Country.focus();
        return false;
    }
	
	<? if(CONFIG_CITY_MANDATORY == "1") {?>
	
	 if(theForm.City.value == "" || IsAllSpaces(theForm.City.value)){
    	alert("Please provide the Agnet City Name.");
        theForm.City.focus();
        return false;
    }
	<? } ?>

<? if($complianceFlag){	?>
		/*if(document.getElementById("checkType"))
		{*/
			return userComplianceCheck();
		//}
<? } ?>
	<?
		/***** Code added by Usman Ghani aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
		if ( defined("CONFIG_POST_CODE_MENDATORY")
			&& CONFIG_POST_CODE_MENDATORY == 1 )
		{
			?>
				if ( theForm.postcode.value == "" )
				{
					alert("Please provide post code.");
					theForm.postcode.focus();
					return false;
				}
			<?
		}
		/***** End of code aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
	?>
/*	
	if(theForm.City.options.selectedIndex == 0){
    	alert("Please select the agent's city from the list.");
        theForm.City.focus();
        return false;
    }*/
	if(theForm.agentPhone.value == "" || IsAllSpaces(theForm.agentPhone.value)){
    	alert("Please provide the agent's phone number.");
        theForm.agentPhone.focus();
        return false;
    }
<?	
/**
 * To make field mandatory (CONFIG based) if it not distributor
 * @Ticket #4158
 */
	if (CONFIG_AGENT_ACCOUNT_LIMIT == '1' && CONFIG_AGENT_LIMIT =='1' && $_GET["ida"]!="Y"){ ?>
	if(theForm.agentAccountLimit.value == "" || IsAllSpaces(theForm.agentAccountLimit.value)){
    	alert("Please provide the agent's account limit.");
        theForm.agentAccountLimit.focus();
        return false;
    }

<? } ?>
	
	if(theForm.IDAcountry.value == "" || IsAllSpaces(theForm.IDAcountry.value)){
    	alert("Please select atleast one country.");
        theForm.IDAcountry.focus();
        return false;
    }
<?	if (CONFIG_BACK_DATED == '1' && $_GET["ida"] != "Y" && $agentType == 'admin') {  ?>
	if (document.getElementById('rights').value == 'Backdated') {
		if (document.getElementById('backDays').value != '') {
			if (document.getElementById('backDays').value == '0') {
				alert("Please provide back days other than zero");
				document.getElementById('backDays').focus();
				return false;
			}
			if (!isNumeric(document.getElementById('backDays').value)) {
				alert("Please provide the positive numeric back days");
				document.getElementById('backDays').focus();
				return false;
			}
		}
	}
<?	}  ?>

 // Added By Niaz Ahmad #2810 Connect Plus at 30-01-2008 
      <? if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1") { ?>
          	
       	if(theForm.currencyOrigin.value == "" || IsAllSpaces(theForm.currencyOrigin.value)){
    	  alert("Please provide the agent's settlement currency.");
        theForm.currencyOrigin.focus();
        return false;
    }
          <? } ?> 	

	<?
		if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
			&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1 )
		{
			/***** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents *****/
		?>	
			assocDistRadioButton = document.getElementById("letMeSelectAssocDist");
			if ( assocDistRadioButton != null && assocDistRadioButton != undefined )
			{
				if( assocDistRadioButton.checked == true )
				{
					// In this case, Atleast one distributor should be selected.
					// User is not allowed to select all the distributors.
					// If user selects all the distributors, prompt him/her to check the "All" distributors radio button.
					distListControl = document.getElementById("distList");
					totalDistributors = distListControl.options.length
					numberOfSelectedDist = countSelectedItems( distListControl );
					if ( numberOfSelectedDist < 1 )
					{
						alert( "Please select atleast one distributor." );
						assocDistRadioButton.focus();
						return false;
					}
					else if ( numberOfSelectedDist == totalDistributors )
					{
						alert( "If you want all the distributors to be associated with this agent,\nplease click on the 'All' distributors option." );
						assocDistRadioButton.focus();
						return false;
					}
				}
			}
		<?
		}
		/***** End of code against #3428: Connect Plus - Select Distributors for Agents *****/
	?>

	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
   
function clearExpDate() {	// [by Jamshed]
	document.frmAgents.dDate.value = "";	
}

function isNumeric(strString) {
   //  check for valid numeric strings	
   
	var strValidChars = "0123456789";
	var strChar;
	var blnResult = true;
	
	if (strString.length == 0) {
		return false;
	}
	
	//  test strString consists of valid characters listed above
	for (i = 0; i < strString.length && blnResult == true; i++)	{
	  strChar = strString.charAt(i);
	  if (strValidChars.indexOf(strChar) == -1)	{
	     blnResult = false;
	  }
	}
	return blnResult;
}

function showBackDays() {
	if (document.getElementById('rights').value == 'Backdated') {
		document.getElementById('howmanybackdays').style.visibility = 'visible';
		document.getElementById('howmanybackdays').style.position = 'relative';
	} else {
		document.getElementById('backDays').value = '';
		document.getElementById('howmanybackdays').style.visibility = 'hidden';
		document.getElementById('howmanybackdays').style.position = 'absolute';
	}
}

/**
 * Show hide the replace default distributor option
 * @Ticket# 3506
 */
function showHideReplaceBlock()
{
	if(document.getElementById("defaultDistrib").value == 'Y') 
		$("#showReplaceBlock").show();
	else
		$("#showReplaceBlock").hide();
}


function checkFocus(){
	
<? if($_GET["ida"] == "Y"){ ?>
		frmAgents.agentCompany.focus();
	<? }else{ ?>
	document.getElementById('manualAgentNum').focus();
 <? } ?>
}   
	// end of javascript -->
	</script>
	<?
		if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
			&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1 )
		{
			/***** Script added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents *****/
			$onloadJS = "onLoadJS(); ";
		?>
			<script>
				
				function onLoadJS()
				{
					updateAssocDistControls();
				}
		
				function updateAssocDistControls()
				{
					if( document.getElementById("letMeSelectAssocDist").checked == true )
						document.getElementById("distList").disabled = false;
					else
						document.getElementById("distList").disabled = true;
				}
		
				function countSelectedItems( listControl )
				{
					totalItems = listControl.options.length;
					numberOfSelectedItems = 0;
					for (i = 0; i < totalItems; i++)
					{
						if ( listControl.options[i].selected == true )
							numberOfSelectedItems++;
					}
					return numberOfSelectedItems;
				}
				</script>
		<?
		}
		/***** End of code against #3428: Connect Plus - Select Distributors for Agents ****/
	?>

<script language="javascript">

function complianceValidation ()
{
	var agentContactPerson = document.getElementById("agentContactPerson").value;
	contactPerson = escape(agentContactPerson);
	var btnCompliance = escape(document.getElementById("check_compliance").value);
	var agentType = '<?=$agentType2?>';
	if(contactPerson == '')
	{
		$("#comp_id").show();
		$("#compliance_return").html("<strong><font color='red' size='1'>Please provide contact person name</font></strong>");
		return false;
	}else{
	alert("hello");
		$("#comp_id").show();
		$("#compliance_return").load("<?=$url_address?>/compliance/server.php?userType=SA&list=all&payexCompliance(contactPerson)&firstname="+contactPerson+'&btnCompliance='+ btnCompliance+
		'&agentType='+agentType);
	}
}
$(document).ready(function(){

	//userComplianceCheck();
	
	/*$("#btnSubmit").click(function()
	{
		userComplianceCheck();
	});*/
});

function userComplianceCheck(){
var bolReturn;
var agentContactPerson = document.getElementById("agentContactPerson").value;
contactPerson = escape(agentContactPerson);
var agentType = '<?=$agentType2?>';

if(document.getElementById("chkBlock"))
	var chkBlock = document.getElementById("chkBlock").value;

if(contactPerson == '')
{
	$("#comp_id").show();
	$("#compliance_return").html("<strong><font color='red' size='1'>Please provide contact person name</font></strong>");
	return false;
}
		$.ajax({
		   type: "GET",
		   url: "<?=$url_address?>/compliance/server.php?userType=SA&list=all",
		   data: "name="+contactPerson+"&agentType="+agentType,
		   async: false,
		   beforeSend: function(XMLHttpRequest){
		   	$("#comp_rs").html("");
		   },
		   error: function(XMLHttpRequest, textStatus, errorThrow)
		   {
		   		bolReturn = false;
		   },
		   success: function(msg){
				if(msg != '0' )
				{
					$("#comp_rs").html("<strong><font color='red' size='2'>"+msg+"</font></strong>");
				}
				
				if(document.getElementById("checkType"))
				{
					var checkType = document.getElementById("checkType").value;
				} 
				 if(checkType == "E")
				{
					$("#comp_message").html("<strong><font color='red' size='2'>"+msg+"</font></strong>");
					 bolReturn = false;
					
				}
				else if(checkType == "P")
				{
					$("#partial_match").show();	
					
					bolReturn =false;
				}	
		   }
		 });		
	//return false;
	return bolReturn;
}

</script>	
</head>
<body onLoad="<?=(!empty($onloadJS) ? $onloadJS : ""); ?> checkFocus();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font class="topbar_tex">Add <? if($_GET["ida"] == "Y"){ echo ($_GET["type"] == "sub" ? "Sub" : "Super");?> Distributor<? }else{echo ($_GET["type"] == "sub" ? "Sub" : "Super");?>
 <?=__("Agent"); ?>
    <? }?></font></strong><? //debug( selectMultiRecords("select * from admin") ); ?></td>
  </tr>
  <form name="frmAgents" action="add-agent-conf.php?type=<?=$_GET["type"]?>&ida=<?=$_GET["ida"]?>" method="post" onSubmit="return checkForm(this);" enctype="multipart/form-data">
	<input type="hidden" name="agent_type" value="<?=$_GET["type"]; ?>">
  <tr>
    <td align="center">
		<table width="528" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr> 
            <td colspan="2" bgcolor="#000000"> 
              <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td align="center" bgcolor="#EEEEEE"> <font color="#000066" size="2"><strong>Add 
                    <? if($_GET["ida"] == "Y"){ echo ($_GET["type"] == "sub" ? "Sub" : "Super");?>
                    Distributor 
                    <? }else{echo ($_GET["type"] == "sub" ? "Sub" : "Super");?>
                    <? echo __("Agent"); ?>
                    <? }?>
                    </strong></font></td>
                </tr>
              </table>
            </td>
          </tr>
          <? if ($_GET["msg"] != ""){ ?>
          <tr bgcolor="#ededed"> 
            <td colspan="2"> 
              <table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr> 
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><font color="#FF0000"> 
                    <? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; $_SESSION['error'] = ""; ?>
                    </font></td>
                </tr>
              </table>
            </td>
          </tr>
          <? } ?>
          <tr bgcolor="#EEEEEE"> 
            <td height="19" colspan="2" align="center"><font color="#FF0000">* 
              Compulsory Fields</font></td>
          </tr>
          <?
        
		if($_GET["type"] == "sub")
		{
		$nAgentType = $_GET["type"];
		?>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>
              <? if($_GET["ida"] == "Y"){?>
              Select Distributor<font color="#ff0000">*</font>
              <? }else {?>
              Select <? __("Agent");?><font color="#ff0000">*</font>
              <? }?>
              </strong></font></td>
            <td>
              <select name="supAgentID" id="supAgentID" style="font-family:verdana; font-size: 11px; width:226">
                <option value="">- Select One -</option>
                <?
					if($_GET["ida"] == "Y")
					{
						
						$agents = selectMultiRecords("select userID, agentCompany, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent = 'ONLY' order by agentCompany");
					}
					else
					{
						$agents = selectMultiRecords("select userID, agentCompany, username from ".TBL_ADMIN_USERS." where parentID > 0 and agentType='Supper' and isCorrespondent != 'ONLY' order by agentCompany");
					}
					for ($i=0; $i < count($agents); $i++){
				if($agents[$i]["userID"] == $_SESSION["supAgentID"])
				{
				?>
                <option value="<? echo $_SESSION["supAgentID"]; ?>"selected>
                <? echo $agents[$i]["username"]." [".$agents[$i]["agentCompany"]."]"; ?>
                </option>
                <?
				}
				else
				{
				?>
                <option value="<? echo $agents[$i]["userID"]; ?>">
                <? echo $agents[$i]["username"]." [".$agents[$i]["agentCompany"]."]"; ?>
                </option>
                <?
			}
					}
				?>
              </select>
            </td>
          </tr>
          <?
		}
		
		if($_GET["type"] == "" && $_GET["ida"] != "Y" )
		{
			if(IS_BRANCH_MANAGER){
		$nAgentType = $_GET["type"];
			if($agentType2 != 'Branch Manager'){
		?>
		
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>
			<?php echo $managerLabel;?>
              </strong></font></td>
            <td>
			<?php
			if(!empty($populateUser))
				echo $populateUser;
			else{
			?>
              <select name="supAgentID" id="supAgentID" style="font-family:verdana; font-size: 11px; width:226" >
                <option value="">- Select One -</option>
                <?
					
						
						$agents = selectMultiRecords("select userID,name,username, agentCompany, agentContactPerson from ".TBL_ADMIN_USERS." where adminType='Branch Manager'");
					
						
					for ($i=0; $i < count($agents); $i++){
				if($agents[$i]["userID"] == $_SESSION["supAgentID"])
				{
				?>
                <option value="<? echo $_SESSION["supAgentID"]; ?>"selected>
                <? echo $agents[$i]["name"]." [".$agents[$i]["username"]."]"; ?>
                </option>
                <?
				}
				else
				{
				?>
                <option value="<? echo $agents[$i]["userID"]; ?>">
                <? echo $agents[$i]["name"]." [".$agents[$i]["username"]."]"; ?>
                </option>
                <?
			}
					}
				?>
              </select>
			 <?php }?>
            </td>
          </tr>
          <?
        }
	}
	if($agentUserTypeFlag){
	?>
	<tr bgcolor="#ededed"> 
		<td><font color="#005b90"><strong>Account Type</strong></font></td>
		<td>
	   <select id="agentUserType" name="agentUserType">
			<option value="Introducer">Introducer</option>
			<option value="Management Company">Management Company</option>
		</select>
              <script language="JavaScript">
         	SelectOption(document.forms[0].agentUserType, "<?=$_SESSION["agentUserType"]; ?>");
                                </script>
		</td>
	</tr>
<?php 
	}
}?>		
		
		  <!-- Added by Niaz Ahmad #3096 at 17-04-2008 -->
		  <?
		  if (CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y"){
		  	
		  	$loginName = __("Agent") ." Login Name";
		  }elseif(CONFIG_MANUAL_DISTRIBUTOR_NUMBER == "1" && $_GET["ida"] == "Y"){
		  	$loginName = "Distributor Login Name";
		  	}
		  ?>
		  
		
		<?	if (CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y" || CONFIG_MANUAL_DISTRIBUTOR_NUMBER == "1" && $_GET["ida"] == "Y") {  ?>
		      <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong> <?=$loginName?><font color="#ff0000">*</font></strong></font></td>
            <td>
              <input type="text" name="manualAgentNum" id="manualAgentNum" value="<?=stripslashes($_SESSION["manualAgentNum"]); ?>" size="35" maxlength="100">
            </td>
          </tr>
		<?	} ?>
		   
		 
		   
		
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>
              <? if($_GET["ida"] == "Y"){?>
              Distributor Name<font color="#ff0000">*</font> 
              <? }else {?>
              Company Name<font color="#ff0000">*</font>
              <? }?>
              </strong></font></td>
            <td width="361">
              <input type="text" name="agentCompany" value="<?=stripslashes($_SESSION["agentCompany"]); ?>" size="35" maxlength="255">
            </td>
            </tr>
          <? if(CONFIG_DISTRIBUTOR_ALIAS == "1" && $_GET["ida"] == "Y") {?>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong> Distributor Alias</strong></font></td>
            <td>
              <input type="text" name="distAlias" value="<?=stripslashes($_SESSION["distAlias"]); ?>" size="35" maxlength="100">
            </td>
          </tr>
           <? } ?> 
         
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong> Contact Person<font color="#ff0000">*</font></strong></font></td>
            <td>
           <input type="text" name="agentContactPerson" id="agentContactPerson" value="<?=stripslashes($_SESSION["agentContactPerson"]); ?>" size="35" maxlength="100">
			 <? if($complianceFlag){?>
			  <br /><button type="button" name="check_compliance" id="check_compliance" onClick="complianceValidation();">Check Compliance</button>
			  <? } ?>
			  
            </td>
          </tr>
		  <? if($complianceFlag){?>
		 
		   <tr bgcolor="#ededed" style="display:none" id="comp_id"> 
		    <td width="156"><font color="#005b90"><strong> &nbsp;</strong></font></td>
            <td><font color="#ff0000"><strong><div id="compliance_return"></div></strong></font></td>
          </tr>
		
		  <tr bgcolor="#ededed" id="partial_match"  style="display:none" >
				<td colspan="2"> 
						<table align="center">
						<tr>
				          <td align="center" colspan="2">
						  <font size="2"><strong>Partial Match</strong></strong></font><br>
						  <font color="#FF0000">If you want block this user then Tick on Block Box</font>
						  </td>
						<tr bgcolor="#ededed">
				  			<td colspan="2"><div id="comp_rs"></div></td>
				  		</tr>  
						<tr bgcolor="#ededed">
						 <td><font color="#005b90"><strong>Block</strong></font></td>
						 <td ><input type=checkbox name=chkBlock value="Y" id="chkBlock"></td>
					   </tr>    
				   
					 <tr bgcolor="#ededed">
						<td><font color="#005b90"><strong>Remarks</strong></font></td>
						<td> <TEXTAREA Name="compliance_remarks" rows="4" cols="50" id="compliance_remarks"></TEXTAREA></td>
					</tr>   
				 </table>
				 </td>
			</tr>
		  <? } ?>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>Address Line 
              1<font color="#ff0000">*</font></b></font></td>
            <td>
              <input type="text" name="agentAddress" value="<?=stripslashes($_SESSION["agentAddress"]); ?>" size="35" maxlength="254">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>Address Line 
              2</b></font></td>
            <td>
              <input type="text" name="agentAddress2" value="<?=stripslashes($_SESSION["agentAddress2"]); ?>" size="35" maxlength="254">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>Country<font color="#ff0000">*</font></b></font></td>
            <td>
              <SELECT name="Country" style="font-family:verdana; font-size: 11px" onChange='$("#cityDiv").load("add-agent.php?ajaxCity=get&cntry="+this.value);<? if($_GET["ida"] == "Y" && $countryBasedFlag){?>$("#servicesDiv").load("add-agent.php?ajaxServices=get&cntry="+this.value); <? } ?>' > 
                <OPTION value="">- Select Country-</OPTION>
                
                <?
                if(CONFIG_ENABLE_ORIGIN == "1"){
                	
                	if($_GET["ida"] == "Y"){
                		$countryTypes = " and countryType like '%destination%' ";
                	}else{
                		$countryTypes = " and countryType like '%origin%' ";
                		$agentCountry = " ";
                	}	
                                	
                }else{
                		$countryTypes = " ";
                		$agentCountry = " and countryName ='United Kingdom' ";
                	}
                if(CONFIG_COUNTRY_SERVICES_ENABLED){
	                if($_GET["ida"] == "Y"){
										$serviceTable = TBL_SERVICE;
										if($countryBasedFlag){
											$serviceTable = TBL_SERVICE_NEW;
										}
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".$serviceTable." where  countryId = toCountryId  $countryTypes order by countryName");
									}
									else
									{
										$countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes $agentCountry order by countryName";
										if($countryBasedFlag){
											$countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY.", ".TBL_SERVICE_NEW." where 1 and  countryId = fromCountryId $countryTypes $agentCountry order by countryName";
										}
										$countires = selectMultiRecords($countiresQuery);
									}
								}else
								{
										if($_GET["ida"] == "Y"){
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
									}
									else
									{
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes $agentCountry order by countryName");
									}
								}
								
								
					for ($i=0; $i < count($countires); $i++){
				?>
                <OPTION value="<?=$countires[$i]["countryName"]; ?>">
                <?=$countires[$i]["countryName"]; ?>
                </OPTION>
                <?
					}
				?>
              </SELECT>
              <script language="JavaScript">
         	SelectOption(document.forms[0].Country, "<?=$_SESSION["Country"]; ?>");
                                </script>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>City</b></font>
			<? if(CONFIG_CITY_MANDATORY == "1"){ ?><font color="#ff0000">*</font><? } ?>
			</td>
            <td>
			    <? if(CONFIG_BENEFICIARY_CITY_DROPDOWN == "1") { ?>
						<select name="City" id="cityDiv">
							<option value="">- Select City-</option>
						</select>
						<script language="JavaScript">
				         	SelectOption(document.forms[0].City, "<?=$_SESSION["City"]; ?>");
                        </script>
				<? } else { ?>
				     <input type="text" name="City" value="<?=$_SESSION[City];?>" size="35" maxlength="255">
			    <? } ?>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b> 
              <? if ($_SESSION["Country"] == "United States") echo "Zip"; else echo "Postal"; ?>
              Code</b></font>
			  	<?
					/***** Code added by Usman Ghani aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
					if ( defined("CONFIG_POST_CODE_MENDATORY")
						&& CONFIG_POST_CODE_MENDATORY == 1 )
					{
					?>
						<strong><font color="#ff0000">*</font></strong>
					<?
					}
					/***** End of code aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
				?></td>
            <td>
              <input type="text" name="postcode" value="<?=stripslashes($_SESSION["agentZip"]); ?>" size="35" maxlength="15">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Phone<font color="#ff0000">*</font></strong></font></td>
            <td>
              <input type="text" name="agentPhone" value="<?=stripslashes($_SESSION["agentPhone"]); ?>" size="35" maxlength="32">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Fax</strong></font></td>
            <td>
              <input type="text" name="agentFax" value="<?=stripslashes($_SESSION["agentFax"]); ?>" size="35" maxlength="32">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Email</strong></font></td>
            <td>
              <input type="text" name="email" value="<?=stripslashes($_SESSION["email"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Company URL</strong></font></td>
            <td>
              <input type="text" name="agentURL" value="<?=stripslashes($_SESSION["agentURL"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
		  <?php if(CONFIG_HIDE_MSB_AGENT!="1"){?>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>MSB Number</strong></font>
              <? if(CONFIG_REMOVE_MSB_TEXT_FROM_ADD_AGENT_DISTRIBUTOR !="1"){?>
			  <br><font size="1">OR (State Distributor license or permission from 
              Ministry of Finance)</font>
			  <? } ?>
			  </td>
            <td valign="top">
              <input type="text" name="agentMSBNumber" value="<?=stripslashes($_SESSION["agentMSBNumber"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>MSB Expiry</strong></font></td>
            <td> 
              <SELECT name="msbDay" size="1" style="font-family:verdana; font-size: 11px">
                <OPTION value="">Day</OPTION>
                <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value=\"$Day\">$Day</option>\n";
						}
					  ?>
              </select>
              <script language="JavaScript">
         			SelectOption(document.forms[0].msbDay, "<?echo $msbDay;?>");
          		</script>
              <SELECT name="msbMonth" size="1" style="font-family:verdana; font-size: 11px">
                <OPTION value="">Month</OPTION>
                <OPTION value="01">Jan</OPTION>
                <OPTION value="02">Feb</OPTION>
                <OPTION value="03">Mar</OPTION>
                <OPTION value="04">Apr</OPTION>
                <OPTION value="05">May</OPTION>
                <OPTION value="06">Jun</OPTION>
                <OPTION value="07">Jul</OPTION>
                <OPTION value="08">Aug</OPTION>
                <OPTION value="09">Sep</OPTION>
                <OPTION value="10">Oct</OPTION>
                <OPTION value="11">Nov</OPTION>
                <OPTION value="12">Dec</OPTION>
              </SELECT>
              <script language="JavaScript">
         	SelectOption(document.forms[0].msbMonth, "<? echo $msbMonth; ?>");
          </script>
              <SELECT name="msbYear" size="1" style="font-family:verdana; font-size: 11px">
                <OPTION value="" selected>Year</OPTION>
                <?
								  	$cYear=date("Y");
								  	for ($Year=$cYear; $Year<=($cYear+20);$Year++)
									{
										echo "<option value=\"$Year\">$Year</option>\n";
									}
		  ?>
              </SELECT>
              <script language="JavaScript">
         	SelectOption(document.forms[0].msbYear, "<?echo $msbYear?>");
          </script>
            </td>
          </tr>
		  <?php }?>
          <? if(CONFIG_AGENT_HOUSE_NUMBER == "1"){?>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>House Number</strong></font></td>
            <td valign="top">
              <input type="text" name="agentHouseNumber" value="<?=stripslashes($_SESSION["agentHouseNumber"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <? } ?>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Company Registration 
              Number</strong></font></td>
            <td>
              <input type="text" name="agentCompRegNumber" value="<?=stripslashes($_SESSION["agentCompRegNumber"]); ?>" size="35" maxlength="100">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Company Director</strong></font></td>
            <td>
              <input type="text" name="agentCompDirector" value="<?=stripslashes($_SESSION["agentCompDirector"]); ?>" size="35" maxlength="100">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Director Address</strong></font></td>
            <td>
              <input type="text" name="agentDirectorAdd" value="<?=stripslashes($_SESSION["agentDirectorAdd"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><strong>Director 
              Proof ID</strong></font></td>
            <td valign="top">
              <input type="radio" name="agentProofID" value="Passport" <? if ($_SESSION["agentProofID"] == "Passport" || $_SESSION["agentProofID"] == "") echo "checked"; ?>>
              Passport <br>
              <input type="radio" name="agentProofID" value="Driving License" <? if ($_SESSION["agentProofID"] == "Driving License") echo "checked"; ?>>
              Driving License <br>
              <input type="radio" name="agentProofID" value="Other" <? if ($_SESSION["agentProofID"] == "Other") echo "checked"; ?>>
              Other please specify 
              <input type="text" name="otherProofID" value="<?=$_SESSION[otherProofID]; ?>" size="15">
            </td>
          </tr>
		  <? if($agentIDNumberFlag){?>
			  <tr bgcolor="#ededed"> 
				<td width="156"><font color="#005b90"><strong>ID Number</strong></font></td>
				<td>
				  <input type="text" name="agentIDNumber" value="<?=stripslashes($_SESSION["agentIDNumber"]); ?>" size="35" maxlength="255">
				</td>
			  </tr>
		  <? }?>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>ID Expiry</strong></font></td>
            <td>
         	<?	if (CONFIG_FREETEXT_IDFIELDS == '1') {  ?>
        	    <input name="dDate" type="text" id="dDate" value="<? echo (!is_array($_SESSION["dDate"]) ? $_SESSION["dDate"] : "") ?>"> &nbsp;&nbsp;&nbsp;&nbsp;<i>DD-MM-YYYY</i>
        	<?	} elseif (CONFIG_IDEXPIRY_WITH_CALENDAR == "1") {  ?>
        	    <input name="dDate" type="text" id="dDate" readonly value="<? echo (is_array($_SESSION["dDate"]) ? implode("/", $_SESSION["dDate"]) : $_SESSION["dDate"]) ?>">
        	    &nbsp;<a href="javascript:show_calendar('frmAgents.dDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>
        	    &nbsp;&nbsp;<input type="button" name="btnClearDate" value="Clear" onClick="clearExpDate();">
        	<?	} else { ?>
              <SELECT name="idDay" size="1" style="font-family:verdana; font-size: 11px">
                <OPTION value="">Day</OPTION>
                <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value=\"$Day\">$Day</option>\n";
						}
					  ?>
              </select>
              <script language="JavaScript">
         			SelectOption(document.forms[0].idDay, "<?echo $idDay;?>");
          		</script>
              <SELECT name="idMonth" size="1" style="font-family:verdana; font-size: 11px">
                <OPTION value="">Month</OPTION>
                <OPTION value="01">Jan</OPTION>
                <OPTION value="02">Feb</OPTION>
                <OPTION value="03">Mar</OPTION>
                <OPTION value="04">Apr</OPTION>
                <OPTION value="05">May</OPTION>
                <OPTION value="06">Jun</OPTION>
                <OPTION value="07">Jul</OPTION>
                <OPTION value="08">Aug</OPTION>
                <OPTION value="09">Sep</OPTION>
                <OPTION value="10">Oct</OPTION>
                <OPTION value="11">Nov</OPTION>
                <OPTION value="12">Dec</OPTION>
              </SELECT>
              <script language="JavaScript">
         	SelectOption(document.forms[0].idMonth, "<? echo $idMonth; ?>");
          </script>
              <SELECT name="idYear" size="1" style="font-family:verdana; font-size: 11px">
                <OPTION value="" selected>Year</OPTION>
                <?
								  	$cYear=date("Y");
								  	for ($Year=$cYear; $Year<=($cYear+20);$Year++)
									{
										echo "<option value=\"$Year\">$Year</option>\n";
									}
		  ?>
              </SELECT>
              <script language="JavaScript">
         	SelectOption(document.forms[0].idYear, "<?echo $idYear?>");
          </script>
      	<?	}  ?>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><strong> Document 
              Provided</strong></font></td>
            <td>
              <p> 
                <input name="agentDocumentProvided[0]" type="checkbox" id="agentDocumentProvided[0]" value="MSB License" <? echo (strstr($_SESSION["agentDocumentProvided"],"MSB License") ? "checked"  : "")?>>
                MSB License <br>
                <input name="agentDocumentProvided[1]" type="checkbox" id="agentDocumentProvided[1]" value="Company Registration Certificate" <? echo (strstr($_SESSION["agentDocumentProvided"],"Company Registration Certificate") ? "checked"  : "")?>>
                Company Registration Certificate <br>
                <input name="agentDocumentProvided[2]" type="checkbox" id="agentDocumentProvided[2]" value="Utility Bill" <? echo (strstr($_SESSION["agentDocumentProvided"],"Utility Bill") ? "checked"  : "")?>>
                Utility Bill <br>
                <input name="agentDocumentProvided[3]" type="checkbox" id="agentDocumentProvided[3]" value="Director Proof of ID" <? echo (strstr($_SESSION["agentDocumentProvided"],"Director Proof of ID") ? "checked"  : "")?>>
                Director Proof of ID</p>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Bank Name</strong></font></td>
            <td>
              <input type="text" name="agentBank" value="<?=stripslashes($_SESSION["agentBank"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Bank Account Name</strong></font></td>
            <td>
              <input type="text" name="agentAccountName" value="<?=stripslashes($_SESSION["agentAccountName"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Bank Account Number</strong></font></td>
            <td>
              <input type="text" name="agentAccounNumber" value="<?=stripslashes($_SESSION["agentAccounNumber"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Bank Branch Code</strong></font></td>
            <td>
              <input type="text" name="agentBranchCode" value="<?=stripslashes($_SESSION["agentBranchCode"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Bank Branch Name</strong></font></td>
            <td>
              <input type="text" name="agentBranchName" value="<?=stripslashes($_SESSION["agentBranchBranch"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Bank Account Type</strong></font></td>
            <td>
              <select name="agentAccountType">
                <option value="Current">Current</option>
                <option value="Saving">Saving</option>
                <option value="Other">Other</option>
              </select>
              <script language="JavaScript">
						         			SelectOption(document.forms[0].agentAccountType, "<? echo $_SESSION["agentAccountType"]; ?>");
						          	  </script>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" height="24"><font color="#005b90"><strong>Account 
              Currency</strong></font></td>
            <td height="24"> 
              <SELECT NAME="agentCurrency" style="font-family:verdana; font-size: 11px">
                <OPTION VALUE="USD" SELECTED>US Dollar</option>
                <OPTION VALUE="AFA">Afghanistan Afghani</option>
                <OPTION VALUE="ALL">Albanian Lek</option>
                <OPTION VALUE="DZD">Algerian Dinar</option>
                <OPTION VALUE="ADF">Andorran Franc</option>
                <OPTION VALUE="ADP">Andorran Peseta</option>
                <OPTION VALUE="AON">Angolan New Kwanza</option>
                <OPTION VALUE="ARS">Argentine Peso</option>
                <OPTION VALUE="AWG">Aruban Florin</option>
                <OPTION VALUE="AUD">Australian Dollar</option>
                <OPTION VALUE="ATS">Austrian Schilling</option>
                <OPTION VALUE="BSD">Bahamian Dollar </option>
                <OPTION VALUE="BHD">Bahraini Dinar</option>
                <OPTION VALUE="BDT">Bangladeshi Taka</option>
                <OPTION VALUE="BBD">Barbados Dollar</option>
                <OPTION VALUE="BEF">Belgian Franc</option>
                <OPTION VALUE="BZD">Belize Dollar</option>
                <OPTION VALUE="BMD">Bermudian Dollar</option>
                <OPTION VALUE="BTN">Bhutan Ngultrum</option>
                <OPTION VALUE="BOB">Bolivian Boliviano</option>
                <OPTION VALUE="BWP">Botswana Pula</option>
                <OPTION VALUE="BRL">Brazilian Real</option>
                <OPTION VALUE="GBP">British Pound</option>
                <OPTION VALUE="BND">Brunei Dollar</option>
                <OPTION VALUE="BGL">Bulgarian Lev</option>
                <OPTION VALUE="BIF">Burundi Franc</option>
                <OPTION VALUE="XOF">CFA Franc BCEAO</option>
                <OPTION VALUE="XAF">CFA Franc BEAC</option>
                <OPTION VALUE="XPF">CFP Franc</option>
                <OPTION VALUE="KHR">Cambodian Riel</option>
                <OPTION VALUE="CAD">Canadian Dollar</option>
                <OPTION VALUE="CVE">Cape Verde Escudo</option>
                <OPTION VALUE="KYD">Cayman Islands Dollar</option>
                <OPTION VALUE="CLP">Chilean Peso</option>
                <OPTION VALUE="CNY">Chinese Yuan Renminbi</option>
                <OPTION VALUE="COP">Colombian Peso</option>
                <OPTION VALUE="KMF">Comoros Franc</option>
                <OPTION VALUE="CRC">Costa Rican Colon</option>
                <OPTION VALUE="HRK">Croatian Kuna</option>
                <OPTION VALUE="CUP">Cuban Peso</option>
                <OPTION VALUE="CYP">Cyprus Pound</option>
                <OPTION VALUE="CZK">Czech Koruna</option>
                <OPTION VALUE="DKK">Danish Krone</option>
                <OPTION VALUE="DJF">Djibouti Franc</option>
                <OPTION VALUE="DOP">Dominican R. Peso</option>
                <OPTION VALUE="NLG">Dutch Guilder</option>
                <OPTION VALUE="XEU">ECU</option>
                <OPTION VALUE="XCD">East Caribbean Dollar</option>
                <OPTION VALUE="ECS">Ecuador Sucre</option>
                <OPTION VALUE="EGP">Egyptian Pound</option>
                <OPTION VALUE="SVC">El Salvador Colon</option>
                <OPTION VALUE="EEK">Estonian Kroon</option>
                <OPTION VALUE="ETB">Ethiopian Birr</option>
                <OPTION VALUE="EUR">Euro</option>
                <OPTION VALUE="FKP">Falkland Islands Pound</option>
                <OPTION VALUE="FJD">Fiji Dollar</option>
                <OPTION VALUE="FIM">Finnish Markka</option>
                <OPTION VALUE="FRF">French Franc</option>
                <OPTION VALUE="GMD">Gambian Dalasi</option>
                <OPTION VALUE="DEM">German Mark</option>
                <OPTION VALUE="GHC">Ghanaian Cedi</option>
                <OPTION VALUE="GIP">Gibraltar Pound</option>
                <OPTION VALUE="XAU">Gold (oz.)</option>
                <OPTION VALUE="GRD">Greek Drachma</option>
                <OPTION VALUE="GTQ">Guatemalan Quetzal</option>
                <OPTION VALUE="GNF">Guinea Franc</option>
                <OPTION VALUE="GYD">Guyanese Dollar</option>
                <OPTION VALUE="HTG">Haitian Gourde</option>
                <OPTION VALUE="HNL">Honduran Lempira</option>
                <OPTION VALUE="HKD">Hong Kong Dollar</option>
                <OPTION VALUE="HUF">Hungarian Forint</option>
                <OPTION VALUE="ISK">Iceland Krona</option>
                <OPTION VALUE="INR">Indian Rupee</option>
                <OPTION VALUE="IDR">Indonesian Rupiah</option>
                <OPTION VALUE="IRR">Iranian Rial</option>
                <OPTION VALUE="IQD">Iraqi Dinar</option>
                <OPTION VALUE="IEP">Irish Punt</option>
                <OPTION VALUE="ILS">Israeli New Shekel</option>
                <OPTION VALUE="ITL">Italian Lira</option>
                <OPTION VALUE="JMD">Jamaican Dollar</option>
                <OPTION VALUE="JPY">Japanese Yen</option>
                <OPTION VALUE="JOD">Jordanian Dinar</option>
                <OPTION VALUE="KZT">Kazakhstan Tenge</option>
                <OPTION VALUE="KES">Kenyan Shilling</option>
                <OPTION VALUE="KWD">Kuwaiti Dinar</option>
                <OPTION VALUE="LAK">Lao Kip</option>
                <OPTION VALUE="LVL">Latvian Lats</option>
                <OPTION VALUE="LBP">Lebanese Pound</option>
                <OPTION VALUE="LSL">Lesotho Loti</option>
                <OPTION VALUE="LRD">Liberian Dollar</option>
                <OPTION VALUE="LYD">Libyan Dinar</option>
                <OPTION VALUE="LTL">Lithuanian Litas</option>
                <OPTION VALUE="LUF">Luxembourg Franc</option>
                <OPTION VALUE="MOP">Macau Pataca</option>
                <OPTION VALUE="MGF">Malagasy Franc</option>
                <OPTION VALUE="MWK">Malawi Kwacha</option>
                <OPTION VALUE="MYR">Malaysian Ringgit</option>
                <OPTION VALUE="MVR">Maldive Rufiyaa</option>
                <OPTION VALUE="MTL">Maltese Lira</option>
                <OPTION VALUE="MRO">Mauritanian Ouguiya</option>
                <OPTION VALUE="MUR">Mauritius Rupee</option>
                <OPTION VALUE="MXN">Mexican Peso</option>
                <OPTION VALUE="MNT">Mongolian Tugrik</option>
                <OPTION VALUE="MAD">Moroccan Dirham</option>
                <OPTION VALUE="MZM">Mozambique Metical</option>
                <OPTION VALUE="MMK">Myanmar Kyat</option>
                <OPTION VALUE="ANG">NL Antillian Guilder</option>
                <OPTION VALUE="NAD">Namibia Dollar</option>
                <OPTION VALUE="NPR">Nepalese Rupee</option>
                <OPTION VALUE="NZD">New Zealand Dollar</option>
                <OPTION VALUE="NIO">Nicaraguan Cordoba Oro</option>
                <OPTION VALUE="NGN">Nigerian Naira</option>
                <OPTION VALUE="KPW">North Korean Won</option>
                <OPTION VALUE="NOK">Norwegian Kroner</option>
                <OPTION VALUE="OMR">Omani Rial</option>
                <OPTION VALUE="PKR">Pakistan Rupee</option>
                <OPTION VALUE="XPD">Palladium (oz.)</option>
                <OPTION VALUE="PAB">Panamanian Balboa</option>
                <OPTION VALUE="PGK">Papua New Guinea Kina</option>
                <OPTION VALUE="PYG">Paraguay Guarani</option>
                <OPTION VALUE="PEN">Peruvian Nuevo Sol</option>
                <OPTION VALUE="PHP">Philippine Peso</option>
                <OPTION VALUE="XPT">Platinum (oz.)</option>
                <OPTION VALUE="PLN">Polish Zloty</option>
                <OPTION VALUE="PTE">Portuguese Escudo</option>
                <OPTION VALUE="QAR">Qatari Rial</option>
                <OPTION VALUE="ROL">Romanian Lei</option>
                <OPTION VALUE="RUB">Russian Rouble</option>
                <OPTION VALUE="WST">Samoan Tala</option>
                <OPTION VALUE="STD">Sao Tome/Principe Dobra</option>
                <OPTION VALUE="SAR">Saudi Riyal</option>
                <OPTION VALUE="SCR">Seychelles Rupee</option>
                <OPTION VALUE="SLL">Sierra Leone Leone</option>
                <OPTION VALUE="XAG">Silver (oz.)</option>
                <OPTION VALUE="SGD">Singapore Dollar</option>
                <OPTION VALUE="SKK">Slovak Koruna</option>
                <OPTION VALUE="SIT">Slovenian Tolar</option>
                <OPTION VALUE="SBD">Solomon Islands Dollar</option>
                <OPTION VALUE="SOS">Somali Shilling</option>
                <OPTION VALUE="ZAR">South African Rand</option>
                <OPTION VALUE="KRW">South-Korean Won</option>
                <OPTION VALUE="ESP">Spanish Peseta</option>
                <OPTION VALUE="LKR">Sri Lanka Rupee</option>
                <OPTION VALUE="SHP">St. Helena Pound</option>
                <OPTION VALUE="SDD">Sudanese Dinar</option>
                <OPTION VALUE="SDP">Sudanese Pound</option>
                <OPTION VALUE="SRG">Suriname Guilder</option>
                <OPTION VALUE="SZL">Swaziland Lilangeni</option>
                <OPTION VALUE="SEK">Swedish Krona</option>
                <OPTION VALUE="CHF">Swiss Franc</option>
                <OPTION VALUE="SYP">Syrian Pound</option>
                <OPTION VALUE="TWD">Taiwan Dollar</option>
                <OPTION VALUE="TZS">Tanzanian Shilling</option>
                <OPTION VALUE="THB">Thai Baht</option>
                <OPTION VALUE="TOP">Tonga Pa'anga</option>
                <OPTION VALUE="TTD">Trinidad/Tobago Dollar</option>
                <OPTION VALUE="TND">Tunisian Dinar</option>
                <OPTION VALUE="TRL">Turkish Lira</option>
                <OPTION VALUE="AED">UAE Dirham</option>
                <OPTION VALUE="UGS">Uganda Shilling</option>
                <OPTION VALUE="UAH">Ukraine Hryvnia</option>
                <option value="GBP">United Kingdom Pounds</option>
                <OPTION VALUE="UYP">Uruguayan Peso</option>
                <OPTION VALUE="VUV">Vanuatu Vatu</option>
                <OPTION VALUE="VEB">Venezuelan Bolivar</option>
                <OPTION VALUE="VND">Vietnamese Dong</option>
                <OPTION VALUE="YUN">Yugoslav Dinar</option>
                <OPTION VALUE="ZMK">Zambian Kwacha</option>
                <OPTION VALUE="ZWD">Zimbabwe Dollar</option>
              </SELECT>
              <script language="JavaScript">
						         			SelectOption(document.forms[0].agentCurrency, "<? echo $_SESSION["agentCurrency"]; ?>");
						          	  </script>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" height="24"><font color="#005b90"><strong>Commission Currency</strong></font></td>
            <td height="24"> 
              <SELECT NAME="agentCommCurrency" id="agentCommCurrency" style="font-family:verdana; font-size: 11px">
                <OPTION VALUE="USD" SELECTED>US Dollar</option>
                <OPTION VALUE="AFA">Afghanistan Afghani</option>
                <OPTION VALUE="ALL">Albanian Lek</option>
                <OPTION VALUE="DZD">Algerian Dinar</option>
                <OPTION VALUE="ADF">Andorran Franc</option>
                <OPTION VALUE="ADP">Andorran Peseta</option>
                <OPTION VALUE="AON">Angolan New Kwanza</option>
                <OPTION VALUE="ARS">Argentine Peso</option>
                <OPTION VALUE="AWG">Aruban Florin</option>
                <OPTION VALUE="AUD">Australian Dollar</option>
                <OPTION VALUE="ATS">Austrian Schilling</option>
                <OPTION VALUE="BSD">Bahamian Dollar </option>
                <OPTION VALUE="BHD">Bahraini Dinar</option>
                <OPTION VALUE="BDT">Bangladeshi Taka</option>
                <OPTION VALUE="BBD">Barbados Dollar</option>
                <OPTION VALUE="BEF">Belgian Franc</option>
                <OPTION VALUE="BZD">Belize Dollar</option>
                <OPTION VALUE="BMD">Bermudian Dollar</option>
                <OPTION VALUE="BTN">Bhutan Ngultrum</option>
                <OPTION VALUE="BOB">Bolivian Boliviano</option>
                <OPTION VALUE="BWP">Botswana Pula</option>
                <OPTION VALUE="BRL">Brazilian Real</option>
                <OPTION VALUE="GBP">British Pound</option>
                <OPTION VALUE="BND">Brunei Dollar</option>
                <OPTION VALUE="BGL">Bulgarian Lev</option>
                <OPTION VALUE="BIF">Burundi Franc</option>
                <OPTION VALUE="XOF">CFA Franc BCEAO</option>
                <OPTION VALUE="XAF">CFA Franc BEAC</option>
                <OPTION VALUE="XPF">CFP Franc</option>
                <OPTION VALUE="KHR">Cambodian Riel</option>
                <OPTION VALUE="CAD">Canadian Dollar</option>
                <OPTION VALUE="CVE">Cape Verde Escudo</option>
                <OPTION VALUE="KYD">Cayman Islands Dollar</option>
                <OPTION VALUE="CLP">Chilean Peso</option>
                <OPTION VALUE="CNY">Chinese Yuan Renminbi</option>
                <OPTION VALUE="COP">Colombian Peso</option>
                <OPTION VALUE="KMF">Comoros Franc</option>
                <OPTION VALUE="CRC">Costa Rican Colon</option>
                <OPTION VALUE="HRK">Croatian Kuna</option>
                <OPTION VALUE="CUP">Cuban Peso</option>
                <OPTION VALUE="CYP">Cyprus Pound</option>
                <OPTION VALUE="CZK">Czech Koruna</option>
                <OPTION VALUE="DKK">Danish Krone</option>
                <OPTION VALUE="DJF">Djibouti Franc</option>
                <OPTION VALUE="DOP">Dominican R. Peso</option>
                <OPTION VALUE="NLG">Dutch Guilder</option>
                <OPTION VALUE="XEU">ECU</option>
                <OPTION VALUE="XCD">East Caribbean Dollar</option>
                <OPTION VALUE="ECS">Ecuador Sucre</option>
                <OPTION VALUE="EGP">Egyptian Pound</option>
                <OPTION VALUE="SVC">El Salvador Colon</option>
                <OPTION VALUE="EEK">Estonian Kroon</option>
                <OPTION VALUE="ETB">Ethiopian Birr</option>
                <OPTION VALUE="EUR">Euro</option>
                <OPTION VALUE="FKP">Falkland Islands Pound</option>
                <OPTION VALUE="FJD">Fiji Dollar</option>
                <OPTION VALUE="FIM">Finnish Markka</option>
                <OPTION VALUE="FRF">French Franc</option>
                <OPTION VALUE="GMD">Gambian Dalasi</option>
                <OPTION VALUE="DEM">German Mark</option>
                <OPTION VALUE="GHC">Ghanaian Cedi</option>
                <OPTION VALUE="GIP">Gibraltar Pound</option>
                <OPTION VALUE="XAU">Gold (oz.)</option>
                <OPTION VALUE="GRD">Greek Drachma</option>
                <OPTION VALUE="GTQ">Guatemalan Quetzal</option>
                <OPTION VALUE="GNF">Guinea Franc</option>
                <OPTION VALUE="GYD">Guyanese Dollar</option>
                <OPTION VALUE="HTG">Haitian Gourde</option>
                <OPTION VALUE="HNL">Honduran Lempira</option>
                <OPTION VALUE="HKD">Hong Kong Dollar</option>
                <OPTION VALUE="HUF">Hungarian Forint</option>
                <OPTION VALUE="ISK">Iceland Krona</option>
                <OPTION VALUE="INR">Indian Rupee</option>
                <OPTION VALUE="IDR">Indonesian Rupiah</option>
                <OPTION VALUE="IRR">Iranian Rial</option>
                <OPTION VALUE="IQD">Iraqi Dinar</option>
                <OPTION VALUE="IEP">Irish Punt</option>
                <OPTION VALUE="ILS">Israeli New Shekel</option>
                <OPTION VALUE="ITL">Italian Lira</option>
                <OPTION VALUE="JMD">Jamaican Dollar</option>
                <OPTION VALUE="JPY">Japanese Yen</option>
                <OPTION VALUE="JOD">Jordanian Dinar</option>
                <OPTION VALUE="KZT">Kazakhstan Tenge</option>
                <OPTION VALUE="KES">Kenyan Shilling</option>
                <OPTION VALUE="KWD">Kuwaiti Dinar</option>
                <OPTION VALUE="LAK">Lao Kip</option>
                <OPTION VALUE="LVL">Latvian Lats</option>
                <OPTION VALUE="LBP">Lebanese Pound</option>
                <OPTION VALUE="LSL">Lesotho Loti</option>
                <OPTION VALUE="LRD">Liberian Dollar</option>
                <OPTION VALUE="LYD">Libyan Dinar</option>
                <OPTION VALUE="LTL">Lithuanian Litas</option>
                <OPTION VALUE="LUF">Luxembourg Franc</option>
                <OPTION VALUE="MOP">Macau Pataca</option>
                <OPTION VALUE="MGF">Malagasy Franc</option>
                <OPTION VALUE="MWK">Malawi Kwacha</option>
                <OPTION VALUE="MYR">Malaysian Ringgit</option>
                <OPTION VALUE="MVR">Maldive Rufiyaa</option>
                <OPTION VALUE="MTL">Maltese Lira</option>
                <OPTION VALUE="MRO">Mauritanian Ouguiya</option>
                <OPTION VALUE="MUR">Mauritius Rupee</option>
                <OPTION VALUE="MXN">Mexican Peso</option>
                <OPTION VALUE="MNT">Mongolian Tugrik</option>
                <OPTION VALUE="MAD">Moroccan Dirham</option>
                <OPTION VALUE="MZM">Mozambique Metical</option>
                <OPTION VALUE="MMK">Myanmar Kyat</option>
                <OPTION VALUE="ANG">NL Antillian Guilder</option>
                <OPTION VALUE="NAD">Namibia Dollar</option>
                <OPTION VALUE="NPR">Nepalese Rupee</option>
                <OPTION VALUE="NZD">New Zealand Dollar</option>
                <OPTION VALUE="NIO">Nicaraguan Cordoba Oro</option>
                <OPTION VALUE="NGN">Nigerian Naira</option>
                <OPTION VALUE="KPW">North Korean Won</option>
                <OPTION VALUE="NOK">Norwegian Kroner</option>
                <OPTION VALUE="OMR">Omani Rial</option>
                <OPTION VALUE="PKR">Pakistan Rupee</option>
                <OPTION VALUE="XPD">Palladium (oz.)</option>
                <OPTION VALUE="PAB">Panamanian Balboa</option>
                <OPTION VALUE="PGK">Papua New Guinea Kina</option>
                <OPTION VALUE="PYG">Paraguay Guarani</option>
                <OPTION VALUE="PEN">Peruvian Nuevo Sol</option>
                <OPTION VALUE="PHP">Philippine Peso</option>
                <OPTION VALUE="XPT">Platinum (oz.)</option>
                <OPTION VALUE="PLN">Polish Zloty</option>
                <OPTION VALUE="PTE">Portuguese Escudo</option>
                <OPTION VALUE="QAR">Qatari Rial</option>
                <OPTION VALUE="ROL">Romanian Lei</option>
                <OPTION VALUE="RUB">Russian Rouble</option>
                <OPTION VALUE="WST">Samoan Tala</option>
                <OPTION VALUE="STD">Sao Tome/Principe Dobra</option>
                <OPTION VALUE="SAR">Saudi Riyal</option>
                <OPTION VALUE="SCR">Seychelles Rupee</option>
                <OPTION VALUE="SLL">Sierra Leone Leone</option>
                <OPTION VALUE="XAG">Silver (oz.)</option>
                <OPTION VALUE="SGD">Singapore Dollar</option>
                <OPTION VALUE="SKK">Slovak Koruna</option>
                <OPTION VALUE="SIT">Slovenian Tolar</option>
                <OPTION VALUE="SBD">Solomon Islands Dollar</option>
                <OPTION VALUE="SOS">Somali Shilling</option>
                <OPTION VALUE="ZAR">South African Rand</option>
                <OPTION VALUE="KRW">South-Korean Won</option>
                <OPTION VALUE="ESP">Spanish Peseta</option>
                <OPTION VALUE="LKR">Sri Lanka Rupee</option>
                <OPTION VALUE="SHP">St. Helena Pound</option>
                <OPTION VALUE="SDD">Sudanese Dinar</option>
                <OPTION VALUE="SDP">Sudanese Pound</option>
                <OPTION VALUE="SRG">Suriname Guilder</option>
                <OPTION VALUE="SZL">Swaziland Lilangeni</option>
                <OPTION VALUE="SEK">Swedish Krona</option>
                <OPTION VALUE="CHF">Swiss Franc</option>
                <OPTION VALUE="SYP">Syrian Pound</option>
                <OPTION VALUE="TWD">Taiwan Dollar</option>
                <OPTION VALUE="TZS">Tanzanian Shilling</option>
                <OPTION VALUE="THB">Thai Baht</option>
                <OPTION VALUE="TOP">Tonga Pa'anga</option>
                <OPTION VALUE="TTD">Trinidad/Tobago Dollar</option>
                <OPTION VALUE="TND">Tunisian Dinar</option>
                <OPTION VALUE="TRL">Turkish Lira</option>
                <OPTION VALUE="AED">UAE Dirham</option>
                <OPTION VALUE="UGS">Uganda Shilling</option>
                <OPTION VALUE="UAH">Ukraine Hryvnia</option>
                <option value="GBP">United Kingdom Pounds</option>
                <OPTION VALUE="UYP">Uruguayan Peso</option>
                <OPTION VALUE="VUV">Vanuatu Vatu</option>
                <OPTION VALUE="VEB">Venezuelan Bolivar</option>
                <OPTION VALUE="VND">Vietnamese Dong</option>
                <OPTION VALUE="YUN">Yugoslav Dinar</option>
                <OPTION VALUE="ZMK">Zambian Kwacha</option>
                <OPTION VALUE="ZWD">Zimbabwe Dollar</option>
              </SELECT>
              <script language="JavaScript">
						         			SelectOption(document.forms[0].agentCommCurrency, "<? echo $_SESSION["agentCommCurrency"]; ?>");
						          	  </script>
            </td>
          </tr>
          <!--tr bgcolor="#ededed"> 
            <td width="156" justify="middle" height="20"><font color="#005b90"></font></td>
            <td height="20"> </td>
          </tr-->
<?		  if (CONFIG_AGENT_LIMIT == '1'){ ?>		  
		  <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>
			
	<? echo __("Agent"); ?>			
			Account Limit 
		  <? 
/**
 * To make field mandatory (CONFIG based) if it not distributor
 * @Ticket #4158
 */
		  if (CONFIG_AGENT_ACCOUNT_LIMIT == '1' && $_GET["ida"]!="Y"){ ?>
		  	<font color="#ff0000">*</font></strong></font>
		  <? } ?>
			</strong></font></td>
            <td>

              <input type="text" name="agentAccountLimit" id="agentAccountLimit" value="<?=stripslashes($_SESSION["agentAccountLimit"]); ?>" size="35" maxlength="15">
            </td>
          </tr>
        <?
		}
        if(CONFIG_USER_COMMISSION_MANAGEMENT == '1' && ($_GET["ida"] != "Y" || CONFIG_COMM_MANAGEMENT_FOR_ALL == '1')){
        }else{
        ?>
          
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Commission package</strong></font></td>
            <td>
              <select name="commPackage" id="commPackage">
                <option value="001">Fixed Amount</option>
                <option value="002">Percentage of Transaction Amount</option>
                <option value="003">Percentage of Fee</option>
                <option value="004">None</option>
              </select>
              <script language="JavaScript">
         	SelectOption(document.forms[0].commPackage, "<?=$_SESSION["commPackage"]; ?>");
                                </script>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Commission Percentage/Fee</strong></font></td>
            <td>
              <input type="text" name="agentCommission" value="<?=stripslashes($_SESSION["agentCommission"]); ?>" size="35" maxlength="6">
            </td>
          </tr>
       <? }?>
	   <!-- Added by Niaz Ahmad at 07-05-2009 @4926 -->
	   <? if(CONFIG_DISTRIBUTOR_BANK_CHARGES == "1" && $_REQUEST["ida"] == "Y") { ?>
	   <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Admin Charges</strong></font></td>
            <td>
	    <input type="text" name="distributorBankcharges" value="<?=stripslashes($_SESSION["distributorBankcharges"]); ?>" size="35" maxlength="6">
		</td>
	    <tr bgcolor="#ededed"> 
	   <? } ?>
           
		   <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Status</strong></font></td>
            <td>
              <input type="radio" name="agentStatus" value="New" <? if ($_SESSION["agentStatus"] == "" || $_SESSION["agentStatus"] == "New") echo "checked"; ?>>
              New 
              <input type="radio" name="agentStatus" value="Active" <? if ($_SESSION["agentStatus"] == "Active") echo "checked"; ?>>
              Active 
              <input type="radio" name="agentStatus" value="Disabled" <? if ($_SESSION["agentStatus"] == "Disabled") echo "checked"; ?>>
              Disabled 
              <input type="radio" name="agentStatus" value="Suspended" <? if ($_SESSION["agentStatus"] == "Suspended") echo "checked"; ?>>
              Suspended</td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Correspondent</strong></font></td>
            <td>
              <select name="correspondent" id="correspondent">
               <? if($_GET["ida"] != "Y") {?> <option value="N">Agent</option><!--Can't distribute transactions--><? } ?>
                <? if($_GET["ida"] == "Y"){ ?><option value="ONLY">Distributor</option><!--Only distribute transactions--><? } ?>
               
              </select>
              <script language="JavaScript">
         	SelectOption(document.forms[0].correspondent, "<?=$_SESSION["isCorrespondent"]; ?>");
                                </script>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
          <td><font color="#005b90"><strong>Select country to <? if ($_GET["ida"] == "Y") { echo "Assign distribution"; }else{ echo "Send money"; } ?><font color="#ff0000">*</font></strong></font></td>
            <td>Hold Ctrl key for multiple selection<br>
              <SELECT name="IDAcountry[]" size="4" multiple id="IDAcountry" style="font-family:verdana; font-size: 11px">
                <?
                
                if(CONFIG_ENABLE_ORIGIN == "1"){
                	
                	
                		$countryTypes = " and countryType like '%destination%' ";
                	                                	
                	}else{
                		$countryTypes = " ";
                	
                	}
                if(CONFIG_COUNTRY_SERVICES_ENABLED){
	                
								$serviceTable = TBL_SERVICE;
								if($countryBasedFlag){
									$serviceTable = TBL_SERVICE_NEW;
								}
								$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".$serviceTable." where  countryId = toCountryId  $countryTypes order by countryName");
									
								}else
								{
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
									
								}
                
                //$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
					for ($i=0; $i < count($countires); $i++){
				?>
                <OPTION value="<?=$countires[$i]["countryName"]; ?>" <? echo (strstr($_SESSION["IDAcountry"],$countires[$i]["countryName"]) ? "selected"  : "")?>> 
                <?=$countires[$i]["countryName"]; ?>
                </OPTION>
                <?
					}
				?>
              </SELECT>
            </td>
          </tr>
<!--			///////////////////////////////////////////////////////-->
			<tr bgcolor="#ededed">
				<?php
				if(!isset($_GET["ida"]) && !isset($_GET["type"]) ) {
				?>
					<td>
						<font color="#005b90"><strong>Select associated distributor <font color="#ff0000">*</font></strong></font>
					</td>
				<td>
					<SELECT name="associatedDistributor[]" size="4" multiple id="associatedDistributor" style="font-family:verdana; font-size: 11px">
						<?
						$query="";
						if ($agentType2 === "SUPA" || $agentType2 === "SUBA" ) {
							$query="select name, agentCompany from admin where userID='$parentID' or  parentID = '$parentID'";
						}  else {
							$query="SELECT name , agentCompany FROM `admin` WHERE `adminType` = 'Agent' AND (`agentType` = 'Supper' or `agentType` = 'Sub') AND `isCorrespondent` = 'ONLY' and agentStatus='Active'";
						}

						$result=selectMultiRecords($query);
						foreach($result as $row){
							echo "<option value='".$row['name']."(".$row['agentCompany'].")'>".$row['name']."(".$row['agentCompany'].")</option>";
						}
						?>
					</SELECT>
				</td>
					<?php
				} else if($_GET["ida"] === "Y") {
					?>
					<td>
						<font color="#005b90"><strong>Select associated agent <font color="#ff0000">*</font></strong></font>
					</td>
					<td>
						<SELECT name="associatedAgent[]" size="4" multiple id="associatedAgent" style="font-family:verdana; font-size: 11px">
							<?
							$query="";
							if ($agentType2 === "SUPA" || $agentType2 === "SUBA" ) {
								$query=selectFrom("select linked_Agent from admin where userID=$parentID");
								$arrayOfUserID = explode(", ", $query['linked_Agent']);
								$userIDs = "";
								for ($i=0;$i<count($arrayOfUserID);$i++) {
									$userID = $arrayOfUserID[$i];
									if ($i === count($arrayOfUserID)-1) {
										$userIDs .= "'".$userID."'";
									} else {
										$userIDs .= "'".$userID."', ";
									}
								}
								$query="select name, agentCompany from admin where userID in ($userIDs)";
							}  else {
								$query="SELECT name , agentCompany FROM `admin` WHERE `adminType` = 'Agent' AND (`agentType` = 'Supper' or `agentType` = 'Sub') AND `isCorrespondent` = 'N' and agentStatus='Active'";
							}

							$result=selectMultiRecords($query);
							foreach($result as $row){
								echo "<option value='".$row['name']."(".$row['agentCompany'].")'>".$row['name']."(".$row['agentCompany'].")</option>";
							}
							?>
						</SELECT>
					</td>
					<?php
				} else {
					?>
					<td>

					</td>
					<?php
				}
				?>
			</tr>
<!--			//////////////////////////////////////////////////////////////////-->
			<tr bgcolor="#ededed">
					<td>
						<font color="#005b90"><strong>Select services  </strong></font>
					</td>
					<td>
						<SELECT name="services[]" size="4" multiple id="services" style="font-family:verdana; font-size: 11px">
							<option value="W">Website</option>
							<option value="O">Online</option>
							<option value="BO">Back Office</option>
							<option value="G">Gateway</option>
						</SELECT>
					</td>

			</tr>
<!--			////////////////////////////////////////////-->
		  <?php  if(CONFIG_HIDE_AGENT_COMPANY_LOGO!="1"){ ?>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Company Logo </strong></font></td>
            <td>
              <input type="file" name="logo">
            </td>
          </tr>
		  <?php  }?>
		  <?php if(CONFIG_HIDE_AGENT_AUTHO_SERVICE!="1"){?>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Authorised for the services</strong></font></td>
            <td> Hold Ctrl key for multiple selection<br>
              <SELECT name="authorizedFor[]" size="4" id="servicesDiv" multiple style="font-family:verdana; font-size: 11px" >
                <?
			if(trim($_SESSION["authorizedFor"]) == "")
			{?>
                <option value="Pick up" selected>Pick up</option>
                <?
			}
			else
			{
			?>
                <option value="Pick up" <? echo (strstr($_SESSION["authorizedFor"],"Pick up") ? "selected"  : "")?>>Pick 
                up</option>
                <?
			}
			?>
                <option value="Bank Transfer" <? echo (strstr($_SESSION["authorizedFor"],"Bank Transfer") ? "selected"  : "")?>>Bank 
                Transfer</option>
                <option value="Home Delivery" <? echo (strstr($_SESSION["authorizedFor"],"Home Delivery") ? "selected"  : "")?>>Home 
                Delivery</option>
       <? if(CONFIG_ATM_CARD_OPTION == "1"){ ?>
       					
       					<option value="ATM Card" <? echo (strstr($_SESSION["authorizedFor"],"ATM Card") ? "selected"  : "")?>>ATM Card</option>
       <? } ?>         
              </SELECT>
            </td>
            </tr>
		  <?php }?>
             <!-- Added By Niaz Ahmad #2810 Connect Plus at 16-01-2008 -->
          <? if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1") { ?>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Settlement Currency<font color="#ff0000">*</font></strong></font></td>
            <td> 
                 <select name="currencyOrigin">
				<option value="">-- Select Currency --</option>
						  <?
						 						  
							$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
					
							while($rstRow = mysql_fetch_array($nResult))
							{
								$country = $rstRow["country"];
								$currency = $rstRow["currencyName"];	
								$description = $rstRow["description"];
								$erID  = $rstRow["cID"];							
								
							 echo "<option value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";	
							}
						  ?>				
				</select>
				<script language="JavaScript">
     			SelectOption(document.forms[0].currencyOrigin, "<?=$_SESSION["currencyOrigin"] ?>");
     	  </script>	
         </td>  
         </tr> 
           <? } ?>

		<?
			/*** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents ****/
			if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
				&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1 
				&& $_REQUEST["ida"] != "Y")
			{
				?>
				 <tr bgcolor="#ededed">
					<td><font color="#005b90"><strong>Select Distributors</strong></font></td>
					<td>
					<?
						//debug( selectMultiRecords("select * from admin where isCorrespondent='ONLY'") );
						if ( !empty($_REQUEST["assocDistributors"]) )
							$assocDistIDs = implode( ",", $_REQUEST["assocDistributors"] );

						$allDistributors = selectMultiRecords("select userID, name, username from admin where isCorrespondent='ONLY' and parentID > '0' and adminType='Agent' and agentStatus='Active'");
					?>
						<input type="radio" onClick="updateAssocDistControls();" name="associateAllDistributors" value="1" <?=( empty($assocDistIDs) ? "checked='checked'" : "" ); ?> /> All &nbsp;&nbsp
						<input type="radio" onClick="updateAssocDistControls();" id="letMeSelectAssocDist" name="associateAllDistributors" value="0" <?=( !empty($assocDistIDs) ? "checked='checked'" : "" ); ?> /> Let Me Select<br />
						Hold Control key for multiple selection<br />
						<select id="distList" name="assocDistributors[]" multiple="multiple" size="6">
						<?
							foreach( $allDistributors as $distData )
								echo "<option " . ( in_array($distData["userID"], $assocDistIDs) ? "selected='selected'" : "" ) . " value='" . $distData ["userID"] . "'>" . $distData ["name"] . " [" . $distData ["username"] . "]</option>";
						?>
						</select>
					</td>
				 </tr>
				<?
			}
			/*** End of code #3428: Connect Plus - Select Distributors for Agents ****/
		?>

            <? if(CONFIG_AGENT_PAYMENT_MODE == "1"){ ?>
          
            <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Payment Mode</strong></font></td>
            <td><select name="agentPaymentMode" id="agentPaymentMode">
                <? if(CONFIG_PAYMENT_MODE_OPTION == "1"){ ?>
               			 <option value="exclude">At Source</option>
                
                <? }else{ ?>
                <option value="include">At End Of Month</option>
                <option value="exclude">At Source</option>
               <? } ?>
              </select>
              <script language="JavaScript">
         	SelectOption(document.forms[0].agentPaymentMode, "<?=$_SESSION["agentPaymentMode"]; ?>");
                                </script>
            </td>
          </tr>
          <? }else{ ?>
           <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Payment Mode</strong></font></td>
            <td><select name="agentPaymentMode" id="agentPaymentMode">
	            	<option value="include">At End Of Month</option>  <!-- Was At source here but the logic that is being followed is that include is for at end of month and exclude for at source so changed accordingly .. Modified by Khola-->
    			</select>
              <script language="JavaScript">
         	SelectOption(document.forms[0].agentPaymentMode, "<?=$_SESSION["agentPaymentMode"]; ?>");
                                </script>
            </td>
          </tr>
          
          
          <? } ?>
		  <?php if(CONFIG_HIDE_AGENT_ACCESS_IP!="1"){?>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Access From IP</strong></font></td>
            <td>For multiple please separate by comma.<br> 
              <input name="accessFromIP" type="text" id="accessFromIP" size="40" maxlength="255">
            </td>
          </tr>
		  <?php }?>
		  <!--  Added by Niaz Ahmad at 02-05-2009 @4816 Faith Exchange Upload Document   -->
		  <? if(CONFIG_UPLOAD_DOCUMNET_ADD_SUPPER_AGENT	== "1") { ?> 
		  <tr bgcolor="#ededed"> 
           <td width="37%"align="right"><font color="#005b90"><b>Upload Documents&nbsp;</b></font></td>
           	<td width="37%"><input type="checkbox" name="uploadImage" value="Y" <?=($_SESSION["uploadImage"]=="Y")?'checked="checked"':''?>>
           		<input type="hidden" name="mode" value="Add">
           		</td>
           				</tr>
         <? 
		    }
		 ?>
		  
          <? if (CONFIG_BACK_DATED == "1" && $_GET["ida"] != "Y"){ ?>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Right for Backdated</strong></font></td>
            <td> 
              <select name="rights" id="rights" <? if ($agentType2 == 'admin') { ?>onChange="showBackDays();"<? } ?>>
              	<option value="">No</option>
              	<option value="Backdated">Yes</option>
              </select>
            </td>
          </tr>
         <?	if(CONFIG_PAYIN_CUSTOMER == '1') {?>
          <tr bgcolor="#ededed"> 
	                  <td width="17%"><font color="#005b90"><b>Payin Book&nbsp;</b></font></td>
	                  <td width="26%"><input type="checkbox"  name="ch_payinBook" <? if($_SESSION["ch_payinBook"]!="") echo "checked"; ?> ></td>
	                </tr>
	               <tr bgcolor="#ededed">
	                  <td width="20%"><font color="#005b90"><b>Payin Book No.&nbsp;</b></font></td>
	                  <td width="37%"><input type="textbox" name="payinBook" value="<?=$_SESSION["payinBook"];?>" maxlength="<?=CONFIG_PAYIN_NUMBER ?>"></td>
	                </tr>
          <? }?>
          
				<tr bgcolor="#ededed" id="howmanybackdays" style="visibility:hidden;position:absolute;">
					<td><font color="#005b90"><strong>How Many Back Days</strong></font></td>
					<td> <input type="text" name="backDays" id="backDays" size="3"> <i>By default,</i> 1</td>
				</tr>
        <? } ?>
         <? if (CONFIG_ONLINE_AGENT == "1" && $_GET["ida"] != "Y")
         { 
         	?>
         	 	<tr bgcolor="#ededed">
					<td>
						<font color="#005b90"><strong>Assigned to Online Senders</strong></font>
					</td>
					<td> 
						<select name="isOnline" id="isOnline">
							<option value="N" <? echo($_SESSION["isOnline"]=="Y" ? "" : "selected")?>>No</option>
							<option value="Y" <? echo($_SESSION["isOnline"]=="Y" ? "selected"  : "")?>>Yes</option>	
						</select>
					</td>
				</tr>
         
         	<?
         }
         ?>
         
         
         <?
         if(CONFIG_DEFAULT_DISTRIBUTOR == '1' && $_REQUEST["ida"] == "Y" && ($agentType2 ==  'SUPI' || $agentType2 ==  'SUPAI' || $agentType2 ==  'admin'))
         {
         	?>

         <tr bgcolor="#ededed">
						<td>
							<font color="#005b90"><strong>Default Distributor</strong></font>
						</td>
						<td> 
							<select name="defaultDistrib" id="defaultDistrib" onChange="javascript: showHideReplaceBlock();">
								<option value="N" <? echo($_SESSION["defaultDistrib"]=="Y" ? "" : "selected")?>>No</option>
								<option value="Y" <? echo($_SESSION["defaultDistrib"]=="Y" ? "selected"  : "")?>>Yes</option>	
							</select>
						</td>
				</tr>
				<tr bgcolor="#ededed" id="showReplaceBlock" style="display:<?=($_SESSION["defaultDistrib"] == "Y"?"table-row":"none")?>;">
					<td>
						<font color="#005b90"><strong>Replace Existing Default Distributor</strong></font>
					</td>
					<td> 
						<input type="radio" name="replaceDefaultDistributor" id="replaceDefaultDistributor" value="Y" <?=($_SESSION["replaceDefaultDistributor"]=="Y"?"checked":"")?>>&nbsp;Yes
						<input type="radio" name="replaceDefaultDistributor" id="replaceDefaultDistributor" value="N" <?=($_SESSION["replaceDefaultDistributor"]=="N"?"checked":"")?>>&nbsp;No
					</td>
				</tr>
				
				
        <?
         	}
         	?>
         	<?
       
         if(CONFIG_MANAGE_CLAVE == '1' && $_GET["ida"] == "Y")
         {
         	?>

         	<tr bgcolor="#ededed">
					<td>
						<font color="#005b90"><strong>Use Clave Range</strong></font>
					</td>
					<td> 
						<select name="hasClave" id="hasClave">
              			<option value="N" <? echo($_SESSION["hasClave"]=="Y" ? "" : "selected")?>>No</option>
              			<option value="Y" <? echo($_SESSION["hasClave"]=="Y" ? "selected"  : "")?>>Yes</option>	
					</td>
				</tr>
        <?
         	}
         	?>
         	
         	<?
       
         if(CONFIG_UNLIMITED_EX_RATE_OPTION == '1'&& $_GET["ida"] != "Y")
         {
         	?>
         	<tr bgcolor="#ededed">
					<td>
						<font color="#005b90"><strong>Manual Exchange Rate</strong></font>
					</td>
					<td> 
						<select name="unlimitedRate" id="unlimitedRate">
              			<option value="N" <? echo($_SESSION["unlimitedRate"]=="Y" ? "" : "selected")?>>No</option>
              			<option value="Y" <? echo($_SESSION["unlimitedRate"]=="Y" ? "selected"  : "")?>>Yes</option>	
					</td>
				</tr>
				
        <tr bgcolor="#ededed">
					<td>
						<font color="#005b90"><strong>Countries for Manual Exchange Rate</strong></font>
					</td>
					<td> 
						<select name="rateCountries[]" id="rateCountries[]" size="4" multiple>
							  <?
                
                if(CONFIG_ENABLE_ORIGIN == "1"){
                	
                	
                		$countryTypes = " and countryType like '%destination%' ";
                	                                	
                	}else{
                		$countryTypes = " ";
                	
                	}
                if(CONFIG_COUNTRY_SERVICES_ENABLED){
	                
	                
								$serviceTable = TBL_SERVICE;
								if($countryBasedFlag){
									$serviceTable = TBL_SERVICE_NEW;
								}
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".$serviceTable." where  countryId = toCountryId  $countryTypes order by countryName");
									
								}else
								{
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
									
								}
                
				                //$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
									for ($i=0; $i < count($countires); $i++){
								?>
				                <OPTION value="<?=$countires[$i]["countryName"]; ?>" <? echo (strstr($_SESSION["rateCountries"],$countires[$i]["countryName"]) ? "selected"  : "")?>> 
				                <?=$countires[$i]["countryName"]; ?>
				                </OPTION>
				                <?
									}
								?>
              </select>
     			</td>
				</tr>
        <?
         	}
         	?>
         	
         	
         	 <? 
         	 if(CONFIG_SHARE_OTHER_NETWORK == '1')
		       { 
		       	$jointClients = selectMultiRecords("select * from ".TBL_JOINTCLIENT." where 1 and isEnabled = 'Y' ");
		       	?>
		       	 	<tr bgcolor="#ededed">
						<td>
							<font color="#005b90"><strong>Available to</strong></font>
						</td>
						<td> 
							<select name="remoteAvailable" id="remoteAvailable">
								<option value=""> Select One </option>
								<?
								for($cl = 0; $cl < count($jointClients); $cl++)
								{
								?>
								
									 <OPTION value="<?=$jointClients[$cl]["clientId"]; ?>" <? echo (strstr($_SESSION["remoteAvailable"],$jointClients[$cl]["clientId"]) ? "selected"  : "")?>> <?=$jointClients[$cl]["clientName"]?></option>
								<?
								}
								?>
		            			
						</td>
					</tr>
		       
		       	<?
		       }
		       ?>
         	
         	<? if(CONFIG_NOTES_AGENT_DIST == "1"){ ?>
         	
						<tr bgcolor="#ededed">
	                  <td width="20%"><font color="#005b90"><b>Notes&nbsp;</b></font></td>
	                  <td ><input type="textarea" name="notes" value="<?=$_SESSION["notes"];?>" ></td>
	                
					</tr>
         	<? } ?>
          <tr bgcolor="#ededed"> 
            <td colspan="2" align="center"> 
              <input type="submit" value=" Save " id="btnSubmit">
              &nbsp;&nbsp; 
              <input type="reset" value=" Clear ">
            </td>
          </tr>
        </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>