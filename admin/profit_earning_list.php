<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

$totalAmount="";



////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPA")
		{
			$condition = True;	
		}
	}else{
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager")
			{
				$condition = True;
			}
		
		}
		
//////////////////////////Making of Condition///////////////

if($_POST["Search"] != "")
{
	$fromDate = $_POST["fYear"] . "-" . $_POST["fMonth"] . "-" . $_POST["fDay"];
	$toDate = $_POST["tYear"] . "-" . $_POST["tMonth"] . "-" . $_POST["tDay"];
	$queryDate = "(entry_time >= '$fromDate 00:00:00' and entry_time <= '$toDate 23:59:59')";
	
	
	//(entry_time, origin_country, dest_country, origin_amount, origin_currency, intermediate_rate, intermediate_curr, send_rate_inter_to_dest, dest_currency, bank_charges)
//		$date_time = getCountryTime(CONFIG_COUNTRY_CODE);
		$searchQuery = "select * From ".TBL_PROFIT_EARNING." where 1 and $queryDate";
		$searchQueryCnt = "select count(*) From ".TBL_PROFIT_EARNING." where 1 and $queryDate";		
		//$_POST["origCountry"]."','".$_POST["destCountry"]."','".$originAmount."','".$originCurrency."','".$intermediateRate."','".$intermediateCurr."','".$destinationRate."','".$destinationCurrency."','".$bankCharges2."')";	
	$profitRecords = SelectMultiRecords($searchQuery);
	$totalCount = countRecords($searchQueryCnt);
	
	
}

?>
<html>
<head>
	<title>Calculate Profit</title>
<script>
var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function delete_confirm(id)
{
  if(confirm("Are you sure you want to delete this item?"))
    return true;
  else
    return false;
}
</SCRIPT>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5" align="center">
<tr>
    <td class="topbar">Profit Earning 
      Report List</td>
  </tr>
 </table>
<table width="83%" border="0" cellspacing="1" cellpadding="5" align="center">
  <form action="profit_earning_list.php" method="post" name="calculator">
  
  <tr>
  	<td>
  		<table width="100%" border="0" align="center">	
  			<tr>
  				<td>
  				<div align="center">
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.forms[0].fDay, "<?=$_POST["fDay"]; ?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        <script language="JavaScript">
         	SelectOption(document.forms[0].fMonth, "<?=$_POST["fMonth"]; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.forms[0].fYear, "<?=$_POST["fYear"]; ?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

		 <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tDay, "<?=$_POST["tDay"]; ?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tMonth, "<?=$_POST["tMonth"]; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tYear, "<?=$_POST["tYear"]; ?>");
        </script><br><br>
  				</td>
  			<tr>
  			<tr>
  				<td align="center">
  					<input type="submit" name="Search" value="Search">
  				</td>
  			</tr>
		</table>	     
         <table border="0" align="center" width="100%">
         	<?
         	if($totalCount)
         	{
         	?>
         	<tr bgcolor="#DFE6EA"> 
         		<td width="146"><font color="#005b90"><strong>Date</strong></font></td>
         		<td width="146"><font color="#005b90"><strong>Countries</strong></font></td>
	            <td width="146"><font color="#005b90"><strong>Net Amount</strong></font></td>
	            <td width="159"><font color="#005b90"><strong>Bank Charges</strong></font></td>	
	            <td width="159"><font color="#005b90"><strong>Conversion Rate</strong></font></td>	
	            <td width="146"><font color="#005b90"><strong>Destination Rate Originating</strong></font></td>
	            <td width="146"><font color="#005b90"><strong>Destination Rate Destination</strong></font></td>
	            <td width="159"><font color="#005b90"><strong>Margin Profit </strong></font></td>	
	            <td width="159"><font color="#005b90"><strong>Total Profit </strong></font></td>	            
         	</tr>
         	<?
		         for($i = 0; $i < count($profitRecords); $i++)
		         {
//				///Destination Rate with Intermediate Currency at Sending End.  e.g. USD --> GMD at UK
				if($profitRecords[$i]["reciev_rate_inter_to_dest"] != "")
				{	
					$dest_Rate = $profitRecords[$i]["send_rate_inter_to_dest"]/$profitRecords[$i]["intermediate_rate"];
					
					$marginProfit = $profitRecords[$i]["reciev_rate_inter_to_dest"] - $dest_Rate;
					$NetAmount = ($profitRecords[$i]["origin_amount"] * $profitRecords[$i]["intermediate_rate"])-$profitRecords[$i]["bank_charges"]; 
					$totalProfit = $NetAmount * $marginProfit;
				}
		         ?>
		         	<tr bgcolor="#DFE6EA"> 
			            <td width="146"><a HREF="profit_earning_Bayba_destination.php?profitID=<? echo $profitRecords[$i]["profit_id"]; ?>" target="mainFrame" class="tblItem style4"><strong><font color="#006699"><? echo($profitRecords[$i]["entry_time"]); ?></font></strong></a></td>
			            <td width="159"><? echo("From ".$profitRecords[$i]["origin_country"]." To ".$profitRecords[$i]["dest_country"]); ?></td>	
			            <td width="146"><? echo($NetAmount." ".$profitRecords[$i]["intermediate_curr"]); ?></td>
			            <td width="159"><? echo($profitRecords[$i]["bank_charges"]); ?></td>	            
			            <td width="159"><? echo($profitRecords[$i]["intermediate_rate"]." ".$profitRecords[$i]["intermediate_curr"]); ?></td>	
			            <td width="159"><? echo($dest_Rate." At ".$profitRecords[$i]["origin_country"]); ?></td>	            
			            <td width="159"><? echo($profitRecords[$i]["reciev_rate_inter_to_dest"]." At ".$profitRecords[$i]["dest_country"]); ?></td>	            
			            <td width="159"><? echo($marginProfit); ?></td>	            
			            <td width="159"><? echo($totalProfit ." ".$profitRecords[$i]["dest_currency"]); ?></td>	            
		         	</tr>
	         	<?
        		}
        	}else{
        	?>
        	<tr>
        		<td align="center">
        			<font color="#005b90"><strong>No Record Selected to View</strong></font></td>
        		</td>
        	</tr>	
        	<?
        	}
         	?>
        </table>      
			
	 
	 </td>
	</tr>
	</form>
</table>
</body>
</html>