<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$nowTime = getCountryTime(CONFIG_COUNTRY_CODE);
$today = date('d-m-Y  h:i:s A');//date("d-m-Y");
$agentName = $_SESSION["loggedUserData"]["username"];
//echo "<pre>";
//print_r($_SESSION);
//echo "</pre>";

if($_SESSION["benAgentID"] == '')
	$_SESSION["benAgentID"] = $_POST['agentID'];

	
	
	$_SESSION["BenTempcurrencyTo"] =  $_SESSION["currencyTo"];
	
if(CONFIG_COMPLIANCE_PROMPT_CONFIRM == "1"){
$currentRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction')";
$currentRule = selectFrom($currentRuleQuery);

$cumulativeRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction')";
$cumulativeRule = selectFrom($cumulativeRuleQuery);


}

$agentType = getAgentType();
if($agentType == "SUPA" || $agentType == "SUPAI" || $agentType == "SUBA" || $agentType == "SUBAI")
{
	$queryCust = "select *  from ".TBL_ADMIN_USERS." where username ='$username'";
	$senderAgentContent = selectFrom($queryCust);	
	$_POST["custAgentID"] = $senderAgentContent["userID"];
}

$_SESSION["moneyPaid"] 			= $_POST["moneyPaid"];
$_SESSION["transactionPurpose"] = $_POST["transactionPurpose"];
$_SESSION["other_pur"] 			= $_POST["other_pur"];
$_SESSION["fundSources"] 		= $_POST["fundSources"];
$_SESSION["refNumber"] 			= $_POST["refNumber"];
$_SESSION["benAgentID"] 		= $_POST["agentID"];
$_SESSION["discount"]       = $_POST["discount"];
$_SESSION["chDiscount"]     = $_POST["chDiscount"];
$_SESSION["exchangeRate"]     = $_POST["exchangeRate"];
$_SESSION["IMFee"]     = $_POST["IMFee"];

$_SESSION["bankName"] 		= $_POST["bankName"];
$_SESSION["branchCode"] 	= $_POST["branchCode"];
$_SESSION["branchAddress"] 	= trim($_POST["branchAddress"]);
$_SESSION["swiftCode"] 		= $_POST["swiftCode"];
$_SESSION["accNo"] 			= $_POST["accNo"];
$_SESSION["ABACPF"] 		= $_POST["ABACPF"];
$_SESSION["IBAN"] 			= $_POST["IBAN"];
$_SESSION["ibanRemarks"] 			= $_POST["ibanRemarks"];
$_SESSION["currencyTo"]     = $_POST['currencyTo'];

if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
{
	$_SESSION["chequeAmount"] = $_REQUEST["chequeAmount"];
}

if($_POST["dDate"] != "")
{
	$_SESSION["transDate"] = $_POST["dDate"];
}

$_SESSION["question"] 			= $_POST["question"];
$_SESSION["answer"]     = $_POST["answer"];	
$_SESSION["tip"]     = $_POST["tip"];

$_SESSION["discount_request"] = $_POST["discount_request"];	

if($_SESSION["transType"]=="Bank Transfer")
	{		
		$_SESSION["bankCharges"] 		= $_POST["bankCharges"];
	}
	else
	{
		$_SESSION["bankCharges"]="";
	}
	
	
if(CONFIG_SENDER_ACCUMULATIVE_AMOUNT == "1"){

$senderAccumulativeAmount = 0;
         
            		  $to = getCountryTime(CONFIG_COUNTRY_CODE);
									$month = substr($to,5,2);
									$year = substr($to,0,4);
									$day = substr($to,8,2);
								  $noOfDays = CONFIG_NO_OF_DAYS;
								  $fromDate = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d")-$noOfDays ,   date("Y")));
									$from = $fromDate." 00:00:00"; 
									
									// These dates set from configuration page
									if(CONFIG_COMPLIANCE_NUM_OF_DAYS == "1"){
									
										if($cumulativeRule["cumulativeFromDate"]!='' && $cumulativeRule["cumulativeToDate"]!=''){
  			        	    $from	= $cumulativeRule["cumulativeFromDate"]." 00:00:00"; 
  		                $to	= $cumulativeRule["cumulativeToDate"]." 23:59:59"; 
  	                } 
  	              }
									$senderTransAmount = selectFrom("select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_POST["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'");
								  //echo "select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_POST["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'";
                 $senderAccumulativeAmount += $senderTransAmount["transAmount"];
 

	if($_POST["transID"] == "")
	{
 				$senderAccumulativeAmount += $_POST["transAmount"];
	}
}
	

if($_POST["transID"] != "")
{
	$backUrl = "add-transaction.php?msg=Y&transID=" . $_POST["transID"];
}
else
{
	$backUrl = "add-transaction.php?msg=Y";
}
if(trim($_POST["transType"]) == "")
{
	insertError(TE3);	
	redirect($backUrl);
}

if(trim($_POST["transType"]) == "Pick up")
{
	if(trim($_POST["collectionPointID"]) == "")
	{
		insertError("Please Select Collection Point");	
		redirect($backUrl);
	}
}


if(trim($_POST["customerID"]) == "")
{
	insertError(TE4);	
	redirect($backUrl);
}
/*if(trim($_POST["agentID"]) == "" && trim($_POST["transType"]) == "Pick up")
{
	insertError(TE5);	
	redirect($backUrl);
}*/

if(trim($_POST["benID"]) == "")
{
	insertError(TE6);	
	redirect($backUrl);
}
if(isset($_POST["benIdTypeFlag"]) && trim($_POST["benIdTypeFlag"]) == ""  && strstr(CONFIG_BEN_IDTYPE_COMP_TRANSACTION_TYPES,trim($_POST["transType"])))
{
	insertError("Selected Beneficiary should have ID details for Transactin Type ".trim($_POST["transType"]).". Please Edit the Beneficiary.");	
	redirect($backUrl);
}

if(isset($_POST["benSOFFlag"]) && trim($_POST["benSOFFlag"]) == ""  && strstr(strtoupper(CONFIG_BEN_SOF_TYPE_COMP_TRANSACTION_TYPES),strtoupper(trim($_POST["transType"]))))
{
	insertError("Selected Beneficiary should have S/O,W/O,D/O value for Transactin Type  ".trim($_POST["transType"]).". Please Edit the Beneficiary.");	
	redirect($backUrl);
}
// Validity cheks for Bank Details of Beneficiary.
if(trim($_POST["transType"]) == "Bank Transfer")
{
	if (CONFIG_EURO_TRANS_IBAN == "1" && $_POST["benCountryRegion"] == "European") {
		
		if (trim($_POST["IBAN"]) == "") {
			insertError(TE21);
			redirect($backUrl);	
		}
		
	}	else {
		
		if(trim($_POST["bankName"]) == "") {
			insertError(TE14);	
			redirect($backUrl);
		}
		if(trim($_POST["accNo"]) == "") {
			insertError(TE18);	
			redirect($backUrl);
		}
		if(trim($_POST["branchAddress"]) == "") {
			insertError(TE16);	
			redirect($backUrl);
		}
		if(CONFIG_CPF_ENABLED == '1') {
			if(trim($_POST["ABACPF"]) == "" && strstr(CONFIG_CPF_COUNTRY , '$_POST["benCountry"]')) {
				insertError(TE20);	
				redirect($backUrl);
			}
		}
		
	}
	if(trim($_POST["distribut"]) == "") {
		insertError("Please select a Distributor from the list");	
		redirect($backUrl);
	}
}
/*if(trim($_POST["refNumber"]) != "")
{
	
	if (isExist("select refNumber from ".TBL_TRANSACTIONS." where refNumber = '".$_POST["refNumber"]."'")){
		insertError(TE7);		
		redirect($backUrl);
	
}	
}*/

if($_POST["notService"] == "Home Delivery")
{
	insertError(TE22);	
	redirect($backUrl);
}

if(trim($_POST["transAmount"]) == "" || $_POST["transAmount"] <= 0)
{
	insertError(TE8);	
	redirect($backUrl);
}

if(trim($_POST["exchangeRate"]) == "" || $_POST["exchangeRate"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["exchangeID"]) == "")
{
	insertError(TE9);	
	redirect($backUrl);
}

if(CONFIG_ZERO_FEE == '1')
{
	if( $_POST["IMFee"] < 0)
	{
		insertError(TE2);	
		redirect($backUrl);
	}	
	
}else{
	if(trim($_POST["IMFee"]) == "" || $_POST["IMFee"] <= 0)
	{
		insertError(TE2);	
		redirect($backUrl);
	}
}

if(trim($_POST["localAmount"]) == "" || $_POST["localAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}
if($_POST["benCountry"] == 'Pakistan' || $_POST["benCountry"] == 'India' || $_POST["benCountry"] == 'Poland' || $_POST["benCountry"] == 'Brazil' || $_POST["benCountry"] == 'Philippines')
{

}
else
{
	if($_SESSION["chDiscount"]=="" && (trim($_POST["IMFee"]) == "" || $_POST["IMFee"] <= 0))
	{
		insertError(TE9);	
		redirect($backUrl);
	}
}
if(trim($_POST["totalAmount"]) == "" || $_POST["totalAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["transactionPurpose"]) == "")
{
	insertError(TE10);	
	redirect($backUrl);
}

if(trim($_POST["fundSources"]) == "" && CONFIG_NONCOMPUL_FUNDSOURCES != '1')
{
	insertError(TE11);	
	redirect($backUrl);
}

if(CONFIG_MONEY_PAID_NON_MANDATORY != "1" && CONFIG_CALCULATE_BY != '1'){
	if(trim($_POST["moneyPaid"]) == "")
	{
		
		insertError(TE12);	
		redirect($backUrl);
	}
}

/*
$_SESSION['RequestforBD'] = "";
$currDay = date("d");
$backDays = $_SESSION["loggedUserData"]["backDays"];
if ($agentType != 'admin' && $backDays != '0') {
	if ($_POST['dDate'] != "") {
		$dDateArr = explode("-", $_POST['dDate']);
		$dDay = $dDateArr[2];
		$diff = $currDay - $dDay;
		if ($diff > $backDays) {
			$_SESSION['RequestforBD'] = "Y";
		}
	}
}
*/
$refNumber = str_replace(" ","",$_POST["refNumber"]);
$_SESSION["refNumber"] = $refNumber;
$_POST["refNumber"] = $refNumber;

if (CONFIG_MANUAL_CODE_WITH_RANGE == "1" && $_SESSION["compManualCode"] != "") {
	if ($_POST["refNumber"] == "") {
		insertError("Please provide " . $manualCode);
		redirect($backUrl);
	}
	$qDate = "SELECT MIN(created) AS created FROM ".TBL_RECEIPT_RANGE." WHERE agentID = '".$_POST["custAgentID"]."' AND used != 'Used'";
	$qDRes = mysql_query($qDate) or die("Query Error: " . $qDate . "<br>" . mysql_error());
	$qDRow = mysql_fetch_array($qDRes);
	if ($qDRow["created"] == "") {
		insertError("Please first add agent's receipt book range");
		redirect($backUrl);
	} else {
		$qRanVal = "SELECT * FROM ".TBL_RECEIPT_RANGE." WHERE `agentID` = '".$_POST["custAgentID"]."' AND used != 'Used' AND `created` = '".$qDRow["created"]."'";
		$rValRes = mysql_query($qRanVal) or die("Query Error: " . $qRanVal . "<br>" . mysql_error());
		$rValRow = mysql_fetch_array($rValRes);
		$usedVal = ($rValRow["used"] == "0" ? $rValRow["rangeFrom"] : $rValRow["used"]);
		
		if ($rValRow["used"] != "0") {
			$usedVal++;
		}	
		$nextRangeVal = $usedVal;
		$_SESSION["refNumber"] = $_SESSION["refNumber"] . "-" . $nextRangeVal;
	}
}

if(trim($_POST["Declaration"]) != "Y")
{
	insertError(TE13);	
	redirect($backUrl);
}

if($_SESSION["refNumber"] != '')
{
	if($_POST["transID"] != "")
		$strRandomQuery = selectFrom("select * from ". TBL_TRANSACTIONS." where transID!='".$_POST["transID"]."' and refNumber = '".$_SESSION["refNumber"]."'");
	else
		$strRandomQuery = selectFrom("SELECT * FROM ". TBL_TRANSACTIONS ." where refNumber = '".$_SESSION["refNumber"]."'");
		  
		
	//$nRandomResult = mysql_query($strRandomQuery)or die("Invalid query: " . mysql_error()); 
	//$nRows = mysql_fetch_array($nRandomResult);
	$oldReference = $strRandomQuery["refNumber"];
	if($oldReference)
	{
	insertError(TE7);	
	redirect($backUrl);
	}
}
if ($_POST["t_amount"] != "")
	$_SESSION["amount_transactions"] = $_POST["t_amount"];
if ($_POST["l_amount"] != "")
	$_SESSION["amount_left"] = $_POST["l_amount"];
	
if ($_SESSION["amount_left"] == "0")
{
	$_SESSION["amount_transactions"] = "";
	$_SESSION["amount_left"] = "";	
}

if($_POST["custAgentID"] != "")
{
	$strAddressQuery = selectFrom("select * from ". TBL_ADMIN_USERS." where userID ='".$_POST["custAgentID"]."'");
	$printAddress = $strAddressQuery["agentAddress"];
	$printPhone = $strAddressQuery["agentPhone"];
}

if(CONFIG_CURRENCY_DENOMINATION_ON_CONFIRM == "1")
{
	if($_POST["transID"] != "")
		$strCurrencyNotes = selectFrom("select * from ". TBL_CURRENCYNOTES." where transId =".$_POST["transID"]);
}



?>
<html>
<head>
<title>Transaction Confirmation</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<script src="jquery.js"></script>
<script language="javascript" src="../javascript/functions.js">	</script>
	
	<script language="javascript">
	
	function KeyDown(src) {
/*
		var totalAmount = Number(document.getElementById('total').value);
		var calculated = Number(document.getElementById(Field).value * v);
		
		var amount = totalAmount + calculated
		document.getElementById("total").value = amount;
*/

		if(Number(document.getElementById("field"+src).value) < 0 || isNaN(document.getElementById("field"+src).value))
		{
			alert('value must be in postive integer');
			document.getElementById("field"+src).value = 0;
			//document.getElementById("total"+src).value = 0;
			document.getElementById("field"+src).focus();
		}
		//else
		//{

		var val1  = Number(document.getElementById('field1').value * 1) ;
		var val2  = Number(document.getElementById('field10').value * 10) ;
		var val3  = Number(document.getElementById('field2').value * 2) ;
		var val4  = Number(document.getElementById('field20').value * 20) ;
		var val5  = Number(document.getElementById('field5').value * 5) ;
		var val6  = Number(document.getElementById('field50').value * 50) ;
		var sum   = val1 + val2 + val3 + val4 + val5 + val6;

	  document.getElementById('total1').value	= val1;
		document.getElementById('total10').value	= val2;
		document.getElementById('total2').value	= val3;
		document.getElementById('total20').value	= val4;
		document.getElementById('total5').value	= val5;
		document.getElementById('total50').value	= val6;
		
		<?
			$fTotalAmount = $_POST["totalAmount"];
			if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
			{
				$fTotalAmount = $_REQUEST["totalAmount"] - $_REQUEST["chequeAmount"];
			}
		?>		
		var totalAmount = <?=$fTotalAmount;?>
		
		document.getElementById('total').value = Math.round(sum * 100)/100;
		
		var diff = sum - totalAmount;
		
		document.getElementById('changeGiven').value = diff;
		
		if(diff < 0)
			$("#chequeRow").show();
		else
			$("#chequeRow").hide();
		
		//}
	}
	

	function displayDenom(){
		
		document.getElementById('changeGiven').value = -<?=$fTotalAmount;?>;
		
		<?php
			if(CONFIG_CURRENCY_DENOMINATION_ON_CONFIRM == "1" && $_POST["transID"] != "")
			{
		?>
			document.getElementById('field1').value =  <?=$strCurrencyNotes["notes1"]?> ;
			document.getElementById('field10').value =  <?=$strCurrencyNotes["notes10"]?> ;
			document.getElementById('field2').value =  <?=$strCurrencyNotes["notes2"]?> ;
			document.getElementById('field20').value =  <?=$strCurrencyNotes["notes20"]?> ;
			document.getElementById('field5').value =  <?=$strCurrencyNotes["notes5"]?> ;
			document.getElementById('field50').value =  <?=$strCurrencyNotes["notes50"]?> ;
			document.getElementById('total').value =  <?=$strCurrencyNotes["total"]?> ;
			
			document.getElementById('total1').value =  <?=$strCurrencyNotes["notes1"]?> ;
			document.getElementById('total10').value =  <?=$strCurrencyNotes["notes10"]?> * 10;
			document.getElementById('total2').value =  <?=$strCurrencyNotes["notes2"]?> * 2;
			document.getElementById('total20').value =  <?=$strCurrencyNotes["notes20"]?> * 20;
			document.getElementById('total5').value =  <?=$strCurrencyNotes["notes5"]?> * 5;
			document.getElementById('total50').value =  <?=$strCurrencyNotes["notes50"]?> * 50;
			
			document.getElementById('changeGiven').value = <?=$strCurrencyNotes["total"]?>-<?=$fTotalAmount?>;
		<?php
			}
		?>
	}

</script>
	
	<script language="javascript">
	<!--
	
	var CONFIG_DISPLAY_MONEY_PAID_BY_CHEQUE = "<?=CONFIG_DISPLAY_MONEY_PAID_BY_CHEQUE?>";
	var alertOnNegChange = "<?=CONFIG_ALERT_OUTSTANDING_CHANGE_AMOUNT?>";
	function toValidate(theForm,strval)
	{
		var change = document.getElementById("changeGiven").value;
		
		if(CONFIG_DISPLAY_MONEY_PAID_BY_CHEQUE != false && change < 0)
		{
	  		alert("There is an Outstanding amount.");
  			return false;
		} else if(alertOnNegChange=="1" && change < 0) {
		  		alert("There is an Outstanding amount.");
				return false;		
		} else {
			document.getElementById('once').disabled=true;
			checkFormMy(theForm,strval);
		}

	}
	
	function checkFormMy(theForm,strval){
	
		var minAmount = 0;
		var maxAmount = 0;

		var condition;
  
 
		var conditionCumulative;
		var conditionCurrent;
  	var ruleConfirmFlag=true;
  	var ruleFlag=false;
  	var transSender = '';
  	
  	//Check for change given
  	
  	
  	
  	
	if(strval == 'Yes')
	{
		transSender="transSend";
		}else{
			transSender="NoSend";
			}
	
	var accumulativeAmount = 0;
	accumulativeAmount = <? echo $senderAccumulativeAmount ?>;
	var currentRuletransAmount = <?=$_POST["transAmount"]?>;	
	var currentRuleAmountToCompare = 0;
	var amountToCompareTrans; 
	
// cummulative Amount Rule /////////
<?	
if($cumulativeRule["ruleID"]!='') {
    
     if($cumulativeRule["matchCriteria"] == "BETWEEN"){
		  $betweenAmount= explode("-",$cumulativeRule["amount"]);
		  $fromAmount=$betweenAmount[0];   
		  $toAmount=$betweenAmount[1]; 
?>
		conditionCumulative = <?=$fromAmount?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmount?>;
		
		<?
    }else{
?>
    amountToCompare = <?=$cumulativeRule["amount"]?>;
    conditionCumulative =	accumulativeAmount  <?=$cumulativeRule["matchCriteria"]?> amountToCompare;
 <?	}?>			
		
	
		 if(conditionCumulative)
	   {

				
	   	if(confirm("<?=$cumulativeRule["message"]?>"))
    	{
    	    ruleConfirmFlag=true;
    			/*document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();*/
    		
                
      }else{ 
    
    			/*	document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   					
      	addTrans.localAmount.focus();*/
       
        ruleConfirmFlag=false;
       // ruleFlag=true;
      }

    }else{ 

    	   ruleConfirmFlag=true;
    } 
 
	
<? } ?>

////////////current transaction amount///////


<? if($currentRule["ruleID"]!=''){
	   
		 
	   if($currentRule["matchCriteria"] == "BETWEEN"){
				
		  $betweenAmountCurrent= explode("-",$currentRule["amount"]);
		  $fromAmountCurrent=$betweenAmountCurrent[0];   
		  $toAmountCurrent=$betweenAmountCurrent[1]; 
		?>
		conditionCurrent = <?=$fromAmountCurrent?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmountCurrent?>;
		<?
 }else{
 	?>      
 	      
 		    currentRuleAmountToCompare = <?=$currentRule["amount"]?>;
 		    conditionCurrent =	currentRuletransAmount  <?=$currentRule["matchCriteria"]?> currentRuleAmountToCompare;
 <?	}?>
	
 
 if(conditionCurrent)
	   {

				if(confirm("<?=$currentRule["message"]?>"))
				{
					
    	     ruleConfirmFlag=true;
    			
    			/*document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();*/
    		
              
     }else{
    
    				/*document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   				   	addTrans.localAmount.focus();*/
         ruleConfirmFlag=false;
         //ruleFlag=true;
      }

    }else{ 

    	     ruleConfirmFlag=true; 
    } 
  
<? } ?>  

	
 if(ruleConfirmFlag){
 	
   
  	   	document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
 	}
 	
 if(ruleConfirmFlag == false){
 	
 	  	  document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   			addTrans.localAmount.focus();
 	}		

}		

	
	
	

	-->
	</script>





<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style5 {
	color: #005b90;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body onLoad="displayDenom();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
 
  <tr>
  	
    <td class="topbar"><div class='noPrint'><b><font color="#000000" size="2">Confirm Transaction.</font></b></div></td>
    
  </tr>

   <tr>
      <td align="center"><br>
	  <form name="addTrans" method="post" onSubmit="return toValidate(addTrans,'yes');">
        <table width="100%" border="0" bordercolor="#FF0000">
         
          <tr>
            <td><fieldset>
            <legend class="style2">Sender Company</legend>
            <br>
            
            <table border="0" width="100%" cellspacing="1" cellpadding="0" bgcolor="#000000">
              <tr  bgcolor="#ffffff">
                <td align="center" bgcolor="#D9D9FF" >
                <?
                	echo printLogoOnReciept($_POST["custAgentID"], $agentName);
				      	?>	
                </td>
              	 
              	 
              	 <td width="100%" align="center" colspan="3">
            	<strong><? echo strtoupper($company." ".COMPANY_NAME_EXTENSION); ?></strong><br>Address:
            	<? if(CONFIG_POS_ADDRESS == '1'){ echo(COMPANY_ADDRESS); ?><br>
            <? echo(COMPANY_PHONE);}else{echo($printAddress); ?> <br>
            	<? echo("Phone:".$printPhone);} ?>
            </td>
              </tr>
              <tr bgcolor="#ffffff">
              	 <td width="100" align="left"><font color="#005b90">Date</font></td>
              	<td width="250">
              		<!--$_POST["dDate"]!= "" shows if back date has been entered on the 
              				add-transaction page then show the  back date this was being done 
              				initially according to the new requirment current date and time 
              				is to be shown. its now done via config-->
              		<? if($_POST["dDate"]!= "" && CONFIG_BACK_LEDGER_DATES == '1'){echo($_POST["dDate"]);}else{echo $nowTime;} ?>
              	</td>
              	 <td width="100" align="left"><font color="#005b90">&nbsp;</font></td>
              	<td width="250">
              		<? //Transaction No   it is removed 
            /* if($_POST["transID"] == "")
             { 		
							$query = "select count(transID) as cnt from ". TBL_TRANSACTIONS." where  1";
							if(CONFIG_TRANS_REF == '1')
							{
								$query .= " and custAgentID = '".$_POST["custAgentID"]."'";
							}
							$nextAutoID = countRecords($query);
							$nextAutoID = $nextAutoID + 1;
						
							$senderAgent = selectFrom("select username from ". TBL_ADMIN_USERS." where userID = '".$_POST["custAgentID"]."'");
								if(CONFIG_TRANS_REF == '1')
								{
									
									$imReferenceNumber = strtoupper($senderAgent["username"])."-". $nextAutoID ;
								}else{
										$imReferenceNumber = $systemPre."-". $nextAutoID ;
										if(strstr(CUSTOM_AGENT_TRANS_REF , strtoupper($senderAgent["username"])))
										{
											$imReferenceNumber = strtoupper($senderAgent["username"]) ."-". $nextAutoID ;
										}
									}
								
								$strRandomQuery = selectFrom("SELECT * FROM ". TBL_TRANSACTIONS ." where refNumberIM = '".$imReferenceNumber."'");
					  		$oldReference = $strRandomQuery["refNumberIM"];
								while($oldReference)
								{
									$nextAutoID ++;
									
									if(CONFIG_TRANS_REF == '1')
									{
										$imReferenceNumber = strtoupper($senderAgent["username"])."-". $nextAutoID ;
									}else{
										$imReferenceNumber = $systemPre."-". $nextAutoID ;///For Reference Number	
										if(strstr(CUSTOM_AGENT_TRANS_REF , strtoupper($senderAgent["username"])))
										{
											$imReferenceNumber = strtoupper($senderAgent["username"]) ."-". $nextAutoID ;
										}
									}
									$strRandomQuery = selectFrom("SELECT * FROM ". TBL_TRANSACTIONS ." where refNumberIM = '".$imReferenceNumber."'");
										
									$oldReference = $strRandomQuery["refNumberIM"];
								}
								
								
							
						}else{
							$senderTrans = selectFrom("select refNumber, refNumberIM from ". TBL_TRANSACTIONS." where transID = '".$_POST["transID"]."'");
							$imReferenceNumber = strtoupper($senderTrans["refNumberIM"]);
						}*/
				//echo $imReferenceNumber
				?>
              	</td>
            </tr>
            <tr bgcolor="#ffffff">
              	  <td width="100" align="left">&nbsp;</td>
              	<td width="250">&nbsp;<?
              		
              		?>
              		
              	</td>
              	 <td width="150" align="left"><font color="#005b90"><? echo $manualCode;?></font></td>
              	<td width="200">
              		<? echo($_SESSION["refNumber"]); ?>
              	</td>           	
            </tr>
             <tr bgcolor="#ffffff">
              	 <td width="700" align="left" colspan="4"  height="25"><font color="#005b90"><strong>Sender Detail</strong></font></td>
              	
            </tr>
     <? 
			$queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Mobile, Email  from ".TBL_CUSTOMER." where customerID ='" . $_POST[customerID] . "'";
			$customerContent = selectFrom($queryCust);
			if($customerContent["customerID"] != "")
			{
		?>
            <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Full Name</font></td>
              	<td width="550" colspan="3">
              		<? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?>
              	</td>              	
            </tr>
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Address</font></td>
              	<td width="550" colspan="3">
              		<? echo $customerContent["Address"] . " " . $customerContent["Address1"]?>, <?=$customerContent["City"]?> - <?=$customerContent["Zip"]?>, <?=$customerContent["State"]?>, <?=$customerContent["Country"]?>.
              		 Ph: <?=$customerContent["Phone"]?> Mob: <?=$customerContent["Mobile"]?>
              	</td>              	
            </tr>
             
            <?
        }
            ?>
            
              <tr bgcolor="#ffffff">
              	 <td width="700" align="left" colspan="4" height="25"><font color="#005b90"><strong>Beneficiary Detail</strong></font></td>
             </tr>
              <? 
		
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Mobile, Email from ".TBL_BENEFICIARY." where benID ='" . $_POST["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
		?>
            <tr bgcolor="#ffffff">
              	 <td width="200" align="left"><font color="#005b90">Beneficiary Name</font></td>
              	<td width="500" colspan="3">
              		<? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?>
              	</td>
            </tr>
                     
             <tr bgcolor="#ffffff">
              	 <td width="200" align="left"><font color="#005b90">Address</font></td>
              	<td width="500" colspan="3">
              		<? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?>, <?=$benificiaryContent["City"]?> - <?=$benificiaryContent["Zip"]?>, <?=$benificiaryContent["State"]?>, <?=$benificiaryContent["Country"]?>.
              		 Ph: <?=$benificiaryContent["Phone"]?> Mob: <?=$benificiaryContent["Mobile"]?>
              	</td>
              	
            </tr> 
        <?
        }            
        ?>
           
            <tr bgcolor="#ffffff">
              	 <td width="750" align="left" colspan="4" height="25"><font color="#005b90"><strong>Transaction Detail</strong></font></td>
             		</tr>
           			<tr bgcolor="#ffffff">
              		 <td width="200" align="left"><font color="#005b90">Destination</font></td>
              		 <td width="500" colspan="3">
              			<? 
	              			$queryColl = "select *  from ".TBL_COLLECTION." where cp_id ='".$_POST["collectionPointID"]."'";
											$destinCollection = selectFrom($queryColl);
											if($destinCollection["cp_id"] != '')
											{
												echo $destination = $destinCollection["cp_corresspondent_name"]." ".$destinCollection["cp_branch_name"].", ".$destinCollection["cp_branch_address"].", ".$destinCollection["cp_city"].", ".$destinCollection["cp_state"].", ".$destinCollection["cp_country"];
												echo " ,Ph:".$destinCollection["cp_phone"] .", Fax:" .$destinCollection["cp_fax"];
											}
										?>
              		</td>
           		 </tr> 
               <tr bgcolor="#ffffff">
              	 <td width="100" align="left"><font color="#005b90">Transaction Amount</font></td>
              	<td width="250">
              		<? echo($_POST["currencyFrom"]." ".$_POST["transAmount"]);?>
              	</td>
              	 <td width="100" align="left"><font color="#005b90">Exchange Rate</font></td>
              	<td width="250">
              		<? echo($_POST['currencyTo']." ".$_POST["exchangeRate"]); ?> 
              	</td>
            </tr> 
             <tr bgcolor="#ffffff">
              	 <td width="100" align="left"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
              	<td width="250"> 
              		<? echo($_POST["currencyFrom"]." ".$_POST["IMFee"]);?>
              	</td>
              	 <td width="100" align="left"><font color="#005b90">Curr. Charges</font></td>
              	<td width="250">
              		<? echo($_POST["currencyFrom"]." ".$_POST["currencyCharge"]);?> 
              	</td>
            </tr> 
             <tr bgcolor="#ffffff">
              	 <td width="200" align="left"><font color="#005b90">Total Amount</font></td>
              	<td width="250">
              		<? echo($_POST["currencyFrom"]." ".$_POST["totalAmount"]); ?>
					<?
						$_SESSION["totalAmount"] = $_POST["totalAmount"];
						
						if ( !empty( $_REQUEST["chequeAmount"] ) )
						{
							$cashAmount = $_REQUEST["totalAmount"] - $_REQUEST["chequeAmount"];
							?>
								(<font color="#005b90">By Cheque: </font><?=$_POST["currencyFrom"] . " " . $_REQUEST["chequeAmount"];?>, <font color="#005b90">By Cash: </font><?=$_POST["currencyFrom"] . " " . $cashAmount;?>)
							<?
						}
					?>
              	</td>
              	 <td width="200" align="left"><font color="#005b90">Foriegn Amount</font></td>
              	<td width="250">
              		<? echo($_POST["currencyTo"]." ".$_POST["localAmount"]); ?> 
              	</td>
            </tr> 
            
            
            <tr bgcolor="#ffffff">
              	 <td width="750" align="left" colspan="4" height="25"><font color="#005b90"><strong>Currency Denomination</strong></font></td>
             		</tr>
            
            <tr bgcolor="#ffffff">
              	<td width="200" align="left">
              	 		<font color="#005b90">1 GBP X </font>
              	</td>
              	<td width="250">&nbsp;
              		<input type="text" name="field1" id="field1" size="4" maxlenght="4" value="0" onKeyUp="KeyDown('1')" />
              		&nbsp;<b>=</b>&nbsp;
              		<input type="text" name="total1" id="total1" size="4" maxlenght="4" value="0" disabled="disabled" style="background-color:#ffffff" /> GBP
              	</td>
              	 <td width="200" align="left"><font color="#005b90">10 GBP X </font></td>
              	<td width="250">&nbsp;
              		<input type="text" name="field10" id="field10" size="4" maxlenght="4" value="0" onKeyUp="KeyDown('10')" />
              		&nbsp;<b>=</b>&nbsp;
              		<input type="text" name="total10" id="total10" size="4" maxlenght="4" value="0" disabled="disabled" style="background-color:#ffffff" /> GBP
              	</td>
            </tr> 
            <tr bgcolor="#ffffff">
              	<td width="200" align="left">
              	 		<font color="#005b90">2 GBP X </font>
              	</td>
              	<td width="250">&nbsp;
              		<input type="text" name="field2" id="field2" size="4" maxlenght="4" value="0" onKeyUp="KeyDown('2')" />
              		&nbsp;<b>=</b>&nbsp;
              		<input type="text" name="total2" id="total2" size="4" maxlenght="4" value="0" disabled="disabled" style="background-color:#ffffff" /> GBP
              	</td>
              	 <td width="200" align="left"><font color="#005b90">20 GBP X </font></td>
              	<td width="250">&nbsp;
              		<input type="text" name="field20" id="field20" size="4" maxlenght="4" value="0" onKeyUp="KeyDown('20')" />
              		&nbsp;<b>=</b>&nbsp;
              		<input type="text" name="total20" id="total20" size="4" maxlenght="4" value="0" disabled="disabled" style="background-color:#ffffff" /> GBP
              	</td>
            </tr> 
            <tr bgcolor="#ffffff">
              	<td width="200" align="left">
              	 		<font color="#005b90">5 GBP X </font>
              	</td>
              	<td width="250">&nbsp;
              		<input type="text" name="field5" id="field5" size="4" maxlenght="4" value="0" onKeyUp="KeyDown('5')" />
              		&nbsp;<b>=</b>&nbsp;
              		<input type="text" name="total5" id="total5" size="4" maxlenght="4" value="0" disabled="disabled" style="background-color:#ffffff" /> GBP
              	</td>
              	 <td width="200" align="left"><font color="#005b90">50 GBP X </font></td>
              	<td width="250">&nbsp;
              		<input type="text" name="field50" id="field50" size="4" maxlenght="4" value="0" onKeyUp="KeyDown('50')" />
              		&nbsp;<b>=</b>&nbsp;
              		<input type="text" name="total50" id="total50" size="4" maxlenght="4" value="0" disabled="disabled" style="background-color:#ffffff" /> GBP
              	</td>
            </tr>
            <tr bgcolor="#ffffff">
              	<td width="200" align="left">&nbsp;</td>
              	<td width="250">Total Paid:
              	</td>
              	 <td width="200" align="left">&nbsp;
              	 		<input type="text" name="total" id="total" size="4" maxlenght="4" value="0" disabled="disabled" style="background-color:#ffffff" /> GBP
              	 		
              	 </td>
              	<td width="250">&nbsp;</td>
            </tr> 
            <tr bgcolor="#ffffff">
              	<td width="200" align="left">&nbsp;</td>
              	<td width="250">Change Given:
              	</td>
              	 <td width="200" align="left">&nbsp;
              	 		<input type="text" name="changeGiven" id="changeGiven" size="4" maxlenght="4" value="0" readonly="readonly" style="background-color:#ffffff" /> GBP
              	 </td>
              	<td width="250">&nbsp;</td>
            </tr>
			
			<? if(CONFIG_DISPLAY_MONEY_PAID_BY_CHEQUE == false) { ?>
			<tr bgcolor="#ffffff" id="chequeRow" style="display:none">
              	<td width="200" align="right">&nbsp;</td>
              	<td width="250" align="right"> 
					<input type="checkbox" name="treatAsCheque" id="treatAsCheque" value="Y" />&nbsp;
					Treat this amount as cheque? &nbsp;&nbsp;
              	</td>
              	<td width="200" align="left">&nbsp;
					&nbsp;&nbsp;Cheque Number <input type="text" name="treatChequeNum" id="treatChequeNum" value="" size="20" />
              	</td>
              	<td width="250">&nbsp;</td>
            </tr>
			<? } ?>
			 
            
            
              <tr bgcolor="#ffffff">
              	 <td width="100" align="left" colspan="4"  height="25"><font color="#005b90"><strong></strong></font></td>
              	
            </tr>        
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">CUSTOMER SIGNATURE</font></td>
              	<td width="200">&nbsp;
              		
              	</td>
							<td width="150" align="left"><font color="#005b90">AGENT SIGNATURE</font></td>
              	<td width="200">&nbsp;
              		
              	</td>		              	 
            </tr> 
          
            </table>
          
            <br>
            </fieldset></td>
           
          </tr>         
        
           <tr>
            <td align="center">
				 <!--<form name="addTrans" action="add-transaction-conf.php" method="post">-->
				  <input name="benAgentParentID" type="hidden" value="<? echo $benAgentParentID?>">
				  <input name="custAgentParentID" type="hidden" value="<? echo $custAgentParentID?>">
				  <input type="hidden" name="imReferenceNumber" value="<?=$imReferenceNumber;?>">
				  <input type="hidden" name="refNumber" value="<?=$_SESSION["refNumber"];?>">
				  <input type="hidden" name="mydDate" value="<?=$nowTime;?>">
					<input name="distribut" type="hidden" value="<? echo $_POST["distribut"];?>">				  				  
					<input name="senderAccumulativeAmount" type="hidden" value="<?=$senderAccumulativeAmount;?>">	
			  
	  <? 
	
	  		foreach ($_POST as $k=>$v) 
			{
				$hiddenFields .= "<input name=\"" . $k . "\" type=\"hidden\" value=\"". $v ."\">\n";
				$str .= "$k\t\t:$v<br>";
			}
		
echo $hiddenFields;//window.open('printing_reciept.php?transID=< echo $imReferenceNumber; >', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')

	  ?> 

			<div class='noPrint'>
				 <input type="Submit" name="SubmitButton" value="Confirm Order"  id="once" > 
			<!--	<input type="Submit" name="SubmitButton" value="Confirm Order"  id="once"  checkForm(addTrans,'yes')" > -->
     </div>
			</td>
          </tr>
          
	          <tr>
	            <td align="right">
	             <div class='noPrint'>
	            	<a href="add-transaction.php?transID=<? echo $_POST["transID"]?>" class="style2">Change Information</a>
	             </div>
	            </td>
	        
	          </tr>
	        
            </table>
			            </form>			

            </td>
          </tr>
        </table></td>
    </tr>

</table>
</body>
</html>