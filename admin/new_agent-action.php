<?php
// Session Handling
session_start();
///////////////////////History is maintained via method named 'activities'

// Including files
include ("../include/config.php");
include ("security.php");
include("connectOtherDataBase.php");
$agentType2 = getAgentType();
$parentID2 = $_SESSION["loggedUserData"]["userID"];
$backURL = "new_agent.php";

define("PIC_SIZE","51200");

$services = "";
for ($i=0;$i<count($_POST["services"]);$i++)
{
	$servicesValue=$_POST["services"][$i];
	if ($i===count($_POST["services"])-1)
		$services .= $servicesValue;
	else
		$services .= $servicesValue.", ";
}

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$logo = $_FILES["logo"]["tmp_name"];
$logo_name = $_FILES["logo"]["name"];
$logo_path = 'logos/' . $logo_name;
$target_file = 'logos/' . basename($_FILES["logo"]["name"]);
$agentIDNumberFlag = false;
if(defined("CONFIG_SUP_AGENT_DIST_ID_NUMBER") && CONFIG_SUP_AGENT_DIST_ID_NUMBER=="1"){
	$agentIDNumberFlag = true;
}
if ($_GET["userID"]!="")
{
	$userID = $_GET["userID"];
}

if($_GET["searchBy"] != "")
{
	$searchBy = $_GET["searchBy"];
}

if ($_GET["Country"] != ""){
	$Country = $_GET["Country"];
}
if ($_GET["agentStatus"] != ""){
	$agentStatus = $_GET["agentStatus"];
}
if ($_GET["agentCode"] != ""){
	$agentCode = $_GET["agentCode"];
}
if ($_GET["type"] != ""){
	$typeV = $_GET["type"];
}
if ($_GET["ida"] != ""){
	$ida = $_GET["ida"];
}
$pageID = $_POST["pageID"];
$alertID = $_POST["alertID"];

$pageID = $_POST["pageID"];
$alertID = $_POST["alertID"];
if ($_REQUEST["mode"] != ""){
	$mode = $_REQUEST["mode"];
}

switch(trim($_POST["Country"])){
	case "United States": $ccode = "US"; break;
	case "Canada": $ccode = "CA"; break;
	default:
		$countryCode = selectFrom("select countryCode from ".TBL_COUNTRY." where countryName='".trim($_POST["Country"])."'");
		$ccode = $countryCode["countryCode"];
		break;
}

//echo  implode(",", $_POST["IDAcountry"]);
//echo implode(" ", $arr );
//exit;

/*if (CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y" || CONFIG_MANUAL_DISTRIBUTOR_NUMBER == "1" && $_GET["ida"] == "Y") {
        if (trim($_POST["manualAgentNum"]) == "") {
                insertError(AG42) ;
                redirect($backURL) ;
        } else {
                if ($_POST["userID"] == "") {
                        $qUserName = "SELECT `username` FROM ".TBL_ADMIN_USERS." WHERE `username` = '".trim($_POST["manualAgentNum"])."'" ;
                        $qUserRes = selectFrom($qUserName) ;
                        if ($qUserRes["username"] != "") {
                                insertError(AG43) ;
                                redirect($backURL) ;
                        }
                }
        }
}*/

//check if agent with the same country exists
if(CONFIG_ONLINE_AGENT == '1' && $_SESSION["isOnline"] == 'Y' && !defined("CONFIG_USE_VIRTUAL_AGENT")){
	$agentCountryQuery = selectFrom("SELECT userId,username FROM ".TBL_ADMIN_USERS." WHERE agentCountry = '".$agentCountry."' and isOnline = 'Y'" );
	$usernameOnline = $agentCountryQuery["username"];
}

/*
if (trim($_POST["agentCompany"]) == ""){
	insertError(AG1);
	redirect($backURL);
}

if ($usernameOnline != "" && CONFIG_ONLINE_AGENT == '1' && $_SESSION["isOnline"] == 'Y'){

	insertError(AG53.$usernameOnline.AG54);
	redirect($backURL);
}

if (trim($_POST["agentContactPerson"]) == ""){
	insertError(AG2);
	redirect($backURL);
}
if (trim($_POST["agentAddress"]) == ""){
	insertError(AG3);
	redirect($backURL);
}
if (trim($_POST["Country"]) == ""){
	insertError(AG4);
	redirect($backURL);
}*/

/*
if (trim($_POST["City"]) == ""){
	insertError(AG5);
	redirect($backURL);
}
if (trim($_POST["agentZip"]) == ""){
	insertError(AG6);
	redirect($backURL);
}*/
/*if (trim($_POST["agentPhone"]) == ""){
	insertError(AG7);
	redirect($backURL);
}*/

//if($_SESSION["defaultDistrib"] == 'Y')
//{
//	/**
//	 * If want to replace the existing distributor
//	 * @Ticket# 3506
//	 */
//	if($_SESSION["replaceDefaultDistributor"] == 'Y')
//	{
//		foreach ($_POST["IDAcountry"] as $countryValue)
//		{
//			//echo("select userID, username from admin where IDAcountry like '%$countryValue%'");
//	   	$existingIDA = selectFrom("select userID, username from admin where IDAcountry like '%$countryValue%' and defaultDistrib = 'Y'");
//   		if($existingIDA["username"] != "")
//   		{
//   			/**
//   			 * Overwriting the default distributor, by setting it to 'N'
//   			 */
//   			$strOverwriteSql = "update admin set defaultDistrib = 'N' where userID=".$existingIDA["userID"];
//   			update($strOverwriteSql);
//
//   			insertError("Default Distributor ".$existingIDA["username"]." for $countryValue has been updated to non default");
//   		}
//		}
//	}
//	else
//	{
//		/**
//		 * Just to know, which distributor is difault
//		 */
//		foreach ($_POST["IDAcountry"] as $countryValue)
//		{
//			//echo("select userID, username from admin where IDAcountry like '%$countryValue%'");
//	   	$existingIDA = selectFrom("select userID, username from admin where IDAcountry like '%$countryValue%' and defaultDistrib = 'Y'");
//   		if($existingIDA["username"] != "")
//   		{
//   			insertError("Already Exist Default Distributor ".$existingIDA["username"]." for $countryValue");
//				redirect($backURL);
//   		}
//		}
//
//	}
//}
/*
if (CONFIG_FREETEXT_IDFIELDS == '1') {
	$maxFutureYear = date("Y") + CONFIG_IDEXPIRY_YEAR_DELTA;
	if ($_POST["dDate"] != "") {
		//$dDate = explode("-", $_POST["dDate"]);
		$dyear = substr($_POST["dDate"],6,4);
		// Pass an array of date into this function and it returns true or false...
		if (isValidDate($_POST["dDate"])) {
			if ($dyear <= $maxFutureYear) {
				$idExpiryDate = substr($_POST["dDate"],6,4) . "-" . substr($_POST["dDate"],3,2)	. "-" . substr($_POST["dDate"],0,2);
				if ($idExpiryDate <= date("Y-m-d")) {
					insertError(AG44);
					redirect($backURL);
				}
			} else {
				insertError("ID expiry year should not be greater than " . $maxFutureYear . ". If so, then contact to your admin.");
				redirect($backURL);
			}
		} else {
			insertError("Invalid Date Format");
			redirect($backURL);
		}
	} else {
		$idExpiryDate = "";
	}
} else if (CONFIG_IDEXPIRY_WITH_CALENDAR == "1") {
	if ($_POST["dDate"] != "") {
		$dDate = explode("/", $_POST["dDate"]);
		$idExpiryDate = $dDate[2] . "-" . $dDate[1]	. "-" . $dDate[0];
		if ($idExpiryDate <= date("Y-m-d")) {
			insertError(AG44);
			redirect($backURL);
		}
	} else {
		$idExpiryDate = "";
	}
} else {
	$idExpiryDate = $_POST["idYear"] . "-" . $_POST["idMonth"] . "-" . $_POST["idDay"];
}


if ($logo_name != "")
{
	if ($logo_size>PIC_SIZE)
	{
		insertError("Image size must be less than 50K");
		redirect($backURL);
	}
	if (extension($logo_name)!=1)
	{
		insertError("Invalid file format. Only gif, jpg and png are allowed");
		redirect($backURL);
	}
}*/

if ($_POST["agent_type"]== "")
	$agent_type = 'Supper';
else
	$agent_type = $_POST["agent_type"];



if ($_POST["userID"] == ""){


	$password = createCode();
	$agentNumber = selectFrom("select MAX(agentNumber) from ".TBL_ADMIN_USERS." where agentCountry='".$_POST["Country"]."'");
	if (CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y" || CONFIG_MANUAL_DISTRIBUTOR_NUMBER == "1" && $_GET["ida"] == "Y") { // Changed by Jamshed 17-Nov 07
		$agentCode = trim($_POST["manualAgentNum"]) ;
	} else {
		$agentCode = "".$systemPre."".$ccode.($agentNumber[0]+1);///Changed Kashi 11_Nov
	}
	/*		if (isExist("SELECT `username` FROM " . TBL_ADMIN_USERS . " WHERE `username` = '".$agentCode."'")) {
                insertError(AG43);
                redirect($backURL);
            }
            if (isExist("SELECT `username` FROM " . TBL_CM_CUSTOMER . " WHERE `username` = '".$agentCode."'")) {
                insertError(AG43);
                redirect($backURL);
            }
            if (isExist("SELECT `accountName` FROM " . TBL_CUSTOMER . " WHERE `accountName` = '".$agentCode."'")) {
                insertError(AG43);
                redirect($backURL);
            }
            if (isExist("SELECT `loginName` FROM " . TBL_TELLER . " WHERE `loginName` = '".$agentCode."'")) {
                insertError(AG43);
                redirect($backURL);
            }*/




	$strQuery = "select max(userID) as maxid from admin";
	$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
	$rstRow = mysql_fetch_array($nResult);
	$supAgentID = ($rstRow['maxid']+1);
	$insertionKeys = "";
	$insertionValues = "";
	if(CONFIG_UNLIMITED_EX_RATE_OPTION == '1')
	{
		$insertionKeys .= " ,rateCountries, unlimitedRate";
		$insertionValues .= " ,'".$_SESSION["rateCountries"]."', '".$_POST["unlimitedRate"]."'";
	}
	if(CONFIG_AGENT_COMPANY_TYPE == "1"){
		$insertionKeys .= " ,userType ";
		$insertionValues .= " ,'".$_POST["userType"]."' ";
	}
	/*** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents ***/
	$assocDistDBField = "";
	$csvAssocDistIDs = "";
	$assocDistUpdateClause = "";

	if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
		&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1 )
	{
		if ( !empty($_REQUEST["assocDistributors"]) )
		{
			$assocDistDBField = "associated_distributors";
			$csvAssocDistIDs = implode( ",", $_REQUEST["assocDistributors"] );
			$assocDistUpdateClause = " associated_distributors='" . $csvAssocDistIDs . "', ";
		}
		else
		{
			$assocDistUpdateClause = " associated_distributors='', ";
		}
	}
	/***** End of code against #3428: Connect Plus - Select Distributors for Agents *****/
	/* #4926 FaithExchange
		Bank charges added for specific distributor.
	*/
	$Querry_Sqls = "INSERT INTO ".TBL_ADMIN_USERS." (username, password, changedPwd, name, email, created, rights, backDays, parentID, 
		agentNumber, agentCompany, agentContactPerson, agentAddress, agentAddress2, agentCity, agentZip, agentCountry, 
		agentCountryCode, agentPhone, agentFax, agentURL, agentMSBNumber,  agentHouseNumber,agentMCBExpiry, agentCompRegNumber, agentCompDirector,
		 agentDirectorAdd, agentProofID, agentIDExpiry, agentDocumentProvided, agentBank, agentAccountName, agentAccounNumber,
		  agentBranchCode, agentAccountType, agentCurrency, agentAccountLimit, " . (!empty($assocDistDBField) ? $assocDistDBField . ", " : "") . " commPackage ,agentCommission, agentStatus, 
		  isCorrespondent, IDAcountry, custCountries, logo, agentType, accessFromIP, authorizedFor, postCode,payinBook, isOnline, defaultDistrib, paymentMode, settlementCurrency, hasClave,distAlias,notes $insertionKeys, services) VALUES 
		('$agentCode', '$password', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["email"])."',
		 '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$_POST["rights"]."', '".$backDays."', '".$_POST["supAgentID"]."', '".($agentNumber[0]+1)."', '".checkValues($_POST["agentCompany"])."',
		  '".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["agentAddress"])."', 
		  '".checkValues($_POST["agentAddress2"])."', '".$_POST["City"]."', '".$_POST["agentZip"]."', '".$_POST["Country"]."', 
		  '$ccode', '".checkValues($_POST["agentPhone"])."', '".checkValues($_POST["agentFax"])."', 
		  '".checkValues($_POST["agentURL"])."', '".checkValues($_POST["agentMSBNumber"])."','".checkValues($_POST["agentHouseNumber"])."',
		   '".$_POST["msbYear"]."-".$_POST["msbMonth"]."-".$_POST["msbDay"]."', '".checkValues($_POST["agentCompRegNumber"])."', 
		   '".checkValues($_POST["agentCompDirector"])."', '".checkValues($_POST["agentDirectorAdd"])."', 
		   '".checkValues($_POST["agentProofID"])."', '".$idExpiryDate."', 
		   '".$_SESSION["agentDocumentProvided"]."', '".checkValues($_POST["agentBank"])."', 
		'".checkValues($_POST["agentAccountName"])."', '".checkValues($_POST["agentAccounNumber"])."', 
		'".checkValues($_POST["agentBranchCode"])."', '".$_POST["agentAccountType"]."', '".$_POST["agentCurrency"]."', 
		'".checkValues($_POST["agentAccountLimit"])."', " . (!empty($csvAssocDistIDs) ? " '" . $csvAssocDistIDs . "', " : "") . "'".checkValues($_POST["commPackage"])."', 
		'".checkValues($_POST["agentCommission"])."', '".$_POST["agentStatus"]."', '".$_POST["correspondent"]."', 
		'".$_SESSION["IDAcountry"]."', '".$_POST["Country"]."', '$logo_path', '$agent_type', '".checkValues($_POST["accessFromIP"])."', '".checkValues($_SESSION["authorizedFor"])."','".$_POST["postcode"]."','".$_POST["payinBook"]."', '".$_SESSION["isOnline"]."', '".$_SESSION["defaultDistrib"]."', '".$_POST["agentPaymentMode"]."','".$_POST["currencyOrigin"]."', '".$_POST["hasClave"]."','".$_POST["distAlias"]."','".$notes."' $insertionValues, '".$services."')";

	move_uploaded_file($logo, $logo_path);

	insertInto($Querry_Sqls);
	$userID = @mysql_insert_id();
	$arrLastInsertId = selectFrom("select LAST_INSERT_ID() as li");
	$insertedID = $arrLastInsertId["li"];
	$userID = $insertedID;
	//update("update ".TBL_ADMIN_USERS." set agentCommCurrency = '".$_POST["agentCommCurrency"]."' where userID='".$insertedID."'");

	$fromName = SUPPORT_NAME;
	$fromEmail = SUPPORT_EMAIL;
	$subject = "Admin Account creation at $company";
	$message = "Congratulations! ".$_POST["name"].",<br>
		Your agent account has been created at $company agent module. Your login information is as follows:<br>
		Login: ".$agentCode."<br>
		Password: ".$password."<br>
		Name: ".$_POST["agentContactPerson"]."<br>
		To access your account please <a href=\"".ADMIN_ACCOUNT_URL."\">click here</a><br>
		$company Support";

	if(CONFIG_MAIL_GENERATOR == '1'){
		mail($_POST["email"], $subject, $message, $fromName, $fromEmail);
	}

	////To record in History
	$descript = "Agent is added";
	activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,TBL_ADMIN_USERS,$descript);

	/*	if($_POST["supAagentID"] != "")
        {
            if($_POST["correspondent"] == "ONLY")
            {
                insertError(eregi_replace("Super",'Sub', eregi_replace("Agent",'Distributor', AG16)). " The login name is <b>$agentCode</b> and password is <b>$password</b>");
            }
            else
            {
                insertError(eregi_replace("Super",'Sub', AG16). " The login name is <b>$agentCode</b> and password is <b>$password</b>");
            }
        }
        else
        {
            if($_POST["correspondent"] == "ONLY")
            {
                insertError(eregi_replace("Agent",'Distributor', AG16). " The login name is <b>$agentCode</b> and password is <b>$password</b>");
            }
            else
            {
                insertError(AG16 . " The login name is <b>$agentCode</b> and password is <b>$password</b>");
            }
        }

            $strConfirmMessage =  ($_POST["correspondent"] == "ONLY" ? eregi_replace("Agent",'Distributor', AG16) : AG16). " The login name is <b>$agentCode</b> and password is <b>$password</b>";

            if(CONFIG_USE_VIRTUAL_AGENT == "1" && !empty($_POST["isOnline"]) && $_POST["isOnline"] == "Y")
                $strConfirmMessage .= "<br />Virtual Agent URL: <a target='_blank' href='".CONFIG_VIRTUAL_AGENT_URL_PREFIX."/user/?va=".$insertedID."' style=\"color:#669933\">".CONFIG_VIRTUAL_AGENT_URL_PREFIX."/user/?va=".$insertedID."</a>";

            //insertError( ($_POST["correspondent"] == "ONLY" ? eregi_replace("Agent",'Distributor', AG16) : AG16). " The login name is <b>$agentCode</b> and password is <b>$password</b>");

            insertError($strConfirmMessage);

            $backURL .= "&success=Y";*/
}
//echo "S";
if($_GET['source']=='new_agent.php')
{
	$backURL.="?userID=$userID";
}
redirect($backURL);
?>