<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
//include_once ("secureAllPages.php");
include ("javaScript.php");
$currdate=date('Y-m-d h:i:s');
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");
//session_register["agentID2"];

if($_POST["Submit"]=="Search")
{
	
	$_SESSION["grandTotal"]="";
	$_SESSION["CurrentTotal"]="";

/*
	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";
			
		
	$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];
	*/
	
	$_SESSION["closingBalance"] = "";
		$_SESSION["openingBalance"] = "";
		$_SESSION["currDate"] = "";	
	
}

if($_GET["fromDate"]!="")
$_POST["fromDate"] = $_GET["fromDate"] ;
if($_GET["toDate"]!="")
$_POST["toDate"] = $_GET["toDate"] ;

$fromDatevalue=$_POST["fromDate"];
$toDatevalue=$_POST["toDate"];
if(!empty($fromDatevalue)){
$fromDatevalue = str_replace('/', '-', $fromDatevalue);
$fromDate= date("Y-m-d",strtotime($fromDatevalue));
}
else
$fromDate=$currdate;
if(!empty($toDatevalue)){
$toDatevalue = str_replace('/', '-', $toDatevalue);
$toDate= date("Y-m-d",strtotime($toDatevalue));
}
else
$toDate=$currdate;


//debug($fromDate);
//debug($toDate);


if($_POST["agentID"]!="")
	$_SESSION["agentID2"] =$_POST["agentID"];
	elseif($_GET["agentID"]!="")
		$_SESSION["agentID2"]=$_GET["agentID"];
	else 
		$_SESSION["agentID2"] = "";

if($_POST["currencySettlement"]!="")
	$_SESSION["currencySettlement"]=$_POST["currencySettlement"];
elseif ($_GET["currencySettlement"]!="")
$_SESSION["currencySettlement"]=$_GET["currencySettlement"];
else
$_SESSION["currencySettlement"]= "";



	
if ($offset == "")
	$offset = 0;
$limit=20;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;

////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPA" && $agentType != "SUPAI")
		{
			$condition = True;	
		}
	}else{
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager")
			{
				$condition = True;
			}
		
		}
		
//////////////////////////Making of Condition///////////////
            
      
?>
<html>
<head>
	<title>Agent Account Statement</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./javascript/functions.js"></script>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#printReport").click(function(){
		// Maintain report print logs 
		$.ajax({
			url: "maintainReportsLogs.php",
			type: "post",
			data:{
				maintainLogsAjax: "Yes",
				pageUrl: "<?=$_SERVER['PHP_SELF']?>",
				action: "P"
			}
		});
		
		$("#searchSection").hide();
		$("#pagination").hide();
		$("#actionBtn").hide();
		print();
		$("#searchSection").show();
		$("#pagination").show();
		$("#actionBtn").show();
	});
});


var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function delete_confirm(id)
{
  if(confirm("Are you sure you want to delete this item?"))
    return true;
  else
    return false;
}

function showRow(isVisible) {
	if (isVisible) {
		document.getElementById().style.display = 'table-row';	
	} else {
		document.getElementById().style.display = 'none';
	}
}
function checkForm(theForm){
<? if(defined("CURRENCY_AT_ACCOUNT_DEPOSIT") && CURRENCY_AT_ACCOUNT_DEPOSIT=="1"){ ?>	
	if(theForm.currency.value==""){
		alert("Please select the currency.");
		return false;
	}
	return true;
<? }?>
}
function clearCheque(recId)
{
	var confirmClear = confirm("Are you sure you want to Clear this Cheque ?");
	if(!confirmClear){
		return true;
	}
	$.ajax
	({
		type: "GET",
		url: "clearCheque.php",
		data: "id=" + recId,
		beforeSend: function(XMLHttpRequest)
		{
			$("#status"+recId).text("Processing...");
		},
		success: function(responseText)
		{
			$("#status"+recId).html(responseText);
		}
	});
}
</script>
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	 <tr>
    <td class="topbar"><?=__("Agent")?> Account Statement</td>
  </tr>
	<tr>
      <td width="563">
	<form action="agent_Account.php" id="agentAccount" method="post" name="Search" onSubmit="return checkForm(this);">
  		<div align="center" id="searchSection">
                  From Date 
                  	<input name="fromDate" type="text" id="from" readonly >
                  	<!--input name="from" type="text" id="from" value="<?=$from1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('Search.fromDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" title="Select Date From|Please select date from calendar"><img src="images/show-calendar.gif" width=24 height=15 border=0> </a>&nbsp; &nbsp;To Date&nbsp;
		    		    <input name="toDate" type="text" id="to" readonly >
		    		    <!--input name="to" type="text" id="to" value="<?=$to1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('Search.toDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" title="Select Date To|Please select date from calendar"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
		    		    <!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->		    		    
               
                <br>
                &nbsp;&nbsp; 
                 <?    
                 if($condition)
				 {
				  ?>
                <select name="agentID" style="font-family:verdana; font-size: 11px">
                  <option value="">- Select Agent-</option>
                  <?
	//work by Mudassar Ticket #11425
		if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){
                  $agentQuery  = "select userID,username, agentCompany, name, agentContactPerson, agentStatus from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper'and isCorrespondent = 'N' AND agentStatus = 'Active' ";
					}
		else if (CONFIG_AGENT_WITH_ALL_STATUS==1){
		 $agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper'and isCorrespondent = 'N'";
		 }
	//work end by Mudassar Ticket #11425
								if($agentType == "Branch Manager")
								{
									$agentQuery .= " and parentID = '$changedBy'";				
									
								}
						 	$agentQuery .= "order by agentCompany";
								$agents = selectMultiRecords($agentQuery);
					for ($i=0; $i < count($agents); $i++){
				?>
                  <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
                  <?
					}
				?>
                </select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID2"]; ?>");
                                </script>
                 <? } ?>
<? 
          		if(defined("CURRENCY_AT_ACCOUNT_DEPOSIT") && CURRENCY_AT_ACCOUNT_DEPOSIT == "1" )
          		{
          		?>       
          			<br><br>
								Currency&nbsp;<font color="#FF0000">*</font> :
								<select id="currency" name="currency" style="font-family:verdana; font-size: 11px; width:130">
		          			<option value="">- Select Currency -</option>
          				<?
				
								$ledgerCurrency = "select distinct currency from ".TBL_AGENT_ACCOUNT. " WHERE currency!='' ";
								$ledgerCurrencyData = selectMultiRecords($ledgerCurrency);
								$y=0;			
								for($b = 0; $b < count($ledgerCurrencyData); $b++)
								{
										$allCurrency[$y]= $ledgerCurrencyData[$b]['currency'];
										$y++;
								}
								if(count($allCurrency)<1){
									$allCurrency[0] = "GBP";
								}
								$uniqueCurrency = array_unique($allCurrency);
							
				         		$uniqueCurrency = array_values ($uniqueCurrency);
										
				   	   			for($k = 0; $k < count($uniqueCurrency); $k++)
				   	   			{
								?>
									<option value="<?=trim($uniqueCurrency[$k])?>"><?=trim($uniqueCurrency[$k])?></option>
								<? 
								} 
								?>
					</select>
           
             		<script language="JavaScript">
         	    		SelectOption(document.forms[0].currency, "<?=$_REQUEST["currency"]; ?>");
             		</script>
					<br/>
						  <? 
							} 
							?>				 
				 <?
				 	if(CONFIG_ENABLE_FILTER_ON_SEARCH_BY_CHEQUE_NUM == "true")
					{
				?>
						<br />
						Cheque Number: <input type="text" name="chequeNumber" id="chequeNumber" size="30" value="<?=$_REQUEST["chequeNumber"]?>" />
				<?
					}
				 ?>
				 
                <input type="submit" name="Submit" value="Search">
              </div>
        
		<tr>
		<td>
		
      <?
	 if($_POST["Submit"]=="Search"||$_GET["search"]=="search")
	 {
	 	
	 	$Balance="";
		if($_GET["search"]=="search")
			$slctID=$_GET["agentID"];
		else
			{
				$slctID=$_POST["agentID"];	
			}
	
			//$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
			//$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];

	
			$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
			
				 	
	      // $settlementCurr  = selectFrom("select settlementCurrency from admin  where 1 and userID=".$slctID." ");	
         //$settleCurr=$settlementCurr["settlementCurrency"];  
			//$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
			//$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
			$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
			
	        $settlementCurr  = selectFrom("select settlementCurrency from admin  where 1 and userID='".$slctID."' ");	
            $settleCurr=$settlementCurr["settlementCurrency"];  
			$currencyQuery ='';
			if(defined("CURRENCY_AT_ACCOUNT_DEPOSIT") && CURRENCY_AT_ACCOUNT_DEPOSIT == "1" && $_REQUEST["currency"]!=''){
				$currencyQuery = " and currency ='".$_REQUEST["currency"]."' ";
				if($_REQUEST["currency"]=="GBP")
					$currencyQuery = " and (currency ='".$_REQUEST["currency"]."' OR currency ='') ";
				$settleCurr = $_REQUEST["currency"];
				$currencyQueryStr = "&currency=".$_REQUEST["currency"];
			}
			elseif(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == "1" && $settleCurr!='') {
				$currencyQuery = " and currency ='".$settleCurr."' ";
				$currencyQueryStr = "&currency=".$settleCurr;
		 	}	
			$provisional = " and status != 'Provisional' ";
		if ($slctID !=""){
			
			$accountQuery = "Select * from agent_account where agentID = $slctID";
			$accountQuery .= " and $queryDate";				
			$accountQuery .= $provisional;
			$accountQuery .= $currencyQuery;
				
			$accountQueryCnt = "Select count(*) from agent_account where agentID = $slctID";
			$accountQueryCnt .= " and $queryDate";				
			$accountQueryCnt .= $provisional;
			$accountQueryCnt .= $currencyQuery;

			 
			 if(CONFIG_ENABLE_FILTER_ON_SEARCH_BY_CHEQUE_NUM == "true")
			 {
			 	$chequeNumber = trim($_REQUEST["chequeNumber"]);
			 
			 	if(!empty($chequeNumber))
				{
					$accountQuery .= " and chequeNumber ='".$chequeNumber."' ";
					$accountQueryCnt .= " and chequeNumber ='".$chequeNumber."' ";
				}
			 }
			 
		  /*else{
			    $accountQuery .= " and currency !='".$settleCurr."' ";
			    $accountQueryCnt .= " and currency !='".$settleCurr."' ";
		  }*/
			$allCount = countRecords($accountQueryCnt);
			//echo $queryBen;
			$accountQuery .= "order by aaID";
	  	$accountQuery .= " LIMIT $offset , $limit"; 
			
			$contentsAcc = selectMultiRecords($accountQuery);
		}else{
		
			$slctID = $changedBy;
			$accountQuery="Select * from agent_account where agentID = $changedBy";
			$accountQuery .= " and $queryDate";				
			$accountQuery .= $provisional;
			$accountQuery .= $currencyQuery;	
				
				
			$accountQueryCnt = "Select count(*) from agent_account where agentID = $changedBy";
			$accountQueryCnt .= " and $queryDate";				
			$accountQueryCnt .= $provisional;
			$accountQueryCnt .= $currencyQuery;
			
			if(CONFIG_ENABLE_FILTER_ON_SEARCH_BY_CHEQUE_NUM == "true")
			 {
				$chequeNumber = trim($_REQUEST["chequeNumber"]);
			 
				if(!empty($chequeNumber))
				{
					$accountQuery .= " and chequeNumber ='".$chequeNumber."' ";
					$accountQueryCnt .= " and chequeNumber ='".$chequeNumber."' ";
				}
			 }
			 
		  /*else{
			$accountQuery .= " and currency !='".$settleCurr."' ";
		  $accountQueryCnt .= " and currency !='".$settleCurr."' ";
		  }*/
			
			$allCount = countRecords($accountQueryCnt);
			//echo $queryBen;
			$accountQuery .= "order by aaID";
		 $accountQuery .= " LIMIT $offset , $limit"; 
			
			$contentsAcc = selectMultiRecords($accountQuery);
			}
		// debug($accountQuery);
		

		?>	
			
		  <table width="700" border="1" cellpadding="0" bordercolor="#666666" align="center">
			<tr>
			  <td height="25" nowrap bgcolor="#6699CC">
			  <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF" id="pagination">
				  <tr>
				  <td align="left"><?php if (count($contentsAcc) > 0) {;?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
					<?=$allCount; ?>
					<?php } ;?>
				  </td>
				  <?php if ($prv >= 0) { ?>
				  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=first".$currencyQueryStr."&fromDate=".$fromDate."&toDate=".$toDate ;?>"><font color="#005b90">First</font></a> </td>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&currDate=$currDate&closingBalance=$closingBalance&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=pre".$currencyQueryStr."&fromDate=".$fromDate."&toDate=".$toDate;?>"><font color="#005b90">Previous</font></a> </td>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&currDate=$currDate&closingBalance=$closingBalance&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=next".$currencyQueryStr."&fromDate=".$fromDate."&toDate=".$toDate;?>"><font color="#005b90">Next</font></a>&nbsp; </td>
				  <td width="50" align="right"><!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=last";?>"><font color="#005b90">Last</font></a-->&nbsp; </td>
				  <?php } ?>
				</tr>
				
			  </table></td>
			</tr>
			
			<?
			if(count($contentsAcc) > 0)
			{
			?>
			<tr>
			<td>&nbsp;
			<?		
			if(CONFIG_AGENT_STATEMENT_CUMULATIVE == '1')		
			{
			?>
				<table>
					<tr>
						<td width="300">
							<!-- Cumulative Total--><? //$allAmount=agentBalanceDate($slctID, $fromDate, $toDate); 
							/*$contantagentID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$slctID."'");
							$allAmount = $contantagentID["balance"];*/
							$agentBalanceFirst = selectFrom("select opening_balance, closing_balance, dated from " . TBL_ACCOUNT_SUMMARY . " where user_id ='".$slctID."' and dated = (select MIN(dated) from ".TBL_ACCOUNT_SUMMARY." where user_id ='".$slctID."' and dated >= '".$fromDate."')");
							$agentBalanceSecond = selectFrom("select opening_balance, closing_balance, dated from " . TBL_ACCOUNT_SUMMARY . " where user_id ='".$slctID."' and dated = (select MAX(dated) from ".TBL_ACCOUNT_SUMMARY." where user_id ='".$slctID."' and dated <= '".$toDate."')");
							
							$cumulativeOpeningBalance = $agentBalanceFirst["opening_balance"];
							$cumulativeClosingBalance = $agentBalanceSecond["closing_balance"]; 
							$cumulativeRunning = $cumulativeClosingBalance - $cumulativeOpeningBalance;
							number_format($cumulativeRunning,2,'.',',');
							
							
							?>
			</td>
			<td>
				<? if ($allAmount < 0 ){ ?>
				<font color="red"><strong>The agent has to pay the money </strong></font>
				<? 
			}		
			if ($allAmount > 0 )
			{		
			?>
			<strong> Agent Credit Balance  </strong>
			<? } ?>
			</td>
			</tr>
			</table>
			<?
		}
		?>
			</td>
		</tr>
			<tr>
			  <td nowrap bgcolor="#EFEFEF">
			  <table width="781" border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#FFFFFF">
					  <td align="left" ><span class="style1">Date</span></td>
					  <? 
					  if(CONFIG_AGENT_STATEMENT_FIELDS == '1')
            { 
            ?>
					  <td align="left" ><span class="style1">Modified By</span></td>
					  <? 
					  }
					  if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
					  {
					  ?>
					  <td align="left" ><span class="style1">Opening Balance</span></td>
					  <?
						}
					  ?>
					  
					  <td align="left" ><span class="style1">Money In</span></td>
					  <td align="left" ><span class="style1">Money Out</span></td>
					   
						<td align="left" ><span class="style1"><? echo $systemCode; ?></span></td>
						<td align="left" ><span class="style1"><? echo $manualCode; ?></span></td>
						<td align="left" ><span class="style1">Description</span></td>
						<td align="left" ><span class="style1">Note</span></td>	
						<? 
							if(CONFIG_AGENT_STATEMENT_FIELDS == '1')
							{ 
						?>
								<td align="left" ><span class="style1">Status</span></td>
						<?
							}
							
							if(CONFIG_DISPLAY_CHEQUE_FIELDS_ON_AGENT_ACCT_STMT == "true")
							{
						?>
								<td align="left" ><span class="style1">Cheque#</span></td>
								<td align="left" ><span class="style1">Cheque Status</span></td>
						<?
							}
							
							if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
							{
						?>
					  			<td align="left" ><span class="style1">Closing Balance</span></td>
						<?
					  		}
							
							if(CONFIG_AGENT_STATEMENT_FIELDS != '1')
							{
						?>
							   <td>&nbsp;</td> 
							   <td>&nbsp;</td> 
						<?
           					}
						?>
					</tr>
					
				<? 
				
				$preDate = "";
				if($_SESSION["currDate"] != "")
					$preDate = $_SESSION["currDate"];
				if($_SESSION["closingBalance"] != "")	
					$closingBalance = $_SESSION["closingBalance"];
				if($_SESSION["openingBalance"] != "")	
					$openingBalance = $_SESSION["openingBalance"];
				for($i=0;$i < count($contentsAcc);$i++)
				{
					
					///////////////////Opening and Closing////////////////
					
						if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
						{
							
							$currDate = $contentsAcc[$i]["dated"];
							if($currDate != $preDate)
							{
								if($currencyQuery!='') {
									$queryAgent = "select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate' ";
									$queryAgent .= $currencyQuery;
	 							$datesFrom = selectFrom($queryAgent);
							  }else{
							  	$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate'");
							  	}
								$FirstDated = $datesFrom["datedFrom"];
								 
								 $openingBalance = 0;
								
								 
								if($FirstDated != "")
								{
									$account1 = "Select opening_balance, closing_balance from ".TBL_ACCOUNT_SUMMARY." where 1";
									$account1 .= " and dated = '$FirstDated'";				
								  $account1 .= " and  user_id = '".$slctID."' ";	
								  $account1 .= $currencyQuery;		
								  			
									$contents1 = selectFrom($account1);
									
									if($currDate == $FirstDated){
										 $openingBalance =  $contents1["opening_balance"];
								  
									}else{
										$openingBalance =  $contents1["closing_balance"];
								
									}
								}
									
								
								 $closingBalance = $openingBalance;
								
								 $preDate = $currDate;
								
						//	$_SESSION["openingBalance"] = $openingBalance;
							//$_SESSION["settleOpeningBalance"] = $settleOpeningBalance;

							}
								
						}
						$_SESSION["currDate"] = $currDate;
						
																					
					/////////////////////////Opening and Closing///////////
				//$BeneficiryID = $contentsBen[$i]["benID"];
					?>
				<tr bgcolor="#FFFFFF">
					<td width="200" align="left"><strong><font color="#006699"><? 	
					$authoriseDate = $contentsAcc[$i]["dated"];
					echo date("F j, Y", strtotime("$authoriseDate"));
					?></font></strong></td>
				  <? 
				  if(CONFIG_AGENT_STATEMENT_FIELDS == '1')
          {
          ?>
					<td width="200" align="left">
					<? 
					if(strstr($contentsAcc[$i]["description"], "by Teller"))
					{
						$q=selectFrom("SELECT * FROM ".TBL_TELLER." WHERE tellerID ='".$contentsAcc[$i]["modified_by"]."'"); 
						
					}
					else
						$q=selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." WHERE userID ='".$contentsAcc[$i]["modified_by"]."'"); 
					echo $q["name"]; ?></td>
				 <? 
				  }
					  if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
					  {
					  ?>
					      <td width="200" align="left"><? echo number_format($openingBalance,2,'.',','); ?></td> 
					  
					  <?
					  
					 }
					?>
					
			
					
					<?
				
					  				
					
					$currentAmount = 0;
					if($contentsAcc[$i]["TransID"] > 0 && CONFIG_BALANCE_FROM_TRANSACTION == '1'){
						////////////////////Only Express Case///////////
							$trans = selectFrom("SELECT refNumberIM, refNumber, totalAmount, AgentComm, transAmount, refundFee,currencyFrom
							FROM ".TBL_TRANSACTIONS." WHERE transID ='".$contentsAcc[$i]["TransID"]."'"); 
							
					
							
							if($contentsAcc[$i]["type"] == "DEPOSIT")
									{
										if($trans["refundFee"] == 'No')
										{
											$currentAmount = $trans["transAmount"];
										}else{
											$currentAmount = $trans["totalAmount"] - $trans["AgentComm"];
											}
										$Balance = $Balance+$currentAmount;
									}	
									
									if($contentsAcc[$i]["type"] == "WITHDRAW")
									{
										$currentAmount = $trans["totalAmount"] - $trans["AgentComm"];
										$Balance = $Balance-$currentAmount;
									}
							
							
						}else{
						
									if($contentsAcc[$i]["type"] == "DEPOSIT")
									{
										$currentAmount = $contentsAcc[$i]["amount"];
									
										$Balance = $Balance+$currentAmount;
									}	
									
									if($contentsAcc[$i]["type"] == "WITHDRAW")
									{
										$currentAmount = $contentsAcc[$i]["amount"];
										$Balance = $Balance-$currentAmount;
									}
							
							 }	
							
					?>
					<td width="150" align="left"><?  
					if($contentsAcc[$i]["type"] == "DEPOSIT")
					{
						    if($currencyQuery!='') {
						    	 echo customNumberFormat($currentAmount)." ".$contentsAcc[$i]["currency"];
						    	}else{
						    	 echo $currentAmount;
					     	}
					}
					?></td>
					<td align="left"><? 
					if($contentsAcc[$i]["type"] == "WITHDRAW")
					{
						
						    if($currencyQuery!='') {
						    	 echo customNumberFormat($currentAmount)." ".$contentsAcc[$i]["currency"];
						    	}else{
						    	 echo customNumberFormat($currentAmount);
					     	}
						
						
					}
					?></td>
					      
					<? if($contentsAcc[$i]["TransID"] > 0){
							$trans = selectFrom("SELECT refNumberIM, refNumber FROM ".TBL_TRANSACTIONS." WHERE transID ='".$contentsAcc[$i]["TransID"]."'"); 
							
							?>
				<td><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsAcc[$i]["TransID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $trans["refNumberIM"]; ?></font></strong></a></td>
				<td><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsAcc[$i]["TransID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $trans["refNumber"]; ?></font></strong></a></td>
				<? }else{?>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<? }?>
				<td width="56">
					&nbsp;<? echo $contentsAcc[$i]["description"]; ?>
				</td>
				<td width="56">
					&nbsp;<? echo $contentsAcc[$i]["note"]; ?>
				</td>
				<? 
				if(CONFIG_AGENT_STATEMENT_FIELDS == '1')
        { 
        ?>
				<td width="56">
					&nbsp;<? echo $contentsAcc[$i]["status"]; ?>
				</td>
				<?
			  }
			  
			  	if(CONFIG_DISPLAY_CHEQUE_FIELDS_ON_AGENT_ACCT_STMT == "true")
				{
			?>
					<td width="56">&nbsp;<?=$contentsAcc[$i]["chequeNumber"]?></td>
					<td width="56">&nbsp;
						<?
							if($contentsAcc[$i]["chequeStatus"] == "UNCLEARED")
							{
			
								/** 
								 * Only Admin can clear the cheques 
								 * @Ticket #4017
								 */								 						 
								if($agentType == "admin" || $agentType == "Admin Manager" || $agentType == "Admin" || $agentType == "Call")
									echo "<div id='status".$contentsAcc[$i]["aaID"]."'><a href='#' style='{color:#0000ff}' title='Click to CLEAR the cheque.' onClick=\"javacript: clearCheque(".$contentsAcc[$i]["aaID"].");\">".$contentsAcc[$i]["chequeStatus"]."</a></div>";
								else
									echo $contentsAcc[$i]["chequeStatus"];
							} 
							else {
								echo $contentsAcc[$i]["chequeStatus"];
							}
						?>
					</td>
			<?
				}
			  
   			 if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
				{
					
					if($contentsAcc[$i]["chequeStatus"] == "CLEARED" || empty($contentsAcc[$i]["chequeStatus"]))
					{
						if($contentsAcc[$i]["type"] == "WITHDRAW")
						{
							 
						 $closingBalance =  $closingBalance - $contentsAcc[$i]["amount"];
						
						}elseif($contentsAcc[$i]["type"] == "DEPOSIT")
						{
							$closingBalance =  $closingBalance + $contentsAcc[$i]["amount"];
							
						}
					}
					 //$_SESSION["closingBalance"] = $closingBalance;
					 //$_SESSION["settleClosingBalance"] = $settleClosingBalance;
					
					 ?>
					   <td width="200" align="left"><? echo(number_format($closingBalance,2,'.',',')); ?></td>
					 				  
					  <?	
					    }
					  ?>
					  
					
        <?
        if(CONFIG_AGENT_STATEMENT_FIELDS != '1')
        { 
       	?>
        <td>&nbsp;</td> 
        <td>&nbsp;</td> 
        <?
        }
        ?>
					</tr>
				<?
				}
				?>
				<tr bgcolor="#FFFFFF">
				   <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <?
					  if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
					  {
					  ?>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <?
					}
					?>
				   <td>&nbsp;</td>
				  <td align="center">&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
		<?		
		if(CONFIG_AGENT_STATEMENT_CUMULATIVE == '1')		
		{
		?>
				<tr bgcolor="#FFFFFF">
				  <td colspan="3" align="left"><b>Page Total Amount</b></td>
	
				  <td align="left" colspan="2"><? echo  number_format($Balance,2,'.',','); ?></td>
				  	<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						
				</tr>			
				<tr> 
					  <td class="style3" width="200"> Running Total:</td>
					  <td width="200"> 
						<?
					if($_GET["total"]=="first")
					{
					$_SESSION["grandTotal"] = $Balance;
					$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="pre"){
						$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
						$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="next"){
						$_SESSION["grandTotal"] += $Balance;
						$_SESSION["CurrentTotal"]= $Balance;
					}else
						{
						$_SESSION["grandTotal"] = $Balance;
						$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
						}
					echo number_format($_SESSION["grandTotal"],2,'.',',');
					?>
					  </td>
				  	<td>&nbsp;</td>
					<td>&nbsp;</td>
						<td>&nbsp;</td>	
						<td>&nbsp;</td>
						<td>&nbsp;</td>
                </tr>
                <?
            }
            ?>
            <tr bgcolor="#FFFFFF">
              <td colspan="15">
<!--
/************************************************************************/

Here You will write your Comments or any Information

/************************************************************************/
-->			  
			  </td>

            </tr>
            <tr>
			  <td height="25" nowrap bgcolor="#6699CC" colspan="15">
			  <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
				  <tr>
				  <td align="left"><?php if (count($contentsAcc) > 0) {;?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
					<?=$allCount; ?>
					<?php } ;?>
				  </td>
				  <?php if ($prv >= 0) { ?>
				  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=first".$currencyQueryStr;?>"><font color="#005b90">First</font></a> </td>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&currDate=$currDate&closingBalance=$closingBalance&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=pre".$currencyQueryStr;?>"><font color="#005b90">Previous</font></a> </td>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&currDate=$currDate&closingBalance=$closingBalance&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=next".$currencyQueryStr;?>"><font color="#005b90">Next</font></a>&nbsp; </td>
				  <td width="50" align="right"><!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=last";?>"><font color="#005b90">Last</font></a-->&nbsp; </td>
				  <?php } ?>
				</tr>
				
			  </table></td>
			</tr>
            
            
            
            
			</table>		
            <?
			} // greater than zero
			else
			{
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Date and Agent </font>
				</td>
			</tr>
        <tr>
          <td nowrap bgcolor="#EFEFEF">
		  <table width="781" border="0" bordercolor="#EFEFEF">
            <tr bgcolor="#FFFFFF">
				<td width="93" align="left"><span class="style1">Date</span></td>
        <? 
        if(CONFIG_AGENT_STATEMENT_FIELDS == '1')
        { 
        ?>
        <td width="215" align="left"><span class="style1">Modified By</span></td>
         <? 
         }
         ?>           
          <td width="174" align="left"><span class="style1">Money In</span></td>
                    
         <td width="315" align="left">Money Out</td>
      
				 <td width="120"  align="left"><span class="style1">Transaction</span></td>
					<td width="96" align="left"><span class="style1">Description</span></td>
				<? 
				if(CONFIG_AGENT_STATEMENT_FIELDS == '1')
        { 
        ?>
				<td align="left" ><span class="style1">Status</span></td>
        <? 
        }
        if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
				{
				?>
				<td>&nbsp;</td>
				<?
			  }  
        if(CONFIG_AGENT_STATEMENT_FIELDS != '1')
        { 
         ?> 
        <td>&nbsp;</td> 
        <td>&nbsp;</td> 
        <?
        }
        ?>
            </tr>	
			<tr bgcolor="#FFFFFF">
				<td>&nbsp;</td>
      	<td>&nbsp;</td>
      	<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
      	<td align="left">&nbsp;</td>
      	<td>&nbsp;</td>
      	<td>&nbsp;</td>
     	</tr>
			</table>
		   </td>
		  </tr>
			<?
			}
			/*	@4345
				Modified and Added CONFIG_ACCOUNT_MANAGER_DEPOSIT to display for Account Manager(Admin Staff/Basic User)

			*/
			if($agentType == "admin" || ( CONFIG_ADMIN_MANAGER_DEPOSIT == "1" && $agentType == "Admin Manager") || ( CONFIG_ACCOUNT_MANAGER_DEPOSIT == "0" && $agentType == "Admin"))
			{
			if($slctID!=''){
			?>
			
				
			<tr>
		          <td height="15" bgcolor="#c0c0c0"><span class="child"><a HREF="add_Agent_Account.php?agent_Account=<?=$_SESSION["agentID2"];?>&modifiedBy=<?=$_SESSION["loggedUserData"]["userID"];?>&settleCurr=<?=$settleCurr ?>" target="mainFrame" class="tblItem style4"><font color="#990000">Deposit/Withdraw 
                    Money</font></a></span> </td>
			 
  	</tr>
	<? } 
}
	?>
       </table>
	   
	<?	
	 }else
			{
				$_SESSION["grandTotal"]="";
				$_SESSION["CurrentTotal"]="";
		 
	
				$_SESSION["fMonth"]="";
				$_SESSION["fDay"]="";
				$_SESSION["fYear"]="";
				
				$_SESSION["tMonth"]="";
				$_SESSION["tDay"]="";
				$_SESSION["tYear"]="";
			}///End of If Search
	 ?>
	 
	 </td>
	</tr>
	<? if($slctID!=''){?>
		<tr id="actionBtn">
		  	<td> 
				<div align="center">
					<input type="button" name="Submit2" value="Print This Report" id="printReport">
				</div>
			</td>
		  </tr>
		  <? }?>
	</form>
	</td>
	</tr>
</table>
<script language="JavaScript" type="text/javascript" src="tooltip.js"></script>
</body>
</html>