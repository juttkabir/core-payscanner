<?php
// Session Handling
session_start();
///////////////////////History is maintained via method named 'activities'
// Including files
include ("../include/config.php");
include ("security.php");
$tran_date = date("Y-m-d") ;
$from = $_GET["from"];
$backURL = "add-beneficiary-quick.php?notEndSessions=Y&from=".$from."&transID=".$_GET["transID"]."&r=".$_GET["r"];

switch(trim($_POST["Country"])){
case "United States": $ccode = "US"; break;
case "Canada": $ccode = "CA"; break;
default: 
	$countryCode = selectFrom("select countryCode from ".TBL_CITIES." where country='".trim($_POST["Country"])."'");
	$ccode = $countryCode["countryCode"];
break;
}


$phoneMandatoryCountFlag = false;
if(CONFIG_BEN_PHONE_MANDATORY_ONLY_UK=="1"){
	$phoneMandatoryCountFlag = true;
}

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}

if($_GET["r"] == 'dom')///If transaction is domestic
{
	$returnPage = 'add-transaction-domestic.php';
} 
elseif($_GET["r"]=='searchBen'){ 
	$returnPage = 'create-bene-trans.php'; 
} 
else{ 
	$returnPage = $transactionPage;
}


if ($_POST["benID"] == ""){
	session_register("Title");
	session_register("other_title");
	session_register("firstName");
	session_register("lastName");
	session_register("middleName");
	session_register("S/O");
	session_register("sonOfType");
	session_register("IDType");
	session_register("IDNumber");
	session_register("Otherid_name");
	session_register("Otherid");
	session_register("Address");
	session_register("Address1");
	session_register("Country");
	session_register("City");
	session_register("Zip");
	session_register("State");
	session_register("Phone");
	session_register("Mobile");
	session_register("benEmail");
	session_register("userType");
	session_register("iban");
	session_register("emailAddress");
	session_register("serviceType");
	session_register("beneficiaryName");
	/**
     * If the beneficiary is with bank details
     */
	 
//	if(CONFIG_BEN_BANK_DETAILS == "1")
	if(CONFIG_BEN_BANK_DETAILS == "1" || $_POST["ibanbank"] == "bank"){
		session_register("bankName1");
		session_register("account1");
		session_register("branchName1");
		session_register("branchAddress1");
		session_register("iban1");
		session_register("swiftCode1");
		session_register("accountType1");
	}
	elseif($_POST["ibanbank"] == "iban"){
		session_register("iban");
	}
	/* End of code for bank details */

	$_SESSION["Title"] = $_POST["Title"];
	$_SESSION["other_title"] = $_POST["other_title"];
	$_SESSION["firstName"] = $_POST["firstName"];
	$_SESSION["lastName"] = $_POST["lastName"];
	$_SESSION["middleName"] = $_POST["middleName"];
	$_SESSION["secMiddleName"] = $_POST["secMiddleName"];
	$_SESSION["S/O"] = $_POST["S/O"];
	$_SESSION["sonOfType"] = $_POST["sonOfType"];
	$_SESSION["IDType"] = $_POST["IDType"];
	$_SESSION["IDNumber"] = $_POST["IDNumber"];
	$_SESSION["Otherid_name"] = $_POST["Otherid_name"];
	$_SESSION["Otherid"] = $_POST["Otherid"];
	$_SESSION["Address"] = $_POST["Address"];
	$_SESSION["Address1"] = $_POST["Address1"];
	$_SESSION["Country"] = $_POST["Country"];
	$_SESSION["City"] = $_POST["City"];
	$_SESSION["Zip"] = $_POST["Zip"];
	$_SESSION["State"] = $_POST["State"];
	$_SESSION["CPF"] = $_POST["CPF"];
	$_SESSION["Phone"] = $_POST["Phone"];
	$_SESSION["Mobile"] = $_POST["Mobile"];
	$_SESSION["benEmail"] = $_POST["benEmail"];
	$_SESSION["customerID"] = $_POST["customerID"];
	$_SESSION["Citizenship"] = $_POST["Citizenship"];
	$_SESSION["IDissuedate"] = $_POST["IDissuedate"];
	$_SESSION["IDexpirydate"] = $_POST["IDexpirydate"];
	$_SESSION["userType"] = $_POST["userType"];
	$_SESSION["iban"] = $_POST["iban"];
	$_SESSION["emailAddress"] = $_REQUEST["emailAddress"];
	$_SESSION["serviceType"] = $_POST["serviceType"];
	$_SESSION["beneficiaryName"] = $_POST["beneficiaryName"];
	
	/**
     * If the beneficiary is with bank details
     */
//	if(CONFIG_BEN_BANK_DETAILS == "1")
	
	if(CONFIG_BEN_BANK_DETAILS == "1" || $_POST["ibanbank"] == "bank"){
  		$_SESSION["usedBankId"] = $_POST["usedBankId"];
		$_SESSION["bankName1"] = $_POST["bankName"];
		$_SESSION["account1"] = $_POST["account"];
		$_SESSION["branchCode1"] = $_POST["branchName"];
		$_SESSION["branchAddress1"] = $_POST["branchAddress"];
//		$_SESSION["iban1"] = $_POST["iban"];
		$_SESSION["swiftCode1"] = $_POST["swiftCode"];
		$_SESSION["accountType1"] = $_POST["accountType"];
		$_SESSION["country1"] = $_POST["country1"];
	}
	else{//if($_POST["ibanbank"] == "iban"){
		$_SESSION["iban"] = $_POST["iban"];
	}
	/* End of code for bank details */
}

if(CONFIG_CITY_NON_COMPULSORY != '1'){ 
if (trim($_POST["City"]) == "" ){
	insertError(BE6);
	redirect($backURL);
}
}

if (trim($_POST["Country"]) == ""){
	insertError(BE5);
	redirect($backURL);
}

if (CONFIG_CPF_ENABLED == "1" && $_POST["Country"] == CONFIG_CPF_COUNTRY)
{
	/**
	 * Updated for the Ticket#3315
	 * By Jahangir
	 */
	require_once ("lib/classValidateCpfCpnj.php");
	$validate = new VALIDATE; 
	
	if($_POST["cpfcpnj"] == "CPF")
	{
		if(!$validate->cpf($_POST["CPF"]) && $_POST["CPF"] != "00000000000")
		{
			insertError(BE11);	
			redirect($backURL);
		}
	}
	elseif($_POST["cpfcpnj"] == "CPNJ")
	{
		if(!$validate->cnpj($_POST["CPF"]) && $_POST["CPF"] != "00000000000000")
		{
			insertError("CNPJ is not a valid number.");	
			redirect($backURL);
		}
	}
}

if (CONFIG_FREETEXT_IDFIELDS == '1') {
	$maxFutureYear = date("Y") + CONFIG_IDEXPIRY_YEAR_DELTA;
	if ($_POST["IDexpirydate"] != "") {
		$dyear = substr($_POST["IDexpirydate"],6,4);
		//$dDate = explode("-", $_POST["IDexpirydate"]);
		
		// Pass an array of date into this function and it returns true or false...
		if(isValidDate($_POST["IDexpirydate"])){
			if ($dyear <= $maxFutureYear) {
				$idExpiryDate = substr($_POST["IDexpirydate"],6,4) . "-" . substr($_POST["IDexpirydate"],3,2)	. "-" . substr($_POST["IDexpirydate"],0,2);
				if ($idExpiryDate <= date("Y-m-d")) {
					insertError(AG44);
					redirect($backURL);
				}
			} else {
				insertError("ID expiry year should not be greater than " . $maxFutureYear . ". If so, then contact to your admin.");
				redirect($backURL);
			}
		} else {
			insertError("Invalid Date Format");
			redirect($backURL);
		}
	} else {
		$idExpiryDate = "";	
	}
}else {
	if ($_POST["IDexpirydate"] != "") {
		//$dDate = explode("/", $_POST["IDexpirydate"]);
		$idExpiryDate = substr($_POST["IDexpirydate"],6,4) . "-" . substr($_POST["IDexpirydate"],3,2)	. "-" . substr($_POST["IDexpirydate"],0,2);
		if ($idExpiryDate <= date("Y-m-d")) {
			insertError(BE12);
			redirect($backURL);
		}
	} else {
		$idExpiryDate = "";		
	}
}
if($phoneMandatoryCountFlag){
	if($_POST["Country"]=="United Kingdom" && trim($_POST["Phone"]) == ""){
		insertError(BE7);
		redirect($backURL);
	}
}elseif (trim($_POST["Phone"]) == "" && CONFIG_PHONE_NON_COMPULSORY != '1'){
	insertError(BE7);
	redirect($backURL);
}

if (trim($_POST["IDType"]) == "" && CONFIG_ID_MANDATORY == '1') {		
		
		insertError("Select ID Type");
		redirect($backURL);
	}

if (trim($_POST["Mobile"]) == "" && CONFIG_MOBILE_MANDATORY == '1'){
	insertError(BE17);
	redirect($backURL);
}

	// Added by Usman Ghani aginst ticket #3472: Now Transfer - Phone/Mobile
	$phone = checkValues($_POST["Phone"]);
	$mobile = checkValues($_POST["Mobile"]);

	if($phoneMandatoryCountFlag &&  defined("CONFIG_PHONES_AS_DROPDOWN") && CONFIG_PHONES_AS_DROPDOWN == 1 ){
		if($_SESSION["Country"]=="United Kingdom"){
			if ( $_POST["phoneType"] == "phone" )
			{
				$phone = checkValues($_POST["Phone"]);
				$mobile = "";
			}
			else
			{
				$phone = "";
				$mobile = checkValues($_POST["Phone"]);
			}	
		}
	}elseif ( defined("CONFIG_PHONES_AS_DROPDOWN") && CONFIG_PHONES_AS_DROPDOWN == 1 )
	{
		if ( $_POST["phoneType"] == "phone" )
		{
			$phone = checkValues($_POST["Phone"]);
			$mobile = "";
		}
		else
		{
			$phone = "";
			$mobile = checkValues($_POST["Phone"]);
		}
	}
	// End of code against ticket #3472: Now Transfer - Phone/Mobile

/**
 * If bank details with beneficiries
 * @Ticket# 3589
 */
//if(CONFIG_BEN_BANK_DETAILS == "1")
if(CONFIG_BEN_BANK_DETAILS == "1" || $_POST["ibanbank"] == "bank"){
	/**
	 * If no bank selected from the bank search
	 * than check if the provided bank id already exists in bank table
	 */
	if(empty($_POST["usedBankId"]) && !empty($_POST["bankId"]))
	{
		$checkBankId = selectFrom("SELECT bankId from ".TBL_BANKS. " WHERE bankId ='".$_POST["bankId"]."' ");
			
		if(!empty($checkBankId["bankId"]))
		{
			insertError(B1);
			redirect($backURL);
		}
	}
}
/* End #3589 */

if ($_POST["benID"] == ""){ // New Beneficiary
	
	 $Querry_Sqls = "INSERT INTO ".TBL_BENEFICIARY." (Title, other_title, firstName, middleName, secMiddleName, lastName, SOF, Address, Address1, Country, City, Zip, Citizenship, State, Phone, Mobile, email, IDissuedate,IDexpirydate,IDType, IDNumber, CPF, customerID, agentID, otherId, otherid_name, created, userType,iban,IbanOrBank) VALUES 
	('".$_POST["Title"]."','".$_POST["other_title"]."', '".checkValues($_POST["firstName"])."', '".checkValues($_POST["middleName"])."', '".checkValues($_POST["secMiddleName"])."', '".checkValues($_POST["lastName"])."', '".checkValues($_POST["S/O"])."', '".checkValues($_POST["Address"])."', '".checkValues($_POST["Address1"])."', '".$_POST["Country"]."', '".checkValues($_POST["City"])."', '".checkValues($_POST["Zip"])."','".$_POST["Citizenship"]."', '".checkValues($_POST["State"])."', '".$phone."', '".$mobile."', '".checkValues($_POST["emailAddress"])."','".$_POST["IDissuedate"]."', '".$idExpiryDate."', '".$_POST["IDType"]."', '".checkValues($_POST["IDNumber"])."', '" . $_POST["CPF"] . "', '" . $_SESSION["customerID"] . "', '" . $_POST["agentID"] . "',  '" . $_POST["Otherid"] . "','" . $_POST["Otherid_name"] . "', '".$tran_date."','".$_POST["userType"]."','".$_POST["iban"]."','".$_POST["ibanbank"]."')";
	if($_POST["ibanbank"] == "bank"){
		 $Querry_Sqls = "INSERT INTO ".TBL_BENEFICIARY." (Title, other_title, firstName, middleName, secMiddleName, lastName, SOF, Address, Address1, Country, City, Zip, Citizenship, State, Phone, Mobile, email, IDissuedate,IDexpirydate,IDType, IDNumber, CPF, customerID, agentID, otherId, otherid_name, created, userType, IbanOrBank, iban) VALUES 
		('".$_POST["Title"]."','".$_POST["other_title"]."', '".checkValues($_POST["firstName"])."', '".checkValues($_POST["middleName"])."', '".checkValues($_POST["secMiddleName"])."', '".checkValues($_POST["lastName"])."', '".checkValues($_POST["S/O"])."', '".checkValues($_POST["Address"])."', '".checkValues($_POST["Address1"])."', '".$_POST["Country"]."', '".checkValues($_POST["City"])."', '".checkValues($_POST["Zip"])."','".$_POST["Citizenship"]."', '".checkValues($_POST["State"])."', '".$phone."', '".$mobile."', '".checkValues($_POST["emailAddress"])."','".$_POST["IDissuedate"]."', '".$idExpiryDate."', '".$_POST["IDType"]."', '".checkValues($_POST["IDNumber"])."', '" . $_POST["CPF"] . "', '" . $_SESSION["customerID"] . "', '" . $_POST["agentID"] . "',  '" . $_POST["Otherid"] . "','" . $_POST["Otherid_name"] . "', '".$tran_date."','".$_POST["userType"]."','".($_REQUEST["IbanOrBank"] == "i"?"iban":"bank")."','".$_POST["iban"]."')";
	}
	//debug($_REQUEST);
  //debug($Querry_Sqls, true);
	insertInto($Querry_Sqls);
	$benID = @mysql_insert_id();

	/**
	 * Storing the IBAN detail to beneficiary table
	 */
//	if(CONFIG_BEN_BANK_DETAILS == "1")
	if(CONFIG_BEN_BANK_DETAILS == "1" || $_POST["ibanbank"] == "iban"){
		if(CONFIG_BEN_BANK_DETAILS == "1"){
			$updateIbanSql = "update ".TBL_BENEFICIARY." set IBAN='".$_POST["iban"]."' where benID=$benID";
		}
		else{
			$updateIbanSql = "update ".TBL_BENEFICIARY." set IBAN='".$_POST["iban"].",IbanOrBank='".$_POST["ibanbank"]."' where benID=$benID";
		}
		update($updateIbanSql);
	}
	
	/**
	 * Storing the Beneficiary bank details to table
	 */
//	if(CONFIG_BEN_BANK_DETAILS == "1")

	if(CONFIG_BEN_BANK_DETAILS == "1" || $_POST["ibanbank"] == "bank"){
		if(!empty($_POST["usedBankId"]))
		{
			$benBankQry = "Insert into ".TBL_BEN_BANK_DETAILS. " (benId, bankId, accountNo, accountType, branchCode, branchAddress, swiftCode)
										values ($benID, ".$_POST["usedBankId"].",'".$_POST["account"]."','".$_POST["accountType"]."','".$_POST["branchName"]."','".$_POST["branchAddress"]."','".$_POST["swiftCode"]."')";
		}
		else
		{
			
			$dbBankId = $_POST["bankId"];
			if(empty($dbBankId))
			{
				/**
				 * Generate a new id, and keep generating new id untill no db entry matched
				 */
				do
				{
					$dbBankId = uniqid();
					$dbBankIdData = selectFrom("SELECT bankId from ".TBL_BANKS. " WHERE bankId ='".$dbBankId."' ");
				}	
				while(!empty($dbBankIdData["bankId"]));
			}

			$addNewBank = "insert into ".TBL_BANKS." ( bankId, name, country)
											values ( '".$dbBankId."','".$_POST["bankName"]."','".$_POST["country1"]."')";
 	
			insertInto($addNewBank);
			$lastBankId = @mysql_insert_id();
				
			$benBankQry = "Insert into ".TBL_BEN_BANK_DETAILS. " (benId, bankId, accountNo, accountType, branchCode, branchAddress, swiftCode)
										values ($benID, '$lastBankId','".$_POST["account"]."','".$_POST["accountType"]."','".$_POST["branchName"]."','".$_POST["branchAddress"]."','".$_POST["swiftCode"]."')";	
		}
		insertInto($benBankQry);
		$lastBenBankID =  @mysql_insert_id();
		if(CONFIG_IBAN_BEN_BANK_DETAILS == "1"){
			if($_POST["iban"]!="")
				update("UPDATE ".TBL_BEN_BANK_DETAILS. " SET IBAN = '".$_POST["iban"]."' WHERE id='".$lastBenBankID."' ");
		}
		//die();
	}
	if(isset($_POST["serviceType"]) && $_POST["serviceType"]!=""){
		$updateServiceTypeSql = "update ".TBL_BENEFICIARY." set serviceType='".$_POST["serviceType"]."' where benID=$benID";
		update($updateServiceTypeSql);
	}
	/* End of code beneficiary bank details */
	if ( !empty( $_REQUEST["IDTypesValuesData"] ) )
	{
		$idTypeValuesData = $_REQUEST["IDTypesValuesData"];
		foreach( $idTypeValuesData as $idTypeValues )
		{
			if ( !empty( $idTypeValues["id_type_id"] )
				 && !empty( $idTypeValues["id_number"] )
				 && !empty( $idTypeValues["issued_by"] )
				 && !empty( $idTypeValues["issue_date"] )
				 && !empty( $idTypeValues["expiry_date"] ) )
			{
				// If id is empty, its an insert request.
				// Insert new reocrd in this case.

				$issueDate = $idTypeValues["issue_date"]["year"] . "-" . $idTypeValues["issue_date"]["month"] . "-" . $idTypeValues["issue_date"]["day"];
				$expiryDate = $idTypeValues["expiry_date"]["year"] . "-" . $idTypeValues["expiry_date"]["month"] . "-" . $idTypeValues["expiry_date"]["day"];

				$insertQuery = "insert into user_id_types
										(
											id_type_id, 
											user_id, 
											user_type, 
											id_number, 
											issued_by, 
											issue_date, 
											expiry_date
										)
									values
										(
											'" . $idTypeValues["id_type_id"] . "', 
											'" . $benID . "', 
											'B', 
											'" . $idTypeValues["id_number"] . "', 
											'" . $idTypeValues["issued_by"] . "', 
											'" . $issueDate . "',
											'" . $expiryDate . "'
										)";
				insertInto( $insertQuery );
/*
 * @Ticket #4794
 */
				$lastIdInsertId = @mysql_insert_id();
				if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
					update("update user_id_types set notes='".mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$lastIdInsertId."'");
				}
			}
		}
	}
	if(isset($_POST["collectionPointID"]) && $_POST["collectionPointID"]!=""){
		update("update ".TBL_BENEFICIARY." set collectionPointID='".$_POST["collectionPointID"]."' where benID=$benID");
	}
	if(isset($_POST["beneficiaryName"]) && $_POST["beneficiaryName"]!=""){
		update("update ".TBL_BENEFICIARY." set beneficiaryName='".$_POST["beneficiaryName"]."' where benID=$benID");
	}
	////To record in History
	$descript ="Beneficiary is added";
	activities($_SESSION["loginHistoryID"],"INSERTION",$benID,TBL_BENEFICIARY,$descript);
	
	unset(
			$_SESSION["ben_bank_id"],
			$_SESSION["Title"],
			$_SESSION["other_title"],
			$_SESSION["firstName"],
			$_SESSION["lastName"],
			$_SESSION["middleName"],
			$_SESSION["secMiddleName"],
			$_SESSION["S/O"],
			$_SESSION["sonOfType"],
			$_SESSION["IDType"],
			$_SESSION["IDNumber"],
			$_SESSION["Otherid"],
			$_SESSION["Otherid_name"],
			$_SESSION["Address"],
			$_SESSION["Address1"],
			$_SESSION["Country"],
			$_SESSION["City"],
			$_SESSION["Zip"],
			$_SESSION["State"],
			$_SESSION["CPF"],
			$_SESSION["Phone"],
			$_SESSION["Mobile"],
			$_SESSION["benEmail"],
			$_SESSION["Citizenship"],
			$_SESSION["IDissuedate"],
			$_SESSION["IDexpirydate"],
			$_SESSION["userType"],
			$_SESSION["CPF"],
			$_SESSION["iban"],
			$_SESSION["IBAN"],
			$_SESSION["ben_bank_id"],
			$_SESSION["bankName1"],
			$_SESSION["account1"],
			$_SESSION["branchCode1"],
			$_SESSION["branchAddress1"],
			$_SESSION["swiftCode1"],
			$_SESSION["country1"],
			$_SESSION["bankId1"],
			$_SESSION["serviceType"],
			$_SESSION["emailAddress"],
			$_SESSION["beneficiaryName"]
		);
		
	insertError(BE9);
} else { // Edit Beneficiary Structure

	// Added by Usman Ghani aginst ticket #3472: Now Transfer - Phone/Mobile
	$phone = checkValues($_POST["Phone"]);
	$mobile = checkValues($_POST["Mobile"]);
	if ( defined("CONFIG_PHONES_AS_DROPDOWN")
		&& CONFIG_PHONES_AS_DROPDOWN == 1 )
	{
		if ( $_POST["phoneType"] == "phone" )
		{
			$phone = checkValues($_POST["Phone"]);
			$mobile = "";
		}
		else
		{
			$phone = "";
			$mobile = checkValues($_POST["Phone"]);
		}
	}
	// End of code against ticket #3472: Now Transfer - Phone/Mobile

	$Querry_Sqls = "update ".TBL_BENEFICIARY." set Title='".$_POST["Title"]."',
other_title='".checkValues($_POST["other_title"])."',
	 firstName='".checkValues($_POST["firstName"])."', 
	 middleName='".checkValues($_POST["middleName"])."', 
	 lastName='".checkValues($_POST["lastName"])."', 
	 secMiddleName = '".checkValues($_POST["secMiddleName"])."',
	 SOF='".checkValues($_POST["S/O"])."', 
	 SOF_Type='".checkValues($_POST["sonOfType"])."', 
	 IDType='".$_POST["IDType"]."',
	 IDissuedate='".$_POST["IDissuedate"]."', 
	 IDexpirydate='".$idExpiryDate."', 
	 IDNumber='".checkValues($_POST["IDNumber"])."',
	 otherId='".checkValues($_POST["Otherid"])."',
	 otherId_name='".checkValues($_POST["Otherid_name"])."',
	 Address='".checkValues($_POST["Address"])."', 
	 Address1='".checkValues($_POST["Address1"])."', 
	 Zip='".$_POST["Zip"]."', 
	 Citizenship='".$_POST["Citizenship"]."', 
	 Country='".$_POST["Country"]."', 
	 City='".$_POST["City"]."', 
	 CPF='".$_POST["CPF"]."',
	 IBAN='".$_POST["iban"]."',  
	 userType='".$_POST["userType"]."', 
	 State='".$_POST["State"]."', 
	 Phone='".$phone."', 
	 Mobile='".$mobile."', 
	 email='".checkValues($_POST["emailAddress"])."',
	 IbanOrBank='".$_POST['ibanbank']."' where benID='".$_POST["benID"]."'";
	if($_POST["ibanbank"] == "bank"){
		$Querry_Sqls = "update ".TBL_BENEFICIARY." set Title='".$_POST["Title"]."',
		other_title='".checkValues($_POST["other_title"])."',
		 firstName='".checkValues($_POST["firstName"])."', 
		 middleName='".checkValues($_POST["middleName"])."', 
		 lastName='".checkValues($_POST["lastName"])."', 
		 secMiddleName = '".checkValues($_POST["secMiddleName"])."',
		 SOF='".checkValues($_POST["S/O"])."', 
		 IDType='".$_POST["IDType"]."',
		 IDissuedate='".$_POST["IDissuedate"]."', 
		 IDexpirydate='".$idExpiryDate."', 
		 IDNumber='".checkValues($_POST["IDNumber"])."',
		 otherId='".checkValues($_POST["Otherid"])."',
		 otherId_name='".checkValues($_POST["Otherid_name"])."',
		 Address='".checkValues($_POST["Address"])."', 
		 Address1='".checkValues($_POST["Address1"])."', 
		 Zip='".$_POST["Zip"]."', 
		 Citizenship='".$_POST["Citizenship"]."', 
		 Country='".$_POST["Country"]."', 
		 City='".$_POST["City"]."', 
		 CPF='".$_POST["CPF"]."',
		 userType='".$_POST["userType"]."', 
		 State='".$_POST["State"]."', 
		 Phone='".$phone."', 
		 Mobile='".$mobile."', 
		 email='".checkValues($_POST["emailAddress"])."',
		 IbanOrBank='".$_POST['ibanbank']."' where benID='".$_POST["benID"]."'";
	}
	update($Querry_Sqls);
	$benID = $_POST["benID"];	
	/**
	 * Updating the beneficiary bank details
	 */

//	if(CONFIG_BEN_BANK_DETAILS == "1")

	if($_POST["ibanbank"] == "iban"){
		/**
		 * Updating the IBAN number to the beneficiary table
		 */
		update("update ".TBL_BENEFICIARY." set IBAN='".$_POST["iban"]."' where benID='".$_POST["benID"]."'");
	}
	if($_POST["serviceType"]!="" && isset($_POST["serviceType"])){
		update("update ".TBL_BENEFICIARY." set serviceType='".$_POST["serviceType"]."' where benID=$benID");
	}
	if(CONFIG_BEN_BANK_DETAILS == "1" || $_POST["ibanbank"] == "bank"){
		if(CONFIG_BEN_BANK_DETAILS == "1"){
			update("update ".TBL_BENEFICIARY." set IBAN='".$_POST["iban"]."' where benID='".$_POST["benID"]."'");
		}
		if(!empty($_POST["usedBankId"]))
		{
			$benBankUpdateQry = "update ".TBL_BEN_BANK_DETAILS. " set 
													bankId = ".$_POST["usedBankId"].",
													accountNo = '".$_POST["account"]."',
													accountType = '".$_POST["accountType"]."',
													branchCode = '".$_POST["branchName"]."',
													branchAddress = '".$_POST["branchAddress"]."',
													swiftCode = '".$_POST["swiftCode"]."'
													where benId = $benID ";
			update($benBankUpdateQry);
			update("update ".TBL_BANKS. " set name = '".$_POST["bankName"]."' ,country = '".$_POST["country1"]."' where id = '".$_POST["usedBankId"]."'");
		}
		elseif(empty($_POST["usedBankId"]) && $_POST["ibanbank"] == "bank"){
			$dbBankId = $_POST["bankId"];
			if(empty($dbBankId))
			{
				/**
				 * Generate a new id, and keep generating new id untill no db entry matched
				 */
				do
				{
					$dbBankId = uniqid();
					$dbBankIdData = selectFrom("SELECT bankId from ".TBL_BANKS. " WHERE bankId ='".$dbBankId."' ");
				}	
				while(!empty($dbBankIdData["bankId"]));
			}
			$addNewBank = "insert into ".TBL_BANKS." ( bankId, name, country) values ( '".$dbBankId."','".$_POST["bankName"]."','".$_POST["country1"]."')";
			insertInto($addNewBank);
			$lastBankId = @mysql_insert_id();
			
		if(!empty($lastBankId)){
			$benBankQry = "Insert into ".TBL_BEN_BANK_DETAILS. " (benId, bankId, accountNo, accountType, branchCode, branchAddress, swiftCode) values ($benID, ".$lastBankId.",'".$_POST["account"]."','".$_POST["accountType"]."','".$_POST["branchName"]."','".$_POST["branchAddress"]."','".$_POST["swiftCode"]."')";
			insertInto($benBankQry);
			}
		}
		else
		{
			$dbBankId = $_POST["bankId"];
			
			if(empty($dbBankId))
			{
				/**
				 * Generate a new id, and keep generating new id untill no db entry matched
				 */
				do
				{
					$dbBankId = uniqid();
					$dbBankIdData = selectFrom("SELECT bankId from ".TBL_BANKS. " WHERE bankId ='".$dbBankId."' ");
				}	
				while(!empty($dbBankIdData["bankId"]));
			}
			
			$addNewBank = "insert into ".TBL_BANKS." ( bankId, name, country)
											values ( '".$dbBankId."','".$_POST["bankName"]."','".$_POST["country1"]."')";
 	
			insertInto($addNewBank);
			$lastBankId = @mysql_insert_id();
				
			$benBankUpdateQry = "update ".TBL_BEN_BANK_DETAILS. " set 
													bankId = ".$lastBankId.",
													accountNo = '".$_POST["account"]."',
													accountType = '".$_POST["accountType"]."',
													branchCode = '".$_POST["branchName"]."',
													branchAddress = '".$_POST["branchAddress"]."',
													swiftCode = '".$_POST["swiftCode"]."'
													where benId = $benID ";			
		update($benBankUpdateQry);
		}
		if(CONFIG_IBAN_BEN_BANK_DETAILS == "1"){
			if($_POST["iban"]!="")
				update("UPDATE ".TBL_BEN_BANK_DETAILS. " SET IBAN = '".$_POST["iban"]."' WHERE benID='".$benID."' ");
		}
	 }	
	/* End of code for updating beneficiary bank details.  */
	if ( !empty( $_REQUEST["IDTypesValuesData"] ) )
	{
		$idTypeValuesData = $_REQUEST["IDTypesValuesData"];
		foreach( $idTypeValuesData as $idTypeValues )
		{
			if ( !empty( $idTypeValues["id_type_id"] )
				 && !empty( $idTypeValues["id_number"] )
				 && !empty( $idTypeValues["issued_by"] )
				 && !empty( $idTypeValues["issue_date"] )
				 && !empty( $idTypeValues["expiry_date"] ) )
			{
				$issueDate = $idTypeValues["issue_date"]["year"] . "-" . $idTypeValues["issue_date"]["month"] . "-" . $idTypeValues["issue_date"]["day"];
				$expiryDate = $idTypeValues["expiry_date"]["year"] . "-" . $idTypeValues["expiry_date"]["month"] . "-" . $idTypeValues["expiry_date"]["day"];

				if ( !empty($idTypeValues["id"]) )
				{
					// If id is not empty, its an update request.
					// Update the existing record in this case.

					$updateQuery = "update user_id_types 
										set 
											id_number='" . $idTypeValues["id_number"] . "', 
											issued_by='" . $idTypeValues["issued_by"] . "', 
											issue_date='" . $issueDate . "', 
											expiry_date='" . $expiryDate . "' 
										where 
											id='" . $idTypeValues["id"] . "'";
					update( $updateQuery );
/*
 * @Ticket #4794
 */
					if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
						update("update user_id_types set notes='".mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$idTypeValues["id"]."'");
						
					}
				}
				else
				{
					// If id is empty, its an insert request.
					// Insert new reocrd in this case.

					

					$insertQuery = "insert into user_id_types
											(
												id_type_id, 
												user_id, 
												user_type, 
												id_number, 
												issued_by, 
												issue_date, 
												expiry_date 
											)
										values
											(
												'" . $idTypeValues["id_type_id"] . "', 
												'" . $_REQUEST["benID"] . "', 
												'B', 
												'" . $idTypeValues["id_number"] . "', 
												'" . $idTypeValues["issued_by"] . "', 
												'" . $issueDate . "', 
												'" . $expiryDate . "'
											)";
					insertInto( $insertQuery );
/*
 * @Ticket #4794
*/
					$lastIdInsertId = @mysql_insert_id();
  
					if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
						update("update user_id_types set notes='". mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$lastIdInsertId."'");
					}
				}
			}
		}
	}
	if(isset($_POST["collectionPointID"]) && $_POST["collectionPointID"]!=""){
		update("update ".TBL_BENEFICIARY." set collectionPointID='".$_POST["collectionPointID"]."' where benID=$benID");
	}
	if(isset($_POST["beneficiaryName"]) && $_POST["beneficiaryName"]!=""){
		update("update ".TBL_BENEFICIARY." set beneficiaryName='".$_POST["beneficiaryName"]."' where benID=$benID");
	}
	if ( !empty( $_SESSION["IDTypesValuesData"] ) )
		unset( $_SESSION["IDTypesValuesData"] ); 
	unset(
			$_SESSION["ben_bank_id"], 
			$_SESSION["Title"],
			$_SESSION["other_title"],
			$_SESSION["firstName"],
			$_SESSION["lastName"],
			$_SESSION["middleName"],
			$_SESSION["secMiddleName"],
			$_SESSION["S/O"],
			$_SESSION["sonOfType"],
			$_SESSION["IDType"],
			$_SESSION["IDNumber"],
			$_SESSION["Otherid"],
			$_SESSION["Otherid_name"],
			$_SESSION["Address"],
			$_SESSION["Address1"],
			$_SESSION["Country"],
			$_SESSION["City"],
			$_SESSION["Zip"],
			$_SESSION["State"],
			$_SESSION["CPF"],
			$_SESSION["Phone"],
			$_SESSION["Mobile"],
			$_SESSION["benEmail"],
			$_SESSION["Citizenship"],
			$_SESSION["IDissuedate"],
			$_SESSION["IDexpirydate"],
			$_SESSION["userType"],
			$_SESSION["CPF"],
			$_SESSION["iban"],
			$_SESSION["IBAN"],
			$_SESSION["ben_bank_id"],
			$_SESSION["bankName1"],
			$_SESSION["account1"],
			$_SESSION["branchCode1"],
			$_SESSION["branchAddress1"],
			$_SESSION["swiftCode1"],
			$_SESSION["country1"],
			$_SESSION["bankId1"],
			$_SESSION["serviceType"],
			$_SESSION["emailAddress"],
			$_SESSION["beneficiaryName"]
		);

	////To record in History
	$descript ="Beneficiary is updated";
	activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["benID"],TBL_BENEFICIARY,$descript);
	
	insertError(BE10);
}
if($_GET["from"] == "popUp")
{
	//$_SESSION["benID"] = $benID;&docWaiver=<?=$_REQUEST["docWaiver"]?>
	
	
	<script language="javascript">
		opener.document.location = "<?=$returnPage?>?msg=Y&benID=<?=$benID?>&success=Y&transID=<?=$_GET['transID']?>";
		window.close();
	</script>
	<?
}
else
{
	$backURL .= "&success=Y";
	redirect($backURL);
}
?>
