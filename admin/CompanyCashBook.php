<?php
	session_start();
	include ("../include/config.php");
	include ("security.php");
	$agentType = getAgentType();
	
	function formatNumber($fltNumber)
	{
		return number_format($fltNumber, 2, ".", "");
	}

	/**
	 * Variables from Query String
	 */
	$cmd 				= $_REQUEST["cmd"];
	$print				= $_REQUEST["print"];
	$openingBalance 	= $_REQUEST["openingBalance"];
	$closingBalance 	= $_REQUEST["closingBalance"];
	$banks				= $_REQUEST["banks"];
	$overShort			= $_REQUEST["overShort"];
	$id 				= $_REQUEST["id"];
	$userId		    	= $_REQUEST["userId"];
	$currentDate		= $_REQUEST["currentDate"];
	$totalTransactions	= $_REQUEST["totalTransactions"];
	$totalExpense		= $_REQUEST["totalExpense"];
	
	/**	maintain report logs 
	 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
	 *	If not exist enter it in order to maintain report logs
	 */
	include_once("maintainReportsLogs.php");
	maintainReportLogs($_SERVER["PHP_SELF"]);
	
	/**
	 * Some pre data preparation.
	 */
	$openingBalance 	= (!empty($openingBalance))?trim($openingBalance):"0";
	$closingBalance 	= (!empty($closingBalance))?trim($closingBalance):"0";
	$banks 				= (!empty($banks))?trim($banks):"0";
	$overShort		 	= (!empty($overShort))?trim($overShort):"0";
	$userId				= (!empty($userId))?$userId:$_SESSION["loggedUserData"]["userID"];
	$currentDate		= (!empty($currentDate))?$currentDate:date("Y-m-d", time());
	$allowUpdate		= ($currentDate == date("Y-m-d", time()))?"Y":"N";
	$totalTransactions  = (!empty($totalTransactions))?$totalTransactions:"0";
	$totalExpense  		= (!empty($totalExpense))?$totalExpense:"0";

	/**
	 * Check for data validity
	 */
	$openingBalance 	= (is_numeric($openingBalance))?$openingBalance:"0";
	$closingBalance 	= (is_numeric($closingBalance))?$closingBalance:"0";
	$banks 				= (is_numeric($banks))?$banks:"0";
	$overShort		 	= (is_numeric($overShort))?$overShort:"0";
	
	if($cmd == "ADD")
	{
		$sql = "INSERT INTO
					cash_book
					(
						userId,
						openingBalance,
						closingBalance,
						banks,
						overShort,
						created,
						updated
					)
				VALUES
					(
						$userId,
						$openingBalance,
						$closingBalance,
						$banks,
						$overShort,
						now(),
						now()
					)
				";
		insertInto($sql);
		$cmd = "UPDATE";
	}
	
	$sql = "SELECT 
				COUNT(recId) AS cnt
			FROM 
				cash_book
			WHERE
				userId = $userId
				AND created like '$currentDate%'
			";
	$result = selectFrom($sql);
	$numRows = $result["cnt"];
	
	if($numRows > 0)
	{
		$latestUpdatesOn = $currentDate;
		$cmd = "UPDATE";
	} else {
		$sql = "SELECT
					DISTINCT DATE_FORMAT(created, '%Y-%m-%d') as created
				FROM
					cash_book
				ORDER BY
					created DESC
				LIMIT
					0,1
				";
		$result = selectFrom($sql);
		$latestUpdatesOn = $result["created"];
	}

	$sql = "SELECT 
				recId,
				openingBalance,
				closingBalance,
				banks,
				overShort
			FROM
				cash_book
			WHERE
				userId = $userId
				AND created like '$latestUpdatesOn%'
			";
	$result = selectFrom($sql);
	$id						= $result["recId"];

	if($numRows > 0)
	{
		$openingBalance 	= $result["openingBalance"];
		$closingBalance		= $result["closingBalance"];
	} else {
		$openingBalance		= $result["closingBalance"];
		$closingBalance		= 0;
	}
	
	$banks				= $result["banks"];
	$overShort			= $result["overShort"];
	$xBalance			= $openingBalance;
	
	if(empty($cmd))
	{
		$cmd = "ADD";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Company Cash Book</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
<style>
.ce{
	color:#0066FF;
	cursor:pointer;
}
</style>
<script src="javascript/jquery.js"></script>
<script type="text/javascript">
function updateBalance(b)
{
	var banks = parseFloat(document.getElementById("banks").value);
	var closingBalance = parseFloat(document.getElementById("closingBalance").value);
	var final = 0;
	
	closingBalance = closingBalance + b;
	final = closingBalance - banks;
	
	document.getElementById("closingBalance").value = final;
	document.getElementById("balanceAfterBank").innerHTML = final;
	document.getElementById("closingFinal").innerHTML = final;
}
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}


function manageDisplay(rec)
{
	if($("#right"+rec).html() == "[+]")
		$("#right"+rec).html("[-]");
	else
		$("#right"+rec).html("[+]");
	
	$(".sub"+rec).toggle();
}

</script>
</head>

<body>

<? 
if($print != "Y" && $agentType =="admin")
{ /*
?>
	<form action="" method="post" name="frmSearch">
	<table width="80%" border="0" align="center" cellpadding="3" cellspacing="0" class="boundingTable">
		<tr>
			<td class="columnTitle" colspan="2">Search</td>
		</tr>
		<tr>
			<td align="right">Agents</td>
			<?
			$agentSql = "SELECT 
							userID,
							username, 
							name
						FROM 
							".TBL_ADMIN_USERS."
						WHERE 
							adminType = 'Agent'
							AND parentID > 0
							AND isCorrespondent != 'ONLY' 
						ORDER BY
							username
						";
			$agentResult = SelectMultiRecords($agentSql);
			
			$qry1 = "select 
							userID,
							username, 
							name, 
							agentCountry, 
							adminType 
					from 
						".TBL_ADMIN_USERS." 
					where 
						adminType IN ('Admin', 'Call', 'COLLECTOR', 'Branch Manager', 'Admin Manager', 'Support', 'SUPI Manager', 'MLRO', 'AM')
						AND agentStatus='Active' 
					order by adminType, name	
						";
			$src = selectMultiRecords($qry1);
			
				
			?>
			<td align="center">
				<select name="userId" id="userId">
					<option value="">- Select User -</option>
						<optgroup label="Agents">
						<? for($i=0; $i<sizeof($agentResult); $i++) { ?>
							<option value="<?=$agentResult[$i]["userID"]?>"><?=$agentResult[$i]["username"]."[".$agentResult[$i]["name"]."]" ?></option>
						<? } ?>
						</optgroup>
					
					<?
						for ($i=0; $i < count($src); $i++)
						{
							if($src[$i]["adminType"] != $src[$i-1]["adminType"])
								echo "<optgroup label='".$src[$i]["adminType"]."'>";
							
							if($_REQUEST["userId"] == $src[$i]["userID"]):
							?>
								<option value="<?=$src[$i]["userID"]; ?>" selected><? echo $src[$i]["name"] . " [" . $src[$i]["username"] . "]" ; ?></option>
							<?
							else:
							?>
								<option value="<?=$src[$i]["userID"]; ?>"><? echo $src[$i]["name"] . " [" . $src[$i]["username"] . "]"; ?></option>
							<?
							endif;

							if($src[$i]["adminType"] != $src[$i+1]["adminType"])
								echo "</optgroup>";

						}
					?>
				</select>
				<script language="JavaScript">
					SelectOption(document.forms[0].userId, "<?=$userId; ?>");
			   </script>
			</td>
		</tr>
		<tr>
			<td width="50%" align="right" class="reportToolbar" colspan="2"><input type="submit" name="Submit" value="Submit" /></td>
		</tr>
	</table>
	</form>
<? */
}
else
{
?>
	<input type="hidden" name="userId" value="<?=$_SESSION["loggedUserData"]["userID"]?>" />
	<input type="hidden" name="Submit" value="Submit" />
<?
}	

	$strBankCashSql = "SELECT 
				t.refNumberIM, 
				t.totalAmount,
				t.transID,
				a.username
			FROM 
				transactions t,
				admin a
			WHERE 
				    t.transDate like '$currentDate%'
				AND t.moneyPaid = 'By Bank Transfer'
				AND t.custAgentId = a.userId
			order by 
				username
			";
				

	$arrBankCash = SelectMultiRecords($strBankCashSql);

	for($i=0; $i<sizeof($arrBankCash); $i++)
		$xTotalTransactions		+= $arrBankCash[$i]["totalAmount"];


	$strBankCashCancelSql = "SELECT 
				t.refNumberIM, 
				t.totalAmount,
				a.username
			FROM 
				transactions t,
				admin a
			WHERE 
				    t.transDate like '$currentDate%'
				AND t.moneyPaid = 'By Bank Transfer'
				AND t.transStatus LIKE 'Cancelled%'
				AND t.custAgentId = a.userId
			order by 
				username
			";
				
	$arrBankCashCancel = SelectMultiRecords($strBankCashCancelSql);
	
	for($i=0; $i<sizeof($arrBankCashCancel); $i++)
		$xTotalTransactionsCancel		+= $arrBankCashCancel[$i]["totalAmount"];




	$strCashSql = "SELECT 
				t.refNumberIM, 
				t.totalAmount,
				t.transID,
				a.username
			FROM 
				transactions t,
				admin a
			WHERE 
				    t.transDate like '$currentDate%'
				AND t.moneyPaid = 'By Cash'
				AND t.custAgentId = a.userId
			order by 
				username
			";
				
	$arrCash = SelectMultiRecords($strCashSql);

	for($i=0; $i<sizeof($arrCash); $i++)
		$xTotalTransactionsCash		+= $arrCash[$i]["totalAmount"];



	$strCashCancelSql = "SELECT 
				t.refNumberIM, 
				t.totalAmount,
				a.username
			FROM 
				transactions t,
				admin a
			WHERE 
				    t.transDate like '$currentDate%'
				AND t.moneyPaid = 'By Cash'
				AND t.transStatus LIKE 'Cancelled%'
				AND t.custAgentId = a.userId
			order by 
				username
			";
				
	$arrCashCancel = SelectMultiRecords($strCashCancelSql);
	
	for($i=0; $i<sizeof($arrCashCancel); $i++)
		$xTotalTransactionsCashCancel		+= $arrCashCancel[$i]["totalAmount"];

	/* Transactions By Cheque */
	$strChequeSql = "SELECT 
				t.refNumberIM, 
				t.totalAmount,
				t.transID,
				a.username
			FROM 
				transactions t,
				admin a
			WHERE 
				    t.transDate like '$currentDate%'
				AND t.moneyPaid = 'By Cheque'
				AND t.custAgentId = a.userId
			order by 
				username
			";
				
	$arrCheque = SelectMultiRecords($strChequeSql);

	for($i=0; $i<sizeof($arrCheque); $i++)
		$xTotalTransactionsCheque	+= $arrCheque[$i]["totalAmount"];



	$strChequeCancelSql = "SELECT 
				t.refNumberIM, 
				t.totalAmount,
				a.username
			FROM 
				transactions t,
				admin a
			WHERE 
				    t.transDate like '$currentDate%'
				AND t.moneyPaid = 'By Cheque'
				AND t.transStatus LIKE 'Cancelled%'
				AND t.custAgentId = a.userId
			order by 
				username
			";
				
	$arrChequeCancel = SelectMultiRecords($strChequeCancelSql);
	
	for($i=0; $i<sizeof($arrChequeCancel); $i++)
		$xTotalTransactionsChequeCancel		+= $arrChequeCancel[$i]["totalAmount"];



	$strAllCurrencies = "select 
							c.currencyName as cu,
							x.localAmount as la,
							x.totalAmount as ta,
							x.buy_sell as bs,
							x.rate as rt,
							a.username
						  from
						  	currencies as c,
							curr_exchange_account as x,
							admin a
						  where 
							x.buysellCurrency = c.cid 
							AND created_at like '$currentDate%'	
							AND x.createdBy = a.userId
						order by 
							username
							";
							
	$arrAllCurrency = SelectMultiRecords($strAllCurrencies);

	$xTotalTranferCr = 0;
	$xTotalTranferDr = 0;

	$xTotalCurrencyExchangeCr = 0;
	$xTotalCurrencyExchangeDr = 0;

	foreach($arrAllCurrency as $rows)
	{
		if($rows["bs"] == "S")
			$xTotalCurrencyExchangeCr += $rows["la"];
		else
			$xTotalCurrencyExchangeDr += $rows["la"];
	}

	/* Cheque Orders Created */
	$strChequeOrdersNewSql = "select 
								c.cheque_ref, 
								c.cheque_amount,
								c.order_id,
								a.username
							from
								cheque_order c,
								admin a
							where
							    c.created_on like '$currentDate%' 
							AND  c.created_by LIKE CONCAT( a.userId, '|%' )
							";
	$arrChequeOrdersNew = SelectMultiRecords($strChequeOrdersNewSql);
	
	$xTotalChequOrdersNew = 0;

	foreach($arrChequeOrdersNew as $ordersNew)
		$xTotalChequOrdersNew += $ordersNew["cheque_amount"];

	/* Cheque Orders Paid */
	$strChequeOrdersSql = "select 
								c.cheque_ref, 
								c.paid_amount,
								c.order_id,
								a.username
							from
								cheque_order c,
								admin a
							where
							    c.created_on like '$currentDate%'
							AND c.status IN ('ID') 
							AND  c.created_by LIKE CONCAT( a.userId, '|%' )
							";
	$arrChequeOrders = SelectMultiRecords($strChequeOrdersSql);
	
	$xTotalChequOrders = 0;

	foreach($arrChequeOrders as $orders)
		$xTotalChequOrders += $orders["paid_amount"];

	$strInterUserSql = "select 
								c.agentID, 
								c.amount, 
								c.type,
								c.currency,
								c.note,
								c.chequeNumber,
								a.username
							from
								agent_account as c,
								admin a
							where
								c.TransID = '0'
							AND c.note LIKE 'INTER%'
							AND c.dated = '$currentDate'
							AND (c.chequeStatus IS NULL OR c.chequeStatus LIKE 'CLEARED')
							AND c.agentID = a.userId
							order by 
								username
							";
	
	//debug($strInterUserSql);
	$arrInterUser = SelectMultiRecords($strInterUserSql);
	
	//debug($arrInterUser);

	$xTotalTranferCr = 0;
	$xTotalTranferDr = 0;

	foreach($arrInterUser as $rows)
	{
		if($rows["type"] == "DEPOSIT")
			$xTotalTranferDr += $rows["amount"];
		else
			$xTotalTranferCr += $rows["amount"];
	}


	/* Daily Expense */
	$strExpenseSql = "select 
						e.amount, 
						e.narration,
						a.username
					from
						expenses as e,
						admin as a
					where
					    e.created like '$currentDate%' 
					AND e.userId = a.userId
					";
	$arrExpense = SelectMultiRecords($strExpenseSql);
	
	$xTotalExpense = 0;

	foreach($arrExpense as $exp)
		$xTotalExpense += $exp["amount"];

	/* Paypoint Transactions */
	$xTotalPayTrans = 0;
if(CONFIG_ADD_PAYPOINT_TRANS=="1"){
	$strpayTransactionsSql = "select pt.refNumber, pt.transAmount,a.username
							from
								paypointTransactions pt,admin a
							where
								pt.addedBy = a.username 
							AND transDate like '$currentDate%' ";
	$arrpayTransactions = SelectMultiRecords($strpayTransactionsSql);
	foreach($arrpayTransactions as $payTrans)
		$xTotalPayTrans += $payTrans["transAmount"];
}

	$xTotalDr = 0;
	$xTotalCr = 0;

	$xTotalDr = $xTotalTransactionsCancel + $xTotalTransactionsCashCancel + $xTotalTranferDr + $xTotalCurrencyExchangeDr + $xTotalChequOrders + $xTotalTransactionsChequeCancel; 
	$xTotalCr = $xTotalTransactions + $xTotalTransactionsCash + $xTotalCurrencyExchangeCr + $xTotalTranferCr + $xTotalExpense + $xTotalPayTrans + $xTotalChequOrdersNew + $xTotalTransactionsCheque;
	$closingBalance = $openingBalance + $xTotalDr - $xTotalCr;

?>
<br />
<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td align="center" class="reportHeader"><?=strtoupper(SYSTEM)?> (Company Cash Book)<br />
					<span class="reportSubHeader"><?=date("l, d F, Y", strtotime($currentDate))?><br />
					<br />
					</span></td></tr>
			</table>
			<br />
			<form action="" method="post">
			<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
				<tr>
					<td width="3%" class="columnTitle">Sr.#</td>
					<td width="25%" class="columnTitle">Particulars</td>
					<td width="32%" align="right" class="columnTitle">&nbsp;</td>
					<td width="12%" align="right" class="columnTitle">&nbsp;</td>
					<td width="7%" align="right" class="columnTitle">Dr. &pound; </td>
					<td width="7%" align="right" class="columnTitle">Cr. &pound;</td>
					<td width="14%" align="right" class="columnTitle">Balance &pound;</td>
				</tr>
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr class="columnTitle">
					<td>&nbsp;</td>
					<td class="heading">Opening Balance</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">
						<?=$openingBalance?>					</td>
				</tr>
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				
				<tr class="columnTitle">
					<td align="right" id="right1" class="ce" onclick="manageDisplay(1);">[-]</td>
					<td class="heading">Bank Transactions</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">0.00</td>
					<td align="right"><?=formatNumber($xTotalTransactions)?></td>
					<td align="right">&nbsp;</td>
				</tr>
				<?				
					for($i=0; $i<sizeof($arrBankCash); $i++)
					{
						
						if($arrBankCash[$i-1]["username"] != $arrBankCash[$i]["username"])
						{
				?>
						<tr class="sub1">
							<td>&nbsp;</td>
							<td class="subheading" colspan="6"><?=$arrBankCash[$i]["username"]?></td>
						</tr>
				
				<?
						
						}
						
						$xTransactionId			= $arrBankCash[$i]["refNumberIM"];
						$xTransactionAmount		= $arrBankCash[$i]["totalAmount"];
				?>
				<tr class="sub1">
					<td>&nbsp;</td>
					<td class="entries"><?=$xTransactionId?></td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">0.00</td>
					<td align="right"><?=formatNumber($xTransactionAmount)?></td>
					<td align="right">&nbsp;</td>
				</tr>
				<? } ?>			
				
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr class="columnTitle">
					<td align="right" id="right2" class="ce" onclick="manageDisplay(2);">[-]</td>
					<td class="heading">Cancelled Bank Transactions</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right"><?=formatNumber($xTotalTransactionsCancel)?></td>
					<td align="right">0.00</td>
					<td align="right">&nbsp;</td>
				</tr>
				<?			
					for($i=0; $i<sizeof($arrBankCashCancel); $i++)
					{
					
						if($arrBankCashCancel[$i-1]["username"] != $arrBankCashCancel[$i]["username"])
						{
				?>
						<tr class="sub2">
							<td>&nbsp;</td>
							<td class="subheading" colspan="6"><?=$arrBankCashCancel[$i]["username"]?></td>
						</tr>
				
				<?
						
						}
					
						$xTransactionId			= $arrBankCashCancel[$i]["refNumberIM"];
						$xTransactionAmount		= $arrBankCashCancel[$i]["totalAmount"];
				?>
				<tr class="sub2">
					<td>&nbsp;</td>
					<td class="entries"><?=$xTransactionId?></td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right"><?=formatNumber($xTransactionAmount)?></td>
					<td align="right">0.00</td>
					<td align="right">&nbsp;</td>
				</tr>
				<? } ?>			
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr class="columnTitle">
					<td align="right" id="right3" class="ce" onclick="manageDisplay(3);">[-]</td>
					<td class="heading">Cash Transactions</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">0.00</td>
					<td align="right"><?=formatNumber($xTotalTransactionsCash)?></td>
					<td align="right">&nbsp;</td>
				</tr>
				<?				
					for($i=0; $i<sizeof($arrCash); $i++)
					{

						if($arrCash[$i-1]["username"] != $arrCash[$i]["username"])
						{
				?>
						<tr class="sub3">
							<td>&nbsp;</td>
							<td class="subheading" colspan="6"><?=$arrCash[$i]["username"]?></td>
						</tr>
				
				<?
						
						}

						$xTransactionId			= $arrCash[$i]["refNumberIM"];
						$xTransactionAmount		= $arrCash[$i]["totalAmount"];
				?>
				<tr class="sub3">
					<td>&nbsp;</td>
					<td class="entries">
<a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $arrCash[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')">
					<?=$xTransactionId?></a></td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">0.00</td>
					<td align="right"><?=formatNumber($xTransactionAmount)?></td>
					<td align="right">&nbsp;</td>
				</tr>
				<? } ?>			
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr class="columnTitle">
					<td align="right" id="right4" class="ce" onclick="manageDisplay(4);">[-]</td>
					<td class="heading">Cancelled Cash Transactions</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right"><?=formatNumber($xTotalTransactionsCashCancel)?></td>
					<td align="right">0.00</td>
					<td align="right">&nbsp;</td>
				</tr>
				<?				
					for($i=0; $i<sizeof($arrCashCancel); $i++)
					{

						if($arrCashCancel[$i-1]["username"] != $arrCashCancel[$i]["username"])
						{
				?>
						<tr class="sub4">
							<td>&nbsp;</td>
							<td class="subheading" colspan="6"><?=$arrCashCancel[$i]["username"]?></td>
						</tr>
				
				<?
						
						}
					
						$xTransactionId			= $arrCashCancel[$i]["refNumberIM"];
						$xTransactionAmount		= $arrCashCancel[$i]["totalAmount"];
				?>
				<tr class="sub4">
					<td>&nbsp;</td>
					<td class="entries"><?=$xTransactionId?></td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right"><?=formatNumber($xTransactionAmount)?></td>
					<td align="right">0.00</td>
					<td align="right">&nbsp;</td>
				</tr>
				<? } ?>	
				
				
				
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr class="columnTitle">
					<td align="right" id="right5" class="ce" onclick="manageDisplay(5);">[-]</td>
					<td class="heading">Cheque Transactions</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">0.00</td>
					<td align="right"><?=formatNumber($xTotalTransactionsCheque)?></td>
					<td align="right">&nbsp;</td>
				</tr>
				<?				
					for($i=0; $i<sizeof($arrCheque); $i++)
					{

						if($arrCheque[$i-1]["username"] != $arrCheque[$i]["username"])
						{
				?>
						<tr class="sub5">
							<td>&nbsp;</td>
							<td class="subheading" colspan="6"><?=$arrCheque[$i]["username"]?></td>
						</tr>
				
				<?
						
						}

						$xTransactionId			= $arrCheque[$i]["refNumberIM"];
						$xTransactionAmount		= $arrCheque[$i]["totalAmount"];
				?>
				<tr class="sub5">
					<td>&nbsp;</td>
					<td class="entries">
<a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $arrCheque[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')">
					<?=$xTransactionId?></a></td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">0.00</td>
					<td align="right"><?=formatNumber($xTransactionAmount)?></td>
					<td align="right">&nbsp;</td>
				</tr>
				<? } ?>			
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr class="columnTitle">
					<td align="right" id="right6" class="ce" onclick="manageDisplay(6);">[-]</td>
					<td class="heading">Cancelled Cheque Transactions</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right"><?=formatNumber($xTotalTransactionsChequeCancel)?></td>
					<td align="right">0.00</td>
					<td align="right">&nbsp;</td>
				</tr>
				<?				
					for($i=0; $i<sizeof($arrChequeCancel); $i++)
					{

						if($arrChequeCancel[$i-1]["username"] != $arrChequeCancel[$i]["username"])
						{
				?>
						<tr class="sub6">
							<td>&nbsp;</td>
							<td class="subheading" colspan="6"><?=$arrChequeCancel[$i]["username"]?></td>
						</tr>
				
				<?
						
						}
					
						$xTransactionId			= $arrChequeCancel[$i]["refNumberIM"];
						$xTransactionAmount		= $arrChequeCancel[$i]["totalAmount"];
				?>
				<tr class="sub6">
					<td>&nbsp;</td>
					<td class="entries"><?=$xTransactionId?></td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right"><?=formatNumber($xTransactionAmount)?></td>
					<td align="right">0.00</td>
					<td align="right">&nbsp;</td>
				</tr>
				<? } ?>	
				
				
				
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
			<? if(CONFIG_ADD_PAYPOINT_TRANS=="1"){?>
				<tr class="columnTitle">
					<td align="right" id="right7" class="ce" onclick="manageDisplay(7);">[-]</td>
					<td class="heading">Paypoint Transactions</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">0.00</td>
					<td align="right"><?=formatNumber($xTotalPayTrans)?></td>
					<td align="right">&nbsp;</td>
				</tr>
				<?				
					for($i=0; $i<sizeof($arrpayTransactions); $i++)
					{
						if($arrpayTransactions[$i-1]["username"] != $arrpayTransactions[$i]["username"])
						{
				?>
						<tr class="sub7">
							<td>&nbsp;</td>
							<td class="subheading" colspan="6"><?=$arrpayTransactions[$i]["username"]?></td>
						</tr>
				
				<?
						
						}
						$xPayTransId			= $arrpayTransactions[$i]["refNumber"];
						$xPayTranAmount		= $arrpayTransactions[$i]["transAmount"];
				?>
				<tr class="sub7">
					<td>&nbsp;</td>
					<td class="entries"><?=$xPayTransId?></td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">0.00</td>
					<td align="right"><?=formatNumber($xPayTranAmount)?></td>
					<td align="right">&nbsp;</td>
				</tr>
				<? } ?>			
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
			<? }?>
				<tr class="columnTitle">
					<td align="right" id="right8" class="ce" onclick="manageDisplay(8);">[-]</td>
					<td class="heading">Total Exchange Currency</td>
					<td class="heading">&nbsp;</td>
					<td class="heading">Total</td>
					<td align="right"><?=formatNumber($xTotalCurrencyExchangeDr)?></td>
					<td align="right"><?=formatNumber($xTotalCurrencyExchangeCr)?></td>
					<td align="right">&nbsp;</td>
				</tr>
				
				<?
					$val = $arrAllCurrency;
					for($i=0; $i < count($val) ; $i++)
					{

						if($val[$i-1]["username"] != $val[$i]["username"])
						{
				?>
						<tr class="sub8">
							<td>&nbsp;</td>
							<td class="subheading" colspan="6"><?=$val[$i]["username"]?></td>
						</tr>
				
				<?
						
						}


						$xTransactionId = ($val[$i]["bs"] == "B"? "Buy":"Sell")." ".$val[$i]["cu"]."@".$val[$i]["rt"];
						$xTransactionAmountCr = 0;
						$xTransactionAmountDr = 0;

						if($val[$i]["bs"] == "S")
							$xTransactionAmountCr = $val[$i]["la"];
						else
							$xTransactionAmountDr = $val[$i]["la"];
				?>
				
				<tr class="sub8">
					<td>&nbsp;</td>
					<td class="entries"><?=$xTransactionId?></td>
					<td align="left">&nbsp;</td>
					<td align="left"><?=formatNumber($val[$i]["ta"])." ".$val[$i]["cu"]?></td>
					<td align="right"><?=formatNumber($xTransactionAmountDr)?></td>
					<td align="right"><?=formatNumber($xTransactionAmountCr)?></td>
					<td align="right">&nbsp;</td>
				</tr>
				
				<? } ?>
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr class="columnTitle">
					<td align="right" id="right9" class="ce" onclick="manageDisplay(9);">[-]</td>
					<td class="heading">Total Cheque Order Created</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">0.00</td>
					<td align="right"><?=formatNumber($xTotalChequOrdersNew)?></td>
					<td align="right">&nbsp;</td>
				</tr>
				
				<?
					$ordersNew = $arrChequeOrdersNew;
					for($i=0; $i < count($ordersNew) ; $i++)
					{
						if($ordersNew[$i-1]["username"] != $ordersNew[$i]["username"])
						{
				?>
						<tr class="sub9">
							<td>&nbsp;</td>
							<td class="subheading" colspan="6"><?=$ordersNew[$i]["username"]?></td>
						</tr>
				
				<?
						}

						$xChequeAmount = $ordersNew[$i]["cheque_amount"];
						$xTransactionId = $ordersNew[$i]["cheque_no"];
						$xChequeId = $ordersNew[$i]["order_id"];

				?>
				<tr class="sub9">
					<td>&nbsp;</td>
					<td class="entries">
<a href="#" onclick='window.open("/admin/chequeOrderFullDetail.php?eodi="+<?=$xChequeId?>,"CustomerReceipt","location=0,scrollbars=1,width=800,height=900");'>
					<?=$xTransactionId?></a>					</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">0.00</td>
					<td align="right"><?=formatNumber($xChequeAmount)?></td>
					<td align="right">&nbsp;</td>
				</tr>
				
				<? } ?>
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr class="columnTitle">
					<td align="right" id="right10" class="ce" onclick="manageDisplay(10);">[-]</td>
					<td class="heading">Total Cheque Order Paid</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right"><?=formatNumber($xTotalChequOrders)?></td>
					<td align="right">0.00</td>
					<td align="right">&nbsp;</td>
				</tr>
				
				<?
					$orders = $arrChequeOrders;
					for($i=0; $i < count($orders) ; $i++)
					{
						if($orders[$i-1]["username"] != $orders[$i]["username"])
						{
				?>
						<tr class="sub10">
							<td>&nbsp;</td>
							<td class="subheading" colspan="6"><?=$orders[$i]["username"]?></td>
						</tr>
				
				<?
						}

						$xChequeAmount = $orders[$i]["paid_amount"];
						$xTransactionId = $orders[$i]["cheque_no"];
						$xChequeId = $orders[$i]["order_id"];

				?>
				<tr class="sub10">
					<td>&nbsp;</td>
					<td class="entries">
<a href="#" onclick='window.open("/admin/chequeOrderFullDetail.php?eodi="+<?=$xChequeId?>,"CustomerReceipt","location=0,scrollbars=1,width=800,height=700");'>
					<?=$xTransactionId?></a>					</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right"><?=formatNumber($xChequeAmount)?></td>
					<td align="right">0.00</td>
					<td align="right">&nbsp;</td>
				</tr>
				
				<? } ?>
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr class="columnTitle">
					<td align="right" id="right11" class="ce" onclick="manageDisplay(11);">[-]</td>
					<td class="heading">Inter-User-Transfer</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right"><?=formatNumber($xTotalTranferDr)?></td>
					<td align="right"><?=formatNumber($xTotalTranferCr)?></td>
					<td align="right">&nbsp;</td>
				</tr>
				<?
					$rows = $arrInterUser;
					for($i=0; $i<count($rows); $i++)
					{

						if($rows[$i-1]["username"] != $rows[$i]["username"])
						{
				?>
						<tr class="sub11">
							<td>&nbsp;</td>
							<td class="subheading" colspan="6"><?=$rows[$i]["username"]?></td>
						</tr>
				
				<?
						}
						$typeDepostiWithdraw = $rows[$i]["type"];
						/**************
							#5328 - Minas Center
									Inter-User Transfer labels changed.
									if it is WITHDRAW then shown it as DEPOSIT (Cr.)
									and vice versa.
									(Originally DEPOSIT should be Dr. and WITHDRAW should be Cr.)
									by Aslam Shahid
						**************/
						if(CONFIG_SENDER_DEPOSIT_RECEIVER_WITHDRAW=="1"){
							if($rows[$i]["type"] == "DEPOSIT")
							{
								$typeDepostiWithdraw = "WITHDRAW";
							}
							else{
								$typeDepostiWithdraw = "DEPOSIT";
							}
						}
						/**************
							#5328 - Minas Center
									Showing labels as From OR To user for 
									DEPOSIT OR WITHDRAW.
									by Aslam Shahid
						**************/
						if(CONFIG_SHOW_DESCRIPTION_INTER_USER=="1"){
							$otherUserQ  = "SELECT ad.name userName 
												FROM agent_account aa,admin ad 
												WHERE ad.userID=aa.agentID 
													AND chequeNumber='".$rows[$i]["chequeNumber"] ."' 
													AND agentID!='".$rows[$i]["agentID"]."'";
							$otherUserRS = selectFrom($otherUserQ);
							if($typeDepostiWithdraw == "DEPOSIT")
							{
								$descriptionLogInterUser = "From user ".strtoupper($otherUserRS["userName"]);
							}
							else{
								$descriptionLogInterUser = "To user ".strtoupper($otherUserRS["userName"]);
							}
						}


						$xTransactionId = "INTER USER ".$typeDepostiWithdraw;
						
						$transactionDr = 0;
						$transactionCr = 0;
						
						if($rows[$i]["type"] == "DEPOSIT")
						{
							$transactionDr = $rows[$i]["amount"];
							$transactionCr = 0;						
						}
						else
						{
						
							$transactionDr = 0;
							$transactionCr = $rows[$i]["amount"];
						}
				?>
					<tr class="sub11">
						<td>&nbsp;</td>
						<td class="entries"><?=$xTransactionId?></td>
						<td align="left"><?=$descriptionLogInterUser?></td>
						<td align="right">&nbsp;</td>
						<td align="right"><?=formatNumber($transactionDr)?></td>
						<td align="right"><?=formatNumber($transactionCr)?></td>
						<td align="right">&nbsp;</td>
					</tr>
				<? } ?>
				
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr class="columnTitle">
					<td align="right" id="right12" class="ce" onclick="manageDisplay(12);">[-]</td>
					<td class="heading">Daily Expenses</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">0.00</td>
					<td align="right"><?=formatNumber($xTotalExpense)?></td>
					<td align="right">&nbsp;</td>
				</tr>
	
				<?

					for($i=0; $i< count($arrExpense); $i++)
					{
					
						if($arrExpense[$i-1]["username"] != $arrExpense[$i]["username"])
						{
				?>
						<tr class="sub12">
							<td>&nbsp;</td>
							<td class="subheading" colspan="6"><?=$arrExpense[$i]["username"]?></td>
						</tr>
				
				<?
						}
					
					
						$xTransactionId = $arrExpense[$i]["narration"];
						$expense = $arrExpense[$i]["amount"];
				?>
					<tr class="sub12">
						<td>&nbsp;</td>
						<td class="entries"><?=$xTransactionId?></td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">0.00</td>
						<td align="right"><?=formatNumber($expense)?></td>
						<td align="right">&nbsp;</td>
					</tr>
				<? } ?>
	
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr class="columnTitle">
					<td align="right">&nbsp;</td>
					<td class="heading">Closing Balance of the Day</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">
						<?=formatNumber($closingBalance)?>					</td>
				</tr>
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
			</table>
			<br />
			<? if($print != "Y") { ?>
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td width="50%" class="reportToolbar">
								<input name="btnPrint" type="button" id="btnPrint" value=" Print " onclick="javascript: location.href='CompanyCashBook.php?id=<?=$id?>&print=Y&userId=<?=$userId?>&currentDate=<?=$currentDate?>';" />
							</td>
							<td width="50%" align="right" class="reportToolbar">
							    <input name="id" type="hidden" id="id" value="<?=$id?>" />
								<input name="cmd" type="hidden" id="cmd" value="<?=$cmd?>" />
								<input name="userId" type="hidden" id="userId" value="<?=$userId?>" />
								<? /* if($allowUpdate == "Y") { ?>
									<input type="submit" name="Submit" value="Submit" />
									<input type="reset" name="Submit2" value="Reset" /></td>
								<? } */ ?>
						</tr>
					</table>
			<? } ?>
			</form>
		</td>
	</tr>
</table>
<?php
if($print == "Y")
{
	maintainReportLogs($_SERVER["PHP_SELF"],'P');
?>
	<script type="text/javascript">
        print();
    </script>
<?php
}?>
</body>
</html>
