<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
// define page navigation parameters
if ($offset == "")
	$offset = 0;
$limit=50;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = "erID";

$colSpans = 12;

if(CONFIG_AGENT_RATE_MARGIN)
{
	$colSpans = $colSpans + 1;
}
if(CONFIG_24HR_BANKING == "1")
{
	$colSpans = $colSpans + 2;
}

/**
 * Adding more filter to search the data
 * @Ticket# 3426
 *
 * Add the functionality that filter search can available to Super Agents also
 * @Ticket# 3425
 */
$filters = "";
if(CONFIG_EXCHANGE_RATE_FILTERS == 1 && $_GET["Submit"] && ($agentType == "admin" || $agentType == "SUPA" ))
{
	/**
	 * Date filter
	 */
	$fromDate = $_GET["fYear"] . "-" . $_GET["fMonth"] . "-" . $_GET["fDay"];
	$toDate = $_GET["tYear"] . "-" . $_GET["tMonth"] . "-" . $_GET["tDay"];
	$filters .= " and (dated >= '$fromDate' and dated <= '$toDate')";

	
	if(!empty($_GET["basedOn"]))
	{
		$filters .= " and rateFor = '$_GET[basedOn]' ";
	}
	if(!empty($_GET["country"]))
	{
		$filters .= " and countryOrigin = '$_GET[country]' ";
	}
	if(!empty($_GET["destCountry"]))
	{
		$filters .= " and country = '$_GET[destCountry]' ";
	}
}


$SQL_Qry = "select * from ".TBL_EXCHANGE_RATES." where isActive !='N' ";

/**
 * Adding the filter results
 */
if(CONFIG_EXCHANGE_RATE_FILTERS == 1)
	$SQL_Qry .= $filters;
/* End of query filteration */
 
if(CONFIG__CPE__ONLY_AGENT_EXCHAGE_RATE_VIEW == 1 && $agentType == "SUPA") 
{
	$strSubAgentSql = "select userID from admin where parentID=".$_SESSION["loggedUserData"]["userID"];
	$arrSubAgentData = SelectMultiRecords($strSubAgentSql);
	
	$strSubAgentIds = "";
	foreach($arrSubAgentData as $saiK => $saiV)
	{
		$strSubAgentIds .= $saiV["userID"].",";
	}
	$strSubAgentIds .= $_SESSION["loggedUserData"]["userID"];
	$SQL_Qry .= " and rateValue IN ($strSubAgentIds)";
}
$rates = SelectMultiRecords($SQL_Qry);
$allCount = count($rates);
if ($sortBy !="")
	$SQL_Qry = $SQL_Qry. " order by $sortBy DESC ";
$SQL_Qry = $SQL_Qry. " LIMIT $offset , $limit";


$rates = SelectMultiRecords($SQL_Qry);



$settlementRateQuery = "select e.erID from exchangerate as e,relatedExchangeRate as r where e.erID = r.erID";
$settlementRateArray = selectMultiRecords($settlementRateQuery);

for ($i=0; $i < count($settlementRateArray); $i++){

$settlementRate[$i] = $settlementRateArray[$i]["erID"];

}
?>
<html>
<head>
	<title>Categories List</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
	function checkForm(theForm) {
		var a = false;
		var none = true;
		var j = 2; 
		for(i =0; i < <?=count($rates); ?>; i++){
			if(theForm.elements[j].checked == true){
				a = confirm("Are you sure you want to delete selected exchange rate(s) from the database?");
				none = false;
				break;
			}
			j++;
		}
		if (none) {
			a = false;
			alert("No exchange rate(s) is selected for deletion.")
		}
		return a;
	}
	function checkAllFn(theForm, field) {
		if (field == 1) {
			if (theForm.del.value == 0)
				theForm.del.value = 1;
			else
				theForm.del.value = 0;
			if (theForm.del.value == 1){
				j=2;
				for(i=0; i < <?=count($rates);?>; i++) {
					theForm.elements[j].checked = true;
					j++;
				}
			} else {
				j = 2;
				for(i=0; i< <?=count($rates);?>; i++) {
					theForm.elements[j].checked = false;
					j++;
				}
			}
		} else {
			if (theForm.app.value == 0)
				theForm.app.value = 1;
			else
				theForm.app.value = 0;
			if (theForm.app.value == 1){
				j = 3;
				for(i=0; i<<?=count($rates);?>; i++) {
					theForm.elements[j].checked = true;
					j = j+2;
				}
			} else {
				j = 3;
				for(i=0; i<<?=count($rates);?>; i++) {
					theForm.elements[j].checked = false;
					j = j+2;
				}
			}
		}
	}
	
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}
	
	// end of javascript -->
	</script>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">Manage Exchange Rates</td>
  </tr>
  <tr>
  		<td>
  			<!---------------------->
  			
  			
					<?
						/**
						 * Displaying the search filters for super admin only
						 * @Ticket# 3426
						 *
						 * Add the functionality that filter search can available to Super Agents also
 						 * @Ticket# 3425
						 */ 
						if(CONFIG_EXCHANGE_RATE_FILTERS == 1 && ($agentType == "admin" || $agentType == "SUPA" ))
						{
					?>
						<fieldset>
		  			<table border="0" align="center" cellpadding="5" cellspacing="1">
              <tr>
              	<td align="center">
              				<b>SEARCH FILTER</b>		
              	</td>
            	</tr>
              <tr>
							  <form name="search" method="get" action="rate-list.php">
	                <td width="100%" colspan="4" bgcolor="#DFE6EA" align="center">
										<b>Originating Country </b>
										<?
											$countiresRs = "select distinct(countryName), countryId from ".TBL_COUNTRY." where  countryType like '%origin%' order by countryName";
										?>
									   	<select name="country" style="font-family: verdana; font-size: 11px;">
									   		<option value="">Select Country</option>
										   <?
												$countires = selectMultiRecords($countiresRs);
												for ($i=0; $i < count($countires); $i++){
											 ?>
												<OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
											 <?
													}
											 ?>
											</SELECT>
								             <script language="JavaScript">
												   				SelectOption(document.search.country, "<?=$_GET["country"]?>");
											    	  </script>
									    	  		&nbsp;&nbsp;
															<b>Destination Country</b> 
															<?
																$countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." where  countryType like '%destination%' ";
															?>
													   	<select name="destCountry" style="font-family: verdana; font-size: 11px;">
													   		<option value="">Select Country</option>
													   <?
															  $countiresQuery .=" order by countryName ";
																$countires = selectMultiRecords($countiresQuery);
																for ($i=0; $i < count($countires); $i++){
														 ?>
																<OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
														 <?
																	}
														 ?>
																</SELECT>
								               <script language="JavaScript">
											     				SelectOption(document.search.destCountry, "<?=$_GET["destCountry"]?>");
										      	  </script>

															<br /><br />
																	
															<b>From </b>
															<? 
															$month = date("m");
															
															$day = date("d");
															$year = date("Y");
															?>
													
											        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
										          <?
														  	for ($Day=1;$Day<32;$Day++)
																{
																	if ($Day<10)
																	$Day="0".$Day;
																	echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
																}
														  ?>
											        </select>
											        <script language="JavaScript">
											         	SelectOption(document.search.fDay, "<?=(!empty($_GET["fDay"])?$_GET["fDay"]:"")?>");
											        </script>
															<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
											          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
											          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
											          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
											          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
											          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
											          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
											          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
											          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
											          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
											          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
											          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
											          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
											        </SELECT>
											        <script language="JavaScript">
											         	SelectOption(document.search.fMonth, "<?=(!empty($_GET["fMonth"])?$_GET["fMonth"]:"")?>");
											        </script>
											        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
											          <?
															  	$cYear=date("Y");
															  	for ($Year=2004;$Year<=$cYear;$Year++)
																	{
																		echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
																	}
															  ?>
											        </SELECT> 
															<script language="JavaScript">
											         	SelectOption(document.search.fYear, "<?=(!empty($_GET["fYear"])?$_GET["fYear"]:"")?>");
											        </script>
											        <b>To	</b>
											        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
											          <?
															  	for ($Day=1;$Day<32;$Day++)
																	{
																		if ($Day<10)
																		$Day="0".$Day;
																		echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
																	}
															  ?>
											        </select>
															<script language="JavaScript">
											         	SelectOption(document.search.tDay, "<?=(!empty($_GET["tDay"])?$_GET["tDay"]:"")?>");
											        </script>
															<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
											
											          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
											          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
											          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
											          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
											          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
											          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
											          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
											          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
											          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
											          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
											          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
											          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
											        </SELECT>
															<script language="JavaScript">
											         	SelectOption(document.search.tMonth, "<?=(!empty($_GET["tMonth"])?$_GET["tMonth"]:"")?>");
											        </script>
											        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
										          <?
														  	$cYear=date("Y");
														  	for ($Year=2004;$Year<=$cYear;$Year++)
																{
																	echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
																}
														  ?>
											        </SELECT>
															<script language="JavaScript">
											        SelectOption(document.search.tYear, "<?=(!empty($_GET["tYear"])?$_GET["tYear"]:"")?>");
											        </script>
											        <br /><br />
															&nbsp;&nbsp;
														 <b> Based On </b>
													  <select name="basedOn" style="font-family: verdana; font-size: 11px;">
																  <option value="">All</option>
												  				<option value="generic" <? if($_GET["basedOn"] == "generic") echo "selected"; ?>>Generic</option>
																	<option value="agent"  <? if($_GET["basedOn"] == "agent") echo "selected"; ?>>Agent</option>
																	<option value="distributor" <? if($_GET["basedOn"] == "distributor") echo "selected"; ?>>Distributor</option>
														</select>      
														<br /><br />
									          <input type="submit" name="Submit" value="Process">
													</td>
											</form>
									    </tr>
									</table>
								</fieldset>
							<?
								/**
								 * Closing of the Search filter interface
								 */
								}
							?>			
          <br>
  			<!---------------------->
  	</td>
	</tr>
  <form action="delete-rates.php" method="post" onSubmit="return checkForm(this);">
  <input type="hidden" name="del" value="0">
  <input type="hidden" name="app" value="0">
  <tr>
    <td align="center">
		<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
          <?
			if ($allCount > 0){
		?>
          <tr> 
            <td colspan="<?=$colSpans?>" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($rates) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($rates));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <td>&nbsp;</td>
            			<td>&nbsp;</td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
          <tr bgcolor="#DFE6EA"> 
          	<? if( CONFIG_REMOVE_DELETE_ON_MANAGE_RATE !="1"){ ?>
            <td width="50"><a href="#" onClick="checkAllFn(document.forms[0], 1);"><font color="#005b90"><strong>Delete</strong></font></a></td>
            <? } ?>
            <td width="109"><a href="<?php print $PHP_SELF . "?sortBy=country";?>"><font color="#005b90"><strong>Originating Country</strong></font></a></td>
            <td width="109"><a href="<?php print $PHP_SELF . "?sortBy=currencyOrigin";?>"><font color="#005b90"><strong>Originating Currency</strong></font></a></td>
            <td width="109"><a href="<?php print $PHP_SELF . "?sortBy=country";?>"><font color="#005b90"><strong>Destination Country</strong></font></a></td>
            <td width="109"><a href="<?php print $PHP_SELF . "?sortBy=currency";?>"><font color="#005b90"><strong>Destination Currency</strong></font></a></td>
            <td width="109"><a href="<?php print $PHP_SELF . "?sortBy=country";?>"><font color="#005b90"><strong>Date</strong></font></a></td>
            <td width="109"><a href="<?php print $PHP_SELF . "?sortBy=country";?>"><font color="#005b90"><strong>Updation Date</strong></font></a></td>
            <td width="125"><a href="<?php print $PHP_SELF . "?sortBy=primaryExchange";?>"><font color="#005b90"><b><? if(CONFIG_ENABLE_RATE == "1"){echo(CONFIG_PIMARY_RATE);}else{echo("Primary Exchange");} ?></b></font></a></td>
            <td width="109"><a href="<?php print $PHP_SELF . "?sortBy=marginPercentage";?>"><font color="#005b90"><b>Margin</b></font></a></td>
            <td width="93"><a href="<?php print $PHP_SELF . "?sortBy=primaryExchange";?>"><font color="#005b90"><b><? if(CONFIG_ENABLE_RATE == "1"){echo(CONFIG_NET_RATE);}else{echo("Net Exchange");} ?></b></font></a></td>
            <td><font color="#005b90"><b>Based On</b></font></td>
            <td><font color="#005b90"><b>Value</b></font></td>
            <?
		        if(CONFIG_AGENT_RATE_MARGIN == '1')
		        {
		        ?>
            	<td><font color="#005b90"><b>Agent Margin</b></font></td>
            	<td><font color="#005b90"><b>Agent Value</b></font></td>

		        <?
		      	}
		        ?>
		        <?
		        if(CONFIG_24HR_BANKING == "1")
		        {
		        ?>
            	<td><font color="#005b90"><b>24 hr Banking Margin Type</b></font></td>
            	<td><font color="#005b90"><b>24 hr Banking Margin Value</b></font></td>
            	<td><font color="#005b90"><b>24 hr Banking Exchange Rate</b></font></td>

		        <?
		      	}
		        ?>
		        
          </tr>
          <?
			for ($i=0; $i < count($rates); $i++){
				$sharedExch = selectFrom("select * from ".TBL_SHARED_EXCHANGE." where localXchangeId = '".$rates[$i]["erID"]."'"); 
				
		?>
          <tr valign="top" bgcolor="#eeeeee" title="Rate By: <? echo $rates[$i]["sProvider"]?>">
          <?	if($sharedExch["remoteServerId"] > 0 || $sharedExch["shareXchangeId"] == '')
           {
           ?> 
           <? if( CONFIG_REMOVE_DELETE_ON_MANAGE_RATE !="1"){ ?>
	            <td align="center" ><input type="checkbox" name="erID[<?=$i;?>]" value="<?=$rates[$i]["erID"]; ?>"></td>
	            <? } ?>
	            <? if(count($settlementRate) > 0 && in_array($rates[$i]['erID'],$settlementRate)){ ?>
	            
	            <td><font color="#005b90"><? echo stripslashes($rates[$i]["countryOrigin"]); ?></font></td>
	            <? }else{ ?>
	            
	            <td><a href="update-rate.php?erID=<?=$rates[$i]["erID"];?>&rateFor=<?=$rates[$i]["rateFor"];?>&newOffset=<?=$offset;?>"><font color="#005b90"><b><?=stripslashes($rates[$i]["countryOrigin"]); ?></b></font></a></td>
	            
	            <?}?>
	            <td><?=stripslashes($rates[$i]["currencyOrigin"]); ?></td>

 								<? if(count($settlementRate) > 0 && in_array($rates[$i]['erID'],$settlementRate)){ ?>
	            
	            <td><font color="#005b90"><? echo stripslashes($rates[$i]["country"]); ?></font></td>
	            <? }else{ ?>

	            <td><a href="update-rate.php?erID=<?=$rates[$i]["erID"];?>&rateFor=<?=$rates[$i]["rateFor"];?>&newOffset=<?=$offset;?>"><font color="#005b90"><b><?=stripslashes($rates[$i]["country"]); ?></b></font></a></td>
	          <? } ?>
	          <?
	        }else{
	        	?>
	        		<td align="center" >&nbsp; </td>
	            <td><font color="#005b90"><b><?=stripslashes($rates[$i]["countryOrigin"]); ?></b></font></td>
	            <td><?=stripslashes($rates[$i]["currencyOrigin"]); ?></td>
	            <td><font color="#005b90"><b><?=stripslashes($rates[$i]["country"]); ?></b></font></td>
	        	<?
	        	}
	            ?>
            <td><?=stripslashes($rates[$i]["currency"]); ?></td>
            <!-- td><font color="#005b90"><b><?=stripslashes($rates[$i]["country"]); ?></b></font></td -->
            <td><?=dateFormat(stripslashes($rates[$i]["dated"]),"2"); ?></td>
            <td><?=dateFormat(stripslashes($rates[$i]["updationDate"]),"2"); ?></td>
            
			<? if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1" && $rates[$i]["isDenomination"] == "Y") { ?>
				<td colspan="3" align="center"><a href="update-rate.php?erID=<?=$rates[$i]["erID"];?>&rateFor=<?=$rates[$i]["rateFor"];?>&newOffset=<?=$offset;?>" style="color:#0099CC"><b>Denomination Based Exchange Rate</b></a></td>	
			<? } else {?>
				<td><?=stripslashes($rates[$i]["primaryExchange"]); ?></td>
				<td><? echo(stripslashes($rates[$i]["marginPercentage"])); if($rates[$i]["marginType"] != 'fixed'){echo(" %");} ?> </td>
				<td><? if($rates[$i]["marginType"] == 'fixed')
					{
						 $calculatedRate = $rates[$i]["primaryExchange"] - $rates[$i]["marginPercentage"];
					}else{
						 $calculatedRate = round($rates[$i]["primaryExchange"] - ($rates[$i]["primaryExchange"] * $rates[$i]["marginPercentage"])/100, 4);
						}
					
					echo($calculatedRate); ?></td>
			<? } ?>
			
			
			<td><?=strtoupper(stripslashes($rates[$i]["rateFor"])); ?></td>
            
            <td><? 
            		if(strtolower($rates[$i]["rateFor"]) == "distributor" || strtolower($rates[$i]["rateFor"]) == "agent" || strtolower($rates[$i]["rateFor"]) == "intermediate")
            		{
            			$agentName = selectFrom("select userID,username, name from ".TBL_ADMIN_USERS." where userID = '".$rates[$i]["rateValue"]."'");
            			echo($agentName["name"]." [".$agentName["username"]."]");
            			}elseif(strtolower($rates[$i]["rateFor"]) == "customer" && CONFIG_CUSTOMER_CATEGORY == '1'){
						$custCat = selectFrom("select name from ".TBL_CUSTOMER_CATEGORY." where id = '".$rates[$i]["rateValue"]."'");
						echo $custCat["name"];
						}else{
            			 echo(stripslashes($rates[$i]["rateValue"]));
            			 } 
            			?>
            </td>
            <?
		        if(CONFIG_AGENT_RATE_MARGIN == '1')
		        {
		        ?>
	            <td><? echo(stripslashes($rates[$i]["agentMarginValue"])); if($rates[$i]["agentMarginType"] != 'fixed'){echo(" %");} ?></td>
	            
	            
	            <td><? if($rates[$i]["agentMarginType"] == 'fixed')
            	{
            		 $agentCalculatedRate = ($rates[$i]["primaryExchange"] - $rates[$i]["marginPercentage"])- $rates[$i]["agentMarginValue"];
            	}else{
            		 $agentCalculatedRate2 = round($rates[$i]["primaryExchange"] - ($rates[$i]["primaryExchange"] * $rates[$i]["marginPercentage"])/100, 4);
            		 $agentCalculatedRate = ($agentCalculatedRate2 - (($agentCalculatedRate2 * $rates[$i]["agentMarginValue"])/100));
            		 //echo("-".(($agentCalculatedRate2 * $rates[$i]["agentMarginValue"])/100)."-");
            		}
            	
            	echo($agentCalculatedRate); ?></td>
       
            </td>
	          <?
          	}
          	?>
          	<?
          	if(CONFIG_24HR_BANKING == "1"){
          	?>  
          		<td><?=stripslashes($rates[$i]["_24hrMarginType"]); ?></td>
            	<td><?=stripslashes($rates[$i]["_24hrMarginValue"]); ?></td>
            	<?
            	$exRate24hr = 0.0;
            	if($rates[$i]["rateFor"] == "bank24hr"){
	            	if($rates[$i]["_24hrMarginType"] == 'lower'){
									$exRate24hr = $rates[$i]["primaryExchange"] - $rates[$i]["_24hrMarginValue"];
								}elseif($rates[$i]["_24hrMarginType"] == 'upper'){
									$exRate24hr = $rates[$i]["primaryExchange"] + $rates[$i]["_24hrMarginValue"];
								}
								if($rates[$i]["marginType"] == 'fixed')
								{
									$exRate24hr = ($exRate24hr - $rates[$i]["marginPercentage"]);
								}else{
									$exRate24hr = ($exRate24hr - ($exRate24hr * $rates[$i]["marginPercentage"]) / 100);
								}
							}
							?>
							<td><?=$exRate24hr?></td>
          	<?
          	}
          	?>
          </tr>
          <?
			}
		?>
          <tr> 
            <td colspan="<?=$colSpans?>" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($rates) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($rates));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                   <td>&nbsp;</td>
            			<td>&nbsp;</td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
          <? if( CONFIG_REMOVE_DELETE_ON_MANAGE_RATE !="1"){ ?>
          <tr> 
            <td colspan="<?=$colSpans?>" align="center"> <input type="submit" value="Delete Rates"> 
            </td>
          </tr>
          <? } ?>
          <?
			} else {
		?>
          <tr> 
            <td colspan="5" align="center"> No Exchnage Rates found in the database. 
            </td>
          </tr>
          <?
			}
		?>
        </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>