<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

 $agentType = getAgentType();

if($agentType == "TELLER"){
	$userID = $_SESSION["loggedUserData"]["cp_id"];
	
}
else{
	$userID = $_SESSION["loggedUserData"]["userID"];
	$username  = $_SESSION["loggedUserData"]["username"];
}

//$userID = $_SESSION["loggedUserData"]["userID"];

// define page navigation parameters
if ($offset == "")
	$offset = 0;
$limit=20;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];

$loginName = "";
$cp_country = "";
$tellerStatus = "";
if ($_GET["cp_country"] != ""){
	$cp_country = $_GET["cp_country"];
	$where .= " and c.cp_country='".$_GET["cp_country"]."'";
	$qryString .= "&cp_country=" . $_GET["cp_country"];
}
if ($_GET["tellerStatus"] != ""){
	$tellerStatus = $_GET["tellerStatus"];
	$where .= " and c.cp_active='".$_GET["tellerStatus"]."'";
	$qryString .= "&cp_active=" . $_GET["tellerStatus"];
}
if ($_GET["loginName"] != ""){
	$loginName = $_GET["loginName"];
	$where .= " and t.loginName='".$_GET["loginName"]."'";
	$qryString .= "&loginName=" . $_GET["loginName"];
}


	$SQL_Qry = "select * from ".TBL_COLLECTION." as c, ".TBL_TELLER." as t where	c.cp_id = t.collection_point and t.loginName !='' and t.name !='' ".$where;	
if($agentType !="admin"  && $agentType != "Admin Manager" )
{
	$SQL_Qry .= " and c.cp_ida_id like '$userID' ";
	}
	
$agents = SelectMultiRecords($SQL_Qry);
$allCount = count($agents);
if ($sortBy !="")
	$SQL_Qry = $SQL_Qry. " order by $sortBy ASC ";
$SQL_Qry = $SQL_Qry. " LIMIT $offset , $limit";

$agents = SelectMultiRecords($SQL_Qry);

//$userDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where username='$username'");

?>
<html>
<head>
	<title>Manage Tellers</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
	/*function checkForm(theForm) {
		var a = false;
		var none = true;
		var j = 2; 
		// < (theForm.elements.length - 11)
		for(i =0; i < <?=count($agents); ?>; i++){
			if(theForm.elements[j].checked == true){
				a = confirm("Are you sure you want to delete the checked contacts from the database?");
				none = false;
				break;
			}
			j++;
		}
		if (none) {
			a = false;
			alert("No contact is checked for deletion.")
		}
		return a;
	}*/
	function checkAllFn(theForm, field) {
		if (field == 1) {
			if (theForm.del.value == 0)
				theForm.del.value = 1;
			else
				theForm.del.value = 0;
			if (theForm.del.value == 1){
				j=2;
				for(i=0; i < <?=count($agents);?>; i++) {
					theForm.elements[j].checked = true;
					j++;
				}
			} else {
				j = 2;
				for(i=0; i< <?=count($agents);?>; i++) {
					theForm.elements[j].checked = false;
					j++;
				}
			}
		} else {
			if (theForm.app.value == 0)
				theForm.app.value = 1;
			else
				theForm.app.value = 0;
			if (theForm.app.value == 1){
				j = 3;
				for(i=0; i<<?=count($agents);?>; i++) {
					theForm.elements[j].checked = true;
					j = j+2;
				}
			} else {
				j = 3;
				for(i=0; i<<?=count($agents);?>; i++) {
					theForm.elements[j].checked = false;
					j = j+2;
				}
			}
		}
	}
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
	// end of javascript -->
	</script>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">Manage Tellers </td>
  </tr>
  <tr>
    <td align="center">
		<table width="671" border="0" cellspacing="2" cellpadding="1" align="center">
        <form action="teller_list.php" method="get">
		  <tr bgcolor="#EEEEEE"> 
            <td colspan="5"> 
              <table width="100%" cellpadding="2" cellspacing="0" border="1" bordercolor="#006699">
				<tr>
					<td width="9%" align="right"><b>Country:</b></td>
					<td width="20%">
					<input type="hidden" value="<? echo $_GET["type"]?>" name="type">
					<input type="hidden" value="<? echo $_GET["ida"]?>" name="ida">
					<SELECT name="cp_country" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- All -</OPTION>
				<?
					$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
					for ($i=0; $i < count($countires); $i++){
				?>
				<OPTION value="<?=$countires[$i]["country"]; ?>"><?=$countires[$i]["country"]; ?></OPTION>
				<?
					}
				?>
				</SELECT><script language="JavaScript">
         	SelectOption(document.forms[0].cp_country, "<?=$cp_country; ?>");
                                </script></td>
					<td width="9%" align="right"><b>Status:</b></td>
					<td width="18%"><select name="tellerStatus">
						<option value="">- All -</option>
						<option value="Active">Active</option>
						<option value="Disabled">Disabled</option>
						<option value="New">New</option>
						<option value="Suspended">Suspended</option>
						</select><script language="JavaScript">
         	SelectOption(document.forms[0].tellerStatus, "<?=$_GET[tellerStatus]; ?>");
                                </script></td>
					
                  <td width="15%" align="right"><b>Login Name:</b></td>
                  <td width="23%"><input type="text" name="loginName" value="<?=$loginName; ?>"></td>
					<td width="6%"><input type="submit" value="Go"></td>
				</tr>
				</table></td>
		</tr></form>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
  <form action="delete-contacts.php" method="post" onSubmit="return checkForm(this);">
  <tr align="center">
			<td colspan="5" class="tab-r">
			<?
			if($_GET["msg"] != "")
			{
				echo($_GET["msg"]);
				}
			?>	
			</td>
	</tr>
  <input type="hidden" name="del" value="0">
  <input type="hidden" name="app" value="0">
          <?
			if ($allCount > 0){
		?>
          <tr> 
            <td colspan="5" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($agents) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($agents));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
          <tr bgcolor="#DFE6EA"> 
		  
            <!--td width="47" height="20"><a href="#" onClick="checkAllFn(document.forms[1], 1);"><font color="#005b90"><strong>Disable</strong></font></a></td-->
            <td width="143"><a href="<?php print $PHP_SELF . "?sortBy=name".$qryString;?>"><font color="#005b90"><strong>Contact Person</strong></font></a></td>
            <td width="100"><a href="<?php print $PHP_SELF . "?sortBy=t.loginName".$qryString;?>"><font color="#005b90"><strong>Agent Number</strong></font></a></td>
            <td width="201"><a href="<?php print $PHP_SELF . "?sortBy=cp_corresspondent_name".$qryString;?>"><font color="#005b90"><strong>Company</strong></font></a></td>
			<td width="164"><a href="<?php print $PHP_SELF . "?sortBy=name".$qryString;?>"><font color="#005b90"><strong>Director</strong></font></a></td>
          </tr>
          <?
			for ($i=0; $i<count($agents); $i++){
						?>
          <tr valign="top" bgcolor="#eeeeee"> 
            <!--td align="center" height="20"> 
              <input type="checkbox" name="userID[<?=$i;?>]" value="<?=$agents[$i]["cp_id"]; ?>"></td-->
            <td>
			<font color="#005b90"><b><? echo $agents[$i]["name"] ?></b></font>
			
			</td>
            <td> 
              <?=$agents[$i]["loginName"]; ?>
            </td>
            <td> 
              <?=stripslashes($agents[$i]["cp_corresspondent_name"]); ?>
            </td>
            <td> 
              <?=stripslashes($agents[$i]["name"]); ?>
            </td>
          </tr>
		  <tr bgcolor="#eeeeee">
		  	<td colspan="5" align="right" height="20">
			
			<? 
				if ($agents[$i]["is_active"] != "Disabled")
				{ ?>
					<a href="javascript:;" onClick=" window.open('disable_teller.php?userID=<?=$agents[$i]["tellerID"]; ?>','disableTeller', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=200,width=460')"><font color="#005b90"><b>Disable</b></font></a> | 
					<? 
				} 
				if ($agents[$i]["is_active"] != "Active")
				{ ?>
					<a href="javascript:;" onClick=" window.open('activate_teller.php?userID=<?=$agents[$i]["tellerID"]; ?>','activateTeller', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=200,width=460')"><font color="#005b90"><b>Activate</b></font></a> | 
					<? 
				}
				if ($agents[$i]["is_active"] != "Suspended")
				{ ?>
					<a href="javascript:;" onClick=" window.open('suspend_teller.php?userID=<?=$agents[$i]["tellerID"]; ?>','suspendTeller', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=200,width=460')"><font color="#005b90"><b>Suspend</b></font></a> | 
					<? 
				} 
				$status = "Y";
			?>
			<a href="javascript:;" onClick=" window.open('view_teller.php?userID=<?=$agents[$i]["tellerID"]; ?>','tellerDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=500,width=460')"><font color="#005b90"><b>View Details</b></font></a> |
			<a href="update_teller.php?tellerID=<?=$agents[$i][tellerID];?>&cp_country=<?=$cp_country;?>&tellerStatus=<?=$tellerStatus;?>&loginName=<?=$loginName;?>">
			<font color="#005b90"><b>Update</b></font></a>
			 | <a href="javascript:;" onClick=" window.open('ch_pass_teller.php?userID=<?=$agents[$i]["tellerID"]; ?>','changePassword', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=220,width=460')"><font color="#005b90"><b>Change Password </b></font></a></b>&nbsp;</td>
		  </tr>
          <?
			}
		?>
          <tr> 
            <td colspan="5" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($agents) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($agents));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
		  
          <!--tr> 
            <td colspan="5" align="center"><? if($status == "Y") { ?> <input type="submit" value="Delete Agents"><? } ?>
            </td>
          </tr-->
          <?
			} else {
		?>
          <tr> 
            <td colspan="5" align="center"> No Teller found in the database. 
            </td>
          </tr>
          <?
			}
		?>
		<input type="hidden" name="type" value="<?=$_GET["type"];?>" >
		<input type="hidden" name="ida" value="<? echo $_GET["ida"]?>" >
</form>
        </table>
	</td>
  </tr>
</table>
</body>
</html>