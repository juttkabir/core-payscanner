<?
session_start();
include ("../include/config.php");
include ("security.php");

if ($offset == "")
	$offset = 0;
	
$limit = 50;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;

if ($_POST["agentID"] != "") {
	$agentID = $_POST["agentID"];
} else if ($_GET["agentID"] != "") {
	$agentID = $_GET["agentID"];
} else {
	$agentID = "";	
}

$query    = "SELECT * FROM ".TBL_RECEIPT_RANGE." WHERE 1";
$qryCnt   = "SELECT COUNT(*) FROM ".TBL_RECEIPT_RANGE." WHERE 1";

if($agentID != "all"){
	
	$query .=  " and `agentID` = '$agentID'";
	$queryCnt .= " and `agentID` = '$agentID'";
	
	}else{
			$query .=  " and `agentID` != ''";
	    $queryCnt .= " and `agentID` != ''";
		}
$query   .= " LIMIT $offset , $limit";

$allCount = countRecords($qryCnt);
$ranges   = selectMultiRecords($query);

?>
<html>
<head>
	<title>View <? if(CONFIG_DIST_REF_NUMBER == '1'){ echo CONFIG_DIST_REF_NAME;}else{ ?>Agent's Receipt Book<? }?> Ranges</title>
	
	
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript">
<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
-->
</script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
	
.style2 {	
	color: #6699CC;
	font-weight: bold;
}

</style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font class="topbar_tex">View <? if(CONFIG_DIST_REF_NUMBER == '1'){ echo CONFIG_DIST_REF_NAME;}else{ ?>Agent's Receipt Book<? }?> Ranges
    	</font></strong></td>
  </tr>
  <tr>
    <td align="center">
		<table width="671" border="0" cellspacing="2" cellpadding="1" align="center">
        <form action="view-receipt-ranges.php" name="frmAgentRanges" method="post">
        	
		  <tr bgcolor="#EEEEEE"> 
            <td colspan="5"> 
              <table width="100%" cellpadding="2" cellspacing="0" border="1" bordercolor="#006699">
				<tr>
					<td width="25%" align="right">Select <? if(CONFIG_DIST_REF_NUMBER == '1'){ ?>Distributor<?}else{ ?>Agent<? }?></td>
          <?
          	if(CONFIG_DIST_REF_NUMBER == '1')
          	{
          	 $select = "Select Distributor";
          	}else{
        			$select = "Select Agent";
        		}
          	
          	?>
          <td width="50%">
        	  <select name="agentID" style="font-family:verdana; font-size: 11px" onChange="document.frmAgentRanges.submit();">
            <option value="">- <? echo $select?>-</option>
             <option value="all">All</option>
						<?
				    		if(CONFIG_DIST_REF_NUMBER == '1')
          			{
          				$agentQuery  = " select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType = 'Agent' and isCorrespondent != 'N' ";
          			}else{
									$agentQuery  = " select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType = 'Agent' and agentType='Supper' and isCorrespondent != 'ONLY' ";
								}
								$agentQuery .= " order by agentCompany ";
								$agents = selectMultiRecords($agentQuery);
								for ($i=0; $i < count($agents); $i++) {
						?>
				      	   <option value="<?=$agents[$i]["userID"]; ?>" <? echo ($agents[$i]["userID"] == $agentID ? "selected" : "") ?>><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
				    <?	}  ?>
          </select>
           <script language="JavaScript">
         				SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID"]; ?>");
              </script>
          	</td>
          <td width="25%"><a href="add-receipt-range.php?flag=Y" class="style2">Add <? if(CONFIG_DIST_REF_NUMBER == '1'){ echo CONFIG_DIST_REF_NAME;}else{ ?>Receipt Book<? }?> Range</a>
        </td>
				</tr>
				</form>
				</table></td>
		</tr>
		
		
		 <? if ($_GET["msg"] != ""){ ?>
          <tr bgcolor="#ededed"> 
            <td colspan="6"> 
              <table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr> 
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><font color="#FF0000"> 
                    <? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; $_SESSION['error'] = ""; ?>
                    </font></td>
                </tr>
              </table>
            </td>
          </tr>
          <?
        	}
          ?>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
          <?
			if ($allCount > 0){
				
		?>
		
				<form action="disableDistributorRef.php" name="frmClaveRanges" method="post">
          <input type="hidden" name="agentID" value="<?=$agentID?>">
		        						
          <tr> 
            <td colspan="6" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($ranges) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($ranges));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&agentID=$agentID"?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&agentID=$agentID"?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&agentID=$agentID"?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&agentID=$agentID"?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
          
          <tr bgcolor="#DFE6EA"> 
          	<?
          	if(CONFIG_UPDATE_DISTRIBUTOR_REFERENCE == '1')
						{
          	?>
          	
          			<td width="146" align="center">&nbsp;</td>
          	<?		
          	}
          	if(CONFIG_DIST_REF_NUMBER == '1')
          	{
          	?>
          		
          			<td width="143"><font color="#005b90"><strong>Prefix</strong></font></td>
        		<?
        		}
        		?>
        		
		        		
		        		<td width="143"><font color="#005b90"><strong>Range From</strong></font></td>
		            <td width="150"><font color="#005b90"><strong>Range To</strong></font></td>
		            <td width="201"><font color="#005b90"><strong>Used</strong></font></td>
		       
								<td width="164"><font color="#005b90"><strong>Date</strong></font></td>
								<td width="143"><font color="#005b90"><strong><? if(CONFIG_DIST_REF_NUMBER == '1'){ ?>Distributor<?}else{ ?>Agent<? }?></strong></font></td>
							
						
						<?
							if(CONFIG_UPDATE_DISTRIBUTOR_REFERENCE == '1')
							{
							?>
									<td width="164"><font color="#005b90"><strong>Status</strong></font></td>
									<td width="164">&nbsp;</td>	
							<?
							}
						?>
          </tr>
          <?
			for ($i = 0; $i < count($ranges); $i++){
		?>
          <tr valign="top" bgcolor="#eeeeee"> 
          	<?
            	if(CONFIG_UPDATE_DISTRIBUTOR_REFERENCE == '1')
							{
							?>
							<td>
							<?
									if($ranges[$i]["status"] == "Active")
          				{
          			?>
          		 			<input type="checkbox" name="rangeID[<?=$i?>]" value="<?=$ranges[$i]['id']?>">
          		 	<?
          				}
          			?>
          		</td>
							<?
							}
						
          	if(CONFIG_DIST_REF_NUMBER == '1')
          	{
          	?>
          			<td><? echo $ranges[$i]["prefix"] ?></td>
          	<?
          	}
          	?>
          	<?
          		
          				$agentQuery  = " select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where userID ='". $ranges[$i]['agentID']."' ";
          			
							
								$agents = selectFrom($agentQuery);
								?>
          	 	
		            <td><? echo $ranges[$i]["rangeFrom"] ?></td>
		            <td><? echo $ranges[$i]["rangeTo"] ?></td>
		            <td><? echo $ranges[$i]["used"] ?></td>
		        
		            <td><? echo $ranges[$i]["created"] ?></td>
		            <td><? echo $agents["agentCompany"]." [". $agents["username"]."]" ?></td>
		          
            <?
            	if(CONFIG_UPDATE_DISTRIBUTOR_REFERENCE == '1')
							{
							?>
							<td><? echo $ranges[$i]["status"] ?></td>
							<td>
								<? 
          		if($ranges[$i]["status"] == "Active")
          		{
          			?>
								<a href="add-receipt-range.php?flag=Y&update=Y&refID=<?=$ranges[$i]["id"];?>"><font color="#005b90"><b>Update</b></font></a>
							<?
								}else{
            	?>
            		<a href="disableDistributorRef.php?flag=Y&update=Y&clID=<?=$ranges[$i]["id"];?>&agentID=<?=$ranges[$i]["agentID"]?>"><font color="#005b90"><b>Activate</b></font></a>
            	<?
            	}
            	?>
								
								</td>
							<?
							}
            ?>
          </tr>
       <?	
       }
       ?>
       			<?
       				if(CONFIG_UPDATE_DISTRIBUTOR_REFERENCE == '1')
							{
							?>
       			<tr> 
		            <td colspan="9" align="center"> 
		            	<input name="Disable" type="submit" value="Disable Range(s)"> 
		            
		            </td>
		        </tr>
		        	<?
		      		}
		        ?>
        </form>
       <? 
      } else if ($agentID != "") {
	      	
	      	if(CONFIG_DIST_REF_NUMBER == "1"){
						$value = "Distributor";
					}else{
						$value = "Agent";
					}
      	?>
          <tr bgcolor="#ffffff"> 
            <td width="143" align="center">No Range is defined for the selected <? echo $value?></td>
          </tr>
      	<?
      } else {
      ?>
          <tr bgcolor="#ffffff"> 
            <td width="143" align="center">Please first select <?echo $value?> from the list</td>
          </tr>
      <?	
      }
      ?>
        </table>
	</td>
  </tr>
</table>
</body>
</html>
