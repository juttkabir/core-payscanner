<?php
session_start();
// Including files
include ("../include/config.php");
include ("security.php");
///////////////////////History is maintained via method named 'activities'

$systemCode = SYSTEM_CODE;
$company2 = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$changedBy = $_SESSION["loggedUserData"]["userID"];

$agentCountry = $_POST["benCountry"];
$customerCountry = $_POST["customerCountry"];
if(CONFIG_ONLINE_AGENT == '1')
{
	$custAgentQuery = selectFrom("select userID from admin where agentCountry = '".$customerCountry."' and isOnline = 'Y'");
	$custAgentID = $custAgentQuery["userID"];
}else{
	//echo("Condition Not Selected");
	}

if(trim($_POST["transType"]) == "Bank Transfer" || trim($_POST["transType"]) == "Home Delivery")
{
	
	  $Tran_Type = $_POST["transType"];
		$agentCountry = $_POST["benCountry"];
		$query = "select * from admin where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'N'  and isCorrespondent != ''";				
		/*$nResult = mysql_query($query)or die("Invalid query: " . mysql_error()); 
		$rstRow = mysql_fetch_array($nResult);	
		$benAgentIDs =  $rstRow["userID"];		*/
		
		$ida = selectMultiRecords($query);
		for ($j=0; $j < count($ida); $j++)
		{
			$authority = $ida[$j]["authorizedFor"];
			$toCountry = $ida[$j]["IDAcountry"];
			
			if(strstr($authority,"Home Delivery") && strstr($toCountry,$agentCountry))
			{
				$benAgentIDs =  $ida[$j]["userID"];
				
				}
			
		}
		// if find no IDA against this BenCountry then assign this transaction to AdminIDA 
		if($benAgentIDs == '')
		{
		
		//	$benAgentIDs = 100111;
			}//100098

      if($_POST["transType"] == "Bank Transfer")
			{
				$benAgentIDs = $_POST["distribut"];
				
				}
	
	
}
elseif(trim($_POST["transType"]) == "Pick up")
{
	
			$queryCust = "select *  from cm_collection_point where  cp_id ='" . $_SESSION["collectionPointID"] . "'";
			$senderAgentContent = selectFrom($queryCust);
			$queryIDA="select * from ".TBL_ADMIN_USERS." where userID='".$senderAgentContent["cp_ida_id"]."'";
			$IDAContent=selectFrom($queryIDA);
			//if(count($IDAContent))
			//{
				$benAgentIDs = $IDAContent["userID"];			
			//}
			if($benAgentIDs == "")
			{
			
				//$queryIDA="select * from ".TBL_ADMIN_USERS." where username = 'MonexIDA'";
				//$IDAContent=selectFrom($queryIDA);
				//$benAgentIDs = '20066';//$IDAContent["userID"];
				
			}
}
/*
else
{
	$benAgentIDs = $_POST["agentID"];
}*/

$_SESSION["moneyPaid"] 			= $_POST["moneyPaid"];
$_SESSION["transactionPurpose"] = $_POST["transactionPurpose"];
$_SESSION["fundSources"] 		= $_POST["fundSources"];
$_SESSION["refNumber"] 			= $_POST["refNumber"];
$_SESSION["benAgentID"] 		= $benAgentIDs;
$_SESSION["bankCharges"] = $_POST["bankCharges"];

$_SESSION["bankName"] 		= $_POST["bankName"];
$_SESSION["branchCode"] 	= $_POST["branchCode"];
$_SESSION["branchAddress"] 	= trim($_POST["branchAddress"]);
$_SESSION["swiftCode"] 		= $_POST["swiftCode"];
$_SESSION["accNo"] 			= $_POST["accNo"];
$_SESSION["ABACPF"] 		= $_POST["ABACPF"];
$_SESSION["IBAN"] 			= $_POST["IBAN"];
$_SESSION["accountType"] = $_POST["accountType"];

$_SESSION["cc_cardType"] = $_POST["cc_cardType"];
$_SESSION["cc_cardNo"] = $_POST["cc_cardNo"];
$_SESSION["cc_expiryDate"] = $_POST["cc_expiryDate"] ;
$_SESSION["cc_cvvNo"]	= $_POST["cc_cvvNo"];
$_SESSION["cc_firstName"] = $_POST["cc_firstName"];
$_SESSION["cc_lastName"] = $_POST["cc_lastName"];
$_SESSION["cc_address1"] = $_POST["cc_address1"] ;
$_SESSION["cc_address2"]	= $_POST["cc_address2"];
$_SESSION["cc_city"] = $_POST["cc_city"];
$_SESSION["cc_state"] = $_POST["cc_state"];
$_SESSION["cc_postcode"] = $_POST["cc_postcode"] ;
$_SESSION["cc_country"]	= $_POST["cc_country"];
$_SESSION["tip"]	= $_POST["tip"];

if($_POST["transID"] != "")
{
	$backUrl = "add-customer-transaction.php?msg=Y&transID=" . $_POST["transID"];
}
else
{
	$backUrl = "add-customer-transaction.php?msg=Y";
}

if(trim($_POST["transType"]) == "")
{	
	insertError(TE3);	
	redirect($backUrl);
}
if(trim($_POST["customerID"]) == "")
{
	if($_SESSION["customerID"] == "")
	{
		$_POST["customerID"] = $_SESSION["c_id"];
		$_SESSION["customerID"] = $_SESSION["c_id"];
	}
}

if(trim($_POST["benID"]) == "")
{
	insertError(TE6);	
	redirect($backUrl);
}
if(trim($_POST["moneyPaid"]) == "")
{
	insertError(TE12);	
	redirect($backUrl);
}
// Validity cheks for Bank Details of Beneficiary.
if(trim($_POST["transType"]) == "Bank Transfer")
{
	if(trim($_POST["bankName"]) == "")
	{
		insertError(TE14);	
		redirect($backUrl);
	}
	
	if(trim($_POST["accNo"]) == "")
	{
		insertError(TE18);	
		redirect($backUrl);
	}

	/*if(trim($_POST["branchAddress"]) == "")
	{
		insertError(TE16);	
		redirect($backUrl);
	}*/

	/*if(trim($_POST["ABACPF"]) == "" && $_POST["benCountry"] == "United States")
	{
		insertError(TE19);	
		redirect($backUrl);
	}*/
	
		if(trim($_POST["ABACPF"]) == "" && strstr(CONFIG_CPF_COUNTRY , '$_POST["benCountry"]'))
		{
			insertError(TE20);	
			redirect($backUrl);
		}

	/*if(ibanCountries($_POST["benCountry"]) && trim($_POST["IBAN"]) == "")
	{
		insertError(TE21);	
		redirect($backUrl);
	}*/
}

if(trim($_POST["transAmount"]) == "" || $_POST["transAmount"] <= 0)
{
	insertError(TE8);	
	redirect($backUrl);
}

if(trim($_POST["exchangeRate"]) == "" || $_POST["exchangeRate"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["exchangeID"]) == "")
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["localAmount"]) == "" || $_POST["localAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(CONFIG_ZERO_FEE == '1')
{
	if( $_POST["IMFee"] < 0)
	{
		insertError(TE9);	
		redirect($backUrl);
	}
}
else
{
	if(trim($_POST["IMFee"]) == "" || $_POST["IMFee"] <= 0)
	{
		insertError(TE9);	
		redirect($backUrl);
	}
}

if(trim($_POST["totalAmount"]) == "" || $_POST["totalAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["transactionPurpose"]) == "")
{
	insertError(TE10);	
	redirect($backUrl);
}

if(trim($_POST["Declaration"]) != "Y")
{
	insertError(TE13);	
	redirect($backUrl);
}
if(PIN_CODE)
{
if(trim($_POST["pincode"]) == "")
		{	
				insertError(PIN1);	
				redirect($backUrl);	
		}
		else
		{
			$PINCODE = $_POST["pincode"];//64297
			$c_id= $_POST["customerID"];
			$strRandomQuery = "SELECT * FROM cm_customer where c_id = '$c_id' and SecuriyCode = '$PINCODE'";
			$nRandomResult = mysql_query($strRandomQuery)or die("Invalid query: " . mysql_error()); 
			$nRows = mysql_fetch_array($nRandomResult);
			$oldPINcode = $nRows["SecuriyCode"];
			if($oldPINcode == '')
			{
				insertError(PIN1);	
				redirect($backUrl);
			}
		}		
}
if($_SESSION["moneyPaid"] == 'By Credit Card' || $_SESSION["moneyPaid"] == 'By Debit Card')
{
  		if(trim($_POST["cc_cardType"]) == "")
		{
			insertError(CR1);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_cardNo"]) == "")
		{
			insertError(CR2);	
			redirect($backUrl);
		}
		if(trim($_POST["ccExpYr"]) == "")
		{
			insertError(CR3);	
			redirect($backUrl);
		}
		if(trim($_POST["ccExpMo"]) == "")
		{
			insertError(CR3);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_cvvNo"]) == "")
		{
			insertError(CR4);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_firstName"]) == "")
		{
			insertError(CR5);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_lastName"]) == "")
		{
			insertError(CR6);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_address1"]) == "")
		{
			insertError(CR7);	
			redirect($backUrl);
		}
		/*if(trim($_POST["cc_address2"]) == "")
		{
			insertError(CR8);	
			redirect($backUrl);
		}*/
		if(trim($_POST["cc_city"]) == "")
		{
			insertError(CR9);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_state"]) == "")
		{
			insertError(CR10);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_postcode"]) == "")
		{
			insertError(CR11);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_country"]) == "")
		{
			insertError(CR12);	
			redirect($backUrl);
		}		
}

		
		
if($_POST["transID"] == "")
{
		//Getting New reference number
		$userId = $_SESSION["customerID"];
		$strQuery = "SELECT * FROM cm_customer where c_id = $userId";
		$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
		$rstRow = mysql_fetch_array($nResult);				
		$username = $rstRow["c_name"];	


		$query = "select count(transID) as cnt from transactions where customerID='".$_POST["customerID"]."'";
		$nextAutoID = countRecords($query);
		$nextAutoID = $nextAutoID + 1;
		$cntChars = strlen($nextAutoID);
		if($cntChars == 1)
			$leadingZeros = "0000";
		elseif($cntChars == 2)
			$leadingZeros = "000";
		elseif($cntChars == 3)
			$leadingZeros = "00";		
		elseif($cntChars == 4)
			$leadingZeros = "0";		
		/*elseif($cntChars == 5)
			$leadingZeros = "0";*/

/*		if($cntChars == 1)
			$leadingZeros = "00000";
		elseif($cntChars == 2)
			$leadingZeros = "0000";
		elseif($cntChars == 3)
			$leadingZeros = "000";		
		elseif($cntChars == 4)
			$leadingZeros = "00";		
		elseif($cntChars == 5)
			$leadingZeros = "0";*/

		$imReferenceNumber = strtoupper($username) ."-". $leadingZeros . $nextAutoID ;

if($_SESSION["moneyPaid"] != 'By Credit Card' && $_SESSION["moneyPaid"] != 'By Debit Card')
{				
		$strRandomQuery = "SELECT * FROM cm_customer where c_id = $customerID";
		$nRandomResult = mysql_query($strRandomQuery)or die("Invalid query: " . mysql_error()); 
		$nRows = mysql_fetch_array($nRandomResult);
		$Balance = $nRows["Balance"];		
		if($Balance >= $_POST["transAmount"])	
		{
			//$Balance = ($Balance - ($_POST["transAmount"] + $_POST["IMFee"]));
			//$amount = ($_POST["transAmount"] + $_POST["IMFee"]);
			$Balance = ($Balance - ($_POST["transAmount"] + $_POST["IMFee"] + $_POST["bankCharges"]));
			$amount = ($_POST["transAmount"] + $_POST["IMFee"] + $_POST["bankCharges"]);
			$strQuery = "update cm_customer set Balance = '$Balance' where c_id = $customerID";
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());			
			$Status	= "Authorize";
			$tran_date = date("Y-m-d");
			$AuthorizationDate	= $tran_date;
			
			$tran_date = date("Y-m-d");
			$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
						 values('".$customerID."','".$tran_date."','Transaction is Credit','WITHDRAW','".$amount."','".$imReferenceNumber."'
						 )";						 
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());			
		}
		else
		{
			$Status	= "Pending";
			$AuthorizationDate = "";
			//$amount = ($_POST["transAmount"] + $_POST["IMFee"]);
			$amount = ($_POST["transAmount"] + $_POST["IMFee"] + $_POST["bankCharges"]);
			$tran_date = date("Y-m-d");
			$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo ) 
						 values('".$customerID."','".$tran_date."','Awaiting Payment','WITHDRAW','".$amount."','".$imReferenceNumber."'
						 )";						 
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());					
		}
		
		if(CONFIG_ONLINE_AGENT == '1')
		{	
			
			$descript = "Transaction Created";
			updateAgentAccount($custAgentID, $amount, $imReferenceNumber, "WITHDRAW", $descript, 'Agent');	
		}	
}
		
					
			if($_SESSION["moneyPaid"] == 'By Credit Card' || $_SESSION["moneyPaid"] == 'By Debit Card')
			{
			
/*************************         c r e d i t  c a r d   i n f o r m a t i o n  ******************************/



					$contentCust = selectFrom("select * from cm_customer where c_id='".$_POST["customerID"]."'");
					$contentBenf = selectFrom("select * from cm_beneficiary where benID='".$_POST["benID"]."'");
					
					$query = "select * from cities where  country ='".$_POST["cc_country"]."'";
					$nResult = mysql_query($query)or die("Invalid query: " . mysql_error());
					$rstRow = mysql_fetch_array($nResult);	
					$custContryCode =  $rstRow["countryCode"];
					
					$query = "select * from cities where  country ='".$contentBenf["Country"]."'";
					$nResult = mysql_query($query)or die("Invalid query: " . mysql_error());
					$rstRow = mysql_fetch_array($nResult);	
					$benContryCode =  $rstRow["countryCode"]; 
					$benStateCode =  $rstRow["State"]; 						
					
					
					if($_POST["cc_country"] == "United Kingdom")
					{
						$query = "select * from states where  state  ='".$_POST["cc_state"]."'";
						$nResult = mysql_query($query)or die("Invalid query: " . mysql_error());
						$rstRow = mysql_fetch_array($nResult);	
						$custStateCode =  $rstRow["code"];		
					}
					else
					{					
					$custStateCode =  $_POST["cc_state"];
					}
					
					if($_POST["Save"] == "Y")
					{				
							$sql = "INSERT INTO cm_cust_credit_card
												( 
												customerID , cardNo , cardName ,
												cardCVV ,  
												expiryDate , firstName , 
												lastName , address , address2 ,city , 
												state , 
												country , postalCode 
												) 
												VALUES     
												( 
												'".$_POST["customerID"]."', '".$_POST["cc_cardNo"]."', '".$_POST["cc_cardType"]."',										 
												'".$_POST["cc_cvvNo"]."', 
												'".$_POST["ccExpMo"]."-".$_POST["ccExpYr"]."', '".$_POST["cc_firstName"]."', 
												'".$_POST["cc_lastName"]."', '".$_POST["cc_address1"]."', 
												'".$_POST["cc_address2"]."', '".$_POST["cc_city"]."',
												'".$_POST["cc_state"]."', 											 											 
												'".$_POST["cc_country"]."', '".$_POST["cc_postcode"]."'																								
												) 
												"; 									
							insertInto($sql);
					}					
					
					$nTime = time();
					$nTime = $nTime-1000000000;
					$nTime = ($nTime + $_POST["customerID"]);
									

					$nTime = time();
					$custID = "UK".$userId;
					$nTime = $nTime.$custID;
					$nCharcter = strlen($nTime);

					if($nCharcter == 16)
						$rand = rand(1000, 9999);
					if($nCharcter == 7)
						$rand = rand(100, 999);
											
					$nTransId = $nTime.$rand;
					//$transAmounts = ($_POST["transAmount"]+$_POST["IMFee"]);
					$transAmounts = ($_POST["transAmount"]+$_POST["IMFee"]+$_POST["bankCharges"]);
									
/*					$strXML = "\"<DEBIT><TRANSACTION_ID>".$nTransId."</TRANSACTION_ID><TRANSACTION_AMOUNT>".$_POST["transAmount"]."</TRANSACTION_AMOUNT><ORIGINATION_CURRENCY>GBP</ORIGINATION_CURRENCY><CARD_NUMBER>".$_POST["cc_cardNo"]."</CARD_NUMBER><EXPIRATION_DATE>".$_POST["cc_expiryDate"]."</EXPIRATION_DATE><CARD_TYPE>".$_POST["cc_cardType"]."</CARD_TYPE><CARD_CVV>".$_POST["cc_cvvNo"]."</CARD_CVV><CUSTOMER_ID>".$_POST["customerID"]."</CUSTOMER_ID><FIRST_NAME>".$_POST["cc_firstName"]."</FIRST_NAME><LAST_NAME>".$_POST["cc_lastName"]."</LAST_NAME><ADDRESS>".$_POST["cc_address1"]."</ADDRESS><ADDRESS_2>".$_POST["cc_address2"]."</ADDRESS_2><CITY>".$_POST["cc_city"]."</CITY><STATE>".$custStateCode."</STATE><POSTAL_CODE>".$_POST["cc_postcode"]."</POSTAL_CODE><COUNTRY_CODE>".$custContryCode."</COUNTRY_CODE><TELEPHONE>".$contentCust["c_phone"]."</TELEPHONE><EMAIL>".$contentCust["c_email"]."</EMAIL><IP_ADDRESS>".$contentCust["accessFromIP"]."</IP_ADDRESS><B_FIRST_NAME>".$contentBenf["firstName"]."</B_FIRST_NAME><B_LAST_NAME>".$contentBenf["lastName"]."</B_LAST_NAME><B_ADDRESS>".$contentBenf["Address"]."</B_ADDRESS><B_ADDRESS_2>".$contentBenf["Address1"]."</B_ADDRESS_2><B_CITY>".$contentBenf["City"]."</B_CITY><B_STATE>".$benStateCode."</B_STATE><B_POSTAL_CODE>".$contentBenf["Zip"]."</B_POSTAL_CODE><B_COUNTRY_CODE>".$benContryCode."</B_COUNTRY_CODE><B_TELEPHONE>".$contentBenf["Phone"]."</B_TELEPHONE><B_EMAIL>".$contentBenf["email"]."</B_EMAIL></DEBIT>\"";
*/

$expiryDate = $_POST["ccExpMo"].$_POST["ccExpYr"];

					$strXML = "<DEBIT><TRANSACTION_ID>".$nTransId."</TRANSACTION_ID><TRANSACTION_AMOUNT>".$transAmounts."</TRANSACTION_AMOUNT><ORIGINATION_CURRENCY>GBP</ORIGINATION_CURRENCY><CARD_NUMBER>".$_POST["cc_cardNo"]."</CARD_NUMBER><EXPIRATION_DATE>".$expiryDate."</EXPIRATION_DATE><CARD_TYPE>".$_POST["cc_cardType"]."</CARD_TYPE><CARD_CVV>".$_POST["cc_cvvNo"]."</CARD_CVV><CUSTOMER_ID>".$_POST["customerID"]."</CUSTOMER_ID><FIRST_NAME>".$_POST["cc_firstName"]."</FIRST_NAME><LAST_NAME>".$_POST["cc_lastName"]."</LAST_NAME><ADDRESS>".$_POST["cc_address1"]."</ADDRESS><ADDRESS_2>".$_POST["cc_address2"]."</ADDRESS_2><CITY>".$_POST["cc_city"]."</CITY><STATE>".$custStateCode."</STATE><POSTAL_CODE>".$_POST["cc_postcode"]."</POSTAL_CODE><COUNTRY_CODE>".$custContryCode."</COUNTRY_CODE><TELEPHONE>".$contentCust["c_phone"]."</TELEPHONE><EMAIL>".$contentCust["c_email"]."</EMAIL><IP_ADDRESS>".$contentCust["accessFromIP"]."</IP_ADDRESS><B_FIRST_NAME>".$contentBenf["firstName"]."</B_FIRST_NAME><B_LAST_NAME>".$contentBenf["lastName"]."</B_LAST_NAME><B_ADDRESS>".$contentBenf["Address"]."</B_ADDRESS><B_ADDRESS_2>".$contentBenf["Address1"]."</B_ADDRESS_2><B_CITY>".$contentBenf["City"]."</B_CITY><B_STATE>".$benStateCode."</B_STATE><B_POSTAL_CODE>".$contentBenf["Zip"]."</B_POSTAL_CODE><B_COUNTRY_CODE>".$benContryCode."</B_COUNTRY_CODE><B_TELEPHONE>".$contentBenf["Phone"]."</B_TELEPHONE><B_EMAIL>".$contentBenf["email"]."</B_EMAIL></DEBIT>";
						//exit();
						//echo $strXML;
						//echo "<br><br><br><br><br><br>";
						//exec("IMCreditAppJava.jar $strXML");
						$strXML = urlencode($strXML);
						$strXML = "\"".$strXML."\"";
						exec("java -jar IMCreditAppJava.jar $strXML");
						
						//sleep(10);
						$query = "select * from cm_credit_trans where transID  =  '$nTransId'";
						$nResult = mysql_query($query)or die("Invalid query: " . mysql_error());
						while($rstRow = mysql_fetch_array($nResult))
						{								
							$AttemptCode  =  $rstRow["AttemptCode"];
							$PQTransID =  $rstRow["PQTransID"];
							$ApprovalCode  =  $rstRow["ApprovalCode"];		  
							$ResultCode   =  $rstRow["ResultCode"];
							$ResultText   =  $rstRow["ResultText"];
							$transRefID  = $rstRow["transID"];
						}
						if($AttemptCode == "SUCC" && $ResultCode == "S001")
						{
							/******************************************* Transactin  Working***********************************************/
						$tran_date = date("Y-m-d");
						$AuthorizationDate	= $tran_date;
						$sql = "INSERT INTO transactions
											( 
											customerID, benID, refNumberIM,
											exchangeID, custAgentID, 
											transAmount, exchangeRate, 
											localAmount, IMFee, totalAmount,benAgentID, 
											transactionPurpose, 
											transDate, transType, 											 
											currencyFrom,   
											currencyTo ,moneyPaid ,bankCharges ,PINCODE,createdBy,transStatus,toCountry,
											fromCountry,collectionPointID  ,authoriseDate, question, answer, Tip, outCurrCharges 
											) 
											VALUES     
											( 
											'".$_POST["customerID"]."', '".$_POST["benID"]."', '$imReferenceNumber',										 
											'".$_POST["exchangeID"]."', '".$custAgentID."', 
											'".$_POST["transAmount"]."', '".$_POST["exchangeRate"]."', 
											'".$_POST["localAmount"]."', '".$_POST["IMFee"]."', 
											'".$_POST["totalAmount"]."', '".$benAgentIDs."',
											'".checkValues($_POST["transactionPurpose"])."', 											 											 
											'".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$_POST["transType"]."', 											 
											'".$_POST["currencyFrom"]."', '".$_POST["currencyTo"]."', '".$_POST["moneyPaid"]."',
											'".$_POST["bankCharges"]."','".$_POST["pincode"]."','CUSTOMER','$Status','$agentCountry',
											'".$_POST["benCountry"]."','".$_POST["collectionPointID"]."','".$AuthorizationDate."',
											'".$_SESSION["question"]."','".$_SESSION["answer"]."','".$_SESSION["tip"]."','".$_SESSION["currencyCharge"]."'																								
											) 
											"; 
									
									if(insertInto($sql))
									{
										$amount = ($_POST["transAmount"] + $_POST["IMFee"]);
										//$amount = ($_POST["transAmount"] + $_POST["IMFee"] + $_POST["bankCharges"]);
										$tran_date = date("Y-m-d");
										$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount ,tranRefNo) 
													 values('".$_POST["customerID"]."','".$tran_date."','Payment by Credit Card','DEPOSIT','".$amount."','$imReferenceNumber'
													 )";
										$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
										
										$tran_date = date("Y-m-d");
										$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo ) 
													 values('".$_POST["customerID"]."','".$tran_date."','Payment by Credit Card','WITHDRAW','".$amount."','$imReferenceNumber'
													 )";
										$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
																														
										
										$fromName = SUPPORT_NAME;
										$fromEmail = SUPPORT_EMAIL;
										// getting admin email address.
							
										//$benContents = selectFrom("select email from ". TBL_BENEFICIARY ." where benID= '". $_POST["benID"] ."'");
										//$benEmail = $benContents["email"];
										// Getting Customer Email
										$custContents = selectFrom("select * from cm_customer where c_id= '". $_POST["customerID"] ."'");
										$custEmail = $custContents["c_name"];
										$custTitle = $custContents["Title"];
										$custFirstName = $custContents["FirstName"];
										$custMiddleName = $custContents["MiddleName"];
										$custLastName = $custContents["LastName"];
										$custCountry = $custContents["c_country"];
										$custCity = $custContents["c_city"];
										$custZip = $custContents["c_zip"];
										$custLoginName = $custContents["username"];												
										$custEmail = $custContents["c_email"];
										$custPhone = $custContents["c_phone"];
							
							
										$benContents = selectFrom("select * from cm_beneficiary where benID= '". $_POST["benID"] ."'");			
										$benTitle = $benContents["Title"];
										$benFirstName = $benContents["firstName"];
										$benMiddleName = $benContents["middleName"];
										$benLastName = $benContents["lastName"];
										$benAddress  = $benContents["Address"];
										$benCountry = $benContents["Country"];
										$benCity = $benContents["City"];
										$benZip = $benContents["Zip"];
										$benLoginName = $benContents["username"];												
										$benEmail = $benContents["email"];
										$benPhone = $benContents["Phone"];
										
							
										$AdmindContents = selectFrom("select * from admin where userID = '". $benAgentIDs ."'");			
										$AdmindLoginName = $AdmindContents["username"];
										$AdmindName = $AdmindContents["name"];
										$AdmindEmail = $AdmindContents["email"];
							
							
										$cContents = selectFrom("select * from cm_collection_point where cp_id  = '". $_POST["collectionPointID"] ."'");			
										$agentnamee = $cContents["cp_corresspondent_name"];
										$contactperson = $cContents["cp_corresspondent_name"];
										$company = $cContents["cp_branch_name"];
										$address = $cContents["cp_branch_address"];
										$country = $cContents["cp_country"];
										$city = $cContents["cp_city"];
										$phone = $cContents["cp_phone"];						
										$tran_date = date("Y-m-d");
										$tran_date = date("F j, Y", strtotime("$tran_date"));
										$subject = "New transaction created";
										
							/***********************************************************/
							$message = "
							  
							  
								Dear  $custTitle $custFirstName $custLastName  
							  
								
							  
								Thanks for choosing $company2 as your money transfer company. We will keep you informed through email regarding your transaction till the money reached to your beneficiary. Please see the attached your transaction detail and some important information. 
							  
								
							  
								Transaction Detail: 
								
							  
								-----------------------------------------------------------------------------------
							  
								 Transaction Type:  ".$_POST['transType']." 
								 Status: $Status 
							  
								 $systemCode:   $imReferenceNumber 
								 Transaction Date:  ".dateFormat($tran_date)."  
							  
								 Authorised Date: ".dateFormat($tran_date)."
								
							  
								-----------------------------------------------------------------------------------
							  
							  
								Beneficiary Detail:
								
							  
								-----------------------------------------------------------------------------------
							  
								Beneficiary Name:  $benTitle $benFirstName $benLastName
								
							  
								 Address:  $benAddress 
								 Postal / Zip Code:  $benZip 
							  
								 Country:   $benCountry   
								 Phone:   $benPhone   
							  
								 Email:  $benEmail  
								
							  
								
								
							  ";
							   
							if(trim($_POST['transType']) == 'Bank Transfer')
							{
							  
							$message.="  
								beneficiary Bank Details:
								
								
							  
								 Bank Name:  ".$_POST['bankName']."  
								 Acc Number:  ".$_POST['accNo']."  
							  
								 Branch Code:  ".$_POST['branchCode']."  
								 Branch Address:  ".$_POST['branchAddress']."  
							  
								 Swift Code:  ".$_POST['swiftCode']."  
								
							  
								
								
							  
							";
							}
							elseif(trim($_POST['transType']) == "Pick up")
							{
							$message .= "
							  
								
								
							  
								Collection Point Details:  
								
							  
								
								
							  
								 Agent Name : $agentname 
								 Contact Person:  $contactperson 
							  
								 Company:  $company 
								 Address:  $address 
							  
								Country:   $country
								City:  $city
							  
								Phone:  $phone
								
							   ";
							
							}
							$message .="
							  
								-----------------------------------------------------------------------------------
							  															
							  
								Amount Details: 
								
															  
								-----------------------------------------------------------------------------------
							  
							  
								 Exchange Rate:  ".$_POST['exchangeRate']."
								 Amount:  ".$_POST['transAmount']." 
							  
								 ".$systemPre." Fee:  ".$_POST['IMFee']." 
								 Local Amount:  ".$_POST['localAmount']." 
							  
								 Transaction Purpose:  ".$_POST['transactionPurpose']." 
								 Total Amount:  ".$_POST['totalAmount']." 
							  
								 Money Paid:  ".$_POST['moneyPaid']." 
								

							";							/**********************************************************/			
								sendMail($custEmail, $subject, $message, $fromName, $fromEmail);
							
								$transID = @mysql_insert_id();
										
									if($_SESSION["agentID_id"] != "")
									{		
										$entryDate = date("Y-m-d");
										insertInto("insert into agent_logfile (agentID , transID  , entryDate , remarks,customerID ) 
										VALUES ('". $_SESSION["agentID_id"]."', $transID, $entryDate,'INSERTION','".$_POST["customerID"]."')");		
									}
							
										if(trim($_POST["transType"]) == "Bank Transfer")
										{
										
											$strQuery = "delete FROM cm_bankdetails where benID = '".$_POST["benID"]."'";
											$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 			
							
											$sqlBank = "insert into cm_bankdetails
														( 
														benID, bankName, 
														accNo, branchCode, branchAddress,
														ABACPF , IBAN,swiftCode , accountType
														)
														Values
														(
														'".$_POST["benID"]."',  '".$_POST["bankName"]."',
														'".$_POST["accNo"]."', '".$_POST["branchCode"]."', '".$_POST["branchAddress"]."',
														'".$_POST["ABACPF"]."',	'".$_POST["IBAN"]."',	'".$_POST["swiftCode"]."','".$_SESSION["accountType"]."'
														)
														";
											insertInto($sqlBank);
							
											$queryUpdate="update cm_beneficiary set 
															 transType = '".$_POST["transType"]."'						
															 where benID='".$_POST["benID"]."'";
											update($queryUpdate);		
										  }
										  
										$_SESSION["collectionPointID"] = "";
										$_SESSION["transTypes"] = "";
										$_SESSION["transType"] 		= "";
										$_SESSION["benAgentID"] 	= "";
										$_SESSION["customerID"] 	= "";
										$_SESSION["benID"] 			= "";
										$_SESSION["moneyPaid"] 		= "";
										$_SESSION["transactionPurpose"] = "";
										$_SESSION["fundSources"] 	= "";
										$_SESSION["refNumber"] 		= "";
								
										$_SESSION["transAmount"] 	= "";
										$_SESSION["exchangeRate"] 	= "";
										$_SESSION["exchangeID"] 	= "";
										$_SESSION["localAmount"] 	= "";
										$_SESSION["totalAmount"] 	= "";
										$_SESSION["IMFee"] 			= "";
										//$_SESSION["bankCharges"] = "";	
										// resetting Session vars for trans_type Bank Transfer
										$_SESSION["bankName"] 		= "";
										$_SESSION["branchCode"] 	= "";
										$_SESSION["branchAddress"] 	= "";
										$_SESSION["swiftCode"] 		= "";
										$_SESSION["accNo"] 			= "";
										$_SESSION["ABACPF"] 		= "";
										$_SESSION["IBAN"] 			= "";
										$_SESSION["accountType"] = "";
										
										$_SESSION["cardType"]= "";
										$_SESSION["cardNo"]= "";
										$_SESSION["expiryDate"]= "";
										$_SESSION["cvvNo"]	= "";
										$_SESSION["pincode"] = "";
										
										
										$_SESSION["question"] 			= "";
										$_SESSION["answer"]     = "";	
										$_SESSION["tip"]     = "";	
										$_SESSION["currencyCharge"] = "";
										insertError("Transaction has been added successfully for processing and ".$systemPre." reference number is $imReferenceNumber");
										redirect("thanks.php?msg1=Y&success=Y");
								}
							
							/******************************************* Transaction Working ***********************************************/
						}
						elseif($AttemptCode == "INVR")
						{
							insertError("$AttemptCode : There was an error while communicating with the Server, Please try again after some Time!");	
							redirect($backUrl);						
						}
						elseif($AttemptCode == "NORS")
						{
							insertError("$AttemptCode : There was an error while communicating with the Server, Please try again after some Time!");	
							redirect($backUrl);	
						}
						elseif($AttemptCode == "BADR")
						{
							insertError("$AttemptCode : There was an error while communicating with the Server, Please try again after some Time!");	
							redirect($backUrl);												
						}
						elseif($AttemptCode == "")
						{
							insertError("$AttemptCode: We are Sorry we are unable to Progress your Credit Card at this time. Please try again later.  If the Problem Persist Please Contact to the $company2 Customer Support!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "S000")
						{
							insertError("$ResultCode: We are Sorry we are unable to Progress your Credit Card at this time. Please try again later.  If the Problem Persist Please Contact to the $company2 Customer Support!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "S009" || $ResultCode == "T003")
						{
							insertError("$ResultCode: We are Sorry we are unable to Progress your Credit Card at this time. Please try again later.  If the Problem Persist Please Contact to the $company2 Customer Support!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "T000")
						{
							insertError("$ResultCode: We are Sorry we are unable to Progress your Credit Card at this time. Please try again later.  If the Problem Persist Please Contact to the $company2 Customer Support!");		
							redirect($backUrl);						
						}
						elseif($ResultCode == "T001")
						{
							insertError("$ResultCode: We are Sorry we are unable to Progress your Credit Card at this time. Please try again later.  If the Problem Persist Please Contact to the $company2 Customer Support!");		
							redirect($backUrl);						
						}
						elseif($ResultCode == "T002")
						{
							insertError("$ResultCode: We are Sorry we are unable to Progress your Credit Card at this time. Please try again later.  If the Problem Persist Please Contact to the $company2 Customer Support!");		
							redirect($backUrl);						
						}
						elseif($ResultCode == "T004")
						{
							insertError("System Unavailable, Please try again after in a few minutes!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "R001")
						{
							insertError(" $ResultCode: The amount exceeds your per transaction limit, Please Enter Correct Amount!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "R002")
						{
							insertError(" $ResultCode: The amount exceeds your credit limit, Please Enter Correct Amount!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "R003")
						{
							insertError(" $ResultCode: Ask from PayQuick!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "R004")
						{
							insertError(" $ResultCode: Ask from PayQuick!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "R005")
						{
							insertError(" $ResultCode: Ask from PayQuick!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "P001" || $ResultCode == "P002" || $ResultCode == "P003" || $ResultCode == "S010")
						{
							insertError("Transaction has been Declined, Please try again after some Time!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "X000")
						{
							insertError("$ResultCode: We are Sorry we are unable to Progress your Credit Card at this time. Please try again later.  If the Problem Persist Please Contact to the $company2 Customer Support!");		
							redirect($backUrl);						
						}
						elseif($ResultCode == "X001")
						{
							insertError(" $ResultCode: Ask from PayQuick!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "X002")
						{
							insertError(" $ResultCode: Ask from PayQuick!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "X003")
						{
							insertError("Please Contact to Customer Support office of $company2 !");	
							redirect($backUrl);						
						}					
						

/*************************         c r e d i t  c a r d   i n f o r m a t i o n ******************************/
		}
		else
		{
							/******************************************* Transactin  Working***********************************************/
							
									 $sql = "INSERT INTO transactions
																		( 
																		customerID, benID, refNumberIM,
																		exchangeID, custAgentID, 
																		transAmount, exchangeRate, 
																		localAmount, IMFee, totalAmount,benAgentID, 
																		transactionPurpose, 
																		transDate, transType, 											 
																		currencyFrom,   
																		currencyTo ,moneyPaid ,bankCharges ,PINCODE,createdBy,transStatus,toCountry,
																		fromCountry,collectionPointID  ,authoriseDate,question, answer, tip, outCurrCharges 
																		) 
																		VALUES     
																		( 
																		'".$_POST["customerID"]."', '".$_POST["benID"]."', '$imReferenceNumber',										 
																		'".$_POST["exchangeID"]."', '".$custAgentID."',
																		'".$_POST["transAmount"]."', '".$_POST["exchangeRate"]."', 
																		'".$_POST["localAmount"]."', '".$_POST["IMFee"]."', 
																		'".$_POST["totalAmount"]."', '".$benAgentIDs."',
																		'".checkValues($_POST["transactionPurpose"])."', 											 											 
																		'".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$_POST["transType"]."', 											 
																		'".$_POST["currencyFrom"]."', '".$_POST["currencyTo"]."', '".$_POST["moneyPaid"]."',
																		'".$_POST["bankCharges"]."','".$_POST["pincode"]."','CUSTOMER','$Status','$agentCountry',
																		'".$_POST["benCountry"]."','".$_POST["collectionPointID"]."','".$AuthorizationDate."',
																		'".$_SESSION["question"]."','".$_SESSION["answer"]."','".$_SESSION["tip"]."','".$_SESSION["currencyCharge"]."'		
																								
																		) 
																		"; 
									
									if(insertInto($sql))
									{
										
										$fromName = SUPPORT_NAME;
										$fromEmail = SUPPORT_EMAIL;
										// getting admin email address.
							
										//$benContents = selectFrom("select email from ". TBL_BENEFICIARY ." where benID= '". $_POST["benID"] ."'");
										//$benEmail = $benContents["email"];
										// Getting Customer Email
										$custContents = selectFrom("select * from cm_customer where c_id= '". $_POST["customerID"] ."'");
										$custEmail = $custContents["c_name"];
										$custTitle = $custContents["Title"];
										$custFirstName = $custContents["FirstName"];
										$custMiddleName = $custContents["MiddleName"];
										$custLastName = $custContents["LastName"];
										$custCountry = $custContents["c_country"];
										$custCity = $custContents["c_city"];
										$custZip = $custContents["c_zip"];
										$custLoginName = $custContents["username"];												
										$custEmail = $custContents["c_email"];
										$custPhone = $custContents["c_phone"];
							
							
										$benContents = selectFrom("select * from cm_beneficiary where benID= '". $_POST["benID"] ."'");			
										$benTitle = $benContents["Title"];
										$benFirstName = $benContents["firstName"];
										$benMiddleName = $benContents["middleName"];
										$benLastName = $benContents["lastName"];
										$benAddress  = $benContents["Address"];
										$benCountry = $benContents["Country"];
										$benCity = $benContents["City"];
										$benZip = $benContents["Zip"];
										$benLoginName = $benContents["username"];												
										$benEmail = $benContents["email"];
										$benPhone = $benContents["Phone"];
										
							
										$AdmindContents = selectFrom("select * from admin where userID = '". $benAgentIDs ."'");			
										$AdmindLoginName = $AdmindContents["username"];
										$AdmindName = $AdmindContents["name"];
										$AdmindEmail = $AdmindContents["email"];
							
							
										$cContents = selectFrom("select * from cm_collection_point where cp_id  = '". $_POST["collectionPointID"] ."'");			
										$agentnamee = $cContents["cp_corresspondent_name"];
										$contactperson = $cContents["cp_corresspondent_name"];
										$company = $cContents["cp_branch_name"];
										$address = $cContents["cp_branch_address"];
										$country = $cContents["cp_country"];
										$city = $cContents["cp_city"];
										$phone = $cContents["cp_phone"];						
										$tran_date = date("Y-m-d");
										$tran_date = date("F j, Y", strtotime("$tran_date"));
										$subject = "New transaction created";
										
							/***********************************************************/
							$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
							 
							  <tr>
								<td colspan='2'><p><strong>Dear</strong>  $custTitle&nbsp;$custFirstName&nbsp;$custLastName  </p></td>
							  </tr>
							  <tr>
								<td width='205'>&nbsp;</td>
								<td width='295'>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'><p>Thanks for choosing $company2 as your money transfer company. We will keep you informed through email regarding your transaction till the money reached to your beneficiary. Please see the attached your transaction detail and some important information. </p></td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td><p><strong>Transaction Detail: </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							
							  <tr>
								<td> Transaction Type:  ".$_POST['transType']." </td>
								<td> Status: $Status </td>
							  </tr>
							  <tr>
								<td> $systemCode:   $imReferenceNumber </td>
								<td> Transaction Date:  ".$tran_date."  </td>
							  </tr>
							  <tr>
								<td><p>Authorised Date: </p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  
							  
							  <tr>
								<td><p><strong>Beneficiary Detail: </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td><p>Beneficiary Name:  $benTitle&nbsp;$benFirstName&nbsp;$benLastName</p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td> Address:  $benAddress </td>
								<td> Postal / Zip Code:  $benZip </td>
							  </tr>
							  <tr>
								<td> Country:   $benCountry   </td>
								<td> Phone:   $benPhone   </td>
							  </tr>
							  <tr>
								<td><p>Email:  $benEmail   </p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>";
							   
							if(trim($_POST['transType']) == 'Bank Transfer')
							{
							  
							$message.="  <tr>
								<td><p><strong>Beneficiary Bank Details </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td> Bank Name:  ".$_POST['bankName']."  </td>
								<td> Acc Number:  ".$_POST['accNo']."  </td>
							  </tr>
							  <tr>
								<td> Branch Code:  ".$_POST['branchCode']."  </td>
								<td> Branch Address:  ".$_POST['branchAddress']."  </td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							";//Swift Code:  "$_POST['swiftCode']"  
							}
							elseif(trim($_POST['transType']) == "Pick up")
							{
							$message .= "
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td><p><strong>Collection Point Details  </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td> Agent Name : $agentname </td>
								<td> Contact Person:  $contactperson </td>
							  </tr>
							  <tr>
								<td> Company:  $company </td>
								<td> Address:  $address </td>
							  </tr>
							  <tr>
								<td>Country:   $country</td>
								<td>City:  $city</td>
							  </tr>
							  <tr>
								<td>Phone:  $phone</td>
								<td>&nbsp;</td>
							  </tr> ";
							
							}
							$message .="
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td><p><strong>Amount Details </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td> Exchange Rate:  ".$_POST['exchangeRate']."</td>
								<td> Amount:  ".$_POST['transAmount']." </td>
							  </tr>
							  <tr>
								<td> ".$systemPre." Fee:  ".$_POST['IMFee']." </td>
								<td> Local Amount:  ".$_POST['localAmount']." </td>
							  </tr>  <tr>
								<td> Transaction Purpose:  ".$_POST['transactionPurpose']." </td>
								<td> Total Amount:  ".$_POST['totalAmount']." </td>
							  </tr>  <tr>
								<td> Money Paid:  ".$_POST['moneyPaid']." </td>
								<td> &nbsp; </td>
							  </tr>  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>  
							</table>
							";//Bank Charges:  $_POST['bankCharges']"
							/**********************************************************/			
								sendMail($custEmail, $subject, $message, $fromName, $fromEmail);
							
								$transID = @mysql_insert_id();
								
								////Putting in History
								$descript = "Transaction for online customer is added";
								activities($_SESSION["loginHistoryID"],"INSERTION",$transID,TBL_TRANSACTIONS,$descript);
										
									if($_SESSION["agentID_id"] != "")
									{		
										$entryDate = date("Y-m-d");
										insertInto("insert into agent_logfile (agentID , transID  , entryDate , remarks,customerID ) 
										VALUES ('". $_SESSION["agentID_id"]."', $transID, $entryDate,'INSERTION','".$_POST["customerID"]."')");		
									}
							
										if(trim($_POST["transType"]) == "Bank Transfer")
										{
										
											$strQuery = "delete FROM cm_bankdetails where benID = '".$_POST["benID"]."'";
											$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 			
							
											$sqlBank = "insert into cm_bankdetails
														( 
														benID, bankName, 
														accNo, branchCode, branchAddress,
														ABACPF , IBAN,swiftCode , accountType
														)
														Values
														(
														'".$_POST["benID"]."',  '".$_POST["bankName"]."',
														'".$_POST["accNo"]."', '".$_POST["branchCode"]."', '".$_POST["branchAddress"]."',
														'".$_POST["ABACPF"]."',	'".$_POST["IBAN"]."',	'".$_POST["swiftCode"]."', '".$_SESSION["accountType"]."'
														)
														";
											insertInto($sqlBank);
							
											$queryUpdate="update cm_beneficiary set 
															 transType = '".$_POST["transType"]."'						
															 where benID='".$_POST["benID"]."'";
											update($queryUpdate);		
										  }
										  
										$_SESSION["collectionPointID"] = "";
										$_SESSION["transTypes"] = "";
										$_SESSION["transType"] 		= "";
										$_SESSION["benAgentID"] 	= "";
										$_SESSION["customerID"] 	= "";
										$_SESSION["benID"] 			= "";
										$_SESSION["moneyPaid"] 		= "";
										$_SESSION["transactionPurpose"] = "";
										$_SESSION["fundSources"] 	= "";
										$_SESSION["refNumber"] 		= "";
								
										$_SESSION["transAmount"] 	= "";
										$_SESSION["exchangeRate"] 	= "";
										$_SESSION["exchangeID"] 	= "";
										$_SESSION["localAmount"] 	= "";
										$_SESSION["totalAmount"] 	= "";
										$_SESSION["IMFee"] 			= "";
										$_SESSION["bankCharges"] = "";	
										// resetting Session vars for trans_type Bank Transfer
										$_SESSION["bankName"] 		= "";
										$_SESSION["branchCode"] 	= "";
										$_SESSION["branchAddress"] 	= "";
										$_SESSION["swiftCode"] 		= "";
										$_SESSION["accNo"] 			= "";
										$_SESSION["ABACPF"] 		= "";
										$_SESSION["IBAN"] 			= "";
										$_SESSION["accountType"] = "";
										
										
										$_SESSION["cardType"]= "";
										$_SESSION["cardNo"]= "";
										$_SESSION["expiryDate"]= "";
										$_SESSION["cvvNo"]	= "";
										$_SESSION["pincode"] = "";
										
										$_SESSION["question"] 			= "";
										$_SESSION["answer"]     = "";	
										$_SESSION["tip"]     = "";	
										$_SESSION["currencyCharge"] = "";
										
										insertError("Transaction has been added successfully for processing and ".$systemPre." reference number is $imReferenceNumber");
										redirect("thanks.php?msg1=Y&success=Y");
								}
							
							/******************************************* Transaction Working ***********************************************/
		
		}
			
}
else
{
	
	///To Save the Transaction Before Being changed
	$oldTrans = selectFrom("select * from ".TBL_TRANSACTIONS." where transID='".$_POST["transID"]."'");
	
	$sql = "INSERT INTO ".TBL_AMENDED_TRANSACTIONS." (transID, customerID, benID, benAgentID, 
											custAgentID,  exchangeID, refNumber, transAmount, 
											exchangeRate, localAmount, IMFee, totalAmount,
											transactionPurpose, other_pur, fundSources, moneyPaid, 
											Declaration, addedBy, transDate, transType, 
											toCountry, fromCountry, refNumberIM, 
											currencyFrom, currencyTo, custAgentParentID, 
											benAgentParentID, collectionPointID, bankCharges,
											AgentComm,CommType,admincharges, cashCharges,
											question, answer,tip, outCurrCharges, 
											discountRequest, transStatus, verifiedBy,
											authorisedBy, cancelledBy, recalledBy, isSent,
											remarks, authoriseDate, deliveryOutDate, 
											deliveryDate,	cancelDate, rejectDate,
											failedDate, holdedBy,
											discountType, discounted_amount,
											modifiedBy, modificationDate, holdDate,
											verificationDate, unholdBy, unholdDate)
								VALUES   (  '".$oldTrans["transID"]."', '".$oldTrans["customerID"]."', '".$oldTrans["benID"]."', '".$oldTrans["benAgentID"]."', 
								'".$oldTrans["custAgentID"]."', '".$oldTrans["exchangeID"]."', '".$oldTrans["refNumber"]."', '".$oldTrans["transAmount"]."', 
								'".$oldTrans["exchangeRate"]."', '".$oldTrans["localAmount"]."', '".$oldTrans["IMFee"]."', '".$oldTrans["totalAmount"]."', 
								'".$oldTrans["transactionPurpose"]."','".$oldTrans["other_pur"]."', '".$oldTrans["fundSources"]."', '".$oldTrans["moneyPaid"]."', 
								'Y', '".$oldTrans["addedBy"]."', '".$oldTrans["transDate"]."', '".$oldTrans["transType"]."',
								'".$oldTrans["toCountry"]."',	'".$oldTrans["fromCountry"]."', '".$oldTrans["refNumberIM"]."',
								'".$oldTrans["currencyFrom"]."', '".$oldTrans["currencyTo"]."',	'".$oldTrans["custAgentParentID"]."', 
								'".$oldTrans["benAgentParentID"]."', '".$oldTrans["collectionPointID"]."','".$oldTrans["bankCharges"]."',
								'".$oldTrans["AgentComm"]."','".$oldTrans["CommType"]."','".$oldTrans["admincharges"]."', '".$oldTrans["cashCharges"]."',
								'".$oldTrans["question"]."','".$oldTrans["answer"]."','".$oldTrans["tip"]."','".$oldTrans["outCurrCharges"]."',
								'".$oldTrans["discountRequest"]."','".$oldTrans["transStatus"]."', '".$oldTrans["verifiedBy"]."',
								'".$oldTrans["authorisedBy"]."', '".$oldTrans["cancelledBy"]."', '".$oldTrans["recalledBy"]."','".$oldTrans["isSent"]."',
								'".$oldTrans["remarks"]."','".$oldTrans["authoriseDate"]."','".$oldTrans["deliveryOutDate"]."',
								'".$oldTrans["deliveryDate"]."','".$oldTrans["cancelDate"]."','".$oldTrans["rejectDate"]."',
								'".$oldTrans["failedDate"]."','".$oldTrans["holdedBy"]."',
								'".$oldTrans["discountType"]."','".$oldTrans["discounted_amount"]."',
								'$username', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$oldTrans["holdDate"]."',
								'".$oldTrans["verificationDate"]."','".$oldTrans["unholdBy"]."','".$oldTrans["unholdDate"]."')";
	
								insertInto($sql);
	
	
	if($_SESSION["act"]!="")
		{
		$status = $_SESSION["act"];
		$_SESSION["act"]="";
		}else 
			{
			$status = "Pending";
			}
$queryUpdate = 	"update ".TBL_TRANSACTIONS." set 
				customerID	='".$_POST["customerID"]."', 
				benID		='".$_POST["benID"]."',
				benAgentID	='$benAgentIDs',
				custAgentID ='$custAgentID',
				exchangeID	='".$_POST["exchangeID"]."',
				refNumber 	='".$_POST["refNumber"]."', 
				transAmount	='".$_POST["transAmount"]."', 
				exchangeRate='".$_POST["exchangeRate"]."', 
				localAmount	='".$_POST["localAmount"]."', 
				IMFee		='".$_POST["IMFee"]."',
				totalAmount	='".$_POST["totalAmount"]."', 
				transactionPurpose='".checkValues($_POST["transactionPurpose"])."',
				fundSources	='".checkValues($_POST["fundSources"])."',
				moneyPaid	='".$_POST["moneyPaid"]."', 
				transType	='".$_POST["transType"]."',
				toCountry= '".$_POST["benCountry"]."',
				fromCountry= '".$_POST["custCountry"]."',
				currencyFrom= '".$_POST["currencyFrom"]."', 
				transStatus = '$status',
				currencyTo= '".$_POST["currencyTo"]."', 
				custAgentParentID= '".$_POST["custAgentParentID"]."', 
				benAgentParentID= '".$_POST["benAgentParentID"]."',
				question = '".$_SESSION["question"]."',
				answer = '".$_SESSION["answer"]."',	
				tip = '".$_SESSION["tip"]."',
				outCurrCharges = '".$_SESSION["currencyCharge"]."'
				where transID='".$_POST["transID"]."'";
update($queryUpdate);

///Putting into History
$descript = "Transaction for online customer is updated";
activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["transID"],TBL_TRANSACTIONS,$descript);

$ref = selectFrom("select refNumberIM from transactions where transID='".$_POST["transID"]."'");

if(CONFIG_ONLINE_AGENT == '1')
{	$descript = "Transaction Amended";
	updateAgentAccount($oldTrans["custAgentID"], $oldTrans["totalAmount"], $ref["refNumberIM"], "DEPOSIT", $descript, 'Agent');	
	updateAgentAccount($custAgentID, $_POST["totalAmount"], $ref["refNumberIM"], "WITHDRAW", $descript, 'Agent');	
}


$account="update customer_account set 
		amount='".$_POST["totalAmount"]."',
		payment_mode = 'Transaction Amended'  
		where tranRefNo='".$ref["refNumberIM"]."'";
		update($account);

		$accountBank="update bank_account set 
		amount='".(DEST_CURR_IN_ACC_STMNTS == "1" ? $_POST["localAmount"] : $_POST["transAmount"])."',
		currency = '".(DEST_CURR_IN_ACC_STMNTS == "1" ? $_POST["currencyTo"] : $_POST["currencyFrom"])."',
		bankID = '$benAgentIDs', 
		modified_by = '$changedBy',
		description = 'Transaction Amended'
		where TransID='".$_POST["transID"]."'";
		update($accountBank);		

if($_SESSION["agentID_id"] != "")
{		
	$entryDate = date("Y-m-d");
	insertInto("insert into agent_logfile (agentID , transID  , entryDate , remarks,customerID ) 
	VALUES ('". $_SESSION["agentID_id"]."', '".$_POST["transID"]."', $entryDate,'UPDATION','".$_POST["customerID"]."')");		
}		
if(trim($_POST["transType"]) == "Bank Transfer")
{
	/*
	$sqlBank = "insert into cm_bankdetails
														( 
														benID, bankName, 
														accNo, branchCode, branchAddress,
														ABACPF , IBAN,swiftCode 
														)
														Values
														(
														'".$_POST["benID"]."',  '".$_POST["bankName"]."',
														'".$_POST["accNo"]."', '".$_POST["branchCode"]."', '".$_POST["branchAddress"]."',
														'".$_POST["ABACPF"]."',	'".$_POST["IBAN"]."',	'".$_POST["swiftCode"]."'
														)
														";
	*/
	$strQuery = "delete FROM cm_bankdetails where benID = '".$_POST["benID"]."'";
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 			
							
	$sqlBank = "insert into cm_bankdetails
														( 
														benID, bankName, 
														accNo, branchCode, branchAddress,
														ABACPF , IBAN, swiftCode, accountType 
														)
														Values
														(
														'".$_POST["benID"]."',  '".$_POST["bankName"]."',
														'".$_POST["accNo"]."', '".$_POST["branchCode"]."', '".$_POST["branchAddress"]."',
														'".$_POST["ABACPF"]."',	'".$_POST["IBAN"]."',	'".$_POST["swiftCode"]."', '".$_SESSION["accountType"]."'
														)
														";	
	insertInto($sqlBank);																			
							
	/*$queryUpdateBank = "update cm_bankdetails set 
						benID 		= '".$_POST["benID"]."', 
						bankName 	= '".$_POST["bankName"]."', 
						accNo		= '".$_POST["accNo"]."',
						branchCode	= '".$_POST["branchCode"]."', 
						branchAddress='".$_POST["branchAddress"]."',
						ABACPF 		= '".$_POST["ABACPF"]."',
						IBAN		= 	'".$_POST["IBAN"]."',
						swiftCode = '".$_POST["swiftCode"]."'
						where transID='".$_POST["transID"]."'";
	update($queryUpdateBank);*/
	
	
}
					$_SESSION["collectionPointID"] = "";
					$_SESSION["transTypes"] = "";
					$_SESSION["transType"] 		= "";
					$_SESSION["benAgentID"] 	= "";
					$_SESSION["customerID"] 	= "";
					$_SESSION["benID"] 			= "";
					$_SESSION["moneyPaid"] 		= "";
					$_SESSION["transactionPurpose"] = "";
					$_SESSION["fundSources"] 	= "";
					$_SESSION["refNumber"] 		= "";
					$_SESSION["tip"]     = "";	
					$_SESSION["currencyCharge"] = "";
			
					
					$_SESSION["transAmount"] 	= "";
					$_SESSION["exchangeRate"] 	= "";
					$_SESSION["exchangeID"] 	= "";
					$_SESSION["localAmount"] 	= "";
					$_SESSION["totalAmount"] 	= "";
					$_SESSION["IMFee"] 			= "";
					$_SESSION["bankCharges"] = "";	
					
					// resetting Session vars for trans_type Bank Transfer
					$_SESSION["bankName"] 		= "";
					$_SESSION["branchCode"] 	= "";
					$_SESSION["branchAddress"] 	= "";
					$_SESSION["swiftCode"] 		= "";
					$_SESSION["accNo"] 			= "";
					$_SESSION["ABACPF"] 		= "";
					$_SESSION["IBAN"] 			= "";
					$_SESSION["accountType"] = "";
					
					
					$_SESSION["cardType"]= "";
					$_SESSION["cardNo"]= "";
					$_SESSION["expiryDate"]= "";
					$_SESSION["cvvNo"]	= "";
					$_SESSION["pincode"] = "";
insertError("Transaction has been updated successfully.");
redirect("add-customer-transaction.php?msg1=Y&success=Y");
}
/*
transType : Bank Transfer
customerID : 2
benID : 2
benCountry : Pakistan

bankName : State BAnk
accNo : 123-132-123
branchCode : 123
branchAddress : asdafdasdasd
swiftCode : 
IBAN : 

refNumber : 123-123
transAmount : 500
exchangeRate : 58.8
exchangeID : 3
localAmount : 29400
IMFee : 20
totalAmount : 520
transactionPurpose : Family Assistant
fundSources : assad
moneyPaid : By Cash
Declaration : Y
*/
echo $str;
exit();

?>