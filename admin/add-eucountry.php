<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

if ($_POST["Submit"] == "Add") {
	
	if ($_POST["countries"] != "") {
		
		$i = 0;
		while ($_POST["countries"][$i] != "") {
			
			$qUpdate  = "UPDATE `cities` SET `countryRegion` = 'European' WHERE `country` = '".$_POST["countries"][$i]."'";
			update($qUpdate);
			$qUpdate2 = "UPDATE ".TBL_COUNTRY." SET `countryRegion` = 'European' WHERE `countryName` = '".$_POST["countries"][$i]."'";
			update($qUpdate2);
			$i++;
			
		}
	}
}

if ($_POST["Submit"] == "Delete") {
	
	if ($_POST["countriesDel"] != "") {
		
		$i = 0;
		while ($_POST["countriesDel"][$i] != "") {
			
			$qUpdate  = "UPDATE `cities` SET `countryRegion` = '' WHERE `country` = '".$_POST["countriesDel"][$i]."'";
			update($qUpdate);
			$qUpdate2 = "UPDATE ".TBL_COUNTRY." SET `countryRegion` = '' WHERE `countryName` = '".$_POST["countriesDel"][$i]."'";
			update($qUpdate2);
			$i++;
			
		}
	}
}

?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><b><strong><font class="topbar_tex">Add/Delete Countries for European Union</font></strong></b></td>
  </tr>
  <tr>
    <td>
	<table width="70%"  border="0">
  <tr>
    <td><fieldset>
    <legend class="style2">Add Countries for European Union</legend>
    <br>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			<form name="euCountry" action="add-eucountry.php" method="post">
			  <? if($msg!= "")
			  {
			  ?>
			  <tr align="center">
			    <td colspan="2" class="tab-r"><? echo $msg?><br></td>
			  </tr>
			  <?
			}
			  ?>
 			  <tr>
				  <td align="right" valign="top" bgcolor="#DFE6EA">Countries NOT in European Union</td>
			    <td bgcolor="#DFE6EA">
				<select name="countries[]" multiple size="10" >
				<option value="">-- Select Country --</option>
						  <?
							$strQuery = "SELECT DISTINCT(country) FROM  cities WHERE countryRegion != 'European' ORDER BY country ";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
							while($rstRow = mysql_fetch_array($nResult))
							{
								$Country = $rstRow["country"];	
								echo "<option  value ='$Country'> ".$Country."</option>";	
							}
						  ?>				
				</select>
				</td>
			  </tr>

			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">&nbsp;</td>
			    <td align="center" bgcolor="#DFE6EA"><input name="Submit" type="submit" value="Add"></td></tr></form>
			</table>
</fieldset>
<br><br>
<fieldset>
    <legend class="style2">Delete Countries from European Union</legend>
    <br>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			<form name="euCountry" action="add-eucountry.php" method="post">
 			  <tr>
				  <td align="right" valign="top" bgcolor="#DFE6EA">Countries in European Union</td>
			    <td bgcolor="#DFE6EA">
				<select name="countriesDel[]" multiple size="10" >
				<option value="">-- Select Country --</option>
						  <?
							$strQuery = "SELECT DISTINCT(country) FROM  cities WHERE countryRegion = 'European' ORDER BY country ";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
							while($rstRow = mysql_fetch_array($nResult))
							{
								$Country = $rstRow["country"];	
								echo "<option  value ='$Country'> ".$Country."</option>";	
							}
						  ?>				
				</select>
				</td>
			  </tr>

			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">&nbsp;</td>
			    <td align="center" bgcolor="#DFE6EA"><input name="Submit" type="submit" value="Delete"></td></tr></form>
			</table>
		    <br>
        </fieldset>        </td>
  </tr>
</table>
	</td>
  </tr>
</table>
</body>
</html>