<?php

// Session Handling
session_start();
///////////////////////History is maintained via method named 'activities'
//ini_set("display_errors","1");
//error_reporting("1");
// Including files
include ("../include/config.php");
include ("security.php");
require_once("lib/phpmailer/class.phpmailer.php");
require_once("lib/audit-functions.php");


//$agentType = getAgentType();
$where_sql="";
$agentIdForCust='';
if($_SESSION["loggedUserData"]['adminType']=='Agent')
{
	$agentIdForCust  = $_SESSION["loggedUserData"]["userID"];
	//$where_sql="   AND userID='".	$userID."' OR parentID= '".	$userID."' ";
}
$modifyby = $_SESSION["loggedUserData"]["userID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$payinAmount = CONFIG_PAYIN_FEE;
$agentType = getAgentType();
$tran_date = date("Y-m-d");
$tran_dateTime = getCountryTime(CONFIG_COUNTRY_CODE);
$chkBlock = $_POST["chkBlock"];
$remarks = $_POST["senderRemarks"];
$username = $_SESSION["loggedUserData"]["username"];

$city=$_POST['City'];
$bankName=$_POST['bankName'];
$accNumber=$_POST['account'];
$swiftCode=$_POST['swiftCode'];
$branchName=$_POST['branchName'];
$branchAddress=$_POST['branchAddress'];
$routingNumber=$_POST['routingNumber'];
$IBAN=$_POST['iban'];
$accountType=$_POST['accountType'];
$sortCode=$_POST['sortCode'];
$compRuleJSON=$_POST['compRuleJSON'];

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
}

if(CONFIG_CUSTOM_SENDER == '1')
{
	$customerPage = CONFIG_SENDER_PAGE;
}else{
	$customerPage = "add-customer.php";

}
if(defined("CONFIG_TRANS_TYPE_TT") && CONFIG_TRANS_TYPE_TT==1)
{
	$ttTransFlag=true;
}
switch(trim($_POST["Country"])){
	case "United States": $ccode = "US"; break;
//case "Canada": $ccode = "CA"; break;
	default:
		$countryCode = selectFrom("select countryCode from ".TBL_CITIES." where country='".trim($_POST["Country"])."'");
		$ccode = $countryCode["countryCode"];
		break;
}

if($_GET["r"] == 'dom')///If transaction is domestic
{
	$returnPage = 'add-transaction-domestic.php';
}else{
	$returnPage = $transactionPage;

}

/*	#5610 - Minas Centre
*	Member and Standard customer dropdown is added on form.
*	and M is prefixed to customerID for Membership customers.
*	by A.Shahid
*/
$senderTypeFlag = false;
if(defined("CONFIG_MEMBERSHIP_CUSTOMER_DROPDOWN") && CONFIG_MEMBERSHIP_CUSTOMER_DROPDOWN=="1")
	$senderTypeFlag = true;

$senderFullnameFlag = false;
if(defined("CONFIG_SENDER_FULLNAME_FOR_COMPANIES") && CONFIG_SENDER_FULLNAME_FOR_COMPANIES=="1"){
	$senderFullnameFlag = true;
}

/* @5957-premierexchange by Niaz Ahmad*/
$categoryFlag = false;
$SenderIdFlag =false;
if(CONFIG_CUSTOMER_CATEGORY	== "1")
	$categoryFlag = true;
if(CONFIG_SENDER_ID_EXPIRED_WAVIER == "1")
	$SenderIdFlag=true;
$_SESSION["agentID"] = $_POST["agentID"];
if($_GET["agentId2"] != "")
{
	$_SESSION["agentId2"] = $_GET["agentId2"];
}
$_SESSION["Title"] = $_POST["Title"];
$_SESSION["other_title"] = $_POST["other_title"];
$_SESSION["agentID"] = $_POST["agentID"];
$_SESSION["firstName"] = $_POST["firstName"];
$_SESSION["lastName"] = $_POST["lastName"];
$_SESSION["middleName"] = $_POST["middleName"];
$_SESSION["dobDay"] = $_POST["dobDay"];
$_SESSION["dobMonth"] = $_POST["dobMonth"];
$_SESSION["dobYear"] = $_POST["dobYear"];
$_SESSION["placeOfBirth"] = $_POST["placeOfBirth"];
$_SESSION["acceptedTerms"] = $_POST["acceptedTerms"];
$_SESSION["IDType"] = $_POST["IDType"];
$_SESSION["IDNumber"] = $_POST["IDNumber"];
$_SESSION["Otherid_name"] = $_POST["Otherid_name"];
$_SESSION["Otherid"] = $_POST["Otherid"];
$_SESSION["issuedBy"] = $_POST["issuedBy"];
$_SESSION["IDissuedate"] = $_POST["IDissuedate"];
$_SESSION["dDate"] = $_POST["dDate"];
$_SESSION["idDay"] = $_POST["idDay"];
$_SESSION["idMonth"] = $_POST["idMonth"];
$_SESSION["idYear"] = $_POST["idYear"];
$_SESSION["Address"] = $_POST["Address"];
$_SESSION["Address1"] = $_POST["Address1"];
$_SESSION["proveAddress"] = $_POST["proveAddress"];
$_SESSION["proveAddressType"] = $_POST["proveAddressType"];
$_SESSION["Country"] = $_POST["Country"];
$_SESSION["Countryaddress"] = $_POST["Countryaddress"];
$_SESSION["City"] = $_POST["City"];
$_SESSION["Zip"] = $_POST["Zip"];
$_SESSION["State"] = $_POST["State"];
$_SESSION["Phone"] = $_POST["Phone"];
$_SESSION["Mobile"] = $_POST["Mobile"];
$_SESSION["custEmail"] = $_POST["custEmail"];
$_SESSION["payinBook"] = $_POST["payinBook"];
$_SESSION["ch_payinBook"] = $_POST["ch_payinBook"];
$_SESSION["documentProvided"] = $_POST["documentProvided"];
$_SESSION["issueDay"] = $_POST["issueDay"];
$_SESSION["issueMonth"] = $_POST["issueMonth"];
$_SESSION["issueYear"] = $_POST["issueYear"];
$_SESSION["secMiddleName"] = $_POST["secMiddleName"];
$_SESSION["profession"] = $_POST["profession"];
$_SESSION["fillingNo"] = $_POST["fillingNo"];
$_SESSION["uploadImage"] = $_POST["uploadImage"];
$_SESSION["sonOf"] = $_POST["sonOf"];
$_SESSION["sonOfType"] = $_POST["sonOfType"];
$_SESSION["customerParentID"] = $_POST["customerParentID"];
$_SESSION["source"] = $_POST["source"];
$_SESSION["senderRemarks"] = $_POST["senderRemarks"];
$_SESSION["tradingAcccount"] = $_POST["tradingAcccount"];

if($senderTypeFlag)
	$_SESSION["senderType"] = $_POST["senderType"];
if($categoryFlag)
	$_SESSION["custCategory"] = $_POST["custCategory"];
//$_SESSION["from"]=$_GET["from"];
$uploadImageVar = $_POST["uploadImage"];
//debug($_REQUEST, true);
// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types

if ( is_array( $_REQUEST["IDTypesValuesData"] ) )
{
	$_SESSION["IDTypesValuesData"] = $_REQUEST["IDTypesValuesData"];
}

// End of code against #3299: MasterPayex - Multiple ID Types

if($_GET["from"]!=''){
	$from = $_GET["from"];
}elseif($_POST["from"]!=''){
	$from = $_POST["from"];
}

$DocID = $_POST["DocID"];

$docCount = $_POST["docCount"];

//checks if the customer already exists
$customerName = checkValues($_POST["firstName"]) . " " . checkValues($_POST["middleName"]) . " " . checkValues($_POST["lastName"]);
$field = "customerName,Mobile,IDNumber";
$values = $customerName.",".$_POST["Mobile"].",".$_POST["IDNumber"];
//$values = $customerName;
$tble = "customer";
$retValue = "accountName";



if(CONFIG_DCOUMNETS_PROVIDED_AS_DROPDOWN != "1"){
	if($_SESSION["documentProvided"] != 'Y')
	{
		$_SESSION["documentProvided"] = 'N';
	}
}

if (CONFIG_DISABLE_ACCEPTED_TERMS == "1") { // [by Jamshed]
	$_POST["acceptedTerms"] = "N";
}


$backURL = "$customerPage?msg=Y&from=".$from."&customerID=".$_POST["customerID"]."&pageID=".$_GET["pageID"]."&r=".$_GET["r"]."$alertID=".$_POST["alertID"];

if($_POST["submit"] == "Submit"){
	$arrListId = explode(",",$_POST["matchListID"]);
//debug($_POST["matchListID"],true);
	if($_POST["chkBlock"]!= ''){
		$chk= $_POST["chkBlock"];

		if(count($arrListId) > 0){
			for($k=0;$k<count($arrListId);$k++)
			{
				if(!empty($arrListId[$k])){
					$Querry_Sqls = "update ".TBL_COMPLIANCE_MATCH_LIST." 
						  set
							  isBlocked = '".$_POST["chkBlock"]."',
							  remarks = '".$_POST["remarks"]."'
						  where 
								matchListID = ".$arrListId[$k]."
						   ";

					update($Querry_Sqls);
				}
			}
		}
		$compFlag =false;

		insertError($_POST["message"]);

		$backURL = "add-customer.php?msg=Y&from=".$_GET["from"]."&customerID=".$_POST["customerID"]."&pageID=".$_GET["pageID"]."&r=".$_GET["r"]."$alertID=".$_POST["alertID"];

		///////////////////////////////////////////////////////////
		//Asad
//		echo "Block Checked<br>";
//		echo $compRuleJSON."<br>";

		if($chkBlock!= "")
		{


			/**
			 * #10461: Premier FX-Online module customer management
			 * Short Description
			 * 'custCategory' field is add in query
			 * @Author: Mirza Arslan Baig
			 */
			$Querry_Sqls = "INSERT INTO ".TBL_CUSTOMER." (accountName, password, agentID, Title,other_title, firstName, middleName, secMiddleName, lastName, Address, Address1, proveAddress, Country, City, Zip, State, Phone, Mobile, email, dob,placeOfBirth, acceptedTerms, IDType, IDNumber, IDExpiry,IDissuedate, issuedBy, documentProvided, otherId, otherId_name,customerNumber, payinBook, balance, created, customerName,profession,fillingNo, passport_number,proveAddType,SOF,SOF_Type".$insertIntoKeys.", custCategory,countryOfResidence,online_customer_services, EnabledFromNew, customerStatus) VALUES 
	('$customerCode', '$password', '$agentIds', '".$_POST["Title"]."','".$_POST["other_title"]."', '".checkValues($_POST["firstName"])."', '".checkValues($_POST["middleName"])."', '".checkValues($_POST["secMiddleName"])."' ,'".checkValues($_POST["lastName"])."', '".checkValues($_POST["Address"])."', '".checkValues($_POST["Address1"])."', '".($_POST["proveAddress"] != "" ? $_POST["proveAddress"] : "N")."', '".$_POST["Country"]."', '".checkValues($_POST["City"])."', '".checkValues($_POST["Zip"])."', '".checkValues($_POST["State"])."', '".$phone."', '".$mobile."', '".checkValues($_POST["custEmail"])."', '".$_POST["dobYear"]."-".$_POST["dobMonth"]."-".$_POST["dobDay"]."','".$_POST["placeOfBirth"]."','".$_POST["acceptedTerms"]."','".$_POST["IDType"]."', '".checkValues($_POST["IDNumber"])."', '".$idExpiryDate."','".$idIssueDate."', '".checkValues($_POST["issuedBy"])."', '".checkValues($_SESSION["documentProvided"])."',  '" . $_POST["Otherid"] . "','" . $_POST["Otherid_name"] . "','$customerNumber_value','".$_POST["payinBook"]."', '".$balance."','".$tran_date."', '".$customerName."','".$_POST["profession"]."','".$_POST["fillingNo"]."','".$_POST["passportValidate"]."','".$_POST["proveAddressType"]."','".$_POST["sonOf"]."','".$_POST["sonOfType"]."'".$insertIntoValues.", 'W','".$_POST["Countryaddress"]."','TC',0, 'Disable')";

		}
		else
		{

			/**
			 * #10461: Premier FX-Online module customer management
			 * Short Description
			 * 'custCategory' field is add in query
			 * @Author: Mirza Arslan Baig
			 */

			$Querry_Sqls = "INSERT INTO ".TBL_CUSTOMER." (accountName, password, agentID, Title,other_title, firstName, middleName, secMiddleName, lastName, Address, Address1, proveAddress, Country, City, Zip, State, Phone, Mobile, email, dob,placeOfBirth, acceptedTerms, IDType, IDNumber, IDExpiry,IDissuedate, issuedBy, documentProvided, otherId, otherId_name,customerNumber, payinBook, balance, created, customerName,profession,fillingNo, passport_number,proveAddType,SOF,SOF_Type".$insertIntoKeys.", custCategory,countryOfResidence,online_customer_services,  EnabledFromNew, customerStatus ) VALUES 
	('$customerCode', '$password', '$agentIds', '".$_POST["Title"]."','".$_POST["other_title"]."', '".checkValues($_POST["firstName"])."', '".checkValues($_POST["middleName"])."', '".checkValues($_POST["secMiddleName"])."' ,'".checkValues($_POST["lastName"])."', '".checkValues($_POST["Address"])."', '".checkValues($_POST["Address1"])."', '".($_POST["proveAddress"] != "" ? $_POST["proveAddress"] : "N")."', '".$_POST["Country"]."', '".checkValues($_POST["City"])."', '".checkValues($_POST["Zip"])."', '".checkValues($_POST["State"])."', '".$phone."', '".$mobile."', '".checkValues($_POST["custEmail"])."', '".$_POST["dobYear"]."-".$_POST["dobMonth"]."-".$_POST["dobDay"]."','".$_POST["placeOfBirth"]."','".$_POST["acceptedTerms"]."','".$_POST["IDType"]."', '".checkValues($_POST["IDNumber"])."', '".$idExpiryDate."','".$idIssueDate."', '".checkValues($_POST["issuedBy"])."', '".checkValues($_SESSION["documentProvided"])."',  '" . $_POST["Otherid"] . "','" . $_POST["Otherid_name"] . "','$customerNumber_value','".$_POST["payinBook"]."', '".$balance."','".$tran_date."', '".$customerName."','".$_POST["profession"]."','".$_POST["fillingNo"]."','".$_POST["passportValidate"]."','".$_POST["proveAddressType"]."','".$_POST["sonOf"]."','".$_POST["sonOfType"]."'".$insertIntoValues.", 'W','".$_POST["Countryaddress"]."','TC',0, 'Enable')";
		}

		//debug($Querry_Sqls);
		die("hello");

		//debug($Querry_Sqls,true);
		insertInto($Querry_Sqls);

		//mail:debug($Querry_Sqls);
		$customerID = @mysql_insert_id();

		$timestamp = date("Y-m-d H:i:s");

		insertInto("INSERT INTO compliance_activities_logs (comp_user_id, comp_source_table, compRuleJSON, created_on) values ($customerID, 'Customer', '$compRuleJSON', '$timestamp')");

		///////////////////////////////////////////////////////////

		redirect($backURL);

	}else{
		$chk= 'N';

		if(count($arrListId) > 0){
			for($k=0;$k<count($arrListId);$k++)
			{
				if(!empty($arrListId[$k])){
					$Querry_Sqls = "update ".TBL_COMPLIANCE_MATCH_LIST." 
						  set
							  isBlocked = '".$chk."',
							  remarks = '".$_POST["remarks"]."'
						  where 
								matchListID = ".$arrListId[$k]."
						   ";

					update($Querry_Sqls);
				}
			}
		}

		$compFlag =true;
		if(!empty($Querry_Sqls))
			insertInto($Querry_Sqls);

	}

}


// This config add by Niaz Ahmad for multiple id @4961 prime currency

if(CONFIG_SEARCH_EXISTING_DATA_CUSTOMIZE == "1"){
	$customerName = checkValues($_POST["firstName"]) ." " . checkValues($_POST["lastName"]);
	$dob =  $_POST["dobYear"]."-".$_POST["dobMonth"]."-".$_POST["dobDay"];
	$field = "customerName,dob";
	$values = $customerName.",".$dob;
	$tble = "customer";
	$retValue = "accountName";
	$arrMultiID = $_REQUEST["IDTypesValuesData"];
}

if(CONFIG_SEARCH_EXISTING_DATA == '1'){

	$var = checkExistence($field,$values,$tble,$retValue,$arrMultiID);

	if($var != "" && $_POST["customerID"] == ""){  //checks whether the customer already exists or not
		insertError(CU22." ".$var." ".CU23);
		//redirect($backURL);

	}
}

if(CONFIG_ADD_REF_NUMBER=="1" && CONFIG_UPDATE_CUSTOMER_NUMBER == "1" && $_POST["accountName"]!=""){
	$queryAccountName = "SELECT customerID from ".TBL_CUSTOMER." WHERE accountName='".trim($_POST["accountName"])."' AND customerID!='".$_POST["customerID"]."' ";
	$arrAccountName = selectMultiRecords($queryAccountName);
	if(count($arrAccountName)>0){
		insertError(CU22." Number ".$_POST["accountName"]." ".CU23);
		redirect($backURL);
	}
}


if(trim($_POST["agentID"]) != "")
{
	$agentIds=$_POST["agentID"];
}
else if(trim($_SESSION["agentId2"]) != "")
{
	$agentIds=$_SESSION["agentId2"];
}
/*else if(trim($_GET["agnetID"]) != "")
{
 $agentIds=$__GET["agnetID"];
}*/

//die("asdad");
/*
This config added by Niaz Ahmad at 07-03-2009 @4630
At add sender page select agent in non mandatory for globalexchange
*/
if(CONFIG_ADD_CUSTOMER_AGENT_NO_MANDATORY !="1"){
	if($agentIds == "")
	{

		if(CONFIG_ENABLE_DEFAULT_AGENT == '1')
		{
			$agentIds = '3';
		}
		elseif($_REQUEST["ch_payinBook"] != "N")
		{

			insertError(CU1);
//			redirect($backURL);
		}
	}
}
if (trim($_POST["firstName"]) == ""){
	insertError(CU2);
	redirect($backURL);
}
if (trim($_POST["lastName"]) == ""){
	insertError(CU3);
	redirect($backURL);
}


if(empty($agentIds))
{

	$agentIds=$agentIdForCust;
}


/*if (trim($_POST["Address"]) == ""){

    insertError(CU5);
    redirect($backURL);
}*/
if (CONFIG_FREETEXT_IDFIELDS == '1') {

	$maxFutureYear = date("Y") + CONFIG_IDEXPIRY_YEAR_DELTA;
	if ($_POST["dDate"] != "") {
		$dyear = substr($_POST["dDate"],6,4);
		// Pass an array of date into this function and it returns true or false...
		if (isValidDate($_POST["dDate"])) {
			if ($dyear <= $maxFutureYear) {
				$idExpiryDate = substr($_POST["dDate"],6,4) . "-" . substr($_POST["dDate"],3,2)	. "-" . substr($_POST["dDate"],0,2);
				if ($idExpiryDate <= date("Y-m-d")) {
					insertError(AG44);
					redirect($backURL);
				}
			} else {
				insertError("ID expiry year should not be greater than " . $maxFutureYear . ". If so, then contact to your admin.");
				redirect($backURL);
			}
		} else {
			insertError("Invalid Date Format");
			redirect($backURL);
		}
	} else {
		$idExpiryDate = "";
	}
} else if (CONFIG_IDEXPIRY_WITH_CALENDAR == "1") {
	if ($_POST["dDate"] != "") {
		//$dDate = explode("/", $_POST["dDate"]);
		$idExpiryDate = substr($_POST["dDate"],6,4) . "-" . substr($_POST["dDate"],3,2)	. "-" . substr($_POST["dDate"],0,2);
		if ($idExpiryDate <= date("Y-m-d")) {
			insertError(AG44);
			redirect($backURL);
		}
	} else {
		$idExpiryDate = "";
	}
} else {

	if($_POST["idYear"] != "" && $_POST["idMonth"]!= "" && $_POST["idDay"]!= "")
	{
		$idExpiryDate = $_POST["idYear"] . "-" . $_POST["idMonth"] . "-" . $_POST["idDay"];
	}
	if($idExpiryDate  != ""){
		if ($idExpiryDate < date("Y-m-d")) {


			insertError(AG44);
			redirect($backURL);

		}}
	else {
		$idExpiryDate = $_POST["idYear"] . "-" . $_POST["idMonth"] . "-" . $_POST["idDay"];
	}
}




if ($_POST["IDissuedate"] != "") {
	$maxPastYear = date("Y") - CONFIG_IDEXPIRY_YEAR_DELTA;
	$dyear = substr($_POST["IDissuedate"],6,4);
	//$dDate = explode("-", $_POST["IDissuedate"]);

	// Pass an array of date into this function and it returns true or false...
	if (isValidDate($_POST["IDissuedate"])) {

		$idIssueDate = substr($_POST["IDissuedate"],6,4) . "-" . substr($_POST["IDissuedate"],3,2)	. "-" . substr($_POST["IDissuedate"],0,2);
		if ($idIssueDate > date("Y-m-d")) {
			insertError(AG55);
			redirect($backURL);
		}
	} else {
		insertError("Invalid Date Format");
		redirect($backURL);
	}

} else{

	//	$dDate = explode("/", $_POST["IDexpirydate"]);
	$idIssueDate = $_POST["issueYear"] . "-" . $_POST["issueMonth"] . "-" . $_POST["issueDay"];
	if ($idIssueDate > date("Y-m-d")) {

		insertError(AG55);
		redirect($backURL);
	}else {
		$idIssueDate = $_POST["issueYear"] . "-" . $_POST["issueMonth"] . "-" . $_POST["issueDay"];
	}
}

if (trim($_POST["Country"]) == ""){
	insertError(CU6);
	redirect($backURL);
}
/*
if (trim($_POST["State"]) == ""){
        insertError("Please provide customer's ".(CONFIG_STATE_NAME!="CONFIG_STATE_NAME"?CONFIG_STATE_NAME:"County"));
        redirect($backURL);
}*/
if (trim($_POST["City"]) == "" && CONFIG_CITY_NON_COMPULSORY != '1'){

	insertError(CU7);
	redirect($backURL);
}


if (CONFIG_COMPUL_FOR_AGENTLOGINS == '1' && ($agentType == 'SUBA' || $agentType == 'SUBAI' || $agentType == 'SUPA' || $agentType == 'SUPAI')) {
	if (trim($_POST["Address"]) == ""){


		insertError("Please provide the customer's address.");
		redirect($backURL);
	}
	if (trim($_POST["Zip"]) == ""){

		insertError("Please provide the customer's Zip/Postal Code.");
		redirect($backURL);
	}
}

if (trim($_POST["IDType"]) == "" && CONFIG_ID_MANDATORY == '1') {

	insertError(CU19);
	redirect($backURL);
}

if (CONFIG_COMPULSORY_MOBILE != '0' && (trim($_POST["Phone"]) == ""  && trim($_POST["Mobile"]) == "" )){

	insertError(CU8);
	redirect($backURL);
}

if ( ( !defined("CONFIG_PHONES_AS_DROPDOWN") || CONFIG_PHONES_AS_DROPDOWN != 1 )
	&& ( CONFIG_COMPULSORY_MOBILE== '1' && trim($_POST["Mobile"]) == "" ) )
{
	insertError(CU20);
	redirect($backURL);
}

if (CONFIG_PROVE_ADDRESS_MANDATORY == '1' && trim($_POST["proveAddress"]) == "" )
{
	insertError(CU24);
	redirect($backURL);
}
if(CONFIG_CUST_DOC_FUN != '1')
{
	if (CONFIG_DOCUMENTS_PROVIDED_MANDATORY == '1' && trim($_POST["documentProvided"]) == "" )
	{
		insertError(CU25);
		redirect($backURL);
	}
}

if (CONFIG_SENDER_PHONE_MANDATORY_ONLY_UK== '1')
{
	if($_POST["Country"] == "United Kingdom" && trim($_POST["Mobile"]) != "" ){
		$mobileNumber=$_POST["Mobile"];

		if(defined(CONFIG_MOBILE_FORMAT) && CONFIG_MOBILE_FORMAT != "")
		{
			$number = explode(CONFIG_MOBILE_FORMAT ,$mobileNumber);
		}

		if(is_array($number) && count($number) == '3' )
		{
			if( strlen($number[0])!= '4' || strlen($number[1]) != '3'|| strlen($number[2]) != '4')
			{
				insertError(CU21);
				redirect($backURL);
			}
		}
		else if( ( !defined("CONFIG_PHONES_AS_DROPDOWN") || CONFIG_PHONES_AS_DROPDOWN != 1 )
			&& ( strlen($mobileNumber) != '11' || !is_numeric($mobileNumber) ) )
		{
			insertError(CU21);
			redirect($backURL);

		}
	}
}elseif (CONFIG_COMPULSORY_MOBILE== '1' && trim($_POST["Mobile"]) != "" )
{
	$mobileNumber=$_POST["Mobile"];

	if(defined(CONFIG_MOBILE_FORMAT) && CONFIG_MOBILE_FORMAT != "")
	{
		$number = explode(CONFIG_MOBILE_FORMAT ,$mobileNumber);
	}

	if(is_array($number) && count($number) == '3' )
	{
		if( strlen($number[0])!= '4' || strlen($number[1]) != '3'|| strlen($number[2]) != '4')
		{
			insertError(CU21);
			redirect($backURL);
		}
	}
	else if( ( !defined("CONFIG_PHONES_AS_DROPDOWN") || CONFIG_PHONES_AS_DROPDOWN != 1 )
		&& ( strlen($mobileNumber) != '11' || !is_numeric($mobileNumber) ) )
	{
		insertError(CU21);
		redirect($backURL);

	}
}


if (trim($_POST["ch_payinBook"]) != ""){

	if (trim($_POST["payinBook"]) == "" && CONFIG_AUTO_PAYIN_NUMBER != "1" && CONFIG_PAYIN_CUSTOMER != '2'){
		insertError(CU14);
//		redirect($backURL);
	}
}
if (trim($_POST["payinBook"]) != ""){

	if (trim($_POST["ch_payinBook"]) == ""){
		insertError(CU13);
//		redirect($backURL);
	}
	if(CONFIG_PAYIN_CUSTOMER == '1' && CONFIG_AUTO_PAYIN_NUMBER != '1')
	{
		if (!is_numeric($_POST["payinBook"])){
			insertError(CU12);
			redirect($backURL);
		}
		$payinBookNumber = strlen(trim($_POST["payinBook"]));
		if ($payinBookNumber < CONFIG_PAYIN_NUMBER ){
			insertError(CU15." ".CONFIG_PAYIN_NUMBER);
			redirect($backURL);
		}
		$payinBookNumber = strlen(trim($_POST["payinBook"]));
		if ($payinBookNumber > CONFIG_PAYIN_NUMBER ){
			insertError(CU16." ".CONFIG_PAYIN_NUMBER);
			redirect($backURL);
		}
	}
}

if (trim($_POST["ch_payinBook"]) == ""){
	if(trim($_POST["agentID"]) != "")
	{
		$agentIds=$_POST["agentID"];
	}
	else if(trim($_SESSION["agentId2"]) != "")
	{
		$agentIds=$_SESSION["agentId2"];
	}
	/*
    This config added by Niaz Ahmad at 07-03-2009 @4630
    At add sender page select agent in non mandatory for globalexchange
    */
	if(CONFIG_ADD_CUSTOMER_AGENT_NO_MANDATORY !="1"	){
		if($agentIds == "")
		{
			insertError(CU1);
//			redirect($backURL);
		}
	}
}
if (CONFIG_PAYIN_CUSTOMER != '2') {
	if ($_POST["customerID"] == ""){
		if ($_POST["payinBook"] != ""){
			//echo $_POST["payinBook"];
			if (isExist("select accountName from ".TBL_CUSTOMER." where accountName = '".$_POST["payinBook"]."'")){
				insertError(AG35);
				redirect($backURL);

			}
		}
	} else {
		if ($_POST["payinBook"] != ""){
			if (isExist("select accountName from ".TBL_CUSTOMER." where accountName = '".$_POST["payinBook"]."' and customerID != '".$_POST["customerID"]."'")){
				insertError(AG35);
				//$backURL = "$customerPage?msg=Y&from=" . $_GET["from"]."&customerID=".$_POST["customerID"];
				redirect($backURL);
			}
		}
	}
}

// for page redirection to other pages

if(isset($_GET['page']) && $_GET['page'] == '1'){
	$_SESSION['page'] = '1';
}elseif(isset($_GET['page']) && $_GET['page'] == '2'){
	$_SESSION['page'] = '2';
}

if(CONFIG_LINK_AGENT_SENDER_COUNTRY == '1'){

	$agentCountryQuery = selectFrom("select agentCountry from ".TBL_ADMIN_USERS." where userID = '".$agentIds."'");

	if($agentCountryQuery["agentCountry"] != $_POST["Country"]){
		insertError("Agent and sender should be of the same country.");
		redirect($backURL);
	}
}

//debug($agentIds);
/**
 * If this config is on than at add sender page, if sender is selected from the top than this sender will be saved against the sender
 * else if no sender is selected than default sender from CONFIG_PAYIN_AGENT_NUMBER will be assigned provided if payin book check box is
 * selected from down.
 * else exception will be thorugh to select the agent
 * @Ticket #3790
 */
/*
This config (CONFIG_ADD_CUSTOMER_AGENT_NO_MANDATORY) added by Niaz Ahmad at 07-03-2009 @4630
At add sender page select agent in non mandatory for globalexchange
*/
if(CONFIG_ADD_CUSTOMER_AGENT_NO_MANDATORY !="1"){
	if(CONFIG_USE_AGENT_AT_ADD_SENDER == "1")
	{
		if(empty($agentIds) && empty($_REQUEST["ch_payinBook"]))
		{
			insertError("Please select agent from top of form.");
//			redirect($backURL);
		}
		elseif(CONFIG_PAYIN_CUST_AGENT != "1")
		{
			insertError("Please select agent from top of form.");
//			redirect($backURL);
		}
	}

	/**
	 * Changing the logic such that when "Use Customer Account" is checked than customer should be payin book otherwise not
	 * also if "Use Customer Account" checked than customer agent should be from CONFIG_PAYIN_AGENT_NUMBER rahter than selected from top
	 * NOW also CONFIG_PAYIN_CUST_AGENT_ALL or CONFIG_PAYIN_AGENT_NUMBER should be used.
	 */
	if(!empty($_REQUEST["ch_payinBook"]) && CONFIG_PAYIN_CUST_AGENT == "1" && CONFIG_PAYIN_CUST_AGENT_ALL!="1")
	{
		$agentIds = CONFIG_PAYIN_AGENT_NUMBER;
	}


}
//debug($agentIds, true);

if($_GET["cmpCheck"] != 'Y')
{

	$compFlag = true;
	// compliance logic (Niaz)
	$queryExact="select * from ".TBL_COMPLIANCE_LOGIC." where userType ='sender' and isEnable ='Y' and applyAt='User Creation' and matchCriteria = 'Exact Match' ";
	$compExactMatch=selectMultiRecords($queryExact);

	$queryPartial="select * from ".TBL_COMPLIANCE_LOGIC." where userType ='sender' and isEnable ='Y' and applyAt='User Creation' and matchCriteria = 'Partial Match' ";
	$compPartialMatch=selectMultiRecords($queryPartial);

	$firstName = trim($_POST["firstName"]);
	$middleName = trim($_POST["middleName"]);
	$lastName = trim($_POST["lastName"]);

	$firstName = str_replace("'", "", $firstName);
	$middleName = str_replace("'", "", $middleName);
	$lastName = str_replace("'", "", $lastName);

	$firstName = stripslashes($firstName);
	$middleName = stripslashes($middleName);
	$lastName = stripslashes($lastName);

	if($firstName !='' &&  $middleName!='' &&  $lastName!=''){
		$full_name = $firstName.",".$middleName.",".$lastName;
	}elseif($firstName !='' &&  $middleName!=''){
		$full_name = $firstName.",".$middleName;
	}elseif($firstName !='' &&  $lastName!=''){
		$full_name = $firstName.",".$lastName;
	}elseif($middleName!='' &&  $lastName){
		$full_name = $middleName.",".$lastName;
	}elseif($firstName !=''){
		$full_name = $firstName;
	}elseif($middleName!=''){
		$full_name = $middleName;
	}elseif($lastName!=''){
		$full_name = $lastName;
	}
	/////////////exact match /////////////////////////////

	$k=0;

	$flag="";
	while(($k < count($compExactMatch)) &&  $compFlag){
		$flag="Exact";

		$queryListName = selectFrom("select listID,listName from ".TBL_COMPLIANCE_LIST." where listID= '".$compExactMatch[$k]["listID"]."' ");

		if(trim($queryListName["listName"]) == 'OFAC')
		{
			$queryExactData = "select compliancePersonID as id,listID from ".TBL_COMPLIANCE_PERSON." where listID= '".$compExactMatch[$k]["listID"]."' ";

			if($firstName!=''){
				$queryExactData .= " and (firstName ='".$firstName."')";
			}
			if($middleName!=''){
				$queryExactData .= " and (middleName ='".$middleName."')";
			}
			if($lastName!=''){
				$queryExactData .= " and (lastName ='".$lastName."')";
			}

			$rs_exact = mysql_query($queryExactData);
			$compExactSubRecord = mysql_num_rows($rs_exact);

			if($compExactSubRecord < 1 )
			{
				$queryExactData = "select alt_id,compliancePersonID,alt_name,listID from ".TBL_COMPLIANCE_OFAC_ALT." 
				                    where listID= '".$compExactMatch[$k]["listID"]."' ";

				if($firstName!='' || $middleName !='' || $lastName!=''){
					$queryExactData .= " and  alt_name = '".$full_name."'";
				}

			}
		}
		if($queryListName["listName"] == 'payex'){
			$queryExactData = "select compliancePersonID as id,listID  from ".TBL_COMPLIANCE_PERSON."
				     where listID= '".$compExactMatch[$k]["listID"]."'";

			if($firstName!=''){
				$queryExactData .= " and (firstName ='".$firstName."')";
			}
			if($middleName!=''){
				$queryExactData .= " and (middleName ='".$middleName."')";
			}
			if($lastName!=''){
				$queryExactData .= " and (lastName ='".$lastName."')";
			}
		}

		if($queryListName["listName"] == 'PEP'){
			if($firstName !='' &&  $middleName!='' &&  $lastName!=''){
				$full_name_pep = $firstName." ".$middleName." ".$lastName;
			}elseif($firstName !='' &&  $middleName!=''){
				$full_name_pep = $firstName." ".$middleName;
			}elseif($firstName !='' &&  $lastName!=''){
				$full_name_pep = $firstName." ".$lastName;
			}elseif($middleName!='' &&  $lastName){
				$full_name_pep = $middleName." ".$lastName;
			}elseif($firstName !=''){
				$full_name_pep = $firstName;
			}elseif($middleName!=''){
				$full_name_pep = $middleName;
			}elseif($lastName!=''){
				$full_name_pep = $lastName;
			}
			$queryExactData = "select comp_PEPID as id,listID,fullName,position,portfolio from compliancePEP
				     where listID= '".$compExactMatch[$k]["listID"]."' ";

			if($firstName!='' || $middleName !='' || $lastName !=''){
				$queryExactData .= " and  fullName =('".trim($full_name_pep)."')";
			}
		}
		if($queryListName["listName"] == 'HM Treasury'){
			$queryExactData = "select hm_id as id,list_id as listID,full_name from compliance_hm_treasury
				where list_id= '".$compExactMatch[$k]["listID"]."' ";

			/*if($firstName !='' &&  $middleName!='' &&  $lastName!=''){
		  	    $full_name = $lastName.",".$firstName.",".$middleName;
		  	  }elseif($firstName !='' &&  $lastName!=''){
		  	  	$full_name = $lastName.",".$firstName;
		  	  }elseif($middleName!='' &&  $lastName){
		  	  	$full_name = $lastName.",".$middleName;
		  	   }*/

			if($firstName!='' || $middleName !='' || $lastName !=''){
				$queryExactData .= " and  full_name =('".trim($full_name)."')";
				$queryExactData .=" AND group_type = 'Individual' " ;
			}
		}
		$result = mysql_query($queryExactData);
		if($compPersonRecord = mysql_fetch_array($result))
		{
			// check auto block
			if($compExactMatch[$k]["autoBlock"] == 'Y')
			{
				//Entry in the Data Base[complianceMatchList] and exit?
				$Querry_Sqls="insert into ".TBL_COMPLIANCE_MATCH_LIST." (compliancePersonID,listID,isFullMatch,matchUserType,isBlocked,enteredBy,enterByID,dated) values ('".$compPersonRecord["id"]."','".$compPersonRecord["listID"]."','Y','sender','Y','".$agentType."',".$modifyby.",'".$tran_dateTime."')";

				insertInto($Querry_Sqls);
				$compFlag = false;
//				insertError($compExactMatch[$k]["message"]);
				//$backURL = "add-beneficiary.php?notEndSessions=Y&from=".$from."&benID=".$_POST["benID"]."&msg=Y";

				///////////////////////////////////////////////////////////
				//Asad
//				if($chkBlock!= "")
//				{


					/**
					 * #10461: Premier FX-Online module customer management
					 * Short Description
					 * 'custCategory' field is add in query
					 * @Author: Mirza Arslan Baig
					 */
				$customerNumber = selectFrom("select MAX(customerNumber) from ".TBL_CUSTOMER." where 1 ");
				$customerNumber_value = $customerNumber[0]+1;
					$Querry_Sqls = "INSERT INTO ".TBL_CUSTOMER." (accountName, password, agentID, Title,other_title, firstName, middleName, secMiddleName, lastName, Address, Address1, proveAddress, Country, City, Zip, State, Phone, Mobile, email, dob,placeOfBirth, acceptedTerms, IDType, IDNumber, IDExpiry,IDissuedate, issuedBy, documentProvided, otherId, otherId_name,customerNumber, payinBook, balance, created, customerName,profession,fillingNo, passport_number,proveAddType,SOF,SOF_Type".$insertIntoKeys.", custCategory,countryOfResidence,online_customer_services, EnabledFromNew, customerStatus) VALUES 
	('$customerCode', '$password', '$agentIds', '".$_POST["Title"]."','".$_POST["other_title"]."', '".checkValues($_POST["firstName"])."', '".checkValues($_POST["middleName"])."', '".checkValues($_POST["secMiddleName"])."' ,'".checkValues($_POST["lastName"])."', '".checkValues($_POST["Address"])."', '".checkValues($_POST["Address1"])."', '".($_POST["proveAddress"] != "" ? $_POST["proveAddress"] : "N")."', '".$_POST["Country"]."', '".checkValues($_POST["City"])."', '".checkValues($_POST["Zip"])."', '".checkValues($_POST["State"])."', '".$phone."', '".$mobile."', '".checkValues($_POST["custEmail"])."', '".$_POST["dobYear"]."-".$_POST["dobMonth"]."-".$_POST["dobDay"]."','".$_POST["placeOfBirth"]."','".$_POST["acceptedTerms"]."','".$_POST["IDType"]."', '".checkValues($_POST["IDNumber"])."', '".$idExpiryDate."','".$idIssueDate."', '".checkValues($_POST["issuedBy"])."', '".checkValues($_SESSION["documentProvided"])."',  '" . $_POST["Otherid"] . "','" . $_POST["Otherid_name"] . "','$customerNumber_value','".$_POST["payinBook"]."', '".$balance."','".$tran_date."', '".$customerName."','".$_POST["profession"]."','".$_POST["fillingNo"]."','".$_POST["passportValidate"]."','".$_POST["proveAddressType"]."','".$_POST["sonOf"]."','".$_POST["sonOfType"]."'".$insertIntoValues.", 'W','".$_POST["Countryaddress"]."','TC',0, 'Disable')";

//				}
//				else
//				{

					/**
					 * #10461: Premier FX-Online module customer management
					 * Short Description
					 * 'custCategory' field is add in query
					 * @Author: Mirza Arslan Baig
					 */

//					$Querry_Sqls = "INSERT INTO ".TBL_CUSTOMER." (accountName, password, agentID, Title,other_title, firstName, middleName, secMiddleName, lastName, Address, Address1, proveAddress, Country, City, Zip, State, Phone, Mobile, email, dob,placeOfBirth, acceptedTerms, IDType, IDNumber, IDExpiry,IDissuedate, issuedBy, documentProvided, otherId, otherId_name,customerNumber, payinBook, balance, created, customerName,profession,fillingNo, passport_number,proveAddType,SOF,SOF_Type".$insertIntoKeys.", custCategory,countryOfResidence,online_customer_services,  EnabledFromNew, customerStatus ) VALUES
//	('$customerCode', '$password', '$agentIds', '".$_POST["Title"]."','".$_POST["other_title"]."', '".checkValues($_POST["firstName"])."', '".checkValues($_POST["middleName"])."', '".checkValues($_POST["secMiddleName"])."' ,'".checkValues($_POST["lastName"])."', '".checkValues($_POST["Address"])."', '".checkValues($_POST["Address1"])."', '".($_POST["proveAddress"] != "" ? $_POST["proveAddress"] : "N")."', '".$_POST["Country"]."', '".checkValues($_POST["City"])."', '".checkValues($_POST["Zip"])."', '".checkValues($_POST["State"])."', '".$phone."', '".$mobile."', '".checkValues($_POST["custEmail"])."', '".$_POST["dobYear"]."-".$_POST["dobMonth"]."-".$_POST["dobDay"]."','".$_POST["placeOfBirth"]."','".$_POST["acceptedTerms"]."','".$_POST["IDType"]."', '".checkValues($_POST["IDNumber"])."', '".$idExpiryDate."','".$idIssueDate."', '".checkValues($_POST["issuedBy"])."', '".checkValues($_SESSION["documentProvided"])."',  '" . $_POST["Otherid"] . "','" . $_POST["Otherid_name"] . "','$customerNumber_value','".$_POST["payinBook"]."', '".$balance."','".$tran_date."', '".$customerName."','".$_POST["profession"]."','".$_POST["fillingNo"]."','".$_POST["passportValidate"]."','".$_POST["proveAddressType"]."','".$_POST["sonOf"]."','".$_POST["sonOfType"]."'".$insertIntoValues.", 'W','".$_POST["Countryaddress"]."','TC',0, 'Enable')";
//				}

//				debug($Querry_Sqls);
//				echo $Querry_Sqls;
//				die("hello");

				//debug($Querry_Sqls,true);
				insertInto($Querry_Sqls);

				//mail:debug($Querry_Sqls);
				$customerID = @mysql_insert_id();

				///////////////////////////////////////////////////////////

//				echo $flag;

				$complianceRule = array(
					'ruleID' => $compExactMatch[$k]["ruleID"],
					'applyAt' => $compExactMatch[$k]["applyAt"],
					'userType' => $compExactMatch[$k]["userType"],
					'matchCriteria' => $compExactMatch[$k]["matchCriteria"],
					'listID' => $compExactMatch[$k]["listID"],
					'isEnable' => $compExactMatch[$k]["isEnable"],
					'buildByUserType' => $compExactMatch[$k]["buildByUserType"],
					'buildByUserID' => $compExactMatch[$k]["buildByUserID"],
					'message' => $compExactMatch[$k]["message"],
					'autoBlock' => $compExactMatch[$k]["autoBlock"],
					'dated' => $compExactMatch[$k]["dated"],
					'applyTrans' => $compExactMatch[$k]["applyTrans"],
					'amount' => $compExactMatch[$k]["amount"],
					'cumulativeFromDate' => $compExactMatch[$k]["cumulativeFromDate"],
					'cumulativeToDate' => $compExactMatch[$k]["cumulativeToDate"],
					'criteria_types' => $compExactMatch[$k]["criteria_types"],
					'idType' => $compExactMatch[$k]["idtype"],
					'id_type_list' => $compExactMatch[$k]["id_type_list"],
					'id_type_apply_user' => $compExactMatch[$k]["id_type_apply_user"],
					'docCategory' => $compExactMatch[$k]["docCategory"],
					'doc_provided' => $compExactMatch[$k]["doc_provided"],
					'isHold' => $compExactMatch[$k]["isHold"],
					'showGrid' => $compExactMatch[$k]["showGrid"],
					'updated' => $compExactMatch[$k]["updated"],
					'CTR' => $compExactMatch[$k]["CTR"],
					'id_expiry' => $compExactMatch[$k]["id_expiry"],
					'id_upload' => $compExactMatch[$k]["id_upload"],
					'id_check' => $compExactMatch[$k]["id_check"],
					'is_proceed' => $compExactMatch[$k]["is_proceed"],
					'isForAPItrans' => $compExactMatch[$k]["isForAPItrans"]
				);
				$compRuleJSON = json_encode($complianceRule);

				$timestamp = date("Y-m-d H:i:s");

				insertInto("INSERT INTO compliance_activities_logs (comp_user_id, comp_source_table, compRuleJSON, created_on) values ($customerID, 'Customer', '$compRuleJSON', '$timestamp')");

				///////////////////////////////////////////////////////////

				if(defined("CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX"))
					$strCustomerPrefix = CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX;
				else
					$strCustomerPrefix = "C-";

				/*	#5610 - Minas Centre
                *	Member and Standard customer dropdown is added on form.
                *	and M is prefixed to customerID for Membership customers.
                *	by A.Shahid
                */
				if($senderTypeFlag){
					if(isset($_REQUEST["senderType"]) && $_REQUEST["senderType"]!=""){
						if(strtoupper($_REQUEST["senderType"])=="MEMBERSHIP")
							$strCustomerPrefix = "M";
					}
				}
				$customerCnt = $customerID;
				if(defined("CONFIG_CUSTOM_SENDER_COUNTER_START") && CONFIG_CUSTOM_SENDER_COUNTER_START!="0" && is_numeric(CONFIG_CUSTOM_SENDER_COUNTER_START)){
					if($customerNumber_value < CONFIG_CUSTOM_SENDER_COUNTER_START){
						$customerCnt = CONFIG_CUSTOM_SENDER_COUNTER_START;
						$custNumberUpdate = ",customerNumber = '".$customerCnt."' ";
					}
					else{
						$customerCnt = $customerNumber_value;
						$strCustCnt = "SELECT customerNumber FROM ".TBL_CUSTOMER." WHERE customerNumber = '".CONFIG_CUSTOM_SENDER_COUNTER_START."' ";
						$custCntDate = selectFrom($strCustCnt);
					}
					if($custCntDate["accountName"] == $strCustomerPrefix.$customerCnt)
						$customerCnt = CONFIG_CUSTOM_SENDER_COUNTER_START;
				}
				if($_POST["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != '1')
				{
					$customerCode = $strCustomerPrefix.$customerCnt;
					$numberQuery = " update customer set accountName = '".$customerCode."'";

					/**
					 * making the query for GE to feeb boththe payinBook and accountName entry
					 * @Ticket# 3549

					if(CONFIG_USE_AGENT_AT_ADD_SENDER == 1 && !empty($_REQUEST["ch_payinBook"]))
					{
					$numberQuery .= " , payinBook = '".$customerCode."' ";
					}
					 */
					$numberQuery .= " where customerID = '".$customerID."'";

					update($numberQuery);
					//debug($numberQuery);
				}

				insertError($compExactMatch[$k]["message"].". Reference Number is <b>$customerCode</b>");//CU10.
				$backURL = "add-customer.php?notEndSessions=Y&from=".$from."&customerID=".$_POST["customerID"]."&msg=Y";
				redirect($backURL);

			}
			else
			{
				//entry in Data Base[complianceMatchList] table
				$Querry_Sqls="insert into ".TBL_COMPLIANCE_MATCH_LIST." (compliancePersonID,listID,isFullMatch,isBlocked,matchUserType,enteredBy,enterByID,dated) values ('".$compPersonRecord["id"]."','".$compPersonRecord["listID"]."','Y','sender','N','".$agentType."',".$modifyby.",'".$tran_dateTime."')";
				insertInto($Querry_Sqls);
				insertError($compExactMatch[$k]["message"]);

//				redirect($backURL);

			}
		}
		$k++;


	} // end while loop exact match
	/////////////End Exact Match /////////////////////////////
	/////////////Start Partial Match  Code//////////////////////////

	//$compFlag = true;
	$p=0;
	$includeFlag=false;
	$matchListID = '';
	while(($p < count($compPartialMatch)) &&  $compFlag)
	{
		$flag="Partial";
		$queryListName = selectFrom("select listID,listName from ".TBL_COMPLIANCE_LIST." 
			 where listID= '".$compPartialMatch[$p]["listID"]."' ");

		if($queryListName["listName"] == 'OFAC'){
			$query = "select compliancePersonID as id,listID from ".TBL_COMPLIANCE_PERSON."
				 where listID= '".$compPartialMatch[$p]["listID"]."' ";

			if($firstName!=''){
				$query .= " and (firstName ='".$firstName."' or lastName ='".$firstName."'";
			}
			if($middleName!=''){
				$query .= " or middleName ='".$middleName."'";
			}

			if($lastName!=''){
				$query .= " or lastName ='".$lastName."' or firstName ='".$lastName."')";
			}

			$rs_partial = mysql_query($query);
			$compPartialSubRecord = mysql_num_rows($rs_partial);


			if($compPartialSubRecord < 1 )
			{
				$query = "select alt_id,compliancePersonID,alt_name,listID from ".TBL_COMPLIANCE_OFAC_ALT." 
				               where listID= '".$compPartialMatch[$p]["listID"]."' ";


				/* if($firstName!=''){
                 $query .= "  and  alt_name LIKE('".$firstName."')";
                  }
                 if($middleName!=''){
                 $query .= "  or  alt_name LIKE ('".$middleName."')";
                  }
                if($lastName!=''){
                 $query .= "  or alt_name LIKE ('".$lastName."')";
                  }*/

				if($firstName!=''){
					$query .= "  and  (MATCH (alt_name) AGAINST ('".$firstName."')";
				}
				if($middleName!=''){
					$query .= "  OR  MATCH(alt_name) AGAINST ('".$middleName."')";
				}
				if($lastName!=''){
					$query .= "  OR MATCH(alt_name) AGAINST ('".$lastName."'))";
				}

			}
		}
		if($queryListName["listName"] == 'payex')
		{

			$query = "select compliancePersonID as id,listID from ".TBL_COMPLIANCE_PERSON." 
				where listID= '".$compPartialMatch[$p]["listID"]."' ";
			if($firstName!=''){
				$query .= " AND (firstName ='".$firstName."' or lastName ='".$firstName."'";
			}
			if($middleName!=''){
				$query .= " OR middleName ='".$middleName."'";
			}
			if($lastName!=''){
				$query .= " OR lastName ='".$lastName."'  or firstName ='".$lastName."')";
			}

		}

		if($queryListName["listName"] == 'PEP')
		{
			$query = "select comp_PEPID as id,listID,fullName,position,portfolio from compliancePEP where listID= '".$compPartialMatch[$p]["listID"]."' ";

			if($firstName!='')
				$query .= "  AND ( MATCH(fullName) AGAINST('".$firstName."')";
			if($middleName!='')
				$query .= "  OR  MATCH(fullName) AGAINST ('".$middleName."')";
			if($lastName!='')
				$query .= "  OR MATCH(fullName) AGAINST ('".$lastName."%'))";

		}

		if($queryListName["listName"] == 'HM Treasury'){
			$query = "select hm_id as id,list_id as listID,full_name from compliance_hm_treasury
				where list_id= '".$compPartialMatch[$p]["listID"]."' ";

			/*if($firstName!=''){
                   $query .= "  and  full_name LIKE('%".$firstName."%')";
                     }
                    if($middleName!=''){
                    $query .= "  or  full_name LIKE ('%".$middleName."%')";
                     }
                   if($lastName!=''){
                    $query .= "  or full_name LIKE ('%".$lastName."%')";
                     }*/
			if($firstName!=''){
				$query .= "  and  (MATCH(full_name) AGAINST ('".$firstName."')";
			}
			if($middleName!=''){
				$query .= "  OR  MATCH(full_name) AGAINST ('".$middleName."')";
			}
			if($lastName!=''){
				$query .= "  OR MATCH(full_name) AGAINST ('".$lastName."'))";
			}

			$query .=" AND group_type = 'Individual' " ;
		}

		$rs = mysql_query($query);

		if( $compPartailRecord = mysql_fetch_array($rs))
		{
			$includeFlag=true;
			$compliancePersonID = $compPartailRecord["id"];
			$listID = $compPartailRecord["listID"];
			$agentType = $agentType;
			$enterByID = $modifyby;
			$sender = 'sender';
			$dated = $tran_dateTime;

			$message .= $compPartialMatch[$p]["message"]." <br>";
			//$comp_page = "add-ben-conf.php";
			$comp_page = "add-customer-conf.php";

			$Querry_Sqls = "insert into ".TBL_COMPLIANCE_MATCH_LIST." 		
					   					(
										 compliancePersonID,
										 listID,
										 isFullMatch,
										 matchUserType,
										 enteredBy,
										 enterByID,
										 dated
										 )
										values 
										(
										'".$compliancePersonID."',
										'".$listID."',
										'N',
										'sender',
										'".$agentType."',
										'".$enterByID."',
										'".$dated."'
										)";
			insertInto($Querry_Sqls);

			$matchListID .= @mysql_insert_id().",";
		}

		///////////////////////////////////////////////////////////

//		echo $flag;

		$complianceRule = array(
			'ruleID' => $compPartialMatch[$p]["ruleID"],
			'applyAt' => $compPartialMatch[$p]["applyAt"],
			'userType' => $compPartialMatch[$p]["userType"],
			'matchCriteria' => $compPartialMatch[$p]["matchCriteria"],
			'listID' => $compPartialMatch[$p]["listID"],
			'isEnable' => $compPartialMatch[$p]["isEnable"],
			'buildByUserType' => $compPartialMatch[$p]["buildByUserType"],
			'buildByUserID' => $compPartialMatch[$p]["buildByUserID"],
			'message' => $compPartialMatch[$p]["message"],
			'autoBlock' => $compPartialMatch[$p]["autoBlock"],
			'dated' => $compPartialMatch[$p]["dated"],
			'applyTrans' => $compPartialMatch[$p]["applyTrans"],
			'amount' => $compPartialMatch[$p]["amount"],
			'cumulativeFromDate' => $compPartialMatch[$p]["cumulativeFromDate"],
			'cumulativeToDate' => $compPartialMatch[$p]["cumulativeToDate"],
			'criteria_types' => $compPartialMatch[$p]["criteria_types"],
			'idType' => $compPartialMatch[$p]["idtype"],
			'id_type_list' => $compPartialMatch[$p]["id_type_list"],
			'id_type_apply_user' => $compPartialMatch[$p]["id_type_apply_user"],
			'docCategory' => $compPartialMatch[$p]["docCategory"],
			'doc_provided' => $compPartialMatch[$p]["doc_provided"],
			'isHold' => $compPartialMatch[$p]["isHold"],
			'showGrid' => $compPartialMatch[$p]["showGrid"],
			'updated' => $compPartialMatch[$p]["updated"],
			'CTR' => $compPartialMatch[$p]["CTR"],
			'id_expiry' => $compPartialMatch[$p]["id_expiry"],
			'id_upload' => $compPartialMatch[$p]["id_upload"],
			'id_check' => $compPartialMatch[$p]["id_check"],
			'is_proceed' => $compPartialMatch[$p]["is_proceed"],
			'isForAPItrans' => $compPartialMatch[$p]["isForAPItrans"]
		);

		$compRuleJSON .= json_encode($complianceRule);

		///////////////////////////////////////////////////////////

		$p++;

	}       //end while loop partial match

	//////////////////////////////End Partial Match//////////////////////////////////////////////

	if($includeFlag)
	{

		$compFlag = false;
		if($ttTransFlag && $_REQUEST["callFrom"]=="TT")
		{

			$CustStatus ="Disable";
			$isCompliance = 'Y';


		}
		include ("compliance-partialMatch.php");
	}
}


if($compFlag)
{
	//// end compliance logic (Niaz)
	if ($_POST["customerID"] == ""){

		$password = createCode();
		$custWhr = " AND Country='".$_POST["Country"]."' ";

		if(defined("CONFIG_CUSTOM_SENDER_COUNTER_START") && CONFIG_CUSTOM_SENDER_COUNTER_START!="0")
			$custWhr = "";
		$customerNumber = selectFrom("select MAX(customerNumber) from ".TBL_CUSTOMER." where 1 ".$custWhr);
		$customerNumber_value = $customerNumber[0]+1;
		if($_POST["payinBook"]=="" || CONFIG_PAYIN_CUSTOMER != '1'){
			//$customerCode = $systemPre."CUST".$ccode.($customerNumber[0]+1);////Chnged By Kashi 11-Nov
			$balance = 0;
		}
		else
		{
			$customerCode = $_POST["payinBook"];
			$balance = - $payinAmount ;
		}

		// Added by Usman Ghani aginst ticket #3472: Now Transfer - Phone/Mobile
		$phone = checkValues($_POST["Phone"]);
		$mobile = checkValues($_POST["Mobile"]);
		if ( defined("CONFIG_PHONES_AS_DROPDOWN")
			&& CONFIG_PHONES_AS_DROPDOWN == 1 )
		{
			if ( $_POST["phoneType"] == "phone" )
			{
				$phone = checkValues($_POST["Phone"]);
				$mobile = "";
			}
			else
			{
				$phone = "";
				$mobile = checkValues($_POST["Mobile"]);
			}
		}
		// End of code against ticket #3472: Now Transfer - Phone/Mobile

		$arguments = array("flag"=>"getDatedForComments","senderRemarks" => $_POST["senderRemarks"]);
		$insertUserArr = gateway("CONFIG_INSERT_DATE_IN_COMMENTS",$arguments,CONFIG_INSERT_DATE_IN_COMMENTS);
		$getDatedForComments = $insertUserArr["values"]; // "\n[".date('j F Y')."]\n";
		/*
        * 7132 - Premier FX
        * Added account manager dropdown on add/udate sender page
        * by Aslam Shahid
        */
		$arguments = array("flag"=>"addCustomerData","parentID" => $_POST["customerParentID"]);
		$insertUserArr = gateway("CONFIG_POPULATE_USERS_DROPDOWN_SENDER",$arguments,CONFIG_POPULATE_USERS_DROPDOWN_SENDER);
		$insertIntoKeys	= $insertUserArr["keys"];//",parentID";
		$insertIntoValues =	$insertUserArr["values"];// ",'".$_POST["parentID"]."'";

		$arguments = array("flag"=>"addCustomerData","source" => $_POST["source"]);
		$insertUserArr = gateway("CONFIG_SOURCE_FIELD_CUSTOMER",$arguments,CONFIG_SOURCE_FIELD_CUSTOMER);
		$insertIntoKeys	.= $insertUserArr["keys"];//",parentID";
		$insertIntoValues .= $insertUserArr["values"];// ",'".$_POST["parentID"]."'";

		$arguments = array("flag"=>"addCustomerData","tradingAcccount" => $_POST["tradingAcccount"]);
		$insertUserArr = gateway("CONFIG_TRADING_ACCOUNT_CUSTOMER",$arguments,CONFIG_TRADING_ACCOUNT_CUSTOMER);
		$insertIntoKeys	.= $insertUserArr["keys"];//",parentID";
		$insertIntoValues .= $insertUserArr["values"];// ",'".$_POST["parentID"]."'";

		if($compFlag){

			//added by Anjum

			if($chkBlock!= "")
			{


				/**
				 * #10461: Premier FX-Online module customer management
				 * Short Description
				 * 'custCategory' field is add in query
				 * @Author: Mirza Arslan Baig
				 */
				$Querry_Sqls = "INSERT INTO ".TBL_CUSTOMER." (accountName, password, agentID, Title,other_title, firstName, middleName, secMiddleName, lastName, Address, Address1, proveAddress, Country, City, Zip, State, Phone, Mobile, email, dob,placeOfBirth, acceptedTerms, IDType, IDNumber, IDExpiry,IDissuedate, issuedBy, documentProvided, otherId, otherId_name,customerNumber, payinBook, balance, created, customerName,profession,fillingNo, passport_number,proveAddType,SOF,SOF_Type".$insertIntoKeys.", custCategory,countryOfResidence,online_customer_services, EnabledFromNew, customerStatus) VALUES 
	('$customerCode', '$password', '$agentIds', '".$_POST["Title"]."','".$_POST["other_title"]."', '".checkValues($_POST["firstName"])."', '".checkValues($_POST["middleName"])."', '".checkValues($_POST["secMiddleName"])."' ,'".checkValues($_POST["lastName"])."', '".checkValues($_POST["Address"])."', '".checkValues($_POST["Address1"])."', '".($_POST["proveAddress"] != "" ? $_POST["proveAddress"] : "N")."', '".$_POST["Country"]."', '".checkValues($_POST["City"])."', '".checkValues($_POST["Zip"])."', '".checkValues($_POST["State"])."', '".$phone."', '".$mobile."', '".checkValues($_POST["custEmail"])."', '".$_POST["dobYear"]."-".$_POST["dobMonth"]."-".$_POST["dobDay"]."','".$_POST["placeOfBirth"]."','".$_POST["acceptedTerms"]."','".$_POST["IDType"]."', '".checkValues($_POST["IDNumber"])."', '".$idExpiryDate."','".$idIssueDate."', '".checkValues($_POST["issuedBy"])."', '".checkValues($_SESSION["documentProvided"])."',  '" . $_POST["Otherid"] . "','" . $_POST["Otherid_name"] . "','$customerNumber_value','".$_POST["payinBook"]."', '".$balance."','".$tran_date."', '".$customerName."','".$_POST["profession"]."','".$_POST["fillingNo"]."','".$_POST["passportValidate"]."','".$_POST["proveAddressType"]."','".$_POST["sonOf"]."','".$_POST["sonOfType"]."'".$insertIntoValues.", 'W','".$_POST["Countryaddress"]."','TC',0, 'Disable')";

			}
			else
			{

				/**
				 * #10461: Premier FX-Online module customer management
				 * Short Description
				 * 'custCategory' field is add in query
				 * @Author: Mirza Arslan Baig
				 */

				$Querry_Sqls = "INSERT INTO ".TBL_CUSTOMER." (accountName, password, agentID, Title,other_title, firstName, middleName, secMiddleName, lastName, Address, Address1, proveAddress, Country, City, Zip, State, Phone, Mobile, email, dob,placeOfBirth, acceptedTerms, IDType, IDNumber, IDExpiry,IDissuedate, issuedBy, documentProvided, otherId, otherId_name,customerNumber, payinBook, balance, created, customerName,profession,fillingNo, passport_number,proveAddType,SOF,SOF_Type".$insertIntoKeys.", custCategory,countryOfResidence,online_customer_services,  EnabledFromNew, customerStatus ) VALUES 
	('$customerCode', '$password', '$agentIds', '".$_POST["Title"]."','".$_POST["other_title"]."', '".checkValues($_POST["firstName"])."', '".checkValues($_POST["middleName"])."', '".checkValues($_POST["secMiddleName"])."' ,'".checkValues($_POST["lastName"])."', '".checkValues($_POST["Address"])."', '".checkValues($_POST["Address1"])."', '".($_POST["proveAddress"] != "" ? $_POST["proveAddress"] : "N")."', '".$_POST["Country"]."', '".checkValues($_POST["City"])."', '".checkValues($_POST["Zip"])."', '".checkValues($_POST["State"])."', '".$phone."', '".$mobile."', '".checkValues($_POST["custEmail"])."', '".$_POST["dobYear"]."-".$_POST["dobMonth"]."-".$_POST["dobDay"]."','".$_POST["placeOfBirth"]."','".$_POST["acceptedTerms"]."','".$_POST["IDType"]."', '".checkValues($_POST["IDNumber"])."', '".$idExpiryDate."','".$idIssueDate."', '".checkValues($_POST["issuedBy"])."', '".checkValues($_SESSION["documentProvided"])."',  '" . $_POST["Otherid"] . "','" . $_POST["Otherid_name"] . "','$customerNumber_value','".$_POST["payinBook"]."', '".$balance."','".$tran_date."', '".$customerName."','".$_POST["profession"]."','".$_POST["fillingNo"]."','".$_POST["passportValidate"]."','".$_POST["proveAddressType"]."','".$_POST["sonOf"]."','".$_POST["sonOfType"]."'".$insertIntoValues.", 'W','".$_POST["Countryaddress"]."','TC',0, 'Enable')";
			}

			//debug($Querry_Sqls);
			//die("hello");

			//debug($Querry_Sqls,true);
			insertInto($Querry_Sqls);

			//mail:debug($Querry_Sqls);
			$customerID = @mysql_insert_id();

			///////////////////////////////////////////////////////////
			//Asad
//			echo "Block Unchecked";
//			die();

			$timestamp = date("Y-m-d H:i:s");

			insertInto("INSERT INTO compliance_activities_logs (comp_user_id, comp_source_table, compRuleJSON, created_on) values ($customerID, 'Customer', '$compRuleJSON', '$timestamp')");

			///////////////////////////////////////////////////////////

			insertInto("insert into customerBank values(0,$customerID,0,'$branchName','$city','$bankName','$accNumber','$swiftCode','$branchAddress','$routingNumber','$IBAN','$accountType','$sortCode','')");

			if($ttTransFlag && $_REQUEST["callFrom"]=="TT")
			{
				$CustStatus=$_REQUEST["CustStatus"];
				$isCompliance=$_REQUEST["isCompliance"];

				changeStatusSen($CustStatus,$customerID,$isCompliance);

			}

			//debug($Querry_Sqls);
			// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
			if(count($arrListId) > 0){
				for($k=0;$k<count($arrListId);$k++)
				{
					if(!empty($arrListId[$k])){
						$Querry_Sqls = "update ".TBL_COMPLIANCE_MATCH_LIST." 
						  set
							  userDataBaseID = '".$customerID."'
						  where 
								matchListID = ".$arrListId[$k]."
						   ";

						update($Querry_Sqls);
					}
				}
			}
			if($categoryFlag){
				$categorySql = "update ".TBL_CUSTOMER." set cust_category_id = '".$_REQUEST["custCategory"]."' where customerID = '".$customerID."'";
				update($categorySql);
			}
			if($categoryFlag){
				$categorySql = "update ".TBL_CUSTOMER." set cust_category_id = '".$_REQUEST["custCategory"]."' where customerID = '".$customerID."'";
				update($categorySql);
			}
			if ( !empty( $_REQUEST["IDTypesValuesData"] ) )
			{

				if(isset($_REQUEST["arrMultipleIds"]))
					$idTypeValuesData = unserialize(stripslashes(stripslashes($_REQUEST["arrMultipleIds"])));
				else
					$idTypeValuesData = $_REQUEST["IDTypesValuesData"];

				foreach( $idTypeValuesData as $idTypeValues )
				{
					if ( !empty( $idTypeValues["id_type_id"] )
						&& !empty( $idTypeValues["id_number"] )
						&& !empty( $idTypeValues["issued_by"] )
						&& !empty( $idTypeValues["issue_date"] )
						&& !empty( $idTypeValues["expiry_date"] ) )
					{
						// If id is empty, its an insert request.
						// Insert new reocrd in this case.

						$issueDate = $idTypeValues["issue_date"]["year"] . "-" . $idTypeValues["issue_date"]["month"] . "-" . $idTypeValues["issue_date"]["day"];
						$expiryDate = $idTypeValues["expiry_date"]["year"] . "-" . $idTypeValues["expiry_date"]["month"] . "-" . $idTypeValues["expiry_date"]["day"];

						$insertQuery = "insert into user_id_types
										(
											id_type_id, 
											user_id, 
											user_type, 
											id_number, 
											issued_by, 
											issue_date, 
											expiry_date 
										)
									values
										(
											'" . $idTypeValues["id_type_id"] . "', 
											'" . $customerID . "', 
											'C', 
											'" . $idTypeValues["id_number"] . "', 
											'" . $idTypeValues["issued_by"] . "', 
											'" . $issueDate . "', 
											'" . $expiryDate . "'
										)";
						insertInto( $insertQuery );
						/*
                         * @Ticket #4794
                         */
						$lastIdInsertId = @mysql_insert_id();
						if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
							update("update user_id_types set notes='".mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$lastIdInsertId."'");
						}
					}
				}
			}
			// End of code against #3299: MasterPayex - Multiple ID Types

		}
//debug($customerID,true);
		if($SenderIdFlag)
		{

			if(isset($_REQUEST["docCategory2"]))
				$docCategory = unserialize(stripslashes(stripslashes(stripslashes($_REQUEST["docCategory2"]))));
			else
				$docCategory = $_REQUEST["docCategory"];

			if(isset($_REQUEST["docExpiryData2"]))
				$expiryValues = unserialize(stripslashes(stripslashes(stripslashes($_REQUEST["docExpiryData2"]))));
			else
				$expiryValues = $_REQUEST["docExpiryData"];
			$arrFinalDocExpiry = array();
			//$expiryValues = $_REQUEST["docExpiryData"];
			//debug($_REQUEST,true);
			foreach($docCategory as $docKey => $docVal)
			{
				$expiryDate = $expiryValues[$docKey]["expiry_date"]["year"] . "-" . $expiryValues[$docKey]["expiry_date"]["month"] . "-" . $expiryValues[$docKey]["expiry_date"]["day"];
				$arrFinalDocExpiry[$docKey] = $expiryDate;
			}
			$arrCustDoc = serialize($arrFinalDocExpiry);
			$strUpdateCustDocSql = "update customer set custDocumentProvided = '".$arrCustDoc."' where customerID = '".$customerID."'";

			update($strUpdateCustDocSql);
			//debug($strUpdateCustDocSql,true);
			//debug($customerID);
		}
		/**
		 * To retains the remarks about a customer/sender
		 * at its creation and display at the add transaction
		 * @Ticket #4230
		 */
		if(CONFIG_REMARKS_ABOUT_SENDER == "1")
		{
			$strUpdateCustomerSql = "update customer set senderRemarks = '".$getDatedForComments.addslashes($_REQUEST["senderRemarks"])."' where customerID = '".$customerID."'";
			update($strUpdateCustomerSql);
		}
		// Added by Niaz Ahmad at 27-05-2009 @4851 to validate passport number
		if(CONFIG_PASSPORT_VALIDATION == "1"){
			$strUpdateCustPassportSql = "update customer set passport_number = '".$_REQUEST["passportValidate"]."' where customerID = '".$customerID."'";
			update($strUpdateCustPassportSql);
		}

// Added by Niaz Ahmad at 03-06-2009 @4997
		if(senderFullnameFlag){
			$strUpdateCustFullnametSql = "update customer set   	customerType = '".$_REQUEST["senderFullName"]."' where customerID = '".$customerID."'";
			update($strUpdateCustFullnametSql);
		}

		if(defined("CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX"))
			$strCustomerPrefix = CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX;
		else
			$strCustomerPrefix = "C-";

		/*	#5610 - Minas Centre
        *	Member and Standard customer dropdown is added on form.
        *	and M is prefixed to customerID for Membership customers.
        *	by A.Shahid
        */
		if($senderTypeFlag){
			if(isset($_REQUEST["senderType"]) && $_REQUEST["senderType"]!=""){
				if(strtoupper($_REQUEST["senderType"])=="MEMBERSHIP")
					$strCustomerPrefix = "M";
			}
		}
		$customerCnt = $customerID;
		if(defined("CONFIG_CUSTOM_SENDER_COUNTER_START") && CONFIG_CUSTOM_SENDER_COUNTER_START!="0" && is_numeric(CONFIG_CUSTOM_SENDER_COUNTER_START)){
			if($customerNumber_value < CONFIG_CUSTOM_SENDER_COUNTER_START){
				$customerCnt = CONFIG_CUSTOM_SENDER_COUNTER_START;
				$custNumberUpdate = ",customerNumber = '".$customerCnt."' ";
			}
			else{
				$customerCnt = $customerNumber_value;
				$strCustCnt = "SELECT customerNumber FROM ".TBL_CUSTOMER." WHERE customerNumber = '".CONFIG_CUSTOM_SENDER_COUNTER_START."' ";
				$custCntDate = selectFrom($strCustCnt);
			}
			if($custCntDate["accountName"] == $strCustomerPrefix.$customerCnt)
				$customerCnt = CONFIG_CUSTOM_SENDER_COUNTER_START;
		}
		if($_POST["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != '1')
		{
			$customerCode = $strCustomerPrefix.$customerCnt;
			$numberQuery = " update customer set accountName = '".$customerCode."' ".$custNumberUpdate;

			/**
			 * making the query for GE to feeb boththe payinBook and accountName entry
			 * @Ticket# 3549

			if(CONFIG_USE_AGENT_AT_ADD_SENDER == 1 && !empty($_REQUEST["ch_payinBook"]))
			{
			$numberQuery .= " , payinBook = '".$customerCode."' ";
			}
			 */
			$numberQuery .= " where customerID = '".$customerID."'";
			update($numberQuery);
			//debug($numberQuery);
		}

		if ($_POST["payinBook"] == "" && CONFIG_AUTO_PAYIN_NUMBER == '1')
		{
			$customerCode = $strCustomerPrefix . $customerCnt;
			$_POST["payinBook"] = $customerCode;

			$payinQuery = "UPDATE " . TBL_CUSTOMER . " SET `payinBook` = '$customerCode' ".$custNumberUpdate." WHERE `customerID` = '$customerID'";
			//debug("ch_payinBook >>".$_REQUEST["ch_payinBook"]);
			if(defined("CONFIG_USE_AGENT_AT_ADD_SENDER") && CONFIG_USE_AGENT_AT_ADD_SENDER == 1)
			{
//				if(!empty($_REQUEST["ch_payinBook"]) && $_REQUEST["ch_payinBook"] == "Y")
//				{
					update($payinQuery);
					//debug($payinQuery);
//				}
			}
			elseif(!defined("CONFIG_USE_AGENT_AT_ADD_SENDER") || CONFIG_USE_AGENT_AT_ADD_SENDER == 0)
			{
				update($payinQuery);
				//debug($payinQuery);
			}

		}

		//debug($customerCode, true);

		$descript = "Customer data is added";
		activities($_SESSION["loginHistoryID"],"INSERTION",$customerID,TBL_CUSTOMER,$descript);

		$id_activity_id = @mysql_insert_id();
		$startMessage="Congratulations! You have been registered as a sender of Rich Almond. <br> Your registration information is as follows:";

		include_once("lib/html_to_doc.inc.php");
		$htmltodoc= new HTML_TO_DOC();
		include_once("mailCustomerInfo.php");

//$fp = fopen("htmlEmaiWelcomeLetter.html", 'w+');
		$tmpfname = tempnam("/tmp", "WELCOME_".$_POST["firstName"]."_");
		$fpOpen = fopen($tmpfname, 'w+');
		if($fpOpen){
			//mail:debug($fpOpen);
			fwrite($fpOpen, $data);
			fclose($fpOpen);
			//include("lib/html_to_doc.inc.php");
			//$htmltodoc= new HTML_TO_DOC();
			//$htmltodoc->createDoc($timeStamp.".html","test");
		}

		$To = $_POST["custEmail"];
		$fromName = SUPPORT_NAME;
		$fromEmail = SUPPORT_EMAIL;
//$htmltodoc->createDoc("htmlEmaiWelcomeLetter.html","test");
//$htmltodoc->createDocFromURL("http://yahoo.com","test");
//$subject = SYSTEM;//"Premier FX ";
//$subject = "Deal Contract ";
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$num = md5( time() );

		$whrClauseAdminTable = " and userID = '".$_POST["agentID"]."' ";
		if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR == "1"){
			$whrClauseAdminTable = " and userID = '".$_POST["customerParentID"]."' ";
		}
		$agentUserName=selectFrom(" select name,email from admin where 1 $whrClauseAdminTable ");
		$agentEmail = $agentUserName['email'];
//with scanned copy
//$fullName = $_POST["firstName"]." ".$_POST["lastName"];
		$fullName = $_POST["firstName"];

		if($_POST['Address'])
				{
				$strAdress=$_POST['Address'];
				}
				if($customerCode)
				{
				$accountName=$customerCode;
				}
				if($_POST['Mobile'])
				{
				$strPhone=$_POST['Mobile'];
				}


		$emailContent1 = selectFrom("select * from email_templates left join events on email_templates.eventID=events.id where events.id='2' and email_templates.status = 'Enable'  AND  agentID='".$agentIds."'");
// $logoCompany = "<img border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/mail_signature.jpg?v=1' />";
		$logoCompany = "<img border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/spi_payscanner_com/logo.png' />";
		$message11 = $emailContent1["body"];
		$subject1 = $emailContent1["subject"];
		$varEmail1 = array("{customer}","{logo}","{accountmanager}","{accountName}","{address}","{phone}");
		$accManagerName=explode(" ",$agentUserName["name"]);
		$countName=count($accManagerName);
		if($countName>2){
			$fullNameAccM=$accManagerName[0]." ".$accManagerName[$countName-1];

		}
		else{
			$fullNameAccM=$accManagerName[0]." ".$accManagerName[1];

		}




		$contentEmail1 = array($fullName,$logoCompany,$fullNameAccM,$accountName,$strAdress,$strPhone);
		$messageT1 = str_replace($varEmail1,$contentEmail1,$message11);

//without scanned copy
		$emailContent2 = selectFrom("select * from email_templates left join events on email_templates.eventID=events.id where events.id='7' and email_templates.status = 'Enable' ");
// $logoCompany = "<img border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/mail_signature.jpg?v=1' />";
		$logoCompany = "<img border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/spi_payscanner_com/logo.png' />";
		$message12 = $emailContent2["body"];
		$subject2 = $emailContent2["subject"];
		$varEmail2 = array("{customer}","{logo}","{accountmanager}","{accountName}","{address}","{phone}");

		$contentEmail2 = array($fullName,$logoCompany,$fullNameAccM,$accountName,$strAdress,$strPhone);
		$messageT2 = str_replace($varEmail2,$contentEmail2,$message12);



//$message="\n Dear ".$_POST["firstName"].",\n<br>";
//$message.="\n<br />Please find attached details of your trading account with Premier FX. To"."\n\r";
		$passQuery=selectFrom(" select documentId from document_upload where userId='".$customerID."'");
		$IDTypesValuesData = $_REQUEST["IDTypesValuesData"];
		$IDDetailsFlag = false;
		foreach( $idTypeValuesData as $idTypeValues )
		{
			if($idTypeValues["id_number"] != "" && $idTypeValues["issued_by"] != "")
				$IDDetailsFlag = true;
		}
		//debug($passQuery);
		//debug($idTypeValuesData);
		//debug($IDDetailsFlag,true);
		if(!$IDDetailsFlag){
			$subject = $subject2;
			/*$message .= "\n<br> Please find attached details of your trading account with Premier FX."."\n<br>";
            $message.= "\n<br> To complete your application we will require a scanned copy of your passport and a proof of address document. This is common practice for all financial institutions under money laundering regulations."."\n<br>";
            $message.="\n<br> Please do not hesitate to contact me if you have any further questions.  I look forward to being of service to you in the near future."."\n<br>";*/
			$message = $messageT2;

		}else{
			$subject = $subject1;
			/*$message.= "\n<br> Please find attached details of your trading account with Premier FX."."\n<br>" ;
            $message.="\n<br> Please do not hesitate to contact me if you have any further questions. I look forward to being of service to you in the near future."."\n<br>";*/
			$message = $messageT1;
		}
//$message.=" \n<br>Regards,"."\n<br />";


//$agentIDQuery=selectFrom(" select parentID from customer where customerID='".$customerID."'");
//debug("select name,email from admin where 1 $whrClauseAdminTable", true);

//$message .= "\n ".$agentUserName["name"]."\n";
//Premier Support\n
		/**
		 * #10408: Premier Exchange: Wrong office address On Emails
		 * Short Description
		 * Address is changed against the ticket number mentioned
		 */
		/*$message.= "<table>
                        <tr>
                            <td align='left' width='30%'>
                            <img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width='20%'>&nbsp;</td>
                            <td align='left'>
                                <font size='2px'><font color='#F7A30B'>UK OFFICE</font>\n<br>
                                55 Old Broad Street,London, EC2M 1RX.\n<br>
                                <font color='#F7A30B'>Tel:</font> +44 (0)845 021 2370\n<br>\n<br>
                                <font color='#F7A30B'> PORTUGAL OFFICE</font> \n<br>
                                Rua Sacadura Cabral, Edificio Golfe lA, 8135-144 Almancil, Algarve.  \n<br>
                                <font color='#F7A30B'>Tel:</font> +351 289 358 511\n<br>
                                <font color='#F7A30B'>FAX:</font> +351 289 358 513\n<br>\n<br>
                                <font size='2px'><font color='#F7A30B'>SPAIN OFFICE</font>\n<br>
                                C/Rambla dels Duvs, 13-1, 07003, Mallorca. \n<br>
                                <font color='#F7A30B'>Tel:</font> +34 971 576 724
                                 \n <br>
                                 <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font>
                            </td>
                        </tr>
                    </table>";*/

		$strresume = $tmpfname; //"htmlEmaiWelcomeLetter.html";
// This two steps to help avoid spam
		$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
		$headers .= "X-Mailer: PHP v".phpversion()."\r\n";

		include_once ('../pdfClass/class.ezpdf.php');

// Generate pdf document starts here
		$args = array('agentID'=>$_REQUEST["agentID"],'customerID'=>$customerID,'customerParentID'=>$_REQUEST["customerParentID"]);
		$sfsWelcomeData = attachPDFWelcomeLetterEmailOnReg($args);
		//debug($_POST['sendServiceEmail']);
		if($_POST['sendServiceEmail']!="" && CONFIG_STEPS_STRESS_FREE_SERVICE_EMAIL=='1'){
			$sfsData = attachPDFServiceEmail($args);
		}


// Prepare Email for Sender and Account Manger
		$fromName  = SYSTEM;
		$fromEmail = $fromEmail;
		$welcomeLetter = "WelcomeLetter.html";
		$stress_free_service_name = $sfsData['file_name'];
		$stress_free_service_path = $sfsData['file_path'];

		$welcome_letter_name = $sfsWelcomeData['file_name'];
		$welcome_letter_path = $sfsWelcomeData['file_path'];

		if(!empty($agentEmail)){
			$fromName  = $agentUserName["name"];
			$fromEmail = $agentEmail;
		}

		$sendermailer = new PHPMailer();
		$agentmailer = new PHPMailer();

		$sendermailer->FromName = $agentmailer->FromName = $fromName;
		$sendermailer->From = $agentmailer->From = $fromEmail;
		if(CONFIG_SEND_EMAIL_ON_TEST == "1")
		{
			$sendermailer->AddAddress(CONFIG_TEST_EMAIL,'');
		}
		else
		{
			$sendermailer->AddAddress($To,'');
		}
		$agentmailer->Subject = $sendermailer->Subject = $subject;

		if(!$IDDetailsFlag)
		{
			//Add CC
			if($emailContent2['cc'])
				$sendermailer->AddCC($emailContent2['cc'],'');

			//Add BCC
			if($emailContent2['bcc'])
				$sendermailer->AddBCC($emailContent2['bcc'],'');
		}
		else
		{
			//Add CC
			if($emailContent1['cc'])
				$sendermailer->AddCC($emailContent1['cc'],'');

			//Add BCC
			if($emailContent1['bcc'])
				$sendermailer->AddBCC($emailContent1['bcc'],'');




		}

		$sendermailer->IsHTML(true);
		$agentmailer->IsHTML(true);

// attachements

//$sendermailer->AddAttachment($strresume, $welcomeLetter,"base64");
//$agentmailer->AddAttachment($strresume, $welcomeLetter,"base64");
		$sendermailer->AddAttachment($welcome_letter_path, $welcome_letter_name,"base64");
		$agentmailer->AddAttachment($welcome_letter_path, $welcome_letter_name,"base64");

		if(!empty($stress_free_service_name)){
			$sendermailer->AddAttachment($stress_free_service_path, $stress_free_service_name,"base64");
			$agentmailer->AddAttachment($stress_free_service_path, $stress_free_service_name,"base64");

		}

// Body
		$agentmailer->Body = $sendermailer->Body = $message;

		if($_POST["payinBook"] != ""){
			$strQuery = "insert into  agents_customer_account (customerID ,Date,tranRefNo ,payment_mode,Type ,amount, modifiedBy) 
											 values('".$customerID."','".$tran_date."','','Customer Registered','WITHDRAW','".$payinAmount."','".$modifyby."'
											 )";
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
		}


		if(CONFIG_MAIL_GENERATOR == '1' && ($_POST["sendEmail"]=='Y' || $_POST["sendServiceEmail"]=='Y')){
			if(CONFIG_EMAIL_STATUS == "1")
			{
				$senderEmailFlag = $sendermailer->Send();
			}

			if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1"){
				if(!empty($_POST["customerParentID"])){
					$sqlAccMananger = "SELECT name,email FROM ".TBL_ADMIN_USERS." WHERE userID='".trim($_POST["customerParentID"])."' ";
					$accManangerData = selectFrom($sqlAccMananger);
					if(!empty($accManangerData['email'])){
						if(CONFIG_SEND_EMAIL_ON_TEST == "1")
						{
							$agentmailer->AddAddress(CONFIG_TEST_EMAIL,'');

						}
						else
						{
							$agentmailer->AddAddress($accManangerData['email'],'');
						}
						if(CONFIG_EMAIL_STATUS == "1")
						{
							$agentEmailFlag = $agentmailer->Send();
						}
					}
				}
			}
		}




		//mail:debug("stop",true);
		//debug($headers,true);
		$_SESSION["agentID"] = "";
		$_SESSION["firstName"] = "";
		$_SESSION["lastName"] = "";
		$_SESSION["middleName"] = "";
		$_SESSION["secMiddleName"] = "";
		$_SESSION["dobDay"] = "";
		$_SESSION["dobMonth"] = "";
		$_SESSION["dobYear"] = "";
		$_SESSION["placeOfBirth"] = "";
		$_SESSION["acceptedTerms"] = "";
		$_SESSION["IDType"] = "";
		$_SESSION["IDNumber"] = "";
		$_SESSION["Otherid"] = "";
		$_SESSION["Otherid_name"] = "";
		$_SESSION["issuedBy"] = "";
		$_SESSION["dDate"] = "";
		$_SESSION["idDay"] = "";
		$_SESSION["idMonth"] = "";
		$_SESSION["idYear"] = "";
		$_SESSION["Address"] = "";
		$_SESSION["Address1"] = "";
		$_SESSION["proveAddress"] = "";
		$_SESSION["Country"] = "";
		$_SESSION["City"] = "";
		$_SESSION["Zip"] = "";
		$_SESSION["State"] = "";
		$_SESSION["Phone"] = "";
		$_SESSION["Mobile"] = "";
		$_SESSION["custEmail"] = "";
		$_SESSION["documentProvided"] = "";
		$_SESSION["payinBook"] = "";
		$_SESSION["profession"] = "";
		$_SESSION["fillingNo"] = "";
		$_SESSION["uploadImage"] = "";
		$_SESSION["sonOfType"] = "";
		$_SESSION["sonOf"] = "";
		$_SESSION["customerParentID"] = "";
		$_SESSION["source"] = "";
		$_SESSION["senderRemarks"] = "";
		$_SESSION["tradingAcccount"] = "";
		if($senderTypeFlag)
			$_SESSION["senderType"] = "";
		if($categoryFlag)
			$_SESSION["custCategory"] = "";
		// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types

		if ( !empty( $_SESSION["IDTypesValuesData"] ) )
			unset( $_SESSION["IDTypesValuesData"] );

		// End of code against #3299: MasterPayex - Multiple ID Types
		insertError(CU10."The login name for the sender is <b>$customerCode</b>");

	} else {

		if($_POST["payinBook"]!="" && CONFIG_PAYIN_CUSTOMER == '1'){


			$payinCheck = selectFrom("select payinBook, balance from ".TBL_CUSTOMER." where customerID = '".$_POST[customerID]."' ");

			$payinBookNumber = $payinCheck ["payinBook"];

			$balance = $payinCheck ["balance"];
			if ($_POST["payinBook"] != $payinBookNumber ){
				$strQuery = "insert into  agents_customer_account (customerID ,Date,tranRefNo ,payment_mode,Type ,amount, modifiedBy) 
											 values('".$customerID."','".$tran_date."','','Customer Registered','WITHDRAW','".$payinAmount."','".$modifyby."'
											 )";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());

				$balance = $payinCheck ["balance"];

				$balance = $balance - $payinAmount ;
			}

			$customerCode = $_POST["payinBook"];

		}else
		{
			$balance = 0;

			$numberCheck = selectFrom("select accountName from ".TBL_CUSTOMER." where customerID = '".$_POST[customerID]."' ");

			$customerCode = $numberCheck["accountName"];
		}

		// Added by Usman Ghani aginst ticket #3472: Now Transfer - Phone/Mobile
		$phone = checkValues($_POST["Phone"]);
		$mobile = checkValues($_POST["Mobile"]);
		if ( defined("CONFIG_PHONES_AS_DROPDOWN")
			&& CONFIG_PHONES_AS_DROPDOWN == 1 )
		{
			if ( $_POST["phoneType"] == "phone" )
			{
				$phone = checkValues($_POST["Phone"]);
				$mobile = "";
			}
			else
			{
				$phone = "";
				$mobile = checkValues($_POST["Mobile"]);
			}
		}
		// End of code against ticket #3472: Now Transfer - Phone/Mobile


		$arguments = array("flag"=>"getDatedForComments","senderRemarks" => $_POST["senderRemarks"]);
		$updateUserArr = gateway("CONFIG_INSERT_DATE_IN_COMMENTS",$arguments,CONFIG_INSERT_DATE_IN_COMMENTS);
		$getDatedForComments = $updateUserArr["values"]; // "\n[".date('j F Y')."]\n";

		/*
        * 7132 - Premier FX
        * Added account manager dropdown on add/udate sender page
        * by Aslam Shahid
        */
		$arguments = array("flag"=>"updateCustomerData","parentID" => $_POST["customerParentID"]);
		$updateUserArr = gateway("CONFIG_POPULATE_USERS_DROPDOWN_SENDER",$arguments,CONFIG_POPULATE_USERS_DROPDOWN_SENDER);
		$updationKeyValues = $updateUserArr["keysValues"]; // ",parentID = '".$arguments["parentID"]."' ";

		$arguments = array("flag"=>"updateCustomerData","source" => $_POST["source"]);
		//debug($arguments);
		$updateUserArr = gateway("CONFIG_SOURCE_FIELD_CUSTOMER",$arguments,CONFIG_SOURCE_FIELD_CUSTOMER);
		$updationKeyValues .= $updateUserArr["keysValues"]; // ",parentID = '".$arguments["parentID"]."' ";

		$arguments = array("flag"=>"updateCustomerData","tradingAcccount" => $_POST["tradingAcccount"]);
		$updateUserArr = gateway("CONFIG_TRADING_ACCOUNT_CUSTOMER",$arguments,CONFIG_TRADING_ACCOUNT_CUSTOMER);
		$updationKeyValues .= $updateUserArr["keysValues"]; // ",parentID = '".$arguments["parentID"]."' ";

		$QuerOldData = "SELECT * FROM ".TBL_CUSTOMER." WHERE customerID ='".$_POST["customerID"]."'";
		$arrOldData = selectFrom($QuerOldData);

		$Querry_Sqls = "update ".TBL_CUSTOMER." set  agentID ='".checkValues($agentIds)."',
	 accountName= '$customerCode',
	 title='".checkValues($_POST["Title"])."', 
	 other_title='".checkValues($_POST["other_title"])."', 
	 firstName='".checkValues($_POST["firstName"])."', 
	 lastName='".checkValues($_POST["lastName"])."', 
	 middleName='".checkValues($_POST["middleName"])."', 
	 secMiddleName = '".checkValues($_POST["secMiddleName"])."',
	 Address ='".str_replace("'","\'",$_POST["Address"])."',	 
	 Address1 ='".str_replace("'","\'",$_POST["Address1"])."',
	 proveAddress = '".($_POST["proveAddress"] != "" ? $_POST["proveAddress"] : "N")."',
	 dob =  '".$_POST["dobYear"]."-".$_POST["dobMonth"]."-".$_POST["dobDay"]."',
	 placeOfBirth = '".$_POST["placeOfBirth"]."',
	 IDExpiry = '".$idExpiryDate."',
	 IDissuedate = '".$idIssueDate."',
	 City ='".str_replace("'","\'",$_POST["City"])."', 
	 Zip='".$_POST["Zip"]."',
	 Country='".$_POST["Country"]."',
	 countryOfResidence='".$_POST["Countryaddress"]."',
	 email =  '".$_POST["custEmail"]."',
	 State='".checkValues($_POST["State"])."', 
	 Phone='".$phone."', 
	 Mobile='".$mobile."',
	 otherId = '".$_POST["Otherid"]."', 
	 otherId_name = '".$_POST["Otherid_name"]."',
	 issuedBy = '".$_POST["issuedBy"]."',
	 IDType = '".$_POST["IDType"]."',
	 IDNumber = '".$_POST["IDNumber"]."',
	 documentProvided = '".$_SESSION["documentProvided"]."',
	 balance='".$balance."',
	 payinBook = '".$_POST["payinBook"]."', 
	 customerName='".$customerName."',
	 profession = '".$_POST["profession"]."', 
	 fillingNo = '".$_POST["fillingNo"]."' ,
	 proveAddType = '".$_POST["proveAddressType"]."', 
	 SOF = '".$_POST["sonOf"]."',
	 SOF_Type = '".$_POST["sonOfType"]."'
	 ".$updationKeyValues."
	 where customerID ='".$_POST["customerID"]."'";
		update($Querry_Sqls);

		if($ttTransFlag && $_REQUEST["callFrom"]=="TT")
		{
			$CustStatus=$_REQUEST["CustStatus"];
			$isCompliance=$_REQUEST["isCompliance"];
			changeStatusSen($CustStatus,$_POST["customerID"],$isCompliance);

		}

		if(count($arrListId) > 0){
			for($k=0;$k<count($arrListId);$k++)
			{
				if(!empty($arrListId[$k])){
					$Querry_Sqls = "update ".TBL_COMPLIANCE_MATCH_LIST." 
						  set
							  userDataBaseID = '".$_POST["customerID"]."'
						  where 
								matchListID = ".$arrListId[$k]."
						   ";

					update($Querry_Sqls);
				}
			}
		}
		if(CONFIG_ADD_REF_NUMBER=="1" && CONFIG_UPDATE_CUSTOMER_NUMBER == "1" && $_POST["accountName"]!=""){
			update("UPDATE ".TBL_CUSTOMER." SET accountName='".trim($_POST["accountName"])."' WHERE customerID='".$_POST["customerID"]."' ");
		}
		if($categoryFlag){
			$categorySql = "update ".TBL_CUSTOMER." set cust_category_id = '".$_REQUEST["custCategory"]."' where customerID = '".$_POST["customerID"]."'";
			update($categorySql);
		}
		// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
		if ( !empty( $_REQUEST["IDTypesValuesData"] )  || !empty($_REQUEST["arrMultipleIds"]))
		{
			if(isset($_REQUEST["arrMultipleIds"]))
				$idTypeValuesData = unserialize(stripslashes(stripslashes($_REQUEST["arrMultipleIds"])));
			else
				$idTypeValuesData = $_REQUEST["IDTypesValuesData"];

			/*
              if(CHECK_ID_INFO_FROM_DB == "1"){
                $queryIDCheckDB = "SELECT count(*) as record FROM user_id_types WHERE user_id = '".$_POST["customerID"]."' AND id_number != '' LIMIT 1";
                $resultIDCheckDB = mysql_query($queryIDCheckDB) or die(mysql_error());
                $entry = mysql_fetch_array($resultIDCheckDB);
                if($entry['record'] > 0)
                    $idFlag = true;
            }
            */

			foreach( $idTypeValuesData as $idTypeValues )
			{
				if ( !empty( $idTypeValues["id_type_id"] )
					&& !empty( $idTypeValues["id_number"] )
					&& !empty( $idTypeValues["issued_by"] )
					&& !empty( $idTypeValues["expiry_date"] ))
				{
					$issueDate = $idTypeValues["issue_date"]["year"] . "-" . $idTypeValues["issue_date"]["month"] . "-" . $idTypeValues["issue_date"]["day"];
					$expiryDate = $idTypeValues["expiry_date"]["year"] . "-" . $idTypeValues["expiry_date"]["month"] . "-" . $idTypeValues["expiry_date"]["day"];

					if ( !empty($idTypeValues["id"]) )
					{
						// If id is not empty, its an update request.
						// Update the existing record in this case.

						$updateQuery = "update user_id_types 
										set 
											id_number='" . $idTypeValues["id_number"] . "', 
											issued_by='" . $idTypeValues["issued_by"] . "', 
											issue_date='" . $issueDate . "', 
											expiry_date='" . $expiryDate . "' 
										where 
											id='" . $idTypeValues["id"] . "'";
						update( $updateQuery );
						/*
                         * @Ticket #4794
                         */
						if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
							update("update user_id_types set notes='".mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$idTypeValues["id"]."'");

						}
					}
					else
					{
						// If id is empty, its an insert request.
						// Insert new reocrd in this case.



						$insertQuery = "insert into user_id_types
											(
												id_type_id, 
												user_id, 
												user_type, 
												id_number, 
												issued_by, 
												issue_date, 
												expiry_date 
											)
										values
											(
												'" . $idTypeValues["id_type_id"] . "', 
												'" . $_REQUEST["customerID"] . "', 
												'C', 
												'" . $idTypeValues["id_number"] . "', 
												'" . $idTypeValues["issued_by"] . "', 
												'" . $issueDate . "', 
												'" . $expiryDate . "'
											)";
						insertInto( $insertQuery );
						/*
                         * @Ticket #4794
                        */
						$lastIdInsertId = @mysql_insert_id();

						if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
							update("update user_id_types set notes='". mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$lastIdInsertId."'");
						}
					}
				}
			}
		}

		if ( !empty( $_SESSION["IDTypesValuesData"] ) )
			unset( $_SESSION["IDTypesValuesData"] );

		// End of code against #3299: MasterPayex - Multiple ID Types
		$descript ="Customer data is updated";

		activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["customerID"],TBL_CUSTOMER,$descript);

		$id_activity_id = mysql_insert_id();

		logChangeSet($arrOldData,$modifyby,"customer","customerID",$_POST["customerID"],$_SESSION["loginHistoryID"]);

		$modifyHistoryId = mysql_insert_id();

		$insertActivityId = "UPDATE ".TBL_AUDIT_MODIFY_HISTORY." SET id_activity_id = ".$id_activity_id." WHERE id = ".$modifyHistoryId."";

		update($insertActivityId);

		insertError(CU11);

		if($SenderIdFlag)
		{

			if(isset($_REQUEST["docCategory2"]))
				$docCategory = unserialize(stripslashes(stripslashes(stripslashes($_REQUEST["docCategory2"]))));
			else
				$docCategory = $_REQUEST["docCategory"];


			if(isset($_REQUEST["docExpiryData2"]))
				$expiryValues = unserialize(stripslashes(stripslashes(stripslashes($_REQUEST["docExpiryData2"]))));
			else
				$expiryValues = $_REQUEST["docExpiryData"];

			$arrFinalDocExpiry = array();
			//$expiryValues = $_REQUEST["docExpiryData"];
			foreach($docCategory as $docKey => $docVal)
			{
				$expiryDate = $expiryValues[$docKey]["expiry_date"]["year"] . "-" . $expiryValues[$docKey]["expiry_date"]["month"] . "-" . $expiryValues[$docKey]["expiry_date"]["day"];
				$arrFinalDocExpiry[$docKey] = $expiryDate;
			}
			$arrCustDoc = serialize($arrFinalDocExpiry);
			$strUpdateCustDocSql = "update customer set custDocumentProvided = '".$arrCustDoc."' where customerID = '".$_POST["customerID"]."'";
			update($strUpdateCustDocSql);
		}
		/**
		 * To retains the remarks about a customer/sender
		 * at its creation and display at the add transaction
		 * @Ticket #4230
		 */
		if(CONFIG_REMARKS_ABOUT_SENDER == "1")
		{
			$strUpdateCustomerSql = "update customer set senderRemarks = '".$getDatedForComments.addslashes($_REQUEST["senderRemarks"])."' where customerID = '".$_POST["customerID"]."'";
			//update($strUpdateCustomerSql);
		}

		if(CONFIG_PASSPORT_VALIDATION == "1"){
			$strUpdateCustPassportSql = "update customer set passport_number = '".$_REQUEST["passportValidate"]."' where customerID = '".$_POST["customerID"]."'";
			update($strUpdateCustPassportSql);
		}

// Added by Niaz Ahmad at 03-06-2009 @4997
		if(senderFullnameFlag){
			$strUpdateCustFullnametSql = "update customer set customerType = '".$_REQUEST["senderFullName"]."' where customerID = '".$customerID."'";
			update($strUpdateCustFullnametSql);
		}

		$startMessage="Congratulations! Your account information has been updated successfully. <br> Your new acoount information is as follows:";
		include_once ("mailCustomerInfo.php");
		$To = $_POST["custEmail"];
		$fromName = SUPPORT_NAME;
		$fromEmail = SUPPORT_EMAIL;
		$subject = "Sender Account Updated ";
		//mail:debug(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR);
		//mail:debug($_POST["sendEmail"]);
		//mail:debug(CONFIG_MAIL_GENERATOR);

		if(CONFIG_MAIL_GENERATOR == '1' || $_POST["sendEmail"]=='Y'){
			//mail:debug($To);
			
			sendMail($To, $subject, $data, $fromName, $fromEmail);
			if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1"){
				if(!empty($_POST["customerParentID"])){
					$sqlAccMananger = "SELECT name,email FROM ".TBL_ADMIN_USERS." WHERE userID='".trim($_POST["customerParentID"])."' ";
					//mail:debug($sqlAccMananger);
					$accManangerData = selectFrom($sqlAccMananger);
					//mail:debug($accManangerData['email']);
					if(!empty($accManangerData['email'])){
						sendMail($accManangerData['email'],$subject,$message,$headers);
					}
				}
			}
		}


//mail:debug("stop",true);
//debug($_GET["pageID"]);

		if (($_GET["pageID"]=='1') || ($_GET["pageID"]=='search')){

			if($uploadImageVar == "Y"){
				$backURL = "uploadCustomerImage.php?mode=update";
			}else{
				$backURL = "search-edit-cust.php?msg=Y&agentID=".$_POST["agentID"]."&custName=". $_GET["custName"]."&r=".$_GET["r"]."&nameType=". $_POST["nameType"];
			}

		}elseif($_GET["pageID"]=='alertPage'){

			if(CONFIG_NEW_ALERT_CONTROLLER_PAGE == "1")
				$backURL = "viewAlertControllerNew.php?&newOffset=".$_REQUEST["newOffset"]."&userName=".$_REQUEST["userName"]."&userType=".$_REQUEST["userType"]."&IDType=".$_REQUEST["IDType"]."&alertType=".$_REQUEST["alertType"]."&Submit=".$_REQUEST["Submit"];
			else
				$backURL = "viewAlertController.php?agentID=".$_POST["agentID"]."&custName=". $_GET["custName"]."&r=".$_GET["r"]."&alertID=".$_POST["alertID"];

		}else{
			//debug($_GET, true);
			if($uploadImageVar == "Y"){
				$backURL = "uploadCustomerImage.php?mode=update&custName=". $_GET["custName"]."&pageID=search";
			}else{
				$backURL = "search-cust.php?msg=Y&agentID=".$_POST["agentID"]."&custName=". $_GET["custName"]."&r=".$_GET["r"];
			}

		}

//debug($_SESSION['page']);
//exit("2026");
		if($_SESSION['page'] == '1')
			$backURL = "search-edit-cust.php?refresh=1";

		//$backURL = "senders_list_registered_from_website.php?refresh=1";
		elseif($_SESSION['page'] == '2')
			$backURL = "customers_without_email.php?refresh=1";

	}



	if($from == "popUp" || $from == 'buySellCurr.php' || ($ttTransFlag==true && $_REQUEST["callFrom"]=="TT"))
	{
		$pageID = $from;
		$_SESSION["customerID"] = $customerID;
		if($uploadImageVar == "Y"){
			$returnPage = "uploadCustomerImage.php";

		}elseif($from != '' && $from != 'popUp')
		{
			if(!$ttTransFlag && $_REQUEST["callFrom"]!="TT")
			{
				$returnPage = $from;
				$from = 'popUp';
			}
		}


		//debug($returnPage."?msg=Y&success=Y&transID=".$_GET['transID']."&uploadImage=".$_REQUEST["uploadImage"]."&sendEmail=".$_REQUEST["sendEmail"]."&custmail=".$_POST["custEmail"]."&fname=".$_POST["firstName"]);
		//debug($_REQUEST["callFrom"]);
		//debug("uploadImageVa".$uploadImageVa);
		//debug("ttTransFlag".$ttTransFlag);
		//debug("Stop",true)
		?>
		<script language="javascript">
			<? if($_REQUEST["callFrom"]!="TT")
			{ ?>
			opener.document.location = "<?=$returnPage?>?msg=Y&success=Y&transID=<?=$_GET['transID']?>&uploadImage=<?=$_REQUEST["uploadImage"]?>&sendEmail=<?=$_REQUEST["sendEmail"];?>&custmail=<?=$_POST["custEmail"];?>&fname=<?=$_POST["firstName"]?>&customerID=<?=$_REQUEST["customerID"]?>&agentID=<?=$agentIds?>";

			<?  } //$returnPage .= "?pageID=popUp";
			if($uploadImageVar == "Y"){
			$returnPage .= "?msg=Y&success=Y&pageID=".$pageID;
			if(!empty($customerID)){

				$returnPage .= "&customerID=".$customerID;
			}else{
				$returnPage .= "&customerID=".$_REQUEST["customerID"];
			}
			if($_REQUEST["sendEmail"]!="" && $_REQUEST["uploadImage"])
			{
				$returnPage .= "&uploadImage=".$_REQUEST["uploadImage"]."&sendEmail=".$_REQUEST["sendEmail"]."&custEmail=".$_POST["custEmail"]."&fname=".$_POST["firstName"]."";
			}
			if($ttTransFlag && $_REQUEST["callFrom"]=="TT")
			{
				$returnPage .= "&callFrom=".$_REQUEST["callFrom"];
			}
			redirect($returnPage);?>

			<? }else{ ?>

			<?


			?>
			<?php if($ttTransFlag && $_REQUEST["callFrom"]=="TT")
			{
			if(empty($customerID))
			{
				$upCust=$customerID=$_POST["customerID"];
				$newCust='';

			}
			else
			{
				$newCust=1;
				$upCust=0;
			}?>
			window.opener.document.TT_form.cusdi.value ='<?=$customerID?>';
			window.opener.LoadCustInfo('<?=$upCust?>','<?=$newCust?>');
			<?php }?>
			window.close();
			<? } ?>

		</script>
		<?

	}
	else
	{
		$custType="C";
		$remarks = $_REQUEST["senderRemarks"];
		if(!empty($_REQUEST["customerID"]))
			$customerID = $_REQUEST["customerID"];
		/**
		 * Sendmail on update Customer which are registered from the website
		 */
		if(!empty($customerID) && isset($_POST['sendEmail']) && $_POST['sendEmail'] == 'Y' && isset($_POST['tradingAcccount']) && !empty($_POST['tradingAcccount'])){
			include_once("lib/html_to_doc.inc.php");
			$htmltodoc= new HTML_TO_DOC();
			include_once ("mailCustomerInfo.php");

			//exit();
			//$fp = fopen("htmlEmaiWelcomeLetter.html", 'w+');
			$tmpfname = tempnam("/tmp", "WELCOME_".$_POST["firstName"]."_");
			$fpOpen = fopen($tmpfname, 'w+');
			if($fpOpen){
				//mail:debug($fpOpen);

				fwrite($fpOpen, $data);
				fclose($fpOpen);
				//include("lib/html_to_doc.inc.php");
				//$htmltodoc= new HTML_TO_DOC();
				//$htmltodoc->createDoc($timeStamp.".html","test");
			}
			//debug($fpOpen);
			//debug($data);
			//exit();

			$To = $_POST["custEmail"];
			$fromName = SUPPORT_NAME;
			$fromEmail = SUPPORT_EMAIL;
			//$htmltodoc->createDoc("htmlEmaiWelcomeLetter.html","test");
			//$htmltodoc->createDocFromURL("http://yahoo.com","test");
			$subject = SYSTEM;//"Premier FX ";
			//$subject = "Deal Contract ";
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$num = md5( time() );
			$whrClauseAdminTable = " and userID = '".$_POST["agentID"]."' ";
			if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR == "1"){
				$whrClauseAdminTable = " and userID = '".$_POST["customerParentID"]."' ";
			}
			$agentUserName=selectFrom(" select name,email from admin where 1 $whrClauseAdminTable ");
			$agentEmail = $agentUserName['email'];

			//with scanned copy
			$emailContent1 = selectFrom("select * from email_templates left join events on email_templates.eventID=events.id where events.id='2' and email_templates.status = 'Enable' ");
			//debug($emailContent1);
			$logoCompany = "<img border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/mail_signature.jpg?v=1' />";
			$message11 = $emailContent1["body"];

			$subject1 = $emailContent1["subject"];
			$varEmail1 = array("{customer}","{logo}","{accountmanager}");
			//$contentEmail1 = array($_POST["firstName"]." ".$_POST["middleName"]." ".$_POST["lastName"],$logoCompany,$agentUserName["name"]);
			$contentEmail1 = array($_POST["firstName"],$logoCompany,$agentUserName["name"]);
			$messageT1 = str_replace($varEmail1,$contentEmail1,$message11);
			//debug($messageT1);
			//without scanned copy
			$emailContent2 = selectFrom("select * from email_templates left join events on email_templates.eventID=events.id where events.id='7' and email_templates.status = 'Enable' ");
			$logoCompany = "<img border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/mail_signature.jpg?v=1' />";
			$message12 = $emailContent2["body"];
			$subject2 = $emailContent2["subject"];
			$varEmail2 = array("{customer}","{logo}","{accountmanager}");
			//$fullName = $_POST["firstName"]." ".$_POST["lastName"];
			$fullName = $_POST["firstName"];
			$accManagerName=explode(" ",$agentUserName["name"]);
			$countName=count($accManagerName);
			if($countName>2){
				$fullNameAccM=$accManagerName[0]." ".$accManagerName[$countName-1];

			}
			else{
				$fullNameAccM=$accManagerName[0]." ".$accManagerName[1];

			}
			$contentEmail2 = array($fullName,$logoCompany,$fullNameAccM);
			$messageT2 = str_replace($varEmail2,$contentEmail2,$message12);



			//$message="\n Dear ".$_POST["firstName"].",\n<br>";
			//$message.="\n<br />Please find attached details of your trading account with Premier FX. To"."\n\r";
			$passQuery=selectFrom(" select documentId from document_upload where userId='".$customerID."'");
			$IDTypesValuesData = $_REQUEST["IDTypesValuesData"];
			$IDDetailsFlag = false;
			//debug($idTypeValuesData);


			foreach( $idTypeValuesData as $idTypeValues )
			{

				//if(($idTypeValues["id_number"]=="" && $idTypeValues["issued_by"]=="") && (!isset($idFlag) && (!$idFlag)){
				if($idTypeValues["id_number"] !="" && $idTypeValues["issued_by"] !="")
					$IDDetailsFlag = true;

			}
			//debug($passQuery);
			//debug($IDTypesValuesData);
			//debug($IDDetailsFlag,true);
			if(empty($passQuery)){
				$subject = $subject2;
				$message = $messageT2;
				//$message .= "\n<br> Please find attached details of your trading account with Premier FX."."\n<br>" ;
				//$message.= "\n<br> To complete your application we will require a scanned copy of your passport and a proof of address document. This is common practice for all financial institutions under money laundering regulations."."\n<br>";
			}
			else{
				$subject = $subject1;
				$message = $messageT1;
				//$message.= "\n<br> Please find attached details of your trading account with Premier FX."."\n<br>" ;
				//$message.="\n<br> Please do not hesitate to contact me if you have any further questions. I look forward to being of service to you in the near future."."\n<br>";
			}
			//$message.=" \n<br>Regards,"."\n<br />";


			//$agentIDQuery=selectFrom(" select parentID from customer where customerID='".$customerID."'");

			//$message .= "\n ".$agentUserName["name"]."\n";
			//Premier Support\n
			/**
			 * #10408: Premier Exchange: Wrong office address On Emails
			 * Short Description
			 * Address is changed against the ticket number mentioned
			 */
			/*$message .= "
            <table>
                    <tr>
                        <td align='left' width='30%'>
                        <img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width='20%'>&nbsp;</td>
                        <td align='left'>
                            <font size='2px'><font color='#F7A30B'>UK OFFICE</font>\n<br>
                            55 Old Broad Street,London, EC2M 1RX.\n<br>
                            <font color='#F7A30B'>Tel:</font> +44 (0)845 021 2370\n<br>\n<br>
                            <font color='#F7A30B'> PORTUGAL OFFICE</font> \n<br>
                            Rua Sacadura Cabral, Edificio Golfe lA, 8135-144 Almancil, Algarve.  \n<br>
                            <font color='#F7A30B'>Tel:</font> +351 289 358 511\n<br>
                            <font color='#F7A30B'>FAX:</font> +351 289 358 513\n<br>\n<br>
                            <font size='2px'><font color='#F7A30B'>SPAIN OFFICE</font>\n<br>
                            C/Rambla dels Duvs, 13-1, 07003, Mallorca. \n<br>
                            <font color='#F7A30B'>Tel:</font> +34 971 576 724
                             \n <br>
                             <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font>
                        </td>
                    </tr>
                </table>";*/

			$strresume = $tmpfname;//"htmlEmaiWelcomeLetter.html";
			// This two steps to help avoid spam
			$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
			$headers .= "X-Mailer: PHP v".phpversion()."\r\n";

			include_once ('../pdfClass/class.ezpdf.php');

			// Generate pdf document starts here
			$args = array('agentID'=>$_REQUEST["agentID"],'customerID'=>$customerID,'customerParentID'=>$_REQUEST["customerParentID"]);
			$sfsWelcomeData = attachPDFWelcomeLetterEmailOnReg($args);
			if($_POST['sendServiceEmail']!="" && CONFIG_STEPS_STRESS_FREE_SERVICE_EMAIL=='1'){
				$sfsData = attachPDFServiceEmail($args);
			}
			//$sfsUserGuide = attachPDFUserGuide($args);
			//debug($sfsUserGuide);
			// Prepare Email for Sender and Account Manger
			$fromName  = SYSTEM;
			$fromEmail = $fromEmail;
			$welcomeLetter = "WelcomeLetter.html";
			$stress_free_service_name = $sfsData['file_name'];
			$stress_free_service_path = $sfsData['file_path'];

			$welcome_letter_name = $sfsWelcomeData['file_name'];
			$welcome_letter_path = $sfsWelcomeData['file_path'];

			if(!empty($agentEmail)){
				$fromName  = $agentUserName["name"];
				$fromEmail = $agentEmail;
			}

			$sendermailer = new PHPMailer();
			$agentmailer = new PHPMailer();

			$sendermailer->FromName = $agentmailer->FromName = $fromName;
			$sendermailer->From = $agentmailer->From = $fromEmail;
			if(CONFIG_SEND_EMAIL_ON_TEST == "1")
			{
				$sendermailer->AddAddress(CONFIG_TEST_EMAIL,'');
			}
			else
			{
				$sendermailer->AddAddress($To,'');
			}
			$agentmailer->Subject = $sendermailer->Subject = $subject;

			if(!$IDDetailsFlag && CONFIG_SEND_EMAIL_ON_TEST != "1")
			{
				//Add CC
				if($emailContent2['cc'])
					$sendermailer->AddAddress($emailContent2['cc'],'');

				//Add BCC
				if($emailContent2['bcc'])
					$sendermailer->AddBCC($emailContent2['bcc'],'');
			}
			else if(CONFIG_SEND_EMAIL_ON_TEST != "1")
			{
				//Add CC
				if($emailContent1['cc'])
					$sendermailer->AddAddress($emailContent1['cc'],'');

				//Add BCC
				if($emailContent1['bcc'])
					$sendermailer->AddBCC($emailContent1['bcc'],'');




			}
			$sendermailer->IsHTML(true);
			$agentmailer->IsHTML(true);

			// attachements

			//$sendermailer->AddAttachment($strresume, $welcomeLetter,"base64");
			//$agentmailer->AddAttachment($strresume, $welcomeLetter,"base64");
			$sendermailer->AddAttachment($welcome_letter_path, $welcome_letter_name,"base64");
			$agentmailer->AddAttachment($welcome_letter_path, $welcome_letter_name,"base64");

			if(!empty($stress_free_service_name)){
				$sendermailer->AddAttachment($stress_free_service_path, $stress_free_service_name,"base64");
				$agentmailer->AddAttachment($stress_free_service_path, $stress_free_service_name,"base64");

			}

			// Body
			$agentmailer->Body = $sendermailer->Body = $message;

			if(CONFIG_MAIL_GENERATOR == '1' && ($_POST["sendEmail"]=='Y' || $_POST["sendServiceEmail"]=='Y')){
				if(!isset($senderEmailFlag))
				{
					if(CONFIG_EMAIL_STATUS == "1")
					{
						$senderEmailFlag = $sendermailer->Send();
					}
				}
				if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1"){
					if(!empty($_POST["customerParentID"])){
						$sqlAccMananger = "SELECT name,email FROM ".TBL_ADMIN_USERS." WHERE userID='".trim($_POST["customerParentID"])."' ";
						$accManangerData = selectFrom($sqlAccMananger);
						if(!empty($accManangerData['email'])){
							if(CONFIG_SEND_EMAIL_ON_TEST == "1")
							{
								$agentmailer->AddAddress(CONFIG_TEST_EMAIL,'');

							}
							else
							{
								$agentmailer->AddAddress($accManangerData['email'],'');
							}
							if(!isset($agentEmailFlag))
							{
								if(CONFIG_EMAIL_STATUS == "1")
								{
									$agentEmailFlag = $agentmailer->Send();
								}
							}
						}
					}
				}
			}
			// end mail on update a sender from website
		}
		$query = "insert into userRemarksHistory(userID,userType,remarks,datetime,updated_by) values('$customerID','$custType','$remarks','".date("Y-m-d H:i:s")."','$modifyby')";
		insertInto($query);
//debug($query,true);
		if($uploadImageVar == "Y"){
			$backURL = "uploadCustomerImage.php?msg=Y2&from=".$from."&pageID=".$_GET["pageID"]."&r=".$_GET["r"]."$alertID=".$_POST["alertID"];
			if(!empty($customerID)){
				$backURL .= "&customerID=".$customerID;
			}else{
				$backURL .= "&customerID=".$_REQUEST["customerID"];
			}

		}


		$backURL .= "&success=Y";
		//debug($backURL, true);
		if($ttTransFlag && $_REQUEST["callFrom"]=="TT")
		{
			if(empty($customerID))
			{
				$upCust=$customerID=$_POST["customerID"];
				$newCust='';
			}
			else
			{
				$newCust=1;
				$upCust=0;
			}

			//debug($customerID,true);
			echo '<script>
					window.opener.document.TT_form.cusdi.value ='.$customerID.';
					window.opener.LoadCustInfo('.$upCust.','.$newCust.');
			
				
				</script>';

			exit;

		}
		else
			redirect($backURL);
	}
}

// end compliance check
function changeStatusSen($CustStatus,$customerID,$isCompliance)
{
	if(empty($CustStatus) && empty($isCompliance))
	{
		$CustStatus='Enable';
		$isCompliance='N';
	}


	$updateQuery = " update customer set customerStatus = '".$CustStatus."', isCompliance= '".$isCompliance."' where customerID = '".$customerID."'";
	//debug($updateQuery,true);

	update($updateQuery);
}
?>