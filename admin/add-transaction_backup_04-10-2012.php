<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$TRANSACTION_DATE = dateFormat($date,1);

 /*
 * 7897 - Premier FX
 * Rename 'Create Transaction' text in the system.
 * Enable  : string
 * Disable : "0"
 */
$strTransLabel = 'Transaction';
if(defined('CONFIG_SYSTEM_TRANSACTION_LABEL') && CONFIG_SYSTEM_TRANSACTION_LABEL!='0')
	$strTransLabel = CONFIG_SYSTEM_TRANSACTION_LABEL;
	
/*	#4657
	Author : Aslam Shahid
	Description :	Right for create transaction. range of time given.
					Preference given to the Individually time assigned i.e Super Agent or Super Distributor then All.
					To only work for individually. Disable right for All on Valid System Time functionality.
*/
if(defined("CONFIG_COUNTRY_CODE") && CONFIG_COUNTRY_CODE!="0"){
	$timestamp = strtotime(getCountryTime(CONFIG_COUNTRY_CODE));
}
if($timestamp=="" || empty($timestamp)){
	$timestamp = time();
}
if(defined("CONFIG_ADD_USER_RIGHT") && CONFIG_ADD_USER_RIGHT!="0" && $_REQUEST["create"]!="" && $_REQUEST["transID"]=="" && $_REQUEST["benID"]=="")
{
$queryRights = "select * from setUserRights where userGroup = '".$agentType."' AND rightName  = 'Create Transaction' AND rightStatus='1'";
$rightsData = selectFrom($queryRights);

if($rightsData==""){
	$queryRights = "select * from setUserRights where userGroup = 'All' AND rightName  = 'Create Transaction' AND rightStatus='1'";
	$rightsData = selectFrom($queryRights);
}
$rightValues = explode("|",$rightsData["rightValue"]);
$rightValue1 =$rightValues[0] ; // lower time limit.
$rightValue2 =$rightValues[1] ; // upper time limit.
	if($rightsData !="" && ($timestamp < strtotime($rightValue1) || $timestamp > strtotime($rightValue2)) && $rightValue1!="" && $rightValue2!=""){
		insertError("You are only allowed to Create Transaction between ".date("g A",strtotime($rightValue1))."-".date("g A",strtotime($rightValue2))."");
		$msgTime = "Y";
	}
}
include ("javaScript.php");
$parentID = $_SESSION["loggedUserData"]["userID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$Code = MANUAL_CODE;
$flagBenCountry = 0;
$fromTotalConfig = CONFIG_CALCULATE_FROM_TOTAL;

$countryBasedFlag = false;
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}
$locationString = "Location ID";
if(defined("CONFIG_COLLECTION_LOCATION_ID_TITLE") && CONFIG_COLLECTION_LOCATION_ID_TITLE!="0"){
	$locationString = CONFIG_COLLECTION_LOCATION_ID_TITLE;
}

/* #4804
   Now when manual commision is ON then it may also be
   used in Ajax calculation functionality of Amount/Local Amount
   by Aslam Shahid
*/
$manualCommAjaxFlag = false;
if(defined("CONFIG_USE_MANUAL_COMMISSION_AJAX") && CONFIG_USE_MANUAL_COMMISSION_AJAX=="1"){
	$manualCommAjaxFlag = true;
}
/**  
 * #6747:Premier Exchange
 * Value Date can be assigned and then transaction goes in Value Date Transactions Queue in left menu.
 * to Display link in left menu under Transaction module you need to add right from user functionality for
 * "Validate Value Dated Transactions"
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
 $valueDateFlag = false;
 if(CONFIG_VALUE_DATE_TRANSACTIONS=="1"){
	$valueDateFlag = true;
 }

/**  
 * #6747:Premier Exchange
 * Value Date can be assigned and then transaction goes in Value Date Transactions Queue in left menu.
 * to Display link in left menu under Transaction module you need to add right from user functionality for
 * "Validate Value Dated Transactions"
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
 $useTransDetailsFlag = false;
 if(CONFIG_USE_TRANSACTION_DETAILS=="1"){
	$useTransDetailsFlag = true;
 }

/*
To get focus on the button at the end

*/
if(CONFIG_SEND_ONLY_BUTTON == 1){
	$focusButtonID = "transSend";
}else{
	$focusButtonID = "sendID";
	}

if(CONFIG_CUSTOM_SENDER == '1')
{
	$customerPage = CONFIG_SENDER_PAGE;
}else{
	$customerPage = "add-customer.php";
	
	}

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}

if(CONFIG_TRANS_ROUND_LEVEL > 0)
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 4;
	
	}

function currencyValue()
{
	$compareCurrency = false;
	if(CONFIG_TRANS_ROUND_CURRENCY_TYPE == 'CURRENCY_TO')
	{
		if(strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyTo"].",") || CONFIG_TRANS_ROUND_CURRENCY == "" )
			$compareCurrency = true;	
	}elseif(CONFIG_TRANS_ROUND_CURRENCY_TYPE == 'CURRENCY_FROM')
	{
		if(strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyFrom"].",") || CONFIG_TRANS_ROUND_CURRENCY == "" )
			$compareCurrency = true;	
	}else
	{
		if((strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyTo"].",")) || (strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyFrom"].",")))
			$compareCurrency = true;	
	}
	return $compareCurrency;	
}

function RoundVal($val){
	
	if(CONFIG_TRANS_ROUND_LEVEL > 0)
	{
		$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
	}else{
		$roundLevel = 4;
		}
		
		
	$compareCurrency2 = currencyValue();
	
    if((strstr(CONFIG_ROUND_NUM_FOR, $_POST["moneyPaid"].",")) && $compareCurrency2)
	{
        if(CONFIG_TRANS_ROUND_NUMBER == 1)
		{
      		//$ArrVal = explode(".",$val);
       		//$return=$ArrVal[0];
			$return = round($val);
    	}else{
	        $return=round($val,$roundLevel);
        }
    }else{
      	$return=round($val,$roundLevel);        
    }
return $return;	
}



//$backUrl = "add-transaction.php;

if ($_GET["back"])
{
	$_SESSION["back"] = $_GET["back"];
}

if ($_GET["val"] != "")
	$_SESSION["amount_transactions"] = $_GET["val"];


if(CONFIG_TEST_TRANS_LOAD == "1")
{
		$confirmURL = "express-confirm-transaction-2.php";
}elseif(CONFIG_SKIP_CONFIRM == '1')
{
	$confirmURL = "add-transaction-conf.php";
}elseif(CONFIG_CUSTOM_RECIEPT == "1")
{
	
				if(CONFIG_SHOW_DEFAULT_RECIEPT == "1"){
		
							$confirmURL = "confirm-transaction.php";
		
				}else{
							$confirmURL = CONFIG_RECIEPT_NAME;
				}
}else{
	$confirmURL = "confirm-transaction.php";
}



if($agentType == "TELLER")
{
	
	$userDetails = selectFrom("select * from " . TBL_COLLECTION . " as c, ". TBL_TELLER. " as t  where t.loginName='$username' and t.collection_point = c.cp_id");
	
	$_SESSION["senderAgentID"]  = $senderAgentID = $userDetails["cp_ida_id"];
	
}elseif($agentType == "PAYING BOOK CUSTOMER"){
	
	$userDetails = selectFrom("select * from " . TBL_CUSTOMER . "   where accountName='$username' ");
	
 $_SESSION["senderAgentID"] = $senderAgentID = $userDetails["agentID"];
$_SESSION["customerID"]= $senderID = $userDetails["customerID"];
	
	}
	
	else{
	$userDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where username='$username'");
	//echo "NO TELLER $agentType";
}

if($_GET["act"]!="") {
	$_SESSION["act"] = $_GET["act"];
}


$_SESSION["calculateBy"] =  $_POST["calculateBy"];	


if($_POST["submitSearch"] == "Search")
{
	$_SESSION["searchTrans"] = $_POST["searchTrans"];
}
//echo($_SESSION["transType"]);
// echo("Ben ID Value is".$_GET["benID"]." and session value is".$_SESSION["benID"]);
if($_GET["benID"] != "" || (CONFIG_TRANS_WITHOUT_BENEFICIARY == '1' && $_SESSION['customerID']!='' && $_GET["transID"]==''))
{
		if($_SESSION["transType"] == "")
		{
				$_SESSION["transType"] = "Pick up";
				if (CONFIG_MODIFY_DEFAULT_PICKUP_TO_BANKTRANSFER == "Bank Transfer")
						$_SESSION["transType"] = CONFIG_MODIFY_DEFAULT_PICKUP_TO_BANKTRANSFER;
		}
}
// echo("transType is ".$_SESSION["transType"]);

//echo("at line 39 action is ".$_SESSION["act"]." transID  ".$_GET["transID"]."  and transType ".$_SESSION["transType"]);
if($_GET['benID'] != "")
{
		if($_SESSION["transType"] == "Bank Transfer")
		{
			$_SESSION["bankName"] 		= "";
			$_SESSION["branchCode"] 	= "";
			$_SESSION["branchName"] 	= "";
			$_SESSION["branchAddress"] 	= "";
			$_SESSION["swiftCode"] 		= "";
			$_SESSION["accNo"] 			= "";
			$_SESSION["ABACPF"] 		= "";
			$_SESSION["IBAN"] 			= "";		
			$_SESSION["ibanRemarks"]    = "";	
			$_SESSION["accountType"]    = "";
			$_SESSION["distribut"]      = "";
			$_SESSION["sortCode"] = "";
			$_SESSION["routingNumber"] = "";
		}
		//$_SESSION["collectionPointID"] = '';
		 if($countryBasedFlag){
			$serviceTypeField = ", serviceType, collectionPointId " ;
		 }
		$queryBen = "select Country ".$serviceTypeField."  from ".TBL_BENEFICIARY." where benID ='".$_GET["benID"]."'";
		//echo $queryBen;
		$benificiaryContent = selectFrom($queryBen);	
		$_SESSION["serviceType"]= strtoupper($benificiaryContent["serviceType"]);
	//	$contentExchagneRate = selectFrom("select *  from exchangerate where country = '".$benificiaryContent["Country"]."'");								
	//	$_SESSION["currencyTo"] = $contentExchagneRate["currency"];		
		if($_POST["currencyTo"] != "")
			$_SESSION["currencyTo"] = $_POST["currencyTo"];
		if($_POST["currencyFrom"] != "")
			$_SESSION["currencyFrom"] = $_POST["currencyFrom"];
		
		 if($countryBasedFlag){
			if($benificiaryContent["serviceType"]!=""){
				if($_SESSION["serviceType"]=="HOME DELIVERY")
					$_SESSION["transType"] = "Pick up";
				elseif($_SESSION["serviceType"]=="BOTH" || $_SESSION["serviceType"]=="BANK DEPOSIT" || $_SESSION["serviceType"]=="BANK TRANSFER" || $_SESSION["serviceType"]=="IBAN")
					$_SESSION["transType"] = "Bank Transfer";
			}
			if($_SESSION["transType"]=="Pick up"){
				$_SESSION["collectionPointID"] = $benificiaryContent["collectionPointId"];
			}
		 }
			
}



if($_GET['benAgentID'] != '')
{
	$_SESSION["benAgentID"] = $_GET['benAgentID'];	
}	

if ($_GET['collectionPointID'] != '') {	
	$_SESSION["collectionPointID"] = $_GET['collectionPointID'];
} else if ($_POST['collPoints'] != '' && CONFIG_COLLPOINTS_DROPDOWN == '1') {	// [by JAMSHED]
	$_SESSION["collectionPointID"] = $_POST['collPoints'];
}

if($_GET['ben_bank_id'] != '')
{
	$_SESSION["ben_bank_id"] = $_GET['ben_bank_id'];	
}
	
/*
foreach ($_SESSION as $k=>$v) 
{
	$str .= "$k : $v<br>";
}
echo $str;
*/
//echo "agent type: " . $username . $agentType;

if($_GET["search"] == "end")
{
	$_SESSION["searchTrans"] = "";	
}

if($_GET["create"] != "")
{
			$_SESSION["transType"]		= "";
			$_SESSION["useTransDetails"]	= "";
			$_SESSION["valueDate"]		= "";
			if($agentType != "PAYING BOOK CUSTOMER")
			{
				$_SESSION["customerID"] 	= "";
			}//customerid remains there if the user is paying book customer
			$_SESSION["benID"] 			= "";
			$_SESSION["moneyPaid"] 		= "";
			$_SESSION["transactionPurpose"] = "";
			$_SESSION["other_pur"]		 = "";
			$_SESSION["fundSources"] 	= "";
			$_SESSION["refNumber"] 		= "";
			$_SESSION["transAmount"] 	= "";
			$_SESSION["exchangeRate"] 	= "";
			$_SESSION["exchangeID"] 	= "";
			$_SESSION["localAmount"] 	= "";
			$_SESSION["totalAmount"] 	= "";
			$_SESSION["IMFee"] 			= "";
			$_SESSION["currencyTo"]		= "";
			$_SESSION["currencyFrom"]		= "";
			$_SESSION["discount"]       = "";
			$_SESSION["checkManualRate"] = "";
			$_SESSION["chDiscount"] 	= "";
			$_SESSION["discount_request"] = "";
			$_SESSION["transStatus"] = "";
			$_SESSION["transactionCreatedBy"] = "";

			// resetting Session vars for trans_type Bank Transfer
			$_SESSION["bankName"] 		= "";
			$_SESSION["branchCode"] 	= "";
			$_SESSION["branchName"] 	= "";
			$_SESSION["branchAddress"] 	= "";
			$_SESSION["swiftCode"] 		= "";
			$_SESSION["accNo"] 			= "";
			$_SESSION["ABACPF"] 		= "";
			$_SESSION["IBAN"] 			= "";
			$_SESSION["ibanRemarks"] = "";
			$_SESSION["accountType"] = "";
			$_SESSION['RequestforBD'] = "";
			
			$_SESSION["senderAgentID"]  = "";
			$_SESSION["collectionPointID"] = "";
			$_SESSION["distribut"] = "";
			
			
			$_SESSION["question"] 			= "";
			$_SESSION["answer"]     = "";	
			$_SESSION["tip"] = "";	
			$_SESSION["chequeNo"] = "";	
			if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
			{
				$_SESSION["chequeAmount"] = "";
			}
			
			$_SESSION["currencyCharge"] = "";
			
			//$_SESSION["admincharges"] = "";
			$_SESSION["bankCharges"] = "";
			$_SESSION["distribut"] ="";
			$_SESSION["transDate"] = "";
			$_SESSION["amount_transactions"] = "";
			$_SESSION["amount_left"] = "";
			$_SESSION["batch"] = "";
			$_SESSION["searchTrans"] = "";
			
			$_SESSION["benAgentParentID"] = "";
			$_SESSION["custAgentParentID"] ="";
			$_SESSION["custName"] = "";
			$_SESSION["customerSearch"] = "";
			$_SESSION["custCountry"] = "";
			$_SESSION["benCountry"] = "";
			$_SESSION["custAgentID"] = "";
			$_SESSION["notService"] = "";
			$_SESSION["dDate"] ="";
			$_SESSION["Declaration"] = "";
			$_SESSION["transID"] = "";
			$_SESSION["transSend"] = "";
			$_SESSION["act"] = "";
			$_SESSION["Submit"] = "";
			$_SESSION["mydDate"] = "";
			$_SESSION["imReferenceNumber"] = "";
			$_SESSION["document_remarks"] = "";
			$_SESSION["internalRemarks"] = "";
			$_SESSION["benIdPassword"] = "";
			$_SESSION["exRateLimitMin"] = "";
			$_SESSION["exRateLimitMax"] = "";
			
			$_SESSION["clientRef"] = "";
			
			$_SESSION["ben_bank_id"] = "";
			$_SESSION["bankingType"] = "";	
			$_SESSION["senderBank"] = "";		
			
			/**
			 * Added to fix the bug related to #3506
			 */
			$_SESSION["distribut"] = "";
			$_SESSION["benAgentID"] = "";

			$_SESSION["bankTransferType"] = "";
			
			$_SESSION["cardNo"] = "";
			$_SESSION["cardName"] = "";
			$_SESSION["cardExpiry"] = "";
			$_SESSION["docCategory"] = "";
			$_SESSION["transNote"] = "";
			$_SESSION["reverseCalculation"] = "";
			if(CONFIG_SENDER_ID_EXPIRED_WAVIER==1 )
				$_SESSION["docWaiver"]="";
			/* #6221
			 * By default manual exchange rate is selected
			 * by A.Shahid
			 */
			if(CONFIG_DEFAULT_MANUAL_EXCHANGE_RATE=="1")
				$_SESSION["checkManualRate"] = "Y";
	
}
//debug($_SESSION["checkManualRate"]);
if (CONFIG_ADMIN_ASSOCIATE_AGENT == '1' && strstr(CONFIG_ASSOCIATED_ADMIN_TYPE, $agentType.",")) {
	$adminLinkDetails = selectFrom("select linked_Agent from ".TBL_ADMIN_USERS." where userID = '".$parentID."'");
	
	$linkedAgent = explode(",", $adminLinkDetails["linked_Agent"]);
	$linkedAgentStr = implode("','", $linkedAgent);
	$userIDs = selectMultiRecords("select userID,username,name from ".TBL_ADMIN_USERS." where username in ('".$linkedAgentStr."')");
	if (count($userIDs) == 1) {
		for ($i = 0; $i < count($userIDs); $i++) {
			$_SESSION["senderAgentID"] = $userIDs[$i]["userID"];
		}	
	}
}
if(empty($_SESSION["useTransDetails"]) && $useTransDetailsFlag)
	$_SESSION["useTransDetails"] = "No";
if($_GET["transID"] != "" && trim($_POST["transType"])== "" && $_GET['collectionPointID'] == "" && $_GET['msg'] != 'Y')
{
	//	exit();
	//debug( "Fetching transaction from database" );
	$contentTrans = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$_GET["transID"]."'");
	$_SESSION["transType"] 			= $contentTrans["transType"];
	$_SESSION["transactionCreatedBy"] = $contentTrans["createdBy"];
	$_SESSION["transStatus"] = $contentTrans["transStatus"];
	$_SESSION["benAgentID"] 		= $contentTrans["benAgentID"];
	$_SESSION["distribut"] = $_SESSION["benAgentID"];
	$dist = $_SESSION["distribut"];
	$_SESSION["senderAgentID"] 		= $contentTrans["custAgentID"];
	$_SESSION["customerID"] 		= $contentTrans["customerID"];
	$_SESSION["benID"] 				= $contentTrans["benID"];
	$_SESSION["moneyPaid"] 		= $contentTrans["moneyPaid"];
	$_SESSION["serviceType"] 		= $contentTrans["serviceType"];

	if(CONFIG_SENDER_ID_EXPIRED_WAVIER==1)
	{
		$_SESSION["docWaiver"] 	= $contentTrans["is_waved_doc"];
	}
	if($tempChange!= "Y")
	{
		if(!empty($_REQUEST["senderBank"]))
			$_SESSION["senderBank"] = $_REQUEST["senderBank"];
		else
			$_SESSION["senderBank"] = $contentTrans["senderBank"];
	}

	if(CONFIG_CALCULATE_BY == '1' && ($_SESSION["moneyPaid"] == "By Bank Transfer" || $_SESSION["moneyPaid"] == "Inclusive of Fee" )) // Have kept the condition $_SESSION["moneyPaid"] != "By Cash" beacuse moneypaid was storing by cash/by bank transfer in case of inclusive/exclusive,since there is a chance that it is still being done for some client thats why have  kept that conditin along with the new one.
	{ 
		$fromTotalConfig = '1';
		$_SESSION["calculateBy"] = "inclusive";
	} 
	elseif(CONFIG_CALCULATE_BY == '1' && ($_SESSION["moneyPaid"] == "By Cash" || $_SESSION["moneyPaid"] == "Exclusive of Fee"))
	{
		$fromTotalConfig = '0';
		$_SESSION["calculateBy"] = 'exclusive';
	}
	
	if($_GET["getCalByValue"]== "Y")
	{
		$_SESSION["calculateBy"] = $_GET["calculateBy"];
		
		if($_SESSION["calculateBy"] == "inclusive")
		{
			$fromTotalConfig = '1';
		}
		elseif($_SESSION["calculateBy"] == "exclusive")
		{
			$fromTotalConfig = '0';
		}
	}

	$_SESSION["transactionPurpose"] = $contentTrans["transactionPurpose"];
	$_SESSION["other_pur"] 			= $contentTrans["other_pur"];
	$_SESSION["fundSources"] 		= $contentTrans["fundSources"];
	$_SESSION["refNumber"] 			= $contentTrans["refNumber"];
	$_SESSION["transAmount"]		= $contentTrans["transAmount"];
	$_SESSION["totalAmount"]		= $contentTrans["totalAmount"];
	$_SESSION["localAmount"]		= $contentTrans["localAmount"];
	$_SESSION["question"] 			= $contentTrans["question"];
 	$_SESSION["answer"]     		= $contentTrans["answer"];	
 	$_SESSION["tip"]     			= $contentTrans["tip"];
 	$_SESSION["branchName"]     	= $contentTrans["branchName"];
 	$_SESSION["transDate"] 			= $contentTrans["creation_date_used"];
	//$_SESSION["admincharges"] 	= $contentTrans["admincharges"];
	$_SESSION["bankCharges"] 		= $contentTrans["bankCharges"];	
	$_SESSION["currencyTo"] 		= $contentTrans["currencyTo"];	
	$_SESSION["currencyFrom"] 		= $contentTrans["currencyFrom"];	
	$_SESSION["internalRemarks"] 	= $contentTrans["internalRemarks"];	
	$_SESSION["payPoint"] 			= $contentTrans["payPointType"];	
	$_SESSION["exRateLimitMin"] 	= $contentTrans["exRateLimitMin"];	
	$_SESSION["exRateLimitMax"] 	= $contentTrans["exRateLimitMax"];	
	$_SESSION["clientRef"] 			= $contentTrans["clientRef"];	
	$_SESSION["chequeNo"] 			= $contentTrans["chequeNo"];	
	$_SESSION["reverseCalculation"]	= $contentTrans["chequeNo"];
	if($valueDateFlag){
		if(!empty($contentTrans["valueDate"]) && $contentTrans["valueDate"]!="0000-00-00 00:00:00"){
			$v_date = date("d/m/Y",strtotime($contentTrans["valueDate"]));
			$_SESSION["valueDate"] = $v_date;
		}
	}
	if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
	{
		$_SESSION["chequeAmount"] 	= $contentTrans["chequeAmount"];
	}

	$exRate 						= $contentTrans["exchangeRate"];
	$exID 							= $contentTrans["exchangeID"];
	$_SESSION["IMFee"] 				= $IMFee = $contentTrans["IMFee"];
	
	
	if((CONFIG_AGENT_OWN_MANUAL_RATE == '1' && strstr(CONFIG_MANUAL_RATE_USERS, $agentType.",")) || $_GET["enterOwnRate"] == "Y")
	{
		$_SESSION["checkManualRate"] = $contentTrans["agentExchangeRate"];
		
		if($_SESSION["checkManualRate"] == 'Y')
		{
			$_SESSION["exchangeRate"] = $contentTrans["exchangeRate"];
		}
	}elseif($_POST["transID"] != "" || $_GET["transID"] != ""){
		$_SESSION["exchangeRate"] = $contentTrans["exchangeRate"];
	}else{
		if($_GET["exchangeRate"] != "")
		{
			$_SESSION["exchangeRate"] = $_GET["exchangeRate"];
		}else{
			$_SESSION["exchangeRate"] = $_POST["exchangeRate"];
		}
	}
	
	$totalAmount = $contentTrans["totalAmount"];
	$transAmount = $contentTrans["transAmount"];
	$localAmount = $contentTrans["localAmount"];
	
	if(CONFIG_BACK_DATED == '1' && $contentTrans["creation_date_used"]!="0000-00-00")
	{
		$dbDate = explode("-",$_SESSION["transDate"]);
	
		if(count($dbDate) == 3)
		{
			$dbDate = $dbDate[2]."/".$dbDate[1]."/".$dbDate[0];
			$_SESSION["transDate"] = $dbDate;
		}
	} else {
		$_SESSION["transDate"] = "";
	}
	
	if(CONFIG_CASH_PAID_CHARGES_ENABLED == "1")
	{
		if($_SESSION["moneyPaid"]=='By Cash')
		{
			$_SESSION["moneyPaidCharges"]=CONFIG_CASH_PAID_CHARGES;
		}
	}

	//$_SESSION["act"] = $_GET["act"];	
	// Session vars for trans_type Bank Transfer
	$act = $_SESSION["act"];
	
	if($contentTrans["transType"] == "Bank Transfer")
	{
       $contentBank = selectFrom("select * from " . TBL_BANK_DETAILS . " where transID='".$contentTrans["transID"]."'"); 
        $_SESSION["bankName"]       = $contentBank["bankName"]; 
        $_SESSION["branchCode"]     = $contentBank["branchCode"]; 
		$_SESSION["branchAddress"]  = $contentBank["branchAddress"]; 
        $_SESSION["swiftCode"]      = $contentBank["swiftCode"]; 
        $_SESSION["accNo"]          = $contentBank["accNo"]; 
        $_SESSION["ABACPF"]         = $contentBank["ABACPF"]; 
        $_SESSION["IBAN"]           = $contentBank["IBAN"]; 
        $_SESSION["ibanRemarks"]    = $contentBank["Remarks"]; 
        $_SESSION["accountType"]    = $contentBank["accountType"]; 
		if(!empty($contentTrans["transDetails"]) && $useTransDetailsFlag)
			$_SESSION["useTransDetails"] = $contentTrans["transDetails"];

        if(!empty($contentBank["bankName"])) 
          $_SESSION["bankTransferType"] = "B"; 
        else 
         $_SESSION["bankTransferType"] = "I"; 

		if(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER == "1") {
			$benIbanBankQuery = selectFrom("select * from " . TBL_BANK_DETAILS . " where transID='".$contentTrans["transID"]."'");
			if(!empty($_GET['benID'])){
				$benIbanBankQuery = selectFrom("select * from " . TBL_BANK_DETAILS . " where benID='".$_GET['benID']."'");
			}
			if($benIbanBankQuery!="" && $benIbanBankQuery['IBAN']!=""){
				$_SESSION['IBAN'] = $benIbanBankQuery['IBAN'];
			}
			else{
				$_SESSION["bankName"] 		= $benIbanBankQuery["bankName"];
				$_SESSION["branchCode"] 	= $benIbanBankQuery["branchCode"];
				$_SESSION["branchAddress"] 	= $benIbanBankQuery["branchAddress"];
				$_SESSION["swiftCode"] 		= $benIbanBankQuery["swiftCode"];
				$_SESSION["accNo"] 			= $benIbanBankQuery["accNo"];
				$_SESSION["ABACPF"] 		= $benIbanBankQuery["ABACPF"];
				$_SESSION["IBAN"] 			= "";
				$_SESSION["ibanRemarks"] 	= $benIbanBankQuery["Remarks"];
				$_SESSION["accountType"] 	= $benIbanBankQuery["accountType"];
				if(!empty($contentBank["bankName"]))
					$_SESSION["bankTransferType"] = "B";
				else
					$_SESSION["bankTransferType"] = "I";
			}
		}	
		
		if(CONFIG_ADD_BEN_BANK_FIELDS_ON_CREATE_TRANSACTION == "1"){
		  $_SESSION["sortCode"]       = $contentBank["sortCode"]; 
		  $_SESSION["routingNumber"]       = $contentBank["routingNumber"]; 
		}
	}

	$_SESSION["bankingType"] = $contentTrans["bankingType"];	
	
	if($_GET["collectionPointID"] == "")
	{
		$_SESSION["collectionPointID"] = $contentTrans["collectionPointID"];
		//echo $contentTrans["transType"]. "select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentTrans["agentID"]."'";	
		$agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentTrans["benAgentID"]."'");
		//if ($_POST["Country"] != "")
			$_SESSION["Country"] = $agentContent["agentCountry"];
		//if ($_POST["City"] != "")
			$_SESSION["City"] = $agentContent["agentCity"];
	}
	 if(CONFIG_SENDER_DOCUMENT_AT_CREATE_TRANS == "1"){
	 	$_SESSION["docCategory"] = unserialize($contentTrans["custDocumentProvided"]);
     }
}
else
{
	//	$_SESSION["transType"] = (is_array($_POST) && $_GET["msg"] == "" ? $_POST["transType"] : $_SESSION["transType"]);
	if($_POST["transType"] != "")
	{
		if($_SESSION["transType"] !=  $_POST["transType"])
		{
			$_SESSION["transType"] = (is_array($_POST) && $_GET["msg"] == "" ? $_POST["transType"] : $_SESSION["transType"]);
		}
	}
	if($useTransDetailsFlag)
		$_SESSION["useTransDetails"] = ($_POST["useTransDetails"] != "" ? $_POST["useTransDetails"] : $_SESSION["useTransDetails"]);
	if($valueDateFlag)
		$_SESSION["valueDate"] = ($_POST["value_date"] != "" ? $_POST["value_date"] : $_SESSION["valueDate"]);

	$_SESSION["benAgentID"] = ($_POST["distribut"] != "" ? $_POST["distribut"] : $_SESSION["benAgentID"]);
	$_SESSION["distribut"] = $_SESSION["benAgentID"];
	
	$dist = $_SESSION["distribut"];
	$_SESSION["customerID"] = ($_POST["customerID"] != "" || $_GET["customerID"] ? ($_GET["customerID"] != "" ? $_GET["customerID"] : $_POST["customerID"]) : $_SESSION["customerID"] );
	//$_SESSION["benID"] = ($_POST["benID"] != "" ? $_POST["benID"] : $_SESSION["benID"]);
	$_SESSION["benID"] = ($_POST["benID"] != "" || $_GET["benID"] ? ($_GET["benID"] != "" ? $_GET["benID"] : $_POST["benID"]) : $_SESSION["benID"] );
	//$_SESSION["admincharges"] = ($_POST["admincharges"] != "" ? $_POST["admincharges"] : $_SESSION["admincharges"]);
	// Session vars for trans_type Bank Transfer
	$_SESSION["calculateBy"] = ($_POST["calculateBy"] != "" || $_GET["calculateBy"] ? ($_GET["calculateBy"] != "" ? $_GET["calculateBy"] : $_POST["calculateBy"]) : $_SESSION["calculateBy"] );
	$_SESSION["bankName"] 		= ($_POST["bankName"] != "" ? $_POST["bankName"] : $_SESSION["bankName"]);
	$_SESSION["branchCode"] 	= ($_POST["branchCode"] != "" ? $_POST["branchCode"] : $_SESSION["branchCode"]);
	$_SESSION["branchName"] 	= ($_POST["branchName"] != "" ? $_POST["branchName"] : $_SESSION["branchName"]);
	$_SESSION["branchAddress"] 	= ($_POST["branchAddress"] != "" ? $_POST["branchAddress"] : $_SESSION["branchAddress"]);
	$_SESSION["swiftCode"] 		= ($_POST["swiftCode"] != "" ? $_POST["swiftCode"] : $_SESSION["swiftCode"]);
	$_SESSION["accNo"] 			= ($_POST["accNo"] != "" ? $_POST["accNo"] : $_SESSION["accNo"]);
	$_SESSION["ABACPF"] 		= ($_POST["ABACPF"] != "" ? $_POST["ABACPF"] : $_SESSION["ABACPF"]);
	$_SESSION["IBAN"] 			= ($_POST["IBAN"] != "" ? $_POST["IBAN"] : $_SESSION["IBAN"]);
	$_SESSION["ibanRemarks"] 			= ($_POST["ibanRemarks"] != "" ? $_POST["ibanRemarks"] : $_SESSION["ibanRemarks"]);
	$_SESSION["bankCharges"] = ($_POST["bankCharges"] != "" ? $_POST["bankCharges"] : $_SESSION["bankCharges"]);
	$_SESSION["distribut"] = ($_POST["distribut"] != "" ? $_POST["distribut"] : $_SESSION["distribut"]);
	
	$_SESSION["currencyTo"] = ($_POST["currencyTo"] != "" ? $_POST["currencyTo"] : $_SESSION["currencyTo"]);
	
	$_SESSION["currencyFrom"] = ($_POST["currencyFrom"] != "" ? $_POST["currencyFrom"] : $_SESSION["currencyFrom"]);
	$_SESSION["accountType"] = ($_POST["accountType"] != "" ? $_POST["accountType"] : $_SESSION["accountType"]);
	//$_SESSION["document_remarks"] = ($_POST["document_remarks"] != "" ? $_POST["document_remarks"] : $_SESSION["document_remarks"]);
	$_SESSION["internalRemarks"] = ($_POST["internalRemarks"] != "" ? $_POST["internalRemarks"] : $_SESSION["internalRemarks"]);
	$_SESSION["benIdPassword"] = ($_POST["benIdPassword"] != "" ? $_POST["benIdPassword"] : $_SESSION["benIdPassword"]);
	//echo("at line 305 transID  ".$_GET["transID"]."  and transType ".$_SESSION["transType"]);
	$_SESSION["refNumber"] = ($_POST["refNumber"] != "" ? $_POST["refNumber"] : $_SESSION["refNumber"]);
	$_SESSION["transAmount"] = ($_POST["transAmount"] != "" ? $_POST["transAmount"] : $_SESSION["transAmount"]);
	$_SESSION["localAmount"] = ($_POST["localAmount"] != "" ? $_POST["localAmount"] : $_SESSION["localAmount"]);
	$_SESSION["totalAmount"] = ($_POST["totalAmount"] != "" ? $_POST["totalAmount"] : $_SESSION["totalAmount"]);
	$_SESSION["checkManualRate"] = ($_POST["checkManualRate"] != "" ? $_POST["checkManualRate"] : $_SESSION["checkManualRate"]);
	$_SESSION["transactionPurpose"] = ($_POST["transactionPurpose"] != "" ? $_POST["transactionPurpose"] : $_SESSION["transactionPurpose"]);
	
	$_SESSION["exRateLimitMin"] = ($_POST["exRateLimitMin"] != "" ? $_POST["exRateLimitMin"] : $_SESSION["exRateLimitMin"]);
	$_SESSION["exRateLimitMax"] = ($_POST["exRateLimitMax"] != "" ? $_POST["exRateLimitMax"] : $_SESSION["exRateLimitMax"]);
	$_SESSION["chequeNo"] = ($_POST["chequeNo"] != "" ? $_POST["chequeNo"] : $_SESSION["chequeNo"]);
	$_SESSION["transNote"] = ($_POST["transNote"] != "" ? $_POST["transNote"] : $_SESSION["transNote"]);
	$_SESSION["reverseCalculation"] = ($_SERVER['REQUEST_METHOD']=="POST" ? $_POST["reverseCalculation"] : $_SESSION["reverseCalculation"]);
   if(CONFIG_SENDER_DOCUMENT_AT_CREATE_TRANS == "1"){
		$_SESSION["docCategory"] =($_REQUEST["docCategory"] != "" ? $_REQUEST["docCategory"] : $_SESSION["docCategory"]);
 	}
	if(CONFIG_ADD_BEN_BANK_FIELDS_ON_CREATE_TRANSACTION == "1"){
	$_SESSION["sortCode"] = ($_POST["sortCode"] != "" ? $_POST["sortCode"] : $_SESSION["sortCode"]);
	$_SESSION["routingNumber"] = ($_POST["routingNumber"] != "" ? $_POST["routingNumber"] : $_SESSION["routingNumber"]);
	}	
	if(CONFIG_SENDER_ID_EXPIRED_WAVIER==1)
	{
		$_SESSION["docWaiver"] = ($_REQUEST["docWaiver"] != "" ? $_REQUEST["docWaiver"] : $_SESSION["docWaiver"]);

		
	}
	/* #4804 
	 	This is made not to run when this config is ON. As it get submitted value of transType parameter on selecting other beneficiary.
		It should only take value of serviceType as transType from Beneficiary table.
		by Aslam Shahid
	*/
	if(!$countryBasedFlag){
		$_SESSION["transType"] = ($_REQUEST["transType"] != "" ? $_REQUEST["transType"] : $_SESSION["transType"]);
	}

	if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
	{
		$_SESSION["chequeAmount"] = ($_POST["chequeAmount"] != "" ? $_POST["chequeAmount"] : $_SESSION["chequeAmount"]);
	}
	
	/**
	 * Origional Code Line
	 * $_SESSION["exchangeRate"] = ($_POST["exchangeRate"] != "" ? $_POST["exchangeRate"] : $_GET["exchangeRate"]);
	 *
	 * Improving that if $_GET["exchangeRate"] is empty than associated session index also got empty
	 * So it is over rided by the value session value in following priority
	 * If not empty $_POST["exchangeRate"] than session will store this
	 * else if not empty $_GET["exchangeRate"] than session will store this
	 * other wise session will retain its origional value
	 * @Ticket# 3524 
	 */
	if(!empty($_POST["exchangeRate"]))
		$_SESSION["exchangeRate"] = $_POST["exchangeRate"];
	elseif(!empty($_GET["exchangeRate"]))
		$_SESSION["exchangeRate"] = $_GET["exchangeRate"];
	/**
	 * Enf of code #3524
	 */	
	
	
	$_SESSION["clientRef"] = ($_POST["clientRef"] != "" ? $_POST["clientRef"] : $_SESSION["clientRef"]);
	$_SESSION["bankingType"] = isset($_POST["bankingType"]) ? $_POST["bankingType"] : $_SESSION["bankingType"];	

	$_SESSION["payPoint"] = ($_POST["payPoint"] != "" ? $_POST["payPoint"] : '');
	
	if(!empty($_REQUEST["cardNo"]))
		$_SESSION["cardNo"] = $_REQUEST["cardNo"];
	if(!empty($_REQUEST["cardName"]))
		$_SESSION["cardName"] = $_REQUEST["cardName"];
	if(!empty($_REQUEST["cardExpiry"]))
		$_SESSION["cardExpiry"] = $_REQUEST["cardExpiry"];
	
	if(CONFIG_BACK_DATED == '1')
	{
		if ($_POST["manageTransDate"] != "") {
			$_SESSION["transDate"] = $_POST["dDate"];
		} else {
	 		$_SESSION["transDate"] = ($_POST["dDate"] != "" ? $_POST["dDate"] : $_SESSION["transDate"]);
	 	}
	}else{
		$_SESSION["transDate"] = "";
		}
	if ($_POST["Country"] != "")
		$_SESSION["Country"] = $_POST["Country"];
	if ($_POST["City"] != "")
		$_SESSION["City"] = $_POST["City"];
	if($_POST["discount"] != "")
		$_SESSION["discount"] = $_POST["discount"];
	if($_POST["chDiscount"] != "")
		$_SESSION["chDiscount"] = $_POST["chDiscount"];
	if ($_POST["t_amount"] != "")
		$_SESSION["amount_transactions"] = $_POST["t_amount"];
	if ($_POST["l_amount"] != "")
		$_SESSION["amount_left"] = $_POST["l_amount"];
	if ($_GET["batch"] != "")
		$_SESSION["batch"] = $_GET["batch"];
	
	$_SESSION["manualCommission"] = $_REQUEST["manualCommission"];

	if(CONFIG_CASH_PAID_CHARGES_ENABLED == "1")
	{
		if($_SESSION["moneyPaid"]=='By Cash')
		{
			$_SESSION["moneyPaidCharges"]=CONFIG_CASH_PAID_CHARGES;
			}
	}
	if(isset($_REQUEST["senderBank"]) && (!empty($_REQUEST["senderBank"]) || !empty($_GET["senderBank"])) && empty($_GET["moneyPaid"]))
	{
		/*	#5149
			Here Get was preffered as when control comes from search sender bank then this Transactin from is
			submitted and Get value should be used from search sender bank.
			by Aslam Shahid
		*/

		$_SESSION["senderBank"] = ($_GET["senderBank"]!="" ? $_GET["senderBank"] : $_POST["senderBank"]); 
	} 
	//if($_POST["discount_request"] != "")
	if($_GET["from"] != 'conf')
	{
		 	$_SESSION["discount_request"] = $_POST["discount_request"];	
		
	
	
			$_SESSION["question"] = $_POST["question"];
			$_SESSION["answer"] = $_POST["answer"];
			
		if($_POST["tip"] != ""){
			
					$_SESSION["tip"] = $_POST["tip"];
					
			}elseif($_GET["tip"] != ""){
					
					$_SESSION["tip"] = $_GET["tip"];
					
				}	
			//$_SESSION["branchName"] = $_POST["branchName"];	
			
		//-----if($_POST["moneyPaid"] != "")
		//-----{
		// Code modified by Usman Ghani aginst ticket #3319: Minas Center - Payment Mode Commission Rates
		// It is modified in order to retain the previous status.
		/*** modified the condition $_SESSION["moneyPaid"] = !empty( $_POST["moneyPaid"] ) ? $_POST["moneyPaid"] : $_SESSION["moneyPaid"]; 
			to the one below because in case of update transaction money paid select one option was not being selected
			Code modified by Khola @ticket #3534
		***/
		//$_SESSION["moneyPaid"] = isset( $_POST["moneyPaid"] ) ? $_POST["moneyPaid"] : $_SESSION["moneyPaid"];
		if(isset($_REQUEST["moneyPaid"])){
			$_SESSION["moneyPaid"] = $_REQUEST["moneyPaid"];
		}
		
		// End of code modification aginst ticket #3319: Minas Center - Payment Mode Commission Rates.
		//-----}
			$_SESSION["other_pur"] = $_POST["other_pur"];
			$_SESSION["fundSources"] = $_POST["fundSources"];
		
	}
	
	/**
	 * Remove Bug
	 * @Ticket# 3509 
	 */
	$transAmount = $_SESSION["transAmount"];
	/**
	 * @Ticket #4506
	 */	
	if(!empty($_REQUEST["bankTransferType"]))		
		$_SESSION["bankTransferType"] = $_REQUEST["bankTransferType"];
	else
		$_SESSION["bankTransferType"] = "I";

	
}

if($_SESSION["moneyPaid"] != "By Bank Transfer"){
	$_SESSION["senderBank"] = "";
}	
if($_GET["from"] != 'conf'  && ($_GET["transID"] == "" || $_POST["Amount"] != '' || $_POST["localAmount"] != '' ||$_POST["fromTotal"] != ''))
{
	
		//$_SESSION["transAmount"] = $_POST["transAmount"];
		$transAmount = (float) $_SESSION["transAmount"];
		//$_SESSION["localAmount"] = $_POST["localAmount"];
		$localAmount = (float) $_SESSION["localAmount"];
		
		if(CONFIG_ROUND_NUMBER_ENABLED == '1')
		{
			$localAmount = round($localAmount);		
		}
		
	
	if($fromTotalConfig == '1')
	{	
		
			//$_SESSION["totalAmount"] = $_POST["totalAmount"];
			$totalAmount = (float) $_SESSION["totalAmount"];
		
	}
}




$_SESSION["distribut"] = ($_POST["distribut"] != "" ? $_POST["distribut"] : $_SESSION["distribut"]);


if($_POST["refNumber"] != "")
{	
	$refNumber = str_replace(" ","",$_POST["refNumber"]);
	$_SESSION["refNumber"] = $refNumber;
}
/*if ($_SESSION["transType"] != ""){
	$queryCust ="select customerID, Title, firstName, lastName, middleName from ".TBL_CUSTOMER . " order by firstName";
	$customers = selectMultiRecords($queryCust);
	//echo $queryCust . "<br>";
	if($_SESSION["customerID"] != "")
	{
		$queryBen  ="select * from ".TBL_BENEFICIARY." where customerID ='$_SESSION[customerID]' order by firstName";
		$beneficiaries = selectMultiRecords($queryBen);
		//echo $queryBen . "<br>";
	}
}*/
if($_POST["transID"] != "" && $_POST["transType"]=="Pick up" && $_POST["Country"] == "")
{
	$agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentTrans["benAgentID"]."'");
	$_SESSION["Country"] = $agentContent["agentCountry"];
	$_SESSION["City"] = $agentContent["agentCity"];
}



if($_GET["aID"] != "")
{ 
	$_SESSION["senderAgentID"] = $_GET["aID"];
	
}elseif($_GET["aID"] == "")
	{ 
		if(CONFIG_ENABLE_DEFAULT_AGENT == "1")
		{
		if($_SESSION["senderAgentID"]=="")
				$_SESSION["senderAgentID"] ='3';
			
		}
		
			if($_POST["aID"] != ""){
				
						$_SESSION["senderAgentID"] = $_POST["aID"];
	}else{
		
		
		/**
		 * @Ticket# 3622 
		 * If super admin is logged in and the config for subagent is ON than 
		 * senderAgentId will be the selected one from the search sender, provided it is not null
		 * else system will select the looged userd id as sender id
   	     */ 
		if(CONFIG_CREATE_TRANSACTION_FOR_SUB_AGENT == 1 && $agentType == "SUPA")
		{
			if(!empty($_REQUEST["aID"]))
				$_SESSION["senderAgentID"] = $_REQUEST["aID"];
			
			if($_REQUEST["create"] == "Y")
				$_SESSION["senderAgentID"] = $_SESSION["loggedUserData"]["userID"];
		}
		else
		{
			if($agentType == "SUPA" || $agentType == "SUPAI" || $agentType == "SUBA" || $agentType == "SUBAI")
			{
				$_SESSION["senderAgentID"] = $_SESSION["loggedUserData"]["userID"];
			}
		}
		/* End #3622 */
		
		
		if($agentType == "TELLER")
		{
			$_SESSION["senderAgentID"] = $userDetails["cp_ida_id"];
	
		}
	}
	}
if(!$hideAgentFlag){
	//debug($_REQUEST["agentID"]);
	if(!empty($_REQUEST["agentID"])){
		$_SESSION["senderAgentID"] = $_REQUEST["agentID"]; // this comes after selecting sender from search sender
	}
/*	else{
		$agentHiddenRS = selectFrom("SELECT agentID FROM ".TBL_CUSTOMER." WHERE customerID='".$_SESSION["customerID"]."'");
		$_SESSION["senderAgentID"] = $agentHiddenRS["agentID"];
	}*/
}
//debug($_SESSION["senderAgentID"]);
if($_GET["benAgentID"] != "")
{
	$_SESSION["benAgentID"] = $_GET["benAgentID"];
	$_SESSION["transType"] = "Pick up";
}

if($_GET["customerID"]!="")
	$_SESSION["customerID"] = $_GET["customerID"];


/******************
* This variable is used to show the customer and beneficiary 
* last transaction details (if occured) when beneficiary is selected
******************/
if (CONFIG_CUSTBEN_OLD_TRANS == '1' || CONFIG_INTERNAL_REMARKS == '1') 
{
		if($_GET["benID"] != "")
		{
			/**
			 * When the transaction type changed at the creat transaction to the type which was diffrent than the last transaction type
			 * than try to pick the transaction details related to the new transaction type  which was different to the last transaction's tran type.
			 * Above condition executed when the transType changed else not
			 * @Ticket #4419
			 */
			$strTransTypeSql = !empty($_REQUEST["transType"]) ? " and transType = '".$_REQUEST["transType"]."' " : "" ;
		
			$oldContent = selectFrom("select benAgentID,transID, collectionPointID, transType, internalRemarks from " . TBL_TRANSACTIONS." where transID = (select  Max(transID) from " . TBL_TRANSACTIONS." where customerID = '".$_SESSION["customerID"]."' and benID = '".$_GET["benID"]."' and  toCountry = '".$benificiaryContent["Country"]."' ".$strTransTypeSql." )");
		}
	
}
/*if(CONFIG_INTERNAL_REMARKS == '1')
	{
		if($_SESSION["internalRemarks"] == "")
		{ 
			$_SESSION["internalRemarks"] = $oldContent["internalRemarks"];
		} echo "line 598".$_SESSION["internalRemarks"];
	}*/

	
	
	
if (CONFIG_CUSTBEN_OLD_TRANS == '1')
{
		if ($_GET["benID"] != "")
		{
				/**
				 * @Ticket# 3593
				 * If a beneficiary is selected than clean the session distributor variable
				 */
				unset($_SESSION["distribut"]);
				
				
				if ($oldContent["transType"] != "" && $_POST["transType"]== "")
				{
					$_SESSION["transType"] = $oldContent["transType"];
			  	
			  	/**
			  	 * @Ticket# 3321 
			  	 * Disable the history if the last transfer type is bank transfer
			  	 */
			  	if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1" && $oldContent["transType"] == "Bank Transfer")
			  	{
			  			//$dist = $_SESSION["distribut"];

					  	$_SESSION["distribut"] = "";
						
  			  		$_SESSION["benAgentID"] = "";
			  	}
			  	else
			  	{
				  		$dist = $_SESSION["distribut"];
					  	$_SESSION["distribut"] = $oldContent["benAgentID"];
						
  			  		$_SESSION["benAgentID"] = $_SESSION["distribut"];
			  	}
				
				}
				elseif ($_POST["transType"]!= "")
				{
						$_SESSION["transType"] = $_POST["transType"];
			  	
				}
				if ($_SESSION["transType"] == "Bank Transfer") 
				{
						if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1")
						{
							$_SESSION["distribut"] = "";
							
						}

						if ($oldContent["transType"] != "") 
						{
								$contentBank5 = selectFrom("select * from " . TBL_BANK_DETAILS . " where transID = '".$oldContent["transID"]."'");
						} 
						else 
						{		 
								$contentBank5 = selectFrom("select * from " . TBL_BANK_DETAILS . " where benID='".$_GET["benID"]."'");
						}
						$_SESSION["bankName"] 		 = $contentBank5["bankName"];
						$_SESSION["branchCode"] 	 = $contentBank5["branchCode"];
						$_SESSION["branchAddress"] = $contentBank5["branchAddress"];
						$_SESSION["swiftCode"] 		 = $contentBank5["swiftCode"];
						$_SESSION["accNo"] 			   = $contentBank5["accNo"];
						$_SESSION["ABACPF"] 		   = $contentBank5["ABACPF"];
						$_SESSION["IBAN"] 			   = $contentBank5["IBAN"];
						$_SESSION["ibanRemarks"] 	 = $contentBank5["Remarks"];
						$_SESSION["accountType"]   = $contentBank5["accountType"];
						
						if(CONFIG_ADD_BEN_BANK_FIELDS_ON_CREATE_TRANSACTION == "1")
						{
						  $_SESSION["sortCode"]     = $contentBank5["sortCode"]; 
						  $_SESSION["routingNumber"] = $contentBank5["routingNumber"]; 
						}
				} 
				elseif ($_SESSION["transType"] == "Pick up" && $oldContent["transType"] == $_SESSION["transType"]) 
				{
						if ($oldContent["collectionPointID"] != "" && $_GET['collectionPointID'] == "" && $_POST['collPoints'] == "") 
						{
								$_SESSION["transType"] = $oldContent["transType"];
								$_SESSION["collectionPointID"] = $oldContent["collectionPointID"];
								$_SESSION["distribut"] = $oldContent["benAgentID"];
								
								$dist = $_SESSION["distribut"];
					 			$_SESSION["benAgentID"] = $_SESSION["distribut"];	
						}
				}
		}
}

/**
 * Marking the CONFIG_DEFAULT_PAYMENT_MODE as OFF due to the conflicting requirment
 * with #3517
 */
if (CONFIG_DEFAULT_PAYMENT_MODE == '1' || $_SESSION["moneyPaid"]== "") {
if (CONFIG_DEFAULT_PAYMENT_MODE == '1' && $_SESSION["moneyPaid"]== "") {	
	$defMoneyPaid = selectFrom("SELECT `defaultMoneyPaid` FROM " . TBL_ADMIN_USERS . " WHERE `userID` = '" . $parentID . "'");
	
		$_SESSION["moneyPaid"] = $defMoneyPaid["defaultMoneyPaid"];
}
	if($_SESSION["moneyPaid"] == "" && CONFIG_MONEY_PAID_SELECT_ONE != 1) { // #3519: In case when transaction is updated this condition was assigninging by cash to $_SESSION["moneyPaid"] instead of the db value incase the db value was empty so added CONFIG_MONEY_PAID_SELECT_ONE != 1 condition
	//	if(CONFIG_MONEY_PAID_NON_MANDATORY == "1"){
			
		//	$_SESSION["moneyPaid"] = '';	
			
		//	}else{
		$_SESSION["moneyPaid"] = 'By Cash';
	//}
	}
}

/**
 * The default selection of Money Paid field set to -Select One-
 * @Ticket# 3517
 */
 
if(CONFIG_MONEY_PAID_SELECT_ONE == 1 && empty($_GET["transID"]) && empty($_SESSION["moneyPaid"])) 
{
	$_SESSION["moneyPaid"] = "";	
}
/**
 * End of #3517
 */

if($_SESSION["moneyPaid"] == 'By Cash')	
	$_SESSION["moneyPaidCharges"]=CONFIG_CASH_PAID_CHARGES;
	
if (CONFIG_DEFAULT_TRANSACTION_PURPOSE == "1") {
	// Do nothing
	if($_SESSION["transactionPurpose"] == "") {
		$_SESSION["transactionPurpose"] = 'Family Assistance';
	}
}
elseif(CONFIG_OTHER_TRANSACTION_PURPOSE == "1")
{
	if($_SESSION["transactionPurpose"] == "") {
		$_SESSION["transactionPurpose"] = 'Other';
	}
}
if(CONFIG_NONCOMPUL_FUNDSOURCES != '1' && $_SESSION["fundSources"]==""){
	$_SESSION["fundSources"]='Salary';
}

if(CONFIG_DEFAULT_VALUE_FOR_CALCULATEBY == "1" && $_SESSION["calculateBy"]== ''){
	$_SESSION["calculateBy"] = CONFIG_DEFAULT_VALUE_CHOICE;
}

if(CONFIG_CALCULATE_BY == '1' && ($_POST["calculateBy"] == 'inclusive' || $_GET["calculateBy"] == 'inclusive' || $_SESSION["calculateBy"] == 'inclusive'))
{ 	

				$fromTotalConfig = '1';
				
			$_SESSION["moneyPaid"] = "By Bank Transfer";
				
				$_SESSION["calculateBy"]= 'inclusive';
	
}
elseif(CONFIG_CALCULATE_BY == '1' && ($_POST["calculateBy"]== 'exclusive'|| $_GET["calculateBy"]== 'exclusive' || $_SESSION["calculateBy"] == 'exclusive')){
				$fromTotalConfig = '0';
				$_SESSION["moneyPaid"] = "By Cash";
				$_SESSION["calculateBy"]= 'exclusive';
		
}elseif(CONFIG_CALCULATE_BY == '1' && $_POST["calculateBy"]== '' && $_SESSION["calculateBy"]== ''){
	
	
	if($_SESSION["moneyPaid"] == "By Bank Transfer"){
		
		
				$fromTotalConfig = '1';				
				$_SESSION["moneyPaid"] = "By Bank Transfer";	
				$_SESSION["calculateBy"]= 'inclusive';
	
		
	}elseif($_SESSION["moneyPaid"] == "By Cash"){
		
				$fromTotalConfig = '0';
				$_SESSION["moneyPaid"] = "By Cash";
				$_SESSION["calculateBy"]= 'exclusive';
		
		
		}
	
	
	}

// This Code Added by Niaz Ahmad at 20-02-2008 for Compliance //////
if(CONFIG_COMPLIANCE_PROMPT == "1")
{
	if(CONFIG_COMPLAINCE_PROMPT_IF_NO_CUSTOMER_ID_DETAILS == 1 && !empty($_SESSION["customerID"]))
	{
		$strSqlCusotmer = "select c.Address, c.Address1, c.City, c.Zip, c.placeOfBirth, c.dob, i.id_number, i.expiry_date from ".TBL_CUSTOMER." as c,  user_id_types as i where customerID=".$_SESSION["customerID"]." and user_id=customerID";
		$arrDataCustomer = selectFrom($strSqlCusotmer);
		//debug($arrDataCustomer);
		
		/**
		 * If customer provide the address, city, zip, place of birth, data of birth, id number and id expiration date
		 * than do not put any compliance alert
		 * @Ticket #3795
		 */
		if(empty($arrDataCustomer["Address"]) 
			|| empty($arrDataCustomer["City"])
			|| empty($arrDataCustomer["placeOfBirth"])
			|| empty($arrDataCustomer["dob"])
			|| empty($arrDataCustomer["id_number"])
			|| empty($arrDataCustomer["expiry_date"])
			|| empty($arrDataCustomer["Zip"]) )
		{
			$currentRuleQuery = " select ruleID,applyAt,matchCriteria,message,applyTrans,amount from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Create Transaction' 
			and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Create Transaction')";
			$currentRule = selectFrom($currentRuleQuery);
			
			$complianceQuery = " select ruleID,applyAt,matchCriteria,message,applyTrans,amount,cumulativeFromDate,cumulativeToDate from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Create Transaction' 
			and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Create Transaction')";
			$cumulativeRule = selectFrom($complianceQuery);
		}
	}
	else
	{
		/* Default Case */
		$currentRuleQuery = " select ruleID,applyAt,matchCriteria,message,applyTrans,amount from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Create Transaction' 
			and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Create Transaction')";
		$currentRule = selectFrom($currentRuleQuery);
		
		$complianceQuery = " select ruleID,applyAt,matchCriteria,message,applyTrans,amount,cumulativeFromDate,cumulativeToDate from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Create Transaction' 
		and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Create Transaction')";
		$cumulativeRule = selectFrom($complianceQuery);
	}
}

if(CONFIG_SENDER_ACCUMULATIVE_AMOUNT == "1"){

$senderAccumulativeAmount = 0;
//$senderTransAmount = selectMultiRecords("select accumulativeAmount from ".TBL_CUSTOMER." where customerID = '".$_POST["customerID"]."'");
         
            		  $to = getCountryTime(CONFIG_COUNTRY_CODE);
									$month = substr($to,5,2);
									$year = substr($to,0,4);
									$day = substr($to,8,2);
								  $noOfDays = CONFIG_NO_OF_DAYS;
								  $fromDate = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d")-$noOfDays ,   date("Y")));
									$from = $fromDate." 00:00:00"; 
									
									// These dates set from configuration page
									if(CONFIG_COMPLIANCE_NUM_OF_DAYS == "1"){
									
										if($cumulativeRule["cumulativeFromDate"]!='' && $cumulativeRule["cumulativeToDate"]!=''){
  			        	    $from	= $cumulativeRule["cumulativeFromDate"]." 00:00:00"; 
  		                $to	= $cumulativeRule["cumulativeToDate"]." 23:59:59"; 
  	                } 
  	              }
									$senderTransAmount = selectFrom("select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_SESSION["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'");
								  //echo "select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_SESSION["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'";
                 $senderAccumulativeAmount += $senderTransAmount["transAmount"];
 

	if($_POST["transID"] == "")
	{
 				$senderAccumulativeAmount += $_POST["transAmount"];
	}
}

//echo "<br>--Cumulative Amount--".$senderAccumulativeAmount;

if(CONFIG_SET_FOCUS_ID == "1"){
	
	$focusID = CONFIG_SET_FOCUS_ID_VALUE;
	
}else{
	
	$focusID = "exchangeRate";
	
	}

/**
 * Fetching the Old transaction data for customer 
 * @Ticket #4185
 */
$bolViewOldTransactions = false;
if(CONFIG_COMPLIANCE_SENDER_TRANS == '1' && !empty($_SESSION["customerID"]))
{
	$strGroupRightViewSql = "select * from oldTransactionGroupRight where `group`='".$agentType."' and `haveRight`='Y'"; 
	$arrGroupRightViewData = selectFrom($strGroupRightViewSql);

	if(is_array($arrGroupRightViewData))
	{
		$bolViewOldTransactions = true;
		
		$to = getCountryTime(CONFIG_COUNTRY_CODE);
		
		$fromData = strtotime("-".$arrGroupRightViewData["backDays"]." days");
		/* formating the FROM date */		
		$from = date("Y",$fromData)."-".date("m",$fromData)."-".date("d",$fromData)." 00:00:00";
		//debug($from);
		$countTrans = selectFrom("select count(transID) as transCount, sum(transAmount) as totalAmount from ".TBL_TRANSACTIONS." where customerID = '".$_SESSION["customerID"]."' and transDate between '$from' and '$to'  AND transStatus != 'Cancelled'");
		$intTotalCustomerTransactions = $countTrans["transCount"];
		$fltTotalAmountSend 		  = $countTrans["totalAmount"];
		$intBackDays = $arrGroupRightViewData["backDays"];
		//debug($fltTotalAmountSend);
		//debug($intTotalCustomerTransactions);
	}
}


$hideAgentFlag = true;
if(strstr(CONFIG_HIDE_AGENT_ON_TRANSACTION,$agentType.",") || CONFIG_HIDE_AGENT_ON_TRANSACTION=="1")
	$hideAgentFlag = false;


 /*
 * 7166 - Premier FX
 * Reverse calculation can be done after enering amount and manual exchange rate
 * on tranaction page. There is a checkbox on transaction page.
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * by Aslam Shahid
 */
$operatorMult = "*";
$operatorDiv = "/";
$checkedReverse = "";
$reverseFlag = false;
if(CONFIG_REVERSE_TRANS_AMOUNT_OPERATORS == "1"){
	$reverseFlag = true;
	if(!empty($_SESSION["reverseCalculation"])){
		$checkedReverse = 'checked="checked"';
		$operatorMult = "/";
		$operatorDiv = "*";
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Create <?=$strTransLabel;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script language="JavaScript" src="./javascript/GCappearance2.js"></script>
<script language="JavaScript" src="./javascript/GurtCalendar.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="./styles/calendar2.css">
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<script language="javascript" src="./styles/admin.js"></script>
<script language="javascript" src="./javascript/jquery.maskedinput.min.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<SCRIPT LANGUAGE="javascript">
	
	
	////////
/*	function getHTTPObject() {
var xmlhttp;

if(@_jscript_version >= 5)
try {
xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
alert("Your browser is IE-0"); // Triggers OK in IE 7 - kmk
} catch (e) {
try {
xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
alert("Your browser is IE-1");
} catch (E) {
xmlhttp = false;
}
}else
	xmlhttp = false;

if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
try {
xmlhttp = new XMLHttpRequest();
alert("Your browser is Firefox Based");
} catch (e) {
xmlhttp = false;
}
}
return xmlhttp;
}
	
	*/
	
	////////
	function getHTTPObject() { 
	var xmlhttp; 
	/*@cc_on
 
	 /*@if(@_jscript_version >= 5)
	  
			try {
			  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
			  try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  } catch (E) {
				xmlhttp = false;
			  }
			}
		@else @*/
	  	xmlhttp = false;
	  /*@end
	@*/
	if (!xmlhttp && typeof XMLHttpRequest != 'undefined') { 
		try { 
			xmlhttp = new XMLHttpRequest(); 
			} catch (e) { 
				xmlhttp = false; 
				} 
		} 
		return xmlhttp; 
	}
 
var http = getHTTPObject(); // We create the HTTP Object 
var url1 = "getAmounts.php"; 

function handleHttpResponse() {

  if (http.readyState == 4) {



    results = http.responseText.split(",");

    
    document.getElementById('transAmountID').value = results[0];
    document.getElementById('localAmountID').value = results[1];
    document.getElementById('totalAmountID').value = results[2];
 <? if($_SESSION["manualCommission"] == "Y") { ?>
	    document.getElementById('IMFee').value = document.getElementById("IMFee").value;
 <? } else{ ?>
    document.getElementById('IMFee').value = results[3];
 <? }?>
    document.getElementById('exchangeRate').value = results[4];
	var ccID = document.getElementById('currencyChargeID');
		if(ccID != null)
    	ccID.value = results[5];
    document.getElementById('exchangeID').value = results[6];
   

	}

}

function disableClicks()
{
	<?
	if(CONFIG_SEND_ONLY_BUTTON == '1')
	{
	?>
		document.getElementById('transSend').disabled=true;
		<?
	}
	if(CONFIG_SEND_ONLY_BUTTON == '1')
	{
		?>
		document.getElementById('sendToConfirmID').disabled=true;
		<?
	}else{
		?>
		document.getElementById('sendID').disabled=true;	
		<?
	}
	?>
}
function enableClicks()
{
	<?
	if(CONFIG_SEND_ONLY_BUTTON == '1')
	{
	?>
		document.getElementById('transSend').disabled=false;
		<?
	}
	if(CONFIG_SEND_ONLY_BUTTON == '1')
	{
		?>
		document.getElementById('sendToConfirmID').disabled=false;
		<?
	}else{
		?>
		document.getElementById('sendID').disabled=false;	
		<?
	}
		?>
}
function updateAmounts(param) {

 var amountFlag = param;	
 var amount = document.getElementById("transAmountID").value;
 var amountLocal = document.getElementById("localAmountID").value;
 var amountTotal = document.getElementById("totalAmountID").value;
 var benCountry =  document.getElementById("benCountryID").value;
 var custCountry =  document.getElementById("custCountryID").value;
 var customerID =  document.getElementById("customerID").value;
 var dist =  document.getElementById("distribut").value;
 var exRate =  document.getElementById("exchangeRate").value;
 var manualCommissionAJAX ;
 <? if($manualCommAjaxFlag && $_SESSION["manualCommission"]=="Y") {?>
	 manualCommissionAJAX =  document.getElementById("manualCommissionAJAX").value;
 <? } ?>
	 var fee =  document.getElementById("IMFee").value;
 var transType = document.getElementById("transType").value;
 var currencyFrom = document.getElementById("currencyFrom").value;
 var currencyTo = document.getElementById("currencyTo").value;
 var inclusive = 'No';
 var agentOwnRate = 'No';
 <?
if((CONFIG_AGENT_OWN_MANUAL_RATE == '1' && strstr(CONFIG_MANUAL_RATE_USERS, $agentType.",")) || $_GET["enterOwnRate"] == "Y")
{?>
	
	if(document.getElementById("checkManualRate").value == 'Y')
	{
				agentOwnRate = 'Y';
	}else{
				agentOwnRate = 'N';
		}
<?
}
?>
<?
if(CONFIG_CALCULATE_BY == '1')
{?>
	
	if(document.getElementById("calculateByID").value == 'inclusive')
	{
		var moneyPaid = "Bank Transfer";
		inclusive = 'Yes';
	}else{
		var moneyPaid = "By Cash";
		inclusive = 'No';
		}
<?
}else{
	?>
	 var moneyPaid = document.getElementById("moneyPaid").value;
	 inclusive = 'No';
	<?
	}
?> 

 var customerAgentID = document.getElementById("customerAgentID").value;
 var bankChargesID = document.getElementById("bankChargesID").value;
 var bankingTypeID = document.getElementById("bankingType");
 if(bankingTypeID!=null){
	 var bankingType = bankingTypeID.value;
 }
	

 
 var amountValue = '?amount='+amount+'&benCountry='+benCountry+'&custCountry='+custCountry+'&dist='+dist+'&exRate='+exRate+'&fee='+fee+'&transType='+transType+'&currencyFrom='+currencyFrom+'&currencyTo='+currencyTo+'&amountFlag='+amountFlag+'&amountLocal='+amountLocal+'&moneyPaid='+moneyPaid+'&customerAgentID='+customerAgentID+'&bankChargesID='+bankChargesID+'&inclusive='+inclusive+'&agentOwnRate='+agentOwnRate+'&manualCommissionAJAX='+manualCommissionAJAX+'&amountTotal='+amountTotal+'&bankingType='+bankingType+'&customerID='+customerID;
  http.open("GET", url1 + amountValue, true);
  


 enableClicks();
  http.onreadystatechange = handleHttpResponse;

	
  http.send(null);
}

	



function SelectOption(OptionListName, ListVal)
{ 
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkSearch()
{
	if(document.frmSearchTrans.searchTrans.value == "")
	{
		alert("Please provide Reference No/<?=$manualCode?> in textbox to search transaction.");
		frmSearchTrans.searchTrans.focus();
		return false;
	}
	return true;	
}

function leftTrim(sString)
{
while (sString.substring(0,1) == ' ')
{
sString = sString.substring(1, sString.length);
}
return sString;
}
/*
function forSubmit()
{
	<? if(CONFIG_COMPLIANCE_PROMPT == '1'){ ?>
		ajaxReturn(addTrans,'No');
	<? }else{ ?>
		document.addTrans.action='<? echo CONFIG_RECIEPT_NAME ?>?transID=<? echo $_GET["transID"]?>&focusID=sendID&transSend=noSend'; 
		document.addTrans.submit();
	<? } ?>	
}
*/
function ajaxReturn(theForm, strval)
{
	
	if(document.getElementById("IbanValidRes"))
	{
		$("#IbanValidRes").load("validateIban.php?iban_num="+document.getElementById("IBAN").value,
													null, 
													function (theForm,strval){checkForm(theForm,strval);});
		return true;
	}
	else
	{
		
		
		if(document.getElementById("tipMandatoryDiv") && $("#transType").val() == "Pick up")
		{
			if($("#tip").val() == "")
			{
				alert("Please enter the Beneficiary Verification ID.");
				$("#tip").focus();
				return false;
			}
		}
		
		
		
		if($("#modifyReason"))
		{
			if($("#modifyReason").val() == "")
			{
				alert("Please enter the modify reason for this transaction.");
				$("#modifyReason").focus();
				return false;
			}
			else
				return checkForm(theForm,strval);	
		}
		else
			return checkForm(theForm,strval);
	}
	
}

function checkForm(theForm,strval){
	
	var transSender = '';
	var condition;
	var ruleConfirmFlag=true;
	var ruleFlag=false;
	
	
	if($("#cardNo"))
	{
		if($("#cardNo").val() == "")
		{
			alert("Please enter the 16 digit ATM Card Number to continue.");
			$("#cardNo").focus();
			return false;
		}
	}

	if(strval == 'Yes')
	{
		transSender="transSend";
		}else{
			transSender="NoSend";
			}
	
	 var minAmount = 0;
	

		
	
	/// Compliance /////////////////////
<?	
if($currentRule["ruleID"]!='') {
?>	


<?
	if($currentRule["matchCriteria"] == "BETWEEN"){
		
		  $betweenAmount= explode("-",$currentRule["amount"]);
		  $fromAmount=$betweenAmount[0];   
		  $toAmount=$betweenAmount[1]; 
		 
		 ?>
  condition=<?=$fromAmount?> <= parseInt(document.addTrans.transAmount.value) && parseInt(document.addTrans.transAmount.value) <= <?=$toAmount?>;
  <?
  }else{
 	?>
 		   	 minAmount=<?=$currentRule["amount"]?>;
 		   	 condition =parseInt(document.addTrans.transAmount.value)<?=$currentRule["matchCriteria"]?> minAmount;
 <?	}?>	

	
		if(condition)
	   {

      	if(confirm("<?=$currentRule["message"]?>"))
    	{
    		ruleConfirmFlag=true;
    	
                
      }else{
      			/*<? 
    	if(CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE == '1'){
    		  	?>
    
   					window.open("add-customer.php?transID=<? echo $_GET["transID"]?>&from=popUp&focusID=<?=$focusButtonID?>&customerID=<? echo $_SESSION["customerID"]?>&agentID=<? echo $_SESSION["senderAgentID"]?>&transSend='+transSender","[_self]","scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740");
    <? } ?>
      	addTrans.localAmount.focus();*/
        
         ruleConfirmFlag=false;
         ruleFlag=true;
      }

    }else{
    	
    	
    	 ruleConfirmFlag=true;
    	
    } 
   <?
    
      }
   ?> 
   
     

   // cummulative amount/////////

  <? if($cumulativeRule["ruleID"]!=''){ ?> 
	var conditionCumulative;
	accumulativeAmount = <? echo $senderAccumulativeAmount ?>;
	<?
	if($cumulativeRule["matchCriteria"] == "BETWEEN"){
		
		
		  $betweenAmount= explode("-",$cumulativeRule["amount"]);
		  $fromAmount=$betweenAmount[0];   
		  $toAmount=$betweenAmount[1]; 
	?>	
    conditionCumulative=<?=$fromAmount?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmount?>;
 <?
  }else{
 	?>
 		    amountToCompare = '<?=$cumulativeRule["amount"]?>';
 		    conditionCumulative=accumulativeAmount  <?=$cumulativeRule["matchCriteria"]?> amountToCompare;	
 <?	}?>		  
     
		
		 if(conditionCumulative)
	   {

      	if(confirm("<?=$cumulativeRule["message"]?>"))
    	{
    		ruleConfirmFlag=true;
    	
                
      }else{
      			/*<? 
    	if(CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE == '1'){
    		  	?>
    
   					window.open("add-customer.php?transID=<? echo $_GET["transID"]?>&from=popUp&focusID=<?=$focusButtonID?>&customerID=<? echo $_SESSION["customerID"]?>&agentID=<? echo $_SESSION["senderAgentID"]?>&transSend='+transSender","[_self]","scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740");
    <? } ?>
      	 addTrans.localAmount.focus();*/
      	
      	 ruleConfirmFlag=false;
         ruleFlag=true;
      }

    }else{
    	 
    	 ruleConfirmFlag=true;
    	
    } 
   
   <?
       
    } 
   ?>
<?
		if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1 )
		{
			?>
				if ( theForm.chequeAmount != null )
				{
					chequeAmount = document.getElementById("chequeAmount").value;
					totalAmount = document.getElementById("totalAmountID").value;
					if ( chequeAmount > totalAmount )
					{
						/*alert("Cheque amount cannot be greater than total amount of transaction.");
						document.getElementById("chequeAmount").focus();
						return false;*/
					}
				}
			<?
		}
	?>
if(ruleConfirmFlag){
	
			<?
			if(CONFIG_TEST_TRANS_LOAD == "1")
			{
				?>
				document.addTrans.action='express-confirm-transaction-2.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusButtonID?>&transSend='+transSender; document.addTrans.submit();
				
			<? 
			}
			elseif(CONFIG_SKIP_CONFIRM == '1')
    		{ 
    		?>
    			document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusButtonID?>&transSend='+transSender; document.addTrans.submit();
    		<?
    		}else{
    		
    			if(CONFIG_CUSTOM_RECIEPT == 1)
    			{
    			?>
    				document.addTrans.action='<? echo CONFIG_RECIEPT_NAME ?>?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusButtonID?>&transSend='+transSender; document.addTrans.submit();
    			<?
    			}else{
    			?>	
    				document.addTrans.action='confirm-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusButtonID?>&transSend='+transSender; document.addTrans.submit();
    			<?
    			}
    		}
    		?>
	
	}
	
	if(ruleFlag){
		   <? 
    	if(CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE == '1'){
    		  	?>
    
   					window.open("add-customer.php?transID=<? echo $_GET["transID"]?>&from=popUp&focusID=<?=$focusButtonID?>&customerID=<? echo $_SESSION["customerID"]?>&agentID=<? echo $_SESSION["senderAgentID"]?>&transSend='+transSender","[_self]","scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740");
    <? } ?>
      	addTrans.localAmount.focus();
		
		}

	return true;

   
}

function checkChequeNo(){
	<? if(CONFIG_CHEQUE_BASED_TRANSACTIONS == "1"){?>
	if(document.getElementById("moneyPaid").value == "By Cheque" && document.getElementById("chequeNo").value == ""){
		
			alert("Provide cheque number");
				return false;
		
		}
	<? }?>
	return true;
	}


function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
   
   
   function resetDate(){
        document.addTrans.dDate.value = '';
        document.addTrans.manageTransDate.value= 'not null';
        
   }
   
   function shorProvinces()
{
	if(document.addTrans.chDiscount.checked)
	{
	document.getElementById("Discount").style.display = '';
	document.getElementById("Fee").style.display = 'none';
	}
	else 
	{
	document.getElementById("Discount").style.display = 'none';
	document.getElementById("Fee").style.display = '';
	
	}
	

}



function batch_trans_msg()
{
	<?
	if($_SESSION["batch"] != "")
	{
	?>
			if (document.frmAmount.amount_transactions.value == "")
			{

				alert("Please Enter total amount for Transaction");
				addTrans.transAmount.value = "";
				frmAmount.amount_transactions.focus();
				return false;	
			}
			else
			{
					var a_left = document.frmAmount.amount_left.value;
					var a_trans = document.frmAmount.amount_transactions.value;
					document.addTrans.t_amount.value = a_trans;
					if(document.frmAmount.amount_left.value == "")
					{
						if (document.frmAmount.help2.value == "")
						{
							document.frmAmount.help2.value = document.frmAmount.amount_transactions.value;
							document.frmAmount.amount_left.value = (document.frmAmount.amount_transactions.value - document.addTrans.transAmount.value);
						}
						else
						{
							document.frmAmount.amount_left.value = (document.frmAmount.help2.value - document.addTrans.transAmount.value);
						}
					}
					else
					{
						if (document.frmAmount.help2.value == "")
						{
							document.frmAmount.help2.value = document.frmAmount.amount_left.value;
							document.frmAmount.amount_left.value = (document.frmAmount.amount_left.value - document.addTrans.transAmount.value);
						}
						else
						{
							document.frmAmount.amount_left.value = (document.frmAmount.help2.value - document.addTrans.transAmount.value);
						}
					}
					document.addTrans.l_amount.value = document.frmAmount.amount_left.value;
					if (document.frmAmount.amount_left.value < 0)
					{
						alert("The amount you entered is greater than amount left");
						document.addTrans.transAmount.value = "";	
						addTrans.transAmount.focus();
						document.frmAmount.amount_left.value = a_left;
						document.addTrans.l_amount.value = a_left;
						return false;
					}
					else
					{
						return true;	
					}
			}
		<? }else{
		?>	
		return true;	
		<?	
		}
		?>
}

function fill()
{
	document.addTrans.t_amount.value = document.frmAmount.amount_transactions.value;
	return true; 	
}

function cleanForm()
{
	document.addTrans.transAmount.value = '';	
	document.addTrans.localAmount.value = '';	
	document.addTrans.refNumber.value = '';	
	document.addTrans.IMFee.value = '';	
	document.addTrans.exchangeRate.value = '';	
	document.addTrans.answer.value = '';	
	document.addTrans.question.value = '';	
	document.addTrans.tip.value = '';	
	document.addTrans.fundSources.value = '';	
	document.addTrans.moneyPaid.value = '';	
	document.addTrans.transactionPurpose.value = '';	
	document.addTrans.totalAmount.value = '';	
	document.addTrans.discount.value = '';	
	document.addTrans.chDiscount.value = '';	
	
	
	}

	$(document).ready(function(){
	
		$("input[@name='bankTransferType']").click(function(){
			//alert($(this).val());
			if($(this).val() == "I")
			{
				$("#bankTransferRow1").show();
				$("#bankTransferRow2").show();
				
				for(var i=3; i<7; i++)
					$("#bankTransferRow"+i).hide();
			}
			else
			{
				$("#bankTransferRow1").hide();
				$("#bankTransferRow2").hide();
					
				for(var i=3; i<7; i++)
					$("#bankTransferRow"+i).show();
			}
		});
		
		<? if($_SESSION["bankTransferType"] == "B") { ?>
			$("#bankTransferType2").click();
		<? }else{ ?>
			$("#bankTransferType1").click();
		<? } ?>
		<? if (CONFIG_SWAP_AMOUNT_LOCALAMOUNT_AJAX != "1") {?>
			$("#IMFee").blur(function(){
				<? if(!$manualCommAjaxFlag){ ?>
							disableClicks();
				<? }elseif($manualCommAjaxFlag){ ?>
					if($("#manualCommission").val()=="Y"){
						enableClicks();
						var amountTrans = $("#transAmountID").val();
						var fee =  $("#IMFee").val();
						var amountTotal =  $("#totalAmountID").val();
						var calculatedByV = $("#calculateByID").val();
						if(isNaN($("#IMFee").val())){
							disableClicks();
							alert("Input is not valid.");
							return false;
						}
						<? if($fromTotalConfig!="1"){?>
							var totalAmFee =parseFloat(amountTrans)+parseFloat(fee);
							$("#totalAmountID").val(totalAmFee);
						
						<? }
							else{
						?>
						if(calculatedByV=="exclusive"){
							var totalAmFee =parseFloat(amountTrans)+parseFloat(fee);
							$("#totalAmountID").val(totalAmFee);
						}
						else{
							$("#transAmountID").val(parseFloat(amountTotal)-parseFloat(fee));
							updateAmounts('total');
						}
						<? }?>
					}
				<? } ?>
			});
		<? }?>	
		
		<? if(CONFIG_SENDER_DOCUMENT_AT_CREATE_TRANS == "1") { ?>
           $("#chkDocument").click(function(){
			   if($("#chkDocument").attr('checked'))
				   $("#showDocument").show();
				else
					$("#showDocument").hide();
			});
		<? } ?>
		/* For masked Input against the credit card No */	
		if($("#cardNo"))
			$("#cardNo").mask("9999999999999999");
		
		$('img').cluetip({splitTitle: '|'});
		$("#value_date").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-1 years"))?>'
		});
	});
	function showZeroCommissionAlert(){
		<? if(CONFIG_SHOW_ALERT_ZERO_COMMISSION=="1"){?>
			var fee =  parseFloat($("#IMFee").val());
			if(fee==0){
				alert("Commission has ZERO value.");
			}
		<? }else{?>
				return true;
		<? }?>
	}
	function showHideHistoryFn()
	{
		if(document.getElementById("historyRow").style.display == "none")
		{
			$("#historyRow").show();
			$("#showHideBtn").html("Hide Detail");
		}
		else
		{
			$("#historyRow").hide();
			$("#showHideBtn").html("Show Detail");
		}
	}
	
	
function checkBenIdCompulsory(ListOption, BenID, BenIdType)
{ 
	if(BenID!="" && BenIdType=="" && ListOption!=false){
		<?
		if ( !defined("CONFIG_DISABLE_EDIT_SENDER") || CONFIG_DISABLE_EDIT_SENDER == 0 || strstr( CONFIG_USERS_ALLOWED_TO_EDIT_BENEFICIARY, $agentType ) !== false )
		{?>
			alert('Selected Beneficiary should have ID details for Transactin Type '+ListOption+'.');
		<?
			if ($_SESSION["batch"]!= "") { ?>
				window.open('add-beneficiary.php?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&benID='+BenID+'&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>&transType=<? echo ($_REQUEST["transType"]!="" ? $_REQUEST["transType"]:$_SESSION["transType"])?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740');
			<? }else{
					if(CONFIG_REMOVE_QUICK_LINK != "1"){ ?>
						window.open('add-beneficiary.php?from=popUp&benID='+BenID+'&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>&transType=<? echo ($_REQUEST["transType"]!="" ? $_REQUEST["transType"]:$_SESSION["transType"])?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740');
					<? }else{?>
						window.open('add-beneficiary-quick.php?from=popUp&benID='+BenID+'&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>&transType=<? echo ($_REQUEST["transType"]!="" ? $_REQUEST["transType"]:$_SESSION["transType"])?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740');
			 <?	} 
			} 
		}?>
	}
}
function checkBenSOFCompulsory(ListOption, BenID, BenSOF)
{ 
	if(BenID!="" && BenSOF=="" && ListOption!=false){
		<?
		if ( !defined("CONFIG_DISABLE_EDIT_SENDER") || CONFIG_DISABLE_EDIT_SENDER == 0 || strstr( CONFIG_USERS_ALLOWED_TO_EDIT_BENEFICIARY, $agentType ) !== false )
		{?>
			alert('Selected Beneficiary should have S/O,W/O,D/O value for Transactin Type '+ListOption+'.');
		<?
			if ($_SESSION["batch"]!= "") { ?>
				window.open('add-beneficiary.php?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&benID='+BenID+'&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>&transType=<? echo ($_REQUEST["transType"]!="" ? $_REQUEST["transType"]:$_SESSION["transType"])?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740');
			<? }else{
					if(CONFIG_REMOVE_QUICK_LINK != "1"){ ?>
						window.open('add-beneficiary.php?from=popUp&benID='+BenID+'&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>&transType=<? echo ($_REQUEST["transType"]!="" ? $_REQUEST["transType"]:$_SESSION["transType"])?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740');
					<? }else{?>
						window.open('add-beneficiary-quick.php?from=popUp&benID='+BenID+'&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>&transType=<? echo ($_REQUEST["transType"]!="" ? $_REQUEST["transType"]:$_SESSION["transType"])?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740');
			 <?	} 
			} 
		}?>
	}
}

function checkBenDOBCompulsory(BenID, DOB, Address1,distID,distribut,cpID)
{ 

    if(distID == distribut || distID == cpID ){
	
		if(Address1=="" || DOB=="0000-00-00")
		 {
				alert('Selected Beneficiary should have DOB and Address1 details');
				window.open('add-beneficiary.php?from=popUp&benID='+BenID+'&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>&transType=<? echo ($_REQUEST["transType"]!="" ? $_REQUEST["transType"]:$_SESSION["transType"])?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740');
		 }
	}
 }
function calculateAmountManualComm(){
	var amountLocal = document.getElementById("totalAmountID").value;
	var fee =  document.getElementById("IMFee").value;
	var totalAmFee =parseFloat(amountLocal)+parseFloat(fee);
	document.getElementById("totalAmountID").value=totalAmFee;
	if(!isNaN(fee)){
		$("#IMFee").blur(function(){
			disableClicks();
		});
	}
}
function updateAccountNumber(){
	<?php if(CONFIG_UPDATE_ACCOUNT_WITH_IBAN=="1"){?>
	var iban = $("#IBAN").val();
	if($("#accNo"))
		$("#accNo").val(iban);
	<?php }?>
}
// end of javascript -->
</script>
<style type="text/css">
<!--
.style1 {color: #FFFFFF;}
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #FFFFFF; font-weight: bold; }
.style4 {
	color: #660000;
	font-weight: bold;
}
.style5 {color: #663333;}
.style6 {color: #005b90;}
.dp-choose-date{
	color:#005b90;
}
.localAmount{
	text-indent:-9000px;
	width:95px;
	height:20px;
	font-size:0;
	line-height: 0;
}
.localAmountBtn{
	left:120px;
	position:relative;
	top:-16px;
	color:#000000;
}
.amount{
	text-indent:-9000px;
	width:95px;
	height:20px;
	font-size:0;
	line-height: 0;
}
.amountBtn{
	left:120px;
	position:relative;
	top:-16px;
	color:#000000;
}
.hideBtns{
	display:none;
	visibility:hidden;
}
-->
    </STYLE>
</HEAD>
<BODY <? if ($_GET['focusID'] != '') { ?> onLoad="document.getElementById('<?=$_GET["focusID"]?>').focus();" <? } ?>>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="5">
  <TR>
    <TD BGCOLOR="#c0c0c0"><B><FONT COLOR="#FFFFFF" SIZE="2"><? echo ($_GET["transID"] != "" ? "Update" : "Create");?> 
      <? echo ($_SESSION["batch"] != "" ? "Batch " : "");?><?=$strTransLabel;?> </FONT></B></TD>
  </TR>
  <? if ($_GET["transID"]!=""){ ?>
 		<TR>
 		<TD><A CLASS="style2" HREF="<?=$_SESSION["back"];?>?search=Search">Go Back</A></TD>
 		</TR>
 	<? } ?>
    <tr>
      <td align="center">
	<?
	  if($msgTime!="Y"){
	?>
	  <FONT COLOR="#FF0000">* Compulsory Fields </FONT>
	<? }?>
        <BR>
        <TABLE WIDTH="700" BORDER="0" CELLPADDING="0" CELLSPACING="0">
          <? if ($_GET["msg"] == "Y" || $_GET["msg1"] == "Y" || $msgTime=="Y"){ ?>
		  <TR>
            <TD><TABLE WIDTH="100%" BORDER="0" CELLPADDING="5" CELLSPACING="0" BGCOLOR="#EEEEEE">
              <TR>
                <TD WIDTH="40" ALIGN="center"><FONT SIZE="5" COLOR="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><B><I><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></I></B></FONT></TD>
                <TD WIDTH="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?></TD>
              </TR>
            </TABLE></TD>
          </TR>
		  <? 
			  if($msgTime=="Y"){
			   exit;
			  }
		   } 
		   ?>
          <tr>
            <td width="700" valign="top"><TABLE WIDTH="700" BORDER="0" CELLPADDING="0" CELLSPACING="0" BORDERCOLOR="#FF0000">
              <?
	$wholeAgentFlag = false;
		if($agentType == "admin" || $agentType == "Call" || $agentType == "Admin Manager" || $agentType == "Branch Manager" || $agentType == "MLRO" || strstr($userDetails["rights"], "Create Transaction") || (CONFIG_CREATE_TRANSACTION_FOR_SUB_AGENT == 1 && $agentType == "SUPA")){
			$wholeAgentFlag = true;
		}
		if(!$hideAgentFlag)
			$wholeAgentFlag = false;
		  if($hideAgentFlag && $wholeAgentFlag)
		  {
		  ?>
		  
                <TR>
                	<TD>&nbsp;</TD>
                </TR>
                
                <? if ($_SESSION["batch"]!= "") { ?>
 		
				 		 			<TR>
				            <TD COLSPAN="4">
				            	<FIELDSET>
				            	<TABLE WIDTH="100%" BORDER="0" CELLPADDING="5" CELLSPACING="0" BGCOLOR="#EEEEEE">
				            		<FORM NAME="frmAmount" METHOD="post" >
				              <TR>
				                <TD  align="center"><FONT  color="#990000">Total amount for transactions</FONT></TD>
				                <TD ><FONT color='#990000'><INPUT TYPE="text" NAME="amount_transactions" VALUE="<?= $_SESSION["amount_transactions"];?>" onChange="return fill();"></FONT></TD>
				              	<TD  align="center"><FONT COLOR="#990000">Amount left</FONT></TD>
				                <TD ><FONT color='#990000'><INPUT TYPE="text" NAME="amount_left" VALUE="<?= $_SESSION["amount_left"];?>" ReadOnly></FONT></TD>
				                <INPUT TYPE="hidden" NAME="help2" VALUE="<?= $_SESSION["amount_left"];?>">
				              </TR>
				            </FORM>
				              </TABLE>
				            </FIELDSET>
				            </TD>
				          </TR>
      
		          <? } ?>
		       <?
		       if(CONFIG_DISABLE_SEARCH != '1' && (CONFIG_CREATE_TRANSACTION_FOR_SUB_AGENT == 1 && $agentType != "SUPA"))
		       {
		       ?>   
		          <TR>
                <TD VALIGN="top"><FIELDSET>
                  <LEGEND CLASS="style2">Search Transaction</LEGEND>
                  <TABLE WIDTH="650" BORDER="0" CELLPADDING="2" CELLSPACING="0">
                    <FORM NAME="frmSearchTrans" METHOD="post" ACTION="add-transaction.php" onSubmit="return checkSearch();">
                      <TR>
                        <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Search Transaction </FONT></TD>
                        <TD WIDTH="500" HEIGHT="20"><INPUT NAME="searchTrans" TYPE="text" VALUE="<?=$_SESSION["searchTrans"];?>" SIZE="15">
                        	<INPUT TYPE="submit" NAME="submitSearch" VALUE="Search">
                        	By Reference No/<?=$manualCode?>
                        </TD>
                      </TR>
                    </FORM>
                  </TABLE>
                  
                  <TABLE WIDTH="650" BORDER="0" CELLPADDING="2" CELLSPACING="0">
                   <? 
  					$transRefNumber = !empty($contentTrans["refNumber"]) ? $contentTrans["refNumber"] : $contentTrans["refNumberIM"];
                    //if ($_SESSION["searchTrans"] != "")
					if ( !empty($transRefNumber) )
                    {
	   $querySearchTrans = "select * from ". TBL_TRANSACTIONS . " where (refNumber = '".$transRefNumber."' OR refNumberIM = '".$transRefNumber."')";
	   $searchTransResult = selectFrom($querySearchTrans);
			if($searchTransResult["transID"] != "")
			{
				?>
                    <TR>
                      <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90"><? echo $systemCode; ?></FONT></TD>
                      <TD WIDTH="150" ALIGN="right"><? echo $searchTransResult["refNumberIM"];?> </TD>
                      <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90"><? echo $manualCode; ?> </FONT></TD>
                      <TD WIDTH="150"><? echo $searchTransResult["refNumber"]?></TD>
                    </TR>
                    <TR>
                      <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Date</FONT></TD>
                      <TD WIDTH="150"><? echo dateFormat($searchTransResult["transDate"], "2")?></TD>
                      <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Status</FONT></TD>
                      <TD WIDTH="150"><? echo $searchTransResult["transStatus"]?></TD>
                    </TR>
                    <TR>
                      <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Total Amount</FONT></TD>
                      <TD WIDTH="150"><? echo $searchTransResult["totalAmount"]?></TD>
                      <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Created By</FONT></TD>
                      <?
				  if($searchTransResult["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$searchTransResult["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);
				  }
				  else

				  {
				  $agentContent2 = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$searchTransResult["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent2["name"]);
				  }
				  ?>
                      <TD WIDTH="150"><? echo $createdBy?></TD>
                    </TR>
            <? 
            if($searchTransResult["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent2 = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$searchTransResult["customerID"]."'");  
				   $beneContent2 = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$searchTransResult["benID"]."'");
				   ?>
				   <TR>
                      <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Sender Name</FONT></TD>
                      <TD WIDTH="150"><? echo ucfirst($customerContent2["FirstName"]). " " . ucfirst($customerContent2["LastName"]).""?></TD>
                      <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Beneficiary Name</FONT></TD>
                      <TD WIDTH="150"><? echo ucfirst($beneContent2["firstName"]). " " . ucfirst($beneContent2["lastName"]).""?></TD>
                    </TR>
            <?
				   }
				   else
				   {
				  	$customerContent2 = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$searchTransResult["customerID"]."'");
				  	$beneContent2 = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$searchTransResult["benID"]."'");
				  	?>
				  					<TR>
                      <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Sender Name</FONT></TD>
                      <TD WIDTH="150"><? echo ucfirst($customerContent2["firstName"]). " " . ucfirst($customerContent2["lastName"]).""?></TD>
                      <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Beneficiary Name</FONT></TD>
                      <TD WIDTH="150"><? echo ucfirst($beneContent2["firstName"]). " " . ucfirst($beneContent2["lastName"]).""?></TD>
                    </TR>
            <? } 
					if ( strtoupper($_SESSION["transactionCreatedBy"]) != "CUSTOMER" )
					{
						?>
							<TR>
							  <TD WIDTH="150" ALIGN="right">&nbsp;</TD>
							  <TD WIDTH="150">&nbsp;</TD>
							  <TD WIDTH="150" ALIGN="right">&nbsp;</TD>
							  <TD WIDTH="150" ALIGN="right"><A HREF="add-transaction.php?search=end" CLASS="style2">End Search</A></TD>
							</TR>
						<?
					}
			  }else{
			  ?>
			               <TR>
                      <TD WIDTH="150" ALIGN="Center">&nbsp;</TD>
                     </TR>
			               <TR>
                      <TD WIDTH="150" ALIGN="Center">
                      	<I><FONT COLOR="#FF0000">Sorry, No Transaction Found!</FONT></I>
                      </TD>
                     </TR>
                     <TR>
                      <TD WIDTH="150" ALIGN="right"><A HREF="add-transaction.php?search=end" CLASS="style2">End Search</A></TD>
                     </TR>
            <? }
          }
             ?>
                  </TABLE>
                </FIELDSET></TD>
              </TR>
		     <?
		    } 
			
		
			if ( strtoupper($_SESSION["transactionCreatedBy"]) != "CUSTOMER" )
			{
				/*
				** Online transaction check
				** This check is added in order to hide agent details for online transactions as there is no
				** agent involved in the online transaction. Online transactions are created by customers themselve.
				 */
		     ?>
              <TR>
                <TD VALIGN="top"><FIELDSET>
                  <LEGEND CLASS="style2"><?=__("Agent")?></LEGEND>
									<?  
										if(strpos(CONFIG_ASSOCIATED_ADMIN_TYPE, $agentType.",") === false){
											$flagAgent = False;
										}else{
											$flagAgent = True;
										}
									?>
                  
                  
 
                  <? 
					//debug($_SESSION["senderAgentID"]);
				  if($_SESSION["senderAgentID"] == "")
									{
									?>
                  <TABLE WIDTH="650" BORDER="0" CELLPADDING="2" CELLSPACING="0">
                    <FORM NAME="frmSearch">
                  <? 	
                   	
                   if(CONFIG_ADMIN_ASSOCIATE_AGENT == "1" && $flagAgent){
									?>
                   	   <TR>
                        <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><?=__("Agent")?> Name or Number<FONT COLOR="#ff0000">*</FONT> </FONT></TD>
                        <TD WIDTH="500" HEIGHT="20">
                        	<SELECT NAME="aID" ID="aID" STYLE="font-family:verdana; font-size: 11px" onChange="document.frmSearch.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusID?>'; document.frmSearch.submit();">
                        		 <OPTION VALUE="">- Select Agent -</OPTION>
                              <? for($j=0;$j < count($userIDs); $j++)
                              	{
                              		if($userIDs[$j]["userID"] != '')
                              			{
                              	?>
                              <OPTION VALUE="<?=$userIDs[$j]["userID"]?>"><? echo $userIDs[$j]["name"]."[".$userIDs[$j]["username"]."]" ?></OPTION>
                              <? 	
                              		}
                               }
                               ?>
                             
                            </SELECT>
                    	<? }else{ ?>
                    	
                    	
                    	
                      <TR>
                        <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><?=__("Agent")?> Name or Number<FONT COLOR="#ff0000">*</FONT> </FONT></TD>
                        <TD WIDTH="500" HEIGHT="20"><INPUT NAME="agentName" TYPE="text" SIZE="15">
                            <SELECT NAME="agentType" ID="agentType" STYLE="font-family:verdana; font-size: 11px">
                              <? if(CONFIG_CREATE_TRANSACTION_FOR_SUB_AGENT == 1 && $agentType == "SUPA") { ?>
							  <OPTION VALUE="Sub">Sub Agent</OPTION>
							  <? }else{ ?>
                              <OPTION VALUE="Supper">Super Agent</OPTION>
                              <OPTION VALUE="Sub">Sub Agent</OPTION>
							  <? } ?>
                            </SELECT>
                            
                            <? if ($_SESSION["batch"]!= "") { ?>
                            <INPUT TYPE="button" NAME="Submit" VALUE="Search" onClick=" if(document.frmSearch.agentName.value != '') { window.open('search-agent.php?val=' + document.frmAmount.amount_transactions.value + '&agentName=' + document.frmSearch.agentName.value + '&agentType=' + document.frmSearch.agentType.value  + '&transID=<? echo $_GET["transID"]?>','searchAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') } else { alert ('Please search and select agent.')}">
                          <? }else{ ?>
                            <INPUT TYPE="button" NAME="Submit" VALUE="Search" onClick=" if(document.frmSearch.agentName.value != '') { window.open('search-agent.php?agentName=' + document.frmSearch.agentName.value + '&agentType=' + document.frmSearch.agentType.value  + '&transID=<? echo $_GET["transID"]?>','searchAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') } else { alert ('Please search and select agent.')}">
                          <? }
                          		}
                           ?>
                        </TD>
                      </TR>
                    </FORM>
                  </TABLE>
                  <?
								}
								else
								{          
				        if($_GET["transID"] == "" || UPDATE_TRANSACTIONS_AGENT == '1')
				        {
				        	?>
				        	<TABLE WIDTH="650" BORDER="0" CELLPADDING="2" CELLSPACING="0">
                    <FORM NAME="frmSearch">
				        	<?
				        	if(CONFIG_ADMIN_ASSOCIATE_AGENT == "1" && $flagAgent){ ?>
				        	
				        	<TR> 
	 	                        <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><?=__("Agent")?> Name or Number<FONT COLOR="#ff0000">*</FONT> </FONT></TD> 
	 	                        <TD WIDTH="500" HEIGHT="20"> 
	 	                                <SELECT NAME="aID" ID="aID" STYLE="font-family:verdana; font-size: 11px" onChange="document.frmSearch.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusID?>'; document.frmSearch.submit();"> 
	 	                                         <OPTION VALUE="">- Select Agent -</OPTION> 
	 	                              <? for($j=0;$j < count($userIDs); $j++) 
	 	                                {  
	 	                                        if($userIDs[$j]["userID"] != '') 
	 	                                        {        
	 	                                        ?> 
	 	                                        <OPTION VALUE="<?=$userIDs[$j]["userID"]?>" <? if($_SESSION["senderAgentID"] == $userIDs[$j]["userID"]){echo("selected");} ?>><? echo $userIDs[$j]["name"]."[".$userIDs[$j]["username"]."]" ?></OPTION> 
	 	                                        <?  
	 	                                         } 
	 	                                        }  
	 	                               
	 	                              ?> 
	 	                              
	 	                            </SELECT> 
									</TR>
				        	
				        	<? }else{
				        	
				        ?>
                  
                      <TR>
                        <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><?=__("Agent")?> Name or Number<FONT COLOR="#ff0000">*</FONT> </FONT></TD>
                        <TD WIDTH="500" HEIGHT="20"><INPUT NAME="agentName" TYPE="text" SIZE="15">
                            <SELECT NAME="agentType" ID="agentType" STYLE="font-family:verdana; font-size: 11px">
                              <? if(CONFIG_CREATE_TRANSACTION_FOR_SUB_AGENT == 1 && $agentType == "SUPA") { ?>
							  <OPTION VALUE="Sub">Sub Agent</OPTION>
							  <? }else{ ?>
                              <OPTION VALUE="Supper">Super Agent</OPTION>
                              <OPTION VALUE="Sub">Sub Agent</OPTION>
							  <? } ?>
                            </SELECT>
                            <? if ($_SESSION["batch"]!= "") { ?>
                            <INPUT TYPE="button" NAME="Submit" VALUE="Search" onClick=" if(document.frmSearch.agentName.value != '') { window.open('search-agent.php?val=' + document.frmAmount.amount_transactions.value + '&agentName=' + document.frmSearch.agentName.value + '&agentType=' + document.frmSearch.agentType.value  + '&transID=<? echo $_GET["transID"]?>','searchAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') } else { alert ('Please search and select agent.')}">
                          <? }else{ ?>
                            <INPUT TYPE="button" NAME="Submit" VALUE="Search" onClick=" if(document.frmSearch.agentName.value != '') { window.open('search-agent.php?agentName=' + document.frmSearch.agentName.value + '&agentType=' + document.frmSearch.agentType.value  + '&transID=<? echo $_GET["transID"]?>','searchAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') } else { alert ('Please search and select agent.')}">
                          <? } ?>
                        </TD>
                      </TR>
                    
                <?
              }
              ?>
              </FORM>
                  </TABLE>
              <?
              }
                ?>   
                  
                  <TABLE WIDTH="650" BORDER="0" CELLPADDING="2" CELLSPACING="0">
                    <? 
		
			$queryCust = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $_SESSION["senderAgentID"] . "'";
			if($agentType == "Branch Manager"){
				$queryCust .= " and parentID = '$parentID'";						
			}
			$senderAgentContent = selectFrom($queryCust);
			if($senderAgentContent["userID"] != "")
			{
		?>
                    <TR>
                      <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90"><?=__("Agent")?> Name</FONT></TD>
                      <TD WIDTH="200"><? echo $senderAgentContent["name"];?> </TD>
                      <TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Contact Person </FONT></TD>
                      <TD WIDTH="200"><? echo $senderAgentContent["agentContactPerson"]?></TD>
                    </TR>
                    <TR>
                      <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Address</FONT></TD>
                      <TD COLSPAN="3"><? echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></TD>
                    </TR>
                    <TR>
                      <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Company</FONT></TD>
                      <TD WIDTH="200"><? echo $senderAgentContent["agentCompany"]?></TD>
                      <TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Country</FONT></TD>
                      <TD WIDTH="200"><? echo $senderAgentContent["agentCountry"]?></TD>
                    </TR>
                    <TR>
                      <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Phone</FONT></TD>
                      <TD WIDTH="200"><? echo $senderAgentContent["agentPhone"]?></TD>
                      <TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Email</FONT></TD>
                      <TD WIDTH="200"><? echo $senderAgentContent["email"]?></TD>
                    </TR>
                    <?
			  }
			  ?>
                  </TABLE>
                  <?
			}
			?>
			
                </FIELDSET></TD>
              </TR>
              <?
		 } // Online transaction check ended here.
	  }
	// first/single distributor is used in bank transfer transactions
	if( CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS != "1" && $_SESSION["transType"] == "Bank Transfer" && CONFIG_SINGLE_DISTRIBUTOR_SYSTEM == "1") {
		$distributors = "select userID from admin where parentID > 0 and adminType='Agent' and isCorrespondent = 'ONLY'  and PPType = '' and agentStatus = 'Active' order by userID ASC";
		$idaSingle = selectFrom($distributors);
		$_SESSION["benAgentID"] = $_SESSION["distribut"] = $idaSingle["userID"];
	}
//debug($_SESSION["senderAgentID"]);
?>
              <FORM ACTION="<?=$confirmURL?>?transID=<?=$_GET["transID"]?>" METHOD="post" NAME="addTrans" >
              	<INPUT TYPE="hidden" NAME="customerAgent" ID="customerAgentID" VALUE="<? echo $_SESSION["senderAgentID"];?>">
                <INPUT TYPE="hidden" NAME="distribut" id="distribut" VALUE="<? echo $_SESSION["benAgentID"];?>">
				<INPUT NAME="collectionPointID" TYPE="hidden" ID="collectionPointID" VALUE="<? echo $_SESSION["collectionPointID"]?>">
<?
			if(($agentType != "admin" && $agentType != "Call" && $agentType != "Admin Manager") || $_SESSION["senderAgentID"]!="")
		  	{
		  	$contentsAgentCountry = selectFrom("select agentCountry from ".TBL_ADMIN_USERS." where username='$username'");
			$agentCountry = $contentsAgentCountry["agentCountry"];
		  	?>
			
				
				<? 
				
		  }
	//if ($_SESSION["transType"] != "")
	//{ ?>
                <TR>
                  <TD>
<FIELDSET>
          <LEGEND CLASS="style2"><?=__("Sender");?> Details </LEGEND>
        
          <TABLE WIDTH="699" BORDER="0" CELLPADDING="2" CELLSPACING="0">
          	<?
          	
          	$flag = 1;
          	if($agentType == "SUPA" ||  $agentType == "SUPAI")
          	{
          		$flag = 1;	
          	}else{
          			if($_SESSION["senderAgentID"]!="")
      				{
      					$flag = 1;	
      				}
          		}
			//debug($flag);
          	if($_GET["transID"] == "" && $flag)
          	{
          		
          	if (CONFIG_PAYIN_RESTRICT == '1') 
          	{
				$arrAgentTypes = explode(",", CONFIG_PAYIN_RESTRICT_LIST);
				if (in_array($agentType, $arrAgentTypes)) 
				{
					$queryCust .= " AND payinBook = '' ";
					$queryCnt  .= " AND payinBook = '' ";
				}
			}	
          	?>
          	
            <TR>
       	<?	if (CONFIG_SENDER_SEARCH_FORMAT == '1') {  ?>
       				<TD ALIGN="left" WIDTH="180"><FONT COLOR="#005b90"><?=__("Sender")?> Number</FONT>
            <INPUT NAME="custNumber" TYPE="text" ID="custNumber" SIZE="10"></TD>
             <TD ALIGN="left" WIDTH="170"> <FONT COLOR="#005b90"><?=__("Sender")?> Name</FONT>
              <INPUT NAME="custName" TYPE="text" ID="custName" SIZE="10"></TD>
              <TD ALIGN="left"><FONT COLOR="#005b90">Contact Number</FONT>
              <INPUT NAME="contactNumber" TYPE="text" ID="contactNumber" SIZE="10">
									<? if ($_SESSION["batch"]!= "") { ?>
                  <INPUT TYPE="button" NAME="Submit" VALUE="Search" onClick=" if(document.addTrans.custName.value == '' && document.addTrans.custNumber.value == '') { alert ('Please provide sender name or number OR both.') } else { window.open('search-cust.php?custNumber='+ document.addTrans.custNumber.value +'&val=' + document.frmAmount.amount_transactions.value + '&custName=' + document.addTrans.custName.value + '&transID=<? echo $_GET["transID"]?>' + '&agentID=<? echo $_SESSION["senderAgentID"]?>', 'searchCust', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') }">
                <? }else{ 
                				if (CONFIG_SENDER_SEARCH_FORMAT == '1') {	?>
                	<INPUT TYPE="button" NAME="Submit" VALUE="Search" onClick=" if(document.addTrans.custName.value == '' && document.addTrans.custNumber.value == '' && document.addTrans.contactNumber.value == '') { alert ('Please provide sender name/number OR contact number OR all three to search.') } else { window.open('search-cust.php?search=Y'+'&custNumber='+ document.addTrans.custNumber.value +'&contactNumber='+ document.addTrans.contactNumber.value +'&custName=' + document.addTrans.custName.value + '&transID=<? echo $_GET["transID"]?>' + '&agentID=<? echo $_SESSION["senderAgentID"]?>', 'searchCust', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') }">
                					<? } else{?>
                  <INPUT TYPE="button" NAME="Submit" VALUE="Search" onClick=" if(document.addTrans.custName.value == '' && document.addTrans.custNumber.value == '') { alert ('Please provide sender name or number OR both.') } else { window.open('search-cust.php?custNumber='+ document.addTrans.custNumber.value +'&custName=' + document.addTrans.custName.value + '&transID=<? echo $_GET["transID"]?>' + '&agentID=<? echo $_SESSION["senderAgentID"]?>', 'searchCust', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') }">
								<? } }?>
							</TD>
      <?	} else {  ?>
              <TD ALIGN="right" WIDTH="100"><FONT COLOR="#005b90">Search Sender<FONT COLOR="#ff0000">*</FONT> </FONT></TD>
              <TD COLSPAN="3"><INPUT NAME="custName" TYPE="text" ID="custName" SIZE="13">
                  <SELECT NAME="customerSearch" ID="customerSearch">
									  <OPTION VALUE="1"> Sender Name</OPTION>
									<?
									if(!in_array($agentType, $arrAgentTypes) ){
											if( strstr(CONFIG_PAYING_BOOK_AGENTS,$_SESSION["senderAgentID"])) {
									?>
									  <OPTION VALUE="2"> <? if(CONFIG_PAYIN_CUSTOMER == '1'){echo("Payin Book Number");}else{echo("Sender Number");}?></OPTION>
									<?
									}}
									?>
									<?	if (CONFIG_PAYIN_CUSTOMER == '1') {  ?>
									 <OPTION VALUE="4" <? echo ($customerSearch == 4 ? "selected" : "") ?>> <? if(CONFIG_PAYIN_CUSTOMER == '1'){echo("Sender Number");}?></OPTION>
									<?	}  ?>
									  <OPTION VALUE="3"> Contact Number</OPTION>
									  <? if(CONFIG_SEARCH_SENDER_BY_POSTCODE == "1") { ?>
										  <option value="5">By Postcode</option>
									  <? } ?>
									</SELECT>
									<? if ($_SESSION["batch"]!= "") { ?>
                  <INPUT TYPE="button" NAME="Submit" VALUE="Search" onClick=" if(document.addTrans.custName.value != '') { window.open('search-cust.php?customerSearch='+ document.addTrans.customerSearch.value +'&val=' + document.frmAmount.amount_transactions.value + '&custName=' + document.addTrans.custName.value + '&searchid=' + document.addTrans.customerSearch.value + '&transID=<? echo $_GET["transID"]?>' + '&agentID=<? echo $_SESSION["senderAgentID"]?>', 'searchCust', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') } else { alert ('Please provide sender name or number.')}">
                <? }else{ ?>
                  <INPUT TYPE="button" NAME="Submit" VALUE="Search" onClick=" if(document.addTrans.custName.value != '') { window.open('search-cust.php?customerSearch='+ document.addTrans.customerSearch.value +'&custName=' + document.addTrans.custName.value + '&searchid=' + document.addTrans.customerSearch.value + '&transID=<? echo $_GET["transID"]?>' + '&agentID=<? echo $_SESSION["senderAgentID"]?>', 'searchCust', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') } else { alert ('Please provide sender name or number.')}">
								<? } ?>
        </TD>
      <?	}  
	  $regularLabel = "Add New Regular";
	  if(defined("CONFIG_ADD_SENDER_TRANS_LABEL") && CONFIG_ADD_SENDER_TRANS_LABEL!="0"){
		  $regularLabel = CONFIG_ADD_SENDER_TRANS_LABEL;
	  }
	  if (CONFIG_SENDER_SEARCH_FORMAT == '1') {?>
              <!--td width="280" align="center" bgcolor="#c0c0c0"><a href="javascript:;" class="style3" onClick=" window.open('<?=$customerPage?>?from=popUp&agnetID=<?=$_SESSION["senderAgentID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">Add New Sender </a></td-->
            	<TD WIDTH="90" ALIGN="center" BGCOLOR="#c0c0c0">
            		<? if ($_SESSION["batch"]!= "") { ?>
            		<A HREF="javascript:;" CLASS="style3" onClick=" window.open('<?=$customerPage?>?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&agnetID=<?=$_SESSION["senderAgentID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
            		<? }else{ ?>
            		<a href="javascript:;" class="style3" onClick=" window.open('<?=$customerPage?>?from=popUp&agnetID=<?=$_SESSION["senderAgentID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
            	<? } ?><?=$regularLabel?> <?=__("Sender")?></A>
						<? if(CONFIG_SENDER_AS_COMPANY =="1"){ ?> 
							<a href="javascript:;" class="style3" onClick=" window.open('add-company.php?from=popUp&agentID=<?=$_SESSION["senderAgentID"]; ?>','add compnay', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
							/ Add New Company</A>
						<? } ?>
                </TD> 
            </TR>
          
              <? }else{ ?>
          
          <TD WIDTH="200" ALIGN="center" BGCOLOR="#c0c0c0">
            		<? if ($_SESSION["batch"]!= "") { ?>
            		<A HREF="javascript:;" CLASS="style3" onClick=" window.open('<?=$customerPage?>?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&agnetID=<?=$_SESSION["senderAgentID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
            		<? }else{ ?>
            		<a href="javascript:;" class="style3" onClick=" window.open('<?=$customerPage?>?from=popUp&agnetID=<?=$_SESSION["senderAgentID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
            	<? } ?><?=$regularLabel?> <?=__("Sender")?></A>
                
                <? if ($_SESSION["batch"]!= "") { ?>
                <A HREF="javascript:;" CLASS="style3" onClick=" window.open('add-customer-Quick.php?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&agnetID=<? echo $_SESSION["senderAgentID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
                <? }else{ ?>
                			<? if(CONFIG_REMOVE_QUICK_SENDER_LINK == "1"){ ?>
                			<? }else{ ?>
                <a href="javascript:;" class="style3" onClick=" window.open('add-customer-Quick.php?from=popUp&agnetID=<? echo $_SESSION["senderAgentID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
                
                	/Add 
                New Quick <?=__("Sender")?></A> <? } } ?> </TD>
            </TR>
       
       <? }}?>
               <INPUT TYPE="hidden" NAME="customerID" id="customerID" VALUE="<? echo $_SESSION["customerID"]?>">
            </TABLE>
            <TABLE  width="699" BORDER="0" CELLPADDING="2" CELLSPACING="0">   
            <? 
if($_GET["customerID"]!="")
	$_SESSION["customerID"] = $_GET["customerID"];

if ( strtoupper($_SESSION["transactionCreatedBy"]) == "CUSTOMER" )
{
	$queryCust = "select * from cm_customer where c_id ='".$_SESSION[customerID]."'";
	//debug( $queryCust );
//	debug( $contentTrans );
//	debug( $searchTransResult );
	$tmpCustContent = selectFrom($queryCust);

	$customerContent["customerID"] = $tmpCustContent["c_id"];
  	$customerContent["Title"] = $tmpCustContent[""];
    $customerContent["firstName"] = $tmpCustContent["FirstName"];
    $customerContent["lastName"] = $tmpCustContent["LastName"];
    $customerContent["middleName"] = $tmpCustContent["MiddleName"];
    $customerContent["accountName"] = $tmpCustContent["c_name"]; // I am not sure about it.
    $customerContent["Address"] = $tmpCustContent["c_address"];
 	$customerContent["Address1"] = $tmpCustContent["c_address2"];
    $customerContent["proveAddress"] = $tmpCustContent["proveAddress"];
    $customerContent["City"] = $tmpCustContent["c_city"];
    $customerContent["State"] = $tmpCustContent["c_state"];
    $customerContent["Zip"] = $tmpCustContent["c_zip"];
    $customerContent["Country"] = $tmpCustContent["c_country"];
    $customerContent["Phone"] = $tmpCustContent["c_phone"];
    $customerContent["Email"] = $tmpCustContent["c_email"];
    $customerContent["IDissuedate"] = "";
    $customerContent["IDExpiry"] = "";
    $customerContent["payinBook"] = "";
    $customerContent["remarks"] = $tmpCustContent["remarks"];
    $customerContent["IDType"] = "";
    $customerContent["IDNumber"] = "";
    $customerContent["otherId"] = "";
    $customerContent["otherId_name"] = "";
}
else
{ 
if(CONFIG_SENDER_AS_COMPANY == "1"){
	$extraFieldSender = ",company_msb_number";
}
	$queryCust = "select customerID, Title, firstName, lastName, middleName,accountName, Address, Address1, proveAddress, City, State, Zip,Country, Phone, Email, IDissuedate, IDExpiry, payinBook, remarks,IDType, IDNumber,otherId,otherId_name,customerType".(CONFIG_REMARKS_ABOUT_SENDER == "1"?",senderRemarks":"")." ".$extraFieldSender ." from " . TBL_CUSTOMER . " where customerID ='".$_SESSION["customerID"]."'";
	//debug( $queryCust );
	//debug( $searchTransResult );
	$customerContent = selectFrom($queryCust);
	//debug( $customerContent );
}

if($customerContent["customerID"] != "")
{
		if($_POST["document_remarks"] != $_SESSION["document_remarks"] && $_GET["from"] != 'conf')
		{
		$updateSender = "update ".TBL_CUSTOMER." set remarks ='".$_POST["document_remarks"]."' where customerID = '".$_SESSION["customerID"]."'";	
		update($updateSender);
		$_SESSION["document_remarks"] = $_POST["document_remarks"];
		}else{	
				$_SESSION["document_remarks"] = $customerContent["remarks"];
		}
		
?>
            <? if(CONFIG_DISPLAY_SENDER_REFERENCE_NUMBER == "1"){ ?>
            <TR>
              <TD WIDTH="157" ALIGN="right"><FONT COLOR="#005b90"><?=__("Sender")?> Reference Number</FONT></TD>
              <TD COLSPAN="6"><STRONG><? echo $customerContent["accountName"] ?></STRONG></TD>
            </TR>
            <? } ?>
            <TR>
              <TD WIDTH="157" ALIGN="right"><FONT COLOR="#005b90"><?=__("Sender")?> Name</FONT></TD>
              <TD COLSPAN="6"><? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></TD>
            </TR>
            
            <TR>
              <TD WIDTH="157" ALIGN="right"><FONT COLOR="#005b90">Address</FONT></TD>
              <TD COLSPAN="6"><? echo $customerContent["Address"] . " " . $customerContent["Address1"]?></TD>
            </TR>
            <? if(CONFIG_SHOW_CITY == '1'){
            ?>	<TR>
            			<TD ALIGN="right"><FONT COLOR="#005b90">City </FONT></TD>
              		<TD COLSPAN="6"><? echo $customerContent["City"]?></TD> 
              	</TR>
           <?	}  ?>
		<TR>		
	 		<?	if (CONFIG_PROVE_ADDRESS == "1") {  ?>
            
              <TD WIDTH="157" ALIGN="right"><FONT COLOR="#005b90">Proved Address</FONT></TD>
              <TD WIDTH="179"><? echo ($customerContent["proveAddress"] == "Y" ? "Yes" : "No") ?></TD>
          	<?	
			}
				if(CONFIG_SENDER_AS_COMPANY == "1" && $customerContent["customerType"] == "company"){
			?>
			
			 <TD WIDTH="157" ALIGN="right"><FONT COLOR="#005b90">MSB Number</FONT></TD>
             <TD WIDTH="179"><? echo $customerContent["company_msb_number"] ?></TD>
         
		  <? } ?>
		   </TR>
		  <?
            if(CONFIG_SHOW_ID_DETAILS == "1"){
            	
            ?>
            	<TR>
	              <TD WIDTH="157" ALIGN="right"><FONT COLOR="#005b90">ID Type</FONT></TD>
	              <TD WIDTH="179"><? echo ($customerContent["IDType"] != "" ? $customerContent["IDType"] : CONFIG_ID_DETAILS_MSG);?></TD>
	              <TD ALIGN="right"><FONT COLOR="#005b90">ID Number</FONT></TD>
	              <TD WIDTH="280"><? echo ($customerContent["IDNumber"] != "" ? $customerContent["IDNumber"] : CONFIG_ID_DETAILS_MSG);?></TD>
            	</TR>
            	<TR>
	              <TD WIDTH="157" ALIGN="right"><FONT COLOR="#005b90">ID Issue date</FONT></TD>
	              <TD WIDTH="179"><? echo ($customerContent["IDissuedate"] != '0000-00-00' ? dateFormat($customerContent["IDissuedate"]) : CONFIG_ID_DETAILS_MSG);?></TD>
	              <TD WIDTH="130" ALIGN="right"><FONT COLOR="#005b90">ID Expiry Date</FONT></TD>
	              <TD WIDTH="280"><? echo ($customerContent["IDExpiry"] != '0000-00-00' ? dateFormat($customerContent["IDExpiry"]) : CONFIG_ID_DETAILS_MSG);?></TD>
            	</TR>
            <?
            }
            ?>
          	
          <? if(CONFIG_ID_MANDATORY == '1'){?>
          <TR>
              <TD WIDTH="157" ALIGN="right"><FONT COLOR="#005b90">ID Type</FONT></TD>
              <TD WIDTH="179"><? echo $customerContent["IDType"]?></TD>
              <TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">ID Number</FONT></TD>
              <TD WIDTH="280"><? echo $customerContent["IDNumber"]?></TD>
            </TR>
            
            <TR>
              <TD WIDTH="157" ALIGN="right"><FONT COLOR="#005b90">Other ID Type</FONT></TD>
              <TD WIDTH="179"><? echo $customerContent["otherId_name"]?></TD>
              <TD WIDTH="280" ALIGN="right"><FONT COLOR="#005b90">Other ID Number</FONT></TD>
              <TD WIDTH="280"><? echo $customerContent["otherId"]?></TD>
            </TR>
          	<?}?>
          	
            <TR>
              <TD WIDTH="157" ALIGN="right"><FONT COLOR="#005b90">Postal / Zip</FONT></TD>
              <TD WIDTH="179"><? echo $customerContent["Zip"]?></TD>
              <TD WIDTH="67" ALIGN="right"><FONT COLOR="#005b90">Country</FONT></TD>
              <TD WIDTH="280" COLSPAN="3"><? echo $customerContent["Country"]?>
              <INPUT TYPE="hidden" NAME="custCountry" VALUE="<? echo $customerContent["Country"]?>"></TD>
            </TR>
            <TR>
              <TD WIDTH="157" ALIGN="right"><FONT COLOR="#005b90">Phone</FONT></TD>
              <TD WIDTH="179"><? echo $customerContent["Phone"]?></TD>
              <TD WIDTH="67" ALIGN="right"><FONT COLOR="#005b90">Email</FONT></TD>
              <TD WIDTH="280"><? echo $customerContent["Email"]?></TD>
            </TR>
			<? if(CONFIG_SHOW_SENDER_MOBILE == "1") { ?>
			<tr>
              <td width="157" align="right"><font color="#005b90">Mobile</font></td>
              <td width="179"><? echo $customerContent["Mobile"]?></td>
              <td width="67" align="right">&nbsp;</td>
              <td width="280">&nbsp;</td>
            </tr>
			<? } ?> 
			<!-- Added by Niaz Ahmad at 02-07-2009 @4997 AMB -->
			<? if(CONFIG_SENDER_AS_COMPANY == "1"){ ?>
			<TR>
			   <TD WIDTH="157" ALIGN="right"><FONT COLOR="#005b90"><?=__("Sender");?> Type</FONT></TD>
			   <? 
				   	if($customerContent["customerType"] == "company")
				         $custType = "company";
					else
						 $custType = "individual";	 
			  ?>
              <TD WIDTH="179"><?=$custType;?></TD>
			  <TD WIDTH="67" align="right">&nbsp;</td>
              <TD WIDTH="280">&nbsp;</td>
            </TR>
			<? } ?> 
			<!-- Added by Niaz Ahmad at 02-07-2009 @5052 AMB -->
			<? 
				if(CONFIG_SENDER_DOCUMENT_AT_CREATE_TRANS == "1"){
					if(empty($_SESSION["docCategory"]))
							$style ="display:none";  	
			?>
			<TR>
			   <TD WIDTH="50" ALIGN="right"><FONT COLOR="#005b90">Document Provided</FONT>&nbsp;
			   <input type="checkbox" name="chkDocument" id="chkDocument" value="Y" <?=(!empty($_SESSION["docCategory"])?'checked="checked"':'')?>/>
			   </TD>
			     <TD WIDTH="200" align="right" id="showDocument" style="<?=$style?>">
			   <?
			   		$categoryQuery = "select id,description from ".TBL_CATEGORY." "; 
                  	$categoryData = selectMultiRecords($categoryQuery);
					for ($i=0; $i < count($categoryData); $i++)
						{ 
						  $categoryName = $categoryData[$i][description];
						  echo $categoryName;
				?>	
				  &nbsp;
				
				  <input type="checkbox" name="docCategory[<?=$categoryData[$i]["id"]?>]" id="<?=$categoryName?>" value="Y" 
				  <?=($_SESSION["docCategory"][$categoryData[$i]["id"]]) == "Y"? 'checked="checked"':''?> /><br />
              <? } ?>
			   </TD>
			   <TD WIDTH="67">&nbsp;</td>
              <TD WIDTH="100">&nbsp;</td>
			  </TR>
			<? } ?> 
			
            <?	if (CONFIG_CUST_DOC_FUN == '1') {  ?>
             <TR>
              <TD WIDTH="150" HEIGHT="20">&nbsp;</TD>
              <TD WIDTH="200" HEIGHT="20">&nbsp;</TD>
              <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"> Remarks</FONT> </TD>
              <TD WIDTH="200" HEIGHT="20"><INPUT TYPE="text" NAME="document_remarks" VALUE="<? if($_SESSION["document_remarks"] != ""){ echo $_SESSION["document_remarks"];}else{ echo $customerContent["remarks"];} ?>"></TD>
            </TR>
      
       		<?	}  ?>          
            
			<?
				if (CONFIG_COMPLIANCE_SENDER_TRANS == "1" || CONFIG_EDIT_SENDER == "1")
				{
			?>
            <TR>
            	
            <?
			/* View old transaction accumulative data */
            if($bolViewOldTransactions)
			{
            ?>
            <td align="center" bgcolor='#c0c0c0' width="290">
				<font color="#005b90">
					<a href="javascript:;" class="style3" onClick=" window.open('compliance_cust_trans.php?customer=<?=$customerContent["customerID"]?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=780')">
						Last <?=$intBackDays?> Day Transactions 
					</a><?=$intTotalCustomerTransactions?>
            	</font>
			</td>
			<td align="center" bgcolor='#c0c0c0' width="280">
				<font color="#005b90"> Total Amount <? echo(round($fltTotalAmountSend, $roundLevel));?></font>
			</td>		
            <?	} else {  ?>
            	<td colspan="2" align="center">&nbsp;</td>
            <?	}  ?>


            	<TD ALIGN="center">&nbsp;</TD>
		<?
			if ( strtoupper($_SESSION["transactionCreatedBy"]) != "CUSTOMER" )
			{
				?>	
            	<TD ALIGN="center" <?=( !defined("CONFIG_DISABLE_EDIT_SENDER") || CONFIG_DISABLE_EDIT_SENDER == 0 || strstr( CONFIG_USERS_ALLOWED_TO_EDIT_SENDER, $agentType ) !== false ? "bgcolor='#c0c0c0'" : ""); ?>>
				<?	
				if ( !defined("CONFIG_DISABLE_EDIT_SENDER") || CONFIG_DISABLE_EDIT_SENDER == 0 || strstr( CONFIG_USERS_ALLOWED_TO_EDIT_SENDER, $agentType ) !== false )
				{
				
					if ($_SESSION["batch"]!= "") { ?>
					<A HREF="#" onClick="javascript:window.open('add-beneficiary.php?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&customerID=<? echo $customerContent["customerID"] ?>&transID=<? echo $_GET["transID"] ?>&transType=<?=($_REQUEST["transType"]!=""?$_REQUEST["transType"]:$_SESSION["transType"])?>', 'editCust', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" CLASS="style3">
					<? }else{ 
							if(CONFIG_SENDER_AS_COMPANY == "1")
							        {
									  if($customerContent["customerType"] == "company")
								 		{
											$customerPage = "add-company.php";
										}	
									}	
					?>
					<a href="#" onClick="javascript:window.open('<?=$customerPage?>?from=popUp&customerID=<? echo $customerContent["customerID"] ?>&transID=<? echo $_GET["transID"] ?>&agnetID=<?=$_SESSION["senderAgentID"]?>&transType=<?=($_REQUEST["transType"]!=""?$_REQUEST["transType"]:$_SESSION["transType"])?>', 'editCust', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" class="style3">
					<? } ?>
						Edit <?=__("Sender")?></A>
					<?
				} else {  ?>
					&nbsp;
				<?	}  ?>
            	</TD>
		<? } ?>
            </TR>
       		<?	}  ?>
			
			<?
				/**
				 * View sender documents at create transaction
				 * @Ticket #3779
				 */
				if(CONFIG_VIEW_SENDER_DOCS_AT_ADD_TRANSACTION == 1)
				{
 	            ?>
 	            <tr>
 	                <td colspan="3">&nbsp;</td>
 	                <td align="center" bgcolor="#c0c0c0">
 	                    <a href="#" onClick="javascript:window.open('viewDocument.php?customerID=<?=$customerContent["customerID"]?>', 'ViewDocuments', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" class="style3">
 	                        View Documents
 	                    </a>
 	                </td>
 	            </tr>
 	            <?
 	                }
 	            ?>
			
			<? if(CONFIG_REMARKS_ABOUT_SENDER == "1") { ?>
				<? if(!empty($customerContent["remarks"])) { ?>
				<tr>
				<td align="center" bgcolor="#c0c0c0" colspan="4">
					<?=$customerContent["remarks"]?>
				</td>
				</tr>
				<? 
				} 
				
				if(!empty($customerContent["senderRemarks"])) 
				{ 
				?>
				
				<tr>
				<td align="center" bgcolor="#ffffff" colspan="4">
					<?=stripslashes($customerContent["senderRemarks"])?>
				</td>
			</tr>
			<? 
					}
				} 
			?>


<?
}
?>
          </TABLE>
</FIELDSET>
                  </TD>
                </TR>
				<TR>
					
                  <TD> <FIELDSET><LEGEND CLASS="style2">Select Beneficiary </LEGEND>
                    <? if($_SESSION["customerID"]!="")
						{
							/**
							 * cleaning the distributor session var, that is,
							 * For Now transfer, is can run its pricess to find the default distributor
							 * when a new beneficiary is selected
							 * @Ticket# 3593
							 */
							if(CONFIG_DEFAULT_DISTRIBUTOR == "1" && !empty($_GET["benID"]) && !empty($_GET["transID"]))
							{
								unset($_SESSION["distribut"]);
								
							}

							$benTableName = TBL_BENEFICIARY;
							$IBANField = ", IBAN";
							if ( strtoupper($_SESSION["transactionCreatedBy"]) == "CUSTOMER" )
							{
								$benTableName = "cm_beneficiary";
								$IBANField = "";
							}
							$enableDisbleBenFlag = false;
							$StatusField = "";
							 if(defined("CONFIG_ENABLE_DISABLE_BENS") && (strstr(CONFIG_ENABLE_DISABLE_BENS, $agentType) || CONFIG_ENABLE_DISABLE_BENS=="1") && 
								defined("CONFIG_SHOW_ENABLE_DISABLE_BENS") && (strstr(CONFIG_SHOW_ENABLE_DISABLE_BENS, $agentType) || CONFIG_SHOW_ENABLE_DISABLE_BENS=="1")
							 ){
									$StatusField = ", status ";
									$enableDisbleBenFlag = true;
							 }
						 	$transTypeField = "";
							 if(defined("CONFIG_COUNTRY_BASED_ROUTINE_TRANSACTION") && CONFIG_COUNTRY_BASED_ROUTINE_TRANSACTION=="1"){
							 	$serviceTypeField = ", serviceType " ;
							 }
							 
if(CONFIG_BENEFICIARY_AS_COMPANY == "1"){
	$extraFieldsBen = ",company_msb_number,ben_type";
}


							$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip, Country, Phone, Mobile, Email, IDType,otherId_name ".$StatusField."" . $IBANField . "" . $serviceTypeField . " ".$extraFieldsBen." from " . $benTableName . " where customerID ='$_SESSION[customerID]'";
							 if($enableDisbleBenFlag){
							 	if(CONFIG_SHOW_ENABLE_DISABLE_BENS_COLORED!="1"){
									$queryBen .=" and status ='ENABLED'";
								}
							}

							$queryBen .=" order by firstName";
							$benificiaryContent2 = selectMultiRecords($queryBen);
							//debug($queryBen);
							if(count($benificiaryContent2) > 0 || CONFIG_TRANS_WITHOUT_BENEFICIARY == '1'){?>
							<TABLE WIDTH="100%" BORDER="0" CELLPADDING="2" CELLSPACING="0" BORDERCOLOR="#006600">
							<?php
							}
							if(count($benificiaryContent2) > 0)
							{?>
                      <?
					  	/**
						 * Displaying the bank details of Ben
						 * Ticket# 3321
						 */
						 if(CONFIG_BEN_BANK_DETAILS == "1")
						 {
					  ?>
							<TR CLASS="style2"> 
								<TD WIDTH="15%">Beneficairy Name</TD>
								<TD WIDTH="10%">Bank</TD>
								<TD WIDTH="20%">Bank Address</TD>
								<TD WIDTH="15%">Branch Name/Code</TD>
								<TD WIDTH="10%">Account Number</TD>	
								<TD WIDTH="10%">Account Type</TD>	
								<TD WIDTH="10%">&nbsp;</TD>
								<TD WIDTH="8%"></TD>
							</TR>
					  <?
					  	 }
						 else
						 {
					  ?>
					  <TR CLASS="style2"> 
                        <TD WIDTH="30%">Beneficairy Name</TD>
                        <TD WIDTH="13%">Country</TD>
                        <TD WIDTH="13%">Phone</TD>
					    <?	
						if (CONFIG_SHOW_BEN_MOBILE=="1")
						{
				    	?>
						<td width="13%">Mobile</td>
						<?
						}
						?>
						
                        <?
                        if(CONFIG_SHOW_BEN_ID == '1')
                        {
                        ?>
                        	<TD WIDTH="12%">ID Name</TD>
                        	<TD WIDTH="12%">Other ID Name</TD>	
                        <?
                        }
                        ?>
                        <TD WIDTH="12%">&nbsp;</TD>
                        <TD WIDTH="8%"></TR>
                      <? }
					  	/**
						 * Ening the else part of CONFIG_BEN_BANK_DETAILS
						 */
					  	}
					  	else
					  	{
					  		/**
					  		 * Removing the session values used in bank fields
					  		 */
					  		if(CONFIG_BEN_BANK_DETAILS == "1")
								{
									$_SESSION["bankName"] =	"";
									$_SESSION["accNo"] =	"";
									$_SESSION["branchCode"] =	"";
									$_SESSION["branchName"] =	"";
									$_SESSION["branchAddress"] =	"";
									$_SESSION["swiftCode"] =	"";
									$_SESSION["accountType"] =	"";
								}
					  	}
					  	
						
						 if(CONFIG_BEN_BANK_DETAILS == "1")
						 {
						 for($i=0;$i < count($benificiaryContent2); $i++)
							{
								$disabledColor = "";
								if($benificiaryContent2[$i]["status"]!="Enabled" && CONFIG_SHOW_ENABLE_DISABLE_BENS_COLORED=="1"){
									$disabledColor = "style='color:red;'";
								}
								if(CONFIG_BEN_BANK_DETAILS == "1")
								{
									$queryBenBanks = "select b.name, d.branchAddress,d.branchCode,d.accountNo,d.accountType  from ". TBL_BEN_BANK_DETAILS ." as d, ".TBL_BANKS ." as b where d.benId = ".$benificiaryContent2[$i]["benID"] ." and d.bankId=b.id";
									$benBanksData = selectFrom($queryBenBanks);
								}
								?>
                      <TR <?=$disabledColor?>> 
                        <TD><? echo $benificiaryContent2[$i]["firstName"]." ".$benificiaryContent2[$i]["lastName"];?></TD>
                        <TD><? echo $benBanksData["name"];?></TD>
                        <TD><? echo $benBanksData["branchAddress"];?></TD>
												<TD><? echo $benBanksData["branchCode"];?></TD>
												<TD><? echo $benBanksData["accountNo"];?></TD>
												<TD><? echo $benBanksData["accountType"];?></TD>
                         
                        <TD ALIGN="center">
							<? if($disabledColor!=""){?>
								N/A
							<? }else{?>
                        	<? if ($_SESSION["batch"]!= "") { ?>
                        	<A HREF="#" onClick="document.addTrans.action='add-transaction.php?val=' + document.frmAmount.amount_transactions.value + '&msg=1&benID=<? echo $benificiaryContent2[$i]["benID"]?>&transID=<? echo $_GET["transID"]?>&focusID=<?=$focusID?>';document.addTrans.submit();" CLASS="style2">
                        	<? }else{ ?>
                        	<a href="#" onClick="document.addTrans.action='add-transaction.php?msg=1&benID=<? echo $benificiaryContent2[$i]["benID"]?>&transID=<? echo $_GET["transID"]?>&focusID=<?=$focusID?>';document.addTrans.submit();" class="style2">
                        	<? } ?>
                        		Select</A>
                        	<? } ?>
								</TD>

                     	<TD ALIGN="center">
                	<?
					if ( !defined("CONFIG_DISABLE_EDIT_SENDER") || CONFIG_DISABLE_EDIT_SENDER == 0 || strstr( CONFIG_USERS_ALLOWED_TO_EDIT_BENEFICIARY, $agentType ) !== false )
					{
						if ($_SESSION["batch"]!= "") { ?>
						<A HREF="#" onClick="javascript:window.open('add-beneficiary.php?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&benID=<? echo $benificiaryContent2[$i]["benID"]?>&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>&transType=<? echo ($_REQUEST["transType"] !="" ? $_REQUEST["transType"] : $_SESSION["transType"])?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" CLASS="style2">
                     		<? }else{
                     					if(CONFIG_REMOVE_QUICK_LINK != "1"){ ?>
                     		<a href="#" onClick="javascript:window.open('add-beneficiary.php?from=popUp&benID=<? echo $benificiaryContent2[$i]["benID"]?>&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>&transType=<? echo ($_REQUEST["transType"] !="" ? $_REQUEST["transType"] : $_SESSION["transType"])?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" class="style2">
                     		<? }else{?>
                     		<a href="#" onClick="javascript:window.open('add-beneficiary-quick.php?from=popUp&benID=<? echo $benificiaryContent2[$i]["benID"]?>&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>&transType=<? echo ($_REQUEST["transType"] !="" ? $_REQUEST["transType"] : $_SESSION["transType"])?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" class="style2">
                     		
                     <?	} }?>
                     			Edit</A>
                 	<?	}  ?>
                     			</TD>

					  </TR>
                    
                    <? 
							}
						 
						 }
						 else
						 {
							for($i=0;$i < count($benificiaryContent2); $i++)
							{
								$disabledColor = "";
								if($benificiaryContent2[$i]["status"]!="Enabled" && CONFIG_SHOW_ENABLE_DISABLE_BENS_COLORED=="1"){
									$disabledColor = "style='color:red;'";
								}
							?>
                      <tr <?=$disabledColor?>> 
                        <td><? echo $benificiaryContent2[$i]["firstName"]." ".$benificiaryContent2[$i]["lastName"];?></td>
                        <td><? echo $benificiaryContent2[$i]["Country"];?></td>
						
                        <td>
							<?
								$phoneNumber = $benificiaryContent2[$i]["Phone"];
								$phoneType = "Home";

								//Added by Usman Ghani aginst ticket #3472: Now Transfer - Phone/Mobile								
								if ( CONFIG_PHONES_AS_DROPDOWN == 1
									&& empty( $phoneNumber ) )
								{
									$phoneNumber = $benificiaryContent2[$i]["Mobile"];
									$phoneType = "Mobile";
								}
								
								// End of code aginst ticket #3472: Now Transfer - Phone/Mobile

								echo $phoneNumber;
								if(SYSTEM != "familyexpress")
									echo " (".$phoneType.")";
							?>
						</TD>
						<?	
						if (CONFIG_SHOW_BEN_MOBILE=="1")
						{
				    	?>
						<td><?=$benificiaryContent2[$i]["Mobile"]?></td>
						<?
						}
						?>
                         <?
                        if(CONFIG_SHOW_BEN_ID == '1')
                        {
                        ?>
                        	<TD><? echo $benificiaryContent2[$i]["IDType"];?></TD>
                        	<TD><? echo $benificiaryContent2[$i]["otherId_name"];?></TD>
                        <?
                        }
                        ?>
                        <TD ALIGN="center">
							<? if($disabledColor!=""){?>
								N/A
							<? }else{?>
                        	<? if ($_SESSION["batch"]!= "") { ?>
                        	<A HREF="#" onClick="document.addTrans.action='add-transaction.php?val=' + document.frmAmount.amount_transactions.value + '&msg=1&benID=<? echo $benificiaryContent2[$i]["benID"]?>&transID=<? echo $_GET["transID"]?>&focusID=<?=$focusID?>';document.addTrans.submit();" CLASS="style2">
                        	<? }else{ ?>
                        	<a href="#" onClick="document.addTrans.action='add-transaction.php?msg=1&benID=<? echo $benificiaryContent2[$i]["benID"]?>&transID=<? echo $_GET["transID"]?>&focusID=<?=$focusID?>';document.addTrans.submit();" class="style2">
                        	<? } ?>
                        		Select</A>
							<? }?>
								</TD>

                     	<TD ALIGN="center">
                	<?
					if ( !defined("CONFIG_DISABLE_EDIT_SENDER") || CONFIG_DISABLE_EDIT_SENDER == 0 || strstr( CONFIG_USERS_ALLOWED_TO_EDIT_BENEFICIARY, $agentType ) !== false )
					{
						if ($_SESSION["batch"]!= "") { ?>
						<A HREF="#" onClick="javascript:window.open('add-beneficiary.php?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&benID=<? echo $benificiaryContent2[$i]["benID"]?>&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>&transType=<? echo ($_REQUEST["transType"] !="" ? $_REQUEST["transType"] : $_SESSION["transType"])?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" CLASS="style2">
                     		<? }else{
                     				if(CONFIG_REMOVE_QUICK_LINK != "1"){
									 	if($benificiaryContent2[$i]["ben_type"] == "company")
								 		{
											$beneficiaryPage = "add-ben-company.php";
										}else{
											$beneficiaryPage = "add-beneficiary.php";
										}		
							 ?>
                     		<a href="#" onClick="javascript:window.open('<?=$beneficiaryPage?>?from=popUp&benID=<? echo $benificiaryContent2[$i]["benID"]?>&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>&transType=<? echo ($_REQUEST["transType"] !="" ? $_REQUEST["transType"] : $_SESSION["transType"])?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" class="style2">
                     		<? }else{?>
                     		<a href="#" onClick="javascript:window.open('add-beneficiary-quick.php?from=popUp&benID=<? echo $benificiaryContent2[$i]["benID"]?>&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>&transType=<? echo ($_REQUEST["transType"] !="" ? $_REQUEST["transType"] : $_SESSION["transType"])?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" class="style2">
                     		
                     <?	} }?>
                     			Edit</A>
                 	<?	}  ?>
                     			</TD>

					  </TR>
                    
                    <? 
							}
						}
					}
					?>
					</TABLE>
                  </FIELDSET></TD>
				</TR>
				<tr>
                  <td>
                  	<fieldset>
                      <LEGEND CLASS="style2">Beneficiary Details </LEGEND>
                   
                  
					<TABLE WIDTH="699" BORDER="0" CELLPADDING="2" CELLSPACING="0">
           <TR>
					 <TD COLSPAN="4"><BR></TD>
					 </TR>
					 <? 
					 /**
					  * Move beneficiary field up to have the country field for the search of the bank
					  * @Ticket# 3546 
					  */
					 
						$benTableName = TBL_BENEFICIARY;
						
						if(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER == "1" && $_SESSION["transType"] == "Bank Transfer")
						{
							$IBANField = ", IBAN";
							$IbanOrBankField = ", IbanOrBank";
						}


						if ( strtoupper($_SESSION["transactionCreatedBy"]) == "CUSTOMER" )
						{
							$benTableName = "cm_beneficiary";
							$IBANField = "";
							$IbanOrBankField = ", IbanOrBank";
						}
						 if(defined("CONFIG_COUNTRY_BASED_ROUTINE_TRANSACTION") && CONFIG_COUNTRY_BASED_ROUTINE_TRANSACTION=="1"){
							$serviceTypeField = ", serviceType " ;
						 }
						 $benSOFFlag = false;
						 $benSOFField = "";
						if(defined("CONFIG_BEN_SOF_TYPE_COMP_TRANSACTION_TYPES") && strstr(strtoupper(CONFIG_BEN_SOF_TYPE_COMP_TRANSACTION_TYPES),strtoupper($_SESSION["transType"])) && $_SESSION["transType"]!=""){
							 $benSOFFlag = true;
							 $benSOFField = ", SOF ";
						 }
						$cpfFlag = false;
						$cpfField = "";
						if(defined("CONFIG_DISPLAY_CPF_TRANS_STATUS") && CONFIG_DISPLAY_CPF_TRANS_STATUS=="1"){
							$cpfFlag = true;
						}
						if($cpfFlag){
							$cpfField = ", CPF,userType";
						}
						//debug("Before");
						
if(CONFIG_BENEFICIARY_AS_COMPANY == "1"){
	$extraFieldsBen = ",company_msb_number,ben_type";
}

						 $queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Mobile, Email, dob,IDType,otherId,otherId_name" . $IBANField . $IbanOrBankField. $serviceTypeField .$benSOFField. $cpfField ." ".$extraFieldsBen."  from ".$benTableName." where benID ='$_SESSION[benID]' and customerID ='$_SESSION[customerID]'";
						 $benificiaryContent = selectFrom($queryBen);
						 //debug($_SESSION["transType"]);
						 //debug( $benificiaryContent );
						 //debug("After");
					 
					 if($_SESSION["customerID"]!="")
						{
						?>
					  <TR>
                        <TD WIDTH="146" ALIGN="right"><FONT COLOR="#005b90">&nbsp; </FONT></TD>
                        <TD COLSPAN="2">    <INPUT NAME="benID" TYPE="hidden" ID="benID" VALUE="<? echo $_SESSION["benID"]?>">
                        </TD>
				<?
					if ( strtoupper($_SESSION["transactionCreatedBy"]) != "CUSTOMER" )
					{
						?>
                        <TD WIDTH="245" HEIGHT="25" ALIGN="center" BGCOLOR="#c0c0c0" CLASS="style1">
                        	 <? if(CONFIG_REMOVE_QUICK_LINK != 1){?>
                        	<? if ($_SESSION["batch"]!= "") { ?>
                        	<A HREF="javascript:;" CLASS="style1" onClick=" window.open('add-beneficiary.php?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&agentID=<?=$_SESSION["senderAgentID"]."&customerID=".$_SESSION["customerID"]; ?>&transID=<? echo $_GET["transID"]?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=380,width=740')">
                        	<? }else{ ?>
                        	<a href="javascript:;" class="style1" onClick=" window.open('add-beneficiary.php?from=popUp&agentID=<?=$_SESSION["senderAgentID"]."&customerID=".$_SESSION["customerID"]; ?>&transID=<? echo $_GET["transID"] ?>&docWaiver=<?php echo $_SESSION["docWaiver"]  ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=380,width=740')">
												<? } ?>
                        		<B>Add 
                          New Beneficiary</B></A><? }?>
                         
                          <? if ($_SESSION["batch"]!= "") { ?>
                          <A HREF="javascript:;" CLASS="style3" onClick=" window.open('add-beneficiary-quick.php?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&agentID=<?=$_SESSION["senderAgentID"]."&customerID=".$_SESSION["customerID"]; ?>&transID=<? echo $_GET["transID"]?> &docWaiver=<?php echo $_SESSION["docWaiver"] ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')"> 
                          <? }else{ ?>
                          
						<? 
						if(CONFIG_REMOVE_QUICK_BENEFICIARY_LINK == "1"){ ?>
						<?	}else{?>
							  <a href="javascript:;" class="style3" onClick=" window.open('add-beneficiary-quick.php?from=popUp&agentID=<?=$_SESSION["senderAgentID"]."&customerID=".$_SESSION["customerID"]; ?>&transID=<? echo $_GET["transID"] ?> &docWaiver=<?php echo $_SESSION["docWaiver"] ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
							  <?
							  if(CONFIG_REMOVE_QUICK_LINK != 1){
							  ?>
							  /
							  <?
							  }
							  ?>
							  Add New Quick Beneficiary</A>
							  <?
							}
						}?>
						<? if(CONFIG_BENEFICIARY_AS_COMPANY == "1"){ ?>
						<a href="javascript:;" class="style1" onClick=" window.open('add-ben-company.php?from=popUp&agentID=<?=$_SESSION["senderAgentID"]."&customerID=".$_SESSION["customerID"]; ?>&transID=<? echo $_GET["transID"]?> &docWaiver=<?php echo $_SESSION["docWaiver"] ?>','Add Beneficiary As Company', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=380,width=740')">
						
						 /
						 <strong>Add Beneficiary As Company</strong></A>
						<? } ?>
						
						</TD>
				<? } ?>
                      </TR>
         <? if(CONFIG_BEN_BANK_DETAILS == "1") {?>
                      <TR>
								 				<TD WIDTH="100%" ALIGN="center" COLSPAN="4" VALIGN="middle">
	                      	<BR /><BR />
								 					<FONT COLOR="#005b90">Bank Name</FONT>
								 					<INPUT TYPE="text" SIZE="10" ID="searchBankName" NAME="searchBankName"/>
								 					<!--&nbsp;&nbsp;
								          <font color="#005b90">Swift Code</font>
								          <input type="text" size="10" id="searchSwiftCode" name="searchSwiftCode"/ -->
								          <INPUT TYPE="button" onClick=" if(document.addTrans.searchBankName.value == '' || IsAllSpaces(document.addTrans.searchBankName.value)) { alert ('Please provide bank name to search.') } else { window.open('search-bank.php?search=Y&transID=<?=$_GET["transID"]?>'+'&bankName='+ document.addTrans.searchBankName.value +'&benCountry=<?=$benificiaryContent["Country"]?>&swiftSearch=Y&moneyPaid=<?=$_SESSION["moneyPaid"]?>', 'searchBank', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') }" VALUE="Search" NAME="Submit"/>
													&nbsp;&nbsp;&nbsp;<SPAN STYLE="background-color:#c0c0c0" CLASS="style1">
												 	<A onClick=" window.open('add-bank.php?from=popUp&transID=<?=$_GET["transID"]?>','', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" HREF="javascript:;">
								          	<B>Add New Bank</B></A>
								          </SPAN>
								        </TD>
								      </TR>
    							<? } ?>
								
					<? 
					/**
					 This Config added by Niaz Ahmad at 17-04-2009 @4720 for Muthoot Client
					*/
					if(CONFIG_ADD_BEN_BANK_ON_CREATE_TRANSACTION == "1" && $_SESSION["transType"] == "Bank Transfer") { 
					
					?>
						
						 
	 	       
				<SCRIPT>
					function searchBenBank()
					{
						var bankName = document.getElementById("bankName").value;
						var searchType = document.getElementById("searchType").value;
						if(bankName == '' || IsAllSpaces(bankName))
						{ 
							alert ('Please provide search string.');
							document.getElementById("bankName").focus;
						} 
						else 
						{ 
							window.open('search-bank.php?from=add-transaction.php&search=Y&transID=<?=$_GET["transID"]?>'+'&bankName='+ bankName +'&searchType='+ searchType +'&benCountry=<?=$benificiaryContent["Country"]?>', 'searchBenBank', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') 
						}
					}
				</script>
	 	        	<tr> 
						<td height="20" align="center" colspan="4">
							<br />
							<font color="#005b90">Search Bank</font>
							<input type="text" size="15" id="bankName" name="bankName" value="<?=$_REQUEST["bankName"]?>" />

							&nbsp;&nbsp;

							<font color="#005b90">Search By</font>
							<select name="searchType" id="searchType">
								<?
									$typeLable[] = "Bank Name";
									$typeLable[] = "Branch Name";
									$typeLable[] = "State";
											
									$typeValue[] = "BANK_NAME";
									$typeValue[] = "BRANCH_NAME";
									$typeValue[] = "STATE_NAME";
									
									for($i=0; $i<sizeof($typeLable); $i++)
									{
										if($typeValue[$i] == $searchType)
										{
											echo "<option value=\"".$typeValue[$i]."\" selected>".$typeLable[$i]."</option>";
										} else {
											echo "<option value=\"".$typeValue[$i]."\">".$typeLable[$i]."</option>";
										}
									}
								?>
							</select>
							<input type="button" onClick="searchBenBank();" value="Search" name="Submit"/>

							
							<SPAN STYLE="background-color:#c0c0c0" CLASS="style1">   
								<A onClick=" window.open('add-bank.php?from=popUp&transID=<?=$_GET["transID"]?>&moneyPaid=<?=$_SESSION["moneyPaid"]?>&custCountry=<?=$_REQUEST["custCountry"]?>','', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" HREF="javascript:;"> 
									<B>Add New Bank</B>
								</A> 
							</SPAN> 
							<BR />
	 	                 </TD>
	 	        	</TR> 
		        	<!--<TR> 
						<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><?=__("Sender");?> Bank</FONT><FONT COLOR="#ff0000">*</FONT></TD> 
						<TD WIDTH="150" HEIGHT="20" ALIGN="left"> 
							<?
								if(!empty($_SESSION["senderBank"]))
									$senderBanks = selectFrom("SELECT bankId,name,branchAddress FROM ".TBL_BANKS." WHERE bankId = '".$_SESSION["senderBank"]."'"); 
							?>
							<INPUT NAME="senderBankDetails" TYPE="text" SIZE="35" VALUE="<?=!empty($_SESSION["senderBank"])?$senderBanks["name"]." , ".$senderBanks["branchAddress"]:"" ?>" readonly/>
							<INPUT TYPE="hidden" NAME="senderBank" VALUE="<?=$_SESSION["senderBank"]?>" />
	 	                 </TD> 
	 	                 <TD>&nbsp;</TD>
	 	        	</TR> -->
             	
			 	
						
					<? } ?>			

					  <? } ?>
      <? 
      /*
       * Move this code to upside
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email,otherId,otherId_name,IBAN  from ".TBL_BENEFICIARY." where benID ='$_SESSION[benID]' and customerID ='$_SESSION[customerID]'";
			//echo $queryBen;
			$benificiaryContent = selectFrom($queryBen);
			*/
			if($benificiaryContent["benID"] != "")
			{
				/**
				 * Getting the bank details of the beneficiary
				 * @Ticket# 3321
				 *
				 * If the transfer type is bank transfer than get the beneficiary bank details
				 * if details are available then use the session variables to place the bank details
				 * at the fields available
				 */
			if(CONFIG_BEN_BANK_DETAILS == "1" && $_SESSION["transType"] == "Bank Transfer"){
				/*
				if(!empty($_SESSION["ben_bank_id"]))
				{
					
					//**
					 //* If url contain any new request in term of beneficiary or search bank id
					 //* than fetch data from corresponding tables
					 //* else retain the user defined values at create transaction
					 ///
					if(!empty($_GET["ben_bank_id"]) || !empty($_GET["benID"]))
					{
						//**
						 //* Give first priority to search bank data
						 //* and than remove this as only once it can be used
						 ///
						if(!empty($_SESSION["ben_bank_id"]))
						{
							
							$benBankDetailViewQry = "select bankId, name from ".TBL_BANKS." where id = ".$_SESSION["ben_bank_id"];
							$benBankDetailViewData = selectFrom($benBankDetailViewQry);
							unset($_SESSION["ben_bank_id"]);
							
							$_SESSION["bankId"] = $benBankDetailViewData["bankId"];
							$_SESSION["bankName"] =	$benBankDetailViewData["name"];
							
							
							//$benBankDetailViewQry = "select d.accountNo, d.branchCode, d.branchAddress, d.swiftCode, d.accountType from ".TBL_BEN_BANK_DETAILS." as d, ". TBL_BANKS ." as b where d.benId = ".$benificiaryContent["benID"]." and d.bankId=b.id";
							//$benBankDetailViewData = selectFrom($benBankDetailViewQry);
							
							//$_SESSION["bankName"] =	$benBankDetailViewData["name"];
							unset($_SESSION["accNo"]);// =	$benBankDetailViewData["accountNo"];
							unset($_SESSION["branchCode"]);// =	$benBankDetailViewData["branchCode"];
							unset($_SESSION["branchAddress"]);// =	$benBankDetailViewData["branchAddress"];
							unset($_SESSION["swiftCode"]);// =	$benBankDetailViewData["swiftCode"];
							unset($_SESSION["accountType"]);// =	$benBankDetailViewData["accountType"];
							
						}
						//**
						 //* Else fetch the beneficiary bank data
						 ///
						else
						{
							$benBankDetailViewQry = "select b.bankId, b.name, d.accountNo, d.branchCode, d.branchAddress, d.swiftCode, d.accountType from ".TBL_BEN_BANK_DETAILS." as d, ". TBL_BANKS ." as b where d.benId = ".$benificiaryContent["benID"]." and d.bankId=b.id";
							$benBankDetailViewData = selectFrom($benBankDetailViewQry);
							
							$_SESSION["bankId"] = $benBankDetailViewData["bankId"];
							$_SESSION["bankName"] =	$benBankDetailViewData["name"];
							$_SESSION["accNo"] =	$benBankDetailViewData["accountNo"];
							$_SESSION["branchCode"] =	$benBankDetailViewData["branchCode"];
							$_SESSION["branchAddress"] =	$benBankDetailViewData["branchAddress"];
							$_SESSION["swiftCode"] =	$benBankDetailViewData["swiftCode"];
							$_SESSION["accountType"] =	$benBankDetailViewData["accountType"];
						}					
						
					}	
				}
				else
				{
						$benBankDetailViewQry = "select * from ".TBL_BEN_BANK_DETAILS." as d, ". TBL_BANKS ." as b where d.benId = ".$benificiaryContent["benID"]." and d.bankId=b.id";
				}
				*/
				
				
				/**
				 * If url contain any new request in term of beneficiary or search bank id
				 * than fetch data from corresponding tables
				 * else retain the user defined values at create transaction
				 */
				$benID = $benificiaryContent["benID"];
				/**
				 * Give first priority to search bank data
				 */	
				$extraBenBankFields = "";
				if(CONFIG_IBAN_BEN_BANK_DETAILS == "1"){
					$extraBenBankFields = ", d.IBAN";							
				}				
				if(!empty($_GET["ben_bank_id"]) || !empty($_GET["benID"]))
				{

					/**
					 * Give first priority to search bank data
					 * and than remove this as only once it can be used
					 */
					 /* #5027
					 	This condition made under $_GET["notEndSessions"] as this value comes from selecting sender Bank.
						And then it was also going in this block which was creating problem for bank valus of Beneficiary Bank
						above.
						by Aslam Shahid
					 */
					if($_GET["notEndSessions"]!="not"){
						if(!empty($_SESSION["ben_bank_id"]))
						{
							//$benBankDetailViewQry = "select * from ".TBL_BEN_BANK_DETAILS." as d, ". TBL_BANKS ." as b where b.id = ".$_SESSION["ben_bank_id"]." and d.bankId=b.id";
							$benBankDetailViewQry = "select name from ".TBL_BANKS." where id = ".$_SESSION["ben_bank_id"]; 
							//$benBankDetailViewQry = "select * from ".TBL_BANKS." where id = ".$_SESSION["ben_bank_id"];
							$benBankDetailViewData = selectFrom($benBankDetailViewQry);
							//debug($benBankDetailViewData);
							$_SESSION["bankName"] = $benBankDetailViewData["name"];
							unset($_SESSION["ben_bank_id"]);
							
							$benBankDetailViewQry = "select d.accountNo, d.branchCode, d.branchAddress, d.swiftCode, d.accountType".$extraBenBankFields." from ".TBL_BEN_BANK_DETAILS." as d, ". TBL_BANKS ." as b where d.benId = ".$benificiaryContent["benID"]." and d.bankId=b.id";
							$benBankDetailViewData = selectFrom($benBankDetailViewQry);
						
							unset($_SESSION["accNo"]);// =  $benBankDetailViewData["accountNo"]; 
							unset($_SESSION["branchCode"]);// = $benBankDetailViewData["branchCode"]; 
							unset($_SESSION["branchAddress"]);// =  $benBankDetailViewData["branchAddress"]; 
							unset($_SESSION["swiftCode"]);// =  $benBankDetailViewData["swiftCode"]; 
							unset($_SESSION["accountType"]);// =    $benBankDetailViewData["accountType"];
						}
					   /** 
					   * Else fetch the beneficiary bank data 
						*/ 
						else 
						{ 
							$benBankDetailViewQry = "select b.name, d.accountNo, d.branchCode, d.branchAddress, d.swiftCode, d.accountType".$extraBenBankFields." from ".TBL_BEN_BANK_DETAILS." as d, ". TBL_BANKS ." as b where d.benId = ".$benificiaryContent["benID"]." and d.bankId=b.id"; 
							$benBankDetailViewData = selectFrom($benBankDetailViewQry); 
							 
							$_SESSION["bankName"] = $benBankDetailViewData["name"]; 
							$_SESSION["accNo"] =    $benBankDetailViewData["accountNo"]; 
							$_SESSION["branchCode"] =   $benBankDetailViewData["branchCode"]; 
							$_SESSION["branchAddress"] =    $benBankDetailViewData["branchAddress"]; 
							$_SESSION["swiftCode"] =    $benBankDetailViewData["swiftCode"]; 
							$_SESSION["accountType"] =  $benBankDetailViewData["accountType"]; 
							if($benBankDetailViewData["IBAN"]!="")
								$_SESSION["IBAN"] =  $benBankDetailViewData["IBAN"]; 
						}
					}
				}
			}
			
			/**
			  This Config added by Niaz Ahmad at 17-04-2009 @4720 for Muthoot Client
			*/
			if(CONFIG_ADD_BEN_BANK_ON_CREATE_TRANSACTION == "1" && $_SESSION["transType"] == "Bank Transfer"){
				/**
				 * If url contain any new request in term of beneficiary or search bank id
				 * than fetch data from corresponding tables
				 * else retain the user defined values at create transaction
				 */
				$benID = $benificiaryContent["benID"];
				/**
				 * Give first priority to search bank data
				 */					
				if(!empty($_GET["ben_bank_id"]) || !empty($_GET["benID"]))
				{
					/**
					 * Give first priority to search bank data
					 * and than remove this as only once it can be used
					 */
					if(!empty($_SESSION["ben_bank_id"]))
					{
						$benBankDetailViewQry = "select name,branchName,branchAddress,branchCode from ".TBL_BANKS." where id = ".$_SESSION["ben_bank_id"]; 
						$benBankDetailViewData = selectFrom($benBankDetailViewQry);
						
						$_SESSION["bankName"] = trim($benBankDetailViewData["name"]);
						$_SESSION["branchAddress"] = trim($benBankDetailViewData["branchAddress"]);
						$_SESSION["branchName"] = trim($benBankDetailViewData["branchName"]);
						unset($_SESSION["ben_bank_id"]);
						unset($_SESSION["accNo"]);// =  $benBankDetailViewData["accountNo"]; 
						unset($_SESSION["swiftCode"]);// =  $benBankDetailViewData["swiftCode"]; 
						unset($_SESSION["accountType"]);// =    $benBankDetailViewData["accountType"]; 
					}
				   /** 
				   * Else fetch the beneficiary bank data 
					*/ 
					else 
					{ 
						$benBankDetailViewQry = "select b.name,b.branchName, d.accountNo, d.branchCode, d.branchAddress, d.swiftCode, d.accountType".$extraBenBankFields." from ".TBL_BEN_BANK_DETAILS." as d, ". TBL_BANKS ." as b where d.benId = ".$benificiaryContent["benID"]." and d.bankId=b.id"; 
						$benBankDetailViewData = selectFrom($benBankDetailViewQry); 
						$_SESSION["accNo"] =    $benBankDetailViewData["accountNo"]; 
						$_SESSION["bankName"] = $benBankDetailViewData["name"]; 
						$_SESSION["branchCode"] =   $benBankDetailViewData["branchCode"]; 
						$_SESSION["branchName"] =   $benBankDetailViewData["branchName"];
						$_SESSION["branchAddress"] =    $benBankDetailViewData["branchAddress"]; 
						$_SESSION["swiftCode"] =    $benBankDetailViewData["swiftCode"]; 
						$_SESSION["accountType"] =  $benBankDetailViewData["accountType"]; 
						if($benBankDetailViewData["IBAN"]!="")
							$_SESSION["IBAN"] =  $benBankDetailViewData["IBAN"]; 
						if($benBankDetailViewData["name"]==""){
							$benBankDetailViewQry = "select b.name,b.branchName, d.accNo, d.branchCode, d.branchAddress, d.swiftCode, d.accountType".$extraBenBankFields." from ".TBL_BANK_DETAILS." as d, ". TBL_BANKS ." as b where d.benId = ".$benificiaryContent["benID"]." and d.bankId=b.id order by d.bankID desc"; 
							$benBankDetailViewData = selectFrom($benBankDetailViewQry); 
							$_SESSION["accNo"] =    $benBankDetailViewData["accNo"]; 														
							$_SESSION["bankName"] = $benBankDetailViewData["name"]; 
							$_SESSION["branchCode"] =   $benBankDetailViewData["branchCode"]; 
							$_SESSION["branchName"] =   $benBankDetailViewData["branchName"];
							$_SESSION["branchAddress"] =    $benBankDetailViewData["branchAddress"]; 
							$_SESSION["swiftCode"] =    $benBankDetailViewData["swiftCode"]; 
							$_SESSION["accountType"] =  $benBankDetailViewData["accountType"]; 
							if($benBankDetailViewData["IBAN"]!="")
								$_SESSION["IBAN"] =  $benBankDetailViewData["IBAN"]; 
						}
					}
	
				}
			}
			
			if(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER == "1" && $_SESSION["transType"] == "Bank Transfer") {
				/**
				 * If url contain any new request in term of beneficiary or search bank id
				 * than fetch data from corresponding tables
				 * else retain the user defined values at create transaction
				 */
				$benID = ($_GET['benID'] != "" ? $_GET['benID'] : $benificiaryContent["benID"]);
				// To make sure that this code executes when transaction was not created by CUSTOMER.
				// as Bank details are already retrieved for CUSOTMER. This code should not be executed when CUSTOMER Transaction is edited.
				// Also this code is executed when transaction is Created for admin module. And beneficiary is selected or added during creation.
				if ( strtoupper($_SESSION["transactionCreatedBy"]) != "CUSTOMER" ){
					if($benificiaryContent["IbanOrBank"] =="iban"){
						$_SESSION['IBAN'] = $benificiaryContent["IBAN"];
					}
					elseif($benificiaryContent["IbanOrBank"] =="bank"){
						$benBankDetailViewQry = "select b.name, d.accountNo, d.branchCode, d.branchAddress, d.swiftCode, d.accountType".$extraBenBankFields." from ".TBL_BEN_BANK_DETAILS." as d, ". TBL_BANKS ." as b where d.benId = ".$benID." and d.bankId=b.id";
						$benBankDetailViewData = selectFrom($benBankDetailViewQry);
						$_SESSION["bankName"] 		=	$benBankDetailViewData["name"];
						$_SESSION["accNo"] 			=	$benBankDetailViewData["accountNo"];
						$_SESSION["branchCode"] 	=	$benBankDetailViewData["branchCode"];
						$_SESSION["branchAddress"] 	=	$benBankDetailViewData["branchAddress"];
						$_SESSION["swiftCode"] 		=	$benBankDetailViewData["swiftCode"];
						$_SESSION["accountType"] 	=	$benBankDetailViewData["accountType"];
						if($benBankDetailViewData["IBAN"]!="")
							$_SESSION["IBAN"] =  $benBankDetailViewData["IBAN"]; 
					}
				}
			}
			
			if (CONFIG_MANUAL_FEE_RATE == "1")
			{
				$countryList = explode(",",CONFIG_BEN_COUNTRY);
				//debug($countryList);
				for ($i = 0; $i < count($countryList); $i++)
				{
					//debug(strtoupper($benificiaryContent["Country"])." == ".$countryList[$i]);
					if (strtoupper($benificiaryContent["Country"]) == $countryList[$i])
					{
						$flagBenCountry = 1;	
						//debug($benificiaryContent["Country"]);
					}	
				}
			}
				
				///to enable/disbale edit on amend for exchaneg rate and commission
				$readonlyFlag2 = "";
		if((CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")&& (CONFIG_EDIT_VALUES == "1" && $_GET["transID"] != ""))
		{ 
				$readonlyFlag2 =  "readonly" ;
				//debug($readonlyFlag2);

		}elseif(CONFIG_EDIT_VALUES == "1" && $_GET["transID"] != "")
		{
				$readonlyFlag2 = "readonly";
				//debug($readonlyFlag2);
		}elseif(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1"){
			
				$readonlyFlag2 = "";
				//debug($readonlyFlag2);
		}else{
			
				$readonlyFlag2 = "readonly";
				//debug($readonlyFlag2);
			
		} 
		
		?>
                      <TR>
                        <TD WIDTH="146" ALIGN="right"><FONT COLOR="#005b90"> Name</FONT></TD>
                        <TD COLSPAN="2"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></TD>
                        <TD WIDTH="245">&nbsp;</TD>
                      </TR>
                      <TR>
                        <TD WIDTH="146" ALIGN="right"><FONT COLOR="#005b90">Address</FONT></TD>
                        <!--td colspan="2"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td-->
                        <TD WIDTH="196"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></TD>
                        <TD WIDTH="146" ALIGN="right"><FONT COLOR="#005b90">City </FONT></TD>
                        <TD WIDTH="196"><? echo $benificiaryContent["City"]?></TD>
                      </TR>
                      <TR>
                        <TD WIDTH="146" ALIGN="right"><FONT COLOR="#005b90">Postal / Zip </FONT></TD>
                        <TD WIDTH="196"><? echo $benificiaryContent["Zip"]?></TD>
                        <TD WIDTH="96" ALIGN="right"><FONT COLOR="#005b90">Country</FONT></TD>
                        <TD WIDTH="245"><? echo $benificiaryContent["Country"]?>
                            <INPUT TYPE="hidden" NAME="benCountry" VALUE="<? echo $benificiaryContent["Country"]?>">
                        </TD>
                      </TR>
                      <TR>
                        <TD WIDTH="146" ALIGN="right"><FONT COLOR="#005b90">Phone</FONT></TD>
                        <TD WIDTH="196">
							<?
								if(CONFIG_PHONES_AS_DROPDOWN == 1)
								{
									if(empty($benificiaryContent["Phone"]))
									{
										echo $benificiaryContent["Mobile"]." (Mobile)";
									} else {
										echo $benificiaryContent["Phone"]." (Home)";
									}
								} else {
									echo $benificiaryContent["Phone"];
								}
							?>
						</TD>
                        <TD WIDTH="96" ALIGN="right"><FONT COLOR="#005b90">Email</FONT></TD>
                        <TD WIDTH="245"><? echo $benificiaryContent["Email"]?></TD>
                      </TR>
					  <? if(CONFIG_BENEFICIARY_AS_COMPANY == "1" && $benificiaryContent["ben_type"] == "company") {?>
					    <TR>
                        <TD WIDTH="146" ALIGN="right"><FONT COLOR="#005b90">MSB Number</FONT></TD>
                        <TD colspan="6"><? echo $benificiaryContent["company_msb_number"]?></TD>
                       
                      </TR>
					  <? } ?>
				<?	if($cpfFlag){ ?>
                      <TR>
                        <TD WIDTH="146" ALIGN="right"><FONT COLOR="#005b90">
						<?			
							if(strtoupper($benificiaryContent["Country"]) == "BRAZIL" && $cpfFlag)
							{
								if($benificiaryContent["userType"]=="Business User"){
									echo  "CNPJ Number ";
								}
								else{
									echo  "CPF Number ";
								}
							}
							?>
						
						</FONT></TD>
                        <TD WIDTH="196">
							<?
								if(strtoupper($benificiaryContent["Country"]) == "BRAZIL" && $cpfFlag)
								{
									echo  $benificiaryContent["CPF"];
								}
							?>
						</TD>
                        <TD colspan="2"></TD>
                      </TR>
			 	 <? }?>
                      <?
				}
				
			//}
  ?>
                    </TABLE>
                                   
					<? if(($benificiaryContent["benID"] != "" && !$countryBasedFlag) || (CONFIG_TRANS_WITHOUT_BENEFICIARY == '1' && $_SESSION['customerID']!=''))
					{
					
					$countryContent = selectFrom("select serviceAvailable, bankCharges, outCurrCharges  from " . TBL_SERVICE. ", ".TBL_COUNTRY." where 1 and (toCountryId = countryId and  countryName='".$benificiaryContent["Country"]."')");
					?>
					<TABLE WIDTH="650" BORDER="0" CELLPADDING="2" CELLSPACING="0">
                      <TR>
                        <TD COLSPAN="2"><SPAN CLASS="style6">Services available for <STRONG><? echo $benificiaryContent["Country"]?></STRONG>: <? echo ($countryContent["serviceAvailable"] != "" ? $countryContent["serviceAvailable"] : "None")?></SPAN></TD>
                      </TR>
                      <TR>
                        <TD WIDTH="150" ALIGN="right" VALIGN="top"><FONT COLOR="#005b90">Transaction Type</FONT><FONT COLOR="#ff0000">*</FONT></TD>
                        <TD WIDTH="500">
												<SELECT NAME="transType" ID="transType" STYLE="font-family:verdana; font-size: 11px" onChange="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&benID=<?=$benificiaryContent["benID"];?>&focusID=<?=$focusID?>'; document.addTrans.submit();">
                            <OPTION VALUE="">- Select Transaction Type -</OPTION>
                            <?
														if (strstr($countryContent["serviceAvailable"], "Cash Collection"))
														{
																if ($_SESSION["transType"] == "Pick up")
																		echo "<option selected value='Pick up'>Pick up</option>";
																else
																		echo "<option value='Pick up'>Pick up</option>";
														}
														if (strstr($countryContent["serviceAvailable"], "Bank Deposit") || CONFIG_TRANS_WITHOUT_BENEFICIARY=='1')
														{
								
																if ($_SESSION["transType"] == "Bank Transfer")
																{
																		if(CONFIG_CUSTOM_BANK_CHARGE == '1')
																		{
																				if(CONFIG_CUSTOM_BANK_CHARGE_CURRENCY == 'FROM')
																				{
																						$bankChargeAmount = $_SESSION["transAmount"];	
																				}
																				else
																				{
																						$bankChargeAmount = $_SESSION["localAmount"];	
																				}
																				$_SESSION["bankCharges"] = getBankCharges($customerContent["Country"], $benificiaryContent["Country"], $_SESSION["currencyFrom"], $_SESSION["currencyTo"], $bankChargeAmount, $dist);
																		}
																		else
																		{
																				$_SESSION["bankCharges"] = $countryContent["bankCharges"];
																		}
																		$Selected = "selected";
																		//echo "<option value='Bank Transfer' selected>Bank Transfer</option>";
									
																}
								
																		echo "<option value='Bank Transfer'($Selected : echo '$Selected'? echo '') >Bank Transfer</option>";
														}
							
														if(CONFIG_ATM_CARD_OPTION == "1")
														{
																if (strstr($countryContent["serviceAvailable"], "ATM Card"))
																{
																		if ($_SESSION["transType"] == "ATM Card")
																				echo "<option value='ATM Card' selected>ATM Card</option>";
																		else
																				echo "<option value='ATM Card'>ATM Card</option>";
																}
														}
							
														if (strstr($countryContent["serviceAvailable"], "Home Delivery"))
														{
																if ($_SESSION["transType"] == "Home Delivery")
																		echo "<option value='Home Delivery' selected>Home Delivery</option>";
																else
																		echo "<option value='Home Delivery'>Home Delivery</option>";
														}
						

													?><?  //echo $_SESSION["transType"] = "";?>
                        	</SELECT>
                        <? if($_POST['transType']== "Home Delivery"){
                                if($benificiaryContent["Address"]== "" && $benificiaryContent["Address1"] == ""){
                            ?>
                        <DIV ID="msgdiv" STYLE="visibility:visible; vertical-align:bottom;">
                            <FONT COLOR="#3366FF"><BR>Please Enter Address Detail by clicking Edit button on Select Beneficiary Section</FONT>
                        </DIV>
                        <?
                                }
                           }
                        ?>

                       <SCRIPT LANGUAGE="JavaScript">
					   		SelectOption(document.addTrans.transType,"<?=$_SESSION["transType"]; ?>");
						</SCRIPT>
<? 
	if(defined("CONFIG_BEN_IDTYPE_COMP_TRANSACTION_TYPES") && CONFIG_BEN_IDTYPE_COMP_TRANSACTION_TYPES!="0" && $_SESSION["transType"]!=""){
		$strSqlBenID = "select i.id_number from ".TBL_BENEFICIARY." as b,  user_id_types as i where benID='".$benificiaryContent["benID"]."' and user_id=benID";
		$strSqlBenIDData = selectFrom($strSqlBenID);
	
?>
		<input type="hidden" name="benIdTypeFlag" id="benIdTypeFlag" value="<?=$strSqlBenIDData["id_number"]?>">
	<SCRIPT LANGUAGE="JavaScript">
		checkBenIdCompulsory("<?=strstr(CONFIG_BEN_IDTYPE_COMP_TRANSACTION_TYPES,$_SESSION["transType"])?> ","<?=$benificiaryContent["benID"]?>","<?=$strSqlBenIDData["id_number"]?>");
	</SCRIPT>
<? 
}
 
	if($benSOFFlag){
?>
		<input type="hidden" name="benSOFFlag" id="benSOFFlag" value="<?=$benificiaryContent["SOF"]?>">
	<SCRIPT LANGUAGE="JavaScript">
		checkBenSOFCompulsory("<?=strstr(CONFIG_BEN_SOF_TYPE_COMP_TRANSACTION_TYPES,$_SESSION["transType"])?> ","<?=$benificiaryContent["benID"]?>","<?=$benificiaryContent["SOF"]?>");
	</SCRIPT>
<? 
}


/**
 * @Ticket# 3622 
 * If super admin is logged in and the config for subagent is ON than 
 * senderAgentId will be the selected one from the search sender, provided it is not null
 * else system will select the looged userd id as sender id
 */ 
if($agentType == "SUPA" && CONFIG_CREATE_TRANSACTION_FOR_SUB_AGENT == 1)
{
	if(!empty($senderAgentContent['userID']))
		$senderAgentID  = $senderAgentContent['userID'];
	else
		$senderAgentID  = $_SESSION["loggedUserData"]["userID"];
}
else
{
	if($agentType == 'SUPA' || $agentType == 'SUPAI' || $agentType == 'SUBA' || $agentType == 'SUBAI' ) 
	{
		$senderAgentID  = $_SESSION["loggedUserData"]["userID"];
	}
	else//if($agentType == 'admin')
	{
		$senderAgentID  = $senderAgentContent['userID'];
	}
}
if(!$hideAgentFlag){
	if(!empty($_SESSION["senderAgentID"])){
		$senderAgentID = $_SESSION["senderAgentID"]; // this comes after selecting sender from search sender
	}
}
?>

                        <INPUT TYPE="hidden" NAME="custAgentID" VALUE="<? echo $senderAgentID ; ?>"></TD>
				<?php if($useTransDetailsFlag){?>
                        <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Details</FONT><FONT COLOR="#ff0000">*</FONT></TD>
                        <TD WIDTH="500">
							<SELECT NAME="useTransDetails" ID="useTransDetails" STYLE="font-family:verdana; font-size: 11px" onChange="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&benID=<?=$benificiaryContent["benID"];?>&focusID=<?=$focusID?>'; document.addTrans.submit();">
								<OPTION VALUE="No">No</OPTION>
								<OPTION VALUE="Yes">Yes</OPTION>
                        	</SELECT>
							<SCRIPT LANGUAGE="JavaScript">
								SelectOption(document.addTrans.useTransDetails,"<?=$_SESSION["useTransDetails"]; ?>");
							</SCRIPT>
				<?php }?>
                      </TR>
                    </TABLE>
					 <? 
				}
				elseif($benificiaryContent["benID"] != "" && $countryBasedFlag){
					?>
					<TABLE WIDTH="650" BORDER="0" CELLPADDING="2" CELLSPACING="0">
                      <TR>
                        <TD WIDTH="150" ALIGN="right" VALIGN="top"><FONT COLOR="#005b90">Service Type</TD>
                        <TD WIDTH="500">
							<input type="text"  NAME="serviceType" ID="serviceType" value="<?=$_SESSION["serviceType"]?>">
							<input type="hidden"  NAME="transType" ID="transType" value="<?=$_SESSION["transType"]?>" readonly>
<? 
	if(defined("CONFIG_BEN_IDTYPE_COMP_TRANSACTION_TYPES") && CONFIG_BEN_IDTYPE_COMP_TRANSACTION_TYPES!="0" && $_SESSION["transType"]!=""){
		$strSqlBenID = "select i.id_number from ".TBL_BENEFICIARY." as b,  user_id_types as i where benID='".$benificiaryContent["benID"]."' and user_id=benID";
		$strSqlBenIDData = selectFrom($strSqlBenID);
	
?>
		<input type="hidden" name="benIdTypeFlag" id="benIdTypeFlag" value="<?=$strSqlBenIDData["id_number"]?>">
	<SCRIPT LANGUAGE="JavaScript">
		checkBenIdCompulsory("<?=strstr(CONFIG_BEN_IDTYPE_COMP_TRANSACTION_TYPES,$_SESSION["transType"])?> ","<?=$benificiaryContent["benID"]?>","<?=$strSqlBenIDData["id_number"]?>");
	</SCRIPT>
<? 
}
/**
 * @Ticket# 3622 
 * If super admin is logged in and the config for subagent is ON than 
 * senderAgentId will be the selected one from the search sender, provided it is not null
 * else system will select the looged userd id as sender id
 */ 
if($agentType == "SUPA" && CONFIG_CREATE_TRANSACTION_FOR_SUB_AGENT == 1)
{
	if(!empty($senderAgentContent['userID']))
		$senderAgentID  = $senderAgentContent['userID'];
	else
		$senderAgentID  = $_SESSION["loggedUserData"]["userID"];
}
else
{
	if($agentType == 'SUPA' || $agentType == 'SUPAI' || $agentType == 'SUBA' || $agentType == 'SUBAI' ) 
	{
		$senderAgentID  = $_SESSION["loggedUserData"]["userID"];
	}
	else//if($agentType == 'admin')
	{
		$senderAgentID  = $senderAgentContent['userID'];
	}
}
if(!$hideAgentFlag){
	if(!empty($_SESSION["senderAgentID"])){
		$senderAgentID = $_SESSION["senderAgentID"]; // this comes after selecting sender from search sender
	}
}

?>

                        <INPUT TYPE="hidden" NAME="custAgentID" VALUE="<? echo $senderAgentID ; ?>"></TD>
                      </TR>
                    </TABLE>
					 <? 
					 }
					if ($_SESSION["transType"] != "")
					{ 
					?>
                    <TABLE WIDTH="650" BORDER="0" CELLPADDING="2" CELLSPACING="0">
<?
if (strstr($countryContent["serviceAvailable"], "Cash Collection") && !$countryBasedFlag){
if ($_SESSION["transType"] == "Pick up")
{
?>						
							<?	if (CONFIG_COLLPOINTS_DROPDOWN != '1') {  ?>
                      <TR>
                        <TD COLSPAN="3"><BR>
                          Select correspondent - Please enter city <? if(CONFIG_CP_SEARCH_VIA_COUNTRY == '1'){?>/country<? }?> name to display 
                          <?=$company?> Transfer 's correspondents.<BR>
						</TD>
                      </TR>
				  		<?	} else {	echo "<br>";  }  ?>
                      <TR>
                        <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">
						<?
						if ($_SESSION["transType"] == "Pick up")
						{
							if (CONFIG_COLLPOINTS_DROPDOWN == '1') {
								echo "Select Collection Point";
							} else {
								echo "Search Collection Point";
							}
						}
						else
						{
							echo "Search correspondent";
						}
						?>
						<FONT COLOR="#ff0000">*</FONT>
						 </FONT></TD>
                        <TD WIDTH="300">
                   		<?	if (CONFIG_COLLPOINTS_DROPDOWN == '1') {  ?>
                   				<SELECT NAME="collPoints" ID="collPoints" onChange="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&benID=<?=$benificiaryContent["benID"];?>&focusID=<?=$focusID?>'; document.addTrans.submit();">
                   					<OPTION VALUE="">- Select Collection Point -</OPTION>
                   			<?
                   					$queryCollPoints = "SELECT `cp_id`, `cp_corresspondent_name` FROM " . TBL_COLLECTION . " WHERE `cp_country` = '".$benificiaryContent["Country"]."'";
                   					$collPoints = selectMultiRecords($queryCollPoints);
                   					for ($i = 0; $i < count($collPoints); $i++) {
                   			?>
                   					<OPTION VALUE="<? echo $collPoints[$i]["cp_id"] ?>" <? echo ($_SESSION["collectionPointID"] == $collPoints[$i]["cp_id"] ? "selected" : "") ?>><? echo $collPoints[$i]["cp_corresspondent_name"] ?></OPTION>
                   			<?	}  ?>
                   				</SELECT>
                   				<!--script language="JavaScript">SelectOption(document.addTrans.collPoints,"<?=$_SESSION["collectionPointID"]; ?>");</script-->
                   		<?	} else { ?>
                        	<INPUT NAME="agentCity" TYPE="text" ID="agentCity" SIZE="15">
                          <INPUT TYPE="button" NAME="Submit" VALUE="Search" onClick=" if(document.addTrans.agentCity.value != '') { window.open('search-agent-city.php?agentCity=' + document.addTrans.agentCity.value + '&transID=<? echo $_GET["transID"]?>' +  '&benCountry=<? echo $benificiaryContent["Country"]?>'+  '&exchangeRate=<? echo $_SESSION["exchangeRate"]?>'+  '&calculateBy=<? echo $_SESSION["calculateBy"]?>', 'agentCity', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=800') } else { alert ('Please write city name in the text box to search.')}">
                      <?	}  ?>
                          <INPUT NAME="distribut" TYPE="hidden" ID="distribut" VALUE="<? echo $_SESSION["benAgentID"]?>">
                      
					    </TD>
                     <TD WIDTH="200">&nbsp;</TD>
                      </TR>

                    </TABLE>
<TABLE WIDTH="650" BORDER="0" CELLPADDING="2" CELLSPACING="0"> 
                      <? 
					 		
		$queryCust = "select *  from cm_collection_point as c, admin a where c.cp_ida_id = a.userID And cp_id  ='" . $_SESSION["collectionPointID"] . "'and a.agentStatus = 'Active'";
			$senderAgentContent = selectFrom($queryCust);
			$senderAgentContent["cp_id"];
			$dist = $senderAgentContent["cp_ida_id"];
			$_SESSION["distribut"] = $dist;
			
			$_SESSION["benAgentID"] = $dist;
			 $queryDistributor = "select *  from " .TBL_ADMIN_USERS. " where  userID  ='" . $senderAgentContent["cp_ida_id"] . "'";
			$queryExecute = selectFrom($queryDistributor);
			
			if($senderAgentContent["cp_id"] != "")
			{
		?>
                        <TR>
                        <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Collection point name</FONT></TD>
                        <TD WIDTH="200"><? echo $senderAgentContent["cp_corresspondent_name"]?> </TD>
                        <TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Contact Person </FONT></TD>
                        <TD WIDTH="200"><? echo $senderAgentContent["cp_contact_person_name"]?></TD>
                      </TR>
                    	 <? if(CONFIG_DISTRIBUTOR_ALIAS != "1") { ?>
                      <TR>
                       <TD WIDTH="150" ALIGN="right"><FONT CLASS="style2">Distributor</FONT></TD>
                        <TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["name"]?></TD>
                      </TR> 
                      <? } ?>
                     
                     <? if(CONFIG_DISTRIBUTOR_ALIAS == "1" && $agentType != "SUPA") { ?>
                      <TR>
                       <TD WIDTH="150" ALIGN="right"><FONT CLASS="style2">Distributor<? if($queryExecute["distAlias"] != ''){ echo "Alias"; }?></FONT></TD>
                         <? if($queryExecute["distAlias"] != '') { ?>
                           <TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["distAlias"]?></TD>
                         <? }else{ ?>
                            <TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["agentCompany"]?></TD>
                         <? } ?>  
                      </TR> 
                      <? } ?>
                      
                      <? if(CONFIG_DISTRIBUTOR_ALIAS == "1" && $agentType == "SUPA") {?>
                     <TR>
                        <TD WIDTH="150" ALIGN="right"><FONT CLASS="style2">Distributor Alias</FONT></TD>
                        <TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["distAlias"]?></TD>
                     </TR> 
                          <? } ?>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Branch Address</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_branch_address"]?></td>
						<td width="100" align="right"><font color="#005b90"><?=$locationString?></font></td>
                        <td width="200"><? echo $senderAgentContent["cp_branch_no"]?></td>
                      </tr>
                     
                      <TR>
                        <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">City</FONT></TD>
                        <TD WIDTH="200"><? echo $senderAgentContent["cp_city"]?></TD>
                        <TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Country</FONT></TD>
                        <TD WIDTH="200"><? echo $senderAgentContent["cp_country"]?></TD>
                      </TR>
                      <TR>
                        <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Phone</FONT></TD>
                        <TD WIDTH="200"><? echo $senderAgentContent["cp_phone"]?></TD>
                        <TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Fax</FONT></TD>
                        <TD WIDTH="200"><? echo $senderAgentContent["cp_fax"]?></TD>
                      </TR> 
                      

                      <?
			  }
			  ?>
<?
	}
}elseif ($countryBasedFlag && $_SESSION["transType"] == "Pick up"){
			$queryCust = "select *  from cm_collection_point as c, admin a where c.cp_ida_id = a.userID And cp_id  ='" . $_SESSION["collectionPointID"] . "'and a.agentStatus = 'Active'";
			$senderAgentContent = selectFrom($queryCust);
			$senderAgentContent["cp_id"];
			$dist = $senderAgentContent["cp_ida_id"];
			$_SESSION["distribut"] = $dist;
			
			$_SESSION["benAgentID"] = $dist;
			 $queryDistributor = "select *  from " .TBL_ADMIN_USERS. " where  userID  ='" . $senderAgentContent["cp_ida_id"] . "'";
			$queryExecute = selectFrom($queryDistributor);
			
			if($senderAgentContent["cp_id"] != "")
			{
		?>
                        <TR>
                        <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Collection point name</FONT></TD>
                        <TD WIDTH="200"><? echo $senderAgentContent["cp_corresspondent_name"]?> </TD>
                        <TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Contact Person </FONT></TD>
                        <TD WIDTH="200"><? echo $senderAgentContent["cp_contact_person_name"]?></TD>
                      </TR>
                    	 <? if(CONFIG_DISTRIBUTOR_ALIAS != "1") { ?>
                      <TR>
                       <TD WIDTH="150" ALIGN="right"><FONT CLASS="style2">Distributor</FONT></TD>
                        <TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["name"]?></TD>
                      </TR> 
                      <? } ?>
                     
                     <? if(CONFIG_DISTRIBUTOR_ALIAS == "1" && $agentType != "SUPA") { ?>
                      <TR>
                       <TD WIDTH="150" ALIGN="right"><FONT CLASS="style2">Distributor<? if($queryExecute["distAlias"] != ''){ echo "Alias"; }?></FONT></TD>
                         <? if($queryExecute["distAlias"] != '') { ?>
                           <TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["distAlias"]?></TD>
                         <? }else{ ?>
                            <TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["agentCompany"]?></TD>
                         <? } ?>  
                      </TR> 
                      <? } ?>
                      
                      <? if(CONFIG_DISTRIBUTOR_ALIAS == "1" && $agentType == "SUPA") {?>
                     <TR>
                        <TD WIDTH="150" ALIGN="right"><FONT CLASS="style2">Distributor Alias</FONT></TD>
                        <TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["distAlias"]?></TD>
                     </TR> 
                          <? } ?>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Branch Address</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_branch_address"]?></td>
						<td width="100" align="right"><font color="#005b90">Location ID</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_branch_no"]?></td>
                      </tr>
                     
                      <TR>
                        <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">City</FONT></TD>
                        <TD WIDTH="200"><? echo $senderAgentContent["cp_city"]?></TD>
                        <TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Country</FONT></TD>
                        <TD WIDTH="200"><? echo $senderAgentContent["cp_country"]?></TD>
                      </TR>
                      <TR>
                        <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Phone</FONT></TD>
                        <TD WIDTH="200"><? echo $senderAgentContent["cp_phone"]?></TD>
                        <TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Fax</FONT></TD>
                        <TD WIDTH="200"><? echo $senderAgentContent["cp_fax"]?></TD>
                      </TR> 
					  <INPUT NAME="distribut" TYPE="hidden" ID="distribut" VALUE="<? echo $_SESSION["benAgentID"]?>">
                      <?
			  }
			  ?>
<?
}
?>			  
                    </TABLE>                    <?
					  }
					  if($_SESSION["transType"] == "Home Delivery")
					  {
					  	if (CONFIG_CITY_SERVICE_ENABLED)
					  	{
					  		$cityCount = countRecords("select count(*) from ".TBL_SRV_NOT_AVAILABLE." where country = '".$benificiaryContent["Country"]."' and city = '".$benificiaryContent["City"]."' and service like '%Home Delivery%' and Active = 'Yes'");
					  		if ($cityCount > 0)
					  		{
					  	$queryBenAgentID = selectFrom("select dist_ID from ".TBL_SRV_NOT_AVAILABLE." where country = '".$benificiaryContent["Country"]."' and city = '".$benificiaryContent["City"]."' and Active = 'Yes'");
               
               	$_SESSION["distribut"] = $queryBenAgentID[0];
				
				$dist = $_SESSION["distribut"];
	 			$_SESSION["benAgentID"] = $_SESSION["distribut"];
	 		      	 ?>
                    <TABLE WIDTH="650" BORDER="0" CELLPADDING="2" CELLSPACING="0">
                      <TR>
                        <TD WIDTH="650"><SPAN CLASS="style4">Note:</SPAN><BR>
                        <SPAN CLASS="style5">Please make sure that Beneficiary's Home Address is correct. <?=$company?> Transfer will not be responsible for faulty address provided by Sender or Agent.</SPAN>
                        <INPUT TYPE="hidden" NAME="notService" VALUE="">
                        </TD>
                      </TR>
                    </TABLE>
					  				
               <? } else
               {
               	 ?>
                    <TABLE WIDTH="650" BORDER="0" CELLPADDING="2" CELLSPACING="0">
                      <TR>
                        <TD WIDTH="650"><SPAN CLASS="style4">Sorry:</SPAN><BR>
                        <SPAN CLASS="style5">Service for Home Delivery in city: <? echo "\"" . $benificiaryContent["City"] . "\"" ?> is not Available.</SPAN>
                        <INPUT TYPE="hidden" NAME="notService" VALUE="Home Delivery">
                        </TD>
                      </TR>
                    </TABLE>
                    <?
                  }
                }
                else
                {
                	?>
                	<TABLE WIDTH="650" BORDER="0" CELLPADDING="2" CELLSPACING="0">
                      <TR>
                        <TD WIDTH="650"><SPAN CLASS="style4">Note:</SPAN><BR>
                        <SPAN CLASS="style5">Please make sure that Beneficiary's Home Address is correct. <?=$company?> Transfer will not be responsible for faulty address provided by Sender or Agent.</SPAN>
                        <INPUT TYPE="hidden" NAME="notService" VALUE="">
                        </TD>
                      </TR>
                    </TABLE>
                    <?
                    
                    $distributors = selectFrom("select userID from admin where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'N'  and isCorrespondent != '' and authorizedFor like '%Home Delivery%' and IDAcountry like '%".$benificiaryContent["Country"]."%'");					
				
					if($distributors["userID"] != "")
					{
						   $_SESSION["distribut"] = $distributors["userID"];
						   
						   $dist = $_SESSION["distribut"];
						   $_SESSION["benAgentID"] = $_SESSION["distribut"];
						   
					}else{
						   $_SESSION["distribut"] = "";
						   
						   $dist = "";
						   $_SESSION["benAgentID"] = "";
						}	
					}
				
				
				
				}
				//debug($_SESSION["transType"]);
			  if($_SESSION["transType"] == "Bank Transfer" || $_SESSION["transType"] == "ATM Card")
			  {
			  	?>
                    
                    <TABLE WIDTH="650" BORDER="0" CELLPADDING="2" CELLSPACING="0" BORDERCOLOR="#006600">
             			<?
						$bankDetails = true;
						if($useTransDetailsFlag && $_SESSION["useTransDetails"] != "Yes")
							$bankDetails = false;
					 	if($_SESSION["transType"] == "Bank Transfer" && $bankDetails){ 
						
						?>
                      <TR>
                        <TD HEIGHT="20" COLSPAN="4"><SPAN CLASS="style2">Beneficiary Bank Details</SPAN></TD>
                      </TR>
				<?
				 /***************************************
				  * Multiple Bank Transfer Types Starts *
				  ***************************************/	 
				 if(CONFIG_MULTIPLE_BANK_TYPES == "1")
				 {
							$strIbanSelect = "";
							$strNoBaSelect = "";
							if($_SESSION["bankTransferType"] == "B")
								$strNoBaSelect = 'checked="checked"';
							else
								$strIbanSelect = 'checked="checked"';
								
					?>  
						<tr id="bankTransferTypeRow">
							<td>&nbsp;</td>
							<td align="left" colspan="3">
								<input type="radio" id="bankTransferType1" name="bankTransferType" value="I" <?=$strIbanSelect?> />&nbsp;IBAN&nbsp;&nbsp;
								<input type="radio" id="bankTransferType2" name="bankTransferType" value="B" <?=$strNoBaSelect?> />&nbsp;Normal Bank Transfer
							</td>
						</tr>
						<?
							$benIBAN = $benificiaryContent["IBAN"];
							if ( strtoupper($_SESSION["transactionCreatedBy"]) == "CUSTOMER" )
							{
								$IBANQuery = "select IBAN from cm_bankdetails where benID='".$benificiaryContent["benID"]."'";
								if(defined("CONFIG_IBAN_OR_BANK_TRANSFER") && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
									$IBANQuery = "select IBAN from bankDetails where transID='".$contentTrans["transID"]."'";
								}
								$resultForIBAN = selectFrom($IBANQuery);
								if ( !empty( $resultForIBAN ) )
								{
									$benIBAN = $resultForIBAN["IBAN"];
								}
							}
						?>
						<TR VALIGN="top" id="bankTransferRow1">
							<TD WIDTH="135" HEIGHT="20" ALIGN="right" VALIGN="top"><FONT COLOR="#005b90">IBAN<FONT COLOR="#ff0000">*</FONT> </FONT></TD>
							<TD WIDTH="209" HEIGHT="20">
								<INPUT TYPE="text" NAME="IBAN" ID="IBAN" VALUE="<?=$benIBAN;?>" maxlength="30" SIZE="40" readonly>
							</TD>
							<TD WIDTH="102" HEIGHT="20" ALIGN="right">
								<FONT COLOR="#005b90"><? echo ($benificiaryContent["Country"] == "GEORGIA" ? "Remarks" : "&nbsp;") ?></FONT>
							</TD>
							<TD WIDTH="188" HEIGHT="20">
								<? echo ($benificiaryContent["Country"] == "GEORGIA" ? "<input type='text' name='ibanRemarks' value='".$_SESSION["ibanRemarks"]."'>" : "&nbsp;") ?>
							</TD>
						</TR>
						<TR id="bankTransferRow2" <? if ($_SESSION["IBAN"] == '') { ?>style="display:none;"<? } ?>>
							<TD WIDTH="135" HEIGHT="20" ALIGN="center" COLSPAN="3">&nbsp;</TD>
							<TD WIDTH="188" HEIGHT="20"><INPUT TYPE="hidden" NAME="benCountryRegion" VALUE="European">&nbsp;</TD>
						</TR>
						<TR id="bankTransferRow3" style="display:none">
                        <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Bank Name<FONT COLOR="#ff0000">*</FONT> </FONT></TD>
                        <TD WIDTH="200" HEIGHT="20">
							<INPUT NAME="bankName" TYPE="text" ID="bankName" VALUE="<?=$_SESSION["bankName"]; ?>" MAXLENGTH="25" />
						</TD>
                        <TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Acc Number<FONT COLOR="#ff0000">*</FONT> </FONT></TD>
                        <TD WIDTH="200"><INPUT NAME="accNo" TYPE="text" ID="accNo" VALUE="<?=$_SESSION["accNo"]; ?>" MAXLENGTH="50" /></TD>
                      </TR>
                     	<TR id="bankTransferRow4" style="display:none">
                        <TD WIDTH="150" HEIGHT="20" ALIGN="right">
							<FONT COLOR="#005b90">
							<? if(CONFIG_BRANCH_NAME_LABEL == '1'){ ?>Branch name<? }else {?>Branch Name/Number<? }?><FONT COLOR="#ff0000">*</FONT></FONT>
						</TD>
                      
						<TD WIDTH="200" HEIGHT="20">
							<INPUT NAME="branchCode" TYPE="text" ID="branchCode" VALUE="<? echo $_SESSION["branchCode"]; ?>" MAXLENGTH="25" />
						</TD>
						
						<TD WIDTH="150" HEIGHT="20" ALIGN="right">&nbsp;</TD>
			        	<TD WIDTH="200" HEIGHT="20">&nbsp;</TD>
		    	    </TR>
					  <?php
					  	$branchMandatoryFlag = false;
						$mandatoryStr = '<FONT COLOR="#ff0000">*</FONT>';
					  	if(defined("CONFIG_BRANCH_ADDRESS_TRANS_NON_MANDATORY") && CONFIG_BRANCH_ADDRESS_TRANS_NON_MANDATORY=="1"){
						  	$branchMandatoryFlag = true;
							$mandatoryStr = '';
						}
							
					  ?>
                    	<TR id="bankTransferRow5" style="display:none">
                        <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Branch Address<?php echo $mandatoryStr?></FONT> </TD>
                        <TD WIDTH="200" HEIGHT="20">
							<INPUT NAME="branchAddress" TYPE="text" ID="branchAddress" VALUE="<?=$_SESSION["branchAddress"]; ?>" SIZE="30" MAXLENGTH="254"  />
						</TD>
                       <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Swift Code<FONT COLOR="#ff0000"></FONT></FONT></TD>
                       <TD WIDTH="200" HEIGHT="20"><INPUT NAME="swiftCode" TYPE="text" ID="swiftCode" VALUE="<?=$_SESSION["swiftCode"]; ?>" MAXLENGTH="25" /></TD>
                    </TR>
                    	<TR id="bankTransferRow6" style="display:none">
                      <?
						  if(CONFIG_ACCOUNT_TYPE_ENABLED == '1')
						  {
							if(CONFIG_REGULAR_ACCOUNT_TYPE_LABEL != "")
								$regularAccountType = CONFIG_REGULAR_ACCOUNT_TYPE_LABEL;
							else
								$regularAccountType = "Current";	
                      ?>	
                        <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Account Type</FONT></TD>
                        <TD WIDTH="200" HEIGHT="20"><BR>
                        	<INPUT NAME="accountType" TYPE="radio" ID="accountType" VALUE="<?=$regularAccountType?>" <? echo ($_SESSION["accountType"] != "Savings" ? " readonly='readonly' checked"  : "")?>> <?=$regularAccountType?><BR>
                        	<INPUT NAME="accountType" TYPE="radio" ID="accountType" VALUE="Savings" <? echo ($_SESSION["accountType"] == "Savings" ? " readonly='readonly' checked"  : "")?>> Savings
                        	</TD>
                       <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">&nbsp;</FONT></TD>
                       <TD WIDTH="200" HEIGHT="20">&nbsp;</TD>
                      </TR>


					 <?
						}
					}
				    /**************************************
				     * Multiple Bank Transfer Types End   *
				     **************************************/	 
					else
					{
					$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benificiaryContent["Country"]."'");
					$bolShowIbanField = false;
					if (CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
					{
						$benIBAN = $benificiaryContent["IBAN"];
						if ( strtoupper($_SESSION["transactionCreatedBy"]) == "CUSTOMER" )
						{
							$IBANQuery = "select IBAN from cm_bankdetails where benID='".$benificiaryContent["benID"]."'";
							if(defined("CONFIG_IBAN_OR_BANK_TRANSFER") && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
								$IBANQuery = "select IBAN from bankDetails where transID='".$contentTrans["transID"]."'";
							}
							$resultForIBAN = selectFrom($IBANQuery);
							if ( !empty( $resultForIBAN ) )
							{
								$benIBAN = $resultForIBAN["IBAN"];
								$bolShowIbanField = true;
							}
						}
						
					}
					
					if($bolShowIbanField) {
						?>
							<TR VALIGN="top">
							<TD WIDTH="135" HEIGHT="20" ALIGN="right" VALIGN="top"><FONT COLOR="#005b90">IBAN<FONT COLOR="#ff0000">*</FONT> </FONT></TD>
							<TD WIDTH="209" HEIGHT="20">
								<INPUT TYPE="text" NAME="IBAN" ID="IBAN" VALUE="<?=$benIBAN;?>" 
										maxlength="30" SIZE="40" readonly>
							</TD>
							
							<!--<td width="209" height="20"><input type="text" name="IBAN" id="IBAN" value="<? echo $_SESSION["IBAN"]?>" maxlength="26" onBlur="if (document.getElementById('IBAN').value != ''){ document.getElementById('ibanDiv').innerHTML = 'You have entered ' + document.getElementById('IBAN').value.length + ' IBAN characters'; document.getElementById('trIBANChars').style.display = 'table-row'; } else { document.getElementById('trIBANChars').style.display = 'none'; }"></td>-->
								<TD WIDTH="102" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><? echo ($benificiaryContent["Country"] == "GEORGIA" ? "Remarks" : "&nbsp;") ?></FONT></TD>
							<TD WIDTH="188" HEIGHT="20">
								<? echo ($benificiaryContent["Country"] == "GEORGIA" ? "<input type='text' name='ibanRemarks' value='".$_SESSION["ibanRemarks"]."'>" : "&nbsp;") ?>
							</TD>
							</TR>
							<TR ID="trIBANChars" <? if ($_SESSION["IBAN"] == '') { ?>style="display:none;"<? } ?>>
							<TD WIDTH="135" HEIGHT="20" ALIGN="center" COLSPAN="3">&nbsp;</TD>
							<TD WIDTH="188" HEIGHT="20"><INPUT TYPE="hidden" NAME="benCountryRegion" VALUE="European">&nbsp;</TD>
							</TR>
						<? 
						} elseif(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER == "1") {
								$benIBAN = $benificiaryContent["IBAN"];
								$ibanBankFlag = true; // if IBAN exists or IbanOrBank exists then it will be false (To enter for Bank Details)
								if ( strtoupper($_SESSION["transactionCreatedBy"]) == "CUSTOMER")
								{
									$IBANQuery = "select IBAN from cm_bankdetails where benID='".$benificiaryContent["benID"]."'";// add/edit transaction
									if(defined("CONFIG_IBAN_OR_BANK_TRANSFER") && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
										$IBANQuery = "select IBAN from bankDetails where transID='".$contentTrans["transID"]."'";// edit transaction simple
										if($_GET["benID"]!=""){
											$IBANQuery = "select IBAN from bankDetails where benID='".$_GET["benID"]."'";// edit transaction of Online Module after selecting beneficiary .
										}
									}
									$resultForIBAN = selectFrom($IBANQuery);
									if ( !empty( $resultForIBAN ) )// add transaction
									{
										$benIBAN = $resultForIBAN["IBAN"];
										$ibanBankFlag = false;
									}
								}
								else{
									if($benificiaryContent["IbanOrBank"]=="iban")// add/edit transaction
										$ibanBankFlag = false;
								}
								if($benIBAN==""){
									$ibanBankFlag = true;
								}
						?>	
							<? if($ibanBankFlag){?>
							<TR>
								<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Bank Name<FONT COLOR="#ff0000">*</FONT></FONT></TD>
								<TD WIDTH="200" HEIGHT="20"><INPUT NAME="bankName" TYPE="text" ID="bankName" VALUE="<?=($_SESSION["bankName"] !="" ? $_SESSION["bankName"] : ""); ?>" MAXLENGTH="25" readonly /></TD>
								<TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Acc Number</FONT><FONT COLOR="#ff0000">*</FONT></TD>
								<TD WIDTH="200"><INPUT NAME="accNo" TYPE="text" ID="accNo" VALUE="<?=($_SESSION["accNo"] !="" ? $_SESSION["accNo"] : ""); ?>" MAXLENGTH="50" readonly /></TD>
							</TR>
							<TR>
								<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">
									<? if(CONFIG_BRANCH_NAME_LABEL == '1'){ ?>Branch name<? }else {?>Branch Name/Number<? }?></TD>
								<TD WIDTH="200" HEIGHT="20">
									<INPUT NAME="branchCode" TYPE="text" ID="branchCode" VALUE="<?=($_SESSION["branchCode"] !="" ? $_SESSION["branchCode"] : ""); ?>" MAXLENGTH="25" readonly />
								</TD>
								<TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Account Type</FONT></TD>
								<TD WIDTH="200" ><INPUT NAME="accountType" TYPE="text" ID="branchAddress" VALUE="<?=($_SESSION["accountType"] !="" ? $_SESSION["accountType"] : ""); ?>" MAXLENGTH="50" readonly /></TD>
							</TR>
					  <?php
					  	$branchMandatoryFlag = false;
						$mandatoryStr = '<FONT COLOR="#ff0000">*</FONT>';
					  	if(defined("CONFIG_BRANCH_ADDRESS_TRANS_NON_MANDATORY") && CONFIG_BRANCH_ADDRESS_TRANS_NON_MANDATORY=="1"){
						  	$branchMandatoryFlag = true;
							$mandatoryStr = '';
						}
							
					  ?>
							<TR>
								<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Branch Address<?php echo $mandatoryStr?></FONT></TD>
								<TD WIDTH="200" HEIGHT="20">
									<INPUT NAME="branchAddress" TYPE="text" ID="branchAddress" VALUE="<?=($_SESSION["branchAddress"] !="" ? $_SESSION["branchAddress"] : ""); ?>" SIZE="30" MAXLENGTH="254" readonly />
								</TD>
							   <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Swift Code</FONT></TD>
							   <TD WIDTH="200" HEIGHT="20"><INPUT NAME="swiftCode" TYPE="text" ID="swiftCode" VALUE="<?=($_SESSION["swiftCode"] !="" ? $_SESSION["swiftCode"] : ""); ?>" MAXLENGTH="25" readonly /></TD>
							</TR>
							<? }
							else{ ?>
							<TR VALIGN="top">
								<TD WIDTH="135" HEIGHT="20" ALIGN="right" VALIGN="top"><FONT COLOR="#005b90">IBAN</FONT></TD>
								<TD WIDTH="209" HEIGHT="20"><INPUT TYPE="text" NAME="IBAN" ID="IBAN" VALUE="<?=($_SESSION["IBAN"] !="" ? $_SESSION["IBAN"] : "");?>" maxlength="30" SIZE="42" readonly >						</TD>
								<TD WIDTH="102" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">&nbsp;</TD>

								<TD WIDTH="188" HEIGHT="20">&nbsp;</TD>
							</TR>
							<? } ?>
						<?	
					} else {
       					?>
						<?
		$renameBankFlag = false;
		$renameStr = 'Bank Name';
  		if(defined("CONFIG_BANKNAME_TRANS_RENAME") && CONFIG_BANKNAME_TRANS_RENAME=="1"){
	  		$renameBankFlag = true;
			$renameStr = 'Beneficiary Account Name';
	}
						?>   
                      <TR>
                        <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><?=$renameStr;?><FONT COLOR="#ff0000">*</FONT> </FONT></TD>
                        <TD WIDTH="200" HEIGHT="20">
						<? 
						$benCountryName = $benificiaryContent["Country"];
						$bankdataQ = "select * from imbanks where 1 and country = '".$benCountryName."'";
						$bank_data = selectMultiRecords($bankdataQ);
						if ($benCountryName == CONFIG_CPF_COUNTRY && count($bank_data) > 0)
						{
							?>
							<SELECT name='bankName' style='font-family:verdana; font-size: 11px; width:150'  >
								<OPTION VALUE="">- select bank -</OPTION>
							<?
							for($i=0;$i < count($bank_data); $i++)
							{
							  	echo "<option value='".$bank_data[$i]["bankName"]."'>".$bank_data[$i]["bankName"]."</option>\n";
							}
							?>
							</SELECT>
							<SCRIPT LANGUAGE="JavaScript">SelectOption(document.addTrans.bankName, "<?=$_SESSION["bankName"]; ?>");</SCRIPT>
							<?
						}
						else
						{

							 //debug($_SESSION["bankName"]);
							 if (!empty($_arrBankCodes))
							 {
							?>
								<select name="bankName">
									<? foreach($_arrBankCodes as $kBank => $vBank) 
									{ 
										$strSelected = "";
										if(trim($vBank) == trim($_SESSION["bankName"]))
										{
											$strSelected = 'selected="selected"';
											//debug($vBank);
										}
									?>
										
										<option value="<?=$vBank?>" <?=$strSelected?>><?=$vBank?></option>
									<? } ?>
								</select>
							<?
							 }
							 else
							 {
						?>
							<INPUT NAME="bankName" TYPE="text" size="40" ID="bankName" VALUE="<?=$_SESSION["bankName"]; ?>" MAXLENGTH="60" <? if(!empty($_SESSION["bankName"]) && CONFIG_BEN_BANK_DETAILS == "1") echo "readonly"; ?>>
						<?
							}
						}
						?>
						<?php
					  	$accNoMandatoryFlag = false;
						$mandatoryStr = '<FONT COLOR="#ff0000">*</FONT>';
					  	if(defined("CONFIG_ACCNO_TRANS_NON_MANDATORY") && CONFIG_ACCNO_TRANS_NON_MANDATORY=="1"){
						  	$accNoMandatoryFlag = true;
							$mandatoryStr = '';
						}
?>
					    </TD>
                        <TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Acc Number<FONT COLOR="#ff0000"><?=$mandatoryStr;?></FONT> </FONT></TD>
                        <TD WIDTH="200"><INPUT NAME="accNo" TYPE="text" ID="accNo" VALUE="<?=$_SESSION["accNo"]; ?>" MAXLENGTH="50" <? if(!empty($_SESSION["accNo"]) && CONFIG_BEN_BANK_DETAILS == "1") echo "readonly"; ?>>
						&nbsp;
						<? if(CONFIG_MIDDLE_TIER_CLIENT_ID == "3") { ?>
							&nbsp;
							<img src="images/info.gif" title="Account No Notice | To send transaction via API, account number should be of 13 digits." />
						<? } ?>
						</TD>
					   </TR>
					   <TR>
                        <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">
						<? if(CONFIG_BRANCH_NAME_LABEL == '1'){ ?>Branch name<? }else {?>Branch Name/Number<? }?></FONT></TD>
						 <?
					   /**
					    This config added by Naiz Ahamd at 17-04-2009 @4720 for Muthoot Client
						To display Branch Name instead of branch code
					   */ 
					    if(CONFIG_ADD_BEN_BANK_ON_CREATE_TRANSACTION == "1") {
						?>
					   <TD WIDTH="200" HEIGHT="20">
							<INPUT NAME="branchName" TYPE="text" ID="branchName" VALUE="<? echo $_SESSION["branchName"]; ?>" MAXLENGTH="25" />
						</TD>
						<? 
						}else{ 
								if(CONFIG_ADD_BEN_BANK_FIELDS_ON_CREATE_TRANSACTION =="1"){
						?>	
							<TD WIDTH="200" HEIGHT="20">
							<textarea NAME="branchCode" ID="branchCode" cols="37" <? if(!empty($_SESSION["branchCode"]) && CONFIG_BEN_BANK_DETAILS == "1") echo "readonly"; ?>><? echo $_SESSION["branchCode"]; ?> </textarea>
							</TD>
							
							<? }else{ ?>
							<TD WIDTH="200" HEIGHT="20">
							<INPUT NAME="branchCode" TYPE="text" ID="branchCode" VALUE="<? echo $_SESSION["branchCode"]; ?>" MAXLENGTH="25" <? if(!empty($_SESSION["branchCode"]) && CONFIG_BEN_BANK_DETAILS == "1") echo "readonly"; ?> />
							</TD>
						<?
						  }
						 }
						  ?>
						<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">&nbsp;</FONT></TD>
						<TD WIDTH="200" HEIGHT="20">&nbsp;</TD>
		        </TR>
		        
                      <TR>
					  <?php
					  	$branchMandatoryFlag = false;
						$mandatoryStr = '<FONT COLOR="#ff0000">*</FONT>';
					  	if(defined("CONFIG_BRANCH_ADDRESS_TRANS_NON_MANDATORY") && CONFIG_BRANCH_ADDRESS_TRANS_NON_MANDATORY=="1"){
						  	$branchMandatoryFlag = true;
							$mandatoryStr = '';
						}
							
					  ?>
                        <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Branch Address<?php echo $mandatoryStr?></FONT> </TD>
						<TD WIDTH="200" HEIGHT="20"><INPUT NAME="branchAddress" TYPE="text" ID="branchAddress" VALUE="<?=$_SESSION["branchAddress"]; ?>" SIZE="30" MAXLENGTH="254" <? if(!empty($_SESSION["branchAddress"]) && CONFIG_BEN_BANK_DETAILS == "1") echo "readonly"; ?>></TD>
						<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Swift Code<FONT COLOR="#ff0000"></FONT></FONT></TD>
						<TD WIDTH="200" HEIGHT="20"><INPUT NAME="swiftCode" TYPE="text" ID="swiftCode" VALUE="<?=$_SESSION["swiftCode"]; ?>" MAXLENGTH="25" <? if(!empty($_SESSION["swiftCode"]) && CONFIG_BEN_BANK_DETAILS == "1") echo "readonly"; ?>></TD>
                       </TR>
					  	<? if(CONFIG_ADD_BEN_BANK_FIELDS_ON_CREATE_TRANSACTION == "1") {?>
						 <TR>
							<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Sort Code</FONT></TD>
							<TD WIDTH="200" HEIGHT="20">
							<INPUT NAME="sortCode" TYPE="text" ID="sortCode" VALUE="<? echo $_SESSION["sortCode"]; ?>" MAXLENGTH="25" />
							</TD>
							<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Routing Number</FONT></TD>
							<TD WIDTH="200" HEIGHT="20">
							<INPUT NAME="routingNumber" TYPE="text" ID="routingNumber" VALUE="<? echo $_SESSION["routingNumber"]; ?>" MAXLENGTH="25" />
							</TD>
						</TR>
						<TR VALIGN="top">
								<TD WIDTH="135" HEIGHT="20" ALIGN="right" VALIGN="top"><FONT COLOR="#005b90">IBAN</FONT></TD>
								<TD WIDTH="209" HEIGHT="20"><INPUT TYPE="text" NAME="IBAN" ID="IBAN" VALUE="<?=($_SESSION["IBAN"] !="" ? $_SESSION["IBAN"] : "");?>" maxlength="30" SIZE="42" onblur="updateAccountNumber();" onkeyup="updateAccountNumber();" />
                                </TD>
								<TD WIDTH="102" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">&nbsp;</TD>

								<TD WIDTH="188" HEIGHT="20">&nbsp;</TD>
							</TR>
						<? } ?>
                      
					  <?
 	                      if(CONFIG_ACCOUNT_TYPE_ENABLED == '1') 
	                      { 
	                        if(CONFIG_REGULAR_ACCOUNT_TYPE_LABEL != "") 
 	                            $regularAccountType = CONFIG_REGULAR_ACCOUNT_TYPE_LABEL; 
 	                        else 
 	                            $regularAccountType = "Current";     
 	                      ?>     
 	                        <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Account Type</FONT></TD> 
	 	                        <TD WIDTH="200" HEIGHT="20"><BR> 
 	                            <INPUT NAME="accountType" TYPE="radio" ID="accountType" VALUE="<?=$regularAccountType?>" <? echo ($_SESSION["accountType"] != "Savings" ? " readonly='readonly' checked"  : "")?>> <?=$regularAccountType?><BR> 
	 	                            <INPUT NAME="accountType" TYPE="radio" ID="accountType" VALUE="Savings" <? echo ($_SESSION["accountType"] == "Savings" ? " readonly='readonly' checked"  : "")?>> Savings 
	 	                            </TD> 
 	                        <? 
	 	                    }else{ 
	 	                        ?> 
	 	                         <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">&nbsp;</FONT></TD> 
	 	                       <TD WIDTH="200" HEIGHT="20">&nbsp;</TD>                       
	 	                        <? 
	 	                        } 
	 	                        ?> 
	 	                       <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">&nbsp;</FONT></TD> 
	                       <TD WIDTH="200" HEIGHT="20">&nbsp;</TD> 
                      </TR>

                      <!--tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Swift Code<font color="#ff0000"></font></font></td>
                        <td width="200" height="20"><input name="swiftCode" type="text" id="swiftCode" value="<?=$_SESSION["swiftCode"]; ?>" maxlength="25"></td>
                        <td width="100" height="20" align="right" title="For european Countries only"><font color="#005b90">IBAN Number </font></td>
                        <td width="200" height="20" title="For european Countries only"><input name="IBAN" type="text" id="IBAN" value="<?=$_SESSION["IBAN"]; ?>" maxlength="50"></td>
                      </tr>
					  <tr>
					  <td colspan="2">Swift Code - Required to ensure accuracy of Bank Details</td>
					  <td colspan="2" align="right">IBAN - Required for European Bank Tansfers</td>
							</tr-->
							
						<?	
							} 
						
						}
						} 
						
						if($_SESSION["transType"] == "ATM Card" || $_SESSION["transType"] == "Bank Transfer"){
						
						?>
						</tr>		
						<? if(CONFIG_PP_TYPE_ON_TRANSACTION == "1" && ($benCountryName == "Poland" || $benCountryName == "Brazil")){ 
							
							
							?>	
					  <TR><TD  align="left"><FONT COLOR="#005b90">Select Pay Point Type<FONT COLOR="#ff0000">*</FONT></FONT></TD>
							<TD><SELECT NAME="payPoint" ID="payPoint" STYLE="font-family:verdana; font-size: 11px; width:226" onChange="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusID?>'; document.addTrans.submit();">													  	
								<OPTION VALUE="">- Select One -</OPTION>
								<OPTION VALUE="Bank Account Deposit" <? echo ($_SESSION["payPoint"]== 'Bank Account Deposit'?  'selected':'');?>>- Bank Account Deposit -</OPTION>
								<OPTION VALUE="Express Bank Account Deposit" <? echo ($_SESSION["payPoint"]== 'Express Bank Account Deposit'?  'selected':'');?>>- Express Bank Account Deposit -</OPTION>
								<OPTION VALUE="Cash Pick Up" <? echo ($_SESSION["payPoint"]== 'Cash Pick Up'?  'selected':'');?>>- Cash Pick up -</OPTION>
							
							</SELECT>
					  	</TD></TR>	
					  	<? } ?>
					  	<?
					  	if($_SESSION["transType"] == "ATM Card"){
					  		//$atmInfo = selectFrom("select cID,cardNo,cardName from cm_cust_credit_card where customerID = '".$_SESSION["benID"]."'");
					  		//if($atmInfo["cID"] != ""){
					  	?>
						 <TR>
							<TD HEIGHT="20" COLSPAN="4"><SPAN CLASS="style2">ATM Card Details</SPAN></TD>
						</TR>
						<TR>
							<TD WIDTH="135" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Card Name</FONT></TD>
							<TD WIDTH="188" HEIGHT="20">
								<input type="text" name="cardName" id="cardName" maxlength="20" value="<?=$_SESSION["cardName"]?>" />
							</TD>
							<TD WIDTH="102" HEIGHT="20" ALIGN="right">
								<FONT COLOR="#005b90">Card Number</FONT>&nbsp;<font color="red">*</font>
							</TD>
							<TD WIDTH="188" HEIGHT="20">
								<input type="text" name="cardNo" id="cardNo" maxlength="16" size="25" value="<?=$_SESSION["cardNo"]?>" />
								&nbsp;
								<img src="images/info.gif" title="ATM Card No Notice | The card number must not be less than or greater than 16 digits." />
							</TD>
						</TR>	
						<TR>
							<TD WIDTH="135" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Bank Name</FONT></TD>
							<TD WIDTH="188" HEIGHT="20">
								<? if (!empty($_arrBankCodes)) { ?>
									<select name="bankName">
										<? foreach($_arrBankCodes as $kBank => $vBank) 
											{ 
											$strSelected = "";
											if(trim($vBank) == trim($_SESSION["bankName"]))
												$strSelected = 'selected="selected"';
										?>
											<option value="<?=$vBank?>" <?=$strSelected?>><?=$vBank?></option>
										<? } ?>
									</select>
								<? } else { ?>
									<INPUT NAME="bankName" TYPE="text" ID="bankName" VALUE="<?=$_SESSION["bankName"]; ?>" MAXLENGTH="25" />
								<? } ?>
							</TD>
							<TD colspan="2">&nbsp;</TD>
						</TR>	
						<TR>
							<TD colspan=4>&nbsp;</TD>
						</TR>
					  	<?
					  		//}
					  	}
					  	?>

					  
					<?
					if(CONFIG_24HR_BANKING == "1"){
					?>
						<TR>
							<TD ALIGN="left" WIDTH="120">
							Banking Type <FONT COLOR="#ff0000">*</FONT>
							</TD>
							<TD>
								<SELECT NAME="bankingType" ID="bankingType" STYLE="font-family:verdana; font-size: 11px; width:226" onChange="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusID?>'; document.addTrans.submit();">
									<OPTION VALUE="">-Select One-</OPTION>
									<OPTION VALUE="normal">Normal banking</OPTION>
									<OPTION VALUE="bank24hr">24 hr banking</OPTION>
								</SELECT>
								<SCRIPT LANGUAGE="JavaScript">
         					SelectOption(document.addTrans.bankingType, "<?=$_SESSION["bankingType"]?>");
                </SCRIPT></TD>
							</td>
						<TR>
					<?
					}
					?>					  
					<?
						/**
						 * Excluding the capability to select the distributor in case of the bank transaction
						 * @Ticket# 3780
						 */
				if( CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS != "1" && $_SESSION["transType"] == "Bank Transfer") 
				{
					if(!defined("CONFIG_SINGLE_DISTRIBUTOR_SYSTEM") || CONFIG_SINGLE_DISTRIBUTOR_SYSTEM == "0") {
						
					?>
					  
					<TR>
					  <TD ALIGN="left" WIDTH="120">
					  		<FONT COLOR="#005b90">
					  				Select Distributor 
					  				<? if( CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS != "1") { ?>
					  				<FONT COLOR="#ff0000">*</FONT>
					  				<? } ?>
					  		</FONT>
					  	</TD>
					  	<TD> 
						
						<?
							/**
							 * @Ticket #5028: Default Distributor of respective country
							 * When ever the beneficiary of the other country selected then the default disturbutor 
							 * will be selected in the drop down list
							 * This is maintained on the check of beneficiary country
							 */
							if($_SESSION["beneficiaryCountry"] != $benificiaryContent["Country"])
								unset($_SESSION["distribut"]);
							$_SESSION["beneficiaryCountry"] = $benificiaryContent["Country"];
							
						?>
					  		<SELECT NAME="distribut" ID="distribut" STYLE="font-family:verdana; font-size: 11px; width:226" onChange="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusID?>'; document.addTrans.submit();">
               <OPTION VALUE="">- Select One -</OPTION>
               <?
					  			
  								/***** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents *****/
									$assocDistClause = "";
									if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
										&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1
										&& ( strstr($agentType, "SUPA") || strstr($agentType, "SUBA"))	)
									{
										// Limit the distributor to only associated distributors if an agent is accessing this page.
										$userID = $_SESSION["loggedUserData"]["userID"];
										if ( !empty( $userID ) )
										{
											// Select associated distributors
											$assocDistQuery = "select associated_distributors from admin where userID='" . $userID . "'";
											$userData = selectFrom( $assocDistQuery );
				
											if ( !empty($userData["associated_distributors"]) )
											{
												// If it is not empty, it means that there are some distributors associated with this agent.
												// So, only those collection points should be shown which are related to those associated distributors.
												// If $userData["associated_distrbutors"] is found to be empty, it means that there is no particular assoication of this
												// agent with any distributor. All the collection will be shown in this case as it will be treated as the current agent is associated with.
												$assocDistIDs = explode( ",", $userData["associated_distributors"] );
												foreach( $assocDistIDs as $distID )
													$assocDistClause .= "'" . $distID . "', ";
				
												$assocDistClause = substr( $assocDistClause, 0, strlen( $assocDistClause ) - 2 );
												$assocDistClause = " and userID in(" . $assocDistClause . ") ";
											}
										}
									} /***** End of code against #3428: Connect Plus - Select Distributors for Agents *****/
					  			
					  			if($_POST["transType"]!= ""){
					  				
					  				$transType = $_POST["transType"];
					  				
					  				}else{
					  					
					  					$transType = $_SESSION["transType"];
					  					
					  					}
					  			
					  	$agenCountry = $benificiaryContent["Country"];
					  	/* Code modified by Khola @ ticket#3497
					  		Have changed the query in $distributors; added the condition userID !='".$_SESSION["senderAgentID"]."' 
					  		which is for, if AnD selected as agent cannot be seleced as distributor for the same transaction. 
					  		Applied restriction for core functionality */
					  		
					  		/**
					  		 * Exclude the userId check field in query
					  		 */
					  		
					  	if(CONFIG_PP_TYPE_ON_TRANSACTION == "1" && $_SESSION["payPoint"]!= "" ){
					  				
					  				 $distributors = "select * from admin where parentID > 0 and adminType='Agent' and isCorrespondent = 'ONLY'  and subagentNum != '' and PPType = '".$_SESSION['payPoint']."' and agentStatus = 'Active' " . $assocDistClause . " order by userID";				
					  		
					  		}else{
					  				$distributors = "select * from admin where parentID > 0 and adminType='Agent' and isCorrespondent != 'N'  and isCorrespondent != '' and PPType = '' and agentStatus = 'Active' " . $assocDistClause . " order by userID";				
					  		}
					  	$ida = selectMultiRecords($distributors);
					  	
					  	/***** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents *****/
						if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
							&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1
							&& ( strstr($agentType, "SUPA") || strstr($agentType, "SUBA")) )
						{
							$totalIDAs = count($ida);
							for ($j=0; $j < $totalIDAs; $j++)
							{
								?>
									<OPTION VALUE="<? echo $ida[$j]["userID"] ?>" <?=($_REQUEST["distribut"] == $ida[$j]["userID"] ? "selected='selected'" : "");?>>
										<? echo $ida[$j]["name"] . " [". $ida[$j]["username"] ."]"; ?>
									</OPTION>
								<?
							}
						} /***** End of code against #3428: Connect Plus - Select Distributors for Agents *****/
						else
						{
							
							$bolSelectedDistributorInList = false;
							
							for ($j=0; $j < count($ida); $j++)
							{
								$authority = $ida[$j]["authorizedFor"];
								$toCountry = $ida[$j]["IDAcountry"];
								$isDefault = $ida[$j]["defaultDistrib"];

								if(strstr(strtoupper($authority),strtoupper($transType)) && strstr(strtoupper($toCountry),strtoupper($agenCountry)))
								{
								
									/* This checkes that is the store distribuote already in the list */
									if($ida[$j]["userID"] == $_SESSION["distribut"])
										$bolSelectedDistributorInList = true;
								
									if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS != "1")
									{
										if($_SESSION["distribut"] == "" && $isDefault == 'Y')
										{
											 $_SESSION["distribut"] = $ida[$j]["userID"];
										}
									}
								
								if(CONFIG_DISTRIBUTOR_ALIAS != '1'){
									
								if($ida[$j]["userID"] == $_SESSION["distribut"])
								{
									$dist =  $ida[$j]["userID"];
									?>
                <OPTION VALUE="<? echo $_SESSION["distribut"]; ?>" selected>
                <? echo $ida[$j]["username"]." [".$ida[$j]["name"]."]"; ?>
               </OPTION>
                <?
								}
								else
								{
								?>
                <OPTION VALUE="<? echo $ida[$j]["userID"]; ?>">
                <? echo $ida[$j]["username"]." [".$ida[$j]["name"]."]"; ?>
                </OPTION>
                <?
								}
							}
								if(CONFIG_DISTRIBUTOR_ALIAS == '1')
								{	
									if($ida[$j]["distAlias"] !='' && $agentType != "SUPA")
									{
										$alias = $ida[$j]["distAlias"];
									}
									else
									{
										$alias = $ida[$j]["agentCompany"];
									}
									
									if($agentType == "SUPA")
									{
										$alias = $ida[$j]["distAlias"];
									}		
											
									if($ida[$j]["userID"] == $_SESSION["distribut"])
									{
										$dist =  $ida[$j]["userID"];
										
										?>
										<OPTION VALUE="<? echo $_SESSION["distribut"]; ?>" selected>
											<? echo $ida[$j]["username"]." [".$alias."]"; ?>
										</OPTION>
										<?
									}
									else
									{
										?>
											<OPTION VALUE="<? echo $ida[$j]["userID"]; ?>">
												<? echo $ida[$j]["username"]." [".$alias."]"; ?>
											</OPTION>
										<?
									}
								}
							}						
						}
						/**
						 * In add trasaction distributor, any distributor can be assigned to any beneficiary
						 * so no restriction on beneficiary country
						 * So id edit trasaction, then an xtra distributor will be added in list
						 * which is assigned from add trasaction distributor area
						 * @Ticket# 3626
						 */
						if(isset($_GET["transID"]) && !empty($_GET["transID"]) && !empty($_SESSION["distribut"]) && CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1" && !$bolSelectedDistributorInList) 
						{
							$defaultDistributorSql = "select * from admin where userID = ".$_SESSION["distribut"];
							$defaultDistributorData = selectFrom($defaultDistributorSql);
						?>
							<OPTION VALUE="<? echo $_SESSION["distribut"]; ?>" selected> 
								<? echo $defaultDistributorData["username"]." [".$defaultDistributorData["name"]."]"; ?>
							</OPTION>
						<?
						}
						/* End #3626 */
					}
					  	?>
			  		</SELECT>
			  		<? if( CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1") { ?>
		  				&nbsp;<EM>Optional</EM>
		  			<? } ?>
				</TD>
			</TR>
				  
                      <?
                      
					  /**
					   * End of if for 
					   * @Ticket# 3321
					   */
					}
  				}
			}
		}
  				//Just Commited as we have drop dwon to distributor be selected manually by user.
  						/*if($_SESSION["transType"] == "Bank Transfer" || $_SESSION["transType"] == "Home Delivery")
							{
									
									$agenCountry = $benificiaryContent["Country"];
									$query = "select * from admin where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'N'  and isCorrespondent != ''";				
									
									$ida = selectMultiRecords($query);
									for ($j=0; $j < count($ida); $j++)
									{
										$authority = $ida[$j]["authorizedFor"];
										$toCountry = $ida[$j]["IDAcountry"];
										if(strstr($authority,"Bank Transfer") && strstr($toCountry,$agenCountry))
										{
											$dist =  $ida[$j]["userID"];
											
											}
									}
									// if find no IDA against this BenCountry then assign this transaction to AdminIDA 
									if($dist == '')
									{
										$dist = 1001111;
										}//100098
										// $benAgentIDs = $adminID;
								//}
								
							}*/
  				
  				
  				?>
        </TABLE>
         <?
		  //distribut
			 if(defined("CONFIG_BEN_DOB_COMP_ATM_VISA_DISTRIBUTOR") && $_SESSION["transType"]!= ""){
			   
			   $distID = explode(",",CONFIG_BEN_ATM_VISA_DISTRIBUTOR);
			    if($_POST["distribut"] == $distID[0] || $senderAgentContent["cp_ida_id"] == $distID[0])
			   	 {
				   /*if($benificiaryContent["dob"] == "" || $benificiaryContent["Address1"] == "")
				   	{*/
				
			?>
				
					<SCRIPT LANGUAGE="JavaScript">
						checkBenDOBCompulsory("<?=$benificiaryContent["benID"]?>","<?=$benificiaryContent["dob"]?>","<?=$benificiaryContent["Address"]?>","<?=$distID[0]?>","<?=$_POST["distribut"]?>","<?=$senderAgentContent["cp_ida_id"]?>");
					</SCRIPT>
			
			<?			
					//}		
				 } 
		      	        
		}
		  
		  ?>                 
                
          <?
		  if($benificiaryContent["benID"] != "" || (CONFIG_TRANS_WITHOUT_BENEFICIARY == '1' && $_SESSION['customerID']!=''))
		  {
		  ?>
		  
                <TR>
                  <td valign="top"><FIELDSET>
                    <LEGEND CLASS="style2">Transaction Details </LEGEND>
                    <? if ($_SESSION["batch"]!= "") { ?>
                    <INPUT TYPE="hidden" NAME="t_amount" VALUE="<?= $_SESSION["amount_transactions"];?>">
                    <INPUT TYPE="hidden" NAME="l_amount" VALUE="<?= $_SESSION["amount_left"];?>">
                  <? }
				$contentCustomer = selectFrom("select *  from ".TBL_CUSTOMER . " where customerID = '".$_SESSION["customerID"]."'");								
				$customerContent["Country"] .  $benificiaryContent["Country"];

				if($_POST['currencyTo'] != "" )
				{
				
				 	 $currencyTo = $_POST['currencyTo'];	
					 $_SESSION["currencyTo"] = $_POST['currencyTo'];
					
				}
	
				$dDate = "";
				
				if($_SESSION['transDate'] != "" )
				{
				
					//$dDate = $_SESSION["transDate"];
					$dDate = explode("/",$_SESSION["transDate"]);
					if(count($dDate) == 3) {
						$dDate = $dDate[2]."-".$dDate[1]."-".$dDate[0];
					} else {
						$dDate = $_SESSION["transDate"];
					}
				
					$_SESSION["transDate"]=$dDate;
				
				}
				
				
				//echo("Distributor ".$dist." ");
			 	//$exchangeData = getExchangeRateTransaction( $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["currencyTo"]);
		/***********
		*
		*Here starts with calculation of Fee and Exchange rates
		*
		************/	 	
		//if(!($fromTotalConfig == '1' && $_POST["totalAmount"]!= ""))
		
	
		
		$benCountryName = $benificiaryContent["Country"];
	
		if($fromTotalConfig != '1')
		{
			
				if(CONFIG_EXCH_RATE_NOT_EDIT == '1' && $_GET["transID"] != "")
				{
					
					$exRateUsed = selectFrom("select rateUsedID, rateWithMargin, currencyFrom , currencyTo from ".TBL_EXCH_RATE_USED." where transID  = '".$_GET["transID"]."'");
					
					$exRate = $exRateUsed["rateWithMargin"];
					$currencyFrom = $exRateUsed["currencyFrom"];
				$currencyTo 	= $exRateUsed["currencyTo"];
					
				}elseif ((CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")  || (CONFIG_AGENT_OWN_MANUAL_RATE == '1' && $_SESSION["checkManualRate"] == 'Y') || $_GET["enterOwnRate"] =="Y")
			 	{
						$bolGetRate = true;
			 			$exID = 0;
			 			//debug($_REQUEST);
						if(defined("CONFIG_DENOMINATION_BASED_EXCHANGE_RATE") && CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1")
						{
							$_SESSION["exchangeRate"] = "";
														
							if(CONFIG_AGENT_OWN_MANUAL_RATE == '1' && strstr(CONFIG_MANUAL_RATE_USERS, $agentType.","))
							{
								if($_REQUEST["checkManualRate"]=="Y")
									$bolGetRate = false;
								else//if(empty($_REQUEST["enterOwnRate"]) || $_REQUEST["enterOwnRate"]=="N")
								{
									$bolGetRate = true;
									$_POST["exchangeRate"] = "";	
								}
							}
							else
								$bolGetRate = true;
								
							if(!empty($_REQUEST["transID"]))
							{
								if($_SESSION["checkManualRate"]=="Y")
									$bolGetRate = false;
								else
									$bolGetRate = true;
							}
						}
				
			 			if ($_SESSION["exchangeRate"] != "" && $_SESSION["exchangeRate"] != 0)
			 			{
							$exRate	= $_SESSION["exchangeRate"];
			 				$_SESSION["exchangeRate"] = "";
			 			}
			 			elseif($_POST["exchangeRate"] != "" && $_POST["exchangeRate"] != 0)
			 			{
			 				$exRate	= $_POST["exchangeRate"];
			 			}
			 			else
			 			{
			 				//$exRate = 0;
							if(CONFIG_24HR_BANKING == "1" && $_SESSION["bankingType"] == "bank24hr")
							{
		 	
					 			$exchangeData = getMultipleExchangeRates($customerContent["Country"], $benificiaryContent["Country"],$_SESSION["currencyTo"], $_SESSION["bankingType"], 0 , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"]);
								$exID 			  = $exchangeData[0];
								$exRate 		  = $exchangeData[1];	
								$currencyFrom = $exchangeData[2];
								$currencyTo 	= $exchangeData[3];	
						 		$_SESSION["currencyFrom"] = $currencyFrom;
						 	}
							elseif($bolGetRate)
							{	
				 				$exchangeData = getMultipleExchangeRates($customerContent["Country"], $benificiaryContent["Country"],$_SESSION["currencyTo"], $dist, $_SESSION["transAmount"] , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"],$_SESSION["senderAgentID"]);
								
								$exID 			  = $exchangeData[0];
								
								
								/* Check the denomination based exchange rate */
								$arrExchangeRate = selectFrom("select * from exchangerate where erID='".$exID."'");			
								if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1" && $arrExchangeRate["isDenomination"] == "Y")
								{
									if($arrExchangeRate["isDenomination"] == "Y")
									{
										$arrDenominationBaseRate = selectFrom("select * from denominationBasedRate where parentRateId='".$arrExchangeRate["erID"]."' and amountFrom <= '".$_SESSION["transAmount"]."' and amountTo >= '".$_SESSION["transAmount"]."'");
										if(is_array($arrDenominationBaseRate))
										{
											$exRate = $arrDenominationBaseRate["exchangeRate"];
										}
										else
										{
											/* error .. not define proper exchange rate */
										}
										
									}
									else	
									{
										$exRate = $exchangeData[1];	
									}	
								}
								else
								{
									$exRate 		  = $exchangeData[1];	
								}
								
								/**
								 * Get the exchange rate DB value only in case if the session value is empty
								 * @Ticket# 3524
								 */
								if(!defined("CONFIG_DENOMINATION_BASED_EXCHANGE_RATE")) 
									if(empty($_SESSION["exchangeRate"]))
										$exRate = $exchangeData[1];	
								/* End of # 3524 */
									
								$currencyFrom = $exchangeData[2];
								$currencyTo 	= $exchangeData[3];
								
								$_SESSION["currencyFrom"] = $currencyFrom;
			 				}
			 				
			 			}
			 	}elseif(CONFIG_MULTI_RATE_PATTERN == '1')
			 	{
			 		$exID = 0;
			 		$exRate = getMultiPatternRate($customerContent["Country"], $benificiaryContent["Country"], $_SESSION["currencyTo"], $dist, 0, $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"], 'Yes');
			 		$currencyFrom = $_SESSION["currencyFrom"];
					$currencyTo 	= $_SESSION["currencyTo"];
			 	}elseif(CONFIG_24HR_BANKING == "1" && $_SESSION["bankingType"] == "bank24hr"){
		 	
		 			$exchangeData = getMultipleExchangeRates($customerContent["Country"], $benificiaryContent["Country"],$_SESSION["currencyTo"], $_SESSION["bankingType"], 0 , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"]);
					$exID 			  = $exchangeData[0];
					$exRate 		  = $exchangeData[1];	
					$currencyFrom = $exchangeData[2];
					$currencyTo 	= $exchangeData[3];	
			
			 	}elseif(CONFIG_CUSTOMER_CATEGORY == '1'){
			 	$exchangeData = getMultipleExchangeRates($customerContent["Country"], $benificiaryContent["Country"],$_SESSION["currencyTo"], $dist, $_SESSION["transAmount"] , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["customerID"]);
			  		$exID 			  = $exchangeData[0];
					$exRate 		  = $exchangeData[1];	
					$currencyFrom = $exchangeData[2];
					$currencyTo 	= $exchangeData[3];	
			    }else{
			 		$exchangeData = getMultipleExchangeRates($customerContent["Country"], $benificiaryContent["Country"],$_SESSION["currencyTo"], $dist, $_SESSION["transAmount"] , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"]);
				
				
				
				
				$exID 			  = $exchangeData[0];
				//$_SESSION["currencyFrom"] = $currencyFrom;
				$currencyFrom = $exchangeData[2];
				$currencyTo 	= $exchangeData[3];

				$arrExchangeRate = selectFrom("select * from exchangerate where erID='".$exID."'");			
				
				//debug($arrExchangeRate);

								
				if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1" && $arrExchangeRate["isDenomination"] == "Y")
				{
								
					if($arrExchangeRate["isDenomination"] == "Y")
					{
						$arrDenominationBaseRate = selectFrom("select * from denominationBasedRate where parentRateId='".$arrExchangeRate["erID"]."' and amountFrom <= '".$_SESSION["transAmount"]."' and amountTo >= '".$_SESSION["transAmount"]."'");
						
						//debug("select * from denominationBasedRate where parentRateId='".$arrExchangeRate["erID"]."' and amountFrom <= '".$_SESSION["transAmount"]."' and amountTo >= '".$_SESSION["transAmount"]."'");
						
						//debug($arrDenominationBaseRate);
						
						if(is_array($arrDenominationBaseRate))
						{
							$exRate = $arrDenominationBaseRate["exchangeRate"];
						}
						else
						{
							/* error .. not define proper exchange rate */
						}
						
					}
					else	
					{
						$exRate = $exchangeData[1];	
					}	
				}
				else
				{
					$exRate 		  = $exchangeData[1];	
				}
				$_SESSION["currencyFrom"] = $currencyFrom;
			}
			if($_POST["local"] == "Local Amount")
			{
				if($_SESSION["transAmount"] != "")
				{
				 $localAmount = getCalculatedAmounts($transAmount, $operatorMult, $exRate); // $operatorMult may be * or /
					//debug($transAmount);
					//debug($localAmount);				 				 
				 	if(CONFIG_ROUND_NUMBER_ENABLED == '1' && strstr(CONFIG_ROUND_NUM_FOR, $_POST["moneyPaid"].","))
					{
						$localAmount = round($localAmount);		
					}
					
				 $_SESSION["localAmount"] = $localAmount;
				}
				if ($_POST["t_amount"] != "")
				{
					$_SESSION["amount_transactions"] = $_POST["t_amount"];
				}
				if ($_POST["l_amount"] != "")
				{
					$_SESSION["amount_left"] = $_POST["l_amount"];
				}
			}
			
			$compareCurrency2 = currencyValue();///it is used for #1772 calculations
			/******
				#5211 - Minas Center
						CONFIG_TRANS_ROUND_NUMBER is parent.
						if CONFIG_ROUND_NUM_FOR is defined like define("CONFIG_ROUND_NUM_FOR", "By Bank Transfer,By Cash,By Cheque,")
						then control should not enter into this block as disturbs trans amount and converts back to decimals.
						This work is done because it came from OPAL (there define("CONFIG_ROUND_NUM_FOR", "inclusive, exclusive,")) from ticket #1772
						by Aslam Shahid
			******/
			$roundNumForFlag = true;
			if(CONFIG_BY_CASH_ROUND_NUM_FOR=="1"){
				$roundNumForFlag = false;
			}
			if($_POST["amount"] == "Amount" || (CONFIG_TRANS_ROUND_NUMBER == '1' && $_POST["moneyPaid"] == 'By Cash' && $compareCurrency2 && $roundNumForFlag))
			{				
				
				if($_SESSION["localAmount"] != "")
				{
					//////This condition is based on opal requirement to calculate amount on rounded value based on cash payment
					if(CONFIG_TRANS_ROUND_NUMBER == 1 && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_FROM')
              		{
              			$localAmount= RoundVal($localAmount);
              		}
              		////////////////////#1772 Ticket
					
					if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1")
					{
						$arrExchangeRate = selectFrom("select * from exchangerate where erID='".$exID."'");			
						//debug($arrExchangeRate);
						if($arrExchangeRate["isDenomination"] == "Y")
						{
							$arrDenominationBaseRate = selectFrom("select * from denominationBasedRate where parentRateId='".$arrExchangeRate["erID"]."' and amountFrom <= '$localAmount' and amountTo >= '$localAmount'");
							
							//debug($arrDenominationBaseRate);
							
							if(is_array($arrDenominationBaseRate))
							{
								$exRate = $arrDenominationBaseRate["exchangeRate"];
								//debug("exRate".$exRate);
								$transAmount = getCalculatedAmounts($localAmount, $operatorDiv, $exRate); // $operatorDiv may be / and *
								//debug($transAmount);
								//debug($localAmount);	
								$_SESSION["transAmount"] = $transAmount;
							}
							else
							{
								/* error .. not define proper exchange rate */
							}
							
						}
						elseif(!empty($exRate))	
						{
							$transAmount = getCalculatedAmounts($localAmount, $operatorDiv, $exRate); // $operatorDiv may be / and *
							//debug($transAmount);
							//debug($localAmount);	
							$_SESSION["transAmount"] = $transAmount;
						}	
					}
					else
					{
						$transAmount = getCalculatedAmounts($localAmount, $operatorDiv, $exRate); // $operatorDiv may be / and *
						//debug($transAmount);
						//debug($localAmount);	
						$_SESSION["transAmount"] = $transAmount;
					}
				}
				if ($_POST["t_amount"] != "")
				{
					$_SESSION["amount_transactions"] = $_POST["t_amount"];
				}
				if ($_POST["l_amount"] != "")
				{
					$_SESSION["amount_left"] = $_POST["l_amount"];
				}
			}
				
{         
	  //Added by Niaz Ahmad Against Ticket # 2538 at 25-10-2007
	            
	           $bolExecuteIMFeeCondition = false;
			   if(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")
			   {
			     // Added by Niaz Ahmad at 29-04-2009 @4812 faith exchange zero commission
			   		if(CONFIG_ZERO_FEE == "1")
						$bolExecuteIMFeeCondition = true;
					else
					{
						if(!empty($_REQUEST["IMFee"]))
							$bolExecuteIMFeeCondition = true;
					}
			   }
			   
			   
				if(CONFIG_FEE_BASED_TRANSTYPE == "1" && defined("CONFIG_COMMISSION_CALCULATED_ON"))
				{
					$amountType=$_SESSION[CONFIG_COMMISSION_CALCULATED_ON];
					$feetransType=$_SESSION["transType"];
										
				}
				else
				{
					$amountType=$_SESSION["transAmount"];
					$feetransType=$_SESSION["transType"];
				}  
	      	   
				$imFee = 0;
				if ($_SESSION["chDiscount"]!="")
				{ 
					
					$imFee = $_SESSION["discount"];
				}
				elseif(CONFIG_TOTAL_FEE_DISCOUNT == '1' && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType)) && $_SESSION["discount_request"] > 0 )
				{
					$imFee = 0 ;
					 //discount_request
				}
				//elseif(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1" && !empty($_REQUEST["IMFee"]))
				elseif($bolExecuteIMFeeCondition)
				{
				  	if(empty($_REQUEST["transID"]) && ( $_SESSION["manualCommission"] == "N" || empty($_REQUEST["manualCommission"])))
					{
						$_SESSION["IMFee"] = "";
						$_POST["IMFee"] = "";
					}
					
					if($_SESSION["IMFee"] != "")
			 		{
			 			$imFee	= $_SESSION["IMFee"];
			 			$_SESSION["IMFee"] = "";
					}
			 		elseif(!empty($_POST["IMFee"]) || (CONFIG_ZERO_FEE == "1" && $_SESSION["manualCommission"] == "Y"))
			 		{
			 			$imFee	= $_POST["IMFee"];
					}
					else
					{
						if(CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1)
							$imFee = imFeeAgent($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["senderAgentID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
						 
						if($imFee <= 0)
							$imFee = imFee($amountType, $customerContent["Country"], $benificiaryContent["Country"],$feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"],$_SESSION["collectionPointID"],$dist);	
					}
  				}
				else
				{
					if(CONFIG_PAYIN_CUSTOMER != '1')
					{
					    if(CONFIG_FEE_BASED_COLLECTION_POINT == 1 && !empty($_SESSION["collectionPointID"]))
						{
							$imFee = imFeeAgent($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["collectionPointID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
						    
						}
						
						if(CONFIG_FEE_DISTRIBUTOR == 1)
						{
							$imFee = imFeeAgent($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $dist, $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
							
						}elseif(CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1)
						{
						 $imFee = imFeeAgent($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["senderAgentID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
						 
						}
						
						if($imFee <= 0)
						{ 
							
							
							$imFee = imFee($amountType, $customerContent["Country"], $benificiaryContent["Country"],$feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"],$_SESSION["collectionPointID"],$dist);	
							
						}
							
					}else
					{	
						if(CONFIG_FEE_BASED_COLLECTION_POINT == 1 && !empty($_SESSION["collectionPointID"]))
						{
							if($customerContent["payinBook"] != "" )
								$imFee = imFeeAgentPayin($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["collectionPointID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
							else
								$imFee = imFeeAgent($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["collectionPointID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
						}
						
						if(CONFIG_BACKDATED_FEE == '1'){
							$feeQuery = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$_GET["transID"]."'");
							if($_GET["transID"] != "" && $_SESSION["transAmount"] == $feeQuery["transAmount"]){
								$imFee = $feeQuery["IMFee"];
							}
						}elseif(CONFIG_FEE_DISTRIBUTOR == '1')
						{
							
							
							if($customerContent["payinBook"] != "" )
							{
								$imFee = imFeeAgentPayin($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $dist,$feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
							
							}else{
						 		$imFee = imFeeAgent($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $dist, $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
						 	
						 	}
						 			 
						}elseif(CONFIG_FEE_AGENT_DENOMINATOR == '1' || CONFIG_FEE_AGENT == '1')
						{
							
							if($customerContent["payinBook"] != "" )
							{
								$imFee = imFeeAgentPayin($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["senderAgentID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
							}
							else{ 
						 		$imFee = imFeeAgent($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["senderAgentID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
						 	
						 	}
					 	}
					 	if($imFee == 0)
						{	
							if($customerContent["payinBook"] != "" )
							{
								$imFee = imFeePayin($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"],$_SESSION["customerID"]);	
							//debug($imFee);	
							}
							else
							{
						 		$imFee = imFee($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"],$_SESSION["collectionPointID"],$dist,$_SESSION["customerID"]);
							//debug($imFee);	
							}
					 	}
				
			  	}
			 }
			
			//echo("at line 2157 transID  ");
			//	echo("Distributor is $dist and Fee is $imFee");
				 $totalAmount =  $_SESSION["transAmount"] ;
				 $localAmount = getCalculatedAmounts($_SESSION["transAmount"], $operatorMult, $exRate); // $operatorMult may be * or /
				//debug($transAmount);
				//debug($localAmount);	
					if(CONFIG_ROUND_NUMBER_ENABLED == '1')
					{
						$localAmount = round($localAmount);		
					}
					
			 	$imFeetemp = ($imFee);
			
				
				if(CONFIG_CURRENCY_CHARGES == '1')
				{
					$local = "No";
					$_SESSION["currencyCharge"]="";
					 $strCurrency = "Select * from ".TBL_COUNTRY." where  currency like '%".$_SESSION["currencyTo"].",%'";
					$currencies = selectMultiRecords($strCurrency);
					for ($c=0; $c < count($currencies); $c++)
					{
						if(strtoupper($currencies[$c]["countryName"]) == strtoupper($benificiaryContent["Country"]))
						$local = "Yes";
					}
					
					if($local=="No")
					{
						///////Out Currency Charges are working on Per 100 Amount
						$tempCharge = $_SESSION["transAmount"]/100.01;
						$tempCharge2 = (int)$tempCharge;
						$tempCharge2 = $tempCharge2 + 1;
						$tempCharge2 = $tempCharge2 * $countryContent["outCurrCharges"];
						$_SESSION["currencyCharge"] = $tempCharge2; 
						$totalAmount = $totalAmount + $_SESSION["currencyCharge"];
					}
					
				}
				if($_SESSION["moneyPaid"] == 'By Cash')
				{
				
						$totalAmount = ( $totalAmount +  $imFee);
						$imFeetemp = ($imFee);		
						if(CONFIG_CASH_PAID_CHARGES_ENABLED == "1")
						{
							$totalAmount = ( $totalAmount +  CONFIG_CASH_PAID_CHARGES);
							}			
										
				}
				elseif($_SESSION["moneyPaid"] == 'By Cheque')
				{
					$totalAmount = ($totalAmount +  $imFee);
					$imFeetemp = ($imFee);
							
				}
				else//if($_SESSION["moneyPaid"] == 'By Bank Transfer')
				{		 
			  
					$totalAmount = ($totalAmount + $imFee);
					$imFeetemp = ($imFee);
				}
				
				 // This config added by Niaz Ahmad at 07-05-2009 @4926 for Faith Exchange
					   if(CONFIG_DISTRIBUTOR_BANK_CHARGES == "1"){
					     $bankChargesQuery = selectFrom("select bankCharges from admin where userID='".$dist."'");
					     $bankCharges = $bankChargesQuery[bankCharges];
						if(!empty($bankCharges))
							$_SESSION["bankCharges"] =  $bankCharges;
						else
							$_SESSION["bankCharges"] = 0;
					   }
				if($_SESSION["transType"]=="Bank Transfer" || CONFIG_DISTRIBUTOR_BANK_CHARGES == "1")
				{		 
					$totalAmount = ( $totalAmount + $_SESSION["bankCharges"]);
					
				}
				
			  if($_SESSION["transAmount"] != "" && $_SESSION["chDiscount"]=="")
			  {
						  if(CONFIG_ZERO_FEE != '1' && $imFee <= 0)
							{
			  ?>
                    <TABLE WIDTH="100%" BORDER="0" CELLPADDING="5" CELLSPACING="0" BGCOLOR="#EEEEEE">
                      <TR>
                        <TD WIDTH="40" ALIGN="center"><FONT SIZE="5" COLOR="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><B><I><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></I></B></FONT></TD>
                        <TD WIDTH="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".TE2."</b</font>"; ?></TD>
                      </TR>
                    </TABLE>                    
                    <?
                }
			  }
}

}else{ 
	
	$msgWrongTotal = "";
	/////Condition OF Calculation from Total Starts Here///////////
	if($_POST["totalAmount"] != '')
	{
		
		$totalAmount = $_POST["totalAmount"];
	}
	$totalAmountTemp = $totalAmount;
	/////////////////Bank Charges///////////////
	
	if($totalAmount > 0)
	{
	
		if($_SESSION["transType"]=="Bank Transfer")
		{
			if(CONFIG_CUSTOM_BANK_CHARGE == '1')
				{
					$_SESSION["bankCharges"] = getBankCharges($customerContent["Country"], $benificiaryContent["Country"], $_SESSION["currencyFrom"], $_SESSION["currencyTo"], $totalAmountTemp, $dist);
				}
					$totalAmountTemp = ( $totalAmountTemp - $_SESSION["bankCharges"]);
		}
			
		/////////////////////////////Cash Charges
		if($_SESSION["moneyPaid"] == 'By Cash')
		{
	 		if(CONFIG_CASH_PAID_CHARGES_ENABLED == "1")
			{
				$totalAmountTemp = ( $totalAmountTemp -  CONFIG_CASH_PAID_CHARGES);
			}			
								
		}
		
		////////////////////////////Outer Currency Charges////////////
		
		if(CONFIG_CURRENCY_CHARGES == '1')
		{
			$local = "No";
			$_SESSION["currencyCharge"]="";
			 $strCurrency = "Select * from ".TBL_COUNTRY." where  currency like '%".$_SESSION["currencyTo"].",%'";
			$currencies = selectMultiRecords($strCurrency);
			for ($c=0; $c < count($currencies); $c++)
			{
				if(strtoupper($currencies[$c]["countryName"]) == strtoupper($benificiaryContent["Country"]))
				$local = "Yes";
			}
			
			if($local=="No")
			{
				///////Out Currency Charges are working on Per 100 Amount
				$tempCharge = $totalAmountTemp/100.01;
				$tempCharge2 = (int)$tempCharge;
				$tempCharge2 = $tempCharge2 + 1;
				$tempCharge2 = $tempCharge2 * $countryContent["outCurrCharges"];
				$_SESSION["currencyCharge"] = $tempCharge2; 
				$totalAmountTemp = $totalAmountTemp - $_SESSION["currencyCharge"];
			}
			
		}
		

		
		///////////////////Calculation of Fee Starts for $fromTotalConfig == "1" //////////////////
		/*********
			#5047 NT
			These lines of code should be taken after handling to getAmounts.php for AJAX based fee logic for $fromTotalConfig=="1"..
		*********/
			if ($chDiscount != ""){ 
					$imFee = $discount;
				}elseif(CONFIG_TOTAL_FEE_DISCOUNT == '1' && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType)) && $_SESSION["discount_request"] > 0){
					$imFee = 0 ;
						 //discount_request
					}
					elseif(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")
					{
						
						if ($_SESSION["IMFee"] != "")
				 		{
				 			$imFee	= $_SESSION["IMFee"];
				 			$_SESSION["IMFee"] = "";
				 		}
				 		elseif ($_POST["IMFee"] != "")
				 		{
				 			$imFee	= $_POST["IMFee"];
				 		}
						
					   $bolExecuteIMFeeCondition = false;
					   if(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")
					   {
						 // Added by Niaz Ahmad at 29-04-2009 @4812 faith exchange zero commission
							if(CONFIG_ZERO_FEE == "1")
								$bolExecuteIMFeeCondition = true;
							else
							{
								if(!empty($_REQUEST["IMFee"]))
									$bolExecuteIMFeeCondition = true;
							}
					   }
					   
					   
						if(CONFIG_FEE_BASED_TRANSTYPE == "1" && defined("CONFIG_COMMISSION_CALCULATED_ON"))
						{
							$amountType=$_SESSION[CONFIG_COMMISSION_CALCULATED_ON];
							$feetransType=$_SESSION["transType"];
												
						}
						else
						{
							$amountType=$_SESSION["transAmount"];
							$feetransType=$_SESSION["transType"];
						}  
					   
						$imFee = 0;
						if ($_SESSION["chDiscount"]!="")
						{ 
							
							$imFee = $_SESSION["discount"];
						}
						elseif(CONFIG_TOTAL_FEE_DISCOUNT == '1' && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType)) && $_SESSION["discount_request"] > 0 )
						{
							$imFee = 0 ;
							 //discount_request
						}
						//elseif(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1" && !empty($_REQUEST["IMFee"]))
						elseif($bolExecuteIMFeeCondition)
						{
							if(empty($_REQUEST["transID"]) && ( $_SESSION["manualCommission"] == "N" || empty($_REQUEST["manualCommission"])))
							{
								$_SESSION["IMFee"] = "";
								$_POST["IMFee"] = "";
								
							}
							
							if($_SESSION["IMFee"] != "")
							{
								$imFee	= $_SESSION["IMFee"];
								$_SESSION["IMFee"] = "";
							}
							elseif(!empty($_POST["IMFee"]) || (CONFIG_ZERO_FEE == "1" && $_SESSION["manualCommission"] == "Y"))
							{
								$imFee	= $_POST["IMFee"];
							}
							else
							{
								if(CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1)
									$imFee = imFeeAgent($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["senderAgentID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
								 
								if($imFee <= 0)
									$imFee = imFee($amountType, $customerContent["Country"], $benificiaryContent["Country"],$feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"],$_SESSION["collectionPointID"],$dist);	
							}
							
						}
						else
						{
							if(CONFIG_PAYIN_CUSTOMER != '1')
							{
								if(CONFIG_FEE_BASED_COLLECTION_POINT == 1 && !empty($_SESSION["collectionPointID"]))
								{
									$imFee = imFeeAgent($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["collectionPointID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
									
								}
								
								if(CONFIG_FEE_DISTRIBUTOR == 1)
								{
									$imFee = imFeeAgent($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $dist, $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
									
								}elseif(CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1)
								{
								 $imFee = imFeeAgent($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["senderAgentID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
								 
								}
								
								if($imFee <= 0)
								{ 
									
									
									$imFee = imFee($amountType, $customerContent["Country"], $benificiaryContent["Country"],$feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"],$_SESSION["collectionPointID"],$dist);	
									
								}
									
							}else
							{	
							
								if(CONFIG_FEE_BASED_COLLECTION_POINT == 1 && !empty($_SESSION["collectionPointID"]))
								{
									if($customerContent["payinBook"] != "" )
										$imFee = imFeeAgentPayin($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["collectionPointID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
									else
										$imFee = imFeeAgent($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["collectionPointID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
								}
								
								if(CONFIG_BACKDATED_FEE == '1'){
									$feeQuery = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$_GET["transID"]."'");
									if($_GET["transID"] != "" && $_SESSION["transAmount"] == $feeQuery["transAmount"]){
										$imFee = $feeQuery["IMFee"];
									}
								}elseif(CONFIG_FEE_DISTRIBUTOR == '1')
								{
									
									
									if($customerContent["payinBook"] != "" )
									{
										$imFee = imFeeAgentPayin($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $dist,$feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
									
									}else{
										$imFee = imFeeAgent($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $dist, $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
									
									}
											 
								}elseif(CONFIG_FEE_AGENT_DENOMINATOR == '1' || CONFIG_FEE_AGENT == '1')
								{
									
									
									if($customerContent["payinBook"] != "" )
									{
										$imFee = imFeeAgentPayin($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["senderAgentID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
									
									}
									else{ 
										$imFee = imFeeAgent($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["senderAgentID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
									
									}
							
								}
								if($imFee == 0)
								{	
														
									if($customerContent["payinBook"] != "" )
									{
										$imFee = imFeePayin($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
										
									}
									else
									{
										$imFee = imFee($amountType,  $customerContent["Country"], $benificiaryContent["Country"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"],$_SESSION["collectionPointID"],$dist);
										
									}
								}
						
						}
					 }
						
					}
		///////////////////Calculation of Fee Ends for $fromTotalConfig == "1" //////////////////
					if($customerContent["payinBook"] != "" )
					{
						$payinBook = true;
					}else{
						$payinBook = false;
						}
			
				$FeeData = CalculateFromTotal($imFee, $totalAmountTemp , $customerContent["Country"], $benificiaryContent["Country"], $dist, $_SESSION["senderAgentID"],$payinBook, $_SESSION["transType"], $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);
					
				 $transAmount = $FeeData[0];
				 $imFee = $FeeData[1];
				
				/*
				if ($transAmount != "" && $imFee != "") {
				 	$validTranAmount = selectFrom("select amountRangeFrom from " . TBL_IMFEE . " where Fee = '".$imFee."' and destCountry='".$benificiaryContent["Country"]."' and origCountry='".$customerContent["Country"]."'");
				 	if ($transAmount < $validTranAmount["amountRangeFrom"]) {
				 		unset($transAmount);
				 		unset($imFee);
				 		$msgWrongTotal = "The Total Amount you entered is invalid. Please consult the fee table.";
				 	}
				}
				*/
				//echo("at line 2334  values are ".$transAmount."  and  ".$_SESSION["transAmount"]);
				//$_SESSION["transAmount"] = $transAmount;
				//echo("at line 2336  values are ".$transAmount."  and  ".$_SESSION["transAmount"]);
				///////////////////////Calculation of Exchange Rates//////////
	
		/*********
			#5047 NT
			These lines of code should be taken after handling to getAmounts.php for AJAX based exchange rate logic for $fromTotalConfig=="1"..
		*********/
				if(CONFIG_EXCH_RATE_NOT_EDIT == '1' && $_GET["transID"] != "")
				{
					
					$exRateUsed = selectFrom("select rateUsedID, rateWithMargin, currencyFrom , currencyTo from ".TBL_EXCH_RATE_USED." where transID  = '".$_GET["transID"]."'");
					
					$exRate = $exRateUsed["rateWithMargin"];
					$currencyFrom = $exRateUsed["currencyFrom"];
				$currencyTo 	= $exRateUsed["currencyTo"];
					
				}elseif ((CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")  || (CONFIG_AGENT_OWN_MANUAL_RATE == '1' && $_SESSION["checkManualRate"] == 'Y') || $_GET["enterOwnRate"] =="Y")
			 	{
						$bolGetRate = true;
			 			$exID = 0;
			 			//debug($_REQUEST);
						if(defined("CONFIG_DENOMINATION_BASED_EXCHANGE_RATE") && CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1")
						{
							$_SESSION["exchangeRate"] = "";
														
							if(CONFIG_AGENT_OWN_MANUAL_RATE == '1' && strstr(CONFIG_MANUAL_RATE_USERS, $agentType.","))
							{
								if($_REQUEST["checkManualRate"]=="Y")
									$bolGetRate = false;
								else//if(empty($_REQUEST["enterOwnRate"]) || $_REQUEST["enterOwnRate"]=="N")
								{
									$bolGetRate = true;
									$_POST["exchangeRate"] = "";	
								}
							}
							else
								$bolGetRate = true;
								
							if(!empty($_REQUEST["transID"]))
							{
								if($_SESSION["checkManualRate"]=="Y")
									$bolGetRate = false;
								else
									$bolGetRate = true;
							}
						}
				
			 			if ($_SESSION["exchangeRate"] != "" && $_SESSION["exchangeRate"] != 0)
			 			{
							$exRate	= $_SESSION["exchangeRate"];
			 				$_SESSION["exchangeRate"] = "";
			 			}
			 			elseif($_POST["exchangeRate"] != "" && $_POST["exchangeRate"] != 0)
			 			{
			 				$exRate	= $_POST["exchangeRate"];
			 			}
			 			else
			 			{
			 				//$exRate = 0;
							if(CONFIG_24HR_BANKING == "1" && $_SESSION["bankingType"] == "bank24hr")
							{
		 	
					 			$exchangeData = getMultipleExchangeRates($customerContent["Country"], $benificiaryContent["Country"],$_SESSION["currencyTo"], $_SESSION["bankingType"], 0 , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"]);
								$exID 			  = $exchangeData[0];
								$exRate 		  = $exchangeData[1];	
								$currencyFrom = $exchangeData[2];
								$currencyTo 	= $exchangeData[3];	
						 		$_SESSION["currencyFrom"] = $currencyFrom;
						 	}
							elseif($bolGetRate)
							{	
				 				$exchangeData = getMultipleExchangeRates($customerContent["Country"], $benificiaryContent["Country"],$_SESSION["currencyTo"], $dist, $_SESSION["transAmount"] , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"],$_SESSION["senderAgentID"]);
								
								$exID 			  = $exchangeData[0];
								
								
								/* Check the denomination based exchange rate */
								$arrExchangeRate = selectFrom("select * from exchangerate where erID='".$exID."'");			
								if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1" && $arrExchangeRate["isDenomination"] == "Y")
								{
									if($arrExchangeRate["isDenomination"] == "Y")
									{
										$arrDenominationBaseRate = selectFrom("select * from denominationBasedRate where parentRateId='".$arrExchangeRate["erID"]."' and amountFrom <= '".$_SESSION["transAmount"]."' and amountTo >= '".$_SESSION["transAmount"]."'");
										if(is_array($arrDenominationBaseRate))
										{
											$exRate = $arrDenominationBaseRate["exchangeRate"];
										}
										else
										{
											/* error .. not define proper exchange rate */
										}
										
									}
									else	
									{
										$exRate = $exchangeData[1];	
									}	
								}
								else
								{
									$exRate 		  = $exchangeData[1];	
								}
								
								/**
								 * Get the exchange rate DB value only in case if the session value is empty
								 * @Ticket# 3524
								 */
								if(!defined("CONFIG_DENOMINATION_BASED_EXCHANGE_RATE")) 
									if(empty($_SESSION["exchangeRate"]))
										$exRate = $exchangeData[1];	
								/* End of # 3524 */
									
								$currencyFrom = $exchangeData[2];
								$currencyTo 	= $exchangeData[3];
								
								$_SESSION["currencyFrom"] = $currencyFrom;
			 				}
			 				
			 			}
			 	}elseif(CONFIG_MULTI_RATE_PATTERN == '1')
			 	{
			 		$exID = 0;
			 		$exRate = getMultiPatternRate($customerContent["Country"], $benificiaryContent["Country"], $_SESSION["currencyTo"], $dist, 0, $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"], 'Yes');
			 		$currencyFrom = $_SESSION["currencyFrom"];
					$currencyTo 	= $_SESSION["currencyTo"];
			 	}elseif(CONFIG_24HR_BANKING == "1" && $_SESSION["bankingType"] == "bank24hr"){
		 	
		 			$exchangeData = getMultipleExchangeRates($customerContent["Country"], $benificiaryContent["Country"],$_SESSION["currencyTo"], $_SESSION["bankingType"], 0 , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"]);
					$exID 			  = $exchangeData[0];
					$exRate 		  = $exchangeData[1];	
					$currencyFrom = $exchangeData[2];
					$currencyTo 	= $exchangeData[3];	
			
			 	}else{
			 	
			
			 		$exchangeData = getMultipleExchangeRates($customerContent["Country"], $benificiaryContent["Country"],$_SESSION["currencyTo"], $dist, $_SESSION["transAmount"] , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"]);
				
				
				
				
				$exID 			  = $exchangeData[0];
				//$_SESSION["currencyFrom"] = $currencyFrom;
				$currencyFrom = $exchangeData[2];
				$currencyTo 	= $exchangeData[3];

				$arrExchangeRate = selectFrom("select * from exchangerate where erID='".$exID."'");			
				
				//debug($arrExchangeRate);

								
				if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1" && $arrExchangeRate["isDenomination"] == "Y")
				{
								
					if($arrExchangeRate["isDenomination"] == "Y")
					{
						$arrDenominationBaseRate = selectFrom("select * from denominationBasedRate where parentRateId='".$arrExchangeRate["erID"]."' and amountFrom <= '".$_SESSION["transAmount"]."' and amountTo >= '".$_SESSION["transAmount"]."'");
						
						//debug("select * from denominationBasedRate where parentRateId='".$arrExchangeRate["erID"]."' and amountFrom <= '".$_SESSION["transAmount"]."' and amountTo >= '".$_SESSION["transAmount"]."'");
						
						//debug($arrDenominationBaseRate);
						
						if(is_array($arrDenominationBaseRate))
						{
							$exRate = $arrDenominationBaseRate["exchangeRate"];
						}
						else
						{
							/* error .. not define proper exchange rate */
						}
						
					}
					else	
					{
						$exRate = $exchangeData[1];	
					}	
				}
				else
				{
					$exRate 		  = $exchangeData[1];	
				}
				$_SESSION["currencyFrom"] = $currencyFrom;
			}
				///////////////////	Calculating Local Amount
				if($_SESSION["transAmount"] != "")
				{
				 $localAmount = getCalculatedAmounts($transAmount, $operatorMult, $exRate); //$operatorMult may be * or /
				//debug($transAmount);
				//debug($localAmount);
				 	if(CONFIG_ROUND_NUMBER_ENABLED == '1')
					{
						$localAmount = round($localAmount);		
					}
					
				 $_SESSION["localAmount"] = $localAmount;
				}
			
				if ($msgWrongTotal != "") {
					$localAmount = "";
					$_SESSION["localAmount"] = "";
				}
				
					
					/////////////////////Error Message if Fee Doesn't Exist/////////
					if($_SESSION["transAmount"] != "")
					  {
						if(CONFIG_ZERO_FEE != '1' && $imFee <= 0)
						{
					  	?>
		                    <TABLE WIDTH="100%" BORDER="0" CELLPADDING="5" CELLSPACING="0" BGCOLOR="#EEEEEE">
		                      <TR>
		                        <TD WIDTH="40" ALIGN="center"><FONT SIZE="5" COLOR="#990000"><B><I>!</I></B></FONT></TD>
		                        <TD WIDTH="635"><? echo "<font color='#990000'><b>".TE2."</b</font>"; ?></TD>
		                      </TR>
		                    </TABLE>                    
		                <?
		                }
		            }
			}
			if ($msgWrongTotal != "") {
			?>
		                    <TABLE WIDTH="100%" BORDER="0" CELLPADDING="5" CELLSPACING="0" BGCOLOR="#EEEEEE">
		                      <TR>
		                        <TD WIDTH="40" ALIGN="center"><FONT SIZE="5" COLOR="#990000"><B><I>!</I></B></FONT></TD>
		                        <TD WIDTH="635"><? echo "<font color='#990000'><b>".$msgWrongTotal."</b</font>"; ?></TD>
		                      </TR>
		                    </TABLE>                    
			<?	
			}		
		}
		/***************
		*
		*Uptill here is the logic to calculate fee and exchange rate etc
		*
		****************/
		
		/***********
		*To Add the Limit
		*
		*if  CONFIG_LIMIT_TRANS_AMOUNT is 1
		************/	
		$messageError = "";
		
			if(CONFIG_UNLIMITED_EXCHANGE_RATE == '1' &&  (strstr(CONFIG_UNLIMITED_EXRATE_COUNTRY, strtoupper($_POST["benCountry"].",")) || CONFIG_UNLIMITED_EXRATE_COUNTRY=="1"))
			{
				//debug($limitedFlag);
				
			}else{	
					if(CONFIG_AGENT_OWN_RATE == '1' || (CONFIG_AGENT_OWN_MANUAL_RATE == '1' && $_POST["checkManualRate"] != '') || (CONFIG_AGENT_RATE_MARGIN == '1'))
				  {
				  	//echo("Config for comparison is added");
				  	/////
				  	$packageQuery="select userID, username, parentID, agentType, isCorrespondent, commPackage, agentCommission, rateCountries, unlimitedRate 
				  	from ". TBL_ADMIN_USERS." where userID = '".$_SESSION["senderAgentID"]."'";
						$agentPackage = selectFrom($packageQuery);
						$limitedFlag = true;
						if(CONFIG_UNLIMITED_EX_RATE_OPTION == '1' && $agentPackage["unlimitedRate"] == "Y")
						{
							if(strstr($agentPackage["rateCountries"], $benificiaryContent["Country"]))
							{
									$limitedFlag = false;
							}
						}
						//debug($limitedFlag);
						if($limitedFlag)
						{
								if($agentPackage["agentType"] == 'Sub')
								{
									$commUser = $agentPackage["parentID"];	
								}else{
									$commUser = $agentPackage["userID"];	
								}
								
								if(CONFIG_USER_COMMISSION_MANAGEMENT == '1'){
									 
									$agentCommiArray = recordAgentCommissionCountrySpecific($commUser, $transAmount, $imFee, "Agent", $benificiaryContent["Country"]);
								  $agentCommi = $agentCommiArray[0];
									$commType = $agentCommiArray[1];
									$agentParentID = $agentPackage["parentID"];
									$package = $agentPackage["commPackage"];
								}else{
								 	$agentCommiArray = recordAgentCommission($commUser, $transAmount, $imFee, "Agent");
									$agentCommi = $agentCommiArray[0];
									$commType = $agentCommiArray[1];
									$agentParentID = $agentPackage["parentID"];
									$package = $agentPackage["commPackage"];
								
									
								}
						  	
						  	/**
							 * Fix the bug that in exchange rate calculation
							 * the 24 hr banking rates ingnored
							 * @Ticket #3337
							 */
							if(CONFIG_24HR_BANKING == "1" && $_SESSION["bankingType"] == "bank24hr")
								$dist = $_SESSION["bankingType"];
								
						  	$exchangeData = getMultipleExchangeRates($customerContent["Country"], $benificiaryContent["Country"],$_SESSION["currencyTo"], $dist, 0 , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"]);
							$exID2 			  = $exchangeData[0];
							//$exRate2  = $exchangeData[1];		
						
						$arrExchangeRate = selectFrom("select * from exchangerate where erID='".$exchangeData[0]."'");			
						//debug($arrExchangeRate);
							
						if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1" && $arrExchangeRate["isDenomination"] == "Y")
						{
							
							if($arrExchangeRate["isDenomination"] == "Y")
							{
								$arrDenominationBaseRate = selectMultiRecords("select * from denominationBasedRate where parentRateId='".$arrExchangeRate["erID"]."' and amountFrom <= '".$_SESSION["localAmount"]."' and amountTo >= '".$_SESSION["localAmount"]."'");
								
								//debug("select * from denominationBasedRate where parentRateId='".$arrExchangeRate["erID"]."' and amountFrom <= '".$_SESSION["localAmount"]."' and amountTo >= '".$_SESSION["localAmount"]."'");
								
								//debug($arrDenominationBaseRate);
								
								if(is_array($arrDenominationBaseRate))
								{
									$exRate2 = $arrDenominationBaseRate["exchangeRate"];
														//debug($transAmount);
									$actualLocal = getCalculatedAmounts($transAmount, $operatorMult, $exRate2); //$operatorMult may be * or /
								}
								else
								{
									/* error .. not define proper exchange rate */
								}
								
							}
							else	
							{
								
								$exRate2  = $exchangeData[1];	
													//debug($transAmount);
								$actualLocal = getCalculatedAmounts($transAmount, $operatorMult, $exRate2); //$operatorMult may be * or /
							}	
						}
						else
						{
							//debug($exchangeData);
							$exRate2  = $exchangeData[1];	
												//debug($transAmount);
							$actualLocal = getCalculatedAmounts($transAmount, $operatorMult, $exRate2); // $operatorMult may be * or /
						}		
					 		  
							  
							  $calculatedLocal = $localAmount;
					 		  
					 		 if($calculatedLocal >= $actualLocal)
							 {
							 		$localDiff = $calculatedLocal - $actualLocal;

							 		if($exRate2 != 0)
										$actualDiff = round(($localDiff / $exRate2),$roundLevel); 	
										
						 		 if($actualDiff > $agentCommi && CONFIG_UNLIMITED_EXRATE_COUNTRY!="0")
								 {
									$maxAvailable = 0;
									
									if($transAmount != 0)
									{
														//debug($transAmount);
									 	$maxAvailable = (($exRate2 * $agentCommi)+($actualLocal))/$transAmount;
								 	
									 	$messageError = "Maximum Exchange Rate can be ".round($maxAvailable,2);
									//$onErroCode = "onKeyUp=\"$('#sendID').attr('disabled','disabled');\"";
										//$messageError = "Maximum Exchange Rate can be ".$maxAvailable;
									}

								 }
							 }	
						}////Limit Flag
				  }////Configs Condition
			}

		if(CONFIG_LIMIT_TRANS_AMOUNT == '1')
		{
			$limitObject = new TransLimit;	
			//echo("Values to be sent to check");
			$limitCheckData[] = $_SESSION["loggedUserData"]["userID"];
			$limitCheckData[] = $totalAmount;
			$limitCheckData[] = $_SESSION["currencyFrom"];
			$value = $limitObject-> calculateLimit($limitCheckData);
			if(!$value)
			{
				$messageError = "You have Exceeded your Limit";
			 
			}
	
			
		}
		if($messageError != "")
		{
		?>
			<TABLE WIDTH="100%" BORDER="0" CELLPADDING="5" CELLSPACING="0" BGCOLOR="#EEEEEE">
	          <TR>
                <TD WIDTH="40" ALIGN="center"><FONT SIZE="5" COLOR="#990000"><B><I>!</I></B></FONT></TD>
				<TD WIDTH="635"><? echo "<font color='#990000'><b>".$messageError."</b</font>"; ?></TD>
	
              </TR>
            </TABLE>   
            
			<?
			//$messageError = "";
		}

?>
		        		          	<TABLE WIDTH="700" BORDER="0" CELLPADDING="2" CELLSPACING="0">
		        		          	<? if(CONFIG_TEST_TRANS_LOAD == '1'){?>
		        		          	<TR>
			        		          	<TD WIDTH="550" COLSPAN="3"><FONT COLOR="#FF0000"><STRONG>
								              		
								              			System is configured to check Load, Please change settings before actual transactions              			
								              			</STRONG></FONT>
							              	</TD>            
		        		          	</TR>
		        		          <? }?>
		        		          	<?  
		        		          	
		        		          	
		        		          	$queryJointDistributor = "select fromServer  from " .TBL_ADMIN_USERS. " where  userID  ='" . $_SESSION["benAgentID"] . "'";
																$jointDistExecute = selectFrom($queryJointDistributor);	
		        		          	
		        		          	if(CONFIG_BACK_DATED == '1')
		         								{
		         									
		        		          	if(CONFIG_SHARE_OTHER_NETWORK != '1' || (CONFIG_SHARE_OTHER_NETWORK == '1' && CONFIG_DISABLE_BACK_DATED_TRANS_FOR_REMOTE_DIST == "1" && $jointDistExecute["fromServer"]== "")){ 
		        		          		
										
		        		          				        		          		 
		          								if((strstr($userDetails["rights"], "Backdated")) || ($userDetails["parentID"]== "0" && $agentType == "admin" )) {
		          									
		          									?>

					                      <TR>
					                        <TD WIDTH="128" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Back Date</FONT></TD>
					                        <td height="20" width="195"><input name="dDate" type="text" id="dDate" value="<? echo $_SESSION["transDate"];?>" readonly>&nbsp;<a href="<? if (strstr($countryContent["serviceAvailable"], "Cash Collection") && $_SESSION["transType"] == "Pick up" && $senderAgentContent["cp_id"] == "") { ?>javascript:alert('Please select the collection point first.');<? } else { ?>javascript:show_calendar('addTrans.dDate');<? } ?>" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=34 height=20 border=0 title="Back Date|Select the back date from pop up." /></a>&nbsp;
					                        <input  type="button" name="Submit3" value=" Reset Date " onClick="resetDate();"><input type="hidden" name="manageTransDate" id="manageTransDate" value=""></td>

					                      <? } else { ?>
											<tr>
												<td colspan="2" width="323">&nbsp;</td>		
										   <? } 
					                    } else { ?>
											<tr>
												<td colspan="2" width="323">&nbsp;</td>		
										<? } 
				                    	} else {
										?>
										<tr>
											<td colspan="2" width="323">&nbsp;</td>		
										<? } ?>
                    	
										<? if(CONFIG_MONEY_PAID_LOCATION == "TOP") { ?>
												<td width="128" height="20" align="right">
													<font color="#005b90">Money Paid</font>
													<? if(CONFIG_MONEY_PAID_NON_MANDATORY != "1"){?>
														<font color="#ff0000">*</font>
													<? }?>
												</td>
												<td width="195" height="20">
												<? 
												if( CONFIG_CALCULATE_BY == '1')
												{ 
													echo "<input type='text' name='moneyPaid' id='moneyPaid' value='";
													
													if($_SESSION["calculateBy"] == "inclusive" )
														if ( CONFIG_MODIFY_BYBANKTRANSFER_TO_INCLUSIVEOFFEE == "Inclusive of Fee" )
															echo CONFIG_MODIFY_BYBANKTRANSFER_TO_INCLUSIVEOFFEE;
														else
															echo "By Bank Transfer";
													elseif($_SESSION["calculateBy"]== 'exclusive')
														if ( CONFIG_MODIFY_BYCASH_TO_EXCLUSIVEOFFEE == "Exclusive of Fee" )
															echo CONFIG_MODIFY_BYCASH_TO_EXCLUSIVEOFFEE;
														else
															echo "By Cash";
													
													echo "' maxlength='25' readonly>";
												}
												else
												{ 
												?>
													<select name="moneyPaid" id="moneyPaid"  onChange="document.addTrans.action='add-transaction.php?transID=<?=$_GET["transID"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();" >
														<option value="">-select one-</option>
														<? if(CONFIG_MONEY_PAID_OPTIONS == "1"){ ?>
															<option value="By Cash HCD">By Cash HCD</option>
															<option value="By Cash HBT">By Cash HBT</option>
															<option value="By Cash HQD">By Cash HQD</option>
															<option value="By Cash BCD">By Cash BCD</option>
															<option value="By Cash BBT">By Cash BBT</option>
															<option value="By Cash BQD">By Cash BQD</option>
															<option value="By Cash WICC">By Cash WICC</option>
														<? } ?>
													<?php if(!strstr(CONFIG_MONEY_PAID_HIDE_VALUES,"By Cash,")){?>
														<option value="By Cash">By Cash</option>
													<?php }?>
													<?php if(CONFIG_DISPLAY_MONEY_PAID_BY_CHEQUE != false && !strstr(CONFIG_MONEY_PAID_HIDE_VALUES,"By Cheque,")){?>
														<option value="By Cheque">By Cheque</option>
													<?php }?>
													<?php if(!strstr(CONFIG_MONEY_PAID_HIDE_VALUES,"By Bank Transfer,")){?>
														<option value="By Bank Transfer">By Bank Transfer</option> 
													<?php }?>
														<? if(CONFIG_MONEYPAID_ONCREDIT == "1" && !strstr(CONFIG_MONEY_PAID_HIDE_VALUES,"By Credit,")) { ?>
															<option value="On Credit">On Credit</option>
														<? } ?>
														<? 
														/**
														 * @Ticket# 3564
														 */
														if(CONFIG_MONEY_PAID_OPTION_BY_CARD == 1 && !strstr(CONFIG_MONEY_PAID_HIDE_VALUES,"By Card,")) { 
														?>
															<option value="By Card">By Card</option>
														<? } ?>
													</select>						
												<? 
												}
												?>
												<script language="JavaScript">
													SelectOption(document.addTrans.moneyPaid, "<?=$_SESSION["moneyPaid"]?>");
												</script>
											</td>
											<? } else { ?>
												<td colspan="2">&nbsp;</td>
											<? } ?>
                             			</tr>    
            
                    
               <?
              
              if(CONFIG_CALCULATE_BY == '1'){ ?>
  <TR>
    <TD WIDTH="128" HEIGHT="20" ALIGN="right"><font color="#005b90">Calculate :</TD>
    	<TD WIDTH="195" HEIGHT="20">
    											<SELECT NAME="calculateBy" ID="calculateByID" onChange="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();">
                          		<?=(CONFIG_MODIFY_BYCASH_TO_EXCLUSIVEOFFEE == "Exclusive of Fee")? "<option value='exclusive'>" . CONFIG_MODIFY_BYCASH_TO_EXCLUSIVEOFFEE . "</option>":"<option value='exclusive'>By Cash</option>"?>
                          		<?=(CONFIG_MODIFY_BYBANKTRANSFER_TO_INCLUSIVEOFFEE == "Inclusive of Fee")? "<option value='inclusive' selected>" . CONFIG_MODIFY_BYBANKTRANSFER_TO_INCLUSIVEOFFEE . "</option>":"<option value='inclusive' selected>By Bank Transfer</option>"?>
                          		
                          </SELECT>
					<SCRIPT LANGUAGE="JavaScript">
         	SelectOption(document.addTrans.calculateBy, "<?=$_SESSION["calculateBy"]?>");
                                </SCRIPT></TD>
    </TR>
<? }
       
            		
            		/***********
            		*	This page will swap the amount and local amount up and down
            		* With above, currency from and currency to are also swapped
            		* For this purpose, there is a config variable CONFIG_SWAP_AMOUNT_LOCALAMOUNT
            		* [by Jamshed]
            		*************/
					if($manualCommAjaxFlag && $_SESSION["manualCommission"]!=""){
?>
						<INPUT TYPE="hidden" NAME="manualCommissionAJAX" ID="manualCommissionAJAX" VALUE="<? echo $_SESSION["manualCommission"];?>">
<?			            
					}
            		include "swap-amount-local.php";
            	
            	?>
						<INPUT TYPE="hidden" NAME="benCountry" ID="benCountryID" VALUE="<? echo $benCountryName;?>">
						<INPUT TYPE="hidden" NAME="custCountry" ID="custCountryID" VALUE="<? echo $customerContent["Country"];?>">
						 
						 <? 
						 	if($_SESSION["chDiscount"]=="")
							{ 
							
								$onBlurCommissionCode = "";
								if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1")
									$onBlurCommissionCode = "onKeyUp=\"$('#sendID').attr('disabled','disabled');\"";
								if($manualCommAjaxFlag){
									$onBlurCommissionCode = "";
								}
								
								$chkYesCommissionRate = "";
								$chkNoCommissionRate = "";
								$manulCommissionRate = $_SESSION["manualCommission"];
								if($manulCommissionRate == "Y")
									$chkYesCommissionRate = ' checked="checked"';
								else
								{
									$chkNoCommissionRate = ' checked="checked"';
									$readonlyFlag2 = 'readonly';
								}
						?>
						
						  <? 
						    // Added by Niaz Ahmad at 29-04-2009 @4809 manual fee only view supper admin (faith exchange)
						if (CONFIG_MANUAL_FEE_RATE == "1" &&  $flagBenCountry == "1" && CONFIG_MANUAL_FEE_RATE_SPECIFIC_USERS == "1")
						{
						  	$manualFeeUsersList = explode(",",CONFIG_MANUAL_FEE_RATE_USERS);
							//debug($manualFeeUsersList);
						for ($i = 0; $i < count($manualFeeUsersList); $i++)
						{
						//debug(strtoupper($benificiaryContent["Country"])." == ".$countryList[$i]);
							if ($agentType == $manualFeeUsersList[$i])
							{
								$flagManualFeeUsers = 1;	
								//debug($manualFeeUsersList[$i]);
							}	
						}
					}
					 ?>
						
						<?php
					$manualFeeFlag = false;
					$manualFeeCountryFlag = false;
					$manualFeeUsersFlag = false;
					$normalFeeFlag = false;
					if(CONFIG_MANUAL_FEE_RATE_SPECIFIC_USERS != "1" && CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1"){
						$manualFeeFlag = true;
						$manualFeeCountryFlag = true;
					}
					elseif(CONFIG_MANUAL_FEE_RATE_SPECIFIC_USERS == "1" && $flagManualFeeUsers == "1"){
						$manualFeeFlag = true;
						$manualFeeUsersFlag = true;
					}
					if(CONFIG_NORMAL_FEE_TRANS_HIDE=="1"){
						$normalFeeFlag = true;
					}
					else{
						$manualFeeFlag = true;
					}
						?>
                       <?php if($manualFeeFlag){ ?>
						  <TR id=Fee>
                            <TD WIDTH="150" HEIGHT="20" ALIGN="right">
							    <? if(CONFIG_MANUAL_FEE_RATE_SPECIFIC_USERS != "1"){ ?>
								<? if($manualFeeCountryFlag) { ?>
									<FONT COLOR="#005b90">Manual Commission</font>
								<? 
								   }
								  }elseif($manualFeeUsersFlag){
								 ?>
								   	<FONT COLOR="#005b90">Manual Commission</font>
								 <? } ?>
								<FONT COLOR="#005b90">
									<? 
									if(!$normalFeeFlag && ($manualFeeUsersFlag || $manualFeeCountryFlag))
										echo '<br /><br />';
										if(CONFIG_FEE_DEFINED == '1')
											echo(CONFIG_FEE_NAME);
										else
											echo ("$systemPre Fee");
									    echo (($manualFeeCountryFlag) ? "<font color='#ff0000'>*</font>" : "");
								    ?>
								</FONT>
							</TD>
                            <TD WIDTH="200">
							  <? if(CONFIG_MANUAL_FEE_RATE_SPECIFIC_USERS != "1"){ ?>
								<? if($manualFeeCountryFlag) { ?>
								<font color="#005b90">
		  			&nbsp; &nbsp; Yes<input type="radio" name="manualCommission" id="manualCommission" value="Y" onchange="document.addTrans.action='<?=$transactionPage?>?transID=<?=$_REQUEST["transID"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();" <?=$chkYesCommissionRate?>>
		  				No<input type="radio" name="manualCommission" id="manualCommission" value="N" onchange="document.addTrans.action='<?=$transactionPage?>?transID=<?=$_REQUEST["transID"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();" <?=$chkNoCommissionRate?>>
		  				</font>
								<? 
								    }
								  }elseif($manualFeeUsersFlag){
								 ?>
								
								<font color="#005b90">
		  			&nbsp; &nbsp; Yes<input type="radio" name="manualCommission" id="manualCommission" value="Y" onchange="document.addTrans.action='<?=$transactionPage?>?transID=<?=$_REQUEST["transID"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();" <?=$chkYesCommissionRate?>>
		  				No<input type="radio" name="manualCommission" id="manualCommission" value="N" onchange="document.addTrans.action='<?=$transactionPage?>?transID=<?=$_REQUEST["transID"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();" <?=$chkNoCommissionRate?>>
		  				</font>
						<?  
						  }
						 // debug($imFee."-->".$imFeetemp);
						 if(!$normalFeeFlag){
							if($manualFeeUsersFlag || $manualFeeCountryFlag)
								echo '<br /><br />';
						?>
								<INPUT TYPE="text" NAME="IMFee" ID="IMFee" VALUE="<? 
                               if(CONFIG_TRANS_ROUND_NUMBER == 1 && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_TO'){
                               			echo ($imFeetemp > 0 ? RoundVal($imFeetemp) : RoundVal($imFee));
                                }else{
                                		echo ($imFeetemp > 0 ? round($imFeetemp, $roundLevel) : round($imFee, $roundLevel));
                                }
                                
                                 ?>" MAXLENGTH="25" <? echo $readonlyFlag2;?> <?=$onBlurCommissionCode?> <?=$onBlurManualCommAjax?>>
                         <?php }?>
                              </TD>
							  <?
							  //$_SESSION["discount"]=$imFee;
							   ?>
                          </TR>
					   <?php }?>
						  
						
					   
						  
                          <TR id=Discount STYLE="DISPLAY: none">
                            <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?> Discount</FONT></TD>
                            <TD WIDTH="200" ALIGN="left"><SPAN CLASS="style1">
                              <INPUT TYPE="text" NAME="discount" MAXLENGTH="32" VALUE="<?=$_SESSION["discount"]?>">
                            </SPAN></TD>
                          </TR>
      
						  <? }
						  
						   else 
						   {
						    ?>
							<TR id=Fee STYLE="DISPLAY:">
                            <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?><? echo ((CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1") ? "<font color='#ff0000'>*</font>" : "");?></FONT></TD>
                           <TD WIDTH="200" ALIGN="left">test1<INPUT TYPE="text" NAME="IMFee" ID="IMFee" VALUE="<? 
                                if(CONFIG_TRANS_ROUND_NUMBER == 1  && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_TO'){
                                    echo ($imFeetemp > 0 ? RoundVal($imFeetemp) : RoundVal($imFee));
                                }else{
                                    echo ($imFeetemp > 0 ? round($imFeetemp, $roundLevel) : round($imFee, $roundLevel));
                                }
                                 
                             ?>" MAXLENGTH="25" <? echo $readonlyFlag2 ;?>>
                             
                              </TD>
							 
                          </TR>
							 						<TR>
                            <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?> Discount</FONT></TD>
                            <TD WIDTH="200" ALIGN="left"><SPAN CLASS="style1">
                              <INPUT TYPE="text" NAME="discount" MAXLENGTH="32" VALUE="<?=$_SESSION["discount"]?>" onBlur="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();">
                            </SPAN></TD>
                          
                         
						  <? }
						  
						 ?>
<?php
				$calculateFlag = false;
				if(CONFIG_FRONT_BUTTONS != '1')
					$calculateFlag = true;
					
			if($calculateFlag){
?>                      
						<? if ($_SESSION["chDiscount"]==""){ ?>
						<TR>
                   <TD>&nbsp;</TD>
                    <TD>&nbsp;</TD>
            <? }
				if($fromTotalConfig != '1'){
               
      if (CONFIG_FRONT_BUTTONS != '1') {
      ?>
					  		<TD ALIGN="right"><FONT COLOR="#005b90">Calculate</FONT> </TD>	
				<TD> 	  		
					  		
              		<INPUT TYPE="submit" NAME="amount" VALUE="Amount" onClick="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusButtonID?>';">
              		<INPUT TYPE="submit" NAME="local" VALUE="Local Amount" onClick="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusButtonID?>';">
              	
              	</TD>
       <?	} else { ?>
					  		<TD ALIGN="right">&nbsp;</TD>	
								<TD>&nbsp;</TD>
      <?	} 
	  ?>
              	<?
              	}
			}
              	?>
              </tr>	
				<?
					/**
					 * Distributor Commission - Which-Ever-Is-Higher Logic.
					 * The following interface will be displayed only if the CONFIG_CALCULATE_WIHC is defined and set to true.
					 * The commission will only be calculated for those distributors which will have enteries in "commission" table.
					 */
					if(defined("CONFIG_CALCULATE_WIHC") && CONFIG_CALCULATE_WIHC == true)
					{
						$settlementExchangeRate = getSettlementExchangeRate($_SESSION["distribut"], $_SESSION["currencyTo"]);
						$distributorCommission = 0;
						
						if($settlementExchangeRate != 0)
						{
							$actualAmount = getCalculatedAmounts($localAmount, $operatorDiv, $settlementExchangeRate); //$exRate;
							$tmpWIHCInfo = calculateWIHC($actualAmount, $_SESSION["distribut"], $_SESSION["collectionPointID"]);
							$distributorCommission = round($tmpWIHCInfo["VALUE"], 2);
						}
				?>
						<tr>
							<td align="right"><font color="#005b90">Distributor Commission</font></td>
							<td>
								<span>
									<input type="text" name="tmpDistComm" value="<?=$distributorCommission?>" readonly="readonly" />
								</span>
							</td>
						</tr>
				<?
					}
				?>
              <tr>
                      	<td width="128" height="20" align="right">
                      		<font color="#005b90">Transaction Purpose<font color="#ff0000"><? if(CONFIG_NON_COMP_TRANS_PURPOSE != "1"){ ?>* <? } ?></font></font></td>
                        <td width="195" height="20"><select  name="transactionPurpose" >
                            <option value="">-select one-</option>
                            <option value="Business">Business</option>
                            <option value="Family Assistance"> Family Assistance</option>
                            <!-- option value="Help">Help</option -->
							<?
								$arguments = array("flag"=>"transPurposeDrodown");								
								$dropdownVal = gateway("CONFIG_TANSACTION_PURPOSES",$arguments,CONFIG_TANSACTION_PURPOSES);
								echo $dropdownVal["dropdownOptions"];
								
								// Added by Usman Ghani aginst ticket #3459: Now Transfer - Create Transaction Form Simplification
								if ( !defined("CONFIG_DO_NOT_SHOW_OTHER_PURPOSE")
									|| CONFIG_DO_NOT_SHOW_OTHER_PURPOSE != 1 )
								{
								?>
									<OPTION VALUE="Other">Other</OPTION>
								<?
								}
								// End of code aginst ticket #3459: Now Transfer - Create Transaction Form Simplification
							?>

                          </SELECT>
					<SCRIPT LANGUAGE="JavaScript">
         	SelectOption(document.addTrans.transactionPurpose, "<?=$_SESSION["transactionPurpose"]?>");
                                </SCRIPT></TD>
                        <TD WIDTH="140" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Total Amount<? if($fromTotalConfig == '1'){echo("<font color='#ff0000'>*</font>");} ?></FONT></TD>
                   
                        <TD WIDTH="241" HEIGHT="20"><INPUT TYPE="text" NAME="totalAmount" ID="totalAmountID" VALUE="<?
                                if(CONFIG_TRANS_ROUND_NUMBER == 1  && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_TO'){
                                    /**
                                     * Depricated In case of prime currency
                                     */
                                    //echo ($totalAmount > 0 ? RoundVal($totalAmount) : "");
                                    /**
                                     * Adding the functionality of rounding the value to whole number
                                     */
                                    echo ($totalAmount > 0 ? round($totalAmount,0) : ""); 
                                    
                                }else{
                                    echo ($totalAmount > 0 ? round($totalAmount, $roundLevel) : "");
                                }
                             ?>" onChange="disableClicks();" MAXLENGTH="25" <? if($fromTotalConfig != '1'){echo("readonly");}else{ if (CONFIG_SWAP_AMOUNT_LOCALAMOUNT_AJAX != "1") { ?>onblur="updateAmounts('total'); <? } }?>">
                             
                             <? 
                if($fromTotalConfig == '1')
                {
                 ?>     
                 <INPUT TYPE="submit" NAME="fromTotal" VALUE="Calculate" onClick="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&enterOwnRate=<?=$_GET["enterOwnRate"]?>&focusID=<?=$focusButtonID?>';">    
                 <?
                }
                ?>
                             </TD>
                        
                             
                      </TR>
					   <? if($_SESSION["transType"]=="Bank Transfer" && CONFIG_REMOVE_ADMIN_CHARGES != "1" ||(CONFIG_DISTRIBUTOR_BANK_CHARGES == "1"))
			       {
				  ?>
					  <TR>
					  <TD ALIGN="right"><FONT COLOR="#005b90"> Admin Charges </FONT></TD>
					  <TD><INPUT TYPE="text" NAME="bankCharges" ID="bankChargesID" VALUE="<?=$_SESSION["bankCharges"]?>" MAXLENGTH="25" readonly></TD>
					  <TD>&nbsp;</TD>
					  <TD>&nbsp;</TD>			  
					  
					  </TR>
					  <? }else{ 
					  	$_SESSION["bankCharges"]= 0;
					  	?>
					  <INPUT TYPE="hidden" NAME="bankCharges" ID="bankChargesID" VALUE="<?=$_SESSION["bankCharges"]?>" MAXLENGTH="25">
					  <?
					}
					  ?>
						<? if (CONFIG_CASH_PAID_CHARGES_ENABLED == "1" && $_SESSION["moneyPaid"]=='By Cash')
			  {?>
					  <TR>
					  <TD ALIGN="right"><FONT COLOR="#005b90"> Cash Handling Charges </FONT></TD>
					  <TD><INPUT TYPE="text" NAME="cashHandling" VALUE="<? echo CONFIG_CASH_PAID_CHARGES;?>" MAXLENGTH="25" readonly></TD>
					  <TD>&nbsp;</TD>
					  <TD>&nbsp;</TD>			  
					  
					  </TR>
					  <? } ?>
					  <? if(CONFIG_CURRENCY_CHARGES == '1')
			  		{?>
					  <TR>
					  <TD ALIGN="right"><FONT COLOR="#005b90"> Currency Charges </FONT></TD>
					  <TD><INPUT TYPE="text" NAME="currencyCharge" ID="currencyChargeID" VALUE="<?=$_SESSION["currencyCharge"]?>" MAXLENGTH="25" readonly></TD>
					  <TD>&nbsp;</TD>
					  <TD>&nbsp;</TD>			  
					  
					  </TR>
					  <? } ?>
<? if((!defined("CONFIG_MONEY_PAID_LOCATION") ||  CONFIG_MONEY_PAID_LOCATION != "TOP") || 
	CONFIG_REMOVE_FUNDS_SOURCES!="1") { ?>
	                    <tr> 
                        <? if(!defined("CONFIG_MONEY_PAID_LOCATION") ||  CONFIG_MONEY_PAID_LOCATION != "TOP") { ?>
						<td width="128" height="20" align="right"><font color="#005b90">Money Paid</font><? if(CONFIG_MONEY_PAID_NON_MANDATORY != "1"){?><font color="#ff0000">*</font><? }?></td>
              <td width="195" height="20">

              	<? if( CONFIG_CALCULATE_BY == '1')
              	{ 
              			echo "<input type='text' name='moneyPaid' id='moneyPaid' value='";
              			
              			if($_SESSION["calculateBy"] == "inclusive" )
              					if ( CONFIG_MODIFY_BYBANKTRANSFER_TO_INCLUSIVEOFFEE == "Inclusive of Fee" )
              							echo CONFIG_MODIFY_BYBANKTRANSFER_TO_INCLUSIVEOFFEE;
              					else
              							echo "By Bank Transfer";
              			elseif($_SESSION["calculateBy"]== 'exclusive')
              					if ( CONFIG_MODIFY_BYCASH_TO_EXCLUSIVEOFFEE == "Exclusive of Fee" )
              							echo CONFIG_MODIFY_BYCASH_TO_EXCLUSIVEOFFEE;
              					else
              							echo "By Cash";
              			echo "' maxlength='25' readonly>";
              	}else{ ?>
              	
						<SELECT NAME="moneyPaid" ID="moneyPaid"  onChange="document.addTrans.action='add-transaction.php?transID=<?=$_GET["transID"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();" >
                            <OPTION VALUE="">-select one-</OPTION>
                            <? if(CONFIG_MONEY_PAID_OPTIONS == "1"){ ?>
                            <OPTION VALUE="By Cash HCD">By Cash HCD</OPTION>
                            <OPTION VALUE="By Cash HBT">By Cash HBT</OPTION>
                            <OPTION VALUE="By Cash HQD">By Cash HQD</OPTION>
                            <OPTION VALUE="By Cash BCD">By Cash BCD</OPTION>
                            <OPTION VALUE="By Cash BBT">By Cash BBT</OPTION>
                            <OPTION VALUE="By Cash BQD">By Cash BQD</OPTION>
                            <OPTION VALUE="By Cash WICC">By Cash WICC</OPTION>
                            <? } ?>
							<?php if(!strstr(CONFIG_MONEY_PAID_HIDE_VALUES,"By Cash,")){?>
								<option value="By Cash">By Cash</option>
							<?php }?>
							
							<?
								if(CONFIG_DISPLAY_MONEY_PAID_BY_CHEQUE != false)
								{
							?>
                            		<OPTION VALUE="By Cheque">By Cheque</OPTION>
							<?
								}
							?>
							<?php if(!strstr(CONFIG_MONEY_PAID_HIDE_VALUES,"By Bank Transfer,")){?>
								<option value="By By Bank Transfer">By By Bank Transfer</option>
							<?php }?>
                            <?
                            if(CONFIG_MONEYPAID_ONCREDIT == "1" && !strstr(CONFIG_MONEY_PAID_HIDE_VALUES,"By Credit,"))
                            { 
                            ?>
                            	<OPTION VALUE="On Credit">On Credit</OPTION>
                            <? 
                            } 
                            ?>
                            <!--option value="By Credit Card">By Credit Card</option>
                            <option value="By Debit Card">By Debit Card</option-->
                            
                            <? 
                            	if(CONFIG_MONEY_PAID_OPTION_BY_CARD == 1 && !strstr(CONFIG_MONEY_PAID_HIDE_VALUES,"By Card,")) { 
                            ?>
                            	<OPTION VALUE="By Card">By Card</OPTION>
                            <? } ?>
                          </SELECT>						
                          <? 
                          }
                          
                          ?>
                            <SCRIPT LANGUAGE="JavaScript">
         	SelectOption(document.addTrans.moneyPaid, "<?=$_SESSION["moneyPaid"]?>");
                                </SCRIPT></TD>
						 <? } else { ?>
						 	<td colspan="2">&nbsp;</td>
						 <? } ?>	
                   <? if(CONFIG_REMOVE_FUNDS_SOURCES != "1") { ?>             
                        <TD WIDTH="96" ALIGN="right"><FONT COLOR="#005b90">Funds Sources<? echo (CONFIG_NONCOMPUL_FUNDSOURCES != '1' ? "<font color='#ff0000'>*</font>" : "") ?></FONT></TD>
						<TD WIDTH="241" HEIGHT="20"><SELECT NAME="fundSources">
                            <OPTION VALUE="">-select one-</OPTION>
                            <OPTION VALUE="Salary">Salary</OPTION>
                            <OPTION VALUE="Savings">Savings</OPTION>
                            <OPTION VALUE="Loan">Loan</OPTION>
                          </SELECT>
                            <SCRIPT LANGUAGE="JavaScript">
					         	SelectOption(document.addTrans.fundSources, "<?=$_SESSION["fundSources"]?>");
                             </SCRIPT></TD>
                        <!-- td width="241"><input type="text" name="fundSources" value="<?=$_SESSION["fundSources"]; ?>" maxlength="100">
                        </td-->
						<? }else{?>
							<td>&nbsp;</td>
						<? }?> 
						<td colspan="2">&nbsp;</td>
                    </TR>
					<? } ?>
                    <? 
                    
                    if($_POST["moneyPaid"] != "By Cheque" && $_SESSION["moneyPaid"] != "By Cheque"){
                    	
                    			$_SESSION["chequeNo"] = "";

								if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
								{
									$_SESSION["chequeAmount"] = "";
								}
                    	
                    	}
                    if(($_POST["moneyPaid"] == "By Cheque" || $_SESSION["moneyPaid"] == "By Cheque") && CONFIG_CHEQUE_BASED_TRANSACTIONS == "1"){?>
						<TR>
							<TD WIDTH="150" HEIGHT="20" ALIGN="right"><font color="#005b90">Cheque No</TD>
							<TD WIDTH="150" HEIGHT="20" ALIGN="left">
								<INPUT TYPE="text" NAME="chequeNo" ID="chequeNo" VALUE="<?=$_SESSION["chequeNo"]; ?>">
							</TD>
						</TR>
					<?
						if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1 )
						{
							?>
								<TR>
									<TD WIDTH="150" HEIGHT="20" ALIGN="right"><font color="#005b90">Cheque Amount</TD>
									<TD WIDTH="150" HEIGHT="20" ALIGN="left">
										<INPUT TYPE="text" NAME="chequeAmount" ID="chequeAmount" VALUE="<?=$_SESSION["chequeAmount"]; ?>">
									</TD>
								</TR>
							<?
						}
					?>
               	<? } ?>
               	<? 
				if(($_REQUEST["moneyPaid"]== "By Bank Transfer"||$_SESSION["moneyPaid"] == "By Bank Transfer") && CONFIG_ADD_SENDING_BANK_ON_CREATE_TRANSACTION == "1"){ 
	 	        ?> 
				<SCRIPT>
					function searchSenderBank()
					{
						var searchString = document.getElementById("searchString").value;
						var searchType = document.getElementById("searchType").value;
						if(searchString == '' || IsAllSpaces(searchString))
						{ 
							alert ('Please provide search string.');
							document.getElementById("searchString").focus;
						} 
						else 
						{ 
							window.open('search-sender-bank.php?from=add-transaction.php&search=Y&transID=<?=$_GET["transID"]?>'+'&searchString='+ searchString +'&searchType='+ searchType +'&country=<?=$customerContent["Country"]?>', 'SearchSenderBank', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') 
						}
					}
				</script>
	 	        	<tr> 
						<td height="20" align="center" colspan="4">
							<br />
							<font color="#005b90">Search For</font>
							<input type="text" size="15" id="searchString" name="searchString" value="<?=$_REQUEST["searchString"]?>" />

							&nbsp;&nbsp;

							<font color="#005b90">On</font>
							<select name="searchType" id="searchType">
								<?
									$typeLable[] = "Branch Address";
									$typeLable[] = "Bank Name";
									$typeLable[] = "Bank Name and Branch Address";
									$typeLable[] = "Sort Code";
									
									$typeValue[] = "BRANCH_ADDRESS";
									$typeValue[] = "BANK_NAME";
									$typeValue[] = "BANK_NAME_ADDRESS";
									$typeValue[] = "SORT_CODE";
									
									for($i=0; $i<sizeof($typeLable); $i++)
									{
										if($typeValue[$i] == $searchType)
										{
											echo "<option value=\"".$typeValue[$i]."\" selected>".$typeLable[$i]."</option>";
										} else {
											echo "<option value=\"".$typeValue[$i]."\">".$typeLable[$i]."</option>";
										}
									}
								?>
							</select>
							<input type="button" onClick="searchSenderBank();" value="Search" name="Submit"/>

							
							<SPAN STYLE="background-color:#c0c0c0" CLASS="style1">   
								<A onClick=" window.open('add-bank.php?from=popUp&transID=<?=$_GET["transID"]?>&moneyPaid=<?=$_SESSION["moneyPaid"]?>&custCountry=<?=$_REQUEST["custCountry"]?>','', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" HREF="javascript:;"> 
									<B>Add New Bank</B>
								</A> 
							</SPAN> 
							<BR />
	 	                 </TD>
	 	        	</TR> 
		        	<TR> 
						<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Sender Bank</FONT><FONT COLOR="#ff0000">*</FONT></TD> 
						<TD WIDTH="150" HEIGHT="20" ALIGN="left"> 
							<?
								if(!empty($_SESSION["senderBank"]))
									$senderBanks = selectFrom("SELECT bankId,name,branchAddress FROM ".TBL_BANKS." WHERE bankId = '".$_SESSION["senderBank"]."'"); 
							?>
							<INPUT NAME="senderBankDetails" TYPE="text" SIZE="35" VALUE="<?=!empty($_SESSION["senderBank"])?$senderBanks["name"]." , ".$senderBanks["branchAddress"]:"" ?>" readonly/>
							<INPUT TYPE="hidden" NAME="senderBank" VALUE="<?=$_SESSION["senderBank"]?>" />
	 	                 </TD> 
	 	                 <TD>&nbsp;</TD>
	 	        	</TR> 
             	<?
			 	} 
             	?>
               	
               <? if(CONFIG_BRANCH_NAME == "1") { ?>     
               <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"> Branch Money Paid In</FONT> </TD>
               <TD WIDTH="200" HEIGHT="20"><INPUT NAME="branchName" TYPE="text" ID="branchName" VALUE="<?=$_SESSION["branchName"]; ?>" ></TD>
               <? } ?>
                      
					  <? if($_SESSION["transactionPurpose"] == "Other" && (CONFIG_HIDE_OTHER_PURPOSE != "1" || CONFIG_HIDE_OTHER_PURPOSE == ""))
			  {?>
					  <TR>
					    <TD><DIV ALIGN="right"><FONT COLOR="#005b90">Other Purpose&nbsp;</FONT></DIV></TD>
					    <TD><SPAN CLASS="style1">
					      <INPUT TYPE="text" NAME="other_pur" MAXLENGTH="32" VALUE="<?=$_SESSION["other_pur"]?>">
					    </SPAN></TD>
					  <TD>&nbsp;</TD>
					  <TD>&nbsp;</TD>
					  </TR>
					  <? }
					  
			if(IS_BRANCH_MANAGER){
			
			/* #4630 
				Author : Aslam
				Descriiption : CONFIG_DISCOUNT_REQUEST!="1" added in following if condition 
				so that all users may have functionality of Fee Discount (No requests to be made from now.)
				CONFIG_DISCOUNT_REQUEST is for nowtransfer. so it should run for globalexchange.
			*/
			if($userDetails["isMain"] == "Y" || $agentType == "Admin Manager" || CONFIG_DISCOUNT_REQUEST!="1") {
			 ?>				  
            <TR>
            	<?
            	if(CONFIG_HIDE_TIP_FIELD != "1"){
            	?>
            		<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><? if (CONFIG_TIP_ENABLED == "1"){ echo(CONFIG_TIP); }else{ echo 'Tip';}  ?></FONT><? if($_SESSION["transType"] != "Bank Transfer"){ ?><font color="#FF0000" id="tipMandatoryDiv">*</font><? } ?> </TD>
            		<TD WIDTH="200" HEIGHT="20"><INPUT NAME="tip" TYPE="text" ID="tip" VALUE="<?=$_SESSION["tip"]; ?>"></TD>
            		
            	<?
            	}else{
            	?>
            		<TD WIDTH="150" HEIGHT="20">&nbsp;</TD>
            		<TD WIDTH="200" HEIGHT="20">&nbsp;</TD>
             	<?
            	}
            	?>
            	<? if (!(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")){ ?>
                <TD WIDTH="140" HEIGHT="20" ALIGN="right">
					<?
						// Added by Usman Ghani aginst ticket #3459: Now Transfer - Create Transaction Form Simplification
						if ( !defined("CONFIG_DO_NOT_SHOW_FEE_DISCOUNT")
							|| CONFIG_DO_NOT_SHOW_FEE_DISCOUNT != 1 )
						{
						?>
							<FONT COLOR="#005b90">Fee Discount</FONT>
						<?
						}
						// End of code aginst ticket #3459: Now Transfer - Create Transaction Form Simplification
					?>
				</TD>
				<TD WIDTH="241" HEIGHT="20" CLASS="style1">
					<?
						// Added by Usman Ghani aginst ticket #3459: Now Transfer - Create Transaction Form Simplification
						if ( !defined("CONFIG_DO_NOT_SHOW_FEE_DISCOUNT")
							|| CONFIG_DO_NOT_SHOW_FEE_DISCOUNT != 1 )
						{
						?>
							<? if ($_SESSION["chDiscount"]!=""){$ch= "checked='checked'";}?>
								<INPUT TYPE="checkbox" NAME="chDiscount" <? echo $ch; ?> onClick="<? if($_SESSION["chDiscount"]!=""){$_SESSION["chDiscount"]="";}?>return shorProvinces()" onBlur="<? if($_SESSION["chDiscount"]==""){?>document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();<? }?>"  >
							<? } ?>
						<?
						}
						// End of code aginst ticket #3459: Now Transfer - Create Transaction Form Simplification
					?>
				</TD>
           </TR>
                                
            <? }
            
          	else
          	{ ?>
          		<TR>
            	<?
            	if(CONFIG_HIDE_TIP_FIELD != "1"){
            	?>
            		<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><? if (CONFIG_TIP_ENABLED == "1"){ echo(CONFIG_TIP); }else{ echo 'Tip'; } ?>  </FONT> </TD>
            		<TD WIDTH="200" HEIGHT="20"><INPUT NAME="tip" TYPE="text" ID="tip" VALUE="<?=$_SESSION["tip"]; ?>"></TD>
             	<?
            	}else{
            	?>	
            		<TD WIDTH="150" HEIGHT="20">&nbsp;</TD>
            		<TD WIDTH="200" HEIGHT="20">&nbsp;</TD>
            	<?
            	}
            	?>
            	<? if (!(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")){ ?>
                <TD WIDTH="96" HEIGHT="20" ALIGN="right">
                	<?
									if ( !defined("CONFIG_DO_NOT_SHOW_FEE_DISCOUNT") || CONFIG_DO_NOT_SHOW_FEE_DISCOUNT != 1 )
									{
									//debug(defined("CONFIG_DO_NOT_SHOW_FEE_DISCOUNT"));
									?>
                		<FONT COLOR="#005b90"><B>Fee Discount Request</B></FONT></TD>
									<?
									}
									?>
									<TD WIDTH="241" HEIGHT="20" CLASS="style1">
										<?
										if ( !defined("CONFIG_DO_NOT_SHOW_FEE_DISCOUNT") || CONFIG_DO_NOT_SHOW_FEE_DISCOUNT != 1 )
										{
										?>
											<INPUT TYPE="text" NAME="discount_request" VALUE="<? echo $_SESSION["discount_request"]; ?>"></TD>
										<? 		
										}
							} 
							?>
           </TR>
        <? }} else
        { if(CONFIG_DISCOUNT_REQUEST != '1')
        	{
        	?>
         	 <TR>
            	<?
            	if(CONFIG_HIDE_TIP_FIELD != "1"){
            	?>
            		<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><? if (CONFIG_TIP_ENABLED == "1"){ echo(CONFIG_TIP); }else{ echo 'Tip'; } ?>  </FONT> </TD>
            		<TD WIDTH="200" HEIGHT="20"><INPUT NAME="tip" TYPE="text" ID="tip" VALUE="<?=$_SESSION["tip"]; ?>"></TD>
             	<?
            	}else{
            	?>
            		<TD WIDTH="150" HEIGHT="20">&nbsp;</TD>
            		<TD WIDTH="200" HEIGHT="20">&nbsp;</TD>		
            	<?
            	}
            	?>
            	<?
            	if (!(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")){ ?>
                <TD WIDTH="96" HEIGHT="20" ALIGN="right">
                	<?
									if ( !defined("CONFIG_DO_NOT_SHOW_FEE_DISCOUNT") || CONFIG_DO_NOT_SHOW_FEE_DISCOUNT != 1 )
									{
									?>
                		<FONT COLOR="#005b90">Fee Discount</FONT>
                	<?
                	}
                	?>
                	</TD>
                	<TD WIDTH="241" HEIGHT="20" CLASS="style1">
                	<?
									if ( !defined("CONFIG_DO_NOT_SHOW_FEE_DISCOUNT") || CONFIG_DO_NOT_SHOW_FEE_DISCOUNT != 1 )
									{
									?>
										<? if ($_SESSION["chDiscount"]!=""){$ch= "checked='checked'";}?>
											<INPUT TYPE="checkbox" NAME="chDiscount" <? echo $ch; ?>   onClick="<? if($_SESSION["chDiscount"]!=""){$_SESSION["chDiscount"]="";}?>return shorProvinces()" onBlur="<? if($_SESSION["chDiscount"]==""){?>document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();<? }?>"  >
									<? 
									} 
									?>
									</TD>
								<?
								}
								?>
							
           </TR>
        <? }elseif(CONFIG_DISCOUNT_REQUEST == '1')
        {
        	?>
        	<TR>
            	<?
            	if(CONFIG_HIDE_TIP_FIELD != "1"){
            	?>
            		<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><? if (CONFIG_TIP_ENABLED == "1"){ echo(CONFIG_TIP); }else{ echo 'Tip'; } ?>  </FONT> </TD>
            		<TD WIDTH="200" HEIGHT="20"><INPUT NAME="tip" TYPE="text" ID="tip" VALUE="<?=$_SESSION["tip"]; ?>"></TD>
             	<?
            	}else{
            	?>
            		<TD WIDTH="150" HEIGHT="20">&nbsp;</TD>
            		<TD WIDTH="200" HEIGHT="20">&nbsp;</TD>		
            	<?
            	}
            	?>
            	<TD WIDTH="96" HEIGHT="20" ALIGN="right">
            		<?
								if ( !defined("CONFIG_DO_NOT_SHOW_FEE_DISCOUNT") || CONFIG_DO_NOT_SHOW_FEE_DISCOUNT != 1 )
								{
								//debug(CONFIG_DO_NOT_SHOW_FEE_DISCOUNT);
								?>
            		 <FONT COLOR="#005b90"><B>Fee Discount Request</B></FONT>
            		<?
            		}
            		?>
            	</TD>
            	
					<TD WIDTH="241" HEIGHT="20" CLASS="style1">
						<?
						if ( !defined("CONFIG_DO_NOT_SHOW_FEE_DISCOUNT") || CONFIG_DO_NOT_SHOW_FEE_DISCOUNT != 1 )
						{
						?>
							<INPUT TYPE="checkbox" NAME="discount_request" VALUE="<? 
					if($_SESSION['discount_request'] > 0)
					{
						echo($_SESSION['discount_request']);
						}else{
                               if(CONFIG_TRANS_ROUND_NUMBER == 1 && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_TO'){
                                    echo ($imFeetemp > 0 ? RoundVal($imFeetemp) : RoundVal($imFee));
                                }else{
                                    echo ($imFeetemp > 0 ? round($imFeetemp, $roundLevel) : round($imFee, $roundLevel));
                                }
                            }
                                 ?>" <? echo ($_SESSION['discount_request'] > 0 ? "checked" : "") ?>
                                 <?
					if(CONFIG_TOTAL_FEE_DISCOUNT == '1')
					 {// && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType))discount_request
					 	?>
					 onChange="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();"	
					 	
					<?
					}
					?>>
					<?
					}
					?>
					</TD>
			</TR>
			
        <?
			
        	}
         }
				if($remarksFlag || CONFIG_VALUE_DATE_TRANSACTIONS=="1") {
	     ?>
      <TR>
				<?php if(!$remarksFlag && $valueDateFlag){?>
				<TD WIDTH="150" HEIGHT="20" ALIGN="right" colspan="2">&nbsp;</TD>
				<?php }?>
	  			<?php if($remarksFlag){?>
            	<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Remarks:</FONT> </TD>
            	<TD WIDTH="200" HEIGHT="20"><INPUT NAME="clientRef" TYPE="text" ID="clientRef" VALUE="<?=$_SESSION["clientRef"]; ?>"></TD>
				<?php }?>
				<?php if($valueDateFlag){ 
				if(CONFIG_VALUE_DATE_MANDATORY=="1")
					$manadatoryLabel = '<font color="#FF0000">*</font>';
				?>
            	<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Maturity Date:<?=$manadatoryLabel?></FONT> </TD>
            	<TD WIDTH="200" HEIGHT="20"><INPUT NAME="value_date" TYPE="text" ID="value_date" readonly="readonly" VALUE="<?=$_SESSION["valueDate"]; ?>"><img title="Value Date|Transaction created without value date will move in 'Apply Value Date' Queue" src="images/info.gif" style="" /></TD>
				<?php }?>
				<?php if(!$remarksFlag || !$valueDateFlag){?>
				<TD WIDTH="150" HEIGHT="20" ALIGN="right" colspan="2">&nbsp;</TD>
				<?php }?>
      </TR>
                          <?
            }
           
           
            
            if(CONFIG_SECRET_QUEST_ENABLED == "1")
            {	?>
             <TR>
              <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"> Secret Question</FONT> </TD>
              <TD WIDTH="200" HEIGHT="20"><INPUT NAME="question" TYPE="text" ID="question" VALUE="<?=$_SESSION["question"]; ?>" ></TD>
             	<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Answer</FONT></TD>
              <TD WIDTH="200" HEIGHT="20"><INPUT NAME="answer" TYPE="text" ID="answer" VALUE="<?=$_SESSION["answer"]; ?>"></TD>
            </TR>
            
            
            <?          
          	}?>
   
       		   <?
					  if(CONFIG_INTERNAL_REMARKS == '1')
					  {
					  ?>
					  <TR>
					  	<TD ALIGN="right"><FONT COLOR="#005b90"> <? echo(CONFIG_LABEL_INTERNAL_REMARKS != '' ? CONFIG_LABEL_INTERNAL_REMARKS : "Internal Remarks"); ?> </FONT></TD> 
				    	<TD><input type="text" name="internalRemarks"  type="text" id="internalRemarks" value="<?=$_SESSION["internalRemarks"]; ?>"></TD> 
				    	<TD>&nbsp;</TD>
					  	<TD>&nbsp;</TD>			  
					  </TR>
					  <?
						}
					  ?>		
					  
			   <?
				/********************
				* Added by Niaz Ahmad at 02-05-2009 
				* Ticket #4915
				* Following config added 'CONFIG_PASSWORD_ON_RECEIPT' 'CONFIG_PASSWORD_ON_RECEIPT_TITLE'
				* Shown Secret Code input field at create transaction as well as on print receipt page.
				********************/	  
			  if((CONFIG_IDTYPE_PASSWORD == '1' && $benificiaryContent["otherId"]!= "") || CONFIG_PASSWORD_ON_RECEIPT == "1")
					  {
					   if(CONFIG_IDTYPE_PASSWORD == '1' && $benificiaryContent["otherId"]!= ""){
					   $title = Password;
					   }elseif(CONFIG_PASSWORD_ON_RECEIPT == "1"){
					   $title = CONFIG_PASSWORD_ON_RECEIPT_TITLE;
					   }
					  ?>
					  <TR>
					  	<TD ALIGN="right"><FONT COLOR="#005b90"> <?=$title;?> </FONT></TD> 
				    	<TD><input type="text" name="benIdPassword"  type="text" id="benIdPassword" value="<?=$_SESSION["benIdPassword"]; ?>"></TD> 
				    	<TD>&nbsp;</TD>
					  	<TD>&nbsp;</TD>			  
					  </TR>
					  <?
						}
					  ?>
					  
					   <?
					  ///this functionality is to alam user to send particular transaction for Distribution on reaching minimum or maximum limit.
					  if(CONFIG_ENABLE_EX_RATE_LIMIT == '1')
					  {
					  ?>
					  <TR>
					  	<TD ALIGN="right"><FONT COLOR="#005b90"> Notify Exchange Rate Limit </FONT></TD> 
				    	<TD>Min<input type="text" name="exRateLimitMin"  type="text" id="exRateLimitMin" value="<?=$_SESSION["exRateLimitMin"]; ?>" size="6">
				    		Max<input type="text" name="exRateLimitMax"  type="text" id="exRateLimitMax" value="<?=$_SESSION["exRateLimitMax"]; ?>" size="6">
			    		</TD>
					  
					  </TR>
					  <?
						}
						/** 
						 * #5365 AMB Exchange
						 * Shown Note input box at add transaction page
						 * Enable  : "1"
						 * Disabl  : "0"
						 * by Niaz Ahmad at 21-08-2009
						 */ 
						if(CONFIG_TRANSACTION_NOTE_AT_ADD_TRANSACTION == "1" && empty($_REQUEST["transID"])){
						?>
						<tr>
								<td align="center" colspan="4">
									<b>Transaction Notes</b>
									<a href="#" >
									<img src="images/info.gif" title="Transaction Notes| Please enter the Transaction Notes. Not use any special letter. e.g PAYMENT IN FULL AND FINAL SETTLEMENT FOR INVOICE NUMBER 513 DATED 25/02/10" />
									</a>
									<br />
									<textarea name="transNote" id="transNote" wrap="soft" cols="75" rows="4"><?=$_SESSION["transNote"]?></textarea>
								</td> 
							</tr>	
						<?
						}
						/**
						 * If transaction goes to the ammended than keep the track what is being ammended
						 * as well as the description from the user
						 * with its charateristics
						 * @Ticket #3337
						 */
						if(CONFIG_STORE_MODIFY_TRANSACTION_HISTORY == "1" && !empty($_REQUEST["transID"])) 
						{ 
							$strTransactionStatus = selectFrom("select transStatus from ".TBL_TRANSACTIONS." where transID = '".$_REQUEST["transID"]."'");
							if(	   $strTransactionStatus["transStatus"] == "Authorize" 
								|| $strTransactionStatus["transStatus"] == "Amended"
								|| $strTransactionStatus["transStatus"] == "Pending")
							{
								$sqlHistoryData = "select history from amended_transactions where transID = '".$_REQUEST["transID"]."' and history is not null";
								$arrHistoryData = selectMultiRecords($sqlHistoryData);
								//debug($arrHistoryData);
								
								//debug(count($arrHistoryData));
								?>
								<tr>
									<td align="center" colspan="4">
										<? if(CONFIG_TRANSACTION_NOTE_AT_ADD_TRANSACTION == "1"){?>
										<b><?=CONFIG_TRANSACTION_NOTE_AT_ADD_TRANSACTION_LABELS?></b> 
										<? }else{ ?>
										<b>Transaction Edit History</b>
										<? } ?>
									[&nbsp;<a id="showHideBtn" href="javascript:void(0)" onclick="showHideHistoryFn();" style="color:#0000FF">Show Detail</a>&nbsp;]
										<br />
										<table align="center" width="100%" id="historyRow" cellpadding="3" cellspacing="0" style="display:none">
								<?
								if(CONFIG_TRANSACTION_NOTE_AT_ADD_TRANSACTION == "1"){
								$strTransactionNoteSql = selectFrom("select transaction_notes from ".TBL_TRANSACTIONS." where transID = '".$_REQUEST["transID"]."'");
								if(!empty($strTransactionNoteSql)){
										$arrTransNote = explode("|",$strTransactionNoteSql["transaction_notes"]);
									
											if(!empty($arrTransNote[0]))
												$arrUserData2 = selectFrom("select username, name, adminType from admin where userid='".$arrTransNote[0]."'");
												
											
									
									?>
								<tr bgcolor="#CCCCCC">
										<td align="left">
											<b><?=$arrUserData2["username"]?> / <?=$arrUserData2["name"]?> / <?=$arrUserData2["adminType"]?></b>
										</td> 
										<td align="right"><?=date("F d, Y H:i:s",$arrHistoryDetail[1])?> GMT</td> 
									</tr>
									<tr bgcolor="#bebebe">
										<td colspan="2" align="left"><?=$arrTransNote[2]?></td>
									</tr>
									<? 
									 } 
									}
									?>
									
								<?
							
								if(count($arrHistoryData) > 0)
								{
									for($count = 0; $count < count($arrHistoryData); $count++)
									{
										if(!empty($arrHistoryData[$count]["history"]))
										{
											$arrHistoryDetail = explode("|",$arrHistoryData[$count]["history"]);
											//debug($arrHistoryDetail);
											if(!empty($arrHistoryDetail[0]))
											{
												$arrUserData = selectFrom("select username, name, adminType from admin where userid='".$arrHistoryDetail[0]."'");
											
								?>
									<tr bgcolor="#CCCCCC">
										<td align="left">
											<b><?=$arrUserData["username"]?> / <?=$arrUserData["name"]?> / <?=$arrUserData["adminType"]?></b>
										</td> 
										<td align="right"><?=date("F d, Y H:i:s",$arrHistoryDetail[1])?> GMT</td> 
									</tr>
									<tr bgcolor="#bebebe">
										<td colspan="2" align="left"><?=$arrHistoryDetail[2]?></td>
									</tr>
								<? 
											}
										}
									} 
								}
								
								if(count($arrHistoryData) < 1) { 
								?>
									<tr bgcolor="#bebebe">
										<td colspan="4" align="center">No modify history found.</td>
									</tr>
								<? } ?>
									</table>
								</td> 
							</tr>

							<tr>
								<td align="center" colspan="4">
								 <? if(CONFIG_TRANSACTION_NOTE_AT_ADD_TRANSACTION == "1"){?>
									<b><?=CONFIG_TRANSACTION_NOTE_AT_ADD_TRANSACTION_LABELS?></b>
									<? }else{ ?>
									<b>Reason to Modify This Transaction <font color="red">*</font></b>
									<? } ?>
									<a href="#" ><img src="images/info.gif" title="Reason To Edit| Please enter the full description i.e. <br /> * For which reason you are going to modify the transaction.<br /> * Who ask you to do this.<br /> * Which fields you have modify.<br /> * Is customer and beneficiary know about the transaction modification.<br /> * Other import information which does not covers in any of the above mentioned sentense." /></a>
									<br />
									<textarea name="modifyReason" id="modifyReason" wrap="soft" cols="75" rows="4"></textarea>
								</td> 
							</tr>	
					<?
							}
						} 
					?>
			 
					 
                 <?	if (CONFIG_TnC_CHECKBOX_OFF == "1") {  ?>		
				 <INPUT TYPE="hidden" NAME="Declaration" VALUE="Y">
                      <?	} else {  ?>				 			 
					  <tr>
                        <TD WIDTH="128" HEIGHT="20" ALIGN="right" VALIGN="top"><INPUT TYPE="checkbox" NAME="Declaration" VALUE="Y"></TD>
                        <TD HEIGHT="20" COLSPAN="3">
							<? if(CONFIG_TRANS_CONDITION_ENABLED == '1')
                        	{
                        		if(defined("CONFIG_TRANS_COND"))
								{
									echo(CONFIG_TRANS_COND);
								}
								else
								{
										$termsConditionSql = selectFrom("SELECT
											company_terms_conditions
										 FROM 
											company_detail
										 WHERE 
											company_terms_conditions!='' AND 
											dated = (
													 SELECT MAX(dated)
													 FROM 
														company_detail 
													WHERE 
														company_terms_conditions!=''
													 )");
								if(!empty($termsConditionSql["company_terms_conditions"]))
									$tremsConditions = $termsConditionSql["company_terms_conditions"];
									
								echo $tremsConditions;
								}
                          		?>
                     	<?	if (defined('CONFIG_CONDITIONS')) {  ?>
                        		<A HREF="#" onClick="javascript:window.open('<?=CONFIG_CONDITIONS?>', 'Conditions', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')" CLASS="style2">Full Terms and Conditions</A>
                     	<?	}  ?>
                        		<? }else{
								
                        	 ?>
                        	I accept that I haven't used any illegal ways to earn money and I am paying tax regulary. I also accept that I am not going to send this money for any illegal act.
                        	<? } ?>
                        	</TD>
                      </TR>
                      <?	}  ?>
             
                      <TR>
                        <TD WIDTH="128" HEIGHT="20" ALIGN="right" VALIGN="top">&nbsp;</TD>
                        <TD HEIGHT="20" COLSPAN="3" ALIGN="center"><?php 
						if(CONFIG_SENDER_ID_EXPIRED_WAVIER == "1"){ ?>
						<input type="hidden" name="docWaiver" value="<?=$_SESSION["docWaiver"]?>"/>
						<?php } ?>
						<INPUT TYPE="hidden" NAME="transID" VALUE="<? echo $_GET["transID"]?>">
                            <? If (CONFIG_SEND_ONLY_BUTTON == 1){?>
                           		<INPUT TYPE="button" VALUE="Send & Print" NAME="transSend" ID="transSend" onClick="<? if(CONFIG_COMPLIANCE_PROMPT == '1'){ ?>checkForm(addTrans,'Yes');<? }else{?> document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend=transSend'; document.addTrans.submit();<? }?>">&nbsp;&nbsp;
                            <?}?>
                           
                           <? if(CONFIG_SEND_ONLY_BUTTON == 1){?>
                           <INPUT  type="button" NAME="save" VALUE=" Send " ID="sendToConfirmID" onClick="return <? if(CONFIG_COMPLIANCE_PROMPT == '1'){ ?>ajaxReturn(addTrans,'No');<? }else{?> document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend=noSend'; document.addTrans.submit();<? }?>">&nbsp;&nbsp;
                           <? } elseif (CONFIG_CUSTOM_RECIEPT == '1'){?>
                          <INPUT  type="button" NAME="save" VALUE="Send" ID="sendID"  onclick="if(checkChequeNo() == true){<? if(CONFIG_COMPLIANCE_PROMPT == '1'){ ?>ajaxReturn(addTrans,'No');<? }else{?>document.addTrans.action='<? echo CONFIG_RECIEPT_NAME ?>?transID=<? echo $_GET["transID"]?>&focusID=sendID&transSend=noSend'; document.addTrans.submit();<? } ?>}">
                          <!--input  type="submit" name="save" value="Send" id="sendID" /-->&nbsp;&nbsp;
                           <? }else{ ?>
                           <INPUT  type="button" NAME="save" VALUE="Send" ID="sendID"  onclick="showZeroCommissionAlert();<? if(CONFIG_COMPLIANCE_PROMPT == '1'){ ?>ajaxReturn(addTrans,'No');<? }else{?> document.addTrans.action='confirm-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=sendID&transSend=noSend'; document.addTrans.submit();<? }?>">&nbsp;&nbsp;
                           <? } ?>
              <INPUT  type="button" NAME="reset" VALUE=" Clear " onClick="cleanForm();" ></TD>
                      </TR>
                      <TR>
                        <TD HEIGHT="20" ALIGN="right" VALIGN="top">&nbsp;</TD>
                        <TD HEIGHT="20" COLSPAN="3" ALIGN="center"><INPUT TYPE="hidden" NAME="act" VALUE="<?=$act?>">&nbsp;</TD>
                      </TR>
                    </TABLE>
                  </FIELDSET></td>
                </tr>
                <?
		  }
//}
?>
	
	
	<? 
	   /* This code added by Niaz Ahmad at 06-03-2009 @4616 
	      When user enter manual exchange rate greater than defined exchange rate
		  then it display error (Maximum Exchange Rate can be 112) after this message 
		  send button must be disable
	   */
	if($messageError != ""){ 
	
	?>
	
			<SCRIPT LANGUAGE="javascript">
				 disableClicks();
				//document.getElementById('sendID').disabled=true;
				//$('#sendID').attr('disabled','disabled');
				</script>
         	<? $messageError = ""; 
			 }
            ?>
			
              </FORM>
            </TABLE></td>
          </tr>
        </TABLE></td>
    </tr>
</table>
</body>
</html>