<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
//include_once ("secureAllPages.php");
include ("security.php");
include ("javaScript.php");
$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];
$modifyby = $_SESSION["loggedUserData"]["userID"];

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

$currencyies = selectMultiRecords("SELECT DISTINCT(currencyFrom) as currencyName FROM ".TBL_TRANSACTIONS);

$strLabel = 'Recurring Cron Report';

$linkedAgent = array();
if(CONFIG_ADMIN_ASSOCIATE_AGENT == '1' && ($agentType != 'admin' && $agentType != 'Branch Manager' /*&& $agentType != 'Admin Manager'*/))
{
	
	$adminLinkDetails = selectFrom("select linked_Agent from ".TBL_ADMIN_USERS." where userID='".$parentID."'");
	
	$linkedAgent = explode(",",$adminLinkDetails["linked_Agent"]);	
	//$query .= " and username in '".$linkedAgent."'";
}

$querySender = "select DISTINCT customerID ,Title, firstName, middleName, lastName from customer ";


$agents = selectMultiRecords($querySender);
					
?>
<html>
<head>
<title><?=$strLabel?></title>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee/grid.css" title="coffee" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/grid.celledit.js" type="text/javascript"></script>

<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script language="JavaScript" src="./javascript/GCappearance2.js"></script>
<script language="JavaScript" src="./javascript/GurtCalendar.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="./styles/calendar2.css">
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<script language="javascript" src="./styles/admin.js"></script>
<script language="javascript" src="./javascript/jquery.maskedinput.min.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>

<script language="javascript" type="text/javascript">

	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var subgridimgpath = 'javascript/jqGrid/themes/green/images';
	var tAmt = 0;
	var lAmt = 0;
	var extraParams;
	var comSubGrid = 'comSubGrid';
	jQuery(document).ready(function(){
		$("#loading").ajaxStart(function(){
		   $("#confirmFlag").val("");
		 });

		var lastSel;
		var maxRows = 20;
		jQuery("#comMainList").jqGrid({
			url:"recurring_report_action.php?getGrid=intComReport&nd="+new Date().getTime()+"&q=1",
			datatype: "json",
			height: 300,
			width: 1370,
			colNames:['Parent Transaction Number','Parent Transaction Status','Start Date', 'End Date', 'Sender Name','Beneficiary Name','Frequency','Amount','Currency from','Currency To','Exchange Rate','Local Amount','Rule Status','Count'],
			colModel:[
				{name:'TransID',index:'TransID', width:100},
				{name:'Pstatus',index:'Pstatus', width:100},
				{name:'StartDate',index:'StartDate', width:50},
				{name:'EndDate',index:'EndDate', width:50},
				{name:'SenderName',index:'SenderName', width:80, align:"left"},
				{name:'BenName',index:'BenName', width:80, align:"left"},
				{name:'Frequency',index:'Frequency', width:80, align:"left"},
				{name:'Amount',index:'Amount', width:50, align:"left"},
				{name:'CurrencyFrom',index:'CurrencyFrom', width:60, align:"left"},
				{name:'CurrencyTo',index:'CurrencyTo', width:60, align:"left"},
				{name:'ExchangeRate',index:'ExchangeRate', width:60, align:"left"},
				{name:'LocalAmount',index:'LocalAmount', width:60, align:"left"},
				{name:'RuleStatus',index:'RuleStatus', width:60, align:"left"},
				{name:'count',index:'count', width:60, align:"left"},
				/* {name:'ChildTransID',index:'ChildTransID', width:100, align:'center'} */,
				
				
			],
			imgpath:gridimgpath,
			rowNum:maxRows,
			rowList: [20,50,100],
			pager: jQuery('#pagernav'),
			sortname: 'transID',
			viewrecords: true,
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			multiselect: true,
			forceFit: true,
			shrinkToFit: true,
			multiselect: true,
			footerrow:true,
			userDataOnFooter:true,
			subGrid: true,
			caption: "<?=$strLabel?>",
			groupOrder: ['desc', 'asc','desc'],
			
		subGridRowExpanded:function(subgrid_id,row_id){
		 
				
				$("#comMainGridID").val(row_id);
				var subgrid_table_id;
				var pager_id;
				
				subgrid_table_id =subgrid_id+"_t"; //comSubGrid;
				//alert(subgrid_table_id);
				pager_id = "p_"+subgrid_table_id;
				$("#"+subgrid_id).html("<table id="+subgrid_table_id+" class='scroll'></table><div id="+pager_id+" class='scroll'></div>");
				var from = jQuery("#from").val();
				var to = jQuery("#to").val();
				var tradeIdS = '';
				if(jQuery("#tradeIdS").val() != undefined)
				var	tradeIdS = jQuery("#tradeIdS").val();
				var transIdS = jQuery("#transIdS").val();
				var comMainGridID = jQuery("#comMainGridID").val();
				//alert(comMainGridID);
				var extraParamsSubGrid = "&from="+from+"&to="+to+"&tradeIdS="+tradeIdS+"&transIdS="+transIdS+"&Submit=SearchTrans";
				
				jQuery("#"+subgrid_table_id).jqGrid({
				
				
					url:"recurring_report_action.php?getGrid=intComReportSubGrid&nd="+new Date().getTime()+"&q=2&rid="+row_id+extraParamsSubGrid,
					footerrow:true,
					userDataOnFooter:true,
					datatype: "json",
					 height: 'auto',
					width: 680,
					caption: "Associated Transactions",
					colNames:['Reference Number','Transaction Status','Transaction Amount','Transaction Type','Exchange Rate'],
					colModel:[
						{name:'refNumberIM',index:'refNumberIM', width:27,align:'center'},
						{name:'transStatus',index:'transStatus', width:20, align:'center'},
						{name:'transAmount',index:'transAmount', width:24, align:'center'},
						{name:'transType',index:'transType', width:25, align:'center'},
						{name:'erate',index:'erate', width:25, align:'center'},
					
					],
					imgpath:subgridimgpath, 
					rowNum:maxRows,
					rowList: [20,50,100],
				//	pager: pager_id,
					sortname: 'refNumber',
				//	multiselect: true
			 
			     
				
				});
				
				
			//	jQuery("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,{edit:false,add:false,del:false})
				
				//alert('*');
			},	
			
			 subGridRowColapsed: function(subgrid_id, row_id) {
				// this function is called before removing the 
				var subgrid_table_id;
				subgrid_table_id = comSubGrid;//subgrid_id+"_t";
				jQuery("#"+subgrid_table_id).remove();
				$("#comMainGridID").val('');
			} 
			
		});
		jQuery("#comMainList").jqGrid('navGrid','#pagernav',{add:false,edit:false,del:false});
		jQuery('a').cluetip({splitTitle: '|'});
		
        // for View Recurring Rule
        jQuery("#btnActionView").click( function(){
			if($(this).val()=="View"){
				var payIDs = jQuery("#comMainList").getGridParam('selarrrow');
				//alert(payIDs);
				//alert(location.pathname);
				payIDs = payIDs.toString();
				if(payIDs!=''){
				  window.location.assign("view-transaction.php?action=&transID="+payIDs);
				}
				else
				{
				
				alert("Please Select Transaction to View");
				$("#allPayIdsV").val('');
				return false;
				 
				}
					
			
			}
		});			
		
		// for Edit Recurring Rule
        jQuery("#btnAction").click( function(){
			if($(this).val()=="Edit"){
				var payIDs = jQuery("#comMainList").getGridParam('selarrrow');
				//alert(payIDs);
				//alert(location.pathname);
				payIDs = payIDs.toString();
				if(payIDs!=''){
				 // window.location.assign("view-transaction.php?action=&transID="+payIDs);
	window.location.assign("add-transaction.php?transID="+payIDs+"&back=edit-transactions.php");
				}
				else
				{
				
				alert("Please Select Rule(s) to Edit");
				$("#allPayIdsV").val('');
				return false;
				
				}
					
			
			}
		});	
		
		//For Stoping Recurring Rule

         jQuery("#btnActionStop").click( function(){
			if($(this).val()=="Stop"){
				var payIDs = jQuery("#comMainList").getGridParam('selarrrow');
				//alert(payIDs);
				if(payIDs!=''){
				
					var confirmDel = confirm("Are you sure to Stop Recurring Rule(s)");
					if(confirmDel==false){
						return false;
					}
					
					
					$.ajax({
						url: 'recurring_report_action.php',
                         data: {
                         ajax: true,
                         stopRule:'true',
							payId:payIDs
                         },
                         async: false,
                         type: 'POST',
                         success: function(data, textStatus, XMLHttpRequest){ 
                                             gridReload('comMainList');    
                                                    //$("#rules").show();
                                               }		
                                        });
				}
				else{
					alert("Please Select Rule(s) to Stop");
					$("#allPayIdsV").val('');
					return false;
				}
			
			}
		});	

        //For Resuming Recurring Rule


         jQuery("#btnActionResume").click( function(){
			if($(this).val()=="Resume"){
				var payIDs = jQuery("#comMainList").getGridParam('selarrrow');
				if(payIDs!=''){
				
					var confirmDel = confirm("Are you sure to  Resume Recurring Rule(s)");
					if(confirmDel==false){
						return false;
					}
					
					
					$.ajax({
						url: 'recurring_report_action.php',
                         data: {
                         ajax: true,
                         resumeRule:'true',
							payId:payIDs
                         },
                         async: false,
                         type: 'POST',
                         success: function(data, textStatus, XMLHttpRequest){ 
                                              gridReload('comMainList');    
                                                    //$("#rules").show();
                                               }		
                                        });
				}
				else{
					alert("Please Select Rule(s) to Resume");
					$("#allPayIdsV").val('');
					return false;
				}
			
			
			}
		});				
	$("#from").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-1 years"))?>'
		});
	
	$("#to").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-1 years"))?>'
		});
	
	
	});

	function gridReload(grid)
	{
		var theUrl = "recurring_report_action.php";
		
		if(grid=='comMainList' || grid=='comSubGrid'){
			var extraParam;
			var from = jQuery("#from").val();
			var to = jQuery("#to").val();
			var custAgentID = '';
			if(jQuery("#custAgentID").val() != undefined)
			var	custAgentID = jQuery("#custAgentID").val();
			var currencyTrans = jQuery("#currencyTrans").val();
			var comMainGridID = jQuery("#comMainGridID").val();
			var txtField = jQuery("#txtField").val();
			var status = jQuery("#status").val();
			var searchBy = jQuery("#searchBy").val();

			if(grid=='comMainList'){
				extraParam = "?from="+from+"&to="+to+"&custAgentID="+custAgentID+"&currencyTrans="+currencyTrans+"&txtField="+txtField+"&status="+status+"&searchBy="+searchBy+"&Submit=SearchTrans&getGrid=intComReport";
			}
			else{
				extraParam = "?from="+from+"&to="+to+"&custAgentID="+custAgentID+"&currencyTrans="+currencyTrans+"&txtField="+$txtField+"&status="+status+"&searchBy="+searchBy+"&Submit=SearchTrans&getGrid=intComReportSubGrid&nd="+new Date().getTime()+"&q=2&rid="+comMainGridID;
			}
			jQuery("#"+grid).setGridParam({
				url: theUrl+extraParam,
				page:1
			}).trigger("reloadGrid");
		}
	}
	


</script>


</head>
<body>
<div id="loading"></div>
	<form name="frmSearch" id="frmSearch">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc" colspan="2"><b><strong><font color="#FFFFFF"><?=$strLabel?></font></strong></b></td>
  </tr>
  <tr>
	<td valign='top'>
	<table  border="0" style="float:left; vertical-align:top" >
    
    <tr>
    <td align="center">
      <table border="1" cellpadding="5" bordercolor="#666666">
      <tr>
            <td nowrap bgcolor="#6699cc"><span class="tab-u"><strong>Search Filter<? echo $from2 . $to;?></strong></span></td>
        </tr>
        <tr>
				
                  <td nowrap align="center">
				    From Date 
                  	<input name="fromDate" type="text" id="from" readonly >
                  	<!--input name="from" type="text" id="from" value="<?=$from1;?>" readonly-->
		    		    &nbsp;&nbsp; &nbsp;To Date&nbsp;
		    		    <input name="toDate" type="text" id="to"  readonly >
		    		    <!--input name="to" type="text" id="to" value="<?=$to1;?>" readonly-->
		    		    &nbsp;&nbsp;
		    		    <!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->	
             
            <select name="status" id="status">
			<option value="">- Status -</option>
			<option value="Active"> Active </option>
         	<option value="Close"> Close </option>
         	<option value="Hold"> Hold </option>
         	
		 
	  </select>
						
              </tr>
	  <tr>
         <td nowrap align="center">
	 
	     Search Transactions : <input type="text" name="txtField" id="txtField">
		  &nbsp;&nbsp;&nbsp;
	
			
			<select name="searchBy" id="searchBy">
			<option value=""> Search By </option>
         	<option value="SN"> Sender Name </option>
         	<option value="BN"> Beneficiary Name </option>
         	<option value="RN"> Reference Number </option>
		 
	  </select>
		  &nbsp;&nbsp;&nbsp;
		  
		  
		  Select Currency
          <select name="currencyTrans" id="currencyTrans">
           <option value="">Select Currency</option>
           <? if(count($currencyies)>0){?>
           <? for($cp=0;$cp<count($currencyies);$cp++){?>
           <option value="<?=$currencyies[$cp]["currencyName"]?>">
            <?=$currencyies[$cp]["currencyName"]?>
           </option>
           <? }?>
           <? }?>
          </select>		  
          &nbsp;&nbsp;&nbsp;
		<input type="button" name="Submit" value=" Search " onClick="gridReload('comMainList')">
		<input type="reset" name="reset" value="Reset">		 </td>
    </table>  	</td>
	</tr>
    
    <tr><td>&nbsp;</td></tr>
	<tr>
    <td valign="top">
    			<table id="comMainList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernav" class="scroll" style="text-align:center;"></div>
				<div id="payListPaidData" style="visibility:hidden;"></div>
				<div id="payListDeleteData" style="visibility:hidden;"></div>	</td>
    </tr>
	<?php if($agentType != 'SUPA' && $agentType != 'SUBA'){?>
	  <tr bgcolor="#FFFFFF">
	   <td align="center">
		<input name="btnActionView" id="btnActionView" type="button"  value="View">
		<input name="btnAction" id="btnAction" type="button"  value="Edit">
		<input name="btnActionStop" id="btnActionStop" type="button"  value="Stop">
		<input name="btnActionResume" id="btnActionResume" type="button"  value="Resume">
		<input type='hidden' name='comMainGridID' id='comMainGridID' value="">
		<input type="hidden" name="allTransIDsV" id="allTransIDsV" value="">
		<input type="hidden" name="confirmFlag" id="confirmFlag" value="">
		</td>
	  </tr>
	<?php }?>
</table>
	</td>
  </tr>
</table>
</form>
</body>
</html>