<?
session_start();
include ("../include/config.php");
include ("security.php");
include ("calculateBalance.php");

$agentType = getAgentType();

$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

//debug($_REQUEST);

session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");

$intUserId = $_REQUEST["agentID"];
$strSubmit = $_REQUEST["Submit"];

if($intUserId != "")
	$_SESSION["agentID2"] = $intUserId;
else 
	$_SESSION["agentID2"] = "";

$today = date("Y-m-d");
$msg = "";


//debug($insertQuery);


if($strSubmit == "Search")
{
	
	$_SESSION["grandTotal"]="";
	$_SESSION["CurrentTotal"]="";


	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";
			
		
	$_SESSION["fMonth"]=$_REQUEST["fMonth"];
	$_SESSION["fDay"]=$_REQUEST["fDay"];
	$_SESSION["fYear"]=$_REQUEST["fYear"];
	
	$_SESSION["tMonth"]=$_REQUEST["tMonth"];
	$_SESSION["tDay"]=$_REQUEST["tDay"];
	$_SESSION["tYear"]=$_REQUEST["tYear"];
	
	$_SESSION["closingBalance"] = "";
	$_SESSION["openingBalance"] = "";
	$_SESSION["currDate"] = "";	
	
}


if ($offset == "")
	$offset = 0;
$limit=100;
if ($_REQUEST["newOffset"] != "") {
	$offset = $_REQUEST["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;

$condition = false;
if($agentType == "admin") 
{
	$condition = true;
	$intUserId = $_REQUEST["agentID"];
}
else{
	$intUserId = $_SESSION["loggedUserData"]["userID"];
}
$currencyV = ($_POST["currency"]!=""?$_POST["currency"]:$_GET["currency"]);
 $ledgerTable = TBL_AGENT_ACCOUNT;
 if(isAnD($intUserId) || $intUserId=="all"){
	$ledgerTable = TBL_AnD_ACCOUNT;
 }

$accountQueryCurrency = "Select DISTINCT(currency) as totalCurrency from ".$ledgerTable." where 1 ";
$accountQueryCurrency .= " and (note LIKE 'INTER AnD%' OR note LIKE 'INTER AGENT%') ";
$contentsAccCurr = selectMultiRecords($accountQueryCurrency);
$totalCurrRec    = count($contentsAccCurr);
?>
<html>
<head>
	<title>Trial Balance</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">


var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

</script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
.boundingTable {
	border: solid 2px #666666;
}

.inputBoxesNumerics {
	text-align: right;
	width: 70px;
	font-size: 11px;
}

.inputBoxesText {
	width: 255px;
	font-size: 11px;
}

.inputBoxesCustom {
	font-size: 11px;
}

.dropDownBoxes {
	font-size: 11px;
}

.reportToolbar {
	background-color: #EEEEEE;
}

.errorMessage {
	color: #FF0000;
	font-weight: bold;
	font-size: 12px;
}

.successMessage {
	color: #009900;
	font-weight: bold;
	font-size: 12px;
}

.fieldComments {
	color: #999999;
}

.tdTitle { 
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:14px;
	color:#0033CC;
	padding-left:4px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	 <tr>
    <td class="topbar">Trial Balance </td>
  </tr>
	<tr>
      <td width="100%">
	<form action="" method="post" name="Search" id="Search">
  		<div align="center">From 
        <? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
                
                <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
                <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
                  <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
                To 
               
                <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </select>
                <script language="JavaScript">
					SelectOption(document.forms[0].tDay, "<?=$_SESSION["tDay"]; ?>");
				</script>
			 <SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
					SelectOption(document.forms[0].tMonth, "<?=$_SESSION["tMonth"]; ?>");
				</script>
                <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
               <?
				$cYear=date("Y");
				for ($Year=2004;$Year<=$cYear;$Year++)
				{
					echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
				}
			   ?>
               </SELECT>
                <script language="JavaScript">
					SelectOption(document.forms[0].tYear, "<?=$_SESSION["tYear"]; ?>");
				</script>
                <br>
                &nbsp;&nbsp; 
                <? 
					if($condition) 
					{ 
						$agentQuery  = "
										select 
											userID,
											username, 
											agentCompany, 
											name, 
											agentContactPerson,
											adminType 
										from 
											".TBL_ADMIN_USERS." 
										where 
											    parentID > 0 
											AND adminType='Agent' 
											AND isCorrespondent = 'Y' 
											AND agentStatus='Active' 
										order by 
											adminType, 
											name	";
						//debug($agentQuery);
						$arrStaff = selectMultiRecords($agentQuery);
						//debug($arrStaff);
				?>
				<br />
				Select Agent : &nbsp;&nbsp;
				<select name="agentID" style="font-family:verdana; font-size: 11px">
					<option value="all" <?=($intUserId=="all"?"selected":"")?>>All</option>
                	<?
						for($i=0; $i < count($arrStaff); $i++)
						{
							if($arrStaff[$i]["adminType"] != $arrStaff[$i-1]["adminType"])
								echo "<optgroup label='".$arrStaff[$i]["adminType"]."'>";
							
							if($intUserId == $arrStaff[$i]["userID"]) { ?>
								<option value="<?=$arrStaff[$i]["userID"]?>" selected="selected"><?=$arrStaff[$i]["name"] . " [" . $arrStaff[$i]["username"] . "]"?></option>
							<? } else { ?>
								<option value="<?=$arrStaff[$i]["userID"]?>"><?=$arrStaff[$i]["name"] . " [" . $arrStaff[$i]["username"] . "]"?></option>
							<? } 

							if($arrStaff[$i]["adminType"] != $arrStaff[$i+1]["adminType"])
								echo "</optgroup>";

						}
					?>
                </select>
                 <? } ?>
				<br /><br />
				Select Currency : &nbsp;&nbsp;
				<select name="currency" id="currency" style="font-family:verdana; font-size: 11px">
					<option value="all" <?=($currencyV=="all"?"selected":"")?>>All</option>
                	<?
						for($tc=0; $tc < $totalCurrRec; $tc++)
						{
						?>
								<option value="<?=$contentsAccCurr[$tc]["totalCurrency"]?>" <?=($currencyV==$contentsAccCurr[$tc]["totalCurrency"]?"selected":"")?>><?=$contentsAccCurr[$tc]["totalCurrency"]?></option>
						<? 
						}
					?>
                </select>
				<br />				 
				<input type="hidden" id="latestCleared" name="latestCleared" value="" />
                <input type="submit" name="Submit" id="Submit" value="Search" /> 
              </div>
        
		<tr>
		<td>
		
      <?
	 if(!empty($strSubmit))
	 {
	 	
	 	$Balance="";
		if($strSubmit == "Search")
			$slctID = $intUserId;
	
		$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
		$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
		$queryDate = "(ac.dated >= '$fromDate 00:00:00' and ac.dated <= '$toDate 23:59:59')";
		$accountWhereClaues =" ";
		if($intUserId!="all"){
			$accountWhereClaues .= " AND agentID = $slctID";
		}
		if($currencyV!="all"){
			$accountWhereClaues .= " AND currency='".$currencyV."'";
		}
		$accountWhereClaues .= " AND $queryDate ";
		$accountWhereClaues .= " AND status != 'Provisional' ";
		$accountWhereClaues .= " AND (note LIKE 'INTER AnD%' OR note LIKE 'INTER AGENT%') ";

		if ($slctID !=""){
		
			//$accountQuery = "Select * from ".$ledgerTable." where 1 ";
	$accountQuery = "Select ac.*,ad.username as acCode,ad.name as acName from ".$ledgerTable." ac,".TBL_ADMIN_USERS." ad where ac.TransID IN(SELECT TransID from ".$ledgerTable." WHERE 1 ".$accountWhereClaues.") AND ac.agentID=ad.userID ";

				
			$accountQueryCnt = "Select count(*) from ".$ledgerTable." ac,".TBL_ADMIN_USERS." ad where ac.TransID IN(SELECT TransID from ".$ledgerTable." WHERE 1 ".$accountWhereClaues.") AND ac.agentID=ad.userID ";
			$allCount = countRecords($accountQueryCnt);

			$accountQuery .= " order by ac.aaID DESC";
	  	$accountQuery .= " LIMIT $offset , $limit"; 
			
			$contentsAcc = selectMultiRecords($accountQuery);
		}else{
		
			$slctID = $changedBy;
			$accountQuery="Select ac.*,ad.username as acCode,ad.name as acName from ".$ledgerTable." ac,".TBL_ADMIN_USERS." ad where ac.TransID IN(SELECT TransID from ".$ledgerTable." WHERE 1 ".$accountWhereClaues.") AND ac.agentID=ad.userID ";
				
			$accountQueryCnt = "Select count(*) from ".$ledgerTable." ac,".TBL_ADMIN_USERS." ad where ac.TransID IN(SELECT TransID from ".$ledgerTable." WHERE 1 ".$accountWhereClaues.") AND ac.agentID=ad.userID ";
			
			$allCount = countRecords($accountQueryCnt);
			//echo $queryBen;
			$accountQuery .= " order by ac.aaID DESC";
		    $accountQuery .= " LIMIT $offset , $limit"; 
			
			$contentsAcc = selectMultiRecords($accountQuery);
		}
		//debug($accountQuery);
		

		?>	
			
		  <table width="76%" border="1" cellpadding="0" bordercolor="#666666" align="center">
		  	<? if(!empty($msg)) 
			   { 
			   		$parts = explode("|", $msg);
			?>
					<tr>
						<td align="center" class="<?=$parts[1]?>">
							<?=$parts[0]?>z
						</td>
					</tr>
			<? 
			  } 
			
			 $strExtraUrlParams = "&fDay=".$_REQUEST["fDay"]."&fMonth=".$_REQUEST["fMonth"]."&fYear=".$_REQUEST["fYear"].
			 					  "&tDay=".$_REQUEST["tDay"]."&tMonth=".$_REQUEST["tMonth"]."&tYear=".$_REQUEST["tYear"]."&agentID=".$intUserId."&Submit=Search";
			
			
			?>
			<tr>
			  <td height="25" nowrap bgcolor="#6699CC">
			  <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
				  <tr>
				  <td align="left"><?php if (count($contentsAcc) > 0) {;?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
					<?=$allCount?>
					<?php } ?>
				  </td>
				  <?php if ($prv >= 0) { ?>
				  <td width="50"><a href="<?=$PHP_SELF."?newOffset=0&total=first".$strExtraUrlParams?>"><font color="#005b90">First</font></a></td>
				  <td width="50" align="right"><a href="<?=$PHP_SELF."?newOffset=$prv&total=pre".$strExtraUrlParams?>"><font color="#005b90">Previous</font></a> </td>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <td width="50" align="right"><a href="<?=$PHP_SELF."?newOffset=$nxt&total=next".$strExtraUrlParams?>"><font color="#005b90">Next</font></a>&nbsp;</td>
				  <td width="50" align="right">&nbsp; </td>
				  <?php } ?>
				</tr>
				
			  </table></td>
			</tr>
			
			<?
			if(count($contentsAcc) > 0)
			{
				$contentsCurr = array();
				$debitAmount  = array();
				$creditAmount = array();
				$rowSpan="";
				if($totalCurrRec>1){
					$rowSpan="rowspan='2'";
				}
			?>
			<tr>
			  <td nowrap bgcolor="#EFEFEF">
			  <table width="100%" border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#FFFFFF" valign="baseline">
					  <td align="center" width="102" <?=$rowSpan?>><span class="style1">Date</span></td>
					  <td align="center"  width="159" <?=$rowSpan?>><span class="style1">A/C Code</span></td>
					  <td align="center" width="188" <?=$rowSpan?>><span class="style1">A/C Name</span></td>
					  <td align="center"  width="106" <?=$rowSpan?>><span class="style1">Currency</span></td>
					  <?
					  	$colSpan="";
					  	if($totalCurrRec>1){
							$colSpan = "colspan='".($totalCurrRec)."'";
						}
					  ?>
					  <td align="center" width="112" <?=$colSpan;?>><span class="style1">Debit</td>
					  <td align="center" width="120" <?=$colSpan?>>Credit</td>
					</tr>
					  <? 
					  	if($totalCurrRec>1){
							$printCurr = "<tr bgcolor='#FFFFFF'>";
							for($curr=0;$curr<$totalCurrRec;$curr++){
								$printCurr .= "<td align='center'><span class='style1'>".$contentsAccCurr[$curr]["totalCurrency"]."</span></td>";
							}
							for($curr=0;$curr<$totalCurrRec;$curr++){
								$printCurr .= "<td align='center'><span class='style1'>".$contentsAccCurr[$curr]["totalCurrency"]."</span></td>";
							}
							$printCurr .= "</tr>";
							echo $printCurr;
					  	}
				
				$preDate = "";
				$openingBalanceSwap = "";
				if($_SESSION["currDate"] != "")
					$preDate = $_SESSION["currDate"];
				if($_SESSION["closingBalance"] != "")	
					$closingBalance = $_SESSION["closingBalance"];
				if($_SESSION["openingBalance"] != "")	
					$openingBalance = $_SESSION["openingBalance"];
				for($i=0;$i < count($contentsAcc);$i++)
				{
					$contentsCurr[]=$contentsAcc[$i]["currency"];
					$currDate = $contentsAcc[$i]["dated"];
					$currentAmount = $contentsAcc[$i]["amount"]; 
					$_SESSION["currDate"] = $currDate;
				?>
				<tr bgcolor="#FFFFFF">
					<td align="center" <?=$rowSpan;?>><strong><font color="#006699"><?
					$authoriseDate = $contentsAcc[$i]["dated"];
					echo date("F j, Y", strtotime("$authoriseDate"));
					?></font></strong></td>
					<td align="center" <?=$rowSpan;?>><?=$contentsAcc[$i]["acCode"];?></td>
					<td align="center" <?=$rowSpan;?>><?=$contentsAcc[$i]["acName"];?></td>
			        <td align="center" <?=$rowSpan;?>><?=$contentsAcc[$i]["currency"];?></td> 
					  
					  <?
						if($contentsAcc[$i]["type"] == "WITHDRAW")
						{
							for($cu=0;$cu<$totalCurrRec;$cu++){
								
								/************/
									/*$currencyTo = 'GBP';
									$currencyFrom = $contentsAcc[$i]["currency"];
									$exchangeSql = "select primaryExchange,marginType from " . TBL_EXCHANGE_RATES . " where  (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo'  and currencyOrigin = '$currencyFrom ' and isDenomination='N' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' and isDenomination='N')";
									$exchangeRate = selectFrom($exchangeSql);
									if(!empty($exchangeRate["primaryExchange"])){
									$amountGBP = $currentAmount * $exchangeRate["primaryExchange"];
									//echo "<td align='center' ".$rowSpan.">".$amountGBP."</td>";
									//debug($amountGBP);
									}*/
								/***********/
								
								if(strtoupper($contentsAccCurr[$cu]["totalCurrency"])==strtoupper($contentsAcc[$i]["currency"]))
								{
									$debitAmount[$cu] += $currentAmount;
									echo "<td align='center' ".$rowSpan.">".$currentAmount."</td>";
								}
								else
								{
								echo "<td align='center' ".$rowSpan.">&nbsp;</td>";
								}
							}
						}
						else{
							for($cu=0;$cu<$totalCurrRec;$cu++){
								echo "<td align='center' ".$rowSpan.">&nbsp;</td>";
							}
						}
 
					if($contentsAcc[$i]["type"] == "DEPOSIT")
					{
						for($cu=0;$cu<$totalCurrRec;$cu++){
							if(strtoupper($contentsAccCurr[$cu]["totalCurrency"])==strtoupper($contentsAcc[$i]["currency"])){
								$creditAmount[$cu] += $currentAmount;
								echo "<td align='center' ".$rowSpan.">".$currentAmount."</td>";
							}
							else{
								echo "<td align='center' ".$rowSpan.">&nbsp;</td>";
							}
						}
					}
					else{
						for($cu=0;$cu<$totalCurrRec;$cu++){
								echo "<td align='center' ".$rowSpan.">&nbsp;</td>";
						}
					}
					?>
		</tr>
			<? 
				 $totalColspan=6;
				if($totalCurrRec>1){
					$totalColspan +=$totalCurrRec;
			?>
			<tr bgcolor="#FFFFFF"></tr>
			<? }?>

		<? } ?>
		<tr bgcolor="#FFFFFF">
		   <td colspan='"<?=$totalColspan;?>"'>&nbsp;</td>
		</tr>
		<tr bgcolor="#FFFFFF" style="border:dotted;">
 			  <td colspan="4"  <?=$rowSpan;?>>&nbsp;</td>
			  <td align="center" width="112" <?=$colSpan;?> style="border:double;border-right:none;"><span class="style1">Debit</span></td>
			  <td align="center" width="120" <?=$colSpan?> style="border:double;"><span class="style1">Credit</span></td>
			</tr>
 		    <? 
				if($totalCurrRec>1){
					$printCurr = "<tr bgcolor='#FFFFFF'>";
					for($curr=0;$curr<$totalCurrRec;$curr++){
						$printCurr .= "<td align='center'><span class='style1'>".$contentsAccCurr[$curr]["totalCurrency"]."</span></td>";
					}
					for($curr=0;$curr<$totalCurrRec;$curr++){
						$printCurr .= "<td align='center'><span class='style1'>".$contentsAccCurr[$curr]["totalCurrency"]."</span></td>";
					}
					$printCurr .= "</tr>";
					echo $printCurr;
				}
			?>
		<tr bgcolor="#FFFFFF"></tr>

		<tr bgcolor="#FFFFFF">
			<td colspan="3"  <?=$rowSpan;?>>&nbsp;</td>
			<td align="center"  <?=$rowSpan;?>><strong>Total</strong></td>
			  <? 
				// To show Total Debit Amount
				for($d=0;$d<$totalCurrRec;$d++){
					if(strtoupper($contentsAccCurr[$d]["totalCurrency"])==strtoupper($contentsCurr[$d])){
						echo "<td align='center' ".$rowSpan.">".$debitAmount[$d]."</td>";
					}
					else{
						echo "<td align='center'  ".$rowSpan.">&nbsp;</td>";
					}
				}
				// To show Total Credit Amount
				for($c=0;$c<$totalCurrRec;$c++){
					if(strtoupper($contentsAccCurr[$c]["totalCurrency"])==strtoupper($contentsCurr[$c])){
						echo "<td align='center' ".$rowSpan.">".$creditAmount[$c]."</td>";
					}
					else{
						echo "<td align='center'  ".$rowSpan.">&nbsp;</td>";
					}
				}
			?>
			</tr>
		<tr bgcolor="#FFFFFF"></tr>
		<tr bgcolor="#FFFFFF">
			<td colspan="3"  <?=$rowSpan;?>>&nbsp;</td>
			<td align="center"  <?=$rowSpan;?>><strong>Balance</strong></td>
			  <?
				// To show Total Balance Amount
				for($bd=0;$bd<$totalCurrRec;$bd++){
					if(strtoupper($contentsAccCurr[$bd]["totalCurrency"])==strtoupper($contentsCurr[$bd])){
						echo "<td align='center'  ".$rowSpan."> 0  "."</td>";
					}
					else{
						echo "<td align='center'  ".$rowSpan.">&nbsp;</td>";
					}
				}
				// To show Total Balance Amount
				for($bc=0;$bc<$totalCurrRec;$bc++){
					if(strtoupper($contentsAccCurr[$bc]["totalCurrency"])==strtoupper($contentsCurr[$bc])){
						echo "<td align='center' ".$rowSpan."> 0 "."</td>";
					}
					else{
						echo "<td align='center'  ".$rowSpan.">&nbsp;</td>";
					}
				}
			?>
		</tr>
		<tr bgcolor="#FFFFFF"></tr>
			</table>		
            <? } else { ?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Date and Agent </font>
				</td>
			</tr>
			<? } ?>

<?php /*?>	  		<? if($agentType == "admin") { ?>
			<tr bgcolor="#FFFFFF">
              <td colspan="15" align="left">
				  		<a href="#" onClick="$('#manual-row').show();" style="color:#0066FF">Manual Deposit/Withdraw into AnD account?</a>
					&nbsp;
			  </td>
            </tr>
			<? } ?><?php */?>
       </table>
	   
	<?	
		 } 
		 else 
		 {
			$_SESSION["grandTotal"]="";
			$_SESSION["CurrentTotal"]="";
	 
	
			$_SESSION["fMonth"]="";
			$_SESSION["fDay"]="";
			$_SESSION["fYear"]="";
			
			$_SESSION["tMonth"]="";
			$_SESSION["tDay"]="";
			$_SESSION["tYear"]="";
		}
	?>
		</td>
	</tr>
	<? if($slctID!=''){?>
		<tr>
		  	<td> 
				<div align="center">
					<input type="button" name="Submit2" value="Print This Report" onClick="print()">
				</div>
			</td>
		  </tr>
	<? }?>
	</form>
	</td>
	</tr>
</table>
</body>
</html>