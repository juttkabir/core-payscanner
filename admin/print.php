<?
	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	$agentType = getAgentType();

	if ( empty($_REQUEST["dataContainerId"]) )
		exit();

?>
<html>
<head>
<title>Agent Account Statement</title>
<link rel="stylesheet" href="css/reports.css" type="text/css">
<script>
	function printData()
	{
		document.getElementById("printableRegion").innerHTML = opener.document.getElementById("<?=$_REQUEST["dataContainerId"];?>").innerHTML;
		print();
	}
</script>
</head>
<body onload="printData();">
<table id='printableRegion'>
</table>
</body>
</html>