<?
/**
 * @package Online Register Users
 * @subpackage New Registered Senders List
 * This page Shows the Search filters for the search date filters and records
 * @author Mirza Arslan Baig
 */

session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
if(CONFIG_SECURE_ALL_PAGES == '0' )
include_once ("secureAllPages.php");
//include ("javaScript.php");
//$arrIDTypeData	= selectMultiRecords("SELECT id, title FROM id_types WHERE 1 ORDER BY title ASC");
?>
<html>
<head>
<title>Prepaid Customer List</title>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee/grid.css" title="coffee" media="screen" />
<!--<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />-->
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />
<link href="css/datePicker.css" rel="stylesheet" type="text/css" />

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script src="javascript/date.js" type="text/javascript"></script>
<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
<script type="text/javascript" src="javascript/jqGrid/js/grid.locale-en.js"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<!--<script src="javascript/jqGrid/js/grid.celledit.js" type="text/javascript"></script>-->
<!--<script src="javascript/jqGrid/js/grid.inlineedit.js" type="text/javascript"></script>-->
<!--<script src="jquery.cluetip.js" type="text/javascript"></script>-->
<!--<script language="javascript" src="javascript/jquery.autocomplete.js"></script>-->
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var extraParams;
	jQuery(document).ready(function(){
		// Date Picker
		Date.format = 'yyyy-mm-dd';
		$('#fdate').datePicker({clickInput:true});
		$('#fdate').dpSetStartDate('1990-01-01');
		
		$('#tdate').datePicker({clickInput:true});
		$('#tdate').dpSetStartDate('1990-01-01');
		var lastSel;
		jQuery("#getList").jqGrid({
			url:'prepaid-registration_conf.php?getGrid=getList&Submit=Search',
			datatype: "json",
			height: 210,
			width: 900, 
			colNames:[
				'Sr.', 
				'Customer Name', 
				'Reference Number',
				'Status',
				'Creation Date',
				'Country',
				
				
				'Action',
				
				'Browser Name',
				'Customer Services'
			],
			colModel:[
				{name:'sr',	index:'sr', width:25,	align:"center",sortable:false},
				{name:'customerName',index:'customerName', width:100,align:"left",editable:false,sortable:false },
				{name:'accountName',index:'accountName',width:100,align:"left",editable:false,sortable:false},
				{name:'status',	index:'status',	width:50, align:"left"},
				{name:'created', index:'created', width:80, align:"left"},
				{name:'Country', index:'Country', width:80, align:"left", editable:false},
				
				{name:'Action',index:'Action',width:80,align:"left",sortable:false},
				
				{name:'browserName',index:'browserName',width:100,align:"left",sortable:false},
				{name:'online_customer_services',index:'online_customer_services',width:300,align:"left",sortable:false}
			],
			imgpath:gridimgpath, 
			rowNum: 10,
			rowList: [5,10,20,50,100,200],
			pager: jQuery('#pagernavT'),
			multiselect:true,
			sortname: "created",
			sortorder: "DESC",
			viewrecords: true,
			loadonce: false,
			multiselect:false,
			loadui: "block",
			loadtext: "Loading ...",
			caption: "Prepaid Customer List"
		}).navGrid('#pagernavT',
				{edit:false, add:false, del:false, search:false},
				{afterSubmit: editMessage, closeAfterEdit:true}, 	// edit options
      			{}, 	// add options
      			{} 		// del options
			);
			
		$('#Submit').click(
			function(){
				if($('#fdate').val() == '' && $('#tdate').val() == '' && $('#cur').val() == '' && $('#custNum').val() == ''){
					alert("Please give filter values to search");
					$('#custNum').focus();
					return;
				}
				if($('#tdate').val() == ''){
					/* alert("Please give to Date");
					$('#tdate').click();
					return; */
				}
				gridReload('getList');
			}
		);
		
		$('#export').click(
			function(){
				gridReload('export');
			}
		);
		
		$('#print').click(
			function(){
				gridReload('print');
			}
		);
		
		<?php 
			if($_REQUEST['refresh']=='1'){
		?>
			setTimeout("gridReload('getList')",900);
			
		<?php 
			}
		?>
		
	});
	
	// define handler function for 'afterSubmit' event.
	var editMessage = function(response, postdata){
		var json   = response.responseText;		// response text is returned from server.
		var result = JSON.parse(json);			// convert json object into javascript object.
		return [result.status,result.message,null]; 
	}
	
	
	
	
	function gridReload(grid)
	{
		var theUrl = "prepaid-registration_conf.php";		
		var extraParam='';

		var fdate	= jQuery("#fdate").val();
		var tdate	= jQuery("#tdate").val();
		var cur 	= jQuery("#cur").val();
		var customerNumber 		= jQuery("#custNum").val();
		extraParam = "?getGrid="+grid+"&fdate="+fdate+"&tdate="+tdate+"&cur="+cur+"&custNum="+customerNumber+"&Submit=Search";
		
		if(grid == 'print' || grid == 'export')
		{
			exportList(extraParam);
		}
		else if(grid=='getList')
		{
			jQuery("#getList").setGridParam({
				url: theUrl+extraParam,
				page:1
			}).trigger("reloadGrid");
		}
	}
	
	function exportList(extraParam){
		window.open("prepaid-registration_conf.php"+extraParam,'ExportDailyTransaction','height=600,width=1000,location=1,status=1,scrollbars=1');
	}
	
	function customerEdit(senderID, type){
		if(type != 'company')
			window.open("add-customer.php?customerID="+senderID+"&page=1", '_blank', 'scrollbars=yes,toolbar=no, location =no,status=no,menubar=no,resizeable=yes,height=300,width=800');
	}
	
	function customerAction(senderID){
		window.open("disable_sender.php?customerID="+senderID+'&page=senders_list_registered_from_website', '_blank', 'scrollbars=no,toolbar=no,location=no,status=no,menubar=no,resizeable=yes,height=300,width=800');
	}
	
</script>
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000}
.style1 {color: #005b90}
.style4 {color: #005b90; font-weight: bold; }

#searchTable
{
	background-color:#ededed;
	border:1px solid #000000;
}
#searchTable tr td
{
	border-top:1px solid #FFFFFF;
}
.dp-choose-date{
	margin-left:5px;
}
div.scroll span{
	line-height:17px;
	margin-left:5px;
}
-->
</style>
<script language="javascript">
/*
function clearDates()
{
	document.frmSearch.from.value = "";
	document.frmSearch.to.value = "";
	return true;
}
*/


function IssueCard(customerID){

$.ajax({
 		url: '../premier_fx_online_module/ajaxCalls.php',
		method : '',
		dataType: "json",
		async : false,
		data : {
			'rtypeMode' : 'IssueCardRequest',
			'customerID': customerID
		},
		success : function(data){
			if(data.Status == 0){
			
				alert('card Issued Successfully');
				gridReload('getList');
			}else{
				alert('could not issue the card . kindly contact administrator');
			}
		  }
		}); 
	}
	
</script>
</head>
<body>
<div style=" margin:5px; padding-left:5px; height:30px; line-height:30px;background-color:#6699cc; color:#FFFFFF"><strong>Prepaid New Registration<strong></div>
	<form name="frmSearch" id="frmSearch">
	<table width="100" border="0" align="center">
		<tr>
			<td>
				<table id="searchTable" align="center" border="0" cellpadding="5" bordercolor="#666666">
					<tr>
						<th align="left"colspan="4" bgcolor="#6699cc"><span class="tab-u">Search Prepaid Customers </span></th>
					</tr>
					<tr>
						<td align="center" colspan="2">
							From <input type="text" readonly="readonly" name="fdate" id="fdate" value="<?php if($_REQUEST['refresh']=='1') echo $_SESSION['fdate']; ?>"/>
							To 	<input type="text" readonly="readonly" name="tdate" id="tdate" value="<?php if($_REQUEST['refresh']=='1') echo $_SESSION['tdate']; ?>"/>
						</td>
					</tr>
					<tr>
						<td nowrap align="right">Currency</td>
						<td>
						<select style="font-family:verdana; font-size: 11px; width:100;" name="cur" id="cur">
							  <option value=""> - Currency - </option>
							  <option value="All" <?php if($_SESSION['cur'] == 'All' && $_REQUEST['refresh']=='1') echo "selected='selected'"; ?>>All</option>
							  <option value="USD" <?php if($_SESSION['cur'] == 'USD' && $_REQUEST['refresh']=='1') echo "selected='selected'"; ?>>USD</option>
							  <option value="EUR" <?php if($_SESSION['cur'] == 'EUR' && $_REQUEST['refresh']=='1') echo "selected='selected'"; ?>>EUR</option>
						</select>
						</td>
					</tr>
					<tr>
						<td nowrap align="right">Reference Number</td>
						<td>
							<input id="custNum" name="custNum" style="WIDTH: 140px;HEIGHT: 18px; " value="<?php if(isset($_REQUEST['refresh'])) echo $_SESSION['customerNumber']; ?>"/>
						</td>
					</tr>
					<tr>
						<td colspan="4" nowrap align="center">
							<input id="Submit" type="button" name="Submit" value="Search" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table id="getList" border="0" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernavT" class="scroll" style="text-align:center;"></div>
			</td>
		</tr>
		<tr>
			<td align="center">
				<input type="button" name="Submit" id="print" value="Print All" />
				<input type="button" name="Submit" id="export" value="Export All" />
			</td>
		</tr>
	</table>
</form>
</body>
</html>