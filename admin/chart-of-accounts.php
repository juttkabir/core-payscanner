<?
	session_start();

	include ("../include/config.php");
	include ("security.php");
	$agentType = getAgentType();
	include ("javaScript.php");
	//$currentdate = date('d-m-Y  h:i:s A');
	$currentDate		= (!empty($currentDate))?$currentDate:date("Y-m-d", time());
    $print = $_REQUEST["print"];
	
//	debug($_REQUEST);
	
   
	$strAccountChart = "SELECT 
						   id,
						   accountName,
						   accountNumber,
						   description,
						   currency 
					  FROM 
						 accounts_chart
					  WHERE 
						status = 'AC'
					  ";
				 //debug($strAccountChart);
				$accountChartRs = selectMultiRecords($strAccountChart);
				
	
	
	// get total GBP of stock amount
	$total_stock_amount = number_format(getTotalStockAmount(),2,'.',',');
	
	$curr_exch_acc = '';
	if(defined("CONFIG_CURRENCY_EXCHANGE_ACCOUNT"))
	$curr_exch_acc = CONFIG_CURRENCY_EXCHANGE_ACCOUNT;
	if(!empty($_REQUEST["date_report"]) && !empty($_REQUEST["Submit"]))
	{
		$date_report	= $_REQUEST["date_report"];
		$qrDate=explode("/",$_REQUEST["date_report"]);
		$date_report	= $qrDate[2] . "-" . $qrDate[1] . "-" . $qrDate[0];	
		$queryDated = " and dated <= '$date_report' ";	
		$qrCreationDate=" and created <= '$date_report 23:59:59'";	
		
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Chart of Account</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<style>
.ce{
	color:#0066FF;
	cursor:pointer;
}
</style>
<script src="javascript/jquery.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script type="text/javascript">
$(document).ready(function(){
		$("#date_report").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
		});
	});
<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}


function popup(url) 
{
 var width  = 800;
 var height = 800;
 var left   = (screen.width  - width)/2;
 var top    = (screen.height - height)/2;
 var params = 'width='+width+', height='+height;
 params += ', top='+top+', left='+left;
 params += ', directories=no';
 params += ', location=no';
 params += ', menubar=no';
 params += ', resizable=yes';
 params += ', scrollbars=yes';
 params += ', status=no';
 params += ', toolbar=no';
 newwin=window.open(url,'windowname5', params);
 if (window.focus) {newwin.focus()}
 return false;
}

function exportChartOfAc(){
	var exportFormat 	= document.getElementById("exportFormat").value;
	
	if(exportFormat!="")
	{
		window.open('exportChartAccount.php?exportType='+exportFormat+'&date_report='+'<?=$date_report?>','Export Chart Of Account', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420');
	}
	else
	{
		alert("Please Select export type");
		return false;	
	}
}
function hideDivs(){
document.getElementById("printRow").style.display="none";
//document.location.reload();
}

// -->

</script>
</head>
<body>

<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<TD width="40%"  height="60" align="right" valign="middle"><img id=Picture2 height="60" alt="" src="<?=CONFIG_LOGO?>" width="60" border=0></TD>
					<td align="left" class="reportHeader"><?=SYSTEM?> (CHART OF ACCOUNT AS ON)<br />
					<span class="reportSubHeader"><?=date("l, d F, Y", strtotime($currentDate))?><br />
					<br />
					</span>
					</td>
				 </tr>
			</table>
		</td>
	</tr>
<form action="" method="post" name="searchForm">
<tr>
	<td valign='top'>
		<table width="80%" border="0" align="center" cellpadding="3" cellspacing="0" class="boundingTable">			 
			<tr>
				<td align="left"><b>Report Date:</b> </td>
				
				<td> <input type="text" name="date_report" id="date_report" value="<?=$_REQUEST["date_report"]?>" /></td>
				
			</tr>
			<br />
			<tr>
				<td align="center" colspan="4">
				<input type="submit" name="Submit" value="Search">							
				</td>
			</tr>
		</table>
	</td>
</tr>
</form>
<form action="" method="post" name="searchForm">
<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<br />

			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%" class="columnTitle">&nbsp;</td>
					<td width="25%" class="columnTitle">&nbsp;</td>
					<td width="10%" class="columnTitle">&nbsp;</td>
					<td colspan="2" width="30%" align="left" class="columnTitle">Amount in Operational Currency </td>
					<td colspan="2" width="30%" align="left" class="columnTitle">Amount in Foreign Currency </td>
				</tr>
				<tr>
					<td width="10%" class="columnTitle">&nbsp;A/C</td>
					<td width="25%" class="columnTitle">&nbsp;CONTROL NAME </td>
					<td width="10%" class="columnTitle">&nbsp;CUR</td>
					<td width="15%" align="left" class="columnTitle">&nbsp;Dr. </td>
					<td width="15%" align="left" class="columnTitle">&nbsp;Cr. </td>
					<td width="15%" align="left" class="columnTitle">&nbsp;Dr. </td>
					<td width="15%" align="left" class="columnTitle">&nbsp;Cr. </td>
				</tr>
				<tr>
					<td align="left" colspan="7">&nbsp;</td>
				</tr>
				<?
				   // debug(count($accountChartRs));
				   $drSum = 0;
				   $crSum = 0;
				   $drSumF = 0;
				   $crSumF = 0;
					for($i=0; $i < count($accountChartRs); $i++){
					
					 $strAccount = "SELECT 
									   id,
									   accounNumber,
									   accountName,
									   currency,
									   balanceType,
									   balance 
								  FROM 
									 accounts
								  WHERE 
									status = 'AC' AND
									accounType = '".$accountChartRs[$i]["accountNumber"]."' ".$qrCreationDate."";
					// AND	currency = 'GBP'			  
			         //debug($strAccount);
					 $accountRs = selectMultiRecords($strAccount);
					 /*$drBalance = array();
					 $crBalance = array();
					 for($s=0; $s < count($accountRs); $s++){ 
						if($accountRs[$s]["balanceType"] == 'Dr')
						{
							$drBalance[] = $accountRs[$s]["balance"];
							$crBalance[] = 0;
						}
						else
						{
							$drBalance[] = 0;
							$crBalance[] = $accountRs[$s]["balance"];
						}					 
					}*/
				?>
				<tr>
					<td  width="10%" align="left" class="heading">&nbsp;<?=$accountChartRs[$i]["accountNumber"]?></td>
					<td width="25%" align="left" class="heading">&nbsp;<?=$accountChartRs[$i]["accountName"]?></td>					
					<td width="10%" align="left" class="heading">&nbsp;<?=$accountChartRs[$i]["currency"]?></td>
					<td align="left" class="heading">&nbsp;</td>
					<td align="left" colspan="3" class="heading">&nbsp;</td>
				</tr> 
				
				<? if(count($accountRs) > 0 ) { ?>
				<tr><td colspan="7">&nbsp;</td></tr>
				<? 
				for($k=0; $k < count($accountRs); $k++){ 
					$drBalance = 0;
					$crBalance = 0;
					$drBalance1 = 0;
					$crBalance1 = 0;
				   if(empty($_REQUEST["date_report"]))
				  	 $date_report ='';
				   $date_to ='';
				   $strClosingBalance = "
							SELECT 
								id,
								closing_balance,
								dated,
								accountNumber 
							FROM 
								".TBL_ACCOUNT_SUMMARY." 
							WHERE
								accountNumber = '".$accountRs[$k]["accounNumber"]."' AND
								dated = (select MAX(dated) as dated FROM ".TBL_ACCOUNT_SUMMARY." 
								WHERE accountNumber = '".$accountRs[$k]["accounNumber"]."' ".$queryDated.")
							 ";
				  		$closingBalance = selectFrom($strClosingBalance);
						//debug($strClosingBalance);
					
					
					/*
					If closing balances of any account is in - (negative) then it appear as Debit Entry in this report
					If closing balances of any account is in + (positive) then it appear as Credit Entry in this report
					*/
					//debug($accountRs[$k]['balance']);
					if($accountRs[$k]['balanceType'] == 'Dr')
						$drBalance =$closingBalance["closing_balance"] - $accountRs[$k]['balance'];
					elseif($accountRs[$k]['balanceType'] == 'Cr')
						$crBalance =$closingBalance["closing_balance"] + $accountRs[$k]['balance'];
					//debug($accountRs[$k]['balance']);
					//debug($closingBalance["closing_balance"]);
					//debug($accountRs[$k]['balanceType']);	
					/*if(strstr($closingBalance["closing_balance"],'-'))
					  $drBalance += trim($closingBalance["closing_balance"],'-');
					 else
					  $crBalance += $closingBalance["closing_balance"];*/
				if($accountRs[$k]["currency"] == "GBP"){	  
				 if($drBalance < 0)
				 	$drSum += $drBalance;
				 if($drBalance > 0)	
				 	$crSum += $drBalance;
				 if($crBalance < 0)
				 	$drSum += $crBalance;
				 if($crBalance > 0)	
				 	$crSum += $crBalance;	
				}else{ 
					if($drBalance < 0)
						$drSumF += $drBalance;
					if($drBalance > 0)	
						$crSumF += $drBalance;
					if($crBalance < 0)
						$drSumF += $crBalance;
					if($crBalance > 0)	
						$crSumF += $crBalance;
					$strCurrency=$accountRs[$k]["currency"];	
					$cur_sql="select cID from currencies where currencyName ='$strCurrency'";
					$buying_rate_arr_id = selectFrom($cur_sql); 
					$cur_sql="select buying_rate from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."' and created_at = (select max(created_at) from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."')";
					//debug($cur_sql);
					$buying_rate_arr = selectFrom($cur_sql); 
					//debug($buying_rate_arr);
					$buying_rate=$buying_rate_arr[0];
				}
				$show_total_amount = '';
				if($accountRs[$k]["accounNumber"] == $curr_exch_acc)
					$show_total_amount = '<strong>Stock='.$total_stock_amount.'<strong>';
//debug($date_report);
				 if(!empty($closingBalance["dated"]))
				 	$date_report = date("d/m/Y", strtotime($closingBalance["dated"]));
					//debug($date_report);
				?>
				<tr>
					<td width="10%" align="left">&nbsp;<?=$accountRs[$k]["accounNumber"]?></td>
					<td width="25%" align="left">&nbsp;
					 <a class="heading" "javascript: void(0)" onclick="popup('view-ledgers.php?from=chartAccount&account=<?=$accountRs[$k]["accounNumber"]?>&date_from=<?=$date_report?>&date_to=<?=$date_report?>&Submit=Submit')">
					 <?=$accountRs[$k]["accountName"]?></a><?=' '.$show_total_amount?>
					</td>					
					<td width="10%" align="left">&nbsp;<?=$accountRs[$k]["currency"]?></td>
					<td align="left">&nbsp;<?php 
					 $BALANCE = "0.00";
					if($accountRs[$k]["currency"] == "GBP"){
						if($drBalance<0 ){
							$BALANCE = number_format(abs($drBalance), 2, ".", ",");
						}
						else if($crBalance<0){
							$BALANCE = number_format(abs($crBalance), 2, ".", ",");
						}
					}else{
						if($drBalance<0 ){
							$drBalance1=$drBalance/$buying_rate;
							$BALANCE = number_format(abs($drBalance1), 2, ".", ",");
						}
						else if($crBalance<0){
							$crBalance1=$crBalance/$buying_rate;
							$BALANCE = number_format(abs($crBalance1), 2, ".", ",");
						}
						 
					}
					echo $BALANCE;
				/*	else if($accountRs[$k]["accounNumber"] != "11223344"){
					echo number_format(abs($drBalance), 2, ".", ",");
					
					}*/
					
					
					?></td>
					<td align="left">&nbsp;<?php 
					$res = selectFrom("select * from account_summary");
					//debug($drBalance ." -- ".$crBalance."<br />");
					//debug($res);
					 $BALANCE = "0.00";
					if($accountRs[$k]["currency"] == "GBP"){
						if($drBalance>0 ){
							 $BALANCE =number_format(abs($drBalance), 2, ".", ",");
							}
						else if($drBalance == 0 && $crBalance>0 ){
							 $BALANCE =number_format($crBalance, 2, ".", ",");
							}
					}else{
						if($drBalance>0 ){
							$drBalance1=$drBalance/$buying_rate;
						    $BALANCE =number_format(abs($drBalance1), 2, ".", ",");
							}
						else if($drBalance == 0 && $crBalance>0 ){
							$crBalance1=$crBalance/$buying_rate;
							$BALANCE =number_format($crBalance1, 2, ".", ",");
							}
							 if($drBalance < 0)
								$drSum += $drBalance1;
							 if($drBalance > 0)	
								$crSum += $drBalance1;
							 if($crBalance < 0)
								$drSum += $crBalance1;
							 if($crBalance > 0)	
								$crSum += $crBalance1;
					}
					echo $BALANCE;
					/*else if($accountRs[$k]["accounNumber"] != "11223344"){
					echo number_format($crBalance, 2, ".", ",");
					
					}*/
					?></td>
					<td align="left">&nbsp;<?php 
					if($accountRs[$k]["currency"] != "GBP"){
					 $BALANCE = "0.00";
						if($drBalance<0 ){
							$BALANCE = number_format(abs($drBalance), 2, ".", ",");
						}
						else if($crBalance<0){
							$BALANCE = number_format(abs($crBalance), 2, ".", ",");
						}
						echo $BALANCE;
					}
					
				/*	else if($accountRs[$k]["accounNumber"] != "11223344"){
					echo number_format(abs($drBalance), 2, ".", ",");
					
					}*/
					
					
					?></td>
					<td align="left">&nbsp;<?php 
					$res = selectFrom("select * from account_summary");
					//debug($drBalance ." -- ".$crBalance."<br />");
					//debug($res);
					if($accountRs[$k]["currency"] != "GBP"){
					 $BALANCE="0.00";
						if($drBalance>0 ){
							 $BALANCE=number_format(abs($drBalance), 2, ".", ",");
							}
						else if($drBalance == 0 && $crBalance>0 ){
							 $BALANCE=number_format($crBalance, 2, ".", ",");
							}
							echo $BALANCE;
					}
					/*else if($accountRs[$k]["accounNumber"] != "11223344"){
					echo number_format($crBalance, 2, ".", ",");
					
					}*/
					?></td>
					
					
				</tr> 
				<? } ?>
				<tr><td colspan="7">&nbsp;</td></tr>
				<?
				   }
				 }
				 
				  ?>
				
				<tr>
					<td align="left" colspan="7">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="heading">&nbsp; TOTAL </td>
					<td class="heading">&nbsp;</td>
					<td align="left" class="heading"><?=number_format($drSum,2, ".", ",")?></td>
					<td align="left" class="heading"><?=number_format($crSum,2, ".", ",")?></td>
					<td align="left" class="heading"><?=number_format($drSumF,2, ".", ",")?></td>
					<td align="left" class="heading"><?=number_format($crSumF,2, ".", ",")?></td>
				</tr>
				<tr>
					<td align="left" colspan="7">&nbsp;</td>
				</tr>
				<tr id="printRow">
					<td>&nbsp;</td>
					<td class="heading">&nbsp;  Select Format:&nbsp;&nbsp;
					<select name="exportFormat" id="exportFormat">
					<option value="">Select an Option</option>				
					<option value="xls">EXCEL</option>
					<option value="csv">CSV</option>
					<option value="html">HTML</option>
				  </select>
				  <input name="btnExport" id="btnExport" type="button"  value="Export" onClick="exportChartOfAc();">
             		<script language="JavaScript">
         	    		SelectOption(document.forms[0].exportFormat, "<?=$_REQUEST["exportFormat"]; ?>");
             		</script>
					</td>
					
					<td align="left" class="heading">&nbsp;<input type="button" name="Submit2" value=" Print " onClick="hideDivs();print()"></td>
					<td colspan='5' align="left" class="heading">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
</table>
</body>
</html>
			