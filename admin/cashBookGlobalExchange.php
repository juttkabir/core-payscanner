<?
	session_start();

	include ("../include/config.php");
	include ("security.php");
	
	/**
	 * Variables from Query String
	 */
	$cmd 				= $_REQUEST["cmd"];
	$print				= $_REQUEST["print"];
	$openingBalance 	= $_REQUEST["openingBalance"];
	$banked				= $_REQUEST["banked"];
	$cheques			= $_REQUEST["cheques"];
	$othersDr			= $_REQUEST["othersDr"];
	$othersCr			= $_REQUEST["othersCr"];
	$totalDr			= $_REQUEST["totalDr"];
	$totalCr			= $_REQUEST["totalCr"];
	$id 				= $_REQUEST["id"];
	$currentDate		= $_REQUEST["currentDate"];
	$bankName			= $_REQUEST["bankName"];
	$courier			= $_REQUEST["courier"];
	$voucherNumber		= $_REQUEST["voucherNumber"];
	$bankAmount			= $_REQUEST["bankAmount"];
	
	/**
	 * Some pre data preparation.
	 */
	$openingBalance 	= (!empty($openingBalance))?trim($openingBalance):"0";
	$banked				= (!empty($banked))?trim($banked):"0";
	$cheques			= (!empty($cheques))?trim($cheques):"0";
	$othersDr			= (!empty($othersDr))?trim($othersDr):"0";
	$othersCr			= (!empty($othersCr))?trim($othersCr):"0";
	$totalDr			= (!empty($totalDr))?trim($totalDr):"0";
	$totalCr			= (!empty($totalCr))?trim($totalCr):"0";
	$currentDate		= (!empty($currentDate))?$currentDate:date("Y-m-d", time());
	$allowUpdate		= ($currentDate == date("Y-m-d", time()))?true:false;

	/**
	 * Check for data validity
	 */
	$openingBalance 	= (is_numeric($openingBalance))?$openingBalance:"0";
	$banked 			= (is_numeric($banked))?$banked:"0";
	$cheques 			= (is_numeric($cheques))?$cheques:"0";
	$othersDr 			= (is_numeric($othersDr))?$othersDr:"0";
	$othersCr 			= (is_numeric($othersCr))?$othersCr:"0";
	$totalDr 			= (is_numeric($totalDr))?$totalDr:"0";
	$totalCr 			= (is_numeric($totalCr))?$totalCr:"0";
	
	/**
	 * Calculation of Closing Balance
	 */
	if(sizeof($bankAmount) > 0)
	{
		$banked = 0;
		
		for($i=0; $i<sizeof($bankAmount); $i++)
		{
			if(is_numeric($bankAmount[$i]) && $bankAmount[$i] != 0)
			{
				$banked += $bankAmount[$i];
			}
		}
	}	
	
	$totalDr += $banked + $cheques + $othersDr;
	$totalCr += $othersCr;
	$closingBalance = ($openingBalance + $totalCr) - $totalDr;
	
	/**
	 * Save data
	 */
	
	if($cmd == "ADD")
	{
		$sql = "INSERT INTO
					cash_book_global_exchange(
						openingBalance,
						closingBalance,
						banked,
						cheques,
						othersDr,
						othersCr,
						created
					)
				VALUES(
					$openingBalance,
					$closingBalance,
					$banked,
					$cheques,
					$othersDr,
					$othersCr,
					NOW()
				)
				";
		$result = mysql_query($sql) or die(mysql_error());
		$cashBookId = mysql_insert_id();
		
		for($i=0; $i<sizeof($bankAmount); $i++)
		{
			if(is_numeric($bankAmount[$i]) && $bankAmount[$i] != 0)
			{
				$sql = "INSERT INTO
							cash_book_map_global_exchange(
								cashBookId,
								bankId,
								courierId,
								voucherNumber,
								bankAmount
							)
						VALUES(
							'".$cashBookId."',
							'".$bankName[$i]."',
							'".$courier[$i]."',
							'".$voucherNumber[$i]."',
							".$bankAmount[$i]."
						)
						";
				$result = mysql_query($sql) or die(mysql_error());
			}
		}
		
		unset($cmd);
	}
	
	if($cmd == "UPDATE")
	{
		$sql = "UPDATE
					cash_book_global_exchange
				SET
					closingBalance = $closingBalance,
					banked = $banked,
					cheques = $cheques,
					othersDr = $othersDr,
					othersCr = $othersCr,
					updated = NOW()
				WHERE
					recId = ".$id."
				";
		$result = mysql_query($sql) or die(mysql_error());
		
		for($i=0; $i<sizeof($bankAmount); $i++)
		{
			if(is_numeric($bankAmount[$i]) && $bankAmount[$i] != 0)
			{
				// check for existance
				$sql = "SELECT
							recId
						FROM
							cash_book_map_global_exchange
						WHERE
							cashBookId = ".$id."
							AND bankId = '".$bankName[$i]."'
						";
				$result = mysql_query($sql) or die(mysql_error());
				$numrows = mysql_num_rows($result);
				
				if($numrows > 0)
				{
					$sql = "UPDATE
								cash_book_map_global_exchange
							SET
								courierId = '".$courier[$i]."',
								voucherNumber = '".$voucherNumber[$i]."',
								bankAmount = ".$bankAmount[$i]."
							WHERE
								cashBookId = ".$id."
								AND bankId = '".$bankName[$i]."'
							";
				} else {
					$sql = "INSERT INTO
							cash_book_map_global_exchange(
								cashBookId,
								bankId,
								courierId,
								voucherNumber,
								bankAmount
							)
						VALUES(
							'".$id."',
							'".$bankName[$i]."',
							'".$courier[$i]."',
							'".$voucherNumber[$i]."',
							".$bankAmount[$i]."
						)
						";
				}
				
				$result = mysql_query($sql) or die(mysql_error());
			}
		}
		
		unset($cmd);
	}
	
	/**
	 * Fetch data based on table
	 */
	$todayExists = false;
	$allowEnterOpeningBalance = false;
	$sql = "SELECT 
				*
			FROM
				cash_book_global_exchange
			WHERE
				created = '".$currentDate."'
			";
	$result = mysql_query($sql) or die(mysql_error());
	$numrows = mysql_num_rows($result);
	
	if($numrows > 0)
	{
		$todayExists = true;
		$cmd = "UPDATE";
	}
	
	if(!$todayExists)
	{
		$sql = "SELECT 
				*
			FROM
				cash_book_global_exchange
			ORDER BY
				created DESC
			LIMIT
				0,1
			";
		$result = mysql_query($sql) or die(mysql_error());
		$numrows = mysql_num_rows($result);
		
		if($numrows <= 0)
		{
			$allowEnterOpeningBalance = true;
		} else {
			//$cmd = "UPDATE";
		}
	}
	
	while($rs = mysql_fetch_array($result))
	{
		$recId					= $rs["recId"];
		$originalDate			= $rs["created"];
		
		if($todayExists)
		{
			$openingBalance		= $rs["openingBalance"];
			$closingBalance		= $rs["closingBalance"];
			$banked				= $rs["banked"];
			$cheques			= $rs["cheques"];
			$othersDr			= $rs["othersDr"];
			$othersCr			= $rs["othersCr"];
		} else {
			$openingBalance		= $rs["closingBalance"];
			$closingBalance		= 0;
			$banked				= 0;
			$cheques			= 0;
			$othersDr			= 0;
			$othersCr			= 0;
		}
	}
	
	// Fetch from mapping table
	if(!empty($recId))
	{
		$sql = "SELECT
					*
				FROM
					cash_book_map_global_exchange
				WHERE
					cashBookId = ".$recId."
				";
		$result = mysql_query($sql) or die(mysql_error());
		$numrows = mysql_num_rows($result);
		$map = array();
	
		if($numrows > 0)
		{
			while($rs = mysql_fetch_array($result))
			{
				if(!empty($cmd) && $cmd != "ADD")
				{
					$map[$rs["bankId"]]["COURIER"] = $rs["courierId"];
					$map[$rs["bankId"]]["VOUCHER"] = $rs["voucherNumber"];
					$map[$rs["bankId"]]["AMOUNT"] = $rs["bankAmount"];
				}
			}
		}
	}
	
	/**
	 * Fetch Distributors Deposits and Withdraws
	 */
	$distributorsArray = array();
	
	// Fetch Deposits
	$sql = "SELECT
				a.userID, 
				a.username, 
				a.name, 
				sum(d.amount) AS DEPOSITS
			FROM
				bank_account d, 
				admin a
			WHERE
				d.dated = '".$currentDate."'
				AND d.type = 'DEPOSIT'
				AND d.bankID = a.userID
			GROUP BY
				d.bankID
			";
	$result = mysql_query($sql) or die(mysql_error());
	
	while($rs = mysql_fetch_array($result))
	{
		$distributorsArray[$rs["userID"]]["userID"] = $rs["userID"];
		$distributorsArray[$rs["userID"]]["username"] = $rs["username"];
		$distributorsArray[$rs["userID"]]["name"] = $rs["name"];
		$distributorsArray[$rs["userID"]]["DEPOSITS"] = abs($rs["DEPOSITS"]);
		$distributorsArray[$rs["userID"]]["WITHDRAWS"] = 0;
	}
	
	// Fetch Withdraws
	/**
	$sql = "SELECT
				a.userID, 
				a.username, 
				a.name, 
				sum(d.amount) AS WITHDRAWS
			FROM
				bank_account d, 
				admin a
			WHERE
				d.dated = '".$currentDate."'
				AND d.type = 'WITHDRAW'
				AND d.bankID = a.userID
			GROUP BY
				d.bankID
			";
	**/
	$sql = "SELECT
				a.userID, 
				a.username, 
				a.name, 
				sum(t.totalAmount) AS WITHDRAWS
			FROM
				bank_account d, 
				admin a,
				transactions t
			WHERE
				d.dated = '".$currentDate."'
				AND d.type = 'WITHDRAW'
				AND d.bankID = a.userID
				AND d.bankID = t.benAgentID
				AND d.TransID = t.transID
			GROUP BY
				d.bankID
			";
	$result = mysql_query($sql) or die(mysql_error());
	
	while($rs = mysql_fetch_array($result))
	{
		$distributorsArray[$rs["userID"]]["userID"] = $rs["userID"];
		$distributorsArray[$rs["userID"]]["username"] = $rs["username"];
		$distributorsArray[$rs["userID"]]["name"] = $rs["name"];
		
		if(empty($distributorsArray[$rs["userID"]]["DEPOSITS"]))
		{
			$distributorsArray[$rs["userID"]]["DEPOSITS"] = 0;
		}
		
		$distributorsArray[$rs["userID"]]["WITHDRAWS"] = abs($rs["WITHDRAWS"]);
	}
	
	/**
	 * Fetch Agents Outstanding Received (Manual Deposits)
	 */
	$sql = "SELECT
				sum(amount) AS MANUAL_DEPOSITS
			FROM
				agent_account
			WHERE
				type = 'DEPOSIT'
				AND TransID = ''
				AND dated = '".$currentDate."'
				AND agentID != ".CONFIG_PAYIN_AGENT_NUMBER;
	$result = mysql_query($sql) or die(mysql_error());
	
	while($rs = mysql_fetch_array($result))
	{
		$agentManualDeposits = empty($rs["MANUAL_DEPOSITS"])?0:abs($rs["MANUAL_DEPOSITS"]);
	}
	
	/**
	 * Fetch Agents Outstanding
	 */
	/** Discarded - but left until finalized. See new logic next to this comment block.
	$sql = "SELECT
				sum(amount) AS MANUAL_WITHDRAWS
			FROM
				agent_account
			WHERE
				type = 'WITHDRAW'
				AND TransID != ''
				AND dated = '".$currentDate."'
				AND agentID != ".CONFIG_PAYIN_AGENT_NUMBER;
	$result = mysql_query($sql) or die(mysql_error());
	
	while($rs = mysql_fetch_array($result))
	{
		$agentManualWithdraws = empty($rs["MANUAL_WITHDRAWS"])?0:abs($rs["MANUAL_WITHDRAWS"]);
	}
	**/

	// Fetch Total Transactions Amount
	$agentTotalTransactions = 0;
	$sql = "SELECT
				sum(amount) AS TOTAL
			FROM
				agent_account as a,
				transactions as t
			WHERE
				type = 'WITHDRAW'
				AND a.TransID != ''
				AND (t.transStatus = 'Authorize' or t.transStatus = 'Amended') 
				AND t.transId = a.TransID
				AND dated = '".$currentDate."' 
				AND agentID != ".CONFIG_PAYIN_AGENT_NUMBER."
			";
	$result = mysql_query($sql) or die(mysql_error());

	if($rs = mysql_fetch_array($result))
	{
		$agentTotalTransactions = empty($rs["TOTAL"])?0:abs($rs["TOTAL"]);
	}
	
	// Fetch Received Amount
	$agentTotalReceived = 0;
	$sql = "SELECT
				sum(amount) AS RECEIVED
			FROM
				agent_account
			WHERE
				type = 'DEPOSIT'
				AND TransID != ''
				AND dated = '".$currentDate."' 
				AND agentID != ".CONFIG_PAYIN_AGENT_NUMBER."
			";
	$result = mysql_query($sql) or die(mysql_error());
	
	while($rs = mysql_fetch_array($result))
	{
		$agentTotalReceived = empty($rs["RECEIVED"])?0:abs($rs["RECEIVED"]);
	}
	
	// Calculate Outstanding
	$agentTotalOutstanding = abs($agentTotalTransactions /*- $agentTotalReceived*/);
	
	/**
	 * Fetch Customer Outstanding Received from Dummy Agent (defined in CONFIG_PAYIN_AGENT_NUMBER) Deposits with Transaction ID
	 */
	$sql = "SELECT
				sum(amount) AS DEPOSITS
			FROM
				agent_account
			WHERE
				agentID = ".CONFIG_PAYIN_AGENT_NUMBER."
				AND type = 'DEPOSIT'
				AND TransID = ''
				AND dated = '".$currentDate."'
			";
			
	$result = mysql_query($sql) or die(mysql_error());
	
	while($rs = mysql_fetch_array($result))
	{
		$customerOutstandingReceived = empty($rs["DEPOSITS"])?0:abs($rs["DEPOSITS"]);
	}
	
	/**
	 * Fetch Customer Outstanding from Dummy Agent (defined in CONFIG_PAYIN_AGENT_NUMBER)
	 */
	 
	// Withdraws (All)
	$sql = "SELECT
				sum(amount) AS WITHDRAWS
			FROM
				agent_account
			WHERE
				agentID = ".CONFIG_PAYIN_AGENT_NUMBER."
				AND type = 'WITHDRAW'
				AND dated = '".$currentDate."'
			";
	$result = mysql_query($sql) or die(mysql_error());
	
	while($rs = mysql_fetch_array($result))
	{
		$customerOutstanding = empty($rs["WITHDRAWS"])?0:abs($rs["WITHDRAWS"]);
	}
	
	if(CONFIG_EFFECT_LEDGERS_WITH_DOUBLE_ENTRY)
	{
		// Deposits (All)
		$sql = "SELECT
					sum(amount) AS DEPOSITS
				FROM
					agent_account
				WHERE
					agentID = ".CONFIG_PAYIN_AGENT_NUMBER."
					AND type = 'DEPOSIT'
					AND dated = '".$currentDate."'
				";
		$result = mysql_query($sql) or die(mysql_error());
		
		while($rs = mysql_fetch_array($result))
		{
			$customerDeposits = empty($rs["DEPOSITS"])?0:abs($rs["DEPOSITS"]);
			$customerOutstanding = $customerOutstanding - $customerDeposits;
		}
	}
	
	/**
	 * Fetch Cancelled Transactions from Agent (all) Deposits with Transaction ID
	 */
	$sql = "SELECT
				sum(amount) AS CANCELLED
			FROM
				agent_account
			WHERE
				type = 'DEPOSIT'
				AND dated = '".$currentDate."'
				AND TransID != '' 
				AND description = 'Transaction Cancelled'
			";
	$result = mysql_query($sql) or die(mysql_error());
	
	while($rs = mysql_fetch_array($result))
	{
		$cancelled = empty($rs["CANCELLED"])?0:abs($rs["CANCELLED"]);
	}
	
	$tmpTotalDr = 0;
	$tmpTotalCr = 0;
	
	if(empty($cmd))
	{
		$cmd = "ADD";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cash Book</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
<link href="css/jquery.cluetip.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jquery-1.2.5.min.js"></script>
<script type="text/javascript" src="jquery.cluetip.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('a').cluetip({splitTitle: '|'});
});
</script>
</head>

<body>
<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
    <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="center" class="reportHeader"><?=SYSTEM?>
                    (Cash Book)<br />
                    <span class="reportSubHeader">
                        <?=date("l, d F, Y", strtotime($currentDate))?>
                        <br />
                        <br />
                    </span></td>
            </tr>
        </table>
                <br />
                <form action="cashBookGlobalExchange.php" method="post">
                    <table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
                        <tr>
                            <td width="70%" class="columnTitle">Particulars</td>
                            <td width="10%" align="right" class="columnTitle">Dr. &pound; </td>
                            <td width="10%" align="right" class="columnTitle">Cr. &pound;</td>
                            <td width="10%" align="right" class="columnTitle">Balance &pound;</td>
                        </tr>
                        <tr>
                            <td class="heading">Opening Balance </td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">
								<?
									if($allowEnterOpeningBalance && $allowUpdate)
									{
								?>
										<input name="openingBalance" type="text" class="inputBoxesNumerics" id="openingBalance" value="0" size="15" />
								<?
									}else{
										echo number_format($openingBalance, 2, ".", ",");
								?>
										<input type="hidden" name="openingBalance" id="openingBalance" value="<?=$openingBalance?>" />
								<?
									}
								?>
							</td>
                        </tr>
                        <tr>
                            <td>Distributors</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
						<?
							$totalDr = 0;
							$totalCr = 0;
						
							foreach($distributorsArray as $key)
							{
						?>
								<tr>
									<td>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<?=$key["name"]." [".$key["username"]."]"?>
									</td>
									<td align="right"><?=number_format(abs($key["DEPOSITS"]), 2, ".", ",")?></td>
									<td align="right"><?=number_format($key["WITHDRAWS"], 2, ".", ",")?></td>
									<td align="right">&nbsp;</td>
								</tr>
						<?	
								$totalDr += (abs($key["DEPOSITS"]));
								$totalCr += $key["WITHDRAWS"];
								
								$tmpTotalDr += (abs($key["DEPOSITS"]));
								$tmpTotalCr += $key["WITHDRAWS"];
							}
						?>
                        
                        <tr>
                            <td><a href="agentsDepositReport.php?currentDate=<?=$currentDate?>" target="_blank" title="Agents Outstanding Received|Click to open the report to see the break down of agents payment received.">Agents Outstanding Received</a> </td>
                            <td align="right">&nbsp;</td>
                            <td align="right"><?=number_format($agentManualDeposits, 2, ".", ",")?></td>
                            <td align="right">
								<?
									$totalCr += $agentManualDeposits;
									$tmpTotalCr += $agentManualDeposits;
								?>
							</td>
                        </tr>
						<tr>
                            <td><a href="agentsOutstandingReport.php?currentDate=<?=$currentDate?>" target="_blank" title="Agents Outstanding|Click to open the report to see the break down of agents outstanding.">Agents Outstanding</a></td>
                            <td align="right"><?=number_format($agentTotalOutstanding, 2, ".", ",")?></td>
                            <td align="right">&nbsp;</td>
                            <td align="right">
								<?
									$totalDr += $agentTotalOutstanding;
									$tmpTotalDr += $agentTotalOutstanding;
								?>
							</td>
                        </tr>
                        <tr>
                            <td>Customer Outstanding Received</td>
                            <td align="right">&nbsp;</td>
                            <td align="right"><?=number_format($customerOutstandingReceived, 2, ".", ",")?></td>
                            <td align="right">
								<?
									$totalCr += $customerOutstandingReceived;
									$tmpTotalCr += $customerOutstandingReceived;
								?>
							</td>
                        </tr>
                        <tr>
                            <td><a href="customersOutstandingReport.php?currentDate=<?=$currentDate?>" target="_blank" title="Customer Outstanding|Click to open the report to see the break down of customers outstanding.">Customer Outstanding</a> </td>
                            <td align="right"><?=number_format($customerOutstanding, 2, ".", ",")?></td>
                            <td align="right">&nbsp;</td>
                            <td align="right">
								<?
									$totalDr += $customerOutstanding;
									$tmpTotalDr += $customerOutstanding;
								?>
							</td>
                        </tr>
                        <tr>
                            <td>
								Banked<br />
								<table border="0" cellpadding="3" cellspacing="0" width="100%">
									<tr>
										<td>&nbsp;</td>
										<td><strong>Bank Name</strong></td>
										<td><strong>Courier Service</strong></td>
										<td><strong>Voucher Number</strong></td>
										<td align="right"><strong>Amount</strong></td>
									</tr>
									<?
										$courierSql = "SELECT
															*
														FROM
															couriers_lookup
														ORDER BY
															courierName ASC
													  ";
										$courierResult = mysql_query($courierSql) or die(mysql_error());
										$couriers = array();
										$couriersCounter = 0;
										
										while($courierRs = mysql_fetch_array($courierResult))
										{
											$crecId			= $courierRs["recId"];
											$ccourierId		= $courierRs["courierId"];
											$ccourierName	= $courierRs["courierName"];
											
											$couriers[$couriersCounter]["ID"] = $ccourierId;
											$couriers[$couriersCounter]["NAME"] = $ccourierName;
											
											$couriersCounter++;
										}
									
										$banksSql = "SELECT
														*
													 FROM
													 	banks_lookup
													 ORDER BY
													 	bankName ASC
													 ";
										$bankResult = mysql_query($banksSql) or die(mysql_error());
										$banksTotal = 0;
										
										while($banksRs = mysql_fetch_array($bankResult))
										{
											$brecId			= $banksRs["recId"];
											$bbankId		= $banksRs["bankId"];
											$bbankName		= $banksRs["bankName"];
											$banksTotal		+= $map[$bbankId]["AMOUNT"];
									?>
											<tr>
												<td>&nbsp;</td>
												<td>
													<?=$bbankName?>
													<input type="hidden" name="bankName[]" id="bankName[]" value="<?=$bbankId?>" />												</td>
												<td>
													<?
														if($allowUpdate)
														{
													?>
															<select name="courier[]" id="courier[]" class="dropDownBoxes">
													<?
														}
														
														for($counter=0; $counter<sizeof($couriers); $counter++)
														{
															$selected = "";
															
															if($map[$bbankId]["COURIER"] == $couriers[$counter]["ID"])
															{
																$selected = "selected=\"selected\"";
																$echoCourier = $couriers[$counter]["NAME"];
															}
															
															if($allowUpdate)
															{
													?>
																<option value="<?=$couriers[$counter]["ID"]?>" <?=$selected?>><?=$couriers[$counter]["NAME"]?></option>
													<?
															}
														}
																
														if($allowUpdate)
														{
													?>
															</select>
													<?
														} else {
															echo $echoCourier;
															
															$echoCourier = "";
														}
													?>												</td>
												<td>
													<?
														if($allowUpdate)
														{
													?>
															<input type="text" name="voucherNumber[]" id="voucherNumber[]" value="<?=$map[$bbankId]["VOUCHER"]?>" size="10" maxlength="20" class="inputBoxesCustom" />
													<?
														} else {
															echo $map[$bbankId]["VOUCHER"];
														}
													?>												</td>
												<td align="right">
													<?
														if($allowUpdate)
														{
													?>
															<input type="text" name="bankAmount[]" id="bankAmount[]" value="<?=number_format($map[$bbankId]["AMOUNT"], 2, ".", ",")?>" size="10" class="inputBoxesNumerics" />
													<?
														} else {
															echo number_format($map[$bbankId]["AMOUNT"], 2, ".", ",");
														}
													?>												</td>
											</tr>
									<?
										}
									?>
								</table>
								<br />
							</td>
                            <td align="right" valign="bottom">
								<?
									if($banked == 0)
									{
										$banked = $banksTotal;
									}
								
									if($allowUpdate)
									{
								?>
										<input name="banked" type="text" class="inputBoxesNumerics" id="banked" value="<?=number_format($banked, 2, ".", "")?>" size="15" readonly="readonly" />
								<?
									} else {
										echo number_format($banked, 2, ".", ",");
									}
									
									$tmpTotalDr += $banked;
								?>
							</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>

                        <tr>
                            <td>Cheque(s) Deposited </td>
                            <td align="right">
								<?
									if($allowUpdate)
									{
								?>
										<input type="text" name="cheques" id="cheques" class="inputBoxesNumerics" value="<?=number_format($cheques, 2, ".", "")?>" size="15" />
								<?
									} else {
										echo number_format($cheques, 2, ".", ",");
									}
									
									$tmpTotalDr += $cheques;
								?>
							</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        
						<tr>
                            <td>Cancelled Transactions </td>
                            <td align="right"><?=number_format($cancelled, 2, ".", ",")?></td>
                            <td align="right">&nbsp;</td>
                            <td align="right">
								<?
									$totalDr += $cancelled;
									$tmpTotalDr += $cancelled;
								?>
							</td>
                        </tr>
						
						<tr>
                            <td>Others</td>
                            <td align="right">
								<?
									if($allowUpdate)
									{
								?>
										<input type="text" name="othersDr" id="othersDr" class="inputBoxesNumerics" value="<?=number_format($othersDr, 2, ".", "")?>" size="15" />
								<?
									} else {
										echo number_format($othersDr, 2, ".", ",");
									}
									
									$tmpTotalDr += $othersDr;
								?>
							</td>
                            <td align="right">
								<?
									if($allowUpdate)
									{
								?>
										<input type="text" name="othersCr" id="othersCr" class="inputBoxesNumerics" value="<?=number_format($othersCr, 2, ".", "")?>" size="15" />
								<?
									} else {
										echo number_format($othersCr, 2, ".", ",");
									}
									
									$tmpTotalCr += $othersCr;
								?>
							</td>
                            <td align="right">&nbsp;</td>
                        </tr>
						
						<tr>
                            <td>&nbsp;</td>
                            <td align="right"><strong><?=number_format($tmpTotalDr, 2, ".", ",")?></strong></td>
                            <td align="right"><strong><?=number_format($tmpTotalCr, 2, ".", ",")?></strong></td>
                            <td align="right">&nbsp;</td>
                        </tr>
						
                        <tr>
                            <td class="heading">Closing Balance of the Day </td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right" class="netBalance" id="closingFinal">
								<?=number_format(($openingBalance + $tmpTotalCr) - $tmpTotalDr, 2, ".", ",")?>
							</td>
                        </tr>
                    </table>
                    <br />
                    <?
				if($print != "Y")
				{
			?>
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td width="50%" class="reportToolbar"><input name="btnPrint" type="button" id="btnPrint" value=" Print " onclick="javascript: location.href='cashBookGlobalExchange.php?print=Y&amp;currentDate=<?=$currentDate?>';" /></td>
                            <td width="50%" align="right" class="reportToolbar">
									<input name="id" type="hidden" id="id" value="<?=$recId?>" />
                                    <input name="cmd" type="hidden" id="cmd" value="<?=$cmd?>" />
									<input name="totalDr" type="hidden" id="totalDr" value="<?=$totalDr?>" />
									<input name="totalCr" type="hidden" id="totalCr" value="<?=$totalCr?>" />
                                    <?
										if($allowUpdate == "Y")
										{
									?>
											<input type="submit" name="Submit" value="Submit" />
											<input type="reset" name="Submit2" value="Reset" />
									<?
										}
									?>
							</td>
                        </tr>
                    </table>
                    <?
				}
			?>
            </form></td>
    </tr>
</table>
<?
	if($print == "Y")
	{
?>
		<script type="text/javascript">
			print();
		</script>
<?
	}
?>
</body>
</html>
