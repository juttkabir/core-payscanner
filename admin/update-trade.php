<?
session_start();
/** 
 *  Short description
 *	This page is used to view all the trades giving the 
 *	functionality of updating,canceling or viewing them
 *  @package Trading 
 *  @subpackage Edit/Cancel Trade
 *  @copyright HBS Tech. (Pvt) Ltd.
 */
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType2 = getAgentType();



$parentID = $_SESSION["loggedUserData"]["userID"];


$sql_agent_based="";
if($agentType2=='SUPA' ||  $agentType2=='SUBA' )
{
$sql_agent_based=" AND created_by='".$parentID."'";
}


// define page navigation parameters
if ($offset == "")
	$offset = 0;
$limit=20;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = "acid"; 
	//$sortBy = "created"; 

if($_GET["searchBy"])
{
	$searchBy = $_GET["searchBy"];	
	$qryString .= "&searchBy=" . $_GET["searchBy"];
}
$Country = "";
if ($_GET["Country"] != ""){
	$Country = $_GET["Country"];
	$where .= " and agentCountry='".$_GET["Country"]."'";
	$qryString .= "&Country=" . $_GET["Country"];
}
if ($_GET["agentStatus"] != ""){
	$agentStatus = $_GET["agentStatus"];
	$where .= " and agentStatus='".$_GET["agentStatus"]."'";
	$qryString .= "&agentStatus=" . $_GET["agentStatus"];
}

if($searchBy == 1)
{
	
	if ($_GET["agentCode"] != ""){
		$agentCode = $_GET["agentCode"];
		$where .= " and (accountSell = '".$_GET["agentCode"]."' OR accountBuy = '".$_GET["agentCode"]."')";
		$qryString .= "&accountSell=" . $_GET["agentCode"];
		$qryString .= "&accountBuy=" . $_GET["agentCode"];
	}
}elseif($searchBy == 2){

if ($_GET["agentCode"] != ""){
		$agentCode = $_GET["agentCode"];
		$where .= " and (currencySell = '".$_GET["agentCode"]."' OR currencyBuy = '".$_GET["agentCode"]."')";
		$qryString .= "&currencySell =" . $_GET["agentCode"];
		$qryString .= "&currencyBuy=" . $_GET["agentCode"];
	}

}elseif($searchBy == 3){
if ($_GET["agentCode"] != ""){
		$agentCode = $_GET["agentCode"];
		$where .= " and refNumberIM = '".$_GET["agentCode"]."'";
		$qryString .= "&refNumberIM =" . $_GET["agentCode"];
	}
}
$SQL_Qry = "select acid,transNumber,refNumberIM,description,accountSell,accountBuy,status,sys_Trade_No from account_details where 1".$where;	

$qryString .= "&ida=" . $_GET["ida"];

if($agentType2 == "Branch Manager"){
	$SQL_Qry .= " and parentID = '$parentID'";						
}


$SQL_Qry.=$sql_agent_based;


$agents = SelectMultiRecords($SQL_Qry);
$allCount = count($agents);
if ($sortBy !="")
 $SQL_Qry = $SQL_Qry. " order by $sortBy DESC ";
 $SQL_Qry = $SQL_Qry. " LIMIT $offset , $limit";
$agents = SelectMultiRecords($SQL_Qry);
$userDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where username='$username'");

//debug($SQL_Qry);
?>
<html>
<head>
	<title>Manage Trade</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
	function checkForm(theForm) {
		var a = false;
		var none = true;
		var j = 2; 
		// < (theForm.elements.length - 11)
		for(i =0; i < <?=count($agents); ?>; i++){
			if(theForm.elements[j].checked == true){
				a = confirm("Are you sure you want to delete the checked contacts from the database?");
				none = false;
				break;
			}
			j++;
		}
		if (none) {
			a = false;
			alert("No contact is checked for deletion.")
		}
		return a;
	}
	function checkAllFn(theForm, field) {
		if (field == 1) {
			if (theForm.del.value == 0)
				theForm.del.value = 1;
			else
				theForm.del.value = 0;
			if (theForm.del.value == 1){
				j=2;
				for(i=0; i < <?=count($agents);?>; i++) {
					theForm.elements[j].checked = true;
					j++;
				}
			} else {
				j = 2;
				for(i=0; i< <?=count($agents);?>; i++) {
					theForm.elements[j].checked = false;
					j++;
				}
			}
		} else {
			if (theForm.app.value == 0)
				theForm.app.value = 1;
			else
				theForm.app.value = 0;
			if (theForm.app.value == 1){
				j = 3;
				for(i=0; i<<?=count($agents);?>; i++) {
					theForm.elements[j].checked = true;
					j = j+2;
				}
			} else {
				j = 3;
				for(i=0; i<<?=count($agents);?>; i++) {
					theForm.elements[j].checked = false;
					j = j+2;
				}
			}
		}
	}
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
	// end of javascript -->
	</script>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font class="topbar_tex">Manage Trade </font></strong></td>
  </tr>
  <tr>
    <td align="center">
		<table width="671" border="0" cellspacing="2" cellpadding="1" align="center">
        <form action="update-trade.php" method="get">
		  <tr bgcolor="#EEEEEE"> 
            <td colspan="5"> 
              <table width="100%" cellpadding="2" cellspacing="0" border="1" bordercolor="#006699">
				<tr>
				<td align="center" width="15%">
			<a href="add-trade.php"><font color="#005b90">Add New Trade<font></a>
			</td>
				<td align="center" width="15%">
					<b>Search:</b></td>
          <td width="23%"><input type="text" name="agentCode" value="<?=$agentCode; ?>"></td>
          <td>
          <SELECT name="searchBy" style="font-family:verdana; font-size: 11px">
        	  <OPTION value="">- Search Options -</OPTION>
						<option value="1">Account Number</option>
						<option value="2">Currency</option>
						<option value="3">Reference Number</option>
					</SELECT>
				<script language="JavaScript">
         	SelectOption(document.forms[0].searchBy, "<?=$searchBy; ?>");
                                </script></td>	
          </td>
		  
					<!--<td width="9%" align="right"><b>Status:</b></td>
					<td width="18%"><select name="agentStatus">
						<option value="">- All -</option>
						<option value="Active">New</option>
						<option value="Disabled">Disabled</option>
						<option value="New">New</option>
						<option value="Suspended">Suspended</option>
						</select><script language="JavaScript">
         	SelectOption(document.forms[0].agentStatus, "<?=$_GET[agentStatus]; ?>");
                                </script></td>
					
                  -->
				  
					<td width="7%"><input type="submit" value="Go"></td>
			
				</tr>
				</table></td>
		</tr></form>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
  <form action="delete-contacts.php" method="post" onSubmit="return checkForm(this);">
  
  <input type="hidden" name="del" value="0">
  <input type="hidden" name="app" value="0">
          <?
			if ($allCount > 0){
		?>
          <tr> 
            <td colspan="5" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($agents) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($agents));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?> 
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
           
		  <tr bgcolor="#DFE6EA"> 
		  
            <!--td width="47" height="20"><a href="#" onClick="checkAllFn(document.forms[1], 1);"><font color="#005b90"><strong>Disable</strong></font></a></td-->
            <td width="100"><a href="<?php print $PHP_SELF . "?sortBy=accountBuy".$qryString;?>"><font color="#005b90"><strong>Credit Account</strong></font></a></td>
			<td width="100"><a href="<?php print $PHP_SELF . "?sortBy=accountSell".$qryString;?>"><font color="#005b90"><strong>Debit Account</strong></font></a></td>
            <td width="100"><a href="<?php print $PHP_SELF . "?sortBy=refNumberIM Ascending".$qryString;?>"><font color="#005b90"><strong>Reference Number</strong></font></a></td>
			
            <td width="100"><font color="#005b90"><strong>System Trade No</strong></font></td>
			<td width="250"><a href="<?php print $PHP_SELF . "?sortBy=description".$qryString;?>"><font color="#005b90"><strong>Description</strong></font></a></td>
          </tr>
          <?
			for ($i=0; $i<count($agents); $i++){			
		?>
          <tr valign="top" bgcolor="#eeeeee"> 
            <!--td align="center" height="20"> 
              <input type="checkbox" name="userID[<?=$i;?>]" value="<?=$agents[$i]["userID"]; ?>"></td-->
            <td>
			<? echo $agents[$i]["accountBuy"]; ?>
			</td>
           <td> 
              <?=$agents[$i]["accountSell"]; ?>
            </td
            ><td> 
              <?=stripslashes($agents[$i]["refNumberIM"]); ?>
            </td>
			
            <td><?=stripslashes($agents[$i]["sys_Trade_No"]); ?></td>
       
            <td> 
              <?=stripslashes($agents[$i]["description"]); ?>
            </td>
          </tr>
		  <tr bgcolor="#eeeeee">
		  	<td colspan="5" align="right" height="20"><b>
		
			<a href="javascript:;" onClick=" window.open('view-trade-trans.php?acid=<?=$agents[$i]["acid"]; ?>&action=view','Trade Details', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=500,width=460')"><font color="#005b90"><b>View Details</b></font></a>
			<?php
			 /**
			  *	This condition is executed if the trade is either new or edited.
			  *	The update and cancel functionality is not available for the trade
			  * that is either cancelled or deleted
			  */
			 if( $agents[$i]["status"] == 'TN' || $agents[$i]["status"] == 'TE' )
			 {	?>
				 | <a href="add-trade.php?acid=<?=$agents[$i]["acid"];?>&searchBy=<?=$searchBy;?>">
				 		<font color="#005b90">
							<b>Update</b>
						</font>
				   </a></b>
				   <b>
			 	 | <a href="view-trade-trans.php?acid=<?=$agents[$i]["acid"];?>&action=cancel&searchBy=<?=$searchBy;?>">
				 		<font color="#005b90">
							<b>Cancel</b>
						</font>
					</a></b> 
	   <?php } ?> 
			 <b>
			&nbsp;</td>
		  </tr>
          <?
			}
		?>
          <tr> 
            <td colspan="5" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($agents) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($agents));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
		  
          <tr> 
            <!--td colspan="5" align="center"><? if($status == "Y") { ?> <input type="submit" value="Delete Agents"><? } ?>
            </td-->
          </tr>
          <?
			} else {
		?>
          <tr> 
            <td colspan="5" align="center"> No record found in the database. 
            </td>
          </tr>
          <?
			}
		?>
		<input type="hidden" name="type" value="<?=$_GET["type"];?>" >
		<input type="hidden" name="ida" value="<? echo $_GET["ida"]?>" >
</form>
        </table>
	</td>
  </tr>
</table>
</body>
</html>
