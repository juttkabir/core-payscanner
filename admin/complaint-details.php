<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();

if($agentType == "TELLER")
{
	$agentID = $_SESSION["loggedUserData"]["tellerID"];
	$parantID = $_SESSION["loggedUserData"]["cp_ida_id"];
}else
{
	$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
}


$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

/** #5966:Premierexchange
 * Added enquiry type dropdown
 * by Niaz Ahmad
 */
$enquiryFlag = false;
if(CONFIG_ENQUIRY_TYPE == '1')
{
	$enquiryFlag = true;
	$_arrEnquiry = array("F"=>"Funds not Received","CB"=>"Change of Benefits","CBA"=>"Change of Bank Account"); 
}	

if($_POST["Submit"] != "")
{
	if($agentType == "admin" || $agentType == "Call" || $agentType == "Admin Manager")
	{
//echo "update " .  TBL_COMPLAINTS . " set status='".$_POST["status"]."' where cmpID='".$_POST["cmpID"]."'";
//exit;
		update("update " .  TBL_COMPLAINTS . " set status='".$_POST["status"]."', dated = '".getCountryTime(CONFIG_COUNTRY_CODE)."'   where cmpID='".$_POST["cmpID"]."'");
		
	}
	insertInto("insert into ".TBL_COMPLAINTS_INTER." (dated, cmpID, agent, comments) VALUES ('".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$_POST["cmpID"]."', '$username','". checkValues($_POST["details"])."')");
	
	$qTranID = selectFrom("select transID from " . TBL_COMPLAINTS . " where cmpID = '".$_POST["cmpID"]."'");
	
	$contTrans = selectFrom("select custAgentID, benAgentID from " . TBL_TRANSACTIONS . " where transID = '".$qTranID["transID"]."'");
	
	$fromName = "$company Money Support";//SUPPORT_NAME;
	$fromEmail = SUPPORT_EMAIL;
	$subject = "Comments Added";
	$message = "Comments has been added for enquiry: <b>".$_POST["enquiryTitle"]."</b><br><br>
	  
	  <b>Comment Details</b>: ".checkValues($_POST["details"])." <br><br>
	   
		Thank you for using <b>$company</b>";

	$toMail  = $_SESSION["loggedUserData"]["email"];
	$qEmail  = selectMultiRecords("select email from " . TBL_ADMIN_USERS . " where userID IN ('".$contTrans["custAgentID"]."', '".$contTrans["benAgentID"]."')");
	$toMail1 = $qEmail[0]['email'];
	$toMail2 = $qEmail[1]['email'];
	
	$headers  = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
	$headers .= "To: <$toMail1>, <$toMail2>" . "\r\n";
	$headers .= "From: $fromName <$fromEmail>" . "\r\n";
	$headers .= "Cc: <".INQUIRY_EMAIL.">" . "\r\n";
	if(CONFIG_STOP_WHOLE_EMAIL_SYSTEM!="1")
		@mail($toMail, $subject, $message, $headers);
	
	$descript = "Complaint is updated";
	activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["cmpID"],TBL_COMPLAINTS,$descript);		
	insertError("Your comments has been added successfully.");
	redirect("complaint-details.php?msg=Y&success=Y&cmpID=" . $_POST["cmpID"] );
	//$msg = "Enquiry added successfully";
}

if($agentType == "admin" || $agentType == "Call" || $agentType == "Admin Manager")
{
	$queryCmp = "select * from " .  TBL_COMPLAINTS . " where cmpID='".$_GET["cmpID"]."'";
}
/* Duplicate case *
elseif($agentType == "SUPI" || $agentType == "SUPAI")
{
	$queryCmp = "select *  from ". TBL_TRANSACTIONS . " as t, ".TBL_COMPLAINTS." as c where t.transID = c.transID and (t.benAgentParentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.benAgentID = '$agentID') and cmpID='".$_GET["cmpID"]."'";
}
*/
elseif($agentType == "SUPA" || $agentType == "SUPAI")
{
	$queryCmp = "select *  from ". TBL_TRANSACTIONS . " as t, ".TBL_COMPLAINTS." as c where t.transID = c.transID and cmpID='".$_GET["cmpID"]."'";
	/**
	 * Old Commented Query
	$queryCmp = "select *  from ". TBL_TRANSACTIONS . " as t, ".TBL_COMPLAINTS." as c where t.transID = c.transID and (t.benAgentParentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentID = '$agentID') and cmpID='".$_GET["cmpID"]."'";
	*/
}elseif($agentType == "TELLER")
{
	$queryCmp = "select *  from ". TBL_TRANSACTIONS . " as t, ". TBL_COMPLAINTS ." as c where t.transID = c.transID and t.benAgentID = '$parantID' and c.cmpID='".$_GET["cmpID"]."'";
}else
{
	$queryCmp = "select * from " .  TBL_COMPLAINTS . " where cmpID='".$_GET["cmpID"]."'";
}
$contentComplaintDetails = selectFrom($queryCmp);
$transID = $contentComplaintDetails['transID'];

$contTrans = selectFrom("select * from  ". TBL_TRANSACTIONS."  where transID= '$transID'");
$TreferenceNo = $contTrans["refNumberIM"];
$SMNumber = $contTrans["refNumber"];

$contentsInteracts = selectMultiRecords("select * from ". TBL_COMPLAINTS_INTER ." where cmpID='". $contentComplaintDetails["cmpID"]."'");

$agentID = selectFrom("select userID from ".TBL_ADMIN_USERS . " where username='".$contentComplaintDetails["agent"]."'");
?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
<body>
<script language="javascript">
function checkForm(theForm) {
	if(theForm.details.value == "" || IsAllSpaces(theForm.details.value)){
    	alert("Please provide the details of complaint.");
        theForm.details.focus();
        return false;
    }
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }

</script>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">Enquiries Section </td>
  </tr>
  <tr>
    <td>
	<table width="90%"  border="0">
  <tr>
    <td><fieldset>
    <legend class="style2">Enquiry Details </legend>
    <br>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			<? if ($_GET["msg"] == "Y")
			{
			?>
			  <tr>
			    <td colspan="2" align="right" valign="top" bgcolor="#DFE6EA"><table width="100%" cellpadding="5" cellspacing="0" border="0">
                  <tr>
                    <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                    <td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?></td>
                  </tr>
                </table></td>
		      </tr>
			 <?
			 } else if ($_GET["msg"] != "") {
			 ?>
			  <tr>
			    <td colspan="2" align="right" valign="top" bgcolor="#DFE6EA"><table width="100%" cellpadding="5" cellspacing="0" border="0">
                  <tr>
                    <td width="40" align="center">&nbsp;</td>
                    <td width="635"><? echo "<font color='" . SUCCESS_COLOR . "'><b>".$_GET["msg"]."</b></font>"; ?></td>
                  </tr>
                </table></td>
		      </tr>
			<?	}  ?>
			 <tr>
			 	<? 
			 	if ($_GET["status"]!="")
			 	{
			 		$_SESSION["status"] = $_GET["status"];	
			 	}
			 	?>
			    <td valign="top" bgcolor="#DFE6EA"><a class="style2" href="view-complaint.php?status=<?=$_SESSION["status"]?>"><strong>Go Back</strong></a> </td>
			    <td bgcolor="#DFE6EA"></td>
		      </tr>
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">Enquiry Started By </td>
			    <td bgcolor="#DFE6EA"><a href="#" class="install" onClick=" window.open('view-agent.php?userID=<?=$agentID["userID"]; ?>','agentDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=500,width=460')"><? echo stripslashes($contentComplaintDetails["agent"]) ?></a></td>
		      </tr>
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA"><? echo $systemCode; ?> </td>
			    <td bgcolor="#DFE6EA"><? echo $TreferenceNo; ?></td>
			    </tr>
				<? if(CLIENT_NAME == 'amb'){?>
				<tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA"><? echo $manualCode; ?> </td>
			    <td bgcolor="#DFE6EA"><? echo $SMNumber; ?></td>
			    </tr>
				<? 
				} 
				
			  if($enquiryFlag){
			  $enquiyType = '';
			  foreach($_arrEnquiry as $key => $val){
			  	if($key == $contentComplaintDetails["enquiry_type"])
					$enquiyType = $val;
			  }
		      ?>  
			  <td align="right" valign="top" bgcolor="#DFE6EA">Enquiry Type </td>
			 <td width="10%" align="left" valign="top" bgcolor="#DFE6EA"><? echo $enquiyType?></td>
			 <? } ?>
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">Enquiry Title </td>
			    <td bgcolor="#DFE6EA">
			    	<? echo stripslashes($contentComplaintDetails["subject"]) ?>
			    </td>
			    </tr>
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">Description</td>
			    <td bgcolor="#DFE6EA"><? echo stripslashes($contentComplaintDetails["details"]) ?></td>
			    </tr>
			  <tr align="left" bgcolor="C0C0C0"> 
                <td colspan="2" valign="top"><strong>Enquiry Comments </strong></td>
			    </tr>
				<? if(count($contentsInteracts) > 0)
				{
					for($i=0;$i<count($contentsInteracts);$i++)
					{
					?>
					  <tr>
						<td valign="top" bgcolor="#DFE6EA"><? echo dateFormat($contentsInteracts[$i]["dated"], "2")?><br>
					      <span class="tab-s">By:</span> <? echo $contentsInteracts[$i]["agent"]?></td>
						<td valign="top" bgcolor="#DFE6EA"><? echo stripslashes($contentsInteracts[$i]["comments"])?></td>
					  </tr>
				  <?
				  }
			  }
			  else
			  {?>
			  <tr align="center">
			    <td colspan="2" valign="top" bgcolor="#DFE6EA"><strong class="tab-s">No comments added yet. </strong></td>
		      </tr>
			  <?
			  }
			  ?>
			  <form name="addComplaint" action="complaint-details.php" method="post" onSubmit="return checkForm(this);">

			  <tr>
			    <td width="29%" align="right" valign="top" bgcolor="#DFE6EA">Your Comments </td>
			    <td width="71%" bgcolor="#DFE6EA"><textarea name="details" cols="40" rows="6" class="flat" id="details"></textarea></td>
		      </tr>
			  <? if($agentType == "admin" || $agentType == "Call" || $agentType == "Admin Manager")
			  {
			  ?>
			  <tr>
			    <td align="right" bgcolor="#DFE6EA">Action</td>
			    <td bgcolor="#DFE6EA"><select name="status" id="status">
			      <option value="New">New</option>
			      <option value="Close">Close</option>
			      </select></td>
			    </tr>
			  <tr>
			  <?
			  }
			  ?>
			    <td align="right" valign="top" bgcolor="#DFE6EA">&nbsp;</td>
			    <td align="center" bgcolor="#DFE6EA">
			    	<input type="hidden" name="enquiryTitle" value="<? echo stripslashes($contentComplaintDetails["subject"]) ?>">
			    	<input type="hidden" name="cmpID" value="<? echo $_GET["cmpID"];?>">
			      <input name="Submit" type="submit" class="flat" value="Submit"></td>
		      </tr></form>
			</table>
		    <br>
        </fieldset></td>
  </tr>
</table>

	</td>
  </tr>
</table>
</body>
</html>
