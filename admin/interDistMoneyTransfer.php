<?
session_start();
include ("../include/config.php");
include ("calculateBalance.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$distID = "";
if($_REQUEST["distID"] != ""){
	$distID = $_REQUEST["distID"];
}

/*
if($_POST["distID_W"] != "")
	$_SESSION["distID_W"] = $_POST["distID_W"];

if($_POST["amount"] != "")
	$_SESSION["amount"] = $_POST["amount"];

if($_POST["distID_D"] != "")
	$_SESSION["distID_D"] = $_POST["distID_D"];

$_SESSION["note"] = $_POST["note"];
*/
$_REQUEST["sendingCurrencyName"] = $_REQUEST["sendingCurrency"];
$_REQUEST["recievingCurrencyName"] = $_REQUEST["recievingCurrency"];

	if(!empty($_REQUEST["distID_W"]) && !empty($_REQUEST["sendingCurrencyName"]))
 		$distBalance = calculateBalanceWithCurrency($_REQUEST["distID_W"],$_REQUEST["sendingCurrencyName"]);

/**
 * Validation and Inserting the form entries into database
 */
 if(!empty($_REQUEST["distID_W"]) && !empty($_REQUEST["distID_D"]) && !empty($_REQUEST["amount"]) && $_REQUEST["amount"]> 0 && $_REQUEST["sendingCurrencyName"]!= "")
 {
 		//echo $_SESSION["distID_W"];	
 		//echo "Bal for 1st: ".calculateBalance($_SESSION["distID_W"]);
 		if(	( !defined("CONFIG_ALLOW_INTER_DIST_TRANSFER_EVEN_LOW_BALANCE") || CONFIG_ALLOW_INTER_DIST_TRANSFER_EVEN_LOW_BALANCE == 0 ) // Condition modified by Usman Ghani against #3331: Global Exchange: Inter distributor transfer enhancement.
			 && (calculateBalance($_REQUEST["distID_W"]) < 0 || calculateBalance($_REQUEST["distID_W"])-$_REQUEST["amount"] < 0) )
 		{
 			$msg = "Not suffcient amount to withdraw from distributor";
 			$stat = 0; // failure color
 		}
 		else
 		{
 				/**
				 * Making the exchange rate entry in note field
				 */
				$noteToInsert = "";
				if($_REQUEST["sendingCurrency"] != $_REQUEST["recievingCurrency"])
					$noteToInsert = "( Exchange Rate: 1 ".$_REQUEST["sendingCurrency"]." = ".$_REQUEST["rate"]." ".$_REQUEST["recievingCurrency"]." ) ";
				$noteToInsert .= $_REQUEST["note"];
				if(!empty($_REQUEST["rate"])) 
					$recevingAmount = $_REQUEST["rate"] * $_REQUEST["amount"];
				else
					$recevingAmount = $_REQUEST["amount"];
 				
 				
 				
 				/**
 				 * Make Withdraw entry on the ledger of the distributor which is selected in point1
 				 */
 				$sqlWd = "insert into ".TBL_DISTRIBUTOR_ACCOUNT." (bankID, dated, type, amount, currency, modified_by, Description)
 									values (".$_REQUEST["distID_W"].",'".date("Y-m-d")."','WITHDRAW',".$_REQUEST["amount"].",'".$_REQUEST["sendingCurrencyName"]."',".$_SESSION["loggedUserData"]["userID"].",'".$noteToInsert."')
 				";
 				insertInto($sqlWd);
 				
 				/**
 				 * Deposit entry in distributor which is selected in point 4.
 				 */
 				$sqlDp = "insert into ".TBL_DISTRIBUTOR_ACCOUNT." (bankID, dated, type, amount, currency, modified_by, Description)
 									values (".$_REQUEST["distID_D"].",'".date("Y-m-d")."','DEPOSIT',".$recevingAmount.",'".$_REQUEST["recievingCurrencyName"]."',".$_SESSION["loggedUserData"]["userID"].",'".$noteToInsert."')
 				";
 				insertInto($sqlDp);
 			
				
				$msg = "Funds Transfered sucessfully.";
				$stat = 1; // pass color
				/**
				 * Destroying the current values as trasfer successful
				 */		
				unset($_REQUEST["distID_W"]);
				unset($_REQUEST["distID_D"]);
				unset($_REQUEST["amount"]);
				unset($_REQUEST["note"]);
				
				unset($_REQUEST["sendingCurrencyName"]);
				unset($_REQUEST["recievingCurrencyName"]);
			}
 		
}

	/**
	 * Currecy list for sending distributor
	 */
	if(!empty($_REQUEST["distID_W"]))
	{
		/**
		 * If settlment currecies turn off
		 * than following code will be used
		 *
		$distIDACountries = selectFrom("SELECT `IDAcountry` FROM ".TBL_ADMIN_USERS." WHERE `userID` = '".$_REQUEST["distID_W"]."'") ;						
		$arrDistCountries = explode(",", $distIDACountries['IDAcountry']);
		$strCountries = implode ("','", $arrDistCountries);
		$query = "select DISTINCT(s.currencyRec) from countries as c, services as s where c.countryId = s.toCountryId and c.countryName IN ('".$strCountries."') ";
		$queryCurrency = selectMultiRecords($query);
		$strCurrency = "";
		for($j = 0; $j < count($queryCurrency); $j++)
		{
			if($j > 0)
				$strCurrency .= ", ";
			$strCurrency .= trim($queryCurrency[$j]["currencyRec"]);
		}
										
		$arrCurrency = explode (", ",$strCurrency);
		$uniqueCurrencyList1 = array_unique($arrCurrency);	
		
		$uniqueCurrencyListSD = array();
		
		foreach($uniqueCurrencyList1 as $value)
		{
			if(!empty($value))
		   	$uniqueCurrencyListSD[]=$value;
		}
		*/
		
		$distIDACountriesSend = selectFrom("SELECT `settlementCurrency` FROM ".TBL_ADMIN_USERS." WHERE `userID` = '".$_REQUEST["distID_W"]."'");		
		
	}
	
	
	/**
	 * Currecy list for recieving distributor
	 */
	if(!empty($_REQUEST["distID_D"]))
	{
		/**
		 * If settlment currecies turn off
		 * than following code will be used
		$distIDACountries = selectFrom("SELECT `IDAcountry` FROM ".TBL_ADMIN_USERS." WHERE `userID` = '".$_REQUEST["distID_D"]."'") ;						
		$arrDistCountries = explode(",", $distIDACountries['IDAcountry']);
		$strCountries = implode ("','", $arrDistCountries);
		$query = "select DISTINCT(s.currencyRec) from countries as c, services as s where c.countryId = s.toCountryId and c.countryName IN ('".$strCountries."') ";
		$queryCurrency = selectMultiRecords($query);
		$strCurrency = "";
		for($j = 0; $j < count($queryCurrency); $j++)
		{
			if($j > 0)
				$strCurrency .= ", ";
			$strCurrency .= trim($queryCurrency[$j]["currencyRec"]);
		}
										
		$arrCurrency = explode (", ",$strCurrency);
		$uniqueCurrencyList1 = array_unique($arrCurrency);	
		
		$uniqueCurrencyListRD = array();
		
		foreach($uniqueCurrencyList1 as $value)
		{
			if(!empty($value))
		   	$uniqueCurrencyListRD[]=$value;
		}
		*/
		
		$distIDACountriesRece = selectFrom("SELECT `settlementCurrency` FROM ".TBL_ADMIN_USERS." WHERE `userID` = '".$_REQUEST["distID_D"]."'");		
		
	}
	

?>
<html>
<head>
<title>Inter Distributor Money Transfer </title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="jquery.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
<!-- 
function checkForm(theForm) {
	
	if(theForm.distID_W.options.selectedIndex == 0){
    	alert("Please select a Distributor for Withdraw.");
        theForm.distID_W.focus();
        return false;
    }
	
	
	if(theForm.sendingCurrency.options.selectedIndex == 0){
    	alert("Please select a sending currency to transfer");
        theForm.sendingCurrency.focus();
        return false;
  }
	
	if(theForm.distID_D.options.selectedIndex == 0){
    	alert("Please select a Distributor for Deposite.");
        theForm.distID_D.focus();
        return false;
   }
   
   if(theForm.recievingCurrency.options.selectedIndex == 0){
    	alert("Please select a recieving currency to transfer");
        theForm.recievingCurrency.focus();
        return false;
  }
 
 	if(document.getElementById("amount").value == "")
	{
		alert("Please enter the amount to transfer");
		document.getElementById("amount").focus();
		return false;	
	}
 	
 	if(document.getElementById("amount").value < 0 || isNaN(document.getElementById("amount").value))
	{
		alert("The amount to be transfer should be positive");
		document.getElementById("amount").focus();
		return false;
	}
	
	
	 
 	<? 
		if(	( !defined("CONFIG_ALLOW_INTER_DIST_TRANSFER_EVEN_LOW_BALANCE") || CONFIG_ALLOW_INTER_DIST_TRANSFER_EVEN_LOW_BALANCE == 0 ) && !empty($_REQUEST["distID_W"]) && !empty($_REQUEST["sendingCurrencyName"]))
		{
	?>
		var balanceDisplay = <?=$distBalance?>;
		if(agentBalanceDisplay <= 0)
		{
			alert("The agent balance must be in positive to transfer funds.");
			document.getElementById("distID_W").focus();
			return false;
		}
		else if(agentBalanceDisplay - document.getElementById("amount").value < 0)
		{
			alert("You can enter upto "+ agentBalanceDisplay +" " + document.getElementById("sendingCurrency").value + " as Transfer funds.");
			document.getElementById("amount").focus();
			return false;
		}
	<? } ?>
 
 
 	return confirm("Do you confirm "+ document.getElementById("amount").value+" "+ theForm.sendingCurrency.value +" transfer of funds"); 
}

function calculateAmount()
{
	var finalAmount;
	var amount = document.getElementById("amount").value;
	var rate = document.getElementById("rate").value;
	
	if(amount != "" && rate != "")
	{
		finalAmount = amount * rate;
		document.getElementById("ramount").value = finalAmount;
	}
	
	if(document.getElementById("sendingCurrency").value == document.getElementById("recievingCurrency").value)
	{
		document.getElementById("ramount").value = document.getElementById("amount").value;
	}
}


function updateInterface()
{
	if(document.getElementById("sendingCurrency").value != "" && document.getElementById("recievingCurrency").value != "")
	{
		if(document.getElementById("sendingCurrency").value == document.getElementById("recievingCurrency").value)
		{
			$("#exchangeRate").hide();
			document.getElementById("ramount").value = document.getElementById("amount").value;
		}
		else
		{
			$("#exchangeRate").show();
			document.getElementById("ramount").value = document.getElementById("amount").value;
			document.getElementById("rate").value = 1;
		}
	}
	document.getElementById("sc").innerHTML = document.getElementById("sendingCurrency").value;
	document.getElementById("rc").innerHTML = document.getElementById("recievingCurrency").value;

	document.getElementById("ta").innerHTML = document.getElementById("sendingCurrency").value;
	document.getElementById("ra").innerHTML = document.getElementById("recievingCurrency").value;	
}

function placeValues()
{
	document.getElementById("sc").innerHTML = document.getElementById("sendingCurrency").value;
	document.getElementById("rc").innerHTML = document.getElementById("recievingCurrency").value;

	document.getElementById("ta").innerHTML = document.getElementById("sendingCurrency").value;
	document.getElementById("ra").innerHTML = document.getElementById("recievingCurrency").value;		

	updateInterface();
}


-->
</script>
</head>
<body onLoad="placeValues();">
<div class="topbar">
  <strong><font class="topbar_tex">Inter Distributor Money Transfer</font></strong>
</div>
<form action="interDistMoneyTransfer.php" method="post" onSubmit="return checkForm(this);" name="distForm">
<table width="100%" border="1" cellspacing="1" cellpadding="5">
	<tr>
    <td align="center">
			<table width="650" border="0" cellspacing="1" cellpadding="2" align="center">
        <? if ($msg != ""){ ?>
          <tr bgcolor="#EEEEEE">
            <td colspan="2" bgcolor="#EEEEEE">
            	<table width="100%" cellpadding="5" cellspacing="0" border="1">
                <tr>
                  <td width="40" align="center"><font size="5" color="<? echo ($stat ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ( $stat ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><? echo "<font color='" . ( $stat ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$msg." </b><br><br></font>"; ?></td>
                </tr>
              </table>
            </td>
          </tr>
          <? } ?>
        <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Sending Distributor<font color="#ff0000">*</font></strong></font></td>
            <td>
            	<?
								$excludeW = "";
								if(!empty($_REQUEST["distID_D"]))
										$excludeW = " and userID !=".$_REQUEST["distID_D"];
								
								$qry1 = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'N' and agentStatus='Active' and fromServer='' ".$excludeW." order by agentCompany";
								
								$src = selectMultiRecords($qry1);

            	?>
            	<select name="distID_W" id="distID_W" style="font-family:verdana; font-size: 11px" onChange="document.distForm.submit();">
								<option value="">- Select Distributor -</option>
								<?
								for ($i=0; $i < count($src); $i++){
										if($_REQUEST["distID_W"] == $src[$i]["userID"]):
								?>
									<option value="<?=$src[$i]["userID"]; ?>" selected><? echo $src[$i]["agentCompany"] . " [" . $src[$i]["username"] . "]" ; ?></option>
								<?
									else:
								?>
									<option value="<?=$src[$i]["userID"]; ?>"><? echo $src[$i]["agentCompany"] . " [" . $src[$i]["username"] . "]" ; ?></option>
								<?
									endif;
								}
								?>
							</select>
							&nbsp;&nbsp;
							
							
							<select name="sendingCurrency" id="sendingCurrency" style="font-family:verdana; font-size: 11px" onChange="document.distForm.submit();updateInterface();">
								<option value="">- Currency  -</option>
								<?
								/*
								for ($k=0; $k < count($uniqueCurrencyListSD); $k++){
								*/
									if(!empty($distIDACountriesSend["settlementCurrency"]))
									{
										if($_REQUEST["sendingCurrencyName"] == $distIDACountriesSend["settlementCurrency"])
											$strSelected = "selected";
								?>
									<option value="<?=trim($distIDACountriesSend["settlementCurrency"])?>" <?=$strSelected?>><?=trim($distIDACountriesSend["settlementCurrency"])?></option>
								<?
									}
								//}
								?>
							</select>
							
						</td>
				</tr>
				
				<?
			  	if(!empty($_REQUEST["sendingCurrencyName"]) && !empty($_REQUEST["distID_W"]))
			  	{
			  ?>
			  		<tr bgcolor="#ededed"> 
			  				<td>&nbsp;</td>
		            <td width="144">
		            	<font color="#005b90"><strong>Distributor Balance</strong></font>
		            	:<b><?=$distBalance." ".$_REQUEST["sendingCurrencyName"]?></b>
		            </td>
						</tr>
			  <?
			  	}
			  ?>
				
				
				<tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Recieving Distributor<font color="#ff0000">*</font></strong></font></td>
            <td>
            	<?
            		$excludeD = "";
								if($_REQUEST["distID_W"] != "")
										$excludeD = " and userID !=".$_REQUEST["distID_W"];
										
								$qry2 = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'N' and agentStatus='Active' and fromServer='' ".$excludeD." order by agentCompany";
								$dist = selectMultiRecords($qry2);
            	?>
            	<select name="distID_D" id="distID_D" style="font-family:verdana; font-size: 11px" onChange="document.distForm.submit();">
								<option value="">- Select Distributor -</option>
								<?
								for ($i=0; $i < count($dist); $i++){
										if($_REQUEST["distID_D"] == $dist[$i]["userID"]):
								?>
									<option value="<?=$dist[$i]["userID"]; ?>" selected><? echo $dist[$i]["agentCompany"] . " [" . $dist[$i]["username"] . "]" ; ?></option>
								<?
									else:
								?>
									<option value="<?=$dist[$i]["userID"]; ?>"><? echo $dist[$i]["agentCompany"] . " [" . $dist[$i]["username"] . "]" ; ?></option>
								<?
									endif;
								}
								?>
							</select>
							&nbsp;&nbsp;
							<select name="recievingCurrency" id="recievingCurrency" style="font-family:verdana; font-size: 11px"  onChange="updateInterface();">
								<option value="">- Currency  -</option>
								<?
								/*
								for ($k=0; $k < count($uniqueCurrencyListSD); $k++){
								*/
									if(!empty($distIDACountriesRece["settlementCurrency"]))
									{
										if($_REQUEST["recievingCurrencyName"] == $distIDACountriesRece["settlementCurrency"])
											$strSelected = "selected";
								?>
									<option value="<?=trim($distIDACountriesRece["settlementCurrency"])?>" <?=$strSelected?>><?=trim($distIDACountriesRece["settlementCurrency"])?></option>
								<?
									}
								//}
								?>
								
								
								
							</select>
						</td>
				</tr>
				
				
				<tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Transfer Funds<font color="#ff0000">*</font></strong></font></td>
            <td>
            	<input type="text" id="amount" name="amount" style="font-family:verdana; font-size: 11px" value="<?=$_REQUEST["amount"]?>" onBlur="calculateAmount();" />
							&nbsp;<b id="ta"></b>
						</td>
				</tr>

				<tr bgcolor="#ededed" id="exchangeRate" style="display:none"> 
            <td width="144"><font color="#005b90"><strong>Exchange Rate<font color="#ff0000">*</font></strong></font></td>
            <td>
            	<b> 1 </b><span id="sc"><?=$_REQUEST["sendingCurrencyName"]?></span> = 
            	<input type="text" size="5" maxlength="5" id="rate" name="rate" style="font-family:verdana; font-size: 11px" value="<?=$_REQUEST["rate"]?>" onBlur="calculateAmount();" />
            	<span id="rc"><?=$_REQUEST["recievingCurrencyName"]?></span>
				  </td>
				</tr>
				<tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Receiving Funds</strong></font></td>
            <td>
            	<input type="text" id="ramount" name="ramount" style="font-family:verdana; font-size: 11px" value="" readonly/>
            	&nbsp;<b id="ra"></b>
				  </td>
				</tr>						
				
				
				<tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Transfer Fund Note</strong></font></td>
            <td>
            	<textarea name="note" wrap="soft" cols="47" rows="7"><?=$_REQUEST["note"]?></textarea>
						</td>
				</tr>
				
    	</table>
  	</td>
  </tr>
          
  <tr bgcolor="#ededed"> 
  	<td colspan="3" align="center"> <input type="submit" value="Transfer Fund"> </td>
  </tr>
</table>
</form>
</body>
</html>
