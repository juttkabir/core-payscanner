<?php

session_start();

include ("../include/config.php");
include ("security.php");

$filters = "";
$agentName = $_REQUEST["agentName"];
/**
 * Date filter
 */

if(!empty($_REQUEST["date_report"]))
{
	$date_report	= $_REQUEST["date_report"];
	$queryDated = " and dated <= '$date_report' ";	
	$qrCreationDate=" and created <= '$date_report 23:59:59'";	
}
$exportType = $_REQUEST["exportType"];

	$curr_exch_acc = '';
	if(defined("CONFIG_CURRENCY_EXCHANGE_ACCOUNT"))
		$curr_exch_acc = CONFIG_CURRENCY_EXCHANGE_ACCOUNT;
	// get total GBP of stock amount	
	$total_stock_amount = number_format(getTotalStockAmount(),2,'.',',');
	
if($exportType!=""){
	
  $strAccountChart = "SELECT 
							   id,
							   accountName,
							   accountNumber,
							   description,
							   currency 
						  FROM 
							 accounts_chart
						  WHERE 
						  	status = 'AC'
						  ";
			       //  debug($strAccountChart);
					$accountChartRs = selectMultiRecords($strAccountChart);
}
	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	if($exportType == "xls")
	{
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=TrailBalance-".time().".xls"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strNumColumnStart = "<td style='mso-number-format:\"\@\"' align='right'>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
		$strColspanStart = "<td colspan='5'>";
		$str3ColspanStart = "<td colspan='3'>";
		$str2ColspanStart = "<td colspan='2'>";
		$strColspanEnd = "</td>";
	}
	elseif($exportType == "csv")
	{
		header("Content-Disposition: attachment; filename=TrailBalance-".time().".csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "";
		$strColumnEnd   = ",";

	}
	else
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColspanStart = "<td colspan='5'>";
		$strColspanEnd = "</td>";
		$strColumnStart = "<td>";
		$strNumColumnStart= "<td style='mso-number-format:\"\@\"' align='right'>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Export Chart Of Account </title>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
	$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;
	
	$strFullHtml .= $strRowStart;

	$strFullHtml .= $strColumnStart.$strBoldStart."A/C".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."CONTROL NAME".$strBoldClose.$strColumnEnd;
	
	$strFullHtml .= $strColumnStart.$strBoldStart."Dr [Operational]".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Cr [Operational]".$strBoldClose.$strColumnEnd;


			
	$strFullHtml .= $strRowEnd;
	
	$strFullHtml .= $strRowStart;

	for($i=0;$i < count($accountChartRs);$i++)
	{
		 $accDrBal =0;
		 $accCrBal=0;
		 $strAccount = "SELECT 
									   id,
									   accounNumber,
									   accountName,
									   currency,
									   balanceType,
									   balance  
								  FROM 
									 accounts
								  WHERE 
									status = 'AC' AND
									accounType = '".$accountChartRs[$i]["accountNumber"]."' ".$qrCreationDate."";
			        // debug($strAccount);
					 $accountRs = selectMultiRecords($strAccount);
		
		
		if(count($accountRs) > 0 ) { ?>
				<!--<tr><td colspan="4">&nbsp;</td></tr>-->
				<? 
				for($k=0; $k < count($accountRs); $k++){ 
					$drBalance = 0;
					$crBalance = 0;
					$drBalance1 = 0;
					$crBalance1 = 0;
				   $date_from ='';
				   $date_to ='';
				   /*$accDrBal=0; //add all acc dr amount 
				   $accCrBal=0; //add all acc cr amount*/
				   $strClosingBalance = "
							SELECT 
								id,
								closing_balance,
								dated,
								accountNumber 
							FROM 
								".TBL_ACCOUNT_SUMMARY." 
							WHERE
								accountNumber = '".$accountRs[$k]["accounNumber"]."' AND
								dated = (select MAX(dated) as dated FROM ".TBL_ACCOUNT_SUMMARY." 
								WHERE accountNumber = '".$accountRs[$k]["accounNumber"]."' ".$queryDated.")
							 ";
				  		$closingBalance = selectFrom($strClosingBalance);
						
						//debug($strClosingBalance);
					
				
					if($accountRs[$k]['balanceType'] == 'Dr')
						$drBalance =$closingBalance["closing_balance"] - $accountRs[$k]['balance'];
					elseif($accountRs[$k]['balanceType'] == 'Cr')
						$crBalance =$closingBalance["closing_balance"] + $accountRs[$k]['balance'];
					
				if($accountRs[$k]["currency"] == "GBP"){	  
				 if($drBalance < 0)
				 	$drSum += $drBalance;
				 if($drBalance > 0)	
				 	$crSum += $drBalance;
				 if($crBalance < 0)
				 	$drSum += $crBalance;
				 if($crBalance > 0)	
				 	$crSum += $crBalance;	
				}else{ 
					if($drBalance < 0)
						$drSumF += $drBalance;
					if($drBalance > 0)	
						$crSumF += $drBalance;
					if($crBalance < 0)
						$drSumF += $crBalance;
					if($crBalance > 0)	
						$crSumF += $crBalance;
					$strCurrency=$accountRs[$k]["currency"];	
					$cur_sql="select cID from currencies where currencyName ='$strCurrency'";
					$buying_rate_arr_id = selectFrom($cur_sql); 
					$cur_sql="select buying_rate from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."' and created_at = (select max(created_at) from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."')";
					//debug($cur_sql);
					$buying_rate_arr = selectFrom($cur_sql); 
					//debug($buying_rate_arr);
					$buying_rate=$buying_rate_arr[0];
				}
				$show_total_amount = '';
				if($accountRs[$k]["accounNumber"] == $curr_exch_acc)
					$show_total_amount = '<strong>Stock='.$total_stock_amount.'<strong>';

				 if(!empty($closingBalance["dated"]))
				 	$date_from = date("d/m/Y", strtotime($closingBalance["dated"]));
				?>
								
					
					<?php 
					 $BALANCE = "0.00";
					if($accountRs[$k]["currency"] == "GBP"){
						if($drBalance<0 ){
							$BALANCE = $drBalance;
						}
						else if($crBalance<0){
							$BALANCE =$crBalance;
						}
					}else{
						if($drBalance<0 ){
							$drBalance1=$drBalance/$buying_rate;
							$BALANCE = $drBalance1;
						}
						else if($crBalance<0){
							$crBalance1=$crBalance/$buying_rate;
							$BALANCE =$crBalance1;
						}
						 
					} 
					//debug($BALANCE);
					$accDrBal+=number_format($BALANCE, 2, ".", "");
					?>
					<?php 
					$res = selectFrom("select * from account_summary");
					
					 $BALANCE = "0.00";
					if($accountRs[$k]["currency"] == "GBP"){
						if($drBalance>0 ){
							 $BALANCE =$drBalance;
							}
						else if($drBalance == 0 && $crBalance>0 ){
							 $BALANCE =$crBalance;
							}
					}else{
						if($drBalance>0 ){
							$drBalance1=$drBalance/$buying_rate;
						    $BALANCE =$drBalance1;
							}
						else if($drBalance == 0 && $crBalance>0 ){
							$crBalance1=$crBalance/$buying_rate;
							$BALANCE =$crBalance1;
							}
							 if($drBalance < 0)
								$drSum += $drBalance1;
							 if($drBalance > 0)	
								$crSum += $drBalance1;
							 if($crBalance < 0)
								$drSum += $crBalance1;
							 if($crBalance > 0)	
								$crSum += $crBalance1;
					}
					//debug($BALANCE);
					$accCrBal+=number_format($BALANCE, 2, ".", "");
					
					?>
				<? } ?>
				<tr><td colspan="4">&nbsp;</td></tr>
				<?
				   } // end if condition
				
 				$Rzt=abs($accDrBal)-$accCrBal;
				//debug($accDrBal."-".$accCrBal."==".$Rzt);
				if($Rzt>0)
				{
					$drSide=$Rzt;
					$crSide=0;
				}
				elseif($Rzt<0)
				{
					$drSide=0;
					$crSide=$Rzt;
				}
				else
				{
					$drSide=0;
					$crSide=0;
				}
// Data Rows Starts 
		$strFullHtml .= $strColumnStart.$strBoldStart.$accountChartRs[$i]["accountNumber"].$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart.$accountChartRs[$i]["accountName"].$strBoldClose.$strColumnEnd;
		
		$strFullHtml .= $strColumnStart.number_format(abs($drSide), 2, ".", ",").$strColumnEnd;
		$strFullHtml .= $strColumnStart.number_format(abs($crSide), 2, ".", ",").$strColumnEnd;
		
		$strFullHtml .= $strRowEnd;
		
		$drSumT+=$drSide;
		$crSumT+=$crSide;
 } // end out for loop

// Breakline Starts
				$strFullHtml .= $strRowStart;
				$strFullHtml .= $strColspanStart."".$strColspanEnd;
				$strFullHtml .= $strRowEnd;
				$strFullHtml .= $strRowStart;
				$strFullHtml .= $strColspanStart."".$strColspanEnd;
				$strFullHtml .= $strRowEnd;
				
				$strFullHtml .= $strRowStart;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart.$strBoldStart." TOTAL".$strBoldClose.$strColumnEnd;
				
				$strFullHtml .= $strNumColumnStart.$strBoldStart.number_format(abs($drSumT),2, ".", ",").$strBoldClose.$strColumnEnd;
				$strFullHtml .= $strNumColumnStart.$strBoldStart.number_format(abs($crSumT),2, ".", ",").$strBoldClose.$strColumnEnd;
		
				$strFullHtml .= $strRowEnd;
// Breakline Ends

// Total Runnig Row starts
	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlClosing;
//debug($strFullHtml,true);
	echo $strFullHtml;