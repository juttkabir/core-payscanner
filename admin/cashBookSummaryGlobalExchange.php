<?
	session_start();
	include ("../include/config.php");
	include ("security.php");

	/**
	 * Variables from Query String
	 */
	$from = empty($_REQUEST["from"])?date("Y-m-d"):date("Y-m-d", strtotime($_REQUEST["from"]));
	$to = empty($_REQUEST["to"])?date("Y-m-d"):date("Y-m-d", strtotime($_REQUEST["to"]));
	
	/**
	 * Some pre data preparation.
	 */
	$fromFancy = date("d F, Y", strtotime($from));
	$toFancy = date("d F, Y", strtotime($to));
	
	/**
	 * Check for data validity
	 */
	if(!empty($from) && !empty($to))
	{
		$dateWhere = "WHERE 
						created BETWEEN '$from' AND '$to'";
	}
	
	$sql = "SELECT 
				recId,
				openingBalance,
				closingBalance,
				created,
				updated
			FROM
				cash_book_global_exchange
			".$dateWhere."
			ORDER BY
				created DESC
			";
	$result = mysql_query($sql) or die(mysql_error());
	$numrows = mysql_num_rows($result);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cash Book History List</title>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="datepicker.js"></script>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
<link href="css/datepicker1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	$(document).ready(function(){
		$('#calendar').DatePicker({
			format: 'd B, Y',
			flat: true,
			date: [new Date('<?=$fromFancy?>'), new Date('<?=$toFancy?>')],
			//current: new Date(),
			calendars: 3,
			mode: 'range',
			starts: 1,
			onChange: function(formated, dates){
				$('#dateSelection').get(0).innerHTML = '<b>' + formated.join('</b> to <b>') + '</b>';
				$('#from').val(dates[0].getFullYear() + '-' + (dates[0].getMonth() + 1) + '-' + dates[0].getDate());
				$('#to').val(dates[1].getFullYear() + '-' + (dates[1].getMonth() + 1) + '-' + dates[1].getDate());
			}
		});
	});
</script>
</head>

<body>
<table width="90%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td align="center" class="reportHeader">Cash Book History</td>
				</tr>
			</table>
			<br />
			<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
                <tr>
                    <td align="center" style="clear: both;">
						<form action="cashBookSummaryGlobalExchange.php" method="post">
							<span id="calendar"></span>
							<span id="dateSelection"><?="<b>".$fromFancy."</b> to <b>".$toFancy."</b>"?></span><br /><br />
							<input type="hidden" id="from" name="from" value="<?=$from?>" />
							<input type="hidden" id="to" name="to" value="<?=$to?>" />
							<input type="submit" value="Submit" />
						</form>
					</td>
                </tr>
            </table>
			<br />
			<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
				<tr>
					<td width="15%" class="columnTitle">Date</td>
					<td width="20%" align="right" class="columnTitle">Opening Balance</td>
					<td width="20%" align="right" class="columnTitle">Closing Balance</td>
					<td width="6%" align="right" class="columnTitle">&nbsp;</td>
				</tr>
				<?
					while($rs = mysql_fetch_array($result))
					{
						$recId					= $rs["recId"];
						$openingBalance			= $rs["openingBalance"];
						$closingBalance			= $rs["closingBalance"];
						$created		 		= date('F d, Y', strtotime($rs["created"]));
						$updated				= ($rs["updated"]=="0000-00-00")?"":date('F d, Y', strtotime($rs["updated"]));
						$dateQueryString 		= $rs["created"];
				?>
						<tr>
							<td>
								<?=$created?><br />
								<span class="fieldComments"><?=empty($updated)?"Never updated":"Updated on ".$updated?></span>
							</td>
							<td align="right"><?=number_format($openingBalance, 2, ".", ",")?></td>
							<td align="right"><?=number_format($closingBalance, 2, ".", ",")?></td>
							<td align="right"><a href="cashBookGlobalExchange.php?currentDate=<?=$dateQueryString?>">View</a></td>
						</tr>
				<?
					}
				?>
			</table>
		</td>
	</tr>
</table>

</body>
</html>
