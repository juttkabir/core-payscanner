<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$userID  = $_SESSION["loggedUserData"]["userID"];
$agentType = getAgentType();
extract(getHttpVars());

if($agentType == "SUPA" || $agentType == "SUBA"){
    $query="select ruleID,applyAt,applyTrans,matchCriteria,amount,message,dated,cumulativeFromDate,cumulativeToDate from ".TBL_COMPLIANCE_LOGIC." where userID = $userID AND applyAt= 'Create Transaction' or applyAt= 'Confirm Transaction' order by dated";

}
else {
    $query = "select ruleID,applyAt,applyTrans,matchCriteria,amount,message,dated,cumulativeFromDate,cumulativeToDate from " . TBL_COMPLIANCE_LOGIC . " where applyAt= 'Create Transaction' or applyAt= 'Confirm Transaction' order by dated";
//echo "select applyAt,applyTrans,matchCriteria,amount,message,dated,cumulativeFromDate,cumulativeToDate from ".TBL_COMPLIANCE_LOGIC." where applyAt= 'Create Transaction' or applyAt= 'Confirm Transaction'";
}
$viewPopup = selectMultiRecords($query);



?>

<html>
<head>
<title>Compliance Rules List</title>	
</head>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>	
<body>
<table width="665" border="0" cellspacing="1" cellpadding="5" align="center">
  
  
<tr>
 <td align = "center" >
 <table width="255" border="1" cellpadding="5" bordercolor="#666666">
        <form action="compliance-view-popup.php" method="post" name="Search" >
       
      
       <!-- <input type="submit" name="submit" value="View"></td> -->
      
     </tr>
	  </form>
    </table>

</td>
</tr>
  
  <tr>
  <td>
  <table><tr><td ><span class="style1"><strong><font color="#006699">Compliance Rules List</font></strong></span></td></tr></table>	
  	</td>	
  </tr>   
   
    <tr> 
    
    <td  bgcolor="#EFEFEF">
    <table width="665" border="0" bordercolor="#EFEFEF">
    <tr bgcolor="#DFE6EA"> 
    	
		<td width="120"><span class="style1"><strong><font color="#006699">Date</font></strong></span></td>
         
          <td width="150"><span class="style1"><strong><font color="#006699">Apply At</font></strong></span></td>
          <td width="150"><span class="style1"><strong><font color="#006699">Apply At Transaction</font></strong></span></td>
          <td width="75"><span class="style1"><strong><font color="#006699">Amount Criteria</font></strong></span></td>
          <td width="75"><span class="style1"><strong><font color="#006699">Amount</font></strong></span></td>
          <td width="150"><span class="style1"><strong><font color="#006699">Message</font></strong></span></td>
          <td width="150"><span class="style1"><strong><font color="#006699">Edit</font></strong></span></td>
        
         </tr>
        <?

 if(count($viewPopup) > 0)
 {       
 	
  
 	

	 for($i = 0; $i < count($viewPopup); $i++)
   {      
   	
		?>
        <tr bgcolor="#ffffff"> 
		<td width="120"> &nbsp; 
            <?  echo dateFormat($viewPopup[$i]["dated"],"2"); ?>
            
          </td>
          <td width="150"> &nbsp; 
            <?  echo $viewPopup[$i]["applyAt"]; ?>
          </td>
         <td width="150"> &nbsp; 
            <?  echo $viewPopup[$i]["applyTrans"]; ?>
          </td>
        <td width="75"> &nbsp; 
            <?  echo $viewPopup[$i]["matchCriteria"]; ?>
          </td>
          <td width="75"> &nbsp; 
            <?  echo $viewPopup[$i]["amount"]; ?>
          </td>
          <td width="150"> &nbsp; 
            <?  echo $viewPopup[$i]["message"]; ?>
          </td>
           <td width="150"> &nbsp; 
           <a href="compliance-amount-configuration.php?ruleID=<? echo $viewPopup[$i]["ruleID"]?>&flag=update" class="style3">Edit</a>
          </td>
        </tr>
      
		
  <?
} 
}
 ?>
	
</table>
</td>
</tr>

  
</table>
</body>
</html>