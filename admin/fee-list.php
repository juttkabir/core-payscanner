<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$countryBasedFlag = false;
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];

$agentRepository = new \Payex\Repository\AgentRepository($app->getDb());
$agentHelper = new \Payex\Helper\AgentHelper();
$agent = $agentRepository->getById($userID);

//$agentType = getAgentType();
 $where_sql="";
if($_SESSION["loggedUserData"]['adminType']=='Agent')
{
$agentBasedID=$userID;
 //$where_sql="   AND userID='".	$userID."' ";
}


if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}
// define page navigation parameters
if ($offset == "")
	$offset = 0;
$limit=50;
// if(!isset($_POST['FeeFor_list']))
		      // $_POST['FeeFor_list']="deal_contract";
//$_SESSION["FeeFor"] = 		!empty($_POST["FeeFor"]) ? $_POST["FeeFor"] : $_SESSION["FeeFor"];
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " destCountry";
	
$submit   = "";
$origCountry  = "";
$destCountry  = "";
$customer = "";
$feeType  = "";
$AgentID = "";
$collPoint = "";

if($_POST["Submit"] != "")
{	
	if($_POST["origCountry"] != "")
		$origCountry = $_POST["origCountry"];
	if($_POST["destCountry"] != "")
		$destCountry = $_POST["destCountry"];
	if($_POST["customer"] != "")
		$customer = $_POST["customer"];	
	if($_POST["feeType"] != "")
		$feeType = $_POST["feeType"];	
	if($_POST["feeBasedOn"] != "")
		$feeBasedOn = $_POST["feeBasedOn"];
	if($_POST["transactionType2"] != "")
		$feeBasedOn = $_POST["transactionType2"];						
		
	if($_POST["AgentID"] != "")
		$AgentID = $_POST["AgentID"];
	
	if($_POST["collPoint"] != "")
		$collPoint = $_POST["collPoint"];
								
		
	$submit = $_POST["Submit"];
}elseif($_GET["Submit"]!= "")
{
	if($_GET["origCountry"] != "")
		$origCountry = $_GET["origCountry"];
	if($_GET["destCountry"] != "")
		$destCountry = $_GET["destCountry"];
	if($_GET["customer"] != "")
		$customer = $_GET["customer"];	
	if($_GET["feeType"] != "")
		$feeType = $_GET["feeType"];		
	if($_GET["feeBasedOn"] != "")
		$feeBasedOn = $_GET["feeBasedOn"];
	if($_GET["transactionType2"] != "")
		$transactionType2 = $_GET["transactionType2"];	
		if($_GET["AgentID"] != "")
		$AgentID = $_GET["AgentID"];						
	if($_GET["collPoint"] != "")
		$collPoint = $_GET["collPoint"];
	$submit = $_GET["Submit"];
}
	
	if($submit != "")
	{
		//Added By Niaz Ahmad #2538
		if (CONFIG_FEE_BASED_DROPDOWN == "1"){
			
		$query = "select * from ".TBL_MANAGE_FEE . " where 1 ";
		$queryCnt = "select count(*) from ".TBL_MANAGE_FEE . " where 1 ";
	  }else{
		   
		   	$query = "select * from ".TBL_MANAGE_FEE . " where 1 and (feeBasedOn = 'Generic' or feeBasedOn = '' or feeBasedOn IS NULL) " ;
	      $queryCnt = "select count(*) from ".TBL_MANAGE_FEE . " where 1 and (feeBasedOn = 'Generic' or feeBasedOn = '' or feeBasedOn IS NULL) " ;
		}
		
		
		if($origCountry != "")
		{
			$query .= " and origCountry ='". $origCountry."' ";		
			$queryCnt .= " and origCountry ='". $origCountry."' ";
		}
		
		if($_POST['FeeFor_list'] != "")
		{
			$query .= " and FeeFor ='". $_POST[FeeFor_list]."' ";		
			$queryCnt .= " and FeeFor ='". $_POST[FeeFor_list]."' ";
		}
		if($destCountry != "")
		{
			$query .= " and destCountry ='". $destCountry."' ";		
			$queryCnt .= " and destCountry ='". $destCountry."' ";
		}
		if($feeType != "")
		{
			$query .= " and feeType ='". $feeType."' ";		
			$queryCnt .= " and feeType ='". $feeType."' ";
		}
		
	if(CONFIG_AGENT_MANAGE_FEE == "1" || CONFIG_DIST_MANAGE_FEE == "1" || CONFIG_FEE_BASED_COLLECTION_POINT){
		if ( $AgentID != "" && ( $feeType == "agentBased" || $feeType == "agentDenom" || $feeType == "distributorBased" ) ) {
			
			$query .= " and agentNo ='". $AgentID."' ";		
			$queryCnt .= " and agentNo ='". $AgentID."' ";
			
			
		}elseif ( !empty( $collPoint ) && $feeType == "collectionPoint" ) {
			
			$query .= " and agentNo ='". $collPoint."' ";		
			$queryCnt .= " and agentNo ='". $collPoint."' ";
			
			
		}
	}
		/*if (CONFIG_FEE_BASED_DROPDOWN == "1")
		{
			
			$query .= " and feeBasedOn= 'transactionType' ";
	  }*/
	}else
	{
		// $query = "select * from ".TBL_MANAGE_FEE ." where 1 and FeeFor='deal_contract' ";
		// $queryCnt = "select count(*) from ".TBL_MANAGE_FEE." where 1 and FeeFor='deal_contract' ";
	
	
	
	 $query = "select * from ".TBL_MANAGE_FEE ." where 1=1  ";
		$queryCnt = "select count(*) from ".TBL_MANAGE_FEE." where 1=1  ";
	}
//$fees = SelectMultiRecords($SQL_Qry);
	$query	  .= " and isActive != 'N' ";
	$queryCnt .= " and isActive != 'N' ";
	
	if(!empty($agentBasedID))
	{
		$query .= " and  agentNo ='". $agentBasedID."' ";		
		$queryCnt .= " and  agentNo ='". $agentBasedID."' ";
	}
	
	
	
$allCount = countRecords($queryCnt );
if ($sortBy !="")
	 $query .=  " order by $sortBy ASC ";
  $query .= " LIMIT $offset , $limit";
 // print_r($queryCnt);
//echo $query;
$fees = SelectMultiRecords($query);
?>
<html>
<head>
	<title>Manage Fee/Commission</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
		
<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
	
	function checkForm(theForm) {
		var a = false;
		var none = true;
		var j = 2; 
		for(i =0; i < <?=count($fees)+1; ?>; i++){
			if(theForm.elements[j].checked == true){
				a = confirm("Are you sure you want to delete selected Record(s) from the database?");
				none = false;
				break;
			}
			j++;
		}
		if (none) {
			a = false;
			alert("No Record(s) selected for deletion.")
		}
		return a;
	}
	
	function CheckAll()
	{
	
		var m = document.lists;
		var len = m.elements.length;
		if (document.lists.All.checked==true)
	    {
		    for (var i = 0; i < len; i++)
			 {
		      	m.elements[i].checked = true;
		     }
		}
		else{
		      for (var i = 0; i < len; i++)
			  {
		       	m.elements[i].checked=false;
		      }
		    }
	}


	
	function checkAllFn(theForm, field) {
		if (field == 1) {
			if (theForm.del.value == 0)
				theForm.del.value = 1;
			else
				theForm.del.value = 0;
			if (theForm.del.value == 1){
				j=2;
				for(i=0; i < <?=count($fees);?>; i++) {
					theForm.elements[j].checked = true;
					j++;
				}
			} else {
				j = 2;
				for(i=0; i< <?=count($fees);?>; i++) {
					theForm.elements[j].checked = false;
					j++;
				}
			}
		} else {
			if (theForm.app.value == 0)
				theForm.app.value = 1;
			else
				theForm.app.value = 0;
			if (theForm.app.value == 1){
				j = 3;
				for(i=0; i<<?=count($fees);?>; i++) {
					theForm.elements[j].checked = true;
					j = j+2;
				}
			} else {
				j = 3;
				for(i=0; i<<?=count($fees);?>; i++) {
					theForm.elements[j].checked = false;
					j = j+2;
				}
			}
		}
	}
	
	function submitSearchForm()
	{
		searchForm = document.getElementById("search");
		agentIdElement = document.getElementById('AgentID');
		if ( agentIdElement != null )
		{
			agentIdElement.selectedIndex=0;
		}
		searchForm.action='fee-list.php';
		searchForm.submit();
	}
	function setCollPoint(){
		searchForm = document.getElementById("search");
		collPointIdElement = document.getElementById('collPoint');
		if ( collPointIdElement != null )
		{
			collPointIdElement.selectedIndex=0;
		}
		searchForm.action='fee-list.php';
		searchForm.submit();
	}
	
	// end of javascript -->
	</script>
  <style type="text/css">
<!--
.style2 {color: #6699CC;
	font-weight: bold;
}
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">Manage <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></td>
  </tr>
  <tr>
    <td align="center">
		<table width="50%"  border="0">
          <tr>
            <td><fieldset>
              <!--legend class="style2">Search By Country </legend-->
              			  
              <table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
                <form name="search" action="fee-list.php" method="post" id="search">
                	                	
									<?php if(CONFIG_CLIENT_NAME == 'Premier Exchange') { ?>
		<!-- Code By Mahmood Ali Waseem againset ticket # 11209-->
		
		 <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Fee For<font color="#ff0000">*</font></strong></font></td>
            <td width="210">
			<!--onchange="ShowPaymentDetail(this.value)"-->
			<SELECT name="FeeFor_list" id="FeeFor_list" style="font-family:verdana; font-size: 11px"   >
              
				<OPTION value="deal_contract" <?php if($_SESSION['FeeFor']=='deal_contract'){echo 'selected';}?> >Deal Contract</OPTION>
				<OPTION value="payments" <?php if($_SESSION['FeeFor']=='payments'){echo 'selected';}?>>Payments</OPTION>
			</SELECT>
			<script language="JavaScript">
		         			SelectOption(document.search.FeeFor_list, "<?=$_POST['FeeFor_list']?>");
		       	  </script>
             </td>
        </tr>
<?php } ?>		
										
										<tr bgcolor="#ededed">
									<td width="185"><font color="#005b90"><strong>Originating Country</strong></font></td>
									<td width="210">
                      <SELECT name="origCountry" id="origCountry" class=flat>
                        <OPTION value="">- Select Country - </OPTION>
                       <?php
                       $countryTypes = " and  countryType like '%origin%' ";

                       if(CONFIG_COUNTRY_SERVICES_ENABLED
                       && !($agentType == 'SUPA' || $agentType == 'SUBA')
                       ){
                           $serviceTable = TBL_SERVICE;
                           if($countryBasedFlag){
                               $serviceTable = TBL_SERVICE_NEW;
                           }
                           $countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".$serviceTable." where 1 and countryId = fromCountryId  $countryTypes order by countryName");
                       } elseif (($agentType == 'SUPA' || $agentType == 'SUBA')){
                           $countryData = [0 => $agent->getCustCountries()];
                           $originationCountries = $agentHelper->getAgentRelatedCountries($countryData);
                           foreach ($originationCountries as $originationCountry){
                               $selected = $origCountry == $originationCountry ? ' selected="selected"' : '';
                               echo '<option value="' . $originationCountry . '"' . $selected . '>' . $originationCountry . '</option>';
                           }
                           unset($selected);
                       } else {
                           $countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
                       }
                       if ($countires){
                           for ($i=0; $i < count($countires); $i++){
                               $selected = $countires[$i]["countryName"] == $origCountry ? " selected" : "";
                               echo '<option value="' . $countires[$i]["countryName"] . '"' . $selected . '>' . $countires[$i]["countryName"] . '</option>';
                           }
                       }
                       ?>
                      </SELECT>
                    
</font></td>
                                        </tr>
                    <tr bgcolor="#ededed">
                        <td width="185"><font color="#005b90"><strong>Destination Country</strong></font></td>
                        <td width="210">
                            <SELECT name="destCountry" id="destCountry" class=flat>
                                <OPTION value="">- Select Country - </OPTION>
                       <?php
                       //if(CONFIG_ENABLE_ORIGIN == "1"){
                       $countryTypes = " and  countryType like '%destination%' ";
                       //}else{
                       //$countryTypes = " ";
                       //}
                       if(CONFIG_COUNTRY_SERVICES_ENABLED
                           && !($agentType == 'SUPA' || $agentType == 'SUBA')
                       ){
                           $serviceTable = TBL_SERVICE;
                           if($countryBasedFlag){
                               $serviceTable = TBL_SERVICE_NEW;
                           }
                           $countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".$serviceTable." where 1 and countryId = toCountryId  $countryTypes order by countryName");
                       } elseif (($agentType == 'SUPA' || $agentType == 'SUBA')){
                           $countryData = [0 => $agent->getIDACountry()];
                           $destinationCountries = $agentHelper->getAgentRelatedCountries($countryData);
                           foreach ($destinationCountries as $destinationCountry){
                               $selected = $destCountry == $destinationCountry ? ' selected="selected"' : '';
                               echo '<option value="' . $destinationCountry . '"' . $selected . '>' . $destinationCountry . '</option>';
                           }
                           unset($selected);
                       } else {
                           $countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
                       }
                       if ($countires){
                           for ($i=0; $i < count($countires); $i++){
                               $selected = $countires[$i]["countryName"] == $destCountry ? "selected" : "";
                               echo '<option value="' . $countires[$i]["countryName"] . '"' . $selected . '>' . $countires[$i]["countryName"] . '</option>';
                           }
                       }
                       ?>
                            </SELECT>
                            </font>
                        </td>
                    </tr>
                <?php if(CONFIG_PAYIN_CUSTOMER == 1){ ?>
                <tr bgcolor="#ededed">
            <td width="185"><font color="#005b90"><strong>Customer Type</strong></font></td>
            <td width="210">
            	<SELECT name="customer" id="customer" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- Select Customer Type -</OPTION>
								<option value="regular" <? echo ($customer=="regular" ? "selected" : "")?>>Regular(Walkin) Customer</option>
								<option value="payin" <? echo ($customer=="payin" ? "selected" : "")?>>Payin Book Customer</option>
              <script language="JavaScript">
		         			SelectOption(document.search.customer, "<?=$customer?>");
		       	  </script>
		       	  </td>
        			</tr>
        			<? }?>
        			
        			
        		<tr bgcolor="#ededed">
            	<td width="285"><font color="#005b90"><strong><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");} ?> Type</strong></font></td>
            	<td width="210">
				<input type="hidden" name="Submit" value="Submit" />
				<?php
				if ($agentType == "SUPA" || $agentType == "SUBA")
				{
					?>
				<SELECT name="feeType" id="feeType" style="font-family:verdana; font-size: 11px" onchange="submitSearchForm();">
                <OPTION value="">- Select <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?> Type -</OPTION>

					<? if(CONFIG_FEE_AGENT == 1){// removed  (&& CONFIG_FEE_DISTRIBUTOR != 1) from the existing condition against ticket #3399 Money Talks - Fee Management Module done by khola.
						?>
						<option value="agentBased" <? echo ($feeType=="agentBased" ? "selected" : "")?>>Agent Based <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
					<? }?>

					</SELECT>

				<?php
				}
				else{
				?>




            	<SELECT name="feeType" id="feeType" style="font-family:verdana; font-size: 11px" onchange="submitSearchForm();">
                <OPTION value="">- Select <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?> Type -</OPTION>
								<option value="fixed" <? echo ($feeType=="fixed" ? "selected" : "")?>>Fixed <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<option value="percent" <? echo ($feeType=="percent" ? "selected" : "")?>>Percentage of Amount</option>
								<? if(CONFIG_FEE_DENOMINATOR == 1){?>
								<option value="denominator" <? echo ($feeType=="denominator" ? "selected" : "")?>>Denomination Based <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
								<? if(CONFIG_FEE_AGENT_DENOMINATOR == 1){// removed  (&& CONFIG_FEE_DISTRIBUTOR != 1) from the existing condition against ticket #3399 Money Talks - Fee Management Module done by khola.
								?>
								<option value="agentDenom" <? echo ($feeType=="agentDenom" ? "selected" : "")?>>Agent and Denomination Based <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
								<? if(CONFIG_FEE_AGENT == 1){// removed  (&& CONFIG_FEE_DISTRIBUTOR != 1) from the existing condition against ticket #3399 Money Talks - Fee Management Module done by khola.
								?>
								<option value="agentBased" <? echo ($feeType=="agentBased" ? "selected" : "")?>>Agent Based <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
								<? if(CONFIG_FEE_DISTRIBUTOR == 1){?>
								<option value="distributorBased" <? echo ($feeType=="distributorBased" ? "selected" : "")?>>Distributor Based <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
								<? if(CONFIG_FEE_BASED_COLLECTION_POINT == 1){?>
								<option value="collectionPoint"<? echo ($feeType=="collectionPoint" ? "selected" : "")?>>Collection Point <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
							</SELECT>
					<?php } ?>
              <script language="JavaScript">
		         			SelectOption(document.forms[0].feeType, "<?=$feeType?>");
		       	  </script></td>
		    
        		</tr>
        		
        		<? 
				if( ( CONFIG_AGENT_MANAGE_FEE == "1" || CONFIG_DIST_MANAGE_FEE == "1" || CONFIG_COLLPOINT_MANAGE_FEE =="1") && ( $feeType == "agentBased" || $feeType == "agentDenom" || $feeType == "distributorBased" || $feeType == "collectionPoint") ) { 
					
					if($feeType == "agentBased" || $feeType == "agentDenom"){
						$selectName = " Agent";
						$agents = selectMultiRecords("select username,userID, agentCompany, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent'  and isCorrespondent != 'ONLY' order by agentCompany"); 
					}elseif($feeType == "distributorBased" || $feeType == "collectionPoint"){
						$selectName = " Distributor";
						$agents = selectMultiRecords("select username,userID, agentCompany, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent'  and isCorrespondent != 'N' order by agentCompany"); 
					}
				?>
        		
					<tr bgcolor="#ededed" id="titleAgentID">
        			       <td width="285" ><font color="#005b90"><strong>Select <?=$selectName;?></strong></font></td>
        			       <td width="210">
        			       	 <select name="AgentID" id="AgentID" style="font-family:verdana; font-size: 11px; width:226"<? if($feeType == "collectionPoint"){ ?>onChange="setCollPoint();"<? }?>>
	                			<option value="">- Select One -</option>
        			       		<? 
								for ($i=0; $i < count($agents); $i++){ ?>
					

	                <option value="<? echo $agents[$i]["userID"]; ?>"<? echo ($AgentID == $agents[$i]["userID"] ? "selected" : "")?>>
	                <? echo $agents[$i]["username"]." [".$agents[$i]["agentCompany"]."]"; ?>
	                </option>
	                <? }?>
	              </select>
	              <script language="JavaScript">
		         			SelectOption(document.forms[0].AgentID, "<?=$AgentID?>");
		       	  </script>
        			       	</td>
        			</tr>
        			<? }?>
					<?
					if( !empty($AgentID) && $feeType == "collectionPoint" ) {
						$collection = selectMultiRecords("SELECT cp_id, cp_branch_name, cp_city FROM  ".TBL_COLLECTION." where cp_ida_id = '".$AgentID."' ORDER BY cp_branch_name ");
					?>
						<tr bgcolor="#ededed">
							<td><font color="#005b90"><strong>Collection Point</strong></font></td>
							<td>
								<select name="collPoint" id="collPoint">
								<option value="">-- Select Collection Point --</option>
								<?
								for ($k=0; $k < count($collection); $k++)
								{
								
									$collAdd = $collection[$k]["cp_branch_name"]."-".$collection[$k]["cp_city"];	
									$cpID  = $collection[$k]["cp_id"];							
									
								?>
								<OPTION value="<?=$cpID; ?>">
								<? echo $collAdd; ?>
								</OPTION>
								<?
								}
								?>
								</select>
								<script language="JavaScript">
									SelectOption(document.search.collPoint, "<?=$collPoint; ?>");
								</script>
							</td>
						</tr>
					<?
					}
					?>
        			<tr>
        				<td>&nbsp;
        					
        				</td>
        				<td>
        					<input type="submit" value="Submit" class="flat">
        				</td>
        			</tr>
        		</form>
            </table>
                  <br>
            </fieldset></td>
          </tr>
        </table>
		<br>
		  <form name="lists" action="delete-fees.php" method="post" onSubmit="return checkForm(this);">
  <input type="hidden" name="del" value="0">
  <input type="hidden" name="app" value="0">

		<table width="700" border="0" cellspacing="1" cellpadding="1" align="center">
          <?
			if ($allCount > 0){
		?>
          <tr> 
            <td colspan="13" bgcolor="#000000"> 
							<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td colspan="3"> 
                    <?php if (count($fees) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($fees));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&feeType=$feeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&feeType=$feeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&feeType=$feeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&feeType=$feeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </table>
			  		</td>
          </tr>
		  <?php  if(isset($_POST['FeeFor_list']) && $_POST['FeeFor_list'] == 'deal_contract') { ?>
          <tr bgcolor="#DFE6EA"> 
            <!--td width="70"><a href="#" onClick="checkAllFn(document.forms[0], 1);"><font color="#005b90"><strong>Delete</strong></font></a></td-->
            <td width="146" align="center"><input name="All" type="checkbox" id="All" onClick="CheckAll();"></td>
            <td width="159"><a href="<?php print $PHP_SELF . "?sortBy=currencyOrigin&feeType=$feeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&feeBasedOn=$feeBasedOn&Submit=$submit";?>"><font color="#005b90"><strong>Originating Country</strong></font></a></td>
            <?
            if(CONFIG_FEE_CURRENCY == '1')
            {
            ?>
            <td width="150" align="right"><a href="<?php print $PHP_SELF . "?sortBy=feeBasedOn&feeType&feeType=$feeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit";?>"><font color="#005b90"><b>Originating Currency</b></font></a></td>
            <?
          	}	
            ?>
            <td width="159"><a href="<?php print $PHP_SELF . "?sortBy=destCountry&feeType=$feeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&feeBasedOn=$feeBasedOn&Submit=$submit";?>"><font color="#005b90"><strong>Destination Country</strong></font></a></td>
            <?
            if(CONFIG_FEE_CURRENCY == '1')
            {
            ?>
            <td width="150" align="right"><a href="<?php print $PHP_SELF . "?sortBy=currencyDest&feeType&feeType=$feeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit";?>"><font color="#005b90"><b>Destination Currency</b></font></a></td>
            <?
          	}	
            ?>
            <td width="143" align="right"><a href="<?php print $PHP_SELF . "?sortBy=amountRangeFrom&feeType=$feeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit";?>"><font color="#005b90"><strong>Lower Amount Range</strong></font></a></td>
            <td width="148" align="right"><a href="<?php print $PHP_SELF . "?sortBy=amountRangeTo&feeType=$feeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit";?>"><font color="#005b90"><b>Upper Amount Range</b></font></a></td>
            <td width="68" colspan="2" align="right"><a href="<?php print $PHP_SELF . "?sortBy=regularFee&feeType=$feeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit";?>"><font color="#005b90"><b><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></b></font></a></td>
            <td width="68" colspan="2" align="right"><a href="<?php print $PHP_SELF . "?sortBy=feeType&feeType=$feeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit";?>"><font color="#005b90"><b><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?> Type</b></font></a></td>
            <? if(CONFIG_AGENT_MANAGE_FEE == "1" || CONFIG_DIST_MANAGE_FEE == "1"){ ?>
            <td width="68" align="right"><a href="<?php print $PHP_SELF . "?sortBy=agentNo&Agent=$AgentID&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit";?>"><font color="#005b90"><b>Agent/Distributor</b></font></a></td>
            <? } ?>
            <td width="68" align="right"><a href="<?php print $PHP_SELF . "?sortBy=intervalUsed&feeBasedOn=$feeBasedOn&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit";?>"><font color="#005b90"><b>Interval</b></font></a></td>
           
           <!-- Added by Niaz Ahmad Against Ticket # 2538 at 24-10-2007 -->
            <? if (CONFIG_FEE_BASED_DROPDOWN == "1") { ?>
            <td width="300" align="right"><a href="<?php print $PHP_SELF . "?sortBy=feeBasedOn&feeType&feeType=$feeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit";?>"><font color="#005b90"><b>Fee Base</b></font></a></td>
            <td width="200" align="right"><a href="<?php print $PHP_SELF . "?sortBy=transactionType&feeType=$feeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit";?>"><font color="#005b90"><b>
			<? if(CONFIG_CUSTOMER_CATEGORY == '1'){?> Customer Type<? }else{?>Transaction Type<? } ?></b></font></a></td>
            <? } ?>
			
            
          </tr>
		  
		  <?php }  elseif(isset($_POST['FeeFor_list']) && $_POST['FeeFor_list'] == 'payments') {?>
		   <tr bgcolor="#DFE6EA"> 
		   <td width="146" align="center"><input name="All" type="checkbox" id="All" onClick="CheckAll();"></td>
		  <td width="300" align="right"><a href="<?php print $PHP_SELF . "?sortBy=FeeFor&FeeFor_list=$_POST[FeeFor_list]&Submit=$submit";?>"><font color="#005b90"><b>Fee For</b></font></a></td>
		  <td width="300" align="right"><a href="<?php print $PHP_SELF . "?sortBy=PaymentType&feeType&FeeFor_list=$_POST[FeeFor_list]&Submit=$submit";?>"><font color="#005b90"><b>Payment Type</b></font></a></td>
		  <td width="300" align="right"><a href="<?php print $PHP_SELF . "?sortBy=Charges&feeType&feeType=$feeType&Submit=$submit";?>"><font color="#005b90"><b>Commission</b></font></a></td>
		  <td width="300" align="right"><a href="<?php print $PHP_SELF . "?sortBy=PaymentCurrency&Submit=$submit";?>"><font color="#005b90"><b>Currency</b></font></a></td>
		  <td width="300" align="right"><a href="<?php print $PHP_SELF . "?sortBy=PayPer&Submit=$submit";?>"><font color="#005b90"><b>Pay Per</b></font></a></td>
		  </tr>
		  <?php } ?>
          <?
			for ($i=0; $i < count($fees); $i++){
			
				// if(isset($_POST['FeeFor_list']) && $_POST['FeeFor_list'] == 'deal_contract') 
				 if(1) 
				 { 
				
					    $feeBasedOn=$fees[$i]["feeBasedOn"];
            	$transactionType=$fees[$i]["transactionType"];
            /*	if($feeBasedOn!=""){
            	$feeBase=$feeBasedOn;}else{
              $feeBase=$transactionType;}*/
              
				
				$sharedFee = selectFrom("select * from ".TBL_SHARED_FEE." where localFeeId = '".$fees[$i]['manageFeeID']."'"); 
		
		?>
          <tr valign="top" bgcolor="#eeeeee"> 
            
          
		    <?php 
           if($sharedFee["remoteServerId"] > 0 || $sharedFee["shareFeeId"] == '')
           {
           ?>
		  
	            <td align="center"> <input type="checkbox" name="feeID[<?=$i?>]" value="<?=$fees[$i]['manageFeeID']?>"></td> 
	            <td><a href="update-fee.php?feeID=<?=$fees[$i]["manageFeeID"];?>&FeeFor=<?=$fees[$i]["FeeFor"];?>&origCountry=<?=$origCountry;?>&destCountry=<?=$destCountry;?>&customer=<?=$customer;?>&transactionType=<?=$transactionType;?>&feeBasedOn=<?=$feeBasedOn;?>&feeType=<?=$feeType;?>&Submit=<?=$submit;?>"><font color="#005b90"><b><?=stripslashes($fees[$i]["origCountry"]); ?></b></font></a></td>
	            <?
	            if(CONFIG_FEE_CURRENCY == '1')
	            {
	            ?>
	            <td><font color="#005b90"><b><?=stripslashes($fees[$i]["currencyOrigin"]); ?></b></font></td>
	            <?
	          	}	
	            ?>	
	            <td><a href="update-fee.php?feeID=<?=$fees[$i]["manageFeeID"];?>&FeeFor=<?=$fees[$i]["FeeFor"];?>&origCountry=<?=$origCountry;?>&destCountry=<?=$destCountry;?>&customer=<?=$customer;?>&transactionType=<?=$transactionType;?>&feeBasedOn=<?=$feeBasedOn;?>&feeType=<?=$feeType;?>&Submit=<?=$submit;?>"><font color="#005b90"><b>
	              <?=stripslashes($fees[$i]["destCountry"]); ?>
	            </b></font></a></td>
	            <?
	            if(CONFIG_FEE_CURRENCY == '1')
	            {
	            ?>
	            <td><font color="#005b90"><b><?=stripslashes($fees[$i]["currencyDest"]); ?></b></font></td>
	            <?
	          	}	
            
          }else{
	          	?>
	          	<td align="center">&nbsp; </td>
	          	<td><font color="#005b90"><b><?=stripslashes($fees[$i]["origCountry"]); ?></b></font></td>
	          	<?
	            if(CONFIG_FEE_CURRENCY == '1')
	            {
	            ?>
	            <td><font color="#005b90"><b><?=stripslashes($fees[$i]["currencyOrigin"]); ?></b></font></td>
	            <?
	          	}	
	            ?>
	            <td><font color="#005b90"><b><?=stripslashes($fees[$i]["destCountry"]); ?></b></font></a></td>
	            <?	
            	if(CONFIG_FEE_CURRENCY == '1')
	            {
	            ?>
	            <td><font color="#005b90"><b><?=stripslashes($fees[$i]["currencyDest"]); ?></b></font></td>
	            <?
	          	}	
	            ?>
            	
            	
          	<?
          	}
          	?>
          	
            	
            <td align="right"><? echo stripslashes($fees[$i]["amountRangeFrom"]); ?> </td>
            <td align="right"><?=stripslashes($fees[$i]["amountRangeTo"]); ?></td>
            <td align="right"><? if($customer != "payin"){echo(stripslashes($fees[$i]["regularFee"])); }elseif($customer == "payin"){echo(stripslashes($fees[$i]["payinFee"]));} ?></td>
            
            <td colspan="3" align="right"><?
            	 if($fees[$i]["feeType"]=="percent")
            	{
            		echo("Percentage of Amount");
            		}
            		elseif($fees[$i]["feeType"] == "denominator")
            		{
            			echo("Denominator Based");
            			}
            		elseif($fees[$i]["feeType"] == "agentDenom")
            		{
            			echo("Agent Based Denominator");
            		}elseif( $fees[$i]["feeType"] == "agentBased"){
            			echo("Agent Based");
            		}elseif( $fees[$i]["feeType"] == "distributorBased"){
            			echo("Distributor Based");
            		}elseif( $fees[$i]["feeType"] == "collectionPoint"){
            			echo("Collection Point Fee");
            		}else{
            			echo("Fixed Value");
            		}
            		/**
            		 * Adding the payment mode info against ticket# 3319
            		 */
            			if(CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1" && !empty($fees[$i]["paymentMode"]))
            			{
            				echo " on <u>".ucfirst($fees[$i]["paymentMode"])." Payments</u>";
            			}
            		
            	 ?>
            	 
            	 </td>
            	 
            	 <?
				 if(CONFIG_AGENT_MANAGE_FEE == "1" || CONFIG_DIST_MANAGE_FEE == "1" || CONFIG_COLLPOINT_MANAGE_FEE == "1"){ 
            	 
            	 	if ( $fees[$i]["feeType"] == "collectionPoint" ) {
						$collPointDist = selectFrom("select cp_ida_id from ".TBL_COLLECTION." where cp_id = '".$fees[$i]["agentNo"]."'");
						$uID = $collPointDist["cp_ida_id"];
					}else{
						$uID = $fees[$i]["agentNo"];
					}	
						$agentData = selectFrom("select name from admin where userID = '".$uID."'"); ?>
            	 	
					<td align="right"><? echo $agentData["name"]; ?> </td>
            	 <? 
				 } 
				 ?>
            	 
				 <td align="right"><? echo stripslashes($fees[$i]["intervalUsed"]); ?> </td> 
            	
            	<!-- Added by Niaz Ahmad Against Ticket # 2538 at 24-10-2007 -->
      
            <? if (CONFIG_FEE_BASED_DROPDOWN == "1") {      
            	?>
            <td align="right"><? echo $feeBasedOn; ?> </td> 
            <td align="right">
			<? 
			if($feeBasedOn == "customer" && CONFIG_CUSTOMER_CATEGORY == '1')
			{
				$custCat = selectFrom("select name from ".TBL_CUSTOMER_CATEGORY." where id = '".$fees[$i]["transactionType"]."'");
				echo $custCat["name"];
			}else{
			echo $transactionType; 
			}
			?> 
			</td>
            <? } ?>
            	 <?php   
		
			
			
			?>
          </tr>
          <?
				}  // end deal contract if 
				
				
				
				 elseif(isset($_POST['FeeFor_list']) && $_POST['FeeFor_list'] == 'payments') { ?>
		   <tr bgcolor="#DFE6EA"> 
		   <td align="center"> <input type="checkbox" name="feeID[<?=$i?>]" value="<?=$fees[$i]['manageFeeID']?>"></td> 
		  <td align="right"><?php echo  $fees[$i]["FeeFor"]?></td>
		  <td align="right"><a href="update-fee-payment.php?feeID=<?=$fees[$i]["manageFeeID"];?>&FeeFor=<?=$fees[$i]["FeeFor"];?>&currency=<?=$fees[$i]["PaymentCurrency"];?>&charges=<?=$fees[$i]["Charges"];?>&type=<?=$fees[$i]["PaymentType"]?>&payper=<?=$fees[$i]["PayPer"];?>&Submit=<?=$submit;?>"><font color="#005b90"><b><?php echo $fees[$i]["PaymentType"]?></b></font></a></td>
		  <td align="right"><?php echo $fees[$i]["Charges"]?></td>
		  <td align="right"><?php  echo $fees[$i]["PaymentCurrency"]?></td>
		  <td align="right"><?php echo  $fees[$i]["PayPer"]?></td>
		  </tr>
		  <?php } 
				
				
				
				
			}
		?>
          <tr> 
            <td colspan="13" bgcolor="#000000"> 
            	<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td colspan="3"> 
                    <?php if (count($fees) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($fees));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td colspan="9" align="center"> <input type="submit" value="Delete Fees"> 
            </td>
          </tr>
          <?
			} else {
		?>
          <tr> 
            <td colspan="9" align="center"> No <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?> Ranges found in the database. 
            </td>
          </tr>
          <?
			}
		?>
        </table></form>
	</td>
  </tr>

</table>
</body>
</html>
