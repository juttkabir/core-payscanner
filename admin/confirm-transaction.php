<?
session_start();
include ("../include/config.php");

$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$nowTime = date("F j, Y");

if(CONFIG_COMPLIANCE_PROMPT_CONFIRM == "1"){
$currentRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction')";
$currentRule = selectFrom($currentRuleQuery);

$cumulativeRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction')";
$cumulativeRule = selectFrom($cumulativeRuleQuery);


}
//echo"<br>--cumulative rule id--".$cumulativeRule["ruleID"]; 
//echo"<br>--current rule id--".$currentRule["ruleID"];

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}


if($_GET["r"] == 'dom')///If transaction is domestic
{
$returnPage = 'add-transaction-domestic.php';
}else{
	$returnPage = $transactionPage;
	}



if($_SESSION["benAgentID"] == '')
	$_SESSION["benAgentID"] = $_POST['agentID'];
	
	//echo $_SESSION["collectionPointID"];
	
	$_SESSION["BenTempcurrencyTo"] =  $_SESSION["currencyTo"];
	
	//echo $_SESSION["benAgentID"];


$agentType = getAgentType();

if($agentType == "SUPA" || $agentType == "SUPAI" || $agentType == "SUBA" || $agentType == "SUBAI")
{
	$queryCust = "select *  from ".TBL_ADMIN_USERS." where username ='$username'";
	$senderAgentContent = selectFrom($queryCust);	
	$_POST["custAgentID"] = $senderAgentContent["userID"];
}else{
		$queryCust = "select *  from ".TBL_ADMIN_USERS." where userID ='".$_POST["custAgentID"]."'";
		$senderAgentContent = selectFrom($queryCust);	
	}
	
$custAgentParentID = $senderAgentContent["parentID"];
if(CONFIG_SENDER_ACCUMULATIVE_AMOUNT == "1"){

$senderAccumulativeAmount = 0;
         
            		  $to = getCountryTime(CONFIG_COUNTRY_CODE);
									$month = substr($to,5,2);
									$year = substr($to,0,4);
									$day = substr($to,8,2);
								  $noOfDays = CONFIG_NO_OF_DAYS;
								  $fromDate = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d")-$noOfDays ,   date("Y")));
									$from = $fromDate." 00:00:00"; 
									
									// These dates set from configuration page
									if(CONFIG_COMPLIANCE_NUM_OF_DAYS == "1"){
									
										if($cumulativeRule["cumulativeFromDate"]!='' && $cumulativeRule["cumulativeToDate"]!=''){
  			        	    $from	= $cumulativeRule["cumulativeFromDate"]." 00:00:00"; 
  		                $to	= $cumulativeRule["cumulativeToDate"]." 23:59:59"; 
  	                } 
  	              }
									$senderTransAmount = selectFrom("select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_POST["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'");
								  //echo "select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_POST["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'";
                 $senderAccumulativeAmount += $senderTransAmount["transAmount"];
 

	if($_POST["transID"] == "")
	{
 				$senderAccumulativeAmount += $_POST["transAmount"];
	}
}

//echo "<br>--Cumulative Amount--".$senderAccumulativeAmount;


$_SESSION["moneyPaid"] 			= $_POST["moneyPaid"];
$_SESSION["transactionPurpose"] = $_POST["transactionPurpose"];
//echo $_SESSION["transactionPurpose"];
$_SESSION["other_pur"] 			= $_POST["other_pur"];
$_SESSION["fundSources"] 		= $_POST["fundSources"];
$_SESSION["refNumber"] 			= $_POST["refNumber"];
$_SESSION["benAgentID"] 		= $_POST["distribut"];
$_SESSION["discount"]       = $_POST["discount"];
$_SESSION["chDiscount"]     = $_POST["chDiscount"];
$_SESSION["exchangeRate"]     = $_POST["exchangeRate"];
$_SESSION["IMFee"]     = $_POST["IMFee"];

$_SESSION["bankName"] 		= $_POST["bankName"];
$_SESSION["branchCode"] 	= $_POST["branchCode"];
$_SESSION["branchAddress"] 	= trim($_POST["branchAddress"]);
$_SESSION["swiftCode"] 		= $_POST["swiftCode"];
$_SESSION["accNo"] 			= $_POST["accNo"];
$_SESSION["ABACPF"] 		= $_POST["ABACPF"];
$_SESSION["IBAN"] 			= $_POST["IBAN"];
$_SESSION["ibanRemarks"] 			= $_POST["ibanRemarks"];
$_SESSION["currencyTo"]     = $_POST['currencyTo'];	

$_SESSION["exRateLimitMin"] 			= $_POST["exRateLimitMin"];
$_SESSION["exRateLimitMax"]     = $_POST['exRateLimitMax'];	



if($_POST["dDate"] != "")
{
	$_SESSION["transDate"] = $_POST["dDate"];
}

$_SESSION["question"] 			= $_POST["question"];
$_SESSION["answer"]     = $_POST["answer"];	
$_SESSION["tip"]     = $_POST["tip"];
$_SESSION["accountType"]     = $_POST["accountType"];

$_SESSION["benIdPassword"] = $_POST["benIdPassword"];


$_SESSION["discount_request"] = $_POST["discount_request"];	

if($_SESSION["transType"]=="Bank Transfer")
	$_SESSION["bankCharges"] 		= $_POST["bankCharges"];
else
	$_SESSION["bankCharges"]="";

$_SESSION["cardNo"] = $_REQUEST["cardNo"];
$_SESSION["cardName"] = $_REQUEST["cardName"];
$_SESSION["cardExpiry"] = $_REQUEST["cardExpiry"];	
		

if($_POST["transID"] != "")
{
	$backUrl = "$returnPage?msg=Y&from=conf&transID=" . $_POST["transID"]."&calculateBy=" . $_POST["calculateBy"];
}
else
{
	$backUrl = "$returnPage?msg=Y&from=conf&calculateBy=" . $_POST["calculateBy"];
}
if(trim($_POST["transType"]) == "")
{
	insertError(TE3);	
	redirect($backUrl);
}

if(trim($_POST["transType"]) == "Pick up")
{
	if(trim($_POST["collectionPointID"]) == "")
	{
		insertError("Please Select Collection Point");	
		redirect($backUrl);
	}
}


if(trim($_POST["customerID"]) == "")
{
	insertError(TE4);	
	redirect($backUrl);
}
if(trim($_POST["distribut"]) == "" && trim($_POST["transType"]) == "Pick up")
{
	insertError(TE5);	
	redirect($backUrl);
}

if(trim($_POST["benID"]) == "")
{
	insertError(TE6);	
	redirect($backUrl);
}
if(isset($_POST["benIdTypeFlag"]) && trim($_POST["benIdTypeFlag"]) == ""  && strstr(CONFIG_BEN_IDTYPE_COMP_TRANSACTION_TYPES,trim($_POST["transType"])))
{
	insertError("Selected Beneficiary should have ID details for Transactin Type ".trim($_POST["transType"]).". Please Edit the Beneficiary.");	
	redirect($backUrl);
}

if(isset($_POST["benSOFFlag"]) && trim($_POST["benSOFFlag"]) == ""  && strstr(strtoupper(CONFIG_BEN_SOF_TYPE_COMP_TRANSACTION_TYPES),strtoupper(trim($_POST["transType"]))))
{
	insertError("Selected Beneficiary should have S/O,W/O,D/O value for Transactin Type  ".trim($_POST["transType"]).". Please Edit the Beneficiary.");	
	redirect($backUrl);
}
// Validity cheks for Bank Details of Beneficiary.
if(trim($_POST["transType"]) == "Bank Transfer")
{
	if (CONFIG_EURO_TRANS_IBAN == "1" && $_POST["benCountryRegion"] == "European") {
		
		if (trim($_POST["IBAN"]) == "") {
			insertError(TE21);
			redirect($backUrl);	
		}
		
	}	
	elseif(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
		if (isset($_POST["IBAN"]) && trim($_POST["IBAN"]) == "") {
			insertError(TE21);	
			redirect($backUrl);
		}
		if(isset($_POST["bankName"]) && trim($_POST["bankName"]) == "") {
			insertError(TE14);	
			redirect($backUrl);
		}
		if(isset($_POST["accNo"]) && trim($_POST["accNo"]) == "") {
			insertError(TE18);	
			redirect($backUrl);
		}
		if(isset($_POST["branchAddress"]) && trim($_POST["branchAddress"]) == "") {
			insertError(TE16);	
			redirect($backUrl);
		}
	}
	else {
		
		if(trim($_POST["bankName"]) == "") {
			insertError(TE14);	
			redirect($backUrl);
		}
		if(trim($_POST["accNo"]) == "") {
			insertError(TE18);	
			redirect($backUrl);
		}
		if(trim($_POST["branchAddress"]) == "") {
			insertError(TE16);	
			redirect($backUrl);
		}
		if(trim($_POST["branchCode"]) == "") {
			insertError("Please provide the Branch Name/Number.");	
			redirect($backUrl);
		}
		if(CONFIG_CPF_ENABLED == '1') {
			if(trim($_POST["ABACPF"]) == "" && strstr(CONFIG_CPF_COUNTRY , '$_POST["benCountry"]')) {
				insertError(TE20);	
				redirect($backUrl);
			}
		}
		
	}
	if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS != "1")
	{
		if(trim($_POST["distribut"]) == "") {
			insertError("Please select a Distributor from the list");	
			redirect($backUrl);
		}
	}
}

/*if(trim($_POST["refNumber"]) != "")
{
	
	if (isExist("select refNumber from ".TBL_TRANSACTIONS." where refNumber = '".$_POST["refNumber"]."'")){
		insertError(TE7);		
		redirect($backUrl);
	
}	
}*/

if($_POST["notService"] == "Home Delivery")
{
	insertError(TE22);	
	redirect($backUrl);
}

if(trim($_POST["transAmount"]) == "" || $_POST["transAmount"] <= 0)
{
	insertError(TE8);	
	redirect($backUrl);
}

if(trim($_POST["exchangeRate"]) == "" || $_POST["exchangeRate"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

/*if(trim($_POST["exchangeID"]) == "" && CONFIG_ENABLE_DOMESTIC != '1')
{
	insertError(TE9);	
	redirect($backUrl);
}*/

if(CONFIG_ZERO_FEE == '1')
{
	if( $_POST["IMFee"] < 0)
	{
		insertError(TE2);	
		redirect($backUrl);
	}	
	
}else{
	if(trim($_POST["IMFee"]) == "" || $_POST["IMFee"] <= 0)
	{
		insertError(TE2);	
		redirect($backUrl);
	}
}

if((trim($_POST["localAmount"]) == "" || $_POST["localAmount"] <= 0) && CONFIG_ENABLE_DOMESTIC != '1')
{
	insertError(TE9);	
	redirect($backUrl);
}

if(CONFIG_ZERO_FEE == '1')
{
	if( $_POST["IMFee"] < 0)
	{
		insertError(TE9);	
		redirect($backUrl);
	}

}
else
{
	if($_SESSION["chDiscount"]=="" && (trim($_POST["IMFee"]) == "" || $_POST["IMFee"] <= 0))
	{
		insertError(TE9);	
		redirect($backUrl);
	}
}
if(trim($_POST["totalAmount"]) == "" || $_POST["totalAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["transactionPurpose"]) == "" && CONFIG_NON_COMP_TRANS_PURPOSE != "1")
{
	insertError(TE10);	
	redirect($backUrl);
}

if(trim($_POST["fundSources"]) == "" && CONFIG_NONCOMPUL_FUNDSOURCES != '1')
{
	insertError(TE11);	
	redirect($backUrl);
}
if(CONFIG_MONEY_PAID_NON_MANDATORY != "1" && CONFIG_CALCULATE_BY != '1'){
	if(trim($_POST["moneyPaid"]) == "")
	{
		insertError(TE12);	
		redirect($backUrl);
	}
}

if (CONFIG_REFNUM_GENERATOR == '1') {
	if (!generateRefNumber()) {
		if ($agentType == 'admin') {
			insertError("Your transaction will not be created. Please generate reference number template from left menu");
		} else {
			insertError("Your transaction will not be created. Please contact super admin for reference number");
		}
		redirect($backUrl);
	}
}

$refNumber = str_replace(" ","",$_POST["refNumber"]);
$_SESSION["refNumber"] = $refNumber;
$_POST["refNumber"] = $refNumber;

if(trim($_POST["Declaration"]) != "Y")
{
	insertError(TE13);	
	redirect($backUrl);
}
	if($_POST["transID"] != "")
		$strRandomQuery = selectFrom("select * from ". TBL_TRANSACTIONS." where transID != '".$_POST["transID"]."' and refNumber = '".$_SESSION["refNumber"]."'");
	else
		$strRandomQuery = selectFrom("SELECT * FROM ". TBL_TRANSACTIONS ." where refNumber = '".$_SESSION["refNumber"]."'");
		  
		
	//$nRandomResult = mysql_query($strRandomQuery)or die("Invalid query: " . mysql_error()); 
	//$nRows = mysql_fetch_array($nRandomResult);
	$oldReference = $strRandomQuery["refNumber"];
	if($oldReference)
	{
	insertError(TE7);	
	redirect($backUrl);
	}
// This config added by Niaz Ahmad at 27-06-2009 @4998:AMB	
if(CONFIG_ADMIN_STAFF_AMOUNT_LIMIT_ALERT == "1")
{
	if($agentType == "Admin" || $agentType == "COLLECTOR" || $agentType == "Admin Manager" || $agentType == "Call" || $agentType == "Support" || $agentType == "SUPI Manager")
	{
	
	$query = "select adminLimitTransAmount from ".TBL_ADMIN_USERS." where username = '$username'";
	$strAdminSql = selectFrom($query);
		
		if(!empty($strAdminSql["adminLimitTransAmount"]))
		{
			if($_POST["transAmount"] > $strAdminSql["adminLimitTransAmount"])
			{
				insertError("You can't create transaction greater than".$strAdminSql["adminLimitTransAmount"]." Amount");	
				redirect($backUrl);
			}
		}
  }	
}	
?>
<html>
<head>
<title>Transaction Confirmation</title>

<script language="javascript" src="./javascript/functions.js"></script>
<!-- <script language="javascript" src="./javascript/confirmTrans_complianceRule.js"></script> -->

<script language="javascript">

	function showDetails() {
		 
			alert("disable");
			document.addTrans.Submit.Disabled= "disabled";
	}
	
function checkForm(theForm,strval){
	
		var minAmount = 0;
		var maxAmount = 0;

		var condition;
  
 
		var conditionCumulative;
		var conditionCurrent;
  	var ruleConfirmFlag=true;
  	var ruleFlag=false;
  	var transSender = '';
	if(strval == 'Yes')
	{
		transSender="transSend";
		}else{
			transSender="NoSend";
			}
	
	var accumulativeAmount = 0;
	accumulativeAmount = <? echo $senderAccumulativeAmount ?>;
	var currentRuletransAmount = <?=$_POST["transAmount"]?>;	
	var currentRuleAmountToCompare = 0;
	var amountToCompareTrans; 
	
// cummulative Amount Rule /////////
<?	
if($cumulativeRule["ruleID"]!='') {
    
     if($cumulativeRule["matchCriteria"] == "BETWEEN"){
		  $betweenAmount= explode("-",$cumulativeRule["amount"]);
		  $fromAmount=$betweenAmount[0];   
		  $toAmount=$betweenAmount[1]; 
?>
		conditionCumulative = <?=$fromAmount?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmount?>;
		
		<?
    }else{
?>
    amountToCompare = <?=$cumulativeRule["amount"]?>;
    conditionCumulative =	accumulativeAmount  <?=$cumulativeRule["matchCriteria"]?> amountToCompare;
 <?	}?>			
		
	
		 if(conditionCumulative)
	   {

				
	   	if(confirm("<?=$cumulativeRule["message"]?>"))
    	{
    	    ruleConfirmFlag=true;
    			/*document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();*/
    		
                
      }else{ 
    
    			/*	document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   					
      	addTrans.localAmount.focus();*/
       
        ruleConfirmFlag=false;
       // ruleFlag=true;
      }

    }else{ 

    	   ruleConfirmFlag=true;
    } 
 
	
<? } ?>

////////////current transaction amount///////


<? if($currentRule["ruleID"]!=''){
	   
		 
	   if($currentRule["matchCriteria"] == "BETWEEN"){
				
		  $betweenAmountCurrent= explode("-",$currentRule["amount"]);
		  $fromAmountCurrent=$betweenAmountCurrent[0];   
		  $toAmountCurrent=$betweenAmountCurrent[1]; 
		?>
		conditionCurrent = <?=$fromAmountCurrent?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmountCurrent?>;
		<?
 }else{
 	?>      
 	      
 		    currentRuleAmountToCompare = <?=$currentRule["amount"]?>;
 		    conditionCurrent =	currentRuletransAmount  <?=$currentRule["matchCriteria"]?> currentRuleAmountToCompare;
 <?	}?>
	
 
 if(conditionCurrent)
	   {

				if(confirm("<?=$currentRule["message"]?>"))
				{
					
    	     ruleConfirmFlag=true;
    			
    			/*document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();*/
    		
              
     }else{
    
    				/*document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   				   	addTrans.localAmount.focus();*/
         ruleConfirmFlag=false;
         //ruleFlag=true;
      }

    }else{ 

    	     ruleConfirmFlag=true; 
    } 
  
<? } ?>  

 if(ruleConfirmFlag){
 	
   	document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
 	}
 	
 if(ruleConfirmFlag == false){
 	
 	  	  document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   			addTrans.localAmount.focus();
 	}		
	
}		

	
	</script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style5 {
	color: #005b90;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="103%" border="0" cellspacing="1" cellpadding="5">
 <tr>
    <td bgcolor="#C0C0C0"><b><font color="#FFFFFF" size="2">Confirm Transaction.</font></b></td>
  </tr>
  
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
          <tr>
            <td align="right"><div class='noPrint'><a href="<?=$returnPage?>?transID=<? echo $_POST["transID"]?>&calculateBy=<?=$_POST["calculateBy"]?>&getCalByValue=Y" class="style2"><input type="hidden" name="act" value="<? echo $_SESSION["act"]; ?>">Change Information</a></div></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Secure and Safe way to send money</legend>
            <br>
            <?
            	
			if($_POST["custAgentID"]!='')
			{
				$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $_POST["custAgentID"] . "'";
				$custAgent=$_POST["custAgentID"];
			}
			else{
				//		$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='SambaSuper'";
				//	$custAgent='3';
			}
//////////////////////////////////////////////
//debug ($_SESSION["loggedUserData"]["username"]);
$custLogo = selectFrom("select logo  from ".TBL_ADMIN_USERS." where username ='".$_SESSION["loggedUserData"]["username"]."'");

if($custLogo["logo"] != "" && file_exists("../logos/" . $custLogo["logo"]))
	{
	
	$arr = getimagesize("../logos/" . $custLogo["logo"]);		
	$width = ($arr[0] > 200 ? 200 : $arr[0]);
	$height = ($arr[1] > 100 ? 100 : $arr[1]);	
///////////////////////////////////////////////

						?>
<!----------------------------------------------------------->
				<table border="0" align="center" cellpadding="2" cellspacing="2" bordercolor="#DFE6EA">
              <tr>		
		<td width="28%" align="center"><?=printLogoOnReciept($_SESSION["loggedUserData"]["userID"], $_SESSION["loggedUserData"]["username"])?></td>
		</tr>
            </table>
	<? 	
	}else{ ?>
	<table border="0" align="center">
					              <tr>
	<td align="center" width="28%"><img id=Picture2 height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0></td>
	<? } ?>				
		</tr>
				</table>				
				<!----------------------------------------->
            <!--<table border="1" align="center" cellpadding="2" cellspacing="2" bordercolor="#DFE6EA">
              <tr>
                <td>	<?//=printLogoOnReciept($_SESSION["custAgentID"], $_SESSION["loggedUserData"]["username"])?>	</td>
              </tr>
            </table>-->
            <?
						/*}
						else
						{*/
						?>
					           <!-- <table border="0" align="center">
					              <tr>
					                <td align="center" bgcolor="#D9D9FF"><img id=Picture2 height="<?//=CONFIG_LOGO_HEIGHT?>" alt="" src="<?//=CONFIG_LOGO?>" width="<?//=CONFIG_LOGO_WIDTH?>" border=0></td>
					              </tr>
					            </table>-->
						<?
						//}
						?>
            <br>
			<font color="#005b90">Transaction Date: </font><? if($_POST["dDate"]!= ""){echo($_POST["dDate"]);}else{ echo $nowTime;}?><br>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Agent's Details</legend>
            <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td align="right"><font color="#005b90">Company Name</font></td>
                <td colspan="3"><? echo $senderAgentContent["agentCompany"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Address</font></td>
                <td colspan="3"><? echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Country</font></td>
                <td width="200"><? echo $senderAgentContent["agentCountry"]?></td>
                <td width="100" align="right"><font color="#005b90">Phone</font></td>
                <td width="200"><? echo $senderAgentContent["agentPhone"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">MSB No.</font></td>
                <td width="200"><? echo $senderAgentContent["agentMSBNumber"]?></td>
                <td width="100" align="right"><font color="#005b90">Email</font></td>
                <td width="200"><? echo $senderAgentContent["email"]?></td>
              </tr>
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td>
			
			<fieldset>
              <legend class="style2">Sender Details </legend>
              <table border="0" cellpadding="2" cellspacing="0">
                <? 
		
			$queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Mobile, Email  from ".TBL_CUSTOMER." where customerID ='" . $_POST[customerID] . "'";
			$customerContent = selectFrom($queryCust);
			if($customerContent["customerID"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Sender Name</font></td>
                  <td colspan="3"><? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Address</font></td>
                  <td colspan="3"><? echo $customerContent["Address"] . " " . $customerContent["Address1"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                  <td width="200"><? echo $customerContent["Zip"]?></td>
                  <td width="100" align="right"><font color="#005b90">City</font></td>
                  <td width="200"><? echo $customerContent["City"]?></td>
                </tr>
				<tr>
				  <td width="150" align="right"><font color="#005b90">Country</font></td>
                  <td width="200"><? echo $customerContent["Country"]?></td>
				  <td width="100" align="right"><font color="#005b90">Phone</font></td>
                  <td width="200"><? echo $customerContent["Phone"]?></td>
                </tr>                
                <tr>
				  <? if(CONFIG_SHOW_SENDER_MOBILE == "1") { ?>
                  <td width="150" align="right"><font color="#005b90">Mobile</font></td>
                  <td width="200"><? echo $customerContent["Mobile"]?></td>
				  <? }else{ ?>
				  <td width="350" colspan="2">&nbsp;</td>
				  <? } ?> 
                  <td width="100" align="right"><font color="#005b90">Email</font></td>
                  <td width="200"><? echo $customerContent["Email"];?></td>
                </tr>
                <?
			  }
			  ?>
            </table>
              </fieldset></td>
          </tr>
              <? 
			
		/*	$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $_POST["agentID"] . "'";
			$senderAgentContent = selectFrom($querysenderAgent);
			$benAgentParentID = $senderAgentContent["parentID"];
			if($senderAgentContent["userID"] != "")
			{*/
		?>
              <tr>
                <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
                    <table border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                      <? 

			$benTableName = TBL_BENEFICIARY;
			//$IBANField = ", IBAN";
			$SOFField = ", SOF";
			if ( strtoupper($_SESSION["transactionCreatedBy"]) == "CUSTOMER" )
			{
				$benTableName = "cm_beneficiary";
				$SOFField = "";
				//$IBANField = "";
			}

			$queryBen = "select benID, Title, firstName, lastName, middleName" . $SOFField . ", Address, Address1, City, State, Zip,Country, Phone, Email,Mobile from ".$benTableName." where benID ='" . $_POST["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);

			if ( !isset($benificiaryContent["SOF"]) )
				$benificiaryContent["SOF"] = "";

			if($benificiaryContent["benID"] != "")
			{
		?>
                      <tr>
                        <td width="150" align="right"><font color="#005b90" class="style2">Beneficiary Name</font></td>
                        <td colspan="2"><strong><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></strong></td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <?  if(CONFIG_SO_BEN == "1"){  ?>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">S/O</font></td>
                        <td colspan="2"><? echo $benificiaryContent["SOF"] ?></td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <? } ?>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Address</font></td>
                      <?	
					  if (CONFIG_SHOW_BEN_MOBILE=="1")
					  {
					  ?>
					    <td colspan="1"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
						<td width="150" align="right"><font color="#005b90">Mobile</font></td>					
						<td colspan="1"><?=$benificiaryContent["Mobile"]?></td>
					  <?
					  }
					  else
					  {
					  ?>	
					   <td colspan="2"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
					  <?
					  }
					  ?>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                        <td width="200"><? echo $benificiaryContent["Zip"]?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200"><? echo $benificiaryContent["Country"]?>      </td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200"><? echo $benificiaryContent["Phone"]?></td>
                        <td width="100" align="right"><font color="#005b90">Email</font></td>
                        <td width="200"><? echo $benificiaryContent["Email"]; ?></td>
                      </tr>
                      <?
				}
			  if($_POST["transType"] == "Bank Transfer")
			  {
		  			//$benBankDetails = selectFrom("select * from ". TBL_BANK_DETAILS ." where transID='".$transID."'");
					?>
                      <tr>
                        <td colspan="2"><span class="style2">Beneficiary Bank Details </span></td>
                        <td width="100">&nbsp;</td>
                        <td width="200">&nbsp;</td>
                      </tr>
                    <?
                   	$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benificiaryContent["Country"]."'");
										if (CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European") {
                    ?>
                    	<tr>
                    		<td width="135" align="right"><font color="#005b90">IBAN</font></td>
            						<td width="209"><? echo $_SESSION["IBAN"]?></td>
            						<td width="100" align="right"><font color="#005b90"><? echo ($benificiaryContent["Country"] == "GEORGIA" ? "Remarks" : "&nbsp;") ?></font></td>
                				<td width="200"><? echo ($benificiaryContent["Country"] == "GEORGIA" ? $_SESSION["ibanRemarks"] : "&nbsp;") ?></td>
                    	</tr>
                    	
                    <?
                  	}else{
                    ?>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Bank Name</font></td>
                        <td width="200"><? echo $_POST["bankName"]; ?></td>
                        <td width="100" align="right"><font color="#005b90">Acc Number</font></td>
                        <td width="200"><? echo $_POST["accNo"]; ?>                        </td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Branch Code</font></td>
                        <td width="200"><? echo $_POST["branchCode"]; ?>                        </td>
                        <td width="100" align="right"><font color="#005b90">
                          <?
												/*if($_POST["benCountry"] == "United States")
												{
													echo "ABA Number*";
												}
												else*/if(CONFIG_CPF_ENABLED == '1')
												{
													echo  "CPF Number*";
												}
												?>
								          &nbsp; </font></td>
								                        <td width="200"><?
												if(CONFIG_CPF_ENABLED == '1')
												{
												
								         	echo $_POST["ABACPF"];
								                          
												}
												?> &nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Branch Address</font> </td>
                        <td width="200"><? echo $_POST["branchAddress"]; ?>                        </td>
												<td width="100" height="20"  align="right">&nbsp; </td>
												<td width="200" height="20">&nbsp;  </td>
                      </tr>
					 <? if(CONFIG_ADD_BEN_BANK_FIELDS_ON_CREATE_TRANSACTION == "1"){ ?>
						  <tr>
							<td width="150" align="right"><font color="#005b90">Sort Code</font> </td>
							<td width="200"><? echo $_POST["sortCode"]; ?></td>
							<td width="100" height="20"  align="right"><font color="#005b90">Routing Number</font></td>
							<td width="200" height="20"><? echo $_POST["routingNumber"]; ?></td>
						  </tr>
						    <tr>
							<td width="100" height="20"  align="right">Swift Code</td>
							<td width="200" height="20"><? echo $_POST["swiftCode"]; ?></td>
							<td width="150" align="right"><font color="#005b90">IBAN</font> </td>
							<td width="200"><? echo $_POST["IBAN"]; ?></td>
						  </tr>
					 <? } ?>
                      <tr>
                      	<?
                      if(CONFIG_ACCOUNT_TYPE_ENABLED == '1')
                      {?>
                      	<td width="150" align="right"><font color="#005b90">Account Type</font></td>
                        <td width="200"><?=$_SESSION["accountType"]?> </td>
                      <? 
                      }else{
                      ?>	
                        <td width="150" align="right">&nbsp;  </td>
                        <td width="200">&nbsp;  </td>
                      <?
                      }
                      ?>
                        <td width="100" align="right">&nbsp;</td>
                        <td width="200">&nbsp;</td>
                      </tr>
                  	<?  //echo $_POST["IBAN"]; color="#005b90">IBAN Number</font font color="#005b90"Swift Code font echo $_POST["swiftCode"];
                		}
  			}
			  if($_POST["transType"] == "Pick up")
			  {
			  
			$queryCust = "select *  from cm_collection_point where  cp_id  ='" . $_SESSION["collectionPointID"] . "'";
			$senderAgentContent = selectFrom($queryCust);		
			
			$queryDistributor = "select name,agentCompany,distAlias  from " .TBL_ADMIN_USERS. " where  userID  ='" . $senderAgentContent["cp_ida_id"] . "'";
			$queryExecute = selectFrom($queryDistributor);	
			if($senderAgentContent["cp_id"] != "")
			{
		?>
                      <tr>
                        <td colspan="2"><span class="style2">Collection Point Details </span></td>
                        <td width="100">&nbsp;</td>
                        <td width="200">&nbsp;</td>
                      </tr>		
                        <tr>
                        <td width="150" align="right"><font class="style2">Collection point name</font></td>
                        <td width="200"><strong><? echo $senderAgentContent["cp_corresspondent_name"]?> </strong></td>
                        <td width="100" align="right"><font class="style2">Contact Person </font></td>
                        <td width="200"><strong><? echo $senderAgentContent["cp_contact_person_name"]?></strong></td>
                      </tr>
                       
                       <? if(CONFIG_DISTRIBUTOR_ALIAS != '1') { ?>
                       <tr>
                        <td width="150" align="right"><font class="style2">Distributor</font></td>
                        <td width="200" colspan="3"><? echo $queryExecute["name"]?></td>
                       </tr> 
                       <? } ?>
                       
                       <? if(CONFIG_DISTRIBUTOR_ALIAS == '1') {
                          
                          if($queryExecute["distAlias"] !='' && $agentType != "SUPA"){
                          	$alias = $queryExecute["distAlias"];
                          }else{
                          	$alias = $queryExecute["agentCompany"];
                          	}
                          if($agentType == "SUPA"){
                          	$alias = $queryExecute["distAlias"];
                          	}	
                          		
                        ?>
                       <tr>
                        <td width="150" align="right"><font class="style2">Distributor</font></td>
                        <td width="200" colspan="3"><? echo $alias?></td>
                       </tr> 
                       <? } ?> 
                      <tr>
                        <td width="150" align="right"><font class="style2">Address</font></td>
                        <td colspan="3"><strong><? echo $senderAgentContent["cp_branch_address"]?></strong></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font class="style2">City</font></td>
                        <td width="200"><strong><? echo $senderAgentContent["cp_city"]?></strong></td>
                        <td width="100" align="right"><font class="style2">Country</font></td>
                        <td width="200"><strong><? echo $senderAgentContent["cp_country"]?></strong></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font class="style2">Phone</font></td>
                        <td width="200"><strong><? echo $senderAgentContent["cp_phone"]?></strong></td>
                        <td width="100" align="right"><font class="style2">Fax</font></td>
                        <td width="200"><strong><? echo $senderAgentContent["cp_fax"]?></strong></td>
                      </tr> 
                      <?
			  }
			  
			  
			  }
  ?>
            </table>
              </fieldset></td>
              </tr>
         
<?

//}
?>		  
        
		 
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td width="150" align="right"><strong><font color="#005b90"><? echo $manualCode;?></font></strong></td>
                <td width="200"><strong><? echo $_SESSION["refNumber"]?></strong></td>
                <td width="100" align="right"><font color="#005b90">Amount</font></td>
                <td width="200" ><? echo $_POST["transAmount"]?> 
         <?
       if(CONFIG_ENABLE_DOMESTIC != '1')
       {
       ?>       	
                	in  <? echo $_POST["currencyFrom"]?>
             <?
            }
             ?>   	
             </td>
              </tr>
       <?
       if(CONFIG_ENABLE_DOMESTIC != '1')
       {
       ?>       
			  <tr>
                <td width="150" align="right"><font color="#005b90">Exchange Rate</font></td>
                <td width="200"><? echo $_POST["exchangeRate"]?>                  </td>
                <td width="100" align="right"><font class="style2">Local Amount</font></td>
                <td width="200"><strong><? echo $_POST["localAmount"]?>  in  <? echo $_POST["currencyTo"];?>  </strong></td>
			  </tr>
			  <?
				}
			  ?>
			  <?
				$dis =  $_POST["chDiscount"];
				
			  
			  if ($dis!="on") 
			  {
			   ?>
              <tr>
                <td width="150" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
                <td width="200"> <? echo $_POST["IMFee"]?> </td>
                <td width="100" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="200"><? echo $_POST["totalAmount"]?>                </td>
              </tr>
			  <? }
			  
			  else
			  {
			   ?>
			   <tr>
                <td width="150" align="right"><font color="#005b90"> <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?> Discount</font></td>
                <td width="200"><? echo $_POST["discount"];?> </td>
                <td width="100" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="200"><? echo $_POST["totalAmount"]?>                </td>
              </tr>			   
			 <? }?>
			 <?
				/**
				 * Distributor Commission - Which-Ever-Is-Higher Logic.
				 * The following interface will be displayed only if the CONFIG_CALCULATE_WIHC is defined and set to true.
				 * The commission will only be calculated for those distributors which will have enteries in "commission" table.
				 */
				if(CONFIG_CALCULATE_WIHC == true)
				{
					$settlementExchangeRate = getSettlementExchangeRate($_SESSION["distribut"], $_SESSION["currencyTo"]);
					$distributorCommission = 0;
						
					if($settlementExchangeRate != 0)
					{
						$actualAmount = $_SESSION["localAmount"] / $settlementExchangeRate;
						$tmpWIHCInfo = calculateWIHC($actualAmount, $_SESSION["distribut"], $_SESSION["collectionPointID"]);
						$distributorCommission = round($tmpWIHCInfo["VALUE"], 2);
					}
			?>
					<tr>
						<td align="right"><font color="#005b90">Distributor Commission</font></td>
						<td><?=$distributorCommission?></td>
					</tr>
			<?
				}
			?>
              <tr>
                <td width="150" align="right"><font color="#005b90">Transaction Purpose</font></td>
                <td width="200"><? if($_POST["transactionPurpose"] == "Other") {echo $_POST["other_pur"];} else{echo $_POST["transactionPurpose"];} ?>
                </td>
            <? if(CONFIG_REMOVE_FUNDS_SOURCES != "1")
               {?> 
                <td width="100" align="right"><font color="#005b90">Funds Sources</font></td>
                                <td width="200"><? echo $_POST["fundSources"]?>
                </td>
            <? }?>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Money Paid</font></td>
                <td width="200"><? echo $_POST["moneyPaid"]?>
                </td>
                
	        <? if(CONFIG_REMOVE_ADMIN_CHARGES != "1") { ?>        
				<td width="100" align="right"><font color="#005b90">Admin Charges</font></td>
                <td width="200">
        	<? 
				if ($_SESSION["transType"]=="Bank Transfer")
					echo $_POST["bankCharges"];
			?>
				</td>
			<? }?>
			</tr>
			<? if(CONFIG_BRANCH_NAME == "1") { ?>
				<tr>
					<td width="150" height="20" align="right"><font color="#005b90"> Branch Money Paid In</font> </td>
					<td width="200" height="20"><?=$_POST["branchName"]; ?></td>
				</tr>
			<? } ?>
			<? if(CONFIG_CASH_PAID_CHARGES_ENABLED) {
  				  	if($_SESSION["moneyPaid"]=='By Cash') {?>
						<tr>
							<td align="right"><font color="#005b90"> Cash Handling Charges </font></td>
							<td><? echo CONFIG_CASH_PAID_CHARGES;?></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
			<? 		}
				} 
			
			 if(CONFIG_CURRENCY_CHARGES == '1') {?>
				<tr>
					<td align="right"><font color="#005b90"> Currency Charges </font></td>
					<td><?=$_SESSION["currencyCharge"]?></td>
					<td><font color="#005b90"> Discount Request </font></td>
					<td><?=$_SESSION["discount_request"]?></td>
				</tr>
			<? } ?>
			</table>
			</fieldset></td>
				</tr>
				<tr>
					<td align="left"><fieldset>
						<legend class="style2">Transaction Details </legend>
						<table border="0" cellpadding="2" cellspacing="0">
							<tr>
								<td width="150" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
								<td width="200" height="20"><? echo $_POST["transType"]?></td>
								<td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
								<td width="200">&nbsp;</td>
							</tr>
							<? if($_REQUEST["transType"] == "ATM Card") { ?>
								<tr>
									<td width="150" height="20" align="right"><font color="#005b90">Card Name</font></td>
									<td width="200" height="20"><?=$_REQUEST["cardName"]?></td>
									<td width="100" align="right"><font color="#005b90">Card No</font></td>
									<td width="200"><?=$_REQUEST["cardNo"]?></td>
								  </tr>
								  <tr>
									<td width="150" height="20" align="right"><font color="#005b90">Bank Name</font></td>
									<td width="200" height="20"><?=$_REQUEST["bankName"]?></td>
									<td colspan="2">&nbsp;</td>
								  </tr>	
							<? } ?>
							<tr>
								<td width="150" height="20" align="right"><font color="#005b90">&nbsp;<? //echo $systemPre; ?></font></td>
				                <td width="200" height="20">
				<? //Transaction No.
				/*
				if($_POST["transID"] == "")
				{
						$query = "select count(transID) as cnt from ". TBL_TRANSACTIONS." where 1 ";
						if(CONFIG_TRANS_REF == '1')
						{
							$query .= " and custAgentID = '$custAgent'";
						}
						$nextAutoID = countRecords($query);
						$nextAutoID = $nextAutoID + 1;
						
							$senderAgent = selectFrom("select username from ". TBL_ADMIN_USERS." where userID = '$custAgent'");
							
							
							
							if(CONFIG_TRANS_REF == '1')
							{
								$imReferenceNumber = strtoupper($senderAgent["username"]) ."-". $nextAutoID ;
							}else{
								$imReferenceNumber = $systemPre ."-". $nextAutoID ;
									if(strstr(CUSTOM_AGENT_TRANS_REF , strtoupper($senderAgent["username"])))
									{
										$imReferenceNumber = strtoupper($senderAgent["username"]) ."-". $nextAutoID ;
									}
								
								}
						
						
							$strRandomQuery = selectFrom("SELECT * FROM ". TBL_TRANSACTIONS ." where refNumberIM = '".$imReferenceNumber."'");
				  		$oldReference = $strRandomQuery["refNumberIM"];
							while($oldReference)
							{
								$nextAutoID ++;
								
								
								
								if(CONFIG_TRANS_REF == '1')
								{
									$imReferenceNumber = strtoupper($username) . $nextAutoID ;
								}else{
										$imReferenceNumber = $systemPre."-". $nextAutoID ;///For Reference Number
										if(strstr(CUSTOM_AGENT_TRANS_REF , strtoupper($senderAgent["username"])))
										{
											$imReferenceNumber = strtoupper($senderAgent["username"]) ."-". $nextAutoID ;
										}	
									}
								
								
								$strRandomQuery = selectFrom("SELECT * FROM ". TBL_TRANSACTIONS ." where refNumberIM = '".$imReferenceNumber."'");
									
								$oldReference = $strRandomQuery["refNumberIM"];
							}
						echo $imReferenceNumber;
					}else{
						$query = selectFrom("select refNumberIM, transID from ". TBL_TRANSACTIONS." where transID = '".$_POST["transID"]."'");
						echo $imReferenceNumber = $query["refNumberIM"];
						}*/

				//echo $imReferenceNumber;
				?> </td>
              <?
            	if(CONFIG_HIDE_TIP_FIELD != "1"){
            	?>
            		<td width="150" align="right"><font color="#005b90"><? if (CONFIG_TIP_ENABLED == "1"){ echo(CONFIG_TIP); }else{ echo 'Tip';} ?></font></td>
                <td  align="left"><? echo $_POST["tip"]?></td>		
            		
            	<?
            	}else{
            	?>
                <td width="150">&nbsp;</td>
            		<td>&nbsp;</td>
   						<?
   						}
   						?>
              </tr>
              <? if(CONFIG_IDTYPE_PASSWORD == '1' && $_POST["benIdPassword"]!= "")
					  {
					  ?>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Password</font></td>
                <td width="200" height="20"><? echo $_POST["benIdPassword"]?></td>
                <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200">&nbsp;</td>
              </tr>
              <? } ?>
               <? if(CONFIG_ENABLE_EX_RATE_LIMIT == '1')
					  {
					  ?>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Notify Exchange Rate Limit</font></td>
                <td width="200" height="20"><? echo $_POST["exRateLimitMin"]?> to <? echo $_POST["exRateLimitMax"]?></td>
                <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200">&nbsp;</td>
              </tr>
              <? } ?>
              
          		</table>
            </fieldset>
              </td>
          </tr>
          <tr>
            <td align="center"><table width="100%"  border="0" cellspacing="0" cellpadding="10">
              <tr>
                <td width="50%"> _______________________________<br>
                  <br>
                Agent Official Signature</td>
                <td width="50%" align="right"> _______________________________<br>
                  <br>
                Sender Signature </td>
              </tr>
              <tr>
              	<td>
              		Customer Service Number <? echo CONFIG_INVOICE_FOOTER; ?>
              	</td>
              </tr>
              <tr>
              	<td height="20" colspan="3">
                        	<? if(CONFIG_TRANS_CONDITION_ENABLED==1)
                        	{
                        		if(defined("CONFIG_TRANS_COND"))
								{
									echo(CONFIG_TRANS_COND);
								}
								else
								{
										$termsConditionSql = selectFrom("SELECT
											company_terms_conditions
										 FROM 
											company_detail
										 WHERE 
											company_terms_conditions!='' AND 
											dated = (
													 SELECT MAX(dated)
													 FROM 
														company_detail 
													WHERE 
														company_terms_conditions!=''
													 )");
								if(!empty($termsConditionSql["company_terms_conditions"]))
									$tremsConditions = $termsConditionSql["company_terms_conditions"];
									
								echo $tremsConditions;
								}
                        	 }else{
                        	 ?>
                        	I accept that I haven't used any illegal ways to earn money and I am paying tax regulary. I also accept that I am not going to send this money for any illegal act.
                        	<? } ?>
                        	</td>
                        </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center"><div class='noPrint'>
				  <form name="addTrans" action="add-transaction-conf.php?r=<?=$_GET["r"]?>" method="post">
				  <input name="benAgentParentID" type="hidden" value="<? echo $benAgentParentID?>">
				  <input name="calculateBy" type="hidden" value="<? echo $_POST["calculateBy"]?>">
				  <input name="custAgentParentID" type="hidden" value="<? echo $custAgentParentID?>">
				  <input type="hidden" name="imReferenceNumber" value="<?=$imReferenceNumber;?>">
				  <input type="hidden" name="refNumber" value="<?=$_SESSION["refNumber"];?>">
				  <input type="hidden" name="mydDate" value="<?=$nowTime;?>">
					<input name="distribut" type="hidden" value="<? echo $_POST["distribut"];?>">	
					<input name="transID" type="hidden" value="<? echo $_POST["transID"];?>">					  
					<input name="senderAccumulativeAmount" type="hidden" value="<?=$senderAccumulativeAmount;?>">				  
						<input name="settlementRate" type="hidden" value="<?=$settlementRate;?>">				
						<input name="settlementValue" type="hidden" value="<?=$settlementValue;?>">
						<input name="settlementCurrency" type="hidden" value="<?=$settlementCurrency;?>">
				  
	  <?
	  		foreach ($_POST as $k=>$v) 
			{
				$hiddenFields .= "<input name=\"" . $k . "\" type=\"hidden\" value=\"". $v ."\">\n";
				$str .= "$k\t\t:$v<br>";
			}
		
//echo $str;
echo $hiddenFields;//window.open('printing_reciept.php?transID=< echo $imReferenceNumber; >', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')
	  ?>
<? if(CONFIG_DISABLE_BUTTON == '1'){ ?>
       
			<input type="Submit" name="SubmitButton" value="Confirm Order"  id="once" onClick="javascript:document.getElementById('once').disabled=true;<? if(CONFIG_COMPLIANCE_PROMPT_CONFIRM== "1"){ ?> checkForm(addTrans,'yes')<? }else{?>document.addTrans.submit();<?} ?>" >
			<? }else { ?>
			
			<input type="Submit" name="SubmitButton" value="Confirm Order" onClick="<? if(CONFIG_COMPLIANCE_PROMPT_CONFIRM== "1"){ ?> checkForm(addTrans,'yes')<? } ?>">
			<? } ?>
            </form>			
			</div></td>
          </tr>
          <tr>
            <td align="right"><div class='noPrint'><a href="<?=$returnPage?>?transID=<? echo $_POST["transID"]?>&from=conf&calculateBy=<?=$_POST["calculateBy"]?>&getCalByValue=Y" class="style2">Change Information</a></div></td>
          </tr>
        </table></td>
    </tr>

</table>
</body>
</html>