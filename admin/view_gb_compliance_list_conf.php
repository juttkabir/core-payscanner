<?php 
	/**
	 * @package Online Register Users
	 * @subpackage GB Compliance Senders List
	 * This is the Page for the Ajax Processing for GB Compliance Sender List
	 * @author Mirza Arslan Baig
	 */

use Payex\Helper\AgentHelper;
use Payex\Repository\AgentRepository;

require '../src/bootstrap.php';

session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	$agentType = getAgentType();
	$parentID = $_SESSION["loggedUserData"]["userID"];
	$user=$agentType;

    $agentRepository = new AgentRepository($app->getDb());
    $agentHelper = new AgentHelper();
    $agent = $agentRepository->getById($parentID);
	
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");

	$response	= new Services_JSON();
	$page		= $_REQUEST['page']; // get the requested page 
	$limit		= $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx		= $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord		= $_REQUEST['sord']; // get the direction 
	
	if(!$sidx)
	{
		$sidx = 1;
	}				
	if($limit == 0)
	{
		$limit=50;
	}
	$bolExport = false;
	$action = $_REQUEST['getGrid'];
	$customerName = $_REQUEST['custName'];
	$customerNumber = $_REQUEST['custNum'];
	$fromDate = $_REQUEST['fdate'];
	$toDate = $_REQUEST['tdate'];
	/**
	 * This Piece of Code is for Exporting Excel File and Print
	 */
	
	if($action == 'export' || $action == 'print'){
		$bolExport = true;
		if($action == 'export'){
			header("Content-type: application/x-msexcel"); 
			header("Content-Disposition: attachment; filename=GB_Compliance_List.xls"); 
			header("Content-Description: PHP/MYSQL Generated Data");
			header('Content-Type: application/vnd.ms-excel');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		}
		$strMainStart = '<table border="1" align="center" cellspacing="0" width="900">';
		$strMainEnd = '</table>';
		$strRowStart = '<tr>';
		$strRowEnd = '</tr>';
		$strColStart = '<td>&nbsp;';
		$strColStartSr = '<td align="left">';
		$strColEndSr = '</td>';
		$strColEnd = '</td>';
		
		$strHead = "<th colspan='8' style='font-size:20pt'>New Senders List</th>";
		$strHeadCols = "<th>Serial No.</th>";
		$strHeadCols .= "<th>Customer Name</th>";
		$strHeadCols .= "<th>Reference Number</th>";
		$strHeadCols .= "<th>Creation Date</th>";
		$strHeadCols .= "<th>Email</th>";
		$strHeadCols .= "<th>Country</th>";
		$strHeadCols .= "<th>Address</th>";
		$strHeadCols .= "<th>Status</th>";
		$strHeadCols .= "<th>Browser Name</th>";
	}
	
	
	if($_REQUEST['Submit'] == 'Search'){
		$condition = " ";
		if(!empty($customerName))
			$condition = " AND customerName LIKE '$customerName%'";
		if(!empty($customerNumber))
			$condition .= " AND accountName = '$customerNumber'";
		else
			$condition .= " AND accountName LIKE 'CW-%'";
        if ($agentHelper->isAgent($agent)){
            $condition .= " AND agentID = '{$agent->getId()}'";
        }
		if(empty($toDate))
			$toDate = date("Y-m-d");
		//if(empty($fromDate))
			//$fromDate = '2000-01-01';
		
		if(!empty($fromDate))
			$condition .= " AND (created >= '$fromDate 00:00:00' AND created <= '$toDate 23:59:59') ";
		else
			$condition .= " AND (created <= '$toDate 23:59:59') ";
		
		$condition .= " AND customerType NOT LIKE 'company'";
		$query = "SELECT date_format(created, '%Y-%m-%d') as creationDate, customerID, customerName, customerStatus, Country, accountName,Address,email,customerType, browserName FROM ".TBL_CUSTOMER." WHERE 1";
		
		if(!empty($query) && !empty($condition))
			$query .= $condition;
		if(!empty($query)){
			$allCustomersData = mysql_query($query) or die(__LINE__ .":". mysql_error());
			$allCustomersCount = mysql_num_rows($allCustomersData);
			if($allCustomersCount > 0){
				$totalPages = ceil($allCustomersCount / $limit);
			}else{
				$totalPages = 0;
			}
			if($page > $totalPages)
				$page = $totalPages;
			$start = $limit * $page - $limit;
			
			if($start < 0)
				$start = 0;
			if($bolExport)
				$query .= " ORDER BY customerName";
			else
				$query .= " ORDER BY $sidx $sord LIMIT $start, $limit";
			$customersData = mysql_query($query) or die(__LINE__.":".mysql_error());
			if($action == 'export' || $action == 'print'){
				$strExportData = $strMainStart.$strRowStart.$strHead.$strRowEnd;
				$strExportData .= $strRowStart.$strHeadCols;
			}
			else{
				$response->page = $page;
				$response->total = $totalPages;
				$response->records = $allCustomersCount;
			}
			$i = 0;
			while($customer = mysql_fetch_array($customersData)){
				
				$serialNo = $i +1;
				if($action == 'export' || $action == 'print'){
					$strExportData .= $strRowEnd.$strRowStart.$strColStartSr.$serialNo.$strColEndSr;
					$strExportData .= $strColStart.$customer['customerName'].$strColEnd;
					$strExportData .= $strColStart.$customer['accountName'].$strColEnd;
					$strExportData .= $strColStart.$customer['creationDate'].$strColEnd;
					$strExportData .= $strColStart.$customer['email'].$strColEnd;
					$strExportData .= $strColStart.$customer['Country'].$strColEnd;
					$strExportData .= $strColStart.$customer['Address'].$strColEnd;
					$strExportData .= $strColStart.$customer['customerStatus'].$strColEnd;
					$strExportData .= $strColStart.$customer['browserName'].$strColEnd;
				}
				
				$response->rows[$i]['id'] = 'AA-'.$customer['customerID'];
				$response->rows[$i]['cell'] = array(
					$serialNo,
					$customer['customerName'],
					$customer['accountName'],
					$customer['Address'],
					$customer['Country'],
					$customer['email'],
					$customer['creationDate'],
					'<a href="javascript:" onClick="complianceDetails('.$customer['customerID'].');">Details</a>',
					'<a href="javascript:" onClick="documentDetails('.$customer['customerID'].');">view</a>',
					'GB Group',
					$customer['customerStatus'],
					$customer['browserName']
					//,'<a href="javascript:" onClick="customerEdit(\''.$customer['customerID'].'\',\''.$customer['customerType'].'\'); ">Edit</a>'
				);
				$i++;
			}
				$strExportData .= $strRowEnd.$strMainEnd;
				if($action == 'export'){
					echo $strExportData;
				}
				elseif($action == 'print'){
					$strExportData .= '<div align="center"><input type="button" value="Print" onClick="print();"/></div>';
					echo $strExportData;
				}else
					echo $response->encode($response);
		}
	}
	
	
?>