<?
	session_start();

	include ("../include/config.php");
	include ("security.php");
	
	$agentType = getAgentType();

	/**
	 * Variables from Query String
	 */
		$userId = $_REQUEST["userId"];
		$currentDate = $_REQUEST["currentDate"];
	/**
	 * Some pre data preparation.
	**/

  $condition = "";
  
  if($userId	== "superAdmin" || $userId	=="all" ){
  	$condition1 = "adminType = 'Supper'
							AND agentType = 'Supper'
							AND parentID != '0'
							AND isCorrespondent = 'ONLY'
							AND (agentStatus='Disabled' || agentStatus='Suspended')";
	}
	if($userId	 == "adminStaff" || $userId	=="all"){
	$condition2 ="adminType IN ('Admin', 'Call', 'COLLECTOR', 'Branch Manager', 'Admin Manager', 'Support', 'SUPI Manager', 'MLRO', 'AM')
						AND (agentStatus='Disabled' || agentStatus='Suspended')
						AND agentType = 'Supper'";
	}
	if($userId	 == "superAnD" || $userId	=="all"){
	$condition3 =" adminType = 'Agent'
						AND agentType = 'Supper'
						AND parentID != '0'
						AND isCorrespondent = 'Y'
						AND (agentStatus='Disabled' || agentStatus='Suspended')";
	}
	if($userId	 == "sunAnD" || $userId	=="all"){
	$condition4 ="adminType = 'Agent'
						AND agentType = 'Sub'
						AND parentID != '0'
						AND isCorrespondent = 'Y'
						AND (agentStatus='Disabled' || agentStatus='Suspended')";
	}				
							
	
	if($userId	== "superAdmin" || $userId	=="all")
	{
		$superAdminSql = "SELECT 
							userID,
							username, 
							name,
							disableReason,
							suspensionReason,
							agentStatus,
							agentCompany
						FROM 
							".TBL_ADMIN_USERS."
						WHERE 
							".$condition1."
						ORDER BY
							name
						";
			$superAdminResult = SelectMultiRecords($superAdminSql);
			//debug($superAdminSql);
}
if($userId	 == "adminStaff" || $userId	=="all"){
	
			$adminStaffSql = "select 
							userID,
							username, 
							name, 
							disableReason,
							suspensionReason, 
							agentStatus,
							adminType 
					from 
						".TBL_ADMIN_USERS." 
					where 
						".$condition2."
					    ORDER BY
							name
						";
			//debug($adminStaffSql);
			$adminStaffResult = selectMultiRecords($adminStaffSql);
				
}
if($userId	 == "superAnD" || $userId	=="all"){

$superAnDSql = "select 
							userID,
							username, 
							name, 
							disableReason,
							suspensionReason,  
							agentStatus,
							adminType 
					from 
						".TBL_ADMIN_USERS." 
					where 
				       ".$condition3."
					    ORDER BY
							name
						";
			//debug($superAnDSql);
			$superAnDResult = selectMultiRecords($superAnDSql);
}
if($userId	 == "sunAnD" || $userId	=="all"){
$subAnDSql = "select 
							userID,
							username, 
							name, 
							disableReason,
							suspensionReason, 
							agentStatus, 
							adminType 
					from 
						".TBL_ADMIN_USERS." 
					where 
				       ".$condition4."
					    ORDER BY
							name
						";
			//debug($subAnDSql);
			$subAnDResult = selectMultiRecords($subAnDSql);

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Disabled &amp; Suspended Users</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<style>
.ce{
	color:#0066FF;
	cursor:pointer;
}
</style>
<script src="javascript/jquery.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script type="text/javascript">

$(document).ready(function(){
    Date.format = 'yyyy-mm-dd';
	$("#currentDate").datePicker({
		startDate: '2000-01-01'
	});
		
});

function updateBalance(b)
{
	var banks = parseFloat(document.getElementById("banks").value);
	var closingBalance = parseFloat(document.getElementById("closingBalance").value);
	var final = 0;
	
	closingBalance = closingBalance + b;
	final = closingBalance - banks;
	
	document.getElementById("closingBalance").value = final;
	document.getElementById("balanceAfterBank").innerHTML = final;
	document.getElementById("closingFinal").innerHTML = final;
}
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}


function manageDisplay(rec)
{
	if($("#right"+rec).html() == "[+]")
		$("#right"+rec).html("[-]");
	else
		$("#right"+rec).html("[+]");
	
	$(".sub"+rec).toggle();
}
<!--
function popup(url) 
{
 var width  = 1000;
 var height = 900;
 var left   = (screen.width  - width)/2;
 var top    = (screen.height - height)/2;
 var params = 'width='+width+', height='+height;
 params += ', top='+top+', left='+left;
 params += ', directories=no';
 params += ', location=no';
 params += ', menubar=no';
 params += ', resizable=yes';
 params += ', scrollbars=yes';
 params += ', status=no';
 params += ', toolbar=no';
 newwin=window.open(url,'windowname5', params);
 if (window.focus) {newwin.focus()}
 return false;
}
// -->

</script>

</head>

<body>

	<form action="" method="post" name="frmSearch">
	<table width="80%" border="0" align="center" cellpadding="3" cellspacing="0" class="boundingTable">
		<tr>
			<td class="columnTitle" colspan="2">Search</td>
		</tr>
		<tr>
			<td align="right">Users</td>
			
			<td align="left">
				<select name="userId" id="userId">
					<option value="">- Select User -</option>
					<option value="all">All</option>
						<option value="superAdmin">Super Admin</option>
						<option value="superAnD">Super A&D </option>
						<option value="sunAnD">Sub A&D </option>
						<option value="adminStaff">Admin Staff </option>
				</select>
				<script language="JavaScript">
					SelectOption(document.forms[0].userId, "<?=$userId; ?>");
			   </script>
			</td>
		</tr>
		
		<tr>
			<td width="50%" align="right" class="reportToolbar" colspan="2"><input type="submit" name="Submit" value="Submit" /></td>
		</tr>
	</table>
	</form>



<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
			
				<tr>
					<td align="center" class="reportHeader">DISABLED / SUSPENDED USERS LIST <br />
					<span class="reportSubHeader"><?=date("l, d F, Y")?><br />
					<br />
					</span></td></tr>
			</table>
			<br />
			<form action="" method="post">
			<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
				<tr>
					<td width="30%" class="columnTitle">Contact Person</td>
					<td width="15%" class="columnTitle">User Number </td>
					<td width="30%" align="left" class="columnTitle">Company </td>
					<td width="15%" align="left" class="columnTitle">Users Status</td>
					<td width="30%" align="left" class="columnTitle">Modify Reason </td>
					<td width="14%" align="left" class="columnTitle">Action</td>
				</tr>
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr class="columnTitle">
				<td align="left" id="right1" class="ce" onclick="manageDisplay(1);">[-]</td>
					<td class="heading" colspan="5">Super Admin</td>
				</tr>
				
				<?				
					for($i=0; $i<sizeof($superAdminResult); $i++)
					{
						$contactPerson	= $superAdminResult[$i]["name"];
						$username	= $superAdminResult[$i]["username"];
						$agentCompany	= $superAdminResult[$i]["agentCompany"];
						$status	= $superAdminResult[$i]["agentStatus"];
						$disableReason	= $superAdminResult[$i]["disableReason"]." ".$superAdminResult[$i]["suspensionReason"];
				?>
				<tr class="sub1">
					<td><?=$contactPerson?>&nbsp;</td>
					<td align="left"><?=$username?></td>
					<td align="left"><?=$agentCompany?>&nbsp;</td>
					<td align="left"><?=$status?>&nbsp;</td>
					<td align="left"><?=$disableReason?>&nbsp;</td>
					<td align="left">
						<a href="javascript:;" onClick=" window.open('activate_super_admin.php?userID=<?=$superAdminResult[$i]["userID"]; ?>&from=disabled-users-list.php&userId=<?=$userId?>','activateSuperAdmin', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=200,width=460')"><font color="#005b90"><b>Activate</b></font></a>
					</td>
				</tr>
				<? } ?>			
					<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				
				<tr class="columnTitle">
				<td align="left" id="right2" class="ce" onclick="manageDisplay(2);">[-]</td>
					<td class="heading" colspan="5">Admin Staff</td>
				</tr>
				
				<?				
					for($i=0; $i<sizeof($adminStaffResult); $i++)
					{
						$contactPerson	= $adminStaffResult[$i]["name"];
						$username	= $adminStaffResult[$i]["username"]."  <b>[".$adminStaffResult[$i]["adminType"]."]</b>";
						$agentCompany	= $adminStaffResult[$i]["agentCompany"];
						$status	= $adminStaffResult[$i]["agentStatus"];
						$disableReason	= $adminStaffResult[$i]["disableReason"]." ".$adminStaffResult[$i]["suspensionReason"];
				?>
				<tr class="sub2">
					<td><?=$contactPerson?>&nbsp;</td>
					<td align="left"><?=$username?></td>
					<td align="left"><?=$agentCompany?>&nbsp;</td>
					<td align="left"><?=$status?>&nbsp;</td>
					<td align="left"><?=$disableReason?>&nbsp;</td>
					<td align="left">
						<a href="javascript:;" onClick=" window.open('activate_super_admin.php?userID=<?=$adminStaffResult[$i]["userID"]; ?>&from=disabled-users-list.php&userId=<?=$userId?>','activateSuperAdmin', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=200,width=460')"><font color="#005b90"><b>Activate</b></font></a>
					</td>
				</tr>
				<? } ?>	
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>		
				<tr class="columnTitle">
				<td align="left" id="right3" class="ce" onclick="manageDisplay(3);">[-]</td>
					<td class="heading" colspan="5">Super AnD</td>
				</tr>
				
				<?				
					for($i=0; $i<sizeof($superAnDResult); $i++)
					{
						$contactPerson	= $superAnDResult[$i]["name"];
						$username	= $superAnDResult[$i]["username"];
						$agentCompany	= $superAnDResult[$i]["agentCompany"];
						$status	= $superAnDResult[$i]["agentStatus"];
						$disableReason	= $superAnDResult[$i]["disableReason"]." ".$superAnDResult[$i]["suspensionReason"];
				?>
				<tr class="sub3">
					<td><?=$contactPerson?>&nbsp;</td>
					<td align="left"><?=$username?></td>
					<td align="left"><?=$agentCompany?>&nbsp;</td>
					<td align="left"><?=$status?>&nbsp;</td>
					<td align="left"><?=$disableReason?>&nbsp;</td>
					<td align="left">
						<a href="javascript:;" onClick=" window.open('activate_super_admin.php?userID=<?=$superAnDResult[$i]["userID"]; ?>&from=disabled-users-list.php&userId=<?=$userId?>','activateSuperAnD', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=200,width=460')"><font color="#005b90"><b>Activate</b></font></a>
					</td>
				</tr>
				<? } ?>		
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>	
				
				<tr class="columnTitle">
				<td align="left" id="right4" class="ce" onclick="manageDisplay(4);">[-]</td>
					<td class="heading" colspan="5">Sub AnD</td>
				</tr>
				
				<?				
					for($i=0; $i<sizeof($subAnDResult); $i++)
					{
						$contactPerson	= $subAnDResult[$i]["name"];
						$username	= $subAnDResult[$i]["username"];
						$agentCompany	= $subAnDResult[$i]["agentCompany"];
						$status	= $subAnDResult[$i]["agentStatus"];
						$disableReason	= $subAnDResult[$i]["disableReason"]." ".$subAnDResult[$i]["suspensionReason"];
				?>
				<tr class="sub4">
					<td><?=$contactPerson?>&nbsp;</td>
					<td align="left"><?=$username?></td>
					<td align="left"><?=$agentCompany?>&nbsp;</td>
					<td align="left"><?=$status?>&nbsp;</td>
					<td align="left"><?=$disableReason?>&nbsp;</td>
					<td align="left">
						<a href="javascript:;" onClick=" window.open('activate_super_admin.php?userID=<?=$subAnDResult[$i]["userID"]; ?>&from=disabled-users-list.php&userId=<?=$userId?>','activateSubAnD', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=200,width=460')"><font color="#005b90"><b>Activate</b></font></a>
					</td>
				</tr>
				<? } ?>			
				
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
			</table>
			<br />
			
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr>
						  <td width="50%" align="right" class="reportToolbar"><input name="id" type="hidden" id="id" value="<?=$id?>" />
								<input name="userId" type="hidden" id="userId" value="<?=$userId?>" />
							
						</tr>
					</table>
		
			</form>
		</td>
	</tr>
</table>

</body>
</html>
