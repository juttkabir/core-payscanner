<?php  
/*if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE')) {
    session_cache_limiter("public");
}*/
session_start();
include ("../include/config.php");
include ("security.php");
if(isset($_REQUEST["format"]) && $_REQUEST["format"] == "csv")	
{
	/* Export version is csv */
	//echo '##1!te';
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
	
	header('Content-type: application/octet-stream');
	header("Content-disposition: filename=transactionsExportedCSV.csv");
	
	
	//$strDbSql = "select dispatchNumber from dispatch_book where recId=".$_REQUEST["recId"];
	//$arrDbSql = selectFrom($strDbSql);
	
	
	if(!empty($_REQUEST["exportspb"]) && $_REQUEST["exportspb"] == "t" && !empty($_REQUEST["at"]))
	{
		$arrAllTransactions = substr($_REQUEST["at"],0,strlen($_REQUEST["at"])-1);
		
		$strSql = "
				select 
					tr.refNumberIM, 
					tr.dispatchDate, 
					tr.localAmount, 
					ben.Title as bt,
					ben.firstName as bf, 
					ben.middleName as bm,
					ben.lastName as bl, 
					ben.City, 
					ben.CPF, 
					tbd.originalBankId, 
					tbd.bankName, 
					tbd.accNo, 
					tbd.accountType, 
					tbd.branchCode,
					cust.Title,
					cust.firstName, 
					cust.middleName,
					cust.lastName,
					cust.IDNumber 
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben, 
					customer as cust
				where 
					tr.transID IN (".$arrAllTransactions.") and
					tbd.transID = tr.transID and 
					tr.benID = ben.benID and
					tr.customerID = cust.customerID					
				order by 
					tbd.bankName asc";
		
		
	}
	else
	{
	
		$strSql = "
				select 
					tr.refNumberIM, 
					tr.dispatchDate, 
					tr.localAmount, 
					ben.Title as bt,
					ben.firstName as bf, 
					ben.middleName as bm,
					ben.lastName as bl, 
					ben.City, 
					ben.CPF, 
					tbd.originalBankId,  
					tbd.bankName, 
					tbd.accNo, 
					tbd.accountType, 
					tbd.branchCode,
					cust.Title,
					cust.firstName, 
					cust.middleName,
					cust.lastName,
					cust.IDNumber 
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben, 
					customer as cust
				where 
					tr.dispatchNumber = '".$_REQUEST["recId"]."' and
					tbd.transID = tr.transID and 
					tr.benID = ben.benID and
					tr.customerID = cust.customerID  
				order by 
					tbd.bankName asc";
					//$arrDbSql["dispatchNumber"]
	}
	$fullRS = SelectMultiRecords($strSql);
	//debug($strSql);
	//debug($fullRS);

	$strCsvOutput = "";
	
	//$strCsvOutput .= "Transaction Id,Dispatch Date,Sender Name,Sender Document Number,Beneficiary Name,Beneficiary City,CPF/CPNJ,Bank Id,Bank Name,Branch Number,Account Number,Bank Account Type,Local Amount\r\n";
	
	$cpfseperator = array("-","/",".");
	for($i=0; $i < count($fullRS); $i++)
	{
		
		$beneficiaryFullName = $fullRS[$i]["bf"]." ".$fullRS[$i]["bm"]." ".$fullRS[$i]["bl"];
		
		$senderFullName = $fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"];
		
		$strNewFormatTime = strtotime($fullRS[$i]["dispatchDate"]);
		
		$strCsvOutput .= substr($fullRS[$i]["refNumberIM"],0,20).",";
		$strCsvOutput .= substr(date("m/d/Y",$strNewFormatTime),0,20).",";
		$strCsvOutput .= trim(substr($senderFullName,0,40)).",";
		$strCsvOutput .= substr($fullRS[$i]["IDNumber"],0,40).",";
		$strCsvOutput .= trim(substr($beneficiaryFullName,0,40)).",";
		
		$strCsvOutput .= substr($fullRS[$i]["City"],0,30).",";
		$strCsvOutput .= substr(str_replace($cpfseperator,"",$fullRS[$i]["CPF"]),0,14).",";
		$strCsvOutput .= substr($fullRS[$i]["originalBankId"],0,3).",";
		$strCsvOutput .= substr($fullRS[$i]["bankName"],0,60).",";
		
		$strCsvOutput .= substr(str_pad(str_replace("-","",$fullRS[$i]["branchCode"]),5,"0"),0,5).",";
		
		$strCsvOutput .= substr($fullRS[$i]["accNo"],0,20).",";
		$strCsvOutput .= ($fullRS[$i]["accountType"] == "Savings"?"CP":"CC").",";

		//$strCsvOutput .= substr(round($fullRS[$i]["localAmount"],2),0,10);
		$strCsvOutput .= substr(number_format($fullRS[$i]["localAmount"], 2, ".", ","),0,10);
		
		$strCsvOutput .= "\r\n";
	}
	
	echo $strCsvOutput;
	/* End of csv version */
}
else
{
	/* default export version will be excel */
	
	header('Content-Type: application/vnd.ms-excel');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

	header ("Content-type: application/x-msexcel");
	header ("Content-Disposition: attachment; filename=dispatchedTransactions.xls"); 
	header ("Content-Description: PHP/MYSQL Generated Data");
	
	
	$strDbSql = "select dispatchNumber from dispatch_book where recId=".$_REQUEST["recId"];
	$arrDbSql = selectFrom($strDbSql);
	
	$strSql = "
				select 
					tbd.bankName, 
					tr.refNumberIM, 
					tr.benID, 
					tr.transDate, 
					tr.localAmount, 
					tr.benAgentID, 
					ben.firstName, 
					ben.middleName,
					ben.lastName, 
					ben.Country, 
					ben.IDType, 
					ben.IDNumber, 
					ben.userType, 
					ben.CPF, 
					tbd.accNo, 
					tbd.accountType, 
					tbd.branchCode 
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben 
				where 
					tbd.transID = tr.transID and 
					tr.benID = ben.benID and 
					tr.dispatchNumber = '".$arrDbSql["dispatchNumber"]."' order by tbd.bankName asc";
	
	$fullRS = SelectMultiRecords($strSql);
//	ob_start();
?>
	<table>
		<tr>
			<th>DATE</th>
			<th>ORDER</th>
			<th>BENEFICIARY</th>
			<th>ID</td><th></th>
			<th>SORT CODE</th>
			<th>ACCOUNT</th><th></th>
			<th>VALUE</th>
		</tr>
<?
	for($i=0; $i < count($fullRS); $i++)
	{
		
		$lastBankName = $fullRS[$i-1]["bankName"];
		$nextBankName = $fullRS[$i+1]["bankName"];
		$totalAmount += $fullRS[$i]["localAmount"];
		$totalTrasactions += 0;
		if((!empty($lastBankName) && $lastBankName != $fullRS[$i]["bankName"]) || $i == 0)
		{
			$noOfOrders = 0;
			$amountUnderOneBank = 0;
		?>
			<tr>
				<td><?=$fullRS[$i]["bankName"]?></td>
			</tr>
		<?
		}
		$noOfOrders += 1;
		$amountUnderOneBank += $fullRS[$i]["localAmount"];
		?>
		<tr>
			<td><?=$fullRS[$i]["transDate"]?></td>
			<td><?=$fullRS[$i]["refNumberIM"]?></td>
			<td><?=$fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"]?></td>
			
			<? if($fullRS[$i]["IDType"] != "CPF") {	?>
			<td><?=($fullRS[$i]["userType"] == "Business User"?"CPNJ":"CPF")?></td>
			<td><?=$fullRS[$i]["CPF"]?></td>
			<? } else { ?>
			<td><?=(!empty($fullRS[$i]["IDType"]) ? $fullRS[$i]["IDType"] :"&nbsp;")?></td>
			<td><?=(!empty($fullRS[$i]["IDNumber"]) ? $fullRS[$i]["IDNumber"] :"&nbsp;")?></td>
			<? } ?>
			
			<td><?=(!empty($fullRS[$i]["branchCode"])? $fullRS[$i]["branchCode"] : "&nbsp;" )?></td>
			<td><?=$fullRS[$i]["accNo"]?></td>
			<td><?=$fullRS[$i]["accountType"]?></td>
			<td><?=number_format($fullRS[$i]["localAmount"],2,".",",")?></td>
		</tr>
<?
		if((!empty($nextBankName) && $nextBankName != $fullRS[$i]["bankName"]) || $i == count($fullRS)-1)
		{
?>
		<tr>
			<td>Number of Orders: <?=$noOfOrders?> </td>
			<td>-----</td>
			<td>Total Bank: </td>
			<td><?=number_format($amountUnderOneBank,2,".",",")?></td>
		</tr>
<?
		}
	}
?>
	</table>
<?
/* End of Excel export version */
}
?>
