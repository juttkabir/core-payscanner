<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$today = date('Y-m-d');//date("d-m-Y");
//$today = date('d-m-Y  h:i:s A');//date("d-m-Y");
$agentType=getAgentType();
$loggedUName = $_SESSION["loggedUserData"]["username"];
$returnPage = 'add-transaction.php';

$cumulativeRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction')";

$cumulativeRule = selectFrom($cumulativeRuleQuery);

$currentRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction')";
$currentRule = selectFrom($currentRuleQuery);


if(CONFIG_TRANS_ROUND_LEVEL != "")
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 4;
	
	}

if($_GET["transID"] != ""){
		
		$transactionQuery = selectFrom("select * from ". TBL_TRANSACTIONS." where transID = '".$_GET["transID"]."'");
		$bankQuery = selectFrom("select * from ". TBL_BANK_DETAILS." where transID = '".$_GET["transID"]."'");
	}
	
if($_POST["customerID"]!= ""){
	$customerID = $_POST["customerID"];
	}else{
	$customerID = $transactionQuery["customerID"];
}

if($_POST["benID"]!= ""){
	$beneficiaryID = $_POST["benID"];
	}else{
	$beneficiaryID = $transactionQuery["benID"];
}


if($_POST["customerAgent"]!= ""){
	$custAgentID = $_POST["customerAgent"];
	}else{
	$custAgentID = $transactionQuery["custAgentID"];
}

		$strAddressQuery = selectFrom("select * from ". TBL_ADMIN_USERS." where userID ='".$custAgentID."'");


if($_POST["totalAmount"]!= ""){
$totalAmount= $_POST["totalAmount"];
}else{
$totalAmount=$transactionQuery["totalAmount"];	
	}
	
	
if($_POST["transAmount"]!= ""){
$transAmount= $_POST["transAmount"];
}else{
$transAmount=$transactionQuery["transAmount"];	
	}	
	
	if($_POST["localAmount"]!= ""){
$localAmount= $_POST["localAmount"];
}else{
$localAmount=$transactionQuery["localAmount"];	
	}
	



if($_POST["moneyPaid"]!= ""){
$moneyPaid= $_POST["moneyPaid"];
}else{
$moneyPaid=$transactionQuery["moneyPaid"];	
	}
	
	
if($_POST["transactionPurpose"]!= ""){
$transactionPurpose= $_POST["transactionPurpose"];
}else{
$transactionPurpose=$transactionQuery["transactionPurpose"];	
	}



$discount=$transactionQuery["discounted_amount"];	



if($_POST["exchangeRate"]!= ""){
$exchangeRate= $_POST["exchangeRate"];
}else{
$exchangeRate=$transactionQuery["exchangeRate"];	
	}


if($_POST["IMFee"]!= ""){
$IMFee= $_POST["IMFee"];
}else{
$IMFee=$transactionQuery["IMFee"];	
	}



if($_POST["bankName"]!= ""){
$bankName= $_POST["bankName"];
}else{
$bankName=$bankQuery["bankName"];	
	}

if($_POST["branchCode"]!= ""){
$branchCode= $_POST["branchCode"];
}else{
$branchCode=$bankQuery["branchCode"];	
	}

if($_POST["branchAddress"]!= ""){
$branchAddress= $_POST["branchAddress"];
}else{
$branchAddress=$bankQuery["branchAddress"];	
	}


if($_POST["swiftCode"]!= ""){
$swiftCode= $_POST["swiftCode"];
}else{
$swiftCode=$bankQuery["swiftCode"];	
	}



if($_POST["accNo"]!= ""){
$accNo= $_POST["accNo"];
}else{
$accNo=$bankQuery["accNo"];	
	}




if($_POST["currencyTo"]!= ""){
$currencyTo= $_POST["currencyTo"];
}else{
$currencyTo=$transactionQuery["currencyTo"];	
	}
	
	
	
if($_POST["currencyFrom"]!= ""){
$currencyFrom= $_POST["currencyFrom"];
}else{
$currencyFrom=$transactionQuery["currencyFrom"];	
	}
	
	
	if($_POST["benCountry"]!= ""){
$ToCountry= $_POST["benCountry"];
}else{
$ToCountry=$transactionQuery["toCountry"];	
	}
	
	
	if($_POST["transType"] != ""){
		
		$transactionType = $_POST["transType"];
		} else{
			$transactionType = $transactionQuery["transType"];	
			}
			

if($_POST["bankCharges"] != ""){
		
		$bankCharges = $_POST["bankCharges"];
		} else{
			$bankCharges = $transactionQuery["bankCharges"];	
			}
			
if($_POST["refNumber"] != ""){			
	
			$refNumber=$_POST["refNumber"];
	
}else{
			$refNumber=$transactionQuery["refNumber"];
		}
	
	$discount=$transactionQuery["discounted_amount"];	
	
$refNumberIM=$transactionQuery["refNumberIM"];


$printAddress = $strAddressQuery["agentAddress"];
$printPhone = $strAddressQuery["agentPhone"];
$printSenderNo = $CustomerData["accountName"];
$agentUserName = $strAddressQuery["username"];
$agentName = $strAddressQuery["name"];
$custAgentParentID = $strAddressQuery["parentID"];

$distributorId = $transactionQuery["benAgentID"];

$distributorNameQuery = selectFrom("select name from ". TBL_ADMIN_USERS." where userID ='".$distributorId."'");
$distributorName = $distributorNameQuery["name"];

$currencyNameQuery = selectFrom("select description from ". TBL_CURRENCY." where currencyName ='".$currencyTo."'");
$currencyName = $currencyNameQuery["description"];

$fontColor = "#800000";

// This code is added by Niaz Ahmad against #1727 at 08-01-2008
if(CONFIG_SENDER_ACCUMULATIVE_AMOUNT == "1"){

$senderAccumulativeAmount = 0;
//$senderTransAmount = selectMultiRecords("select accumulativeAmount from ".TBL_CUSTOMER." where customerID = '".$_POST["customerID"]."'");


           
            		 $to = getCountryTime(CONFIG_COUNTRY_CODE);
									
									$month = substr($to,5,2);
									$year = substr($to,0,4);
									$day = substr($to,8,2);
									$noOfDays = CONFIG_NO_OF_DAYS;
									
									$fromDate = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d")-$noOfDays ,   date("Y")));

									
									$from = $fromDate." 00:00:00"; 
									$senderTransAmount = selectFrom("select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_POST["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'");
								// echo "select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_POST["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'";
 $senderAccumulativeAmount += $senderTransAmount["transAmount"];
 

	if($_POST["transID"] == "")
	{
 				$senderAccumulativeAmount += $_POST["transAmount"];
	}
}	

?>
<html>
<head>
<title>Transaction Confirmation</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<link href="styles/displaying.css" rel="stylesheet" type="text/css" media="print">

 <script language="javascript">
<!--
function checkForm(theForm,strval){
	
		var minAmount = 0;
		var maxAmount = 0;

		var condition;
  
 
		var conditionCumulative;
		var conditionCurrent;
  	var ruleConfirmFlag=true;
  	var ruleFlag=false;
  	var transSender = '';
	if(strval == 'Yes')
	{
		transSender="transSend";
		}else{
			transSender="NoSend";
			}
	
	var accumulativeAmount = 0;
	accumulativeAmount = <? echo $senderAccumulativeAmount ?>;
	var currentRuletransAmount = <?=$_POST["transAmount"]?>;	
	var currentRuleAmountToCompare = 0;
	var amountToCompareTrans; 
	
// cummulative Amount Rule /////////
<?	
if($cumulativeRule["ruleID"]!='') {
    
     if($cumulativeRule["matchCriteria"] == "BETWEEN"){
		  $betweenAmount= explode("-",$cumulativeRule["amount"]);
		  $fromAmount=$betweenAmount[0];   
		  $toAmount=$betweenAmount[1]; 
?>
		conditionCumulative = <?=$fromAmount?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmount?>;
		
		<?
    }else{
?>
    amountToCompare = <?=$cumulativeRule["amount"]?>;
    conditionCumulative =	accumulativeAmount  <?=$cumulativeRule["matchCriteria"]?> amountToCompare;
 <?	}?>			
		
	
		 if(conditionCumulative)
	   {

				
	   	if(confirm("<?=$cumulativeRule["message"]?>"))
    	{
    	    ruleConfirmFlag=true;
    			/*document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();*/
    		
                
      }else{ 
    
    			/*	document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   					
      	addTrans.localAmount.focus();*/
       
        ruleConfirmFlag=false;
       // ruleFlag=true;
      }

    }else{ 

    	   ruleConfirmFlag=true;
    } 
 
	
<? } ?>

////////////current transaction amount///////


<? if($currentRule["ruleID"]!=''){
	   
		 
	   if($currentRule["matchCriteria"] == "BETWEEN"){
				
		  $betweenAmountCurrent= explode("-",$currentRule["amount"]);
		  $fromAmountCurrent=$betweenAmountCurrent[0];   
		  $toAmountCurrent=$betweenAmountCurrent[1]; 
		?>
		conditionCurrent = <?=$fromAmountCurrent?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmountCurrent?>;
		<?
 }else{
 	?>      
 	      
 		    currentRuleAmountToCompare = <?=$currentRule["amount"]?>;
 		    conditionCurrent =	currentRuletransAmount  <?=$currentRule["matchCriteria"]?> currentRuleAmountToCompare;
 <?	}?>
	
 
 if(conditionCurrent)
	   {

				if(confirm("<?=$currentRule["message"]?>"))
				{
					
    	     ruleConfirmFlag=true;
    			
    			/*document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();*/
    		
              
     }else{
    
    				/*document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   				   	addTrans.localAmount.focus();*/
         ruleConfirmFlag=false;
         //ruleFlag=true;
      }

    }else{ 

    	     ruleConfirmFlag=true; 
    } 
  
<? } ?>  

 if(ruleConfirmFlag){
 	
   	document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
 	}
 	
 if(ruleConfirmFlag == false){
 	
 	  	  document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   			addTrans.localAmount.focus();
 	}		
	
}	
-->	
</script>    

<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style5 {
	color: #005b90;
	font-weight: bold;
}
-->
</style>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<fieldset><legend>Sender Company</legend>
<table width="100%" border="0" cellpadding="1" cellspacing="2">
  <tr>
    <th colspan="3" scope="col"><div align="left">
    	<?=printLogoOnReciept($custAgentID, $loggedUName)?>
    </th>
    <td scope="col"><p><font face="Times New Roman, Times, serif" size="2" ><strong>The bayba group</strong></font></p>
    <p><strong><font face="Times New Roman, Times, serif" size="2"> pick-up reciept </font></strong></p></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Date</font></td>
    <td><font size="2" face="#005b90"><? echo  $today ;?></font></td>
    <td><font size="2" face="#005b90">Transaction no </font></td>
    <td><font size="2" face="#005b90"><? echo  $refNumberIM ;?></font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">Bayba code </font></td>
    <td><font size="2" face="#005b90"><? echo $refNumber; ?></font></td>
  </tr>
  <tr>
  	 <? 
			$queryCust = "select *  from ".TBL_CUSTOMER." where customerID ='" . $customerID . "'";
			$customerContent = selectFrom($queryCust);
			
			$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $beneficiaryID . "'";
			$benificiaryContent = selectFrom($queryBen);
			
		?>
  	
    <td><font size="2" face="#005b90"><strong>Sender Detail </strong></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Full name </font></td>
    <td><font size="2" face="#005b90"><? echo $customerContent["firstName"] ." ".  $customerContent["lastName"];?></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>             
  <tr>
    <td><font size="2" face="#005b90"><strong>Beneficiary Detail </strong></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Beneficiary Name </font></td>
    <td><font size="2" face="#005b90"><? echo $benificiaryContent["firstName"]." ".  $benificiaryContent["lastName"];?></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Address</font></td>
    <td><font size="2" face="#005b90"><? echo $benificiaryContent["Address"]?></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Telephone No </font></td>
    <td><font size="2" face="#005b90"><? echo $benificiaryContent["Phone"]?></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90"><strong>Remittance Details</strong></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Destination Code </font></td>
    <td><font size="2" face="#005b90"><? echo $distributorName; ?></font></td>
     	
    <td><font size="2" face="#005b90">Destination</font></td>
    
      <? if($transactionType== 'Pick up'){
	              			$queryColl = "select *  from ".TBL_COLLECTION." where cp_id ='".$_SESSION["collectionPointID"]."'";
											$destinCollection = selectFrom($queryColl);
										
											?>
    
    <td><font size="2" face="#005b90"><? echo $destinCollection["cp_corresspondent_name"];?></font></td>
    <?  }elseif($transactionType== 'Bank Transfer'){ ?>
    
    <td><font size="2" face="#005b90"><? echo $distributorName;?></font></td>
    <? } ?>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Transfer Currency </font></td>
    <td><font size="2" face="#005b90"><? echo $currencyFrom; ?></font></td>
    <td><font size="2" face="#005b90">Amt Transfer </font></td>
    <td><font size="2" face="#005b90"><? echo number_format($transAmount,$roundLevel,'.',','); ?></font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Paid In </font></td>
    <td><font size="2" face="#005b90"><? echo $currencyTo ?></font></td>
    <td><font size="2" face="#005b90">Exchange Rate </font></td>
    <td><font size="2" face="#005b90"><? echo $exchangeRate; ?></font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">In <? echo $currencyTo ?> </font></td>
    <td><font size="2" face="#005b90"><? echo number_format($localAmount,$roundLevel,'.',','); ?></font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
  <tr>
  	<? if ($customerContent["IDType"] == "Other ID"){ 
  							
  							$IDNoType =  $benificiaryContent["otherId"];
  							$IDNo =  $benificiaryContent["otherId_name"];
  		 }else{ 
  		 	
  		 					$IDNoType =  $benificiaryContent["IDType"];
  		 					$IDNo =  $benificiaryContent["IDNumber"];
  		 				}
  		 	
  		 	?>
  	
    <td><font size="2" face="#005b90">ID Description </font></td>
    <td><font size="2" face="#005b90"><? echo $IDNoType; ?></font></td>
    <td><font size="2" face="#005b90">ID Serial No </font></td>
    <td><font size="2" face="#005b90"><? echo $IDNo; ?></font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Cashier Number </font></td>
    <td><font size="2" face="#005b90"><? echo $loggedUName; ?></font></td>
    <td><font size="2" face="#005b90"><em>Signature</em></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
</table>
</fieldset>

<!--span class="noDisplay"-->
 
<br>
<table width="100%" border="0" cellpadding="1" cellspacing="2">
  <tr>
  	<td>
--------------------------------------------------------------------------------------------------------------------------------------------------------------  		
  		
  	</td>
  </tr>	
</table>

<br>




<fieldset><legend>Sender Company</legend>
<table width="100%" border="0" cellpadding="1" cellspacing="2">
  <tr>
    <th colspan="3" scope="col"><div align="left">
    	<?=printLogoOnReciept($custAgentID, $loggedUName)?>
    	</th>
    <td scope="col"><p><font face="Times New Roman, Times, serif" size="2" ><strong>The bayba group</strong></font></p>
    <p><strong><font face="Times New Roman, Times, serif" size="2"> pick-up reciept </font></strong></p></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Date</font></td>
    <td><font size="2" face="#005b90"><? echo  $today ;?></font></td>
    <td><font size="2" face="#005b90">Transaction no </font></td>
    <td><font size="2" face="#005b90"><? echo  $refNumberIM ;?></font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">Bayba code </font></td>
    <td><font size="2" face="#005b90"><? echo $refNumber; ?></font></td>
  </tr>
  <tr>
  	 <? 
			$queryCust = "select *  from ".TBL_CUSTOMER." where customerID ='" . $customerID . "'";
			$customerContent = selectFrom($queryCust);
			
			$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $beneficiaryID . "'";
			$benificiaryContent = selectFrom($queryBen);
			
		?>
  	
    <td><font size="2" face="#005b90"><strong>Sender Detail </strong></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Full name </font></td>
    <td><font size="2" face="#005b90"><? echo $customerContent["firstName"] ." ".  $customerContent["lastName"];?></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>             
  <tr>
    <td><font size="2" face="#005b90"><strong>Beneficiary Detail </strong></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Beneficiary Name </font></td>
    <td><font size="2" face="#005b90"><? echo $benificiaryContent["firstName"]." ".  $benificiaryContent["lastName"];?></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Address</font></td>
    <td><font size="2" face="#005b90"><? echo $benificiaryContent["Address"]?></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Telephone No </font></td>
    <td><font size="2" face="#005b90"><? echo $benificiaryContent["Phone"]?></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90"><strong>Remittance Details</strong></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Destination Code </font></td>
    <td><font size="2" face="#005b90"><? echo $distributorName; ?></font></td>
     	
    <td><font size="2" face="#005b90">Destination</font></td>
    
      <? if($transactionType== 'Pick up'){
	              			$queryColl = "select *  from ".TBL_COLLECTION." where cp_id ='".$_SESSION["collectionPointID"]."'";
											$destinCollection = selectFrom($queryColl);
										
											?>
    
    <td><font size="2" face="#005b90"><? echo $destinCollection["cp_corresspondent_name"];?></font></td>
    <?  }elseif($transactionType== 'Bank Transfer'){ ?>
    
    <td><font size="2" face="#005b90"><? echo $distributorName;?></font></td>
    <? } ?>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Transfer Currency </font></td>
    <td><font size="2" face="#005b90"><? echo $currencyFrom; ?></font></td>
    <td><font size="2" face="#005b90">Amt Transfer </font></td>
    <td><font size="2" face="#005b90"><? echo number_format($transAmount,$roundLevel,'.',','); ?></font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Paid In </font></td>
    <td><font size="2" face="#005b90"><? echo $currencyTo ?></font></td>
    <td><font size="2" face="#005b90">Exchange Rate </font></td>
    <td><font size="2" face="#005b90"><? echo $exchangeRate; ?></font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">In <? echo $currencyTo ?> </font></td>
    <td><font size="2" face="#005b90"><? echo number_format($localAmount,$roundLevel,'.',','); ?></font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
  <tr>
  	<? if ($customerContent["IDType"] == "Other ID"){ 
  							
  							$IDNoType =  $benificiaryContent["otherId"];
  							$IDNo =  $benificiaryContent["otherId_name"];
  		 }else{ 
  		 	
  		 					$IDNoType =  $benificiaryContent["IDType"];
  		 					$IDNo =  $benificiaryContent["IDNumber"];
  		 				}
  		 	
  		 	?>
  	
    <td><font size="2" face="#005b90">ID Description </font></td>
    <td><font size="2" face="#005b90"><? echo $IDNoType; ?></font></td>
    <td><font size="2" face="#005b90">ID Serial No </font></td>
    <td><font size="2" face="#005b90"><? echo $IDNo; ?></font></td>
  </tr>
  <tr>
    <td><font size="2" face="#005b90">Cashier Number </font></td>
    <td><font size="2" face="#005b90"><? echo $loggedUName; ?></font></td>
    <td><font size="2" face="#005b90"><em>Signature</em></font></td>
    <td><font size="2" face="#005b90">&nbsp;</font></td>
  </tr>
</table>
</fieldset>
<!--/div-->
<!--/span-->

<form name="addTrans" action="add-transaction-conf.php?r=<?=$_GET["r"]?>" method="post">
        <table width="100%" border="0" cellspacing="1">
        	<tr><td align="center"> 	
		<div class='noPrint'>
			
           <input name="benAgentParentID" type="hidden" value="<? echo $benAgentParentID?>">
				  <input name="custAgentParentID" type="hidden" value="<? echo $custAgentParentID?>">
				  <input type="hidden" name="imReferenceNumber" value="<?=$refNumberIM;?>">
				  <input type="hidden" name="refNumber" value="<?=$_SESSION["refNumber"];?>">
				  <input type="hidden" name="mydDate" value="<?=$nowTime;?>">
					<input name="distribut" type="hidden" value="<? echo $_POST["distribut"];?>">	
					<input name="transID" type="hidden" value="<? echo $_POST["transID"];?>">					            
           
          	  <?
	  		foreach ($_POST as $k=>$v) 
			{
				$hiddenFields .= "<input name=\"" . $k . "\" type=\"hidden\" value=\"". $v ."\">\n";
				$str .= "$k\t\t:$v<br>";
			}
		
//echo $str;
echo $hiddenFields;//window.open('printing_reciept.php?transID=< echo $imReferenceNumber; >', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')

	  ?>
	  		
			<? if($success=='Y'){
				
				include "mailRecieptRichAlmond.php";
				$To = $customerContent["Email"];
				
			 $Subject = "New Transaction created";
			 $Name = $customerContent["firstName"];
		 $From = SUPPORT_EMAIL;
				sendMail($To,$Subject,$data,$From, $From);
				?>
	
			<input type="button" name="Submit2" value="Print this Receipt" onClick="print()">
			&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="style2">Go to Create Transaction</a>
			
		</div>
	</td>
		<? }else{ ?>
	<input type="Submit" name="confirmOrder" value="Confirm Order" id="once" onclick="javascript:document.getElementById('once').disabled=true;document.addTrans.submit();<? if(CONFIG_COMPLIANCE_PROMPT_CONFIRM== "1"){ ?> checkForm(addTrans,'yes')<? } ?>" >

<a href="<?=$returnPage?>?transID=<? echo $_POST["transID"]?>&from=conf" class="style2">Change Information</a>
</td>
		<? } ?>
		</tr>
			</table>
         </form>

</body>
</html>