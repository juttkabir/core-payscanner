<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$date_time = date('d-m-Y  h:i:s A');
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	$strMainCaption = "Create Cheque Order";
	$_strDefaultCurrency = "GBP";
	
	
	$arrCurrenciesData = selectMultiRecords("select currencyName, country from currencies order by currencyName");
	$searchByFlag = true;
	if(CONFIG_SEARCH_SENDER_BY_CODE_CQ=="1"){
		$searchByFlag = false;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Create Cheque Order</title>
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="jquery.cluetip.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script>
	$(document).ready(function(){

		$("#loading").ajaxStart(function(){
		   $(this).show();
		   $("#storeData").attr("disabled",true);
		});
		$("#loading").ajaxComplete(function(request, settings){
		   $(this).hide();
		   $("#storeData").attr("disabled",false);
		});
		
		$("#estimated_date").datePicker();
		$("#issue_date").datePicker({
			startDate: '01/01/2000'
		});
		
		//$("#date_in").click(function(){
			$("#date_in").dpSetStartDate('<?=date("d/m/Y")?>');
		//});
				
		$("#company_name").autocomplete('searchCompany.php', {
				mustMatch: true
			}
		);
		
		$("#company_name").result(function(event, data, formatted) {
			if (data)
			{
				//alert(data);
				$("#cmpdi").val(data[1]);
				$("#companyDetails").load("searchCompany.php?cmpdi="+$("#cmpdi").val()+"&getCompanyDetails=get",
					{}, function(){
						if($("#post_ex_dt"))
						{
							//alert($("#post_ex_dt").val());
							$('#estimated_date').val($("#post_ex_dt").val());
							$('#estimated_date').dpSetStartDate($("#post_ex_dt").val());
						}	
					});
				$("#companyDetails").show();
				$("#showHideCompanyDetailBtn").show();
			}
		});
		
		
		$("#showHideCompanyDetailBtn").click(function () {
		 
			if($("#showHideCompanyDetailBtn").html() == "[-]")
			{
				$("#companyDetails").hide();
				$("#showHideCompanyDetailBtn").html("[+]");
			}
			else
			{
				$("#companyDetails").show();
				$("#showHideCompanyDetailBtn").html("[-]");
			}
		});
		
		
		
		
		$("#customer_input").autocomplete('searchCustomer-ajax.php', {
				mustMatch: true,
				maxItemsToShow : -1,
			}
		);
		$("#customer_input").result(function(event, data, formatted) {
			if (data)
			{
				//alert(data);
				$("#cusdi").val(data[1]);
				$("#customerDetails").load("searchCustomer-ajax.php?cusdi="+$("#cusdi").val()+"&getCustomerDetails=get");
				$("#customerDetails").show();
				$("#showHideCustomerDetailBtn").show();
			}
		});
		
		$("#showHideCustomerDetailBtn").click(function () {
		 
			if($("#showHideCustomerDetailBtn").html() == "[-]")
			{
				$("#customerDetails").hide();
				$("#showHideCustomerDetailBtn").html("[+]");
			}
			else
			{
				$("#customerDetails").show();
				$("#showHideCustomerDetailBtn").html("[-]");
			}
		});
		
		$("#newCompanyBtn").click(function(){
			window.open ("/admin/manageCompany.php","AddCompany","location=1,scrollbars=1,width=850,height=450"); 
		});
		
		
		$("#newCustomerBtn").click(function(){
			window.open ("/admin/add-customer.php","AddCustomer","location=1,scrollbars=1,width=900,height=450"); 
		});
		
		$('img').cluetip({splitTitle:'|'});
		
		
		/* vaildations of the fields start */

		$("#order_form").validate({
			//debug: true,
			rules: {
				company_name: "required",
				customer_input: "required",
				cheque_no: {
					required: true,
					minlength: 6,
					number: true
				},
				bank_name: {
					required: true
				},
				account_no: {
					required: true
				},
				branch: {
					required: true
				},
				cheque_currency: {
					required: true
				},
				cheque_amount: {
					required: true,
					number: true,
					min: 1,
					max: function(){
						if($("#_li"))
							return $("#_li").val();
						else
							return true;
					}
				},
				cheque_fee: {
					required: true,
					number: true,
					min: 0
					
				},
				manual_rate_reason : {
					required: function(){
						return $("#fi").val() == "";
					}
				},
				issue_date: {
					required: true
				}
			},
			messages: {
				company_name: "<br />Please select any company by searching",
				customer_input: "<br />Please select the customer of this cheque order",
				cheque_no: {
					required: "&nbsp;Cheque number is required.",
					minlength: "&nbsp;At least 6 characters are necessary",
					number: "&nbsp;Only numbers are allowed"
				},
				bank_name: "&nbsp;Please enter bank name of the associated cheque.",
				account_no: "&nbsp;Account number of the cheque is required.",
				branch: "&nbsp;Please enter the bank branch from where cheque is issued.",
				cheque_currency: "<br />Select the currency of the cheque&nbsp;",
				cheque_amount: {
					required: "<br />Please enter value of the cheque",
					number: "<br />Please provide the valid amount.",
					min: "<br />Enter amount sould be greater than or equal to 1.",
					max: "<br />This amount is greater than your allowed limit!"
				},
				cheque_fee: {
					required: "<br />Please enter value of the fee",
					number: "<br />Please provide the valid fee amount.",
					min: "<br />Enter amount sould be greater than or equal to 0."
				},
				manual_rate_reason: {
					required: "<br />In case of manual fee, please provide the manual fee reason."
				},
				issue_date: {
					required: "<br />Please select the cheque issue data from data picker.&nbsp;"
				}
			},
			submitHandler: function(form) {
				
								
				$("#storeData").attr("disabled",true);
				
				if($("#fi").val() != "")
					feeDataFn();
				
				//alert("STOP");			
			
				$("#order_form").ajaxSubmit(function(data) { 
					//alert(data);
					if(data != "E")
					{
						alert("The cheque order has been created!");
						$("#order_form").clearForm();
						$("#date_in").val("<?=date("d/m/Y");?>");
						$("#cheque_fee").val("");
						$("#amount_paid").val("");
						$("#companyDetails").hide();
						$("#companyDetails").html("");
						$("#showHideCompanyDetailBtn").hide();
						$("#customerDetails").hide();
						$("#customerDetails").html("");
						$("#showHideCustomerDetailBtn").hide();
						$("#cheque_fee").val("");
						window.open ("/admin/chequeReceipt.php?oi="+data,"CustomerReceipt","location=0,scrollbars=1,width=300,height=500"); 
					}
					else
						alert("The record could not be saved due to error, Please try again."); 
				});
				
				$('#storeData').removeAttr("disabled");
		   }
		});
		
		//if($("#_li"))
		//{
			$.validator.addMethod("cheque_amount", 
				function(value, element, params) {
					alert("validation");
					/*var _at = parseFloat($("#_li").val());
					
					alert(_at);
					alert(value);
					if(_at >= parseFloat(value))
						return true;
					else */
						return false;
				}, 
				"Cheque Amount is greater than your allowed amount limit."
			 );
		//}


		/* Validation ends */
		
		
		$("#calculateFeeBtn").click(function(){
			if($("#company_name").valid())
				if($("#cheque_amount").valid())
					feeDataFn();
		});
		
		$("#manualFeeBtn").click(function(){
			$("#fi").val("");
			$("#cheque_fee").val("");
			$("#cheque_fee").attr("readonly",false);
			$("#manualRateReasonRow").show();
			$("#amount_paid").val("");
		});
		
		$("#cheque_amount").blur(function(){
			if($("#company_name").valid())
			   
				 feeDataFn();
			});
		
		$("#cheque_fee").blur(function(){
			calculateFinalAmount();			
		});
		
	});
	function roundNumber(rnum, rlength) { // Arguments: number to round, number of decimal places
	  <!-- by Aslam Shahid -->
	  var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
	  // Output the result to the form field (change for your purposes)
	  return newnumber; 
	}
	function calculateFinalAmount()
	{
		var roundDecimal = 4;
		<? 
			if(CONFIG_ROUND_NUMBER_ENABLED_CHEQ_CASH=="1"){
				if(defined("CONFIG_ROUND_NUMBER_TO_CHEQ_CASH") && is_numeric(CONFIG_ROUND_NUMBER_TO_CHEQ_CASH)){
		?>
			roundDecimal = <?=CONFIG_ROUND_NUMBER_TO_CHEQ_CASH?>;
		<?
				}
			}
		?>
		var fee = parseFloat($("#cheque_fee").val());
		var amount = parseFloat($("#cheque_amount").val());
		var amount_to_customer = parseFloat(amount - fee);

		$("#amount_paid").val(roundNumber(amount_to_customer,roundDecimal));
	}
	
	function feeDataFn()
	{
		$.getJSON("chequeOrderCalculation.php?fee=get&amount="+$("#cheque_amount").val()+"&company="+$("#cmpdi").val(), 
			function(json)
			{
				
				if(json.fee_id != "")
				{
					$("#fi").val(json.fee_id);
					
					$("#cheque_fee").val(json.falf);
					$("#cheque_fee").attr("readonly",true);
					$("#manualRateReasonRow").hide();
					calculateFinalAmount();
					
				}
				else
				{
					alert("There is error in fee setup, please review the cheque order fee module for more information.");
					$("#cheque_fee").val("");
				}
			}
		);
	}
	
	
	function isInLimit()
	{
		
	}
	
</script>
<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style>
td{ font-family:Verdana, Arial, Helvetica, sans-serif;
font-size:12px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.links {
	background-color:#FFFFFF; 
	font-size:10px; 
	text-decoration:none;
}
</style>
</head>
<body>
<div id="loading" style="display:none; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:12px; color: #FFFFFF; background-color:#FF0000; position:absolute; z-index:1; bottom:0; right:0; ">&nbsp;Loading....&nbsp;</div>
<form name="order_form" id="order_form" action="chequeOrderCalculation.php" method="post">
	<table width="80%" border="0" cellspacing="1" cellpadding="2" align="center">
		<tr>
			<td colspan="4" align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong>
			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="4" align="center" class="tdDefination" style="text-align:center">
				All fields mark with <font color="red">*</font> are compulsory Fields.
			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="4" align="right">
				<a href="javascript:void(0)" id="newCompanyBtn" class="links">[ Add New Company ]</a>
			</td>
		</tr>		

		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Search Company by Name:&nbsp;</b><font color="red">*</font></td>
			<td align="left" colspan="3">
				<input type="text" name="company_name" id="company_name" size="80" maxlength="150" />
				<input type="hidden" name="cmpdi" id="cmpdi" value="" />
				&nbsp;
				<a href="javascript:void(0)" id="showHideCompanyDetailBtn" style="display:none; text-decoration:none" title="Show Hide Company Details">[-]</a>
			</td>
		</tr>
		
		<tr bgcolor="#ededed">
			<td colspan="4" id="companyDetails" style="display:none">&nbsp;</td>
		</tr>
		
		
		<tr bgcolor="#ededed">
			<td colspan="4" align="right">
				<a href="javascript:void(0)" id="newCustomerBtn" class="links">[ Add New Customer ]</a>
			</td>
		</tr>		

		<tr bgcolor="#ededed">
		<? 
			$searchName ="";
			if($searchByFlag)
				$searchName = "by Names";
		?>
			<td width="25%" class="tdDefination"><b>Search Customer <?=$searchName?>:&nbsp;</b><font color="red">*</font></td>
			<td align="left" colspan="3">
				<input type="text" name="customer_input" id="customer_input" size="80" maxlength="150" />
				<input type="hidden" name="cusdi" id="cusdi" value="" />
				&nbsp;
				<a href="javascript:void(0)" id="showHideCustomerDetailBtn" style="display:none; text-decoration:none" title="Show Hide Customer Details">[-]</a>
			</td>
		</tr>
		
		<tr bgcolor="#ededed">
			<td colspan="4" id="customerDetails" style="display:none">&nbsp;</td>
		</td>
		
		
		<tr bgcolor="#ededed">
			<td width="100%" colspan="4" align="left">
				<fieldset>
					<legend>Cheque Details</legend>
					<table align="left" width="100%">
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Bank:&nbsp;</b><font color="red">*</font></td>
							<td align="left" width="75%"><input type="text" name="bank_name" id="bank_name" size="40" maxlength="150" value="" /></td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Cheque No:&nbsp;</b><font color="red">*</font></td>
							<td align="left" width="75%"><input type="text" name="cheque_no" id="cheque_no" size="40" maxlength="150" value="" /></td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Branch:&nbsp;</b><font color="red">*</font></td>
							<td align="left" width="75%"><input type="text" name="branch" id="branch" size="40" maxlength="150" value="" /></td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Account Number:&nbsp;</b><font color="red">*</font></td>
							<td align="left" width="75%"><input type="text" name="account_no" id="account_no" size="40" maxlength="150" value="" /></td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Cheque Issue Date:&nbsp;</b><font color="red">*</font></td>
							<td align="left" width="75%"><input type="text" name="issue_date" id="issue_date" size="15" maxlength="150" readonly="" /></td>
						</tr>
					</table>
				</fieldset><br />
				<fieldset>
					<legend>Amount Details</legend>
					<table align="left" width="100%">
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Cheque Currency:&nbsp;</b><font color="red">*</font></td>
							<td align="left" width="25%">
								<select name="cheque_currency" id="cheque_currency" style="width:175px">
									<option value="">Select Currency</option>
									<?
										foreach($arrCurrenciesData as $curKey => $curVal)
										{
											$strCurrencySelectedOption = '';
											if($_strDefaultCurrency == strtoupper($curVal["currencyName"]))
												$strCurrencySelectedOption = 'selected="selected"';
									?>
										<option value="<?=strtoupper($curVal["currencyName"])?>" <?=$strCurrencySelectedOption?>>
											<?=strtoupper($curVal["currencyName"])." of ".strtoupper($curVal["country"])?>
										</option>
									<? } ?>
								</select>
								&nbsp;
								<img src="images/info.gif" title="Note about Currency Selection | The currency selected here is assumed to be the only currency in this order, which covers all the amounts found and hence where ever the amount is defined without any currency, than the amount currency is assumned to be the missing currency." />
							</td>
							<td width="25%" class="tdDefination">&nbsp;</td>
							<td align="left" width="25%">&nbsp;</td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Cheque Amount:&nbsp;</b><font color="red">*</font></td>
							<td align="left" width="25%"><input type="text" name="cheque_amount" id="cheque_amount" size="30" maxlength="150" value="" /></td>
							<td width="25%" class="tdDefination"><b>Date In:&nbsp;</b></td>
							<td align="left" width="25%">
								<input type="text" name="date_in" id="date_in" size="10" value="<?=date("d/m/Y")?>" readonly="" />
							</td>
						</tr>
						<tr bgcolor="#ededed" valign="top">
							<td width="25%" class="tdDefination"><b>Cheque Fee:&nbsp;</b><font color="red">*</font></td>
							<td align="left" width="25%">
								<input type="text" name="cheque_fee" id="cheque_fee" size="10" maxlength="7" readonly="" />
								<input type="hidden" name="fi" id="fi" value="" />
								<br />
								<a href="javascript:void(0)" id="calculateFeeBtn" class="links">[Predefined Fee]</a><img src="images/info.gif" title="Note about Predefined Fee | This brings the calculation using the Fee of Company selected for Amount entered." /> |
								<a href="javascript:void(0)" id="manualFeeBtn" class="links">[Manual Fee]</a>
							</td>
							<td width="25%" class="tdDefination">&nbsp;</td>
							<td align="left" width="25%">&nbsp;</td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="25%" class="tdDefination"><b>Amount Paid to Customer:&nbsp;</b></td>
							<td align="left" width="25%"><input type="text" name="amount_paid" id="amount_paid" size="10" maxlength="12" readonly="" /></td>
							<td width="25%" class="tdDefination"><b>Estimated Date:&nbsp;</b></td>
							<td align="left" width="25%">
								<input type="text" name="estimated_date" id="estimated_date" size="10" readonly="" style="color:#339933; font-size:14px; font-weight:bold" />
							</td>
						</tr>
						<tr bgcolor="#ededed" id="manualRateReasonRow" style="display:none">
							<td width="100%" colspan="4" class="tdDefination" style="text-align:center">
								<br /><b>Reason for Manual Cheque Fee&nbsp;</b><font color="red">*</font><br />
								<textarea name="manual_rate_reason" id="manual_rate_reason" wrap="soft" cols="65" rows="3"></textarea>								
							</td>
						</tr>

					</table>
				</fieldset>
			</td>
		</tr>		

		
		<tr bgcolor="#ededed">
			<td colspan="4" align="center">
				<input type="submit" id="storeData" name="storeData" value="Create Order" />
				&nbsp;&nbsp;
				<input type="reset" value="Clear Fields" />
				<input type="hidden" name="co" value="a" />
				<? if(CONFIG_ADMIN_STAFF_AMOUNT_LIMIT_ALERT == "1" && !empty($_SESSION["loggedUserData"]["adminLimitCheque"])) { ?>
					<input type="hidden" name="_li" id="_li" value="<?=$_SESSION["loggedUserData"]["adminLimitCheque"]?>" />
				<? } ?>
				<input type="hidden" name="order_id" value="<?=$_REQUEST["order_id"]?>" />
			</td>
		</tr>
	</table>
</form>
</body>
</html>