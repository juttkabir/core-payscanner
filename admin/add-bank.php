<?
session_start();
include ("../include/config.php");
include ("security.php");

if($agentType == "TELLER")
{
	$parentID = $_SESSION["loggedUserData"]["cp_ida_id"];
	$userID = $_SESSION["loggedUserData"]["cp_ida_id"];
}
else
{
	$parentID = $_SESSION["loggedUserData"]["userID"];
	$userID = $_SESSION["loggedUserData"]["userID"];
}

$isCorrespondent = $_SESSION["loggedUserData"]["isCorrespondent"];
$adminType = $_SESSION["loggedUserData"]["adminType"];
$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];

/*
if($agentType == "SUPA" || $agentType == "SUPAI")
		$agentID = $parentID;
*/

/**
 * Initializing the session variables
 */
if(!isset($_GET["msg"])){
	
	$_SESSION["name1"] = "";
	$_SESSION["branchAddress1"] = "";
	$_SESSION["swiftCode1"] = "";
	$_SESSION["bankId1"] = "";
	$_SESSION["country1"] = "";
	$_SESSION["branchName1"] = "";
	$_SESSION["branchCode1"] = "";
	$_SESSION["district1"] = "";
	$_SESSION["state1"] = "";
	$moneyPaid = "";	
}
/** Money paid distinguishes whether banks are added as sending bank or beneficiary bank **/
if(isset($_GET["moneyPaid"])){
	$moneyPaid = 	$_GET["moneyPaid"];
}
if($_GET["bankID"] != "" && $_GET["msg"] == "")
{
		$benBanksDataQry = "select * from ".TBL_BANKS." where id=$bankID";
		$banBanksData = selectFrom($benBanksDataQry);

		$_SESSION["name1"] = $banBanksData["bankName"];
		$_SESSION["branchAddress1"] = $banBanksData["branchAddress"];
		$_SESSION["swiftCode1"] = $banBanksData["swiftCode"];
		$_SESSION["bankId1"] = $banBanksData["bankId"];
		$_SESSION["country1"] = $banBanksData["country"];
		$_SESSION["branchName1"] = $banBanksData["branchName"];
		$_SESSION["branchCode1"] = $banBanksData["branchCode"];
		$_SESSION["district1"] = $banBanksData["extra2"];
		$_SESSION["state1"] = $banBanksData["extra3"];		
			
}

/**Handling Labels**/

$label = $_SESSION["bankID"]!= "" ? "Update" : "Add";
if($moneyPaid == "By Bank Transfer"){
	$labelPageHeading = $label." UK Bank";
	$labelBankDetail = "Bank Details";
	$labelBankId = "Sort Code";
	$countryReadonly = "readonly = 'readonly'";
}else{
	$labelPageHeading = $label." Bank Info";
	$labelBankDetail = "Beneficiary Bank Details";
	$labelBankId = "Bank Id";
	$countryReadonly = "";
} 
/**End of label handling**/  
    	
?>
<html>
<head>
	<title>Bank Information</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
				
		if(document.getElementById("name").value == "")
		{
			alert("Please enter the bank name.");
			document.getElementById("name").focus();
			return false;
		}
		/*
		if(document.getElementById("branchCode").value == "")
		{
			alert("Please enter the branch code / name.");
			document.getElementById("branchCode").focus();
			return false;
		}
		*/
		if(document.getElementById("branchAddress"))
		{
			if(document.getElementById("branchAddress").value == "")
			{
				alert("Please enter the branch address.");
				document.getElementById("branchAddress").focus();
				return false;
			}
		}
		if(document.getElementById("country").selectedIndex == 0)
		{
			alert("Please select a country.");
			document.getElementById("country").focus();
			//return false;
		}
	<? if(CONFIG_ADD_BEN_BANK_ON_CREATE_TRANSACTION == "1"){ ?>
	 if(document.getElementById("branchName"))
		{
			if(document.getElementById("branchName").value == "")
			{
				alert("Please enter the branch name.");
				document.getElementById("branchName").focus();
				return false;
			}
		}
	   if(document.getElementById("state"))
		{
			if(document.getElementById("state").value == "")
			{
				alert("Please enter the state name.");
				document.getElementById("state").focus();
				return false;
			}
		}	
	
	<? } ?>	
		
	return true;
}
function IsAllSpaces(myStr)
{
	while (myStr.substring(0,1) == " "){
			myStr = myStr.substring(1, myStr.length);
	}
	if (myStr == ""){
			return true;
	}
	return false;
}

/**
 * If the window is open through pop up then close
 * other wise the close button will behave like reset button
 */
function exitWindow()
{
	if(window.opener)
		window.close();
}
-->
</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">
    	
    	<strong><font color="#000000" size="2"><?=$labelPageHeading?></font></strong>
    </td>
  </tr>
  <form name="frmBenificiary" action="add-bank-conf.php?transID=<? echo $_GET["transID"]?>&moneyPaid=<?=$moneyPaid?>&custCountry=<?=$_GET["custCountry"]?> page=<?=$_GET["page"]?>" method="post">
 		<input type="hidden" name="bankID" value="<? echo $_SESSION["bankID"];?>">
 		<input type="hidden" name="opener" value="<?=$_REQUEST["opener"]?>" />
				
		<tr bgcolor="#ededed">
		<?
		if(isset($_GET["msg"])){
		?>
			<td width="40" align="center" colspan="4">
				<font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font>
            	<? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?>
			</td>
		<?
		}else{
		?>	
			<td colspan="4" align="center">&nbsp;</td>
		<?
		}
		?>
        </tr>
		<tr bgcolor="#ededed">
          	<td colspan="4"><font color="#005b90"><b><?=$labelBankDetail; ?></b></font></td>
        </tr>
		<tr bgcolor="#ededed"> 
		  <td width="25%">
			  <font color="#005b90"><b><?=$labelBankId?></b></font>&nbsp;
		  </td>
		  <td width="25%">
			<input type="text" name="bankId" id="bankId" value="<?=$_SESSION["bankId1"]?>" maxlength="30">
		  </td>
		  <td width="25%" height="20" align="right">
			<font color="#005b90"><b>Bank Name<font color="red">*</font>&nbsp;</b></font>
		  </td>
		  <td width="25%">
			<input type="text" name="name" id="name" value="<?=$_SESSION["name1"]?>" maxlength="30">
		  </td>
		</tr>
		<? if(CONFIG_ADD_BEN_BANK_ON_CREATE_TRANSACTION == "1"){ ?>
			<tr bgcolor="#ededed"> 
		  <td width="25%">
			  <font color="#005b90"><b>Branch Name<font color="red">*</font>&nbsp;</b></font>
		  </td>
		  <td width="25%">
			<input type="text" name="branchName" id="branchName" value="<?=$_SESSION["branchName1"]?>" maxlength="30">
		  </td>
		  <td width="25%" height="20" align="right">
			<font color="#005b90"><b>Branch Address</b>
		  </td>
		  <td width="25%">
			<input type="text" name="branchAddress" id="branchAddress" value="<?=$_SESSION["branchAddress1"]?>" maxlength="30">
		  </td>
		</tr>
		<tr bgcolor="#ededed"> 
		  <td width="25%">
			  <font color="#005b90"><b>Branch Code</b></font>&nbsp;
		  </td>
		  <td width="25%">
			<input type="text" name="branchCode" id="branchCode" value="<?=$_SESSION["branchCode1"]?>" maxlength="30">
		  </td>
		  <td width="25%" height="20" align="right">
			<font color="#005b90"><b>District</b>
		  </td>
		  <td width="25%">
			<input type="text" name="district" id="district" value="<?=$_SESSION["district1"]?>" maxlength="30">
		  </td>
		</tr>
		<tr bgcolor="#ededed"> 
		  <td width="25%">
			  <font color="#005b90"><b>State<font color="red">*</font>&nbsp;</b></font>
		  </td>
		  <td width="25%">
			<input type="text" name="state" id="state" value="<?=$_SESSION["state1"]?>" maxlength="30">
		  </td>
		  <td width="25%">&nbsp;</td>
		  <td width="25%">&nbsp;</td>
		</tr>
		<? } ?>
		<tr bgcolor="#ededed"> 
            <td width="25%" align="left"><font color="#005b90"><b>Country</b></font><font color="red">*</font></td>
            <td width="25%">
	       	<?
				if($adminType == "Agent")
				{
					$queryCountry = "select countryName  from countries where countryType LIKE '%destination%'";
               	}
                else
                {		
                  	$queryCountry = "select countryName  from countries where countryType LIKE '%destination%'";
                }
                //$allCountries = explode(",",$queryCountry["countryName"]);
                $result = mysql_query($queryCountry);
                //$allCountries = $queryCountry["countryName"];
           	?>
				<select name="country" id="country">
					<option>Select Country</option>
					<?
						if($moneyPaid == "By Bank Transfer")
						{
							echo "<option value='United Kingdom'>United Kingdom</option>";
						}
						else
						{
							//foreach($allCountries as $allCountries => $val)
							//	echo "<option value='".$val."'>".$val."</option>";
							
							while($rs = mysql_fetch_array($result))
							{
								if(strtoupper($rs["countryName"]) == "BRAZIL")
								{
									echo "<option value='".$rs["countryName"]."' selected='selected'>".$rs["countryName"]."</option>";
								} else {
									echo "<option value='".$rs["countryName"]."'>".$rs["countryName"]."</option>";
								}
								
							}
						}
					?>
				</select>
            </td>
            <?
            if($moneyPaid == "By Bank Transfer"){
            ?>
				<td width="25%" align="right"><font color="#005b90"><b>Branch Name<font color="red">*</font></b></font></td>
				<td width="25%">
					<input type="text" name="branchAddress" id="branchAddress" value="<?=$_SESSION["branchAddress1"]?>" maxlength="30">	
				</td>
            <?
            }else{
            ?>
            	<td width="25%" align="left">&nbsp;</td>
            	<td width="25%">&nbsp;</td>
            <?
            }
            ?>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="100%" height="20" align="center" colspan="4">
				<input type="hidden" name="transID" value="<?=$_GET["transID"]?>" />
				<input name="Save" type="submit" value=" Save "  onClick="return checkForm(document.frmBenificiary);">
		        &nbsp;&nbsp; 
				<script>
					if(window.opener) 
					{
						document.write='<input name="reset" type="reset" value="Close" onClick="window.close();">';
					}
					else 
						document.write='<input name="reset" type="reset" value="Clear">';
				</script>
				            	
            </td>
          </tr>
         </table>
		</td>
       </tr>
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>
