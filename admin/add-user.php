<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

?>
<html>
<head>
	<title>Add Promotional Code</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
	if(theForm.name.value == "" || IsAllSpaces(theForm.name.value)){
    	alert("Please enter category name.");
        theForm.name.focus();
        return false;
    }
	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
	</script>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#FFFFFF" size="2">Users</font></strong></td>
  </tr>
  <form action="add-user-conf.php" method="post"onSubmit="return checkForm(this);">
  <tr>
    <td align="center"><? if ($_GET["msg"] != "") echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; ?>
		<table width="400" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="400" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Add 
                    User</strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center"><font color="#FF0000">* Compulsory 
              Fields</font></td>
		</tr>
        <tr bgcolor="#ededed">
            <td width="100"><font color="#005b90"><strong>Group*</strong></font></td>
            <td><SELECT name="group" style="font-family:verdana; font-size: 11px" onChange="jumpto()">
				<option value="">- Select One -</option>
				<?
					$SQL_Qry = "select * from ".TBL_USER_GROUPS;
					$groups = selectMultiRecords($SQL_Qry);
					for($i=0; $i<count($groups); $i++){
				?>
                   <OPTION value="<?=$groups[$i]["groupID"]; ?>" <? if ($_SESSION["group"] == $groups[$i]["groupID"]) echo "selected";?>><?=stripslashes($groups[$i]["Title"]); ?></OPTION>
				<?
					}
				?>
						  </SELECT></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="100" valign="top"><font color="#005b90"><strong>Login*</strong></font></td>
            <td><input type="text" name="username" value="<?=stripslashes($_SESSION["username"]); ?>" maxlength="15" size="35"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="100" valign="top"><font color="#005b90"><strong>Password*</strong></font></td>
            <td><input type="password" name="password" value="" maxlength="16" size="35"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="100" valign="top"><font color="#005b90"><strong>Confirm Password*</strong></font></td>
            <td><input type="password" name="cpassword" value="" maxlength="16" size="35"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="100" valign="top"><font color="#005b90"><strong>Name</strong></font></td>
            <td><input type="text" name="name" value="<?=stripslashes($_SESSION["name"]); ?>" maxlength="25" size="35"></td>
        </tr>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" value=" Save ">&nbsp;&nbsp; <input type="reset" value=" Clear ">
			</td>
		</tr>
		
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>
