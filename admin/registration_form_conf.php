<?php 
	/**
	 * @package sender Registration
	 * @Subpackage Sender Registration from Premier Exchange Website
	 * This page is used for Registering Sender From the Premier Exchange Web Site
	 * @author Mirza Arslan Baig
	 */
	session_start();
	include ("../include/config.php");
	set_time_limit(0);
	//ini_set("display_errors","on");
	$date_time = date('d-m-Y  h:i:s A');
	$db = dbConnect();
	//$_SESSION["logs"] = "";
	
	if(!empty($_REQUEST["plog"]))
	{
		$_SESSION["logs"] .="P,";
		
		//die("awais");
	}
	else if(!empty($_REQUEST["slog"]))
	{
		$_SESSION["logs"] ="S,";
		
		//die("awais");
	}

	/*=======getting parameters ======== */


	//$_SESSION["logs"] = $logs;
	if($_REQUEST["chk"] == '1')
	{
		//echo $_SESSION['customerID'];
		$custId=$_SESSION['customerID'];
	$updatelogs = "update registrationFromRequest set logs = '".$_SESSION['logs']."' where session_id = '".$custId."' ";
	//echo $updatelogs;
	update($updatelogs);
	die();
	}
	
	/** File Upload **/
	
	if(isset($_FILES['passportDoc'])){
		//$strQueryBrowserName = "UPDATE ".TBL_CUSTOMER." SET `browserName` = '$strBrowserName' WHERE customerID = '".$_SESSION['customerID']."'";
		
		//echo "Cus size:".($_FILES['passportDoc']['size']/1024);
		$fileSize = $_FILES['passportDoc']['size'];
		if($fileSize < 2097152 && $_FILES['passportDoc']['error'] == 0){
			//$strNewFileName = mysql_insert_id();
			$strFileName = $_FILES['passportDoc']['name'];
			$arrOldName = explode(".", $strFileName);
			//$strNewFileName = $_SESSION['customerID'].'.'.$arrOldName[count($arrOldName)-1];
			$ext = $arrOldName[count($arrOldName)-1]; //under process 
			//$strNewFileName = str_pad((int)$_SESSION['customerID'],5,"0",STR_PAD_LEFT).'_15_passport';
			$passportTypeQuery = "SELECT doc_cat.documentId FROM ".TBL_CATEGORY." AS cat INNER JOIN document_category AS doc_cat ON cat.id = doc_cat.categoryId WHERE cat.description = 'Passport'";
			//$resultPassport = mysql_query($passportTypeQuery, $db) or die(mysql_error());
			$passportTypeID = selectFrom($passportTypeQuery);
			//$passportTypeID = mysql_fetch_array($resultPassport);
			
			
			$strNewFileName = '00000'.$_SESSION['customerID'].'_15_passport.'.$ext;
			$strPath = '../files/customer/'.$strNewFileName;
			$strTempAddress = $_FILES['passportDoc']['tmp_name'];
			if(move_uploaded_file($strTempAddress, $strPath)){
				$strQueryDocument = "INSERT INTO ".TBL_DOCUMENT_UPLOAD." (documentid, userid, path, status) VALUES('".$passportTypeID['documentId']."', '".$_SESSION['customerID']."', '$strPath', 'Enable')";
				//mysql_query($strQueryDocument, $db) or die(mysql_error());
				insertInto($strQueryDocument);
				$custId=$_SESSION['customerID'];
				$_SESSION['logs'] .= "U,";
				$updatelogs = "update registrationFromRequest set logs = '".$_SESSION['logs']."' where session_id = '".$custId."' ";
				//echo $updatelogs;
				update($updatelogs);
				die("Document successfully uploaded!!");
			}else{
				die("Your document is not uploaded. Please email your document to us at : info@premfx.com");
			}
		}else{
			die('The file is too large.Maximum upload file size is 2MB!!');
		}
	}
	
	/**** Email address availability  ****/
	
	 if($_POST['rtype'] != 'updateRegisterType' && $_POST['rtype'] != 'addOtherCurrency')
	{
	
	if(!empty($_POST['email']) && isset($_POST['chkEmailID'])){
		$chkEmail = $_POST['email'];
		$prepaidCurrency ='';
		
		$query = "SELECT c.customerID,c.email,c.online_customer_services   FROM ".TBL_CUSTOMER." as c WHERE c.email = '$chkEmail' ";
		
		$myrecord  = mysql_fetch_assoc(mysql_query($query));
 
		
		if(!empty($myrecord['customerID'])){
		$selectCurrency = "select  pc.card_issue_currency from prepaidCustData as pc WHERE pc.custID = ".$myrecord['customerID'] ;
		   
		$records = selectMultiRecords($selectCurrency);
		
	//	debug($selectCurrency);
  //debug($records );
  
		//$prepaidCurrency = implode(',',$records[0]['card_issue_currency']);
		}
	 
		 for($p=0;$p<count($records);$p++){
			$prepaidCurrency  .= $records[$p]['card_issue_currency'].',';
			}
		$prepaidCurrency = rtrim($prepaidCurrency,','); 
		
		if(!empty($myrecord))
		$intFlagEmail = count($myrecord);
		else
		$intFlagEmail = 0;
		
		if($_POST['chkEmailID'] == '1'){
			if($intFlagEmail > 0 )
				echo '<em style="color:#FF4B4B;font-family:arial;font-size:11px;font-weight:bold">'.$chkEmail.' is already registered.</em>'.'|'.$myrecord['online_customer_services'].'|'.$prepaidCurrency;
		//else
			//echo '<em style="color:#4B9D05">Available!</em>';
		
			exit;
		}
	}
	
	
	/**** End email address availability ***/
	
	/**** Passport availability  ****/
	if(!empty($_POST['passportNum']) && isset($_POST['chkPassport'])){
		$chkPassport = strtoupper($_POST['passportNum']);
		$queryPassportAvail = "SELECT id_number FROM ".TBL_MULTIPLE_USER_ID_TYPES." WHERE id_number = '$chkPassport' AND user_type = 'C'";
		$records = selectMultiRecords($queryPassportAvail);
		 
		$intFlagPassport = count($records);
		if($_POST['chkPassport'] == '1'){
			if($intFlagPassport > 0)
				echo '<em style="color:#FF4B4B;font-family:arial;font-size:11px;font-weight:bold">This passport number is already registered 114.</em>';
				
			exit;
		}
		
	}
	
	/**** End passpoer availibility ****/

	/*** ID card availabilty check ***/
	 
	if(!empty($_POST['idCardNumber']) && isset($_POST['chkIDCard'])){
		$chkIDCard = $_POST['idCardNumber'];
		$queryIDAvailability = "SELECT id_number FROM ".TBL_MULTIPLE_USER_ID_TYPES." WHERE id_number = '$chkIDCard' AND user_type = 'C'";
		$records = selectMultiRecords($queryIDAvailability);
		$intFlagIDCard = count($records);
		if($_POST['chkIDCard'] == '1'){
			if($intFlagIDCard > 0)
				echo '<em style="color:#FF4B4B;font-family:arial;font-size:11px;font-weight:bold">This ID card number is already registered.</em>';
			
			exit;
		}
	}
	
	/*** End id card availabilty check ***/
	
	/* if($_POST['register'] != 'Register'){
		echo '<em style="color:#FF4B4B;font-size:13px;">Sender can not be registered. Please try again.</em>';
		exit;
	} */
	
	
	if(!empty($_POST['email']) || (!empty($_POST['passportNumber']) || (!empty($_POST['line1']) && !empty($_POST['line2']) && !empty($_POST['line3'])))){
		
		$chkEmail = $_POST['email'];
		$passport = $_POST['passportNumber'];
		$NIC = $_POST['line1'].$_POST['line2'].trim($_POST['line3']);
		
		$chkEmail = $_POST['email'];
		$query = "SELECT customerID,email FROM ".TBL_CUSTOMER." WHERE email = '$chkEmail'";
		$records = selectMultiRecords($query);
		
		$intFlagEmail = count($records);
		if($intFlagEmail > 0){
				
		$checkServices = mysql_fetch_assoc(mysql_query("SELECT online_customer_services  as services FROM ".TBL_CUSTOMER." WHERE customerID = ".$records[0]["customerID"]));
		echo $checkServices["services"];
			/* echo '<em style="color:#FF4B4B;font-size:13px;">This email address is already registered.</em>'; */
			exit;
		}
		
		
		if(!empty($passport)){
			$passportValidateQuery = "SELECT count(*) AS numberOfRecords FROM ".TBL_MULTIPLE_USER_ID_TYPES." AS userIDType WHERE userIDType.user_type = 'C' AND userIDType.id_number = '$passport'";
			//$records = mysql_fetch_array($passportValidation);
 
			$records = selectFrom($passportValidateQuery);
			if($records['numberOfRecords'] > 0)
				die('<em style="color:#FF4B4B;font-size:13px;">This passport number is already registered 173!!</em>');
		}elseif(!empty($NIC)){			
			$idCardValidationQuery = "SELECT count(*) AS numberOfRecords FROM ".TBL_MULTIPLE_USER_ID_TYPES." AS userIDType WHERE userIDType.user_type = 'C' AND userIDType.id_number = '$NIC'";
			//$records = mysql_fetch_array($IDCardValidation);
			$records = selectFrom($idCardValidationQuery);
			if($records['numberOfRecords'] > 0)
				die('<em style="color:#FF4B4B;font-size:13px;">This ID card number is already registered!!</em>');
		}
		}
	
	//debug($_POST, true);
	// Server side validation for passport number
	if(empty($_POST['passportNumber']) && (empty($_POST['line1']) || empty($_POST['line2']) || empty($_POST['line3'])))
		die("<h5 style='color:#FF4B4B;font-style:italic;'>Mandatory information is missing, please provide mandatory fields</h5>");
	if(!isset($_POST['passportNumber']))
		die("<h5 style='color:#FF4B4B;font-style:italic;'>Mandatory information is missing, please provide mandatory fields</h5>");
	}	
	/*** Making Browser Name**/
	if(preg_match('/IE/', $_SERVER['HTTP_USER_AGENT']))
		$strBrowserName = "Internet Explorer";
	elseif(preg_match('/Firefox/', $_SERVER['HTTP_USER_AGENT']))
		$strBrowserName = "Mozila Firefox";
	elseif(preg_match('/Chrome/', $_SERVER['HTTP_USER_AGENT']))
		$strBrowserName = "Google Chrome";
	elseif(preg_match('/Opera/', $_SERVER['HTTP_USER_AGENT']))
		$strBrowserName = "Opera";
	/******** Data From Form ********/
	
	//debug($strQueryRequestDebug);
	$mailto = $_POST['email'];
	if(!empty($mailto))
		$custEmail = $mailto;
	
	$ipAddress = $_POST['ipAddress'];
	$title = $_POST['title'];	
	$forename = $_POST['forename'];
	$surname = $_POST['surname'];
	$password = $_POST['password'];
	//$fullName = $forename.' '.$surname;
	$fullName = $forename;
	
	$dobDay = $_POST['dobDay'];
	$dobMonth = $_POST['dobMonth'];
	$dobYear = $_POST['dobYear'];
	
	$gender = $_POST['gender'];
	$birCountry = $_POST['birthCountry'];
	$resCountry = $_POST['residenceCountry'];
	$Countryaddress = $_POST['Countryaddress'];
	$postCode = $_POST['postcode'];
	$buildingNumber = $_POST['buildingNumber'];
	$buildingName = $_POST['buildingName'];
	$street = $_POST['street'];
	$town = $_POST['town'];
	$province = $_POST['province'];
	$landline = $_POST['landline'];
	$initial_load = '';
	$card_issue_currency = '';
	if(isset($_POST['initial_load']) && !empty($_POST['initial_load']) && trim($_POST['initial_load']) != ''){
		$initial_load = trim($_POST['initial_load']);
	}
	if(isset($_POST['card_issue_currency']) && !empty($_POST['card_issue_currency']) && trim($_POST['card_issue_currency']) != ''){
		$card_issue_currency = $_POST['card_issue_currency'];
	}
	if(!empty($_POST['mobile']))
		$mobile = $_POST['mobile'];
	
	// Passport
	if(!empty($_POST['passportNumber'])){
		$passportNumber = $_POST['passportNumber'];
		$passportNumber = strtoupper($passportNumber);
		
		$arrPassportNum = explode("-", $passportNumber);
		$passportNumber1 = $arrPassportNum[0];
		$passportNumber2 = $arrPassportNum[1];
		$passportNumber3 = strtoupper($arrPassportNum[2]);
		$passportNumber4 = $arrPassportNum[3];
		$passportNumber5 = $arrPassportNum[4];
		$passportNumber6 = $arrPassportNum[5];
		$passportNumber7 = $arrPassportNum[6];
		//$passportNumber7 = "<<<<<<<<<<<<<<";
		$passportNumber8 = $arrPassportNum[7];
		$passportNumber9 = $arrPassportNum[8];
		
		//debug($arrPassportNum, true);
		$passportExpiry = $_POST['passportExpiry'];
		$arrpassportExpiry = explode("/", $passportExpiry);
		$passportExpiryDay = $arrpassportExpiry[0];
		$passportExpiryMonth = $arrpassportExpiry[1];
		$passportExpiryYear = $arrpassportExpiry[2];
		
		$passportIssueDate = $_POST['passportIssue'];
		$arrPassportIssueDate = explode("/", $passportIssueDate);
		$passportIssueDay = $arrPassportIssueDate[0];
		$passportIssueMonth = $arrPassportIssueDate[1];
		$passportIssueYear = $arrPassportIssueDate[2];
		
		$passportCountry = $_POST['passportCountry'];
	}
	
	// Identity Card
	$idLine1 = $_POST['line1'];
	$arrLine1 = explode("-", $idLine1);
	$strLine1Part1 = $arrLine1[0];
	$strLine1Part2 = $arrLine1[1];
	$strLine1Part3a = $arrLine1[2];
	$strLine1Part3b = $arrLine1[3];
	$strLine1Part4 = $arrLine1[4];
	
	$idLine2 = $_POST['line2'];
	$arrIdLine2 = explode("-", $idLine2);
	$strLine2Part1a = $arrIdLine2[0];
	$strLine2Part1b = $arrIdLine2[1];
	$strLine2Part2  = $arrIdLine2[2];
	$strLine2Part3a = $arrIdLine2[3];
	$strLine2Part3b = $arrIdLine2[4];
	$strLine2Part4 = $arrIdLine2[5];
	$strLine2Part5 = $arrIdLine2[6];
	$strLine2Part6 = $arrIdLine2[7];
		

	$idLine3 = trim($_POST['line3']);
	if(!empty($idLine1) && !empty($idLine2) && !empty($idLine3))
		$IDCardNumber = $idLine1 . $idLine2 . $idLine3;
	else
		$IDCardNumber = '';
		
	$IDCardNumber = strtoupper($IDCardNumber);
	
	$idExpiryDate = $_POST['idExpiry'];
	$arrIdExpiryDate = explode("/", $idExpiryDate);
	$idExpiryDay = $arrIdExpiryDate[0];
	$idExpiryMonth = $arrIdExpiryDate[1];
	$idExpiryYear = $arrIdExpiryDate[2];
	
	$nationalityCountry = $_POST['nationalityCountry'];
	$idIssueCountry = $_POST['issueCountry'];
	
	
	
	$email = $_POST['email'];
	$userRemarks = $_POST['heardAboutUs'];
	
	if(!empty($buildingNumber))
		$address = $buildingNumber;
	if(!empty($buildingName))
		$address .= ', '.$buildingName;
	if(!empty($street))
		$address .= ', '. $street;
	if(!empty($town))
		$address .= ', '. $town;
	if(!empty($province))
		$address .= ', '.$province;
	if(!empty($resCountry))
		$address .= ', '.$resCountry;
	
	
	
	
	
	/** API Proccess **/
	
	$objParam = new stdClass();
	
	// Test Credentials
	
	// $objParam ->AccountName		= "integration@premfx.com";
	// $objParam ->Password		= "Gw3748563Kb#";
	// $objParam ->ProfileId			= "2d7b8dc3-bd08-4b5b-8364-5fd5a8c0d16a";

	
	// Live Credentials 
	
	$objParam ->AccountName		= "integration@premfx.com";
	$objParam ->Password		= "238768Fs6hwq8#";
	$objParam ->ProfileId			= "e3fddd57-780a-4185-a406-250c5f4ceea3";
	
	// $objParam ->AccountName		= "admin@premfx.com";
	// $objParam ->Password		= "London88!@1";
	// $objParam ->ProfileId			= "e3fddd57-780a-4185-a406-250c5f4ceea3";
	



	
	//require_once "../api/gbgroup/autheticatebyprofile.php";
	//require_once "../api/gbgroup/get_itemchecksbyprofile.php";
	
	//$objParam ->CustomerRef 		= "your own reference";
	
	
	
	
	$objParam ->IPAddress 			= $ipAddress;

	//Personal Details
	$objParam ->UserData = new stdClass();
	$objParam ->UserData->Basic->Title 				= $title;			// required 
	$objParam ->UserData->Basic->Forename 			= $forename;		// required
	$objParam ->UserData->Basic->Surname 			= $surname;			// required
	$objParam ->UserData->Basic->MiddleName			= "";				// 
	$objParam ->UserData->Basic->Gender 			= ucfirst($gender);			// optional / required for 

	$objParam ->UserData->Basic->DOBDay 			= $dobDay;		// optional / required for driving licence check
	$objParam ->UserData->Basic->DOBMonth 			= $dobMonth;		// optional / required for driving licence check
	$objParam ->UserData->Basic->DOBYear 			= $dobYear;		// optional / required for driving licence check


	//Address #1
	$objParam ->UserData->UKData = new stdClass();
	$objParam ->UserData->UKData->Address1 = new stdClass();
	$objParam ->UserData->UKData->Address1->FixedFormat = new stdClass();
	$objParam ->UserData->UKData->Address1->FixedFormat->Postcode 		= $postCode;// required
	$objParam ->UserData->UKData->Address1->FixedFormat->BuildingName 	= "";		// required / optional if BuildingNo supplied
	$objParam ->UserData->UKData->Address1->FixedFormat->BuildingNo 	= $buildingNumber;				// required / optional if BuildingName supplied
	$objParam ->UserData->UKData->Address1->FixedFormat->SubBuilding 	= "";		// optional
	$objParam ->UserData->UKData->Address1->FixedFormat->Organisation 	= "";		// optional
	$objParam ->UserData->UKData->Address1->FixedFormat->Street 		= "";	// required
	$objParam ->UserData->UKData->Address1->FixedFormat->SubStreet 		= "";		// optional
	$objParam ->UserData->UKData->Address1->FixedFormat->Town 			= "";		// optional
	$objParam ->UserData->UKData->Address1->FixedFormat->District 		= "";		// optional

	/*
	// UK Passport
	$objParam ->UserData->UKData->Passport = new stdClass();
	$objParam ->UserData->UKData->Passport->Number1 = "123456789";
	$objParam ->UserData->UKData->Passport->Number2 = "GBR";
	$objParam ->UserData->UKData->Passport->Number3 = "19890701";
	$objParam ->UserData->UKData->Passport->Number4 = "M";
	$objParam ->UserData->UKData->Passport->Number5 = "0810050";
	$objParam ->UserData->UKData->Passport->Number6 = "12";
	//$arrExpiryDate = explode($_POST['passportExpiry'],"-");
	$expiry = "2013-07-01";
	$arrExpiryDate = explode("-",$expiry);
	$objParam ->UserData->UKData->Passport->ExpiryDay = $arrExpiryDate[2];
	$objParam ->UserData->UKData->Passport->ExpiryMonth = $arrExpiryDate[1];
	$objParam ->UserData->UKData->Passport->ExpiryYear = $arrExpiryDate[0];

	*/

	//Drivers Licence
	// $objParam ->UserData->UKData->Driver = new stdClass();
	// $objParam ->UserData->UKData->Driver->Number1						= "MARR9";			//	5 digits
	// $objParam ->UserData->UKData->Driver->Number2						= "608232";			//	6 digits
	// $objParam ->UserData->UKData->Driver->Number3						= "HB9";			//	3 digits
	// $objParam ->UserData->UKData->Driver->Number4						= "00";				//	2 digits
	// $objParam ->UserData->UKData->Driver->Postcode						= "S61 2NL";		//	postcode capture from Address#1
	//$objParam ->UserData->UKData->Driver->Microfiche					= "";				//	14 digits


	//Landline Telephone
	// $objParam ->UserData->UKData->Telephone = new stdClass();
	// $objParam ->UserData->UKData->Telephone->ExDirectory 				= "TRUE";			// Set to TRUE if no number is supplied
	// $objParam ->UserData->UKData->Telephone->Number 					= "";				// optional can be blank if ExDirectory Flag set to True


	//InternationalPassport
	$objParam ->UserData->InternationalPassport = new stdClass();
	$objParam ->UserData->InternationalPassport->Number1			= $passportNumber1;	// 9	digits
	$objParam ->UserData->InternationalPassport->Number2			= $passportNumber2;	// 1	digits
	$objParam ->UserData->InternationalPassport->Number3			= $passportNumber3;	// 3	digits
	$objParam ->UserData->InternationalPassport->Number4			= $passportNumber4;	// 7	digits
	$objParam ->UserData->InternationalPassport->Number5			= $passportNumber5;	// 1	digits
	$objParam ->UserData->InternationalPassport->Number6			= $passportNumber6;	// 7	digits
	$objParam ->UserData->InternationalPassport->Number7			= $passportNumber7;	// 14	digits
	$objParam ->UserData->InternationalPassport->Number8			= $passportNumber8;	// 1	digits
	$objParam ->UserData->InternationalPassport->Number9			= $passportNumber9;	// 1	digits
	$objParam ->UserData->InternationalPassport->ExpiryDay			= $passportExpiryDay; // 2	digits - optional but recommended for enhanced matching
	$objParam ->UserData->InternationalPassport->ExpiryMonth		= $passportExpiryMonth;	// 2	digits - optional but recommended for enhanced matching
	$objParam ->UserData->InternationalPassport->ExpiryYear			= $passportExpiryYear;	// 4	digits - optional but recommended for enhanced matching
	$objParam ->UserData->InternationalPassport->CountryOfOrigin	= $passportCountry;	// GetPassportCountries  method - optional but recommended for enhanced matching 


	// Identity Card 
	$objParam ->UserData->IdentityCard = new stdClass();
	$objParam ->UserData->IdentityCard->Line1->Part1DocType			= $strLine1Part1; // 2	digits / chars
	$objParam ->UserData->IdentityCard->Line1->Part2IssuingState	= $strLine1Part2; // 3	digits / chars
	$objParam ->UserData->IdentityCard->Line1->Part3aDocumentNumber	= $strLine1Part3a; // 9	digits / chars
	$objParam ->UserData->IdentityCard->Line1->Part3bCheckDigit		= $strLine1Part3b; // 1	digits / chars
	$objParam ->UserData->IdentityCard->Line1->Part4Optional		= $strLine1Part4; // 15	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part1aDateOfBirth	= $strLine2Part1a;	// 6	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part1bCheckDigit		= $strLine2Part1b; 	// 1	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part2Gender			= $strLine2Part2; 	// 1	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part3aDateOfExpiry	= $strLine2Part3a;	// 6	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part3bCheckDigit		= $strLine2Part3b;		// 1	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part4Nationality		= $strLine2Part4;										// 3	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part5Optional			= $strLine2Part5;								// 11	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part6CheckDigit			= $strLine2Part6;											// 1	digits / chars
	$objParam ->UserData->IdentityCard->Line3							= $idLine3;	// 30	digits / chars
	$objParam ->UserData->IdentityCard->ExpiryDay						= $idExpiryDay;											// 2	digits / chars
	$objParam ->UserData->IdentityCard->ExpiryMonth						= $idExpiryMonth;											// 2	digits / chars
	$objParam ->UserData->IdentityCard->ExpiryYear						= $idExpiryYear;										// 4	digits / chars
	$objParam ->UserData->IdentityCard->CountryOfNationality			= $nationalityCountry;				// use GetIdentityCardNationalityCountries method
	$objParam ->UserData->IdentityCard->CountryOfIssue					= $issueCountry;								// use GetIdentityCardIssuingCountries method
	
	
	//debug($objParam);
	//LIVE PLATFORM URL	
$soap = new SoapClient("https://www.prove-uru.co.uk:8443/URUWS/URU11a.asmx?wsdl", array('compression' => SOAP_COMPRESSION_ACCEPT, 'trace' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
	$soap = new SoapClient("https://www.prove-uru.co.uk:8443/URUWS/URU11a.asmx?wsdl", array( 'trace' => true));
	
	/*echo "<pre>";
	print_r($objParam);
	echo "</pre>";
	*/
	//PILOT PLATFORM URL
//$soap = new SoapClient("https://pilot.prove-uru.co.uk:8443/URUWS/URU11a.asmx?wsdl", array('compression' => SOAP_COMPRESSION_ACCEPT, 'trace' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));

//$soap = new SoapClient("https://pilot.prove-uru.co.uk:8443/URUCAdminWS/Admin11a.asmx?wsdl", array('compression' => SOAP_COMPRESSION_ACCEPT, 'trace' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));




	if (is_soap_fault($soap)) 
	{
	throw new Exception(" {$soap->faultcode}: {$soap->faultstring} ");
	}
	try {
	$objRet = $soap->AuthenticateByProfile($objParam);
	
	// echo '<pre>';
	// print"Authentication ID : ".($objRet->AuthenticateByProfileResult->AuthenticationId)."<br>";
	// print"Score             : ".($objRet->AuthenticateByProfileResult->Score)."<br>";
	// print"Band Text         : ".($objRet->AuthenticateByProfileResult->BandText)."<br>";
	// echo '</pre>';
	
	 }
	 catch (Exception $e) {
		// //	exit('PHPcode check failed');
		// die("Visit this url for registration: http://clients.premfx.com/private_registration.php");
		// //die("<h5 style='color:#FF4B4B;font-style:italic;'>".$e->getMessage()."</h5>");
		
}
	
	$authenticationID = $objRet->AuthenticateByProfileResult->AuthenticationId;
	$score = $objRet->AuthenticateByProfileResult->Score;
	$bandText = $objRet->AuthenticateByProfileResult->BandText;
	$timeStamp = $objRet->AuthenticateByProfileResult->Timestamp;
	
	
	// $objRet->AuthenticateByProfileResult->ResultCodes->ItemCheck[2] // National Identity Card Number
	// $objRet->AuthenticateByProfileResult->ResultCodes->ItemCheck[3] // Passport International
	
	//debug($objRet->AuthenticateByProfileResult->ResultCodes->ItemCheck[2]);
	//debug($objRet->AuthenticateByProfileResult->ResultCodes->ItemCheck[3]);
	
	
	/***** Account Number Generator ****/

	$customerNumber = selectFrom("select MAX(customerNumber) from ".TBL_CUSTOMER);
	$customerNumber_value = $customerNumber[0]+1;
	//debug($customerNumber_value);
	if(defined("CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX"))
		$strCustomerPrefix = CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX;
	else
		$strCustomerPrefix = "CW-";
	
	$accountName = $strCustomerPrefix.$customerNumber_value;
	
	
	
	
	$payinBooknumber = selectFrom("select MAX(customerNumber) from ".TBL_CUSTOMER);
	$payinBooknumber_value = $payinBooknumber[0]+1;
	if(defined("CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX"))
		$strCustomerPrefix = CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX;
	else
		$strCustomerPrefix = "CW-";
	
	$payinBookname = $strCustomerPrefix.$payinBooknumber_value; 
	
	
	
	
	//debug($accountName);
	
	/***************  START PDF ATTACHMENT DOCUMENT GENERATION ***************/
	/*
	include ('../pdfClass/class.ezpdf.php');
	$pdf =& new Cezpdf();
	$pdf->selectFont('../pdfClass/fonts/Helvetica.afm');
	
	
	// image can be created only if GD library is installed in server
	$image = imagecreatefromjpeg("images/premier/logo.jpg");
	$pdf->addImage($image,50,700,200);
	
	$pdf->ezSetMargins(0,0,80,80);
	for($i=0;$i<=8;$i++)
		$pdf->ezText('',12);

	$pdf->ezText('',12);
	$pdf->ezText('<b><u>Registration</u></b>',12,array("justification" => "center"));
	$pdf->ezText('',12);
	$pdf->ezText('Registration Details are given Below:',12,array("justification" => 'full'));	
	$pdf->ezText('',12);
	
	$pdf->ezText('',12);
	$pdf->ezText('',12);
	
	
	$data = array(
		array(
			'Title',
			"$title"
		),
		array(
			'Forename',
			"$forename"
		),
		array(
			'Surname',
			"$surname"
		),
		array(
			'Reference Number',
			"$accountName"
		),
		array(
			'Date of Birth',
			"$dob"
		),
		array(
			'Gender',
			"$gender"
		),
		array(
			'Country of Birth',
			"$birCountry"
		),
		array(
			'Country of Residence',
			"$resCountry"
		),
		array(
			'Postcode / Zipcode',
			"$postCode"
		),
		array(
			'Your Address',
			"$address"
		),
		array(
			'Telephone - Landline',
			"$landline"
		),
		array(
			'Telephone - Mobile',
			"$mobile"
		),
		array(
			'Passport',
			"$passportNumber"
		),
		array(
			'Passport Expiry Date',
			"$passportExpiry"
		),
		array(
			'Originating Country',
			"$passportCountry"
		),
		array(
			'National ID Card Line1',
			"$idLine1"
		),
		array(
			'National ID Card Line2',
			"$idLine2"
		),
		array(
			'National ID Card Line3',
			"$idLine3"
		),
		array(
			'Identity Card Expiry Date',
			"$idExpiryDate"
		),
		array(
			'Country of Nationality',
			"$nationalityCountry"
		),
		array(
			'Country of Issue Identity Card',
			"$idIssueCountry"
		),
		array(
			'Email',
			"$email"
		)
	);
	
	$pdf->ezTable($data,'' , '',array('showHeadings'=>0,'shaded'=>0,'showLines'=>2));
	
	for($i=0;$i<=1;$i++)
		$pdf->ezText('',12);
	
	
	for($i=0;$i<=4;$i++)
		$pdf->ezText('',12);
	
	

	$pdfcode = $pdf->output();
	//$pdf->ezStream();
	
	// write pdf 
	$fPDFname = tempnam('/tmp','PDF_Registration Details').'.pdf';
	$fp = fopen($fPDFname,'w');
	fwrite($fp,$pdfcode);
	
	$fpPDF = fopen($fPDFname, "rb");
	$filePDF = fread($fpPDF, 102460);
	fclose($fp);
	$filePDF = chunk_split(base64_encode($filePDF));

		
	$file_name = 'Registeration Data.pdf';
	$file_path = $fPDFname;
*/
/***************  END PDF ATTACHMENT DOCUMENT GENERATION ***************/


/***************  Database operations ***************/


		$register_type = "";
		
		if($_REQUEST["register_type"] == "prepaid"){
		$online_customer_services = "PC";
		$register_type = "Prepaid";
		}else{
		$online_customer_services = "TC";
		$register_type = "Trading";		
		}  

$userRemarks=mysql_real_escape_string($userRemarks);
$timeStamp = date('Y-m-d H:i:s');
$query = "INSERT INTO ".TBL_CUSTOMER." (created, firstName, lastName, customerName, accountName,password,gender, email, Address, city, Zip, Phone, Mobile, dob, customerNumber, customerStatus, Country, placeOfBirth, customerType, userRemarks, ipAddress, browserName, custCategory,payinBook,countryOfResidence,initial_load,card_issue_currency,online_customer_services) VALUES('$timeStamp', '$forename', '$surname', '$fullName', '$accountName', '$password','$gender', '$email', '$address', '$town', '$postCode', '$landline', '$mobile', '$dobYear.\"-\".$dobMonth.\"-\".$dobDay', '$customerNumber_value', 'Disable', '$resCountry', '$birCountry', 'C', '$userRemarks', '$ipAddress', '$strBrowserName', 'O','$payinBookname','$Countryaddress','$initial_load','$card_issue_currency','$online_customer_services')";

																	


$strRequestDataFull = serialize(mysql_real_escape_string($_REQUEST));
$strPostDataFullPost = serialize(mysql_real_escape_string($_POST));
	
if($_POST["rtype"] != "updateRegisterType" && $_POST["rtype"] != "addOtherCurrency"){
	

insertInto($query);
$prepaid_cust_id = @mysql_insert_id();
	$_SESSION['customerID'] = @mysql_insert_id();										
											
$query_prepaid = "INSERT INTO  prepaidCustData(custID,initial_load,card_issue_currency,is_sent,services) VALUES(".$prepaid_cust_id.", $initial_load, '$card_issue_currency','N','$online_customer_services')";
 
 if($_POST['register_type'] == 'prepaid'){
 insertInto($query_prepaid);
	 }
}


	//debug($logs);
		
		
$strQueryRequestDebug = "INSERT INTO registrationFromRequest (`first_name`, `sur_name`, `request`, `post`, `logs`, `session_id`) VALUES('".$_POST['forename']."','".$_POST['surname']."', '$strRequestDataFull', '$strPostDataFullPost', '".$_SESSION['logs']."' , '".$_SESSION['customerID']."')";
insertInto($strQueryRequestDebug);
//$strQueryBrowserName = "UPDATE ".TBL_CUSTOMER." SET `browserName` = '$strBrowserName' WHERE customerID = '".$_SESSION['customerID']."'";

//debug($_SESSION['customerID']);
$userType = 'C';
//debug($passportNumber);
//if(!empty($_POST['passportNumber'])){
if(isset($passportNumber)){
	
	//$passportTypeQuery = "SELECT doc_cat.documentId FROM category AS cat INNER JOIN document_category AS doc_cat ON cat.id = doc_cat.categoryId WHERE cat.description = 'Passport'";
	
	$passportTypeQuery = "SELECT id FROM ".TBL_ID_TYPES." AS idTypes WHERE idTypes.title = 'Passport' AND idTypes.active = 'Y'";
	//$passportTypeID = mysql_fetch_array($resultPassport);
	$passportTypeID = selectFrom($passportTypeQuery);
	
	$strPassportIssueDate = $passportIssueYear."-".$passportIssueMonth."-".$passportIssueDay;
	
	// passport database query
	$strPassportIDQuery = "INSERT INTO ".TBL_MULTIPLE_USER_ID_TYPES." (id_type_id, user_id, user_type, id_number, issued_by, issue_date, expiry_date) VALUES('".$passportTypeID['id']."', '".$_SESSION['customerID']."', '$userType', '$passportNumber', '$passportCountry', '$strPassportIssueDate', '".$passportExpiryYear."-".$passportExpiryMonth."-".$passportExpiryDay."')";
	if($_POST['rtype'] != 'updateRegisterType' && $_POST['rtype'] != 'updateRegisterType'){
	insertInto($strPassportIDQuery);
	}
	
	/* ================================ call soap api  ============================= */
	
	
	
	
	
	
	
	/* ================================ call soap api  ============================= */
	
	
	$queryComplianceGB = "INSERT INTO ".TBL_GB_COMPLIANCE." (customerID, authenticationID, documentType, score, result, verificationDate) VALUES('".$_SESSION['customerID']."', '$authenticationID', 'Passport', '$score', '$bandText', '$timeStamp')";

	//debug($queryComplianceGB);
	insertInto($queryComplianceGB);

}

// ID Card database query
if(!empty($IDCardNumber)){


//$IDCardQuery = "SELECT doc_cat.documentId FROM category AS cat INNER JOIN document_category AS doc_cat ON cat.id = doc_cat.categoryId WHERE cat.description = 'ID Card'";


$IDCardQuery = "SELECT id FROM ".TBL_ID_TYPES." AS idTypes WHERE idTypes.title = 'Id Card' AND idTypes.active = 'Y'";

//$IDCardTypeID = mysql_fetch_array($resultIDCard);
$IDCardTypeID = selectFrom($IDCardQuery);


$strIDCardQuery = "INSERT INTO ".TBL_MULTIPLE_USER_ID_TYPES." (id_type_id, user_id, user_type, id_number, issued_by, issue_date, expiry_date) VALUES('".$IDCardTypeID['id']."', '".$_SESSION['customerID']."', '$userType', '$IDCardNumber', '$idIssueCountry','','".$idExpiryYear."-".$idExpiryMonth."-".$idExpiryDay."')";

if($_POST['rtype'] != 'updateRegisterType' && $_POST['rtype'] != 'addOtherCurrency'){
insertInto($strIDCardQuery);
}

$queryComplianceGB = "INSERT INTO ".TBL_GB_COMPLIANCE." (customerID, authenticationID, documentType, score, result, verificationDate) VALUES('".$_SESSION['customerID']."', '$authenticationID', 'ID Card', '$score', '$bandText', '$timeStamp')";
insertInto($queryComplianceGB);
}
//debug($queryComplianceGB);



/***************  START EMAIL BODY FOR CUSTOMER ***************/
$timeStamp = time();
$num = md5( time() );
$emailContent = selectFrom("select * from email_templates left join events on email_templates.eventID=events.id where events.id='1' and email_templates.status = 'Enable' ");
$logoCompany = "<img border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/mail_signature.jpg?v=1' />";
$message1 = $emailContent["body"];
$varEmail = array("{customer}","{logo}");
$contentEmail = array($fullName,$logoCompany);

$messageT = str_replace($varEmail,$contentEmail,$message1);

/*
$messageT = "\n<br />Dear ".$fullName.",\n<br /><br />Thank you for registering with Premier FX. We will contact you with your account details.<br /><br />Regards,<br /> ";



$messageT .= "<table>
				<tr>
					<td align='left' width='30%'>
					<img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width='20%'>&nbsp;</td>
					<td align='left'>
						<font size='2px'><font color='#F7A30B'>UK OFFICE</font>\n<br>
						55 Old Broad Street,London, EC2M 1RX.\n<br>
						<font color='#F7A30B'>Tel:</font> +44 (0)845 021 2370\n<br>\n<br>
						<font color='#F7A30B'> PORTUGAL OFFICE</font> \n<br>
						Rua Sacadura Cabral, Edificio Golfe lA, 8135-144 Almancil, Algarve.  \n<br>
						<font color='#F7A30B'>Tel:</font> +351 289 358 511\n<br>
						<font color='#F7A30B'>FAX:</font> +351 289 358 513\n<br>\n<br>
						<font size='2px'><font color='#F7A30B'>SPAIN OFFICE</font>\n<br>
						C/Rambla dels Duvs, 13-1, 07003, Mallorca. \n<br>
						<font color='#F7A30B'>Tel:</font> +34 971 576 724 
						 \n <br> 
						 <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font>
					</td>
				</tr>
			</table>";
			
			*/
			

			
/***************  END EMAIL BODY FOR CUSTOMER ***************/

/***************  START EMAIL BODY FOR SYSTEM ***************/

$messageSystem = "
				\n<br />New Customer is registered from the website.\n<br /> His Email Address is : $email.
";

/***************  END EMAIL BODY FOR SYSTEM ***************/

// Prepare Email for Sender and Account Manger

//$subject = "Registration Confirmation";
$subject = $emailContent["subject"];
$headers  = 'MIME-Version: 1.0' . "\r\n";
// This two steps to help avoid spam   
$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
$headers .= "X-Mailer: PHP v".phpversion()."\r\n";

/*
$headers .= "Cc: [email]".$emailContent['cc']."[/email]" . "\r\n";
$headers .= "Bcc: [email]".$emailContent['bcc']."[/email]" . "\r\n";
*/
//$fromName  = SYSTEM;
$fromName  = $emailContent["fromName"];
$fromEmail = $emailContent["fromEmail"];
//$fromEmail = "info@premfx.com";
//$fromEmail = "arslan.baig@hbstech.co.uk";
$fromEmailSystem = "Reply-To: noreply@premierfx.com";

require_once("lib/phpmailer/class.phpmailer.php");

$sendermailer = new PHPMailer();
$customerinfo = new PHPMailer();
$systemMailer = new PHPMailer();

$systemMailer->FromName = SYSTEM;
$systemMailer->From = $fromEmailSystem; 

if(CONFIG_SEND_EMAIL_ON_TEST == "1")
{
	$systemMailer->AddAddress(CONFIG_TEST_EMAIL,'');
}
else
{
	$systemMailer->AddAddress($fromEmail,'');
}
$systemMailer->IsHTML(true);

$sendermailer->FromName = $fromName;
$sendermailer->From = $fromEmail; 
if(CONFIG_SEND_EMAIL_ON_TEST == "1")
{
	$sendermailer->AddAddress(CONFIG_TEST_EMAIL,'');
}
else
{
	$toField=explode('/',$emailContent['to']);
	/* 
	for($j=0;$j<count($toField);$j++){
		if($toField[$j]=='Sender' || $toField[$j]=='Beneficiary'){
			;//do Nothing
		}
		else{
			$getEmail=selectFrom("select email from admin where userID='".$toField[$j]."'");
				$sendermailer->AddBCC($getEmail['email'],'');
		}
		
	} */
	
	$sendermailer->AddAddress($custEmail,'');
	if($emailContent['cc'])
	$sendermailer->AddAddress($emailContent['cc'],'');
	
	//Add BCC
	if($emailContent['bcc'])
	$sendermailer->AddBCC($emailContent['bcc'],'');
}
/******************Script to attach welcome letter*************************/
/*$emailid=$_POST['email'];
$q="select accountName from ".TBL_CUSTOMER." where email='".$emailid."'";
$result=selectFrom($q);
include_once ('../pdfClass/class.ezpdf.php');
$sfsWelcomeData = attachPDFWelcomeLetterEmailOnReg($result);
$welcome_letter_name = $sfsWelcomeData['file_name'];
$welcome_letter_path = $sfsWelcomeData['file_path'];*/
/*************************************************************************/
$sendermailer->IsHTML(true);
$sendermailer->IsSMTP(true);



// attachements
/*
if($fpPDF){
	$sendermailer->AddAttachment($file_path, $file_name,"base64");
}
else{
	$attachementInfo = '<br/>Registration information could not be attached with email';
}
*/
/***************************Attachment to the email***********************/
//$sendermailer->AddAttachment($welcome_letter_path, $welcome_letter_name,"base64");

// Subject
$sendermailer->Subject = $subject;
$systemMailer->Subject = "New customer registered from website";

// Body
$sendermailer->Body = $messageT;
$systemMailer->Body = $messageSystem;

//Add CC





	if($custEmail ){
	
		if(CONFIG_EMAIL_STATUS == '1')
		{

			//@mail($custEmail, $subject, $messageT, $headers); 
			
		/* Send email for prepaid customer ticket number  13194 ARSLAN ZAFAR */	
			if($_REQUEST['register_type'] != "prepaid"){
			
			$systemEmailFlag = $systemMailer->Send();
			
			$custEmailFlag = $sendermailer->Send();
			
			}else if($_REQUEST["rtype"] != "updateRegisterType"){
			$customer_name = $_REQUEST['forename'];	
	$email = $_REQUEST['email'];
	customerInfoMail($card_issue_currency, $email, $customerinfo);
	
cardRequestEmail($card_issue_currency, $customer_name, $email);


			}else{
			
			//Nothing to do here
			}
			
			//@mail($custEmail, $subject, $messageT, $headers); 
		}
		
	}
	else{
		$msg = "Sender can not be registered.";

		echo "<h5 style='color:#FF4B4B;font-style:italic;'>".$msg."</h5>";
	}	
if ($custEmailFlag && $_POST["rtype"] != "updateRegisterType" && $_POST["rtype"] != "addOtherCurrency") {
	if($custEmailFlag){
		$msg = "Sender is registered successfully and you will receive an activation email after authentication from administrator.";
	$_SESSION['logs'] .= "R,";
	
	$updatelogs = "update registrationFromRequest set logs = '".$_SESSION['logs']."' where session_id = '".$_SESSION['customerID']."' ";
	//echo $updatelogs;
	update($updatelogs);
	}
	//$msg.=$attachementInfo."<br/>";
	echo "<h5 style='color:#4B9D05;font-style:italic;'>".$msg."</h5>";
	exit;
}
/**** Informative Message ***/

if($_REQUEST["rtype"] == "updateRegisterType"){

	$email = $_POST['email'];

$customerID = mysql_fetch_assoc(mysql_query("SELECT customerID FROM ".TBL_CUSTOMER." WHERE email = '".$email."'"));

	
			// if(!empty($_POST['initial_load']) && !empty($_POST['card_issue_currency'])){
			
			// if($_POST['card_issue_currency']!= '---SELECT---'){
			// $initial_load = " ,initial_load = ".$_POST['initial_load']." , card_issue_currency ='".$_POST['card_issue_currency']."'";
			// }
			// }
			
	$customer_id = $customerID["customerID"];
	$updateServices = mysql_query("UPDATE  ".TBL_CUSTOMER." SET online_customer_services = 'PC,TC'  WHERE customerID = '".$customer_id."'");
						
		/* Send email for prepaid customer ticket number  13194 ARSLAN ZAFAR */	
			if($_REQUEST['register_type'] == "prepaid"){
			
			$customer_name = $_REQUEST['forename'];	
	$email = $_REQUEST['email'];
	// customerInfoMail($card_issue_currency, $email, $customerinfo);
// cardRequestEmail($card_issue_currency, $customer_name, $email);

			}else{
			//nothing here
			}
	
	// $initial_load_Prepaid = $_POST['initial_load'];
	 $card_issue_currency_Prepaid = $_POST['card_issue_currency'];
	
	$custID = $customerID["customerID"];
	
	$check_currency = mysql_query("SELECT card_issue_currency from prepaidCustData where card_issue_currency ='".$card_issue_currency_Prepaid."' AND custID = $custID");
	
	$check_currency_fetch = mysql_fetch_array($check_currency);
	if(!empty($check_currency_fetch['card_issue_currency'])){
		echo "You have already registered with this currency";
		exit;
		}else{
		
		$query_prepaid = "INSERT INTO prepaidCustData(custID,initial_load,card_issue_currency,is_sent,services) VALUES(".$customerID["customerID"].", 'NOT', '$card_issue_currency_Prepaid','N','$online_customer_services')";


	
insertInto($query_prepaid);

customerInfoMail($card_issue_currency, $email, $customerinfo);
cardRequestEmail($card_issue_currency, $customer_name, $email);
//echo "You have registered with currency ".$card_issue_currency_Prepaid."";

		}
		

	
			/* ========= TICKET NUMBER 12917 ARSLAN ZAFAR ==== */





				
	
	
	

	
	
 
	$message = "\n<br />Customer Updated And contains both services Prepaid and Trading.\n";
echo "<h5 style='color:#4B9D05;font-style:italic;'>".$message."</h5>" ;
}else


if($_REQUEST["rtype"] == "addOtherCurrency"){
	
	
	$email_updated = $_POST['email'];

	
	$customerID_updated = mysql_fetch_assoc(mysql_query("SELECT customerID FROM ".TBL_CUSTOMER." WHERE email = '$email_updated'"));
	
	
	 /* ========= TICKET NUMBER 12917 ARSLAN ZAFAR ==== */


 
 
$check_currency_query = "SELECT initial_load,online_customer_services,card_issue_currency FROM ".TBL_CUSTOMER." WHERE customerID = ".$customerID_updated["customerID"];
 
$checkCurrency = selectFrom($check_currency_query);


$initial_load_updated = $checkCurrency['initial_load'];
$online_customer_services_updated = $checkCurrency['online_customer_services'];
$card_issue_currency_updated = $_POST['card_issue_currency'];


 $query_prepaid_updated = "INSERT INTO  prepaidCustData(custID,initial_load,card_issue_currency,is_sent,services) VALUES(".$customerID_updated["customerID"].", 'NOT', '$card_issue_currency_updated','N','$online_customer_services_updated')";
 

 
 if($_POST['card_issue_currency']!= '---SELECT---'){
 insertInto($query_prepaid_updated);
	 }
 
}



/* Send email for prepaid customer ticket number  13194 ARSLAN ZAFAR */


function cardRequestEmail($currency, $customerName, $email){

	$queryTemp="Select * FROM email_templates WHERE eventID='15' AND status='Enable' ";
	
	$resultQueryTemp = selectFrom($queryTemp); 
	//debug ($resultQueryTemp, true);
	$logoCompany = "<img height='100' border='0' src='http://".$_SERVER['HTTP_HOST']."/premier_fx_online_module/images/premier/logo_email.png' />";
	//$subject = $emailTemplate["body"];
	
	
	$to  = $email;
	$subject=$resultQueryTemp['subject'];
	$bodyQuery=$resultQueryTemp['body'];
	$varEmail = array("{customer}","{logo}");
	$contentEmail = array($customerName,$logoCompany);
	$messageT = str_replace($varEmail,$contentEmail,$bodyQuery);
	$strFromEmail=$resultQueryTemp['fromEmail'];
	$status= '';
	$from = 'Peter';
	
	   $headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: $strFromEmail <$from>\r\n";
		$boolSentEmail=mail($to,$subject,$messageT,$headers);
		//$boolSentEmail=sendMail($to,$subject,$messageT,$from,$strFromEmail,$status);
		
	if($boolSentEmail){
		//echo "sent";
		}else{
		//echo "not sent";
		
	/* 	mail($to,$subject,$messageT,$from,$strFromEmail,$status); */
		
		}

	
	
}

/* Send email for prepaid customer info  ticket number  13195 ARSLAN ZAFAR */

function customerInfoMail($card_issue_currency, $email, $customerinfo){



$getCustNum = selectFrom("SELECT `customerNumber` FROM `customer` WHERE email  = '".$email."' limit 1");
$customerNumber = $getCustNum['customerNumber'];

$customerinfo->IsHTML(true);
$customerinfo->IsSMTP(true);
$logoCompany = "<img border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo_email.png?v=1' />";

$messageBody = "<p>Dear (Premier),</p><p>This is to inform you that a New Customer '".$customerNumber."' is registered from the website as Prepaid for '".$card_issue_currency."' Card.
Please issue him/her the required card.
</p><p>His/Her Email Address is:'".$email."'</p><br><br>
<p>'".$logoCompany."'</p>";	

//$customerinfo->AddAddress( "info@premfx.com");
$customerinfo->AddAddress( "info@premfx.com");


$customerinfo->From = "Reply-To:noreply@premierfx.com";
$customerinfo->FromName = "Reply-To:noreply@premierfx.com";

// Subject
$customerinfo->Subject = "Prepaid customer registered from website";

// Body
$customerinfo->Body = $messageBody;


$custEmailFlag = $customerinfo->Send();

}


?>