<?php 
	session_start();
	include ("../include/config.php");
	include ("security.php");
	$systemCode = SYSTEM_CODE;	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	/**	maintain report logs 
	 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
	 *	If not exist enter it in order to maintain report logs
	 */
	include_once("maintainReportsLogs.php");
	maintainReportLogs($_REQUEST["pageUrl"],'E');
	
	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = ""; 
	
	$condition=$_REQUEST["condition"];
	$query=unserialize(base64_decode($_REQUEST["query"]));
	$queryCurrency =unserialize(base64_decode($_REQUEST["queryCurrency"]));
	$queryGroup=unserialize(base64_decode($_REQUEST["queryGroup"]));
	$dfFlag=$_REQUEST["dfFlag"];
	$queryGroupSendingCurrencyCancelled =unserialize(base64_decode($_REQUEST["queryGroupSendingCurrencyCancelled"]));

	$contentsTransCancelled = selectMultiRecords($queryGroupSendingCurrencyCancelled);
		//debug($contentsTransCancelled);
 	$contentsTransGroup = selectMultiRecords($queryGroup);
	$contentsTrans = selectMultiRecords($query);
	$allCount = countRecords($queryCurrency);

	header("Content-type: application/x-msexcel"); 
	header("Content-Disposition: attachment; filename=ReportTransactionSummary.xls"); 
	header("Content-Description: PHP/MYSQL Generated Data");
	header('Content-Type: application/vnd.ms-excel');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
 
	$strMainStart   = "<table border='1' >";
	$strMainEnd     = "</table>";
	$strRowStart    = "<tr>";
	$strRowEnd      = "</tr>";
	$strColumnStart = "<td style='mso-number-format:\"\@\"'>";
	$strColumnEnd   = "</td>";
	$strBoldStart 	= "<b>";
	$strBoldClose 	= "</b>";
	$strColSpan		="<td align='center'>";
	$strColSpan2	="<td align='center'>";
	$strnewline		="</br>";
	$strspace       =", ";
	$strcolspan		="<td colspan='2'>";

	if(count($contentsTrans) > 0 || count($contentsCancel) > 0)
	{
		$strFullHtml = "";
		
		$strFullHtml .= $strHtmlOpening;
		
		
		$strFullHtml .= $strMainStart;
		
		
		
		$strFullHtml .= $strRowStart;
		if($agentType =="SUPI" || $agentType =="SUPAI")
		{
			$strFullHtml .= $strColumnStart.$strBoldStart."Date Created".$strBoldClose.$strColumnEnd;
		}
		
		$strFullHtml .= $strColumnStart.$strBoldStart.$systemCode.$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart.$manualCode.$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Transaction Status".$strBoldClose.$strColumnEnd;
		
		if($agentType!="SUPI" && $agentType!="SUPAI")
		{
			$strFullHtml .= $strColumnStart.$strBoldStart."Creation Date".$strBoldClose.$strColumnEnd;
		}
		
		$strFullHtml .= $strColumnStart.$strBoldStart."	Customer Name".$strBoldClose.$strColumnEnd;
		
		$strFullHtml .= $strColumnStart.$strBoldStart."Beneficiary Name".$strBoldClose.$strColumnEnd;
		
		if($agentType =="SUPI" || $agentType =="SUPAI"){
		
		$strFullHtml .= $strColumnStart.$strBoldStart."Collection Point".$strBoldClose.$strColumnEnd;
		
		}
		if($agentType!="SUPI" && $agentType!="SUPAI")
		{
		$strFullHtml .= $strColumnStart.$strBoldStart."Amount Sent".$strBoldClose.$strColumnEnd;
		}
		if($agentType!="SUPI" && $agentType!="SUPAI")
		{
		$strFullHtml .= $strColumnStart.$strBoldStart."Originating Currency Type".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Exchange Rate".$strBoldClose.$strColumnEnd;
		}
		
		if($agentType!="SUPI" && $agentType!="SUPAI")
		{
		$strFullHtml .= $strColumnStart.$strBoldStart."Foreign Amount".$strBoldClose.$strColumnEnd;
		}
		
		if($agentType!="SUPI" && $agentType!="SUPAI"){
		$strFullHtml .= $strColumnStart.$strBoldStart."Destination Currency Type".$strBoldClose.$strColumnEnd;
		}
		if($agentType =="SUPI" || $agentType =="SUPAI")
		{
		$strFullHtml .= $strColumnStart.$strBoldStart."Total Amount".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Fees".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Net Amount".$strBoldClose.$strColumnEnd;
		}
		
		if($agentType!="SUPI" && $agentType!="SUPAI")
		{
		$strFullHtml .= $strColumnStart.$strBoldStart."Amount Before commission".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Commission".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Amount After Commission".$strBoldClose.$strColumnEnd;
		}
		
		
		
		
		
		
		if(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "0"){
			$strFullHtml .= $strColumnStart.$strBoldStart."Admin Staff Name".$strBoldClose.$strColumnEnd;
		}
		
		if($agentType!="SUPI" && $agentType!="SUPAI"){
		$strFullHtml .= $strColumnStart.$strBoldStart."Introducer Name ".$strBoldClose.$strColumnEnd;
		}
				
		if($agentType=="SUPI" || $agentType=="SUPAI")
		{
		$strFullHtml .= $strColumnStart.$strBoldStart."Status".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Date Picked Up".$strBoldClose.$strColumnEnd;
		}
		$strFullHtml .= $strRowEnd;
		
		
		
		for($i=0;$i < count($contentsTrans);$i++)
		{
		$totalAmount = 0;
		$foriegnTotal = 0;
		$grandTotal = 0;
		
		{
			$strFullHtml .= $strRowStart;
			
			if($agentType =="SUPI" || $agentType =="SUPAI")
			{
				$datee=dateFormat($contentsTrans[$i]["transDate"],$dfFlag);
				$strFullHtml .= $strColumnStart.$datee.$strColumnEnd;
			}
			
			$strFullHtml .= $strColumnStart.$contentsTrans[$i]["refNumberIM"].$strColumnEnd;
			$strFullHtml .= $strColumnStart.$contentsTrans[$i]["refNumber"].$strColumnEnd;
			$strFullHtml .= $strColumnStart.$contentsTrans[$i]["transStatus"].$strColumnEnd;
			if($agentType!="SUPI" && $agentType!="SUPAI")
			{
				$strFullHtml .= $strColumnStart.$contentsTrans[$i]["transDate"].$strColumnEnd;
			}
			$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
			
			$strFullHtml .= $strColumnStart.ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"])."".$strColumnEnd;
			
			
			$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
			
			$strFullHtml .= $strColumnStart.ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"])."".$strColumnEnd;
			
			if($agentType =="SUPI" || $agentType =="SUPAI")
			{ 
				$locationContent = selectFrom("select cp_branch_name, cp_corresspondent_name from cm_collection_point where cp_id='".$contentsTrans[$i]["collectionPointID"]."'");
				$strFullHtml .= $strColumnStart.$contentsTrans[$i]["correspondentName"]." ".$contentsTrans[$i]["BranchName"].$strColumnEnd;
			}
			
			if($agentType!="SUPI" && $agentType!="SUPAI")
			{
				$transAmount=number_format($contentsTrans[$i]["transAmount"],2,'.',',');
				$strFullHtml .= $strColumnStart.$transAmount.$strColumnEnd;
				$totalAmount += $contentsTrans[$i]["transAmount"];
			}
			
			if($agentType!="SUPI" && $agentType!="SUPAI")
			{	
				$exchangeRate=number_format($contentsTrans[$i]["exchangeRate"],2,'.',',');
				$strFullHtml .= $strColumnStart.getCurrencySign($contentsTrans[$i]["currencyFrom"]).$strColumnEnd;
				$strFullHtml .= $strColumnStart.$exchangeRate.$strColumnEnd;
			}
			
			if($agentType!="SUPI" && $agentType!="SUPAI")
			{
				$localAmount=number_format($contentsTrans[$i]["localAmount"],2,'.',',');
				$strFullHtml .= $strColumnStart.$localAmount.$strColumnEnd;
				$foriegnTotal += $contentsTrans[$i]["localAmount"];
			}
			
			if($agentType!="SUPI" && $agentType!="SUPAI")
			{
				$strFullHtml .= $strColumnStart.getCurrencySign($contentsTrans[$i]["currencyTo"]).$strColumnEnd;
			}
			
			$transAmounnt=number_format($contentsTrans[$i]["transAmount"],2,'.',',');
			$IMFeee=number_format($contentsTrans[$i]["IMFee"],2,'.',',');
			$TotalAmount=number_format($contentsTrans[$i]["totalAmount"],2,'.',',');
			
			$grandTotal += $contentsTrans[$i]["totalAmount"];
			$strFullHtml .= $strColumnStart.$transAmounnt.$strColumnEnd;
			$strFullHtml .= $strColumnStart.$IMFeee.$strColumnEnd;
			$strFullHtml .= $strColumnStart.$TotalAmount.$strColumnEnd;
			
			if($agentType=="SUPI" || $agentType=="SUPAI")
			{
				$localAmount=number_format($contentsTrans[$i]["localAmount"],2,'.',',');
				$strFullHtml .= $strColumnStart.$localAmount.$strColumnEnd;
				$foriegnTotal += $contentsTrans[$i]["localAmount"];
			}
			//$bankContent = selectFrom("select name from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["benAgentID"]."'");
			
			
			
			if(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "0"){
				$queryAdminStaff = "SELECT cust.parentID, admins.username, admins.name FROM ".TBL_CUSTOMER." AS cust, ".TBL_ADMIN_USERS." AS admins WHERE cust.customerID = '".$contentsTrans[$i]["customerID"]."' AND admins.userID = cust.parentID LIMIT 1";
				$adminStaffName = selectFrom($queryAdminStaff);
				$strFullHtml .= $strColumnStart.$adminStaffName['name']." [".$adminStaffName['username']."]".$strColumnEnd;
			}
			
			if($agentType!="SUPI" && $agentType!="SUPAI")
			{
				$agentName = selectFrom("select name,username,agentCompany from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				$strFullHtml .= $strColumnStart.$agentName['agentCompany']." [".$agentName['username']."]".$strColumnEnd;
			}
						
			if($agentType=="SUPI" || $agentType=="SUPAI")
			{
				$strFullHtml .= $strColumnStart.$contentsTrans[$i]["transStatus"].$strColumnEnd;
				$strFullHtml .= $strColumnStart;
				if($contentsTrans[$i]["deliveryDate"]!= '0000-00-00 00:00:00')
				{ 
					$strFullHtml .=dateFormat($contentsTrans[$i]["deliveryDate"],$dfFlag);
				} 
				$strFullHtml .= "".$strColumnStart;
			}
			$strFullHtml .= $strRowEnd;
		}
		
		$currencyBottom[$k] = $contentCurrency[$k]["currencyTo"];
		$totalAmountBottom[$k] = $totalAmount;
		$foriegnTotalBottom[$k] = $foriegnTotal;
		$grandTotalBottom[$k] = $grandTotal;	
		}
		$strFullHtml .= $strRowStart;
		
		
		$strFullHtml .= $strColumnStart.$strBoldStart."Total".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strRowEnd;
		$totalCancelLess 			= 0;
		$totalTransAmountGrouped	= 0;
		$totalIMFee 				= 0;
		$totalGrandAmountLess 		= 0;
		$totalCurrencyArr			= array();
		$totalCurrencyArr1			= array();
		
		for($s=0;$s < count($contentsTransGroup);$s++)
		{
			//$totalCurrencyArr[] = $contentsTransGroup[$s]["currencyFrom"];
			$totalCancelLess += $contentsTransGroup[$s]["totalGroup"];
			$totalTransAmountGrouped += $contentsTransGroup[$s]["totalTransAmount"];
			$totalIMFee += $contentsTransGroup[$s]["IMFee"];
			$totalGrandAmountLess += $contentsTransGroup[$s]["grandAmount"];
		}
		if(count($contentsTransGroup)>0)
		{
			for($bottom=0;$bottom < count($contentsTransGroup);$bottom++)
			{
				$strFullHtml .= $strRowStart;
				
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;

				if($agentType!="SUPI" && $agentType!="SUPAI")
				{
					$totalTransAmount=number_format($contentsTransGroup[$bottom]["totalTransAmount"],2,'.',',');
					$strFullHtml .= $strColumnStart.$totalTransAmount.$strColumnEnd;
				
				
				
				if(isset( $totalCurrencyArr[$contentsTransGroup[$bottom]["currencyFrom"]]))
					{
					 $totalCurrencyArr[$contentsTransGroup[$bottom]["currencyFrom"]]= $totalCurrencyArr[$contentsTransGroup[$bottom]["currencyFrom"]] + $contentsTransGroup[$bottom][  	 
							 "totalTransAmount"];
					}
					else {
						
						$totalCurrencyArr[$contentsTransGroup[$bottom]["currencyFrom"]]=$contentsTransGroup[$bottom][  	 
							 "totalTransAmount"];
					}
				
				}
				
				
				$strFullHtml .= $strColumnStart.getCurrencySign($contentsTransGroup[$bottom]["currencyFrom"]).$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				
				if($agentType!="SUPI" && $agentType!="SUPAI")
				{
					$totalLocalAmount=number_format($contentsTransGroup[$bottom]["totalLocalAmount"],2,'.',',');
					$strFullHtml .= $strColumnStart.$totalLocalAmount.$strColumnEnd;
					
					
					
					 if(isset( $totalCurrencyArr1[$contentsTransGroup[$bottom]["currencyTo"]]))
					{
					 $totalCurrencyArr1[$contentsTransGroup[$bottom]["currencyTo"]]= $totalCurrencyArr1[$contentsTransGroup[$bottom]["currencyTo"]] + $contentsTransGroup[$bottom]["totalLocalAmount"];
					}
					else {
						
						$totalCurrencyArr1[$contentsTransGroup[$bottom]["currencyTo"]]=$contentsTransGroup[$bottom][  	 
							"totalLocalAmount"];
					}
					
					
				}
				
				if($agentType!="SUPI" && $agentType!="SUPAI")
				{
					$strFullHtml .= $strColumnStart.$contentsTransGroup[$bottom]["currencyTo"].$strColumnEnd;
				}
				
				if($agentType!="SUPI" && $agentType!="SUPAI")
				{
					$totalTransAmmount=number_format($contentsTransGroup[$bottom]["totalTransAmount"],2,'.',',');
					$strFullHtml .= $strColumnStart.$totalTransAmmount.' '.getCurrencySign($contentsTransGroup[$bottom]["currencyFrom"]).$strColumnEnd;
				}
				
				if($agentType=="SUPI" || $agentType=="SUPAI")
				{
					$strFullHtml .= $strColumnStart."".$strColumnEnd;
				}
				
				if($agentType!="SUPI" && $agentType!="SUPAI")
				{
					$IMFee=number_format($contentsTransGroup[$bottom]["IMFee"],2,'.',',');
					$GrandAmount=number_format($contentsTransGroup[$bottom]["grandAmount"],2,'.',',');
					$strFullHtml .= $strColumnStart.$IMFee.' '.getCurrencySign($contentsTransGroup[$bottom]["currencyFrom"]).$strColumnEnd;
					$strFullHtml .= $strColumnStart.$GrandAmount.' '.getCurrencySign($contentsTransGroup[$bottom]["currencyFrom"]).$strColumnEnd;
				}
				
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				
				if($agentType=="SUPI" || $agentType=="SUPAI")
				{
					$totalLocalAmountt=number_format($contentsTransGroup[$bottom]["totalLocalAmount"],2,'.',',');
					$strFullHtml .= $strColumnStart.$totalLocalAmountt.$strColumnEnd; 
				}
				
				if(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "0"){
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				}
				
				//$strFullHtml .= $strColumnStart."".$strColumnEnd;
				//$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strRowEnd;
			}
			
			if(is_array($totalCurrencyArr))
				$totalCurrencyArr = array_unique($totalCurrencyArr);
			
			if(count($totalCurrencyArr)> 0)
			{
				$totalCurrency = $totalCurrencyArr[0];
				$TotalTransAmountGrouped=number_format($totalTransAmountGrouped,2,'.',',');
				$TotalIMFeee=number_format($totalIMFee,2,'.',',');
				$TotalGrandAmountLess=number_format($totalGrandAmountLess,2,'.',',');
				$strFullHtml .= $strRowStart; 
				$strFullHtml .= $strColumnStart.$strBoldStart."Transaction(s) Not Cancelled   :".$totalCancelLess.$strBoldClose.$strColumnEnd;
				$strFullHtml .= $strColumnStart.$strBoldStart."".$strBoldClose.$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart.$strBoldStart."".$strBoldClose.$strColumnEnd;
				$strFullHtml .= $strColumnStart.$strBoldStart."".$strBoldClose.$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				
				$strFullHtml .= $strColumnStart.$strBoldStart."".$strBoldClose.$strColumnEnd;
				
				if(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "1"){
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				}
				
				$strFullHtml .= $strColumnStart.$strBoldStart."".$strBoldClose.$strColumnEnd;
				$strFullHtml .= $strColumnStart.$strBoldStart."".$strBoldClose.$strColumnEnd;
				$strFullHtml .= $strRowEnd;
			}
		}
		
		$totalTransAmountCanc 		= 0;
		$totalTransIMFeeCanc 		= 0;
		$totalTransGrandAmountCanc 	= 0;
		$totalCurrencyArrCanc		= array();
							
		for($c=0;$c < count($contentsTransCancelled);$c++)
		{
			$totalCurrencyArrCanc[]	= $contentsTransCancelled[$c]["currencyFrom"];
			$totalTransAmountCanc += $contentsTransCancelled[$c]["totalTransAmount"];
			$totalTransIMFeeCanc += $contentsTransCancelled[$c]["IMFee"];
			$totalTransGrandAmountCanc += $contentsTransCancelled[$c]["grandAmount"]; 
		}

		if(count($contentsTransCancelled)>0)
		{
			for($cancel=0;$cancel < count($contentsTransCancelled);$cancel++)
			{		
				$strFullHtml .= $strRowStart;
				
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;

				if($agentType!="SUPI" && $agentType!="SUPAI")
				{
					$totalTransAmount=number_format($contentsTransCancelled[$cancel]["totalTransAmount"],2,'.',',');
					$strFullHtml .= $strColumnStart.$totalTransAmount.$strColumnEnd;
				}
				
				$strFullHtml .= $strColumnStart.getCurrencySign($contentsTransCancelled[$cancel]["currencyFrom"]).$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				
				if($agentType!="SUPI" && $agentType!="SUPAI")
				{
					$totalLocalAmount=number_format($contentsTransCancelled[$cancel]["totalLocalAmount"],2,'.',',');
					$strFullHtml .= $strColumnStart.$totalLocalAmount.$strColumnEnd;
				}
				
				if($agentType!="SUPI" && $agentType!="SUPAI")
				{
					$strFullHtml .= $strColumnStart.$contentsTransCancelled[$cancel]["currencyTo"].$strColumnEnd;
				}
				
				if($agentType!="SUPI" && $agentType!="SUPAI")
				{
					$totalTransAmount=number_format($contentsTransCancelled[$cancel]["totalTransAmount"],2,'.',',');
					$strFullHtml .= $strColumnStart.$totalTransAmount.' '.getCurrencySign($contentsTransCancelled[$cancel]["currencyFrom"]).$strColumnEnd;
				}
				
				if($agentType=="SUPI" || $agentType=="SUPAI")
				{
					$strFullHtml .= $strColumnStart."".$strColumnEnd;
				}
				
				if($agentType!="SUPI" && $agentType!="SUPAI")
				{ 
					$CancelIMFee=number_format($contentsTransCancelled[$cancel]["IMFee"],2,'.',',').' '.getCurrencySign($contentsTransCancelled[$cancel]["currencyFrom"]);
					$CancelGrandAmount=number_format($contentsTransCancelled[$cancel]["grandAmount"],2,'.',',').' '.getCurrencySign($contentsTransCancelled[$cancel]["currencyFrom"]); 
					$strFullHtml .= $strColumnStart.$CancelIMFee.$strColumnEnd;
					$strFullHtml .= $strColumnStart.$CancelGrandAmount.$strColumnEnd;
				}
				
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				
				if($agentType=="SUPI" || $agentType=="SUPAI")
				{
					$CancelLocalAmountt=number_format($contentsTransCancelled[$cancel]["totalLocalAmount"],2,'.',',');
					$strFullHtml .= $strColumnStart.$CancelLocalAmountt.$strColumnEnd;
				}
				
				if(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "1"){
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				}
				
				//$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				
				$strFullHtml .= $strRowEnd;
				
			}
			
			if(is_array($totalCurrencyArrCanc))
				$totalCurrencyArrCanc = array_unique($totalCurrencyArrCanc);
				
			$totalCurrencyCanc = $totalCurrencyArrCanc[0];
			$TotalTansAmountCancl=number_format($totalTransAmountCanc,2,'.',',');
			$totalTransIMFeeCanc=number_format($totalTransIMFeeCanc,2,'.',',');
			$totalTransGrandAmountCanc=number_format($totalTransGrandAmountCanc,2,'.',',');
			
			$strFullHtml .= $strRowStart;
			$diffResult=$allCount-$totalCancelLess;   
			$strFullHtml .= $strColumnStart.$strBoldStart."Transaction(s) [Cancelled]:".$diffResult.$strBoldClose.$strColumnEnd;
			$strFullHtml .= $strColumnStart.$strBoldStart."".$strBoldClose.$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart.$strBoldStart."".$strBoldClose.$strColumnEnd;
			$strFullHtml .= $strColumnStart.$strBoldStart."".$strBoldClose.$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			
			$strFullHtml .= $strColumnStart.$strBoldStart."".$strBoldClose.$strColumnEnd;
			
			if(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "1"){
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			}
			
			$strFullHtml .= $strColumnStart.$strBoldStart."".$strBoldClose.$strColumnEnd;
			$strFullHtml .= $strColumnStart.$strBoldStart."".$strBoldClose.$strColumnEnd;
			$strFullHtml .= $strRowEnd;
			
		}
			
			
				$strFullHtml .= $strHtmlClosing;
				$strFullHtml .= $strMainEnd;	
				echo $strFullHtml;
}
	else
		echo "No Records Found";
		
			$strFullHtml=$strMainStart;
			$strFullHtml .= $strRowStart;
			$strFullHtml .= $strColumnStart.$strBoldStart."Cumulative Sum of Transactions[Not Cancelled]:".$strBoldClose.$strColumnEnd;
			$strFullHtml .= $strRowEnd;
			$strFullHtml .= $strRowStart;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			

			$strFullHtml .=$strcolspan;
			foreach ($totalCurrencyArr as $key => $value){
			
				$strFullHtml .=$key.'='.number_format($value,2,'.',',');
				$strFullHtml .=$strspace;
			}
				$strFullHtml .=$strColumnEnd;
				
								
			
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			
			
		$strFullHtml .=$strcolspan;
			foreach ($totalCurrencyArr1 as $key => $value){
			
				$strFullHtml .=$key.'='.number_format($value,2,'.',',');
				$strFullHtml .=$strspace;
			}
				$strFullHtml .=$strColumnEnd;
			
			//$strFullHtml .=$strColumnStart."".$strColumnEnd;
			
			
			
			$strFullHtml .=$strColumnStart;
			foreach ($totalCurrencyArr as $key => $value){
			
				$strFullHtml .=$key.'='.number_format($value,2,'.',',');
				$strFullHtml .=$strspace;
			}
				$strFullHtml .=$strColumnEnd;
				
					$strFullHtml .=$strColumnStart."".$strColumnEnd;
			
				$strFullHtml .=$strColumnStart;
			foreach ($totalCurrencyArr as $key => $value){
			
				$strFullHtml .=$key.'='.number_format($value,2,'.',',');
				$strFullHtml .=$strspace;
			}
				$strFullHtml .=$strColumnEnd;
			
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
		//	$strFullHtml .=$strColumnStart."".$strColumnEnd;
			//$strFullHtml .=$strColumnStart."".$strColumnEnd;
			/*
			 
			$strFullHtml .=$strColumnStart;
			foreach ($totalCurrencyArr as $key => $value){
			echo $key.'='.$value;
					}
			
			$strColumnEnd;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			$strFullHtml .=$strColumnStart."".$strColumnEnd;
			
			
			
			$strFullHtml .= $strRowEnd;
			$strFullHtml .= $strHtmlClosing;
			$strFullHtml=$strMainEnd;
			
			*/	
		echo $strFullHtml;
	
