<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$id =  $_GET["id"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

if ($offset == "")
{
	$offset = 0;

}
$limit=200;

if ($_GET["newOffset"] != "") {
	
	$offset = $_GET["newOffset"];
}

if($offset == 0)
{
	$_SESSION["dgrandTotal"]="";
	$_SESSION["dCurrentTotal"]="";	
	
	$_SESSION["dgrandTotal2"]="";
	$_SESSION["dCurrentTotal2"]="";		
}

$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";

	  $queryDate = "(t.transDate >= '".$_GET["fromDate"]." 00:00:00' and t.transDate <= '".$_GET["toDate"]." 23:59:59')";
	 $query = "select transID,transDate,refNumberIM,transAmount,localAmount,currencyFrom,currencyTo,exchangeRate from deal_trans_details  where id='".$id."'";
	 $queryCnt = "select count(transID) from deal_trans_details  where id='".$id."'";   	
   	
		
		$query .= " Order By transDate LIMIT $offset , $limit";                      
		$contentsTrans = selectMultiRecords($query);
		$allCount = countRecords($queryCnt);
?>            
<html>        
<head>
<title>View Transaction Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./styles/admin.js"></script>
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5" >
  <tr> 
    <td class="topbar"><b><font color="#000000" size="2">View transaction details.</font></b></td>
  </tr>
 
   <?
			if ($allCount > 0){
	?>
	
	<tr>
		<td colspan="11" align="center">
	<form action="deal_sheet_trans_details.php" method="post" name="trans">
				<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
          <tr>
            <td width="477">
              <?php if (count($contentsTrans) > 0) {;?>
              Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans));?></b>
              of
              <?=$allCount; ?>
              <?php } ;?>
            </td>
            <?php if ($prv >= 0) { ?>
            <td width="50"> <a href="<?php print $PHP_SELF ."?customerID=" . $customerID . "&newOffset=0&sortBy=".$_GET["sortBy"]."&total=first&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo";?>"><font color="#005b90">First</font></a>
            </td>
            <td width="53" align="right"> <a href="<?php print $PHP_SELF ."?customerID=" . $customerID . "&newOffset=$prv&sortBy=".$_GET["sortBy"]."&total=pre&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo"?>"><font color="#005b90">Previous</font></a>
            </td>
            <?php } ?>
            <?php
		if ( ($nxt > 0) && ($nxt < $allCount) ) {
			$alloffset = (ceil($allCount / $limit) - 1) * $limit;
	?>
            <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?customerID=" . $customerID . "&newOffset=$nxt&sortBy=".$_GET["sortBy"]."&total=next&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo";?>"><font color="#005b90">Next</font></a>&nbsp;
            </td>
            <td width="82" align="right"> <!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&total=last";?>"--><font color="#005b90">&nbsp;</font><!--/a-->&nbsp;
            </td>
            <?php }
	   ?>
          </tr>
        </table>		
			</form>  
		</td>
	 </tr> 
<? 
			}
?>
  <tr> 
    <td align="center">
    	<table width="842" border="1" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
        <tr class="style1"> 
		 <th>Dated</th>
          <th>Reference Number</th>
          <th>Currency</th>
		  <th>Trans Amount</th>
		   <th>Rate</th>
          <th>Local Amount</th>
        </tr>
<?
if(count($contentsTrans) > 0)
 {
 	$total = 0;
   	$totalLocal = 0;
   for($i = 0; $i < count($contentsTrans); $i++)
   {      
	?>
        <tr> 
		<td> &nbsp; 
            <?  echo dateFormat($contentsTrans[$i]["transDate"],"2"); ?>
          </td>
         
          <td>
           &nbsp; <?  echo $contentsTrans[$i]["refNumberIM"]; ?>
          </td>
		   <td>
           &nbsp; <?  echo $contentsTrans[$i]["currencyTo"]; ?>
          </td>
		  <td>
           &nbsp; <?  echo(number_format($contentsTrans[$i]["transAmount"],2,'.',',')); 
           ?>
          </td>
		   <td>
           &nbsp; <?  echo $contentsTrans[$i]["exchangeRate"]; ?>
          </td>
          <td>
           &nbsp; <?  echo(number_format($contentsTrans[$i]["localAmount"],2,'.',',')); 
           $total = $total + $contentsTrans[$i]["localAmount"];
           ?>
          </td>
		
        </tr>
		
  <?
  	}?>
	<tr bgcolor="#CCCCCC" class="style1">
  
  	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp; Total</td>		
    <td>
    	&nbsp;<?  echo(number_format($total,2,'.',',')); ?>
    </td>
	<td>&nbsp;</td>	
	<td>&nbsp;</td>	
  </tr>
  <?
  } // count greater than 0
			?>
      </table></td>
  </tr>
  
  <tr>
    <td align="center">
	<div align="center">
	<form name="form1" method="post" action="">   
		<div class='noPrint'>               
			<input type="submit" name="Submit2" value="Print This Transaction" onClick="print()">
		</div>
	</form>
	</div>
	</td>
  </tr>
</table>
</body>
</html>