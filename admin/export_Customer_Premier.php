<?php
session_start();

include ("../include/config.php");
include ("security.php");

$filters = "";
$agentName = $_REQUEST["agentID"];
/**
 * Date filter
 */
$queryCurrency = $_REQUEST["currency"];
$fromDate = $_REQUEST["fromDate"];
$toDate = $_REQUEST["toDate"];
if($fromDate!="" && $toDate!="")
	$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
$exportType = $_REQUEST["exportType"];
$query = stripslashes($_REQUEST["query"]);

if($exportType!="" && $query !=''){
	
			$contentsAcc = selectMultiRecords($query);
			$allCount = count($contentsAcc);
}
	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	if($exportType == "xls")
	{
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=exportCustomer-".time().".xls"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

		$strMainStart   = "<table>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
	}
	elseif($exportType == "csv")
	{
		header("Content-Disposition: attachment; filename=exportCustomer-".time().".csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "";
		$strColumnEnd   = ",";

	}
	else
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Export Customer</title>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;
	$strFullHtml .= $strRowStart;
// Column Heading starts
	$strFullHtml .= $strColumnStart.$strBoldStart."Customer Category".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Sender Name".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."User Created Date".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Address".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Country".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Email".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Sender Number".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Sender Type".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Balance".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Total Transactions Amount".$strBoldClose.$strColumnEnd;	
	$strFullHtml .= $strRowEnd;
// Column Heading ends
	$strFullHtml .= $strRowStart;
	
    for($i=0;$i < count($contentsAcc);$i++)
	{
		if($contentsAcc[$i]["custCategory"] == 'O')
		{
			$strCustCat = "Online";
		}
		elseif($contentsAcc[$i]["custCategory"] == 'W')
		{
			$strCustCat = "Walkin";
		}
		elseif($contentsAcc[$i]["custCategory"] == 'B')
		{
			$strCustCat = "Both";
		}
	$custCategory = selectFrom("select name FROM customer_category WHERE id = '".$contentsAcc[$i]["cust_category_id"]."' AND enabled = 'Y'");
		$strFullHtml .=$strColumnStart.$strCustCat.$strColumnEnd;
		$strFullHtml .= $strColumnStart. ($contentsAcc[$i]["Title"] == "Other" ? $contentsAcc[$i]["other_title"] : $contentsAcc[$i]["Title"]) .  " " . $contentsAcc[$i]["firstName"] . " " . $contentsAcc[$i]["middleName"] . " " . $contentsAcc[$i]["lastName"].$strColumnEnd;
		$strFullHtml .=$strColumnStart.$contentsAcc[$i]["created"].$strColumnEnd;
		$strFullHtml .=$strColumnStart.$contentsAcc[$i]["Address"].$strColumnEnd;
		$strFullHtml .=$strColumnStart.$contentsAcc[$i]["Country"].$strColumnEnd;
		$strFullHtml .=$strColumnStart.$contentsAcc[$i]["Email"].$strColumnEnd;
		$strFullHtml .=$strColumnStart.$contentsAcc[$i]["accountName"].$strColumnEnd;
		$strFullHtml .=$strColumnStart.$custCategory["name"].$strColumnEnd;
		if(CONFIG_SENDER_AS_COMPANY =="1") {
				  	if($contentsCust[$i]["customerType"] == "company")
				         $custType = "company";
					else
						 $custType = "individual";
		$strFullHtml .=$strColumnStart.$custType.$strColumnEnd;
		}
		$strFullHtml .=$strColumnStart.$contentsAcc[$i]["balance"].$strColumnEnd;
		if($contentsAcc[$i]["accumulativeAmount"]== "")
		{
		//echo "Haroon";
		//echo '--'; 
		$strFullHtml .=$strColumnStart.'--'.$strColumnEnd;
		}
		else{
		$strFullHtml .=$strColumnStart.$contentsAcc[$i]["accumulativeAmount"].$strColumnEnd;
		}
		$strFullHtml .= $strRowEnd;
	// Data Row Ends	
	 	

   }


	echo $strFullHtml;
?>


