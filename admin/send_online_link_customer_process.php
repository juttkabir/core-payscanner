<?php 
	/**
	 * @package : online registered users
	 * @subpackage: Send Online link to Customers
	 * @author: M. Awais Umer
	 */
	session_start();
	include ("../include/config.php");
	require_once("lib/phpmailer/class.phpmailer.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	$agentType = getAgentType();
	//debug($_POST);
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	// This two steps to help avoid spam   
	$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
	$headers .= "X-Mailer: PHP v".phpversion()."\r\n";
	if(isset($_REQUEST['customer']) && !empty($_REQUEST['customer'])){
		if(is_array($_REQUEST['customer'])){
			//echo json_encode($_REQUEST['customer']);
			$strCustomerIDs = implode(",", $_REQUEST['customer']);
			$strQueryCustomers = "SELECT email, customerID FROM ".TBL_CUSTOMER." WHERE customerID IN ($strCustomerIDs)";
			$arrCustomers = selectMultiRecords($strQueryCustomers);
			for($x = 0; $x < count($arrCustomers); $x++){
				
				//debug($strUpdatePassword);
				
				$strQueryCustomer = "SELECT cust.email AS email, cust.customerID AS customerID,cust.password AS password, admin.email AS adminEmail, cust.customerName AS customerName, admin.name AS adminName,accountName,Address,Phone,admin.userID FROM ".TBL_CUSTOMER." AS cust INNER JOIN ".TBL_ADMIN_USERS." AS admin ON cust.parentID = admin.userID WHERE customerID = '".$arrCustomers[$x]['customerID']."'";
				
				$arrCustomer = selectFrom($strQueryCustomer);
//				$strUpdateCategory = "UPDATE ".TBL_CUSTOMER." SET `custCategory` = 'B' WHERE customerID = '{$arrCustomers[$x]['customerID']}' and `custCategory` = 'W' ";
//				update($strUpdateCategory);
//				$strUpdateMailSent = "UPDATE ".TBL_CUSTOMER." SET `is_sent` = 'Y' WHERE customerID = '{$arrCustomers[$x]['customerID']}'";
//				update($strUpdateMailSent);
				
				
				$emailContent = selectFrom("select * from email_templates left join events on email_templates.eventID=events.id where events.id='5' and email_templates.status = 'Enable'  AND agentID='".$arrCustomer['userID']."'  ");
$logoCompany = "<img border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />";
	$message1 = $emailContent["body"];
	$varEmail = array("{customer}","{email}","{password}","{admin}","{logo}","{accountName}","{phone}","{address}");
	$contentEmail = array($arrCustomer[$x]['customerName'],$arrCustomer[$x]['email'],$arrCustomer[$x]['password'],$arrCustomer[$x]["adminName"],$logoCompany,$arrCustomer[$x]["accountName"],$arrCustomer[$x]["Phone"],$arrCustomer[$x]["Address"]);
	
	$messageT = str_replace($varEmail,$contentEmail,$message1);

				//$subject = "Issue Password";
				$subject = $emailContent["subject"];
				$customerMailer = new PHPMailer();
				$strBody = $messageT;
				/*$strBody = "Dear ".$arrCustomer[$x]['customerName']."\r\n<br />";
				$strBody .= "Your Trading Account password has been set.\n<br />
				Your username is: ".$arrCustomer[$x]['email']."\r\n<br />
				Your new password is: [ ".$password." ]\r\n\n\r<br /><br />
				\n<br />".$arrCustomer[$x]["adminName"]."<br />\n
				Follow this link to login: http://clients.premfx.com/private_registration.php\n<br />
				<table>
					<tr>
						<td align='left' width='30%'>
						<img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width='20%'>&nbsp;</td>
						<td align='left'>
							<font size='2px'><font color='#F7A30B'>UK OFFICE</font>\n<br>
							55 Old Broad Street,London, EC2M 1RX.\n<br>
							<font color='#F7A30B'>Tel:</font> +44 (0)845 021 2370\n<br>\n<br>
							<font color='#F7A30B'> PORTUGAL OFFICE</font> \n<br>
							Rua Sacadura Cabral, Edificio Golfe lA, 8135-144 Almancil, Algarve.  \n<br>
							<font color='#F7A30B'>Tel:</font> +351 289 358 511\n<br>
							<font color='#F7A30B'>FAX:</font> +351 289 358 513\n<br>\n<br>
							<font size='2px'><font color='#F7A30B'>SPAIN OFFICE</font>\n<br>
							C/Rambla dels Duvs, 13-1, 07003, Mallorca. \n<br>
							<font color='#F7A30B'>Tel:</font> +34 971 576 724 
							 \n <br> 
							 <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font>
						</td>
					</tr>
				</table>
				";*/
				//$fromName  = SYSTEM;
				$fromName  = $emailContent["fromName"];
				$fromEmail  = $emailContent["fromEmail"];
				
				$customerMailer->FromName =  $fromName;
				//$customerMailer->From =  $arrCustomer[$x]['adminEmail'];
				$customerMailer->From =  $fromEmail;
				echo "Hello".$arrCustomer[$x]['email'];
				if(CONFIG_SEND_EMAIL_ON_TEST == "1")
				{
					$customerMailer->AddAddress(CONFIG_TEST_EMAIL,'');
				}
				else
				{
					$customerMailer->AddAddress($arrCustomer[$x]['email'],'');
					if($emailContent['cc'])
					$customerMailer->AddAddress($emailContent['cc'],'');
					
					//Add BCC
					if($emailContent['bcc'])
					$customerMailer->AddBCC($emailContent['bcc'],'');
					
				}
				$customerMailer->Subject = $subject;
				$customerMailer->IsHTML(true);
				$customerMailer->Body = $strBody;
				if(CONFIG_EMAIL_STATUS == "1")
				{
					$custEmailFlag = $customerMailer->Send();
				}
				
			}
		}else{
			$intCustomerID = $_REQUEST['customer'];
			
			$strQueryCustomer = "SELECT cust.email AS email, cust.customerID AS customerID, admin.email AS adminEmail, cust.customerName AS customerName,cust.password AS password, admin.name AS adminName,cust.accountName,cust.Phone,cust.Address,admin.userID as userID FROM ".TBL_CUSTOMER." AS cust INNER JOIN ".TBL_ADMIN_USERS." AS admin ON cust.agentID = admin.userID WHERE customerID = '$intCustomerID'";
			//debug($strQueryCustomer);
			$arrCustomer = selectFrom($strQueryCustomer);
	
//			$strUpdateCategory1 = "UPDATE ".TBL_CUSTOMER." SET `custCategory` = 'B' WHERE customerID = '".$intCustomerID."' and `custCategory` = 'W' ";
//				update($strUpdateCategory1);
//				$strUpdateMailSent1 = "UPDATE ".TBL_CUSTOMER." SET `is_sent` = 'Y' WHERE customerID = '".$intCustomerID."'";
//				update($strUpdateMailSent1);
			
			$emailContent = selectFrom("select * from email_templates left join events on email_templates.eventID=events.id where events.id='5' and email_templates.status = 'Enable' AND agentID='".$arrCustomer['userID']."'   ");
$logoCompany = "<img border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />";
	$message1 = $emailContent["body"];
	$varEmail = array("{customer}","{email}","{password}","{admin}","{accountName}","{phone}","{address}","{logo}");
	$contentEmail = array($arrCustomer['customerName'],$arrCustomer['email'],$arrCustomer['password'],$arrCustomer["adminName"],$arrCustomer["accountName"],$arrCustomer["Phone"],$arrCustomer["Address"],$logoCompany);
	
	$messageT = str_replace($varEmail,$contentEmail,$message1);
	
			//debug($arrCustomer);
			//$subject = "Password Issued";
			$subject = $emailContent["subject"];
			$customerMailer = new PHPMailer();
			$strBody = $messageT;
			/*$strBody = "Dear ".$arrCustomer['customerName']."\r\n<br /><br />\r\n";
			$strBody .= "You have issued the passowrd your login details are as under:<br /><br />\r\n\n
			Login Link : http://clients.premfx.com/private_registration.php\r\n<br />
			Username: ".$arrCustomer['email']."\r\n<br />
			Password: ".$password."\r\n\n\r<br /><br />
			\n<br />".$arrCustomer["adminName"]."<br />\n
			<table>
				<tr>
					<td align='left' width='30%'>
					<img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width='20%'>&nbsp;</td>
					<td align='left'>
						<font size='2px'><font color='#F7A30B'>UK OFFICE</font>\n<br>
						55 Old Broad Street,London, EC2M 1RX.\n<br>
						<font color='#F7A30B'>Tel:</font> +44 (0)845 021 2370\n<br>\n<br>
						<font color='#F7A30B'> PORTUGAL OFFICE</font> \n<br>
						Rua Sacadura Cabral, Edificio Golfe lA, 8135-144 Almancil, Algarve.  \n<br>
						<font color='#F7A30B'>Tel:</font> +351 289 358 511\n<br>
						<font color='#F7A30B'>FAX:</font> +351 289 358 513\n<br>\n<br>
						<font size='2px'><font color='#F7A30B'>SPAIN OFFICE</font>\n<br>
						C/Rambla dels Duvs, 13-1, 07003, Mallorca. \n<br>
						<font color='#F7A30B'>Tel:</font> +34 971 576 724 
						 \n <br> 
						 <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font>
					</td>
				</tr>
			</table>
			";*/
			//debug($strBody);
			//$fromName  = SYSTEM;
			$fromName  = $emailContent["fromName"];
			$fromEmail  = $emailContent["fromEmail"];
				
			$customerMailer->FromName =  $fromName;
			//$customerMailer->FromName =  $fromName;
			//$customerMailer->From =  $arrCustomer['adminEmail']; 
			$customerMailer->From =  $fromEmail;
			echo "Hello1".$arrCustomer['email'];
			if(CONFIG_SEND_EMAIL_ON_TEST == "1")
			{
				$customerMailer->AddAddress(CONFIG_TEST_EMAIL,'');
			}
			else
			{
				$customerMailer->AddAddress($arrCustomer['email'],'');
				if($emailContent['cc'])
				$customerMailer->AddAddress($emailContent['cc'],'');
				
				//Add BCC
				if($emailContent['bcc'])
				$customerMailer->AddBCC($emailContent['bcc'],'');
			}
			$customerMailer->Subject = $subject;
			$customerMailer->IsHTML(true);
			$customerMailer->Body = $strBody;
			if(CONFIG_EMAIL_STATUS == "1")
			{
				echo "Hello1".$arrCustomer['email'];
				$custEmailFlag = $customerMailer->Send();
				echo $custEmailFlag;
			}
		}
		//header("LOCATION: issue_password_customer.php");
		echo "<p style='color:#1F8E23'>Online link has been sent to the customer</p>";
	}
?>
<!--<meta http-equiv="refresh" content="2;url=http://--><?php //echo $_SERVER['HTTP_HOST']."/admin/send_online_link_customer.php" ?><!--">-->