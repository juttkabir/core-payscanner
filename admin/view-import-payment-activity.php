<?php
session_start();
/**
* @package Payex
* @subpackage Reports
* Short Description
* This show the listing of import payments activities performed imported 
* from the Reconcile Online Bank Statement  
*
*/
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$benAgentID =  $_GET["benAgentID"];
$dated =  $_GET["dated"];
$agentID =  $_GET["agentID"];
//include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;


if ($offset == "")
	$offset = 0;
	
$limit=10;
if($_REQUEST["limit"]!="")
	$limit=$_REQUEST["limit"];
if ($_GET["newOffset"] != "") {
	
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " login_time";




$agentType = getAgentType();
//$_SESSION["middleName"] = $_POST["middleName"];
if($_POST["Submit"] =="Search" || $_GET["search"]=="search")
{


	$query = "select * from " . TBL_LOGIN_ACTIVITIES . " where table_name ='barclayspayments'";	
    $queryCnt = "select count(*) from " . TBL_LOGIN_ACTIVITIES . " where table_name ='barclayspayments'";	
if(CONFIG_IMPORT_PAYMENT_AUDIT=="1"){
	$query_select = "SELECT
				bar.id as bar_id,
				bar.importedOn,
				bar.currency,
				bar.amount,
				bar.entryDate,
				bar.description as payment_desc,
				bar.tlaCode,
				bar.isDeleted as isDeleted,
				bar.isResolved as isResolved,
				act.description as description,
				act.action_for,
				act.table_name,
				act.activity,
				act.activity_time activity_time,
				ad.username as username,
				pa.agentAmount as payment_amount,
				pa.action_date as action_date,
				tr.refNumberIM as refNumberIM,
				cu.accountName as accountName ";
	$query_clause=" 
			FROM
				login_activities AS act
				Inner Join barclayspayments AS bar ON bar.id = act.action_for
				Left Join payment_action AS pa ON pa.payID = bar.id
				Left Join admin AS ad ON pa.action_id = ad.userID
				Left Join transactions AS tr ON tr.transID = pa.transID
				Left Join customer AS cu ON cu.customerID = tr.customerID
			WHERE
				act.table_name =  'barclayspayments'
			";
	$query = $query_select.''.$query_clause;
	$queryCnt = "SELECT count(bar.id) ".$query_clause;
}

	if($_POST["Submit"]=="Search")
	{		
		$_REQUEST["fMonth"];
		$_REQUEST["fDay"];
		$_REQUEST["fYear"];
		
		$_REQUEST["tMonth"];
		$_REQUEST["tDay"];
		$_REQUEST["tYear"];
				
		
		/*///#9834 my code Up this comment by Mian Anjum;
		////////#######Note#######/////
		// I replace $_SESSION of dfMonth,dfDay,dfYear,dtMonth,dtDay,dtYear With $_REQUEST in Ticket 9834 Issues Resolving at 16/11/2012;
		
		$_SESSION["dfMonth"]="";
		$_SESSION["dfDay"]="";
		$_SESSION["dfYear"]="";
		
		$_SESSION["dtMonth"]="";
		$_SESSION["dtDay"]="";
		$_SESSION["dtYear"]="";
		
		$_SESSION["dfMonth"]=$_POST["fMonth"];
		$_SESSION["dfDay"]=$_POST["fDay"];
		$_SESSION["dfYear"]=$_POST["fYear"];
		
		$_SESSION["dtMonth"]=$_POST["tMonth"];
		$_SESSION["dtDay"]=$_POST["tDay"];
		$_SESSION["dtYear"]=$_POST["tYear"];
		*/
		
		
	}
	
	/*//#9834 my code under this comment by Mian Anjum;
	$fromDate = $_SESSION["dfYear"] . "-" . $_SESSION["dfMonth"] . "-" . $_SESSION["dfDay"];
	$toDate = $_SESSION["dtYear"] . "-" . $_SESSION["dtMonth"] . "-" . $_SESSION["dtDay"];
	*/
	if($_GET['fromDate']!="" && $_GET['toDate']!="" )
	{
			$fromDate = $_GET['fromDate'];
			$toDate = $_GET['toDate'];
			
	}
	else{
	$fromDate = $_REQUEST["fYear"] . "-" . $_REQUEST["fMonth"] . "-" . $_REQUEST["fDay"];
	$toDate = $_REQUEST["tYear"] . "-" . $_REQUEST["tMonth"] . "-" . $_REQUEST["tDay"];
	}
	
//	debug($fromDate);
if(CONFIG_IMPORT_PAYMENT_AUDIT=="1"){	
	$queryDate = " (activity_time >= '$fromDate 00:00:00' and activity_time <= '$toDate 23:59:59' and action_date >= '$fromDate 00:00:00' and action_date <= '$toDate 23:59:59') ";
}else{
	$queryDate = " (activity_time >= '$fromDate 00:00:00' and activity_time <= '$toDate 23:59:59') ";
	}
		$query .= " and $queryDate";				
		$queryCnt .= " and $queryDate";
	
	
	
$allCount = countRecords($queryCnt);	
//$query .= "  order by transDate DESC";
$query .= " order by activity_time desc LIMIT $offset , $limit";
//debug($query);
$contentsHistory = selectMultiRecords($query);
}

//$query2 = "select * from " . TBL_LOGIN_ACTIVITIES . " where table_name ='barclayspayments'";	

	
	//$contentsHistory = selectMultiRecords($query2);
	
?>
<html>
<head>
<title>Import Payment Audit</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./styles/admin.js"></script>
<script language="javascript" src="jquery.js"></script>
<script language="javascript">

function transPrintView(){

$('#showhideTopArea').hide();
$('#showhideLowerArea').hide();
print();
$('#showhideTopArea').show();
$('#showhideLowerArea').show();

}

	$(document).ready(function(){
		<? for($count=0;$count<count($contentsHistory);$count++){?>
		$("#showHideActivitiesDetailBtn_<?=$count?>").click(function () {
		 
			if($("#showHideActivitiesDetailBtn_<?=$count?>").html() == "[-]")
			{
				$("#activitiesDetails_<?=$count?>").hide();
				$("#showHideActivitiesDetailBtn_<?=$count?>").html("[+]");
			}
			else
			{
				$("#activitiesDetails_<?=$count?>").show();
				$("#showHideActivitiesDetailBtn_<?=$count?>").html("[-]");
			}
		});
		<? }?>
		$("#limit").blur(function(){
			var page_limit = $(this);
			if(page_limit){
				var limit = page_limit.val();
				if((isNaN(limit) || limit>200 || limit<1) && limit!=""){
					alert("Please provise valid Number [ from 1-200 in digits ]");
					page_limit.val("<?php echo $limit?>");
				}
			}
		});
	});
</script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>
<style type="text/css">
<!--
.style1 {
	font-weight: bold;
	font:Verdana, Arial, Helvetica, sans-serif;
}

.style2 {
	color: #6699CC;
	font-weight: bold;
}
table td{
	font-size:10px;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5" >
  <tr> 
    <td class="topbar"><b><font color="#000000" size="2">Import Payment Audit</font></b></td>
  </tr>
  <tr id="showhideTopArea">
  	<td>
  		<table border="1" cellpadding="5" bordercolor="#666666" align="center">
        <form action="view-import-payment-activity.php" method="post" name="Search">
      <tr>
            <td width="463" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search Filters 
              </strong></span></td>
        </tr>
        <tr>
        <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$_REQUEST["fMonth"]; ?>");
        </script>
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$_REQUEST["fDay"]; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$_REQUEST["fYear"]; ?>");
        </script>
        To	<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$_REQUEST["tMonth"]; ?>");
        </script>
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$_REQUEST["tDay"]; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$_REQUEST["tYear"]; ?>");
        </script>
        <br /><br />
        No.of Records [per page] <input type="text" name="limit" id="limit" value="<?php echo $_REQUEST["limit"];?>" size="5" maxlength="3"/>
        <input type="submit" name="Submit" value="Search">
        </td>
      </tr>
	  </form>
    </table>
  	</td>
  </tr>
  <tr>
  	<td>
  		 <table width="841" cellpadding="0" bordercolor="#666666">
        <form action="view-import-payment-activity.php" method="post" name="trans">
          <?
			if ($allCount > 0){
		?>
          <tr>
            <td width="833"  bgcolor="#000000">
			<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td width="477">
                    <?php if (count($contentsHistory) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsHistory));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF ."?newOffset=0&sortBy=".$_GET["sortBy"]."&total=first&search=search&fromDate=$fromDate&toDate=$toDate&limit=".$_REQUEST["limit"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="53" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$prv&sortBy=".$_GET["sortBy"]."&total=pre&search=search&fromDate=$fromDate&toDate=$toDate&limit=".$_REQUEST["limit"]?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&total=next&search=search&fromDate=$fromDate&toDate=$toDate&limit=".$_REQUEST["limit"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="82" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&total=last&search=search&fromDate=$fromDate&toDate=$toDate&limit=".$_REQUEST["limit"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  
                  <?php }
				  } ?>
                </tr>
              </table>
  	</td>
  </tr>
  <tr> 
  	
    <td align="center"> 
    	<font size="10">
    		<table width="800" border="1" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
	        
	        <tr class="style2"> 
                <td>Dated and Time</td>
                <?php if(CONFIG_IMPORT_PAYMENT_AUDIT=="1"){?>
                   <td>Curr</td>
                    <td>Entry Date</td>
                    <td>Amount</td>
                    <td>TLA Code</td>
                    <td>Description Payment</td>
                    <td>Action</td>
                    <td>User</td>
                    <td>Transaction</td>
                    <td>Amount</td>
                <?php }else{?>
                <td>Action</td>
                <td>Description</td>
                <?php }?>
	          <!--td align="center">Show/Hide<br/>Details</td-->
	        </tr>
			<? for($k = 0;$k < count($contentsHistory); $k++){ 
					$transaction_data = "";
					$action_date = "";
					if($contentsHistory[$k]["refNumberIM"].''.$contentsHistory[$k]["accountName"]!="")
						$transaction_data = $contentsHistory[$k]["refNumberIM"].'/'.$contentsHistory[$k]["accountName"];
					if($contentsHistory[$k]["action_date"]!="")
						$action_date = date("d-m-Y",strtotime($contentsHistory[$k]["action_date"]));
					
					$action_payment = "Inserted";
					if($contentsHistory[$k]["isDeleted"]=="Y")
						$action_payment = "Deleted";
					elseif($contentsHistory[$k]["isResolved"]=="Y")
						$action_payment = "Conciliated";
					elseif($contentsHistory[$k]["isProcessed"]=="Y")
						$action_payment = "Proessed";
			?>      
				<tr class="style1">
					<td><? echo($contentsHistory[$k]["activity_time"]); /*dateFormat($contentsHistory[$k]["activity_time"],5);*/ ?>&nbsp;</td>
                    <?php if(CONFIG_IMPORT_PAYMENT_AUDIT=="1"){?>
                        <td><?=$contentsHistory[$k]["currency"]?>&nbsp;</td>						
                        <td><? echo date("d-m-Y",strtotime($contentsHistory[$k]["entryDate"]));?>&nbsp;</td>
                        <td><?=$contentsHistory[$k]["amount"]?>&nbsp;</td>
                        <td><?=$contentsHistory[$k]["tlaCode"]?>&nbsp;</td>
                        <td><?=$contentsHistory[$k]["payment_desc"]?>&nbsp;</td>
                        <td><?=$action_payment?>&nbsp;</td>
                        <td><?=$contentsHistory[$k]["username"]?>&nbsp;</td>
                        <td><?=$transaction_data?>&nbsp;</td>
                        <td><?=$contentsHistory[$k]["payment_amount"]?>&nbsp;</td>
                     <?php }else{?>
						<td><?=$contentsHistory[$k]["activity"]?>&nbsp;</td>					
						<td><?=$contentsHistory[$k]["description"]?>&nbsp;</td>
                     <?php }?>
					<!--td align="center"><a href="javascript:void(0)" id="showHideActivitiesDetailBtn_<?=$k?>" style="font-size:12px;color:#000099; font-weight:bold" title="Show Hide Activities Details">[+]</a></td-->
				</tr>
				<tr class="style1" id="activitiesDetails_<?=$k?>" style="display:none; background-color:#CCCCCC;"> 
					<td colspan="4">
							<table width="98%" cellpadding="2" cellspacing="0" align="center">
							<?php
								
									$strSearchSql = "
													select 
														t.refNumber,
														t.refNumberIM,
														h.change_log,
														h.userid,
														h.datetime
													from
														transactions as t,
														transaction_modify_history as h
													where
														t.transID = h.transid and 
														t.transID='".$contentsHistory[$k]["action_for"]."'
													";
													
									$arrSearchData = selectMultiRecords($strSearchSql);
									$arrDispalyLogData = array();
							$transDesc = strtoupper(trim($contentsHistory[$k]["description"]));
							if($transDesc=="TRANSACTION  IS UPDATED")
							{
								for($i=0 ; $i <count($arrSearchData); $i++)
								{
									if(!empty($arrSearchData[$i]["change_log"]))
									{
										
										$arrDispalyLogData[$arrSearchData[$i]["datetime"]]["userid"] = $arrSearchData[$i]["userid"];
										
										$arrChangeLog = explode("|",$arrSearchData[$i]["change_log"]);	
										//debug($arrChangeLog);
										
										foreach($arrChangeLog as $strLog)
										{
											if(!empty($strLog))
											{
												$arrDetailLog = explode(":", $strLog);
												
												$arrDispalyLogData[$arrSearchData[$i]["datetime"]][$arrDetailLog[0]]["field"] = $arrDetailLog[0];
												
												$arrDetailLogValues = explode("!", $arrDetailLog[1]);
												
												$arrDispalyLogData[$arrSearchData[$i]["datetime"]][$arrDetailLog[0]]["lastValue"] = $arrDetailLogValues[0];
												$arrDispalyLogData[$arrSearchData[$i]["datetime"]][$arrDetailLog[0]]["newValue"] = $arrDetailLogValues[1];
												
											}
										}
										
									}
								}
								
								//debug($arrDispalyLogData);
								
								/**
								 * Modify the array data, such that to store the IDs real name value
								 */
								 foreach($arrDispalyLogData as $dateTime => $arrDetailData)
								 {
									if(!empty($arrDetailData["userid"]))
									{
										$arrUserData = selectFrom("select userid, username, name, adminType from admin where userid='".$arrDetailData["userid"]."'");
										$arrDispalyLogData[$dateTime]["userid"] = $arrUserData;
									}
								 }
								 //debug($arrDispalyLogData);

								 if(count($arrSearchData) > 0)
								 {
									foreach($arrDispalyLogData as $dateTime => $arrDetailData)
									{
										$strFormatedDateTime = date("l jS M, Y H:i:s",strtotime($dateTime))." GMT";
										foreach($arrDetailData as $detailKey => $detailVal)
										{
											//$bgColor = '#cecece';
											if($bgColor == 'lightcyan')
												$bgColor = '#cecaff';
											else
												$bgColor = 'lightcyan';
											
											if($detailKey != "userid")
											{
								?>
										<tr>
											<td class="transactions" width="100%" bgcolor="<?=$bgColor?>">
												<?=$strFormatedDateTime?>&nbsp;
							
												<b><?=$arrDetailData["userid"]["name"]." / ".$arrDetailData["userid"]["username"]." / ".$arrDetailData["userid"]["adminType"]?></b>&nbsp;
												modified &nbsp;
							
												<b><?=$detailVal["field"]?></b> from
							
												<b><?=(empty($detailVal["lastValue"])?" No Value ":$detailVal["lastValue"])?></b> to
												<b><?=(empty($detailVal["newValue"])?" No Value ":$detailVal["newValue"])?></b>
											</td>
										</tr>
								<?
											}
										}
									}
								}
							}
							elseif($transDesc=="TRANSACTION  IS ADDED" || $transDesc=="TRANSACTION IS AUTHORIZED" || $transDesc=="TRANSACTION IS CANCELLED"  || $transDesc=="TRANSACTION IS AWAITING CANCELLATION")
							{
								if($transDesc=="TRANSACTION  IS ADDED"){
									$userType="addedBy";
								}
								elseif($transDesc=="TRANSACTION IS AUTHORIZED"){
									$userType="authorisedBy";								
								}
								elseif($transDesc=="TRANSACTION IS CANCELLED" || $transDesc=="TRANSACTION IS AWAITING CANCELLATION"){
									$userType="cancelledBy";								
								}		
								$strSearchSql = "
												select 
													t.refNumber,
													t.refNumberIM,
													t.addedBy,
													t.authorisedBy,
													t.transDate,
													t.authoriseDate,
													t.cancelDate,
													a.name,
													a.username,
													a.adminType
												from
													transactions as t,
													admin as a
												where
													t.".$userType." = a.name
												and 
													t.transID='".$contentsHistory[$k]["action_for"]."'
												";
												
								$arrSearchData = selectFrom($strSearchSql);
								$bgColor = '#cecaff';
								if($transDesc=="TRANSACTION  IS ADDED"){
									$strFormatedDateTime = date("l jS M, Y H:i:s",strtotime($arrSearchData["transDate"]))." GMT";
									$stFormateAction = "has Created Transaction";
								}
								elseif($transDesc=="TRANSACTION IS AUTHORIZED"){
									$strFormatedDateTime = date("l jS M, Y H:i:s",strtotime($arrSearchData["authoriseDate"]))." GMT";
									$stFormateAction = "has Authorized Transaction";
								}
								elseif($transDesc=="TRANSACTION IS CANCELLED" || $transDesc=="TRANSACTION IS AWAITING CANCELLATION"){
									$strFormatedDateTime = date("l jS M, Y H:i:s",strtotime($arrSearchData["cancelDate"]))." GMT";
									$stFormateAction = "has Cancelled Transaction";
								}
								if($arrSearchData["refNumberIM"]!="")
								{
							?>
							<tr>
								<td class="transactions" width="100%" bgcolor="<?=$bgColor?>">
									<?=$strFormatedDateTime?>&nbsp;
				
									<b><?=$arrSearchData["name"]." / ".$arrSearchData["username"]." / ".$arrSearchData["adminType"]?></b>&nbsp;
									 <?=$stFormateAction?> 
									<?=($arrSearchData["refNumberIM"]!=""?"<i>Transaction Code</i> -> <strong>".$arrSearchData["refNumberIM"]." </strong>":"")?>
									<?=($arrSearchData["refNumber"]!=""?"<i>Manual Code</i> -> <strong>".$arrSearchData["refNumberIM"]."</strong>":"")?>
								</td>
							</tr>
								<?
								}
					
							}
							else{
							?>
							<tr>
								<td class="transactions" width="100%" bgcolor="<?=$bgColor?>" colspan="4">No Details Specified in System</td>
							</tr>
						<?	}
						?>						
						</table>
					</td>
				</tr>
		  	<? }?>
      	</table>
      </font>
    </td>
  </tr>
  <tr id="showhideLowerArea">
    <td align="center">
	<div align="center">
	<?php /***ticket 9834*/ ?> 
	<?php if(defined("CONFIG_SHOW_HIDE_EXPORT_PRINT_ON_RESOLVED") && CONFIG_SHOW_HIDE_EXPORT_PRINT_ON_RESOLVED == "1") {?>
	<input type="button" name="Submit2" value="Print This Transaction" onClick="transPrintView();">
	<?php }else{ ?>
	<form name="form1" method="post" action="">                  
	<input type="submit" name="Submit2" value="Print This Transaction" onClick="print();">
	</form>
	<?php } ?>
	 
	
	
	</div>
	</td>
  </tr>
</table>
</body>
</html>