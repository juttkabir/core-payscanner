<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();

//include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$agentID = $_SESSION["loggedUserData"]["userID"];

	$OR = "";

$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
	$limit=50;
	
	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
	
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";



	
if($_POST["Submit"]!="")
		$Submit = $_POST["Submit"];
	elseif($_GET["Submit"]!="") 
		$Submit=$_GET["Submit"];
		
if($_POST["transID"]!="")
		$transID = $_POST["transID"];
	elseif($_GET["transID"]!="")
		$transID = $_GET["transID"];

if($_POST["searchBy"]!="")
		$by = $_POST["searchBy"];
	elseif($_GET["searchBy"]!="") 
		$by = $_GET["searchBy"];
//debug($_POST);
$acManagerFlagLabel = "Account Manager";
$acManagerFlag = false;
if(CONFIG_POPULATE_USERS_DROPDOWN=="1" && defined("CONFIG_ACCOUNT_MANAGER_COLUMN") && CONFIG_ACCOUNT_MANAGER_COLUMN!="0"){
	$custExtraFields = ", c.parentID as custParentID, am.name as accountManagerName ";
	$acManagerFlagLabel = CONFIG_ACCOUNT_MANAGER_COLUMN;
	$extraJoin = " LEFT JOIN ".TBL_ADMIN_USERS." as am ON am.userID = c.parentID  ";
	$acManagerFlag = true;
}

/**
 *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements 
 *  This condition executes when admin manager or branch manager logs in,
 *  keeping the check of showing the logged in users only the transactions of 
 *  their associated senders.
 */
if( defined("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS") && strstr(CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS,$agentType) )
{
   /**
	*  @var $extraCondition type string
	*  This varibale is used to define a condition 
	*/
	$extraCondition  = " and ( am.userID = '".$agentID."' ) ";
}

if($Submit == "Search")
{
	if($transID != "")
	{
		switch($by)
		{
			case 0:
			{
				$query = "select t.transDate, t.refNumberIM, t.refNumber, t.transStatus, t.totalAmount, t.transType, t.transID, t.clientRef ".$custExtraFields."  from ". TBL_TRANSACTIONS. " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where (t.refNumber = '".$transID."' OR  t.refNumberIM = '".$transID."') ".$extraCondition;
				break;
			}
			case 1:
			{
				// searchNameMultiTables function start (Niaz)
      		 $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="c";
			 $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);
			 
				$query = "select t.transDate, t.refNumberIM, t.refNumber, t.transStatus, t.totalAmount, t.transType, t.transID, t.clientRef  ".$custExtraFields." from ".TBL_TRANSACTIONS. " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$q." and t.createdBy != 'CUSTOMER' ".$extraCondition;
				//$query = "select t.transDate, t.refNumberIM, t.refNumber, t.transStatus, t.totalAmount, t.transType, t.transID  from ". TBL_TRANSACTIONS . " t, ". TBL_CUSTOMER ." c where t.customerID =  c.customerID and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%') and t.createdBy != 'CUSTOMER' ";
				
				$queryonline = "select t.transDate, t.refNumberIM, t.refNumber, t.transStatus, t.totalAmount, t.transType, t.transID from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
				
				// searchNameMultiTables function start (Niaz)
       $fn="FirstName";
			 $mn="MiddleName";
			 $ln="LastName"; 		
			 $alis="cm";
			 $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);
			 
				$queryonline .= $q;
				//$queryonline .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
				break;
			}
			case 2:
			{
				// searchNameMultiTables function start (Niaz)
             $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="b";
			 $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);
				
				$query = "select t.transDate, t.refNumberIM, t.refNumber, t.transStatus, t.totalAmount, t.transType, t.transID, t.clientRef  ".$custExtraFields." from ".TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".$extraJoin." where 1 ".$q." and t.createdBy != 'CUSTOMER' ".$extraCondition;
				$alis="cb";
			 $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);
			 
				$queryonline = "select t.transDate, t.refNumberIM, t.refNumber, t.transStatus, t.totalAmount, t.transType, t.transID from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as cb where t.benID =  cb.benID and t.createdBy = 'CUSTOMER'  ".$q." ";
                
        //$query = "select t.transDate, t.refNumberIM, t.refNumber, t.transStatus, t.totalAmount, t.transType, t.transID  from ". TBL_TRANSACTIONS . " t, ". TBL_BENEFICIARY ." b where t.benID =  b.benID and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%') and t.createdBy != 'CUSTOMER' ";
				//$queryonline = "select t.transDate, t.refNumberIM, t.refNumber, t.transStatus, t.totalAmount, t.transType, t.transID from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as cb where t.benID =  cb.benID and t.createdBy = 'CUSTOMER'  and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%')";
 
				break;
			}
			case 3:
			{
				$query = "select t.transDate, t.refNumberIM, t.refNumber, t.transStatus, t.totalAmount, t.transType, t.transID, t.clientRef ".$custExtraFields."  from ". TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
						LEFT JOIN ". TBL_ADMIN_USERS ." as u ON t.custAgentID = u.userID 
						LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
						$extraJoin."  
						where u.username ='".$transID."' ".$extraCondition;
				break;
			}

		}


				if($agentType == "SUPI" || $agentType == "SUPAI" || $agentType == "SUPA")
				{
					$query .= " and (custAgentID = '".$_SESSION["loggedUserData"]["userID"]."')";
				}
				elseif($agentType == "SUBI" || $agentType == "SUBAI" || $agentType == "SUBA")
				{
					$query .= " and (benAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR custAgentID = '".$_SESSION["loggedUserData"]["userID"]."') ";
				}elseif($agentType == 'Branch Manager')
				{
					
				   
			 
			 		$query .= " and (benAgentParentID = '".$_SESSION["loggedUserData"]["userID"]."' OR custAgentParentID = '".$_SESSION["loggedUserData"]["userID"]."') ";
						
				}

				$query .= " order by t.transDate DESC";
				$contentsTrans = selectMultiRecords($query);
				$allCount = count($contentsTrans);
				
				$query .= " LIMIT $offset , $limit";
				$contentsTrans = selectMultiRecords($query);
				
				//$found = ($contentTrans["transID"] > 0 ? "Y" : "N");
				
				
				
		
		if($by != 0 && $by != 3)
		{	
			$onlinecustomerCount = count(selectMultiRecords($queryonline));
			
			
			$allCount = $allCount + $onlinecustomerCount;
		
		
		
				$other = $offset + $limit;
		 if($other > count($contentsTrans))
		 {
		 	
			if($offset < count($contentsTrans))
			{
				$offset2 = 0;
				$limit2 = $offset + $limit - count($contentsTrans);	
			}elseif($offset >= count($contentsTrans))
			{
				$offset2 = $offset - $countOnlineRec;
				$limit2 = $limit;
			}
			$queryonline .= " order by t.transDate DESC";
		 $queryonline .= " LIMIT $offset2 , $limit2";
			$onlinecustomer = selectMultiRecords($queryonline);
		 }
		
	  }
		//}		
	}
}
//echo  "<br>" . $query;
//debug($query);
//echo "<br><br>" . count($contentsTrans);
?>
<html>
<head>
	<title>Add Promotional Code</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>

    <style type="text/css">
<!--
.style2 {color: #005b90; font-weight: bold; }
.style1 {color: #005b90}
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#000000" size="2">Transactions 
      Status</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table width="255" border="1" cellpadding="5" bordercolor="#666666">
        <form action="trans-status-search.php" method="post" name="Search">
      <tr>
            <td width="286" bgcolor="#C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
        <td align="center" nowrap>
		    <input name="transID" type="text" id="transID" size="15" style="font-family:verdana; font-size: 11px; width:100">
             <select name="searchBy" id="searchBy">
              <option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
              <option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By Sender Name</option>
              <option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
<? 		if($agentType == "admin" || $agentType == "System" || $agentType == "Call" || $agentType == "Admin Manager")
{
?>
                  <option value="3" <? echo ($_POST["searchBy"] == 3 ? "selected" : "")?>>By Agent Number</option>
<?
}
?>
             </select>
             <input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
        <form action="view-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">


          <?
			if ($allCount > 0){
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	   		 <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo (count($contentsTrans) + count($onlinecustomer))?> records to View.</span></td>
        </tr>
					<?
					if(count($contentsTrans) > 0 || count($onlinecustomer) > 0)
					{
					?>
          <tr>
            <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
                <tr bgcolor="#FFFFFF">
                  <td><span class="style1">Date</span></td>
                  <td><span class="style1"><? echo $systemCode;?></span></td>
				   <td><span class="style1"><? echo $manualCode;?></span></td>
                  <td><span class="style1">Status</span></td>
                  <td width="85" bgcolor="#FFFFFF"><span class="style1">Amount </span></td>
                  <td width="102"><span class="style1">Type</span></td>
                  <? if(CONFIG_CLIENT_REF == "1" ) { ?>
								  <td><span class="style1">Client Refrence</span></td>
								  <? } ?>
                  
                  <td width="109" align="center">&nbsp;</td>
                  <td width="74" align="center">&nbsp;</td>
                </tr>
                <? for($i=0;$i < count($contentsTrans);$i++)
			{
				?>
                <tr bgcolor="#FFFFFF">
                  <td width="100" bgcolor="#FFFFFF"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></td>
                  <td width="102" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
				  <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
                  <td width="98" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
                  <td width="85" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["totalAmount"] .  " " . $contentsTrans[$i]["currencyFrom"]?></td>

                  <td width="102" bgcolor="#FFFFFF" title=""><? echo ucfirst($contentsTrans[$i]["transType"])?></td>
                  
                  <? if(CONFIG_CLIENT_REF == "1" ) { ?>
						  		<td bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["clientRef"]?></td>
						  		<? } ?> 
                  
                  <td align="center" bgcolor="#FFFFFF"><a href="trans-status.php?transID=<? echo $contentsTrans[$i]["transID"]?>" class="style2">View Details</a></td>
                  <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $contentsTrans[$i]["transID"]?>&transStatusSearch=Y" class="style2">Enquiry</a></td>
                
                </tr>
                <?
			}
			?><? for($i=0;$i < count($onlinecustomer);$i++)
			{
				?>
                <tr bgcolor="#FFFFFF">
                  <td width="100" bgcolor="#FFFFFF"><? echo dateFormat($onlinecustomer[$i]["transDate"], "2")?></td>
                  <td width="102" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumberIM"]?></td>
				  <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$i]["refNumber"]; ?></td>
                  <td width="98" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["transStatus"]?></td>
                  <td width="85" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["totalAmount"] .  " " . $onlinecustomer[$i]["currencyFrom"]?></td>

                  <td width="102" bgcolor="#FFFFFF" title=""><? echo ucfirst($onlinecustomer[$i]["transType"])?></td>
                  <td align="center" bgcolor="#FFFFFF"><a href="trans-status.php?transID=<? echo $onlinecustomer[$i]["transID"]?>" class="style2">View Details</a></td>
                  <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $onlinecustomer[$i]["transID"]?>&transStatusSearch=Y" class="style2">Enquiry</a></td>
                </tr>
                <?
			}
			?>
                <tr bgcolor="#FFFFFF">
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="102" align="center">&nbsp;</td>
                  <td align="center">&nbsp;</td>
                  <td align="center">&nbsp;</td>
				  <td align="center">&nbsp;</td>
                </tr>
                <?
			} // greater than zero
			?>
            </table></td>
          </tr>
          <tr>
            <td  bgcolor="#000000"> 
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
             </td>
          </tr>

          <?
			} else {
		?>
          <tr>
            <td  align="center"> No Transaction found in the database.
            </td>
          </tr>
          <?
			}
		?>
		</form>
      </table>
      <?
			//} // greater than zero
			?></td>
  </tr>

</table>
</body>
</html>
