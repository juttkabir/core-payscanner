<html>
<head>
<script language="javascript">	
	function printDoc()
	{
		var row=document.getElementById("printRow");
		if (row.style.display == '')
		{
			row.style.display='none';
		}
		else 
		{
			row.style.display='';
		}	
		window.print();
	}	
</script>	
<link href="css/reports.css" rel="stylesheet" type="text/css" />
</head>


<body>
<?php
include ("../include/config.php");
include("double-entry-functions.php");
include ("security.php");
$currentDate=date ("d-m-Y");

$rateDigitsRight = false;
$roundLevel = "2";
$roundRateTo = "4";
if(defined("CONFIG_ROUND_LEVEL_RATE") && is_numeric(CONFIG_ROUND_LEVEL_RATE)){
	$rateDigitsRight = true;
	$roundRateTo = CONFIG_ROUND_LEVEL_RATE;
}
if(CONFIG_TRANS_ROUND_NUMBER == "1" && defined("CONFIG_TRANS_ROUND_LEVEL") && is_numeric(CONFIG_TRANS_ROUND_LEVEL)){
	$rateDigitsRight = true;
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}

//debug ($_REQUEST);
$sendingCurrency=$_REQUEST['sendingCurrency'];
$sending_amount = $_REQUEST["sendingAmountCB"];
$receiving_amount = $_REQUEST["receivingAmountCB"];
$transfer_amount = $_REQUEST["amount"];
$account_title=$_REQUEST['acctitle'];
$note= $_REQUEST["note"];
$sender_amount = $sending_amount - $transfer_amount;
$receiver_amount = $receiving_amount + $transfer_amount;
?>


 <table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td align="center" class="reportHeader"> AMB Exchange<br />
					<span class="reportSubHeader"><?=date("l, d F, Y")?><br />
					<br />
					</span></td></tr>
			</table>
			<br />
			<form action="" method="post">
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%" class="columnTitle" align="left">Date</td>
					<td width="10%" class="columnTitle" align="left">Currency</td>
					<td width="10%" class="columnTitle" align="left">Sending Account</td>
 					<td width="10%" class="columnTitle" align="left">Receiving Account</td>
					<td width="20%" class="columnTitle" align="left">Note</td>
					
					<!--<td width="10%" align="left" class="columnTitle">Account title</td>-->
					<td width="10%" align="left" class="columnTitle">Amount</td>
					<td width="10%" align="left" class="columnTitle">Sender's Balance</td>
					<td width="10%" align="left" class="columnTitle">Receiver's Balance</td>
				</tr>	
 				
 				
 				<tr>
 						<td>&nbsp;<?=$currentDate;?></td>
 						<td>&nbsp;<?= $sendingCurrency;?></td>
 						<td>&nbsp;<?=$_REQUEST["sendingAc"];?></td>
 						<td>&nbsp;<?= $_REQUEST["receivingAc"];?></td>
 						<td>&nbsp;<?= $note;?></td>
 						<?php /*?><td><?= $account_title;?></td><?php */?>
 						<td>&nbsp;<?= $transfer_amount ;?></td>
 						<td>&nbsp;
						<?
						 if(!empty($_REQUEST["sendingAc"]))
						 {
								$sqlSC = "SELECT id,accounNumber,accountName,currency,balanceType,balance FROM accounts WHERE status = 'AC' and accounNumber ='".$_REQUEST["sendingAc"]."' ";
								$sendingCurrData = selectMultiRecords($sqlSC);
								$sendCurrData = array();
								foreach($sendingCurrData as $k=>$v){
									$sendCurrData[] = $v['currency'];
						
									$strClosingBalance = "
											SELECT 
												id,
												closing_balance,
												dated,
												accountNumber 
											FROM 
												".TBL_ACCOUNT_SUMMARY." 
											WHERE
												accountNumber = '".$v["accounNumber"]."' AND
												dated = (select MAX(dated) as dated FROM ".TBL_ACCOUNT_SUMMARY." 
												WHERE accountNumber = '".$v["accounNumber"]."')
											 ";
										$closingBalance = selectFrom($strClosingBalance);
									
									/*
									If closing balances of any account is in - (negative) then it appear as Debit Entry in this report
									If closing balances of any account is in + (positive) then it appear as Credit Entry in this report
									*/
									//debug($accountRs[$k]['balance']);
									if(in_array($_REQUEST["sendingCurrency"],$sendCurrData)){
										if($v['balanceType'] == 'Dr')
											$sendingCBalance = ($closingBalance["closing_balance"] - $v['balance']);
										elseif($v['balanceType'] == 'Cr')
											$sendingCBalance = ($closingBalance["closing_balance"] + $v['balance']);
									}
								}
								//debug($sendingCBalance."=".$crBalance);
								/*if(count($sendCurrData)>0){
									if(in_array($_REQUEST["sendingCurrency"],$sendCurrData)){
										 $_arrInput = array(
															  "accountNumber" =>$sendingAc,
															  "currency" => $_REQUEST["sendingCurrency"]
															);
										$sendingCBalance = getClosingBalance($_arrInput);	
									}
								}*/
								echo number_format($sendingCBalance,$roundLevel,'.','');
							}
							?>
						
						</td>
						<td>&nbsp;
						<?
						 if(!empty($_REQUEST["receivingAc"]))
							{
								$sqlRC = "SELECT id,accounNumber,accountName,currency,balanceType,balance FROM accounts WHERE status = 'AC' and accounNumber ='".$_REQUEST["receivingAc"]."' ";
								$receivingCurrData = selectMultiRecords($sqlRC);
								$receiveCurrData = array();
								foreach($receivingCurrData as $k=>$v){
									$receiveCurrData[] = $v['currency'];
									$strClosingBalance = "
											SELECT 
												id,
												closing_balance,
												dated,
												accountNumber 
											FROM 
												".TBL_ACCOUNT_SUMMARY." 
											WHERE
												accountNumber = '".$v["accounNumber"]."' AND
												dated = (select MAX(dated) as dated FROM ".TBL_ACCOUNT_SUMMARY." 
												WHERE accountNumber = '".$v["accounNumber"]."')
											 ";
										$closingBalance = selectFrom($strClosingBalance);
									
									/*
									If closing balances of any account is in - (negative) then it appear as Debit Entry in this report
									If closing balances of any account is in + (positive) then it appear as Credit Entry in this report
									*/
									if(in_array($_REQUEST["receivingCurrency"],$receiveCurrData)){
										if($v['balanceType'] == 'Dr')
											$receivingCBalance = ($closingBalance["closing_balance"] - $v['balance']);
										elseif($v['balanceType'] == 'Cr')
											$receivingCBalance = ($closingBalance["closing_balance"] + $v['balance']);
									}
								}
								/*if(count($receiveCurrData)>0){
									if(in_array($_REQUEST["receivingCurrency"],$receiveCurrData)){
										$_arrInput = array(
															  "accountNumber" =>$receivingAc,
															  "currency" => $_REQUEST["receivingCurrency"]
															);
										$receivingCBalance = getClosingBalance($_arrInput);
									}
								}*/
								
								echo number_format($receivingCBalance,$roundLevel,'.','');
							}
							?>
						</td>
 				</tr>
 					
 </tr>
			</table>
			<br />
			</form>
		</td>
	</tr>
</table>
</form>
 
 <!--------------------Print Section---------------------------------->
<form name="" method="post"> 
<table align="center">
 	<tr id="printRow">
 		<td id="abc">
 			<INPUT TYPE="BUTTON" VALUE="Go Back" 
ONCLICK="window.location.href='<?=$_SERVER['HTTP_REFERER']?>'">&nbsp; &nbsp; &nbsp;<input type="button" value="Print this Page" onClick="printDoc()">
 		</td>
 	</tr>	
</table> 	
</form>
<!--------------------End here---------------------------------->
</body>
</html>