<?
session_start();
include ("../include/config.php");
$date_time = date('Y-m-d h:i:s A');
include ("security.php");
include("calculateBalance.php");
///////////////////////History is maintained via method named 'activities'

if ($_GET["fromDate"]!="")
{
	$fromDate = $_GET["fromDate"];
}
if ($_GET["toDate"]!="")
{
	$toDate = $_GET["toDate"];
}
if ($_POST["Deposit"] == "Deposit")
{
	$fromDate = $_POST["fromDate"];
	$toDate = $_POST["toDate"];
}

$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];
$_SESSION["agentID2"];
if($_GET["agent_Account"]!="")
	$_SESSION["agentID2"] = $_GET["agent_Account"];
	$today = date("Y-m-d"); 
?>
<html>
<head>
<title>Add into Teller Account</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/interface.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td class="topbar">Teller Account</td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="EEEEEE">
  <form name="form1" method="post" action="add_teller_account.php">
    <tr> 
      <td width="39%">&nbsp;</td>
      <td width="61%">&nbsp;</td>
    </tr>
    <tr> 
      <td><div align="right"><font color="#005b90">Amount &nbsp;&nbsp;</font></div></td>
      <td><input type="text" name="money">
      	<input type="hidden" name="fromDate" value="<? echo $fromDate;?>">
      	<input type="hidden" name="toDate" value="<? echo $toDate;?>">
      	</td>
    </tr>
    <tr> 
      <td><div align="right"><font color="#005b90">Note &nbsp;&nbsp;</font></div></td>
      <td><input type="text" name="note">
      	</td>
    </tr>
    <tr> 
      <td><div align="right"><font color="#005b90">Type &nbsp;&nbsp;</font></div></td>
      <td><select name="type">
      			<option value="Deposit">Deposit</option>
      			<option value="Withdraw">Withdraw</option>
      		</select>
      	</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><input type="submit" name="Submit" value="Save"></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </form>
  <?
if($_POST["Submit"] == "Save")
{/////To Withdraw from the account of a person who is depositing in somwother's account
	/*if($agentType != "admin" && $agentType != "Admin Manager")
	{
		//$balanceCal=calculateBalance($changedBy);
		$contentBenID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$changedBy."'");
		$benBalance	= $contentBenID["balance"];
		
		
		echo $bal = $benBalance - $_POST["money"];
		if($bal >= 0)
			{
				
			update("update " . TBL_ADMIN_USERS . " set balance  = $bal where userID = '".$changedBy."'");	
			$insertQuery="insert into bank_account values('', '$changedBy', '$today', 'WITHDRAW', '".$_POST["money"]."', '$changedBy', '', 'Deposit to Other Bank')";
			
			$q=insertInto($insertQuery);
			
			$insertedID = @mysql_insert_id();
			$descript = "Amount ".$_POST["money"]." is added in Teller account"; 
				activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,"bank_account",$descript);
	
			//$q=mysql_query($insertQuery);
			}
	}
	else*/{
	$bal = 1;
	}
	if($bal >= 0)
	{
		if ($_POST["type"] == "Deposit") {
		  $insertQuery="insert into teller_account(tellerID, dated, type, amount, modified_by, description, note) values('".$_SESSION["agentID2"]."', '$date_time', 'DEPOSIT', '".$_POST["money"]."', '$changedBy', 'Manually Deposited', '".$_POST["note"]."')";
			$q=insertInto($insertQuery);
			$insertedID = @mysql_insert_id();
			$descript = "Amount ".$_POST["money"]." is added in Teller account"; 
					activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,"teller_account",$descript);
	
					
			$contentBenID = selectFrom("select balance from " . TBL_TELLER . " where tellerID ='".$_SESSION["agentID2"]."'");
			$benBalance	= $contentBenID["balance"];
			$benBalance +=  $_POST["money"];
			update("update " . TBL_TELLER . " set balance  = $benBalance where tellerID = '".$_SESSION["agentID2"]."'");
		} elseif ($_POST["type"] == "Withdraw") {
		  $insertQuery="insert into teller_account(tellerID, dated, type, amount, modified_by, description, note) values('".$_SESSION["agentID2"]."', '$date_time', 'WITHDRAW', '".$_POST["money"]."', '$changedBy', 'Manually WITHDRAWN', '".$_POST["note"]."')";
			$q=insertInto($insertQuery);
			$insertedID = @mysql_insert_id();
			$descript = "Amount ".$_POST["money"]." is withdrawn in Teller account"; 
					activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,"teller_account",$descript);
	
					
			$contentBenID = selectFrom("select balance from " . TBL_TELLER . " where tellerID ='".$_SESSION["agentID2"]."'");
			$benBalance	= $contentBenID["balance"];
			$benBalance -=  $_POST["money"];
			update("update " . TBL_TELLER . " set balance  = $benBalance where tellerID = '".$_SESSION["agentID2"]."'");
		}
		
		if($q)
			{?>
		  <tr> 
			<td height="32" bgcolor="#CCCCCC" ><div align="right"><font color="<? echo SUCCESS_COLOR ?>">You 
				have successfully <? echo ($_POST["type"] == "Deposit" ? "deposited" : "withdrawn"); ?> 
				<? echo number_format($_POST["money"],2,'.',',');?>&nbsp;&nbsp;
				</font></div></td>
			<td bgcolor="#CCCCCC" align="left"><font color="#990000"> &nbsp; Now Balance 
			  is <? echo number_format($benBalance,2,'.',',');?></font> </td>
		  </tr>
		  <? }
	 }
	 else{?>
	  <tr> 
			<td height="32" bgcolor="#CCCCCC" ><div align="right"><font color="<? echo CAUTION_COLOR ?>">You 
				Can't <? echo $_POST["type"]; ?> 
				<? echo number_format($_POST["money"],2,'.',',');?>&nbsp;&nbsp;
				</font></div></td>
			<td bgcolor="#CCCCCC" align="left"><font color="#990000"> &nbsp; </font> </td>
		  </tr>
	 
	<? } 
}	 
?>

  <tr> 
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td height="30"><a href="teller_Account.php?tellerID=<?=$_SESSION["agentID2"]?>&search=search&fromDate=<?=$fromDate;?>&toDate=<?=$toDate;?>" class="style2"><font color="#000000">Go 
      Back to Teller's Account Summary</font></a></td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>