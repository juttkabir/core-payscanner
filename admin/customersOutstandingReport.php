<?
	session_start();

	include ("../include/config.php");
	include ("security.php");
	
	/**
	 * Variables from Query String
	 */
	$print				= $_REQUEST["print"];
	$currentDate		= $_REQUEST["currentDate"];
	
	/**
	 * Some pre data preparation.
	 */
	$currentDate		= (!empty($currentDate))?$currentDate:date("Y-m-d", time());

	/**
	 * Check for data validity
	 */
	// nothing till now...
	
	/**
	 * Fetch Agents Manual Withdraws
	 */
	$customersArray = array();
	
	// Fetch Total Transactions Amount
	$sql = "SELECT
				c.firstName, 
				c.middleName, 
				c.lastName, 
				c.accountName,
                                c.payinBook, 
				sum( ac.amount ) AS TOTAL
			FROM
				agent_account ac, 
				transactions t, 
				customer c
			WHERE
				ac.type = 'WITHDRAW'
				AND ac.TransID = t.transID
				AND ac.dated = '".$currentDate."' 
				AND c.customerID = t.customerID
				AND c.payinBook !=''
			GROUP BY
				c.customerID
			";
	$result = mysql_query($sql) or die(mysql_error());
	
	while($rs = mysql_fetch_array($result))
	{
		$customersArray[$rs["userID"]]["accountName"] = $rs["accountName"];
		$customersArray[$rs["userID"]]["firstName"] = $rs["firstName"];
		$customersArray[$rs["userID"]]["middleName"] = $rs["middleName"];
		$customersArray[$rs["userID"]]["lastName"] = $rs["lastName"];
		$customersArray[$rs["userID"]]["TOTAL"] = abs($rs["TOTAL"]);
		$customersArray[$rs["userID"]]["RECEIVED"] = 0;
	}
	
	// Fetch Received Amount
	$sql = "SELECT
				c.firstName, 
				c.middleName, 
				c.lastName, 
				c.accountName,
                                c.payinBook,
				sum( ac.amount ) AS RECEIVED
			FROM
				agent_account ac, 
				transactions t, 
				customer c
			WHERE
				ac.type = 'DEPOSIT'
				AND ac.TransID = t.transID
				AND ac.dated = '".$currentDate."' 
				AND c.payinBook !=''
				AND c.customerID = t.customerID
			GROUP BY
				c.customerID
			";
	$result = mysql_query($sql) or die(mysql_error());
	
	while($rs = mysql_fetch_array($result))
	{
		$customersArray[$rs["userID"]]["accountName"] = $rs["accountName"];
		$customersArray[$rs["userID"]]["firstName"] = $rs["firstName"];
		$customersArray[$rs["userID"]]["middleName"] = $rs["middleName"];
		$customersArray[$rs["userID"]]["lastName"] = $rs["lastName"];
		
		if(empty($customersArray[$rs["userID"]]["TOTAL"]))
		{
			$customersArray[$rs["userID"]]["TOTAL"] = 0;
		}
		
		$customersArray[$rs["userID"]]["RECEIVED"] = abs($rs["RECEIVED"]);
	}
	
	$tmpTotalTransactionAmount = 0;
	$tmpTotalReceivedAmount = 0;
	$tmpTotalOutstandingAmount = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Customers Outstanding Report</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
    <tr>
        <td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td align="center" class="reportHeader">Customers Outstanding Report <br />
						<span class="reportSubHeader">
							<?=date("l, d F, Y", strtotime($currentDate))?>
							<br />
							<br />
						</span>
					</td>
				</tr>
			</table>
			<br />
			<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
				<tr>
					<td width="50%" class="columnTitle">Customer Name</td>
					<td width="20%" class="columnTitle">Customer Number</td>
					<td width="10%" align="right" class="columnTitle">Transaction Amount  </td>
					<td width="10%" align="right" class="columnTitle">Received Amount  </td>
					<td width="10%" align="right" class="columnTitle">Outstanding Amount </td>
				</tr>
				<?
					foreach($customersArray as $key)
					{
				?>
						<tr>
							<td><?=$key["firstName"]." ".$key["middleName"]." ".$key["lastName"]?></td>
							<td><?=$key["accountName"]?></td>
							<td align="right"><?=number_format(abs($key["TOTAL"]), 2, ".", ",")?></td>
							<td align="right"><?=number_format($key["RECEIVED"], 2, ".", ",")?></td>
							<td align="right"><?=number_format(abs($key["TOTAL"] - $key["RECEIVED"]), 2, ".", ",")?></td>
						</tr>
				<?	
						$tmpTotalTransactionAmount += (abs($key["TOTAL"]));
						$tmpTotalReceivedAmount += (abs($key["RECEIVED"]));
						$tmpTotalOutstandingAmount += (abs($key["TOTAL"] - $key["RECEIVED"]));
					}
				?>
				
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				
				<tr>
					<td class="netBalance">Totals</td>
					<td>&nbsp;</td>
					<td align="right" class="netBalance"><strong>
						<?=number_format($tmpTotalTransactionAmount, 2, ".", ",")?>
					</strong></td>
					<td align="right" class="netBalance"><strong>
						<?=number_format($tmpTotalReceivedAmount, 2, ".", ",")?>
					</strong></td>
					<td align="right" class="netBalance" id="closingFinal">
						<?=number_format($tmpTotalOutstandingAmount, 2, ".", ",")?>
					</td>
				</tr>
			</table>
			<br />
			<?
				if($print != "Y")
				{
			?>
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td width="50%" class="reportToolbar"><input name="btnPrint" type="button" id="btnPrint" value=" Print " onclick="javascript: location.href='customersOutstandingReport.php?print=Y&amp;currentDate=<?=$currentDate?>';" /></td>
							<td width="50%" align="right" class="reportToolbar">&nbsp;</td>
						</tr>
					</table>
			<?
				}
			?>
		</td>
    </tr>
</table>
<?
	if($print == "Y")
	{
?>
		<script type="text/javascript">
			print();
		</script>
<?
	}
?>
</body>
</html>
