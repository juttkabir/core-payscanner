<?

session_start();
include ("../include/config.php");

/*
require_once "../include/set_env.php";
require_once "../include/DataObjects/Config.php";
$config_logo = &DataObjects_Config::category_array('logo');
$width = $config_logo['width'];
$height = $config_logo['height'];
$path = $config_logo['file_name'];
*/
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$nowTime = date("F j, Y");
$transID = $_GET["transID"];
$today = date("d-m-Y");

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
}

if($_GET["r"] == 'dom')///If transaction is domestic
{
	$returnPage = 'add-transaction-domestic.php';
}else{
	$returnPage = $transactionPage;
}
$buttonValue = $_GET["transSend"];	

if(isset($_REQUEST["checkbox"]) && sizeof($_REQUEST["checkbox"]) > 0)
{
	for($i=0; $i<sizeof($_REQUEST["checkbox"]); $i++)
	{
		$transID = $_REQUEST["checkbox"][$i];
		$queryTransaction[$i] = selectFrom("select *  from ".TBL_TRANSACTIONS." where transID ='" . $transID . "'");
		
		if($queryTransaction[$i]["custAgentID"] != '')
		{
			$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $queryTransaction[$i]["custAgentID"] . "'";
			$custAgent=$queryTransaction[$i]["custAgentID"];
			$senderAgentContent = selectFrom($querysenderAgent);
			$custAgentParentID = $senderAgentContent["parentID"];
		}	
	}
} 
else 
{
	if($transID!='')
	{
		$queryTransaction[0] = selectFrom("select *  from ".TBL_TRANSACTIONS." where transID ='" . $transID . "'");
	}
	
	if($queryTransaction[0]["custAgentID"] != '')
	{
		$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $queryTransaction[0]["custAgentID"] . "'";
		$custAgent=$queryTransaction[0]["custAgentID"];
		$senderAgentContent = selectFrom($querysenderAgent);
		$custAgentParentID = $senderAgentContent["parentID"];
	}
}

// #3938 added by ashahid
function currencyCheckType($currTo,$currFrom)
{
	$compareCurrency = false;
	if(CONFIG_TRANS_ROUND_CURRENCY_TYPE == 'CURRENCY_TO')
	{
		if(strstr(CONFIG_TRANS_ROUND_CURRENCY, $currTo.",") || CONFIG_TRANS_ROUND_CURRENCY == "" )
			$compareCurrency = true;	
	}elseif(CONFIG_TRANS_ROUND_CURRENCY_TYPE == 'CURRENCY_FROM')
	{
		if(strstr(CONFIG_TRANS_ROUND_CURRENCY, $curryFrom.",") || CONFIG_TRANS_ROUND_CURRENCY == "" )
			$compareCurrency = true;	
	}else
	{
		if((strstr(CONFIG_TRANS_ROUND_CURRENCY, $currTo.",")) || (strstr(CONFIG_TRANS_ROUND_CURRENCY, $curryFrom.",")))
			$compareCurrency = true;	
	}
	return $compareCurrency;	
}
// #3938 added by ashahid
function RoundValueTo($amount,$moneyPaid,$currTo,$currFrom){
	$newValue = $amount;
	$checkCurrency = currencyCheckType($currTo,$currFrom);
	if(CONFIG_TRANS_ROUND_NUMBER >0 && $checkCurrency){
		if(strstr(CONFIG_ROUND_NUM_FOR, $moneyPaid.",") && compareCurrency2){
			$newValue = round($amount,CONFIG_TRANS_ROUND_LEVEL);
		}
		else{
			$newValue = round($amount,4);
		}
	}
	return $newValue;
}
//debug($senderAgentContent);
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<title>Untitled Document</title>

<style type="text/css">
#table
{
font-family:"Times New Roman", Times, serif;
font-size:11px;


} 

.centerAligned{

text-align:center;
}

.rightAligned{

text-align:right;
}
.leftAligned{

text-align:left;

}

.webAdd{
font-family:Geneva, Arial, Helvetica, sans-serif;
font-size:10px;
}
.largeFont{

font-size:18px;
}

</style>

</head>
<body <? if(CONFIG_SKIP_CONFIRM == '1'&& $buttonValue != ""){ ?> onLoad="print()"<? } ?>>
<?
	for($i=0; $i<sizeof($queryTransaction); $i++)
	{
	// #3938 added by ashahid

$totalAmountProcessed = RoundValueTo($queryTransaction[$i]["totalAmount"],$queryTransaction[$i]["moneyPaid"],$queryTransaction[$i]["currencyTo"],$queryTransaction[$i]["currencyFrom"]);
$transAmountProcessed = RoundValueTo($queryTransaction[$i]["transAmount"],$queryTransaction[$i]["moneyPaid"],$queryTransaction[$i]["currencyTo"],$queryTransaction[$i]["currencyFrom"]);
$localAmountProcessed = RoundValueTo($queryTransaction[$i]["localAmount"],$queryTransaction[$i]["moneyPaid"],$queryTransaction[$i]["currencyTo"],$queryTransaction[$i]["currencyFrom"]);

?>
	<table border="1" bordercolor="#4C006F" cellspacing="0" cellpadding="0" width="225">
	<tr>
	<td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0"  id="table">
		 <tr >
			   <td colspan="4" align="center" bgcolor="#FFFFFF">
				
				<?
				
				//printLogoOnReciept($queryTransaction["custAgentID"],$_SESSION["loggedUserData"]["username"]);
				/*
						if($queryTransaction[$i]["custAgentID"] !='')
								{
									$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where username ='" . $_SESSION["loggedUserData"]["username"] . "'";
									$senderAgentContent = selectFrom($querysenderAgent);
								} 
					*/													
								if($senderAgentContent["logo"] != "" && file_exists("../logos/" . $senderAgentContent["logo"]))
								{
										$arr = getimagesize( "../logos/" .$senderAgentContent["logo"]);
										
										$width = ($arr[0] > 100 ? 100 : $arr[0]);
										$height = ($arr[1] > 50 ? 50 : $arr[1]);
							
									echo "<img src='../logos/".$senderAgentContent["logo"]."' width='$width' height='$height'>";
								}
								else
								{
									echo "<img height='50' alt='' src='".CONFIG_LOGO."' width='100'>";
								}
				
				
				?>
			   </td>
	   </tr>
	  <tr>
		<td  scope="col"><B>Order No </td>
		<td  scope="col">&nbsp;<? echo $queryTransaction[$i]["refNumberIM"]; ?></td>
		<td  scope="col">&nbsp;</td>
	  <td width="6%" scope="col">&nbsp;</td>  </tr>
	  <tr>
			<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><B>Date</td>
		<td>&nbsp;<? $date=$queryTransaction[$i]['transDate'];
				$transDate = $queryTransaction[$i]["transDate"];
				echo dateFormat(date('Y-m-d',strtotime($transDate)));
			 ?>
			</td>
		
	  </tr>
	  <tr>
		
		<td><B>Teller</td>
		<td><? echo $queryTransaction[$i]["addedBy"]; ?></td>
		<td><B>&nbsp;Time</td>
		<td>&nbsp;<? $transTime = $queryTransaction[$i]["transDate"];
			echo date('h:i A',strtotime($transTime));
			?></td>
	  </tr>
	  <tr>
		<td><B>Branch</td>
		<td colspan="3"><? echo  $senderAgentContent["name"]; ?></td>
		
	  </tr>
	  <tr>
		<td colspan="4"><hr noshade="noshade" size="1" width="100%" /></td>
	  </tr>
	  <? $queryCust = "select customerID, Title, firstName, lastName, middleName, accountName, Address, Address1, City, State, Zip,Country, Phone, Email,dob,IDType,IDNumber, remarks  from ".TBL_CUSTOMER." where customerID ='" . $queryTransaction[$i]["customerID"] . "'";
				$customerContent = selectFrom($queryCust); ?>
	  <tr>
		<td><B><i>Customer</td>
		<td colspan="3">&nbsp;<i><? echo $customerContent["firstName"]."  ".$customerContent["middleName"]."  ".$customerContent["lastName"];  ?></td>
	  </tr>
	  <tr>
		<td>Sender Number</td>
		<td colspan="3">&nbsp;(<?=$customerContent["accountName"]?>)</td>
	  </tr>
	   <tr>
		<td colspan="4"><hr noshade="noshade" size="1" width="100%" /></td>
	  </tr>
	  
	  <? 	$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $queryTransaction[$i]["benID"] . "'";
				$benificiaryContent = selectFrom($queryBen); ?>
	  <tr>
		<td><B><i>Beneficiary</td>
		<td colspan="3">&nbsp;<i><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"];  ?></td>
		
	  </tr>
	  <tr>
		
		<td colspan="2"><B>
		<? 
		/*****
			#5209 - Minas Center
					CPF and CNPJ are displayed on the basis of User Type for Brazil
		*****/
		if($benificiaryContent["userType"]=="Business User"){
			echo  "CNPJ ";
		}
		else{
			echo  "CPF ";
		}
		?>
		</b> <? echo $benificiaryContent["CPF"]; ?></td>
		<td colspan="2"><B>Country</b> <? echo $queryTransaction[$i]["toCountry"]; ?></td>
		
	  </tr>
	  <tr>
		<td colspan="4"><hr noshade="noshade" size="1" width="100%" /></td>
	  </tr>
	  
	 
		<?  if($queryTransaction[$i]["transType"] == "Bank Transfer")
				  {
					$benBankDetails = selectFrom("select * from ". TBL_BANK_DETAILS ." where transID='".$transID."'"); ?>
		 <tr>
		<td><B>Bank</td>
		<td class="leftAligned" colspan="3">&nbsp;<? echo $benBankDetails["bankName"]; ?></td>
		
	  </tr>
	  <tr>
		<td><B>Sort Code </td>
		<td class="leftAligned" colspan="3">&nbsp;<? echo $benBankDetails["branchCode"]; ?></td>
		
	  </tr>
	  
	  
	  <tr>
		<td><B>Account </td>
		<td class="leftAligned" colspan="3">&nbsp;<? echo $benBankDetails["accNo"]; ?></td>
		
	  </tr>
	   <tr>
		
		<td class="rightAligned" colspan="4">&nbsp;<? echo $benBankDetails["accountType"]." "; ?>Account</td>
		
	  </tr>
	  <? }elseif($queryTransaction[$i]["transType"] == "Pick up")
				  {
			  $queryCp = "select *  from cm_collection_point where  cp_id  ='" . $queryTransaction[$i]["collectionPointID"] . "'";
				$collectionPoint = selectFrom($queryCp);	
				$queryDistributor = "select name  from " .TBL_ADMIN_USERS. " where  userID  ='" . $senderAgentContent["cp_ida_id"] . "'";
				$queryExecute = selectFrom($queryDistributor);		  ?>
			  
			 <tr>
		<td><B>Collection point name </td>
		<td class="leftAligned" colspan="3">&nbsp;<? echo $collectionPoint["cp_branch_name"]; ?></td>
		
	  </tr>
	  
	  
	  <tr>
		<td><B>Branch </td>
		<td class="leftAligned" colspan="3">&nbsp;<? echo $collectionPoint["cp_branch_address"]; ?></td>
		
	  </tr>
	   <tr>
		
		<td class="rightAligned" colspan="4">&nbsp;Tel:<? echo $collectionPoint["cp_phone"]."Fax".$collectionPoint["cp_fax"]; ?></td>
		
	  </tr>
			  
			  <? } ?>
			  
	  <tr>
		<td colspan="4"><hr width="100%" /></td>
	  </tr>
	  <tr>
		
		<td class="rightAligned" colspan="2"><B>Local Country </td>
		<td colspan="2">&nbsp;<?  echo $queryTransaction[$i]["fromCountry"];?></td>
		
	  </tr>
	  <tr>
		<td class="centerAligned"><B>Currency local To </td>
		<td><?  echo $queryTransaction[$i]["currencyTo"];?></td>
		<td><B>Received Mode </td>
		<td><? echo	$queryTransaction[$i]["moneyPaid"];?></td>
	  </tr>
	 <tr>
		<td colspan="4"><hr noshade="noshade" size="1" width="100%" /></td>
	  </tr>
	  <tr>
		<td class="rightAligned" colspan="2"><B>Amount Paid </td>
		<td colspan="2" class="rightAligned">&nbsp;<? echo $queryTransaction[$i]["currencyFrom"]." ".$transAmountProcessed;?></td>
	   
	  </tr>
	  <tr>
		<td class="rightAligned" colspan="2"><B>Fee</td>
		<td colspan="2" class="rightAligned">&nbsp;<?  echo $queryTransaction[$i]["IMFee"];?></td>
		
	  </tr>
	  <tr>
		<td colspan="2" class="rightAligned"><B>Currency Total </td>
		<td colspan="2" class="rightAligned">&nbsp;<? echo $queryTransaction[$i]["currencyFrom"]." ".$totalAmountProcessed;?></td>
		
	  </tr>
	  <tr>
		<td class="rightAligned" colspan="2"><B>Exchange Value </td>
		<td colspan="2" class="rightAligned">&nbsp;<?  echo $queryTransaction[$i]["exchangeRate"];?></td>
		
	  </tr>
	  <? if($queryTransaction[$i]["bankCharges"] != "" && $queryTransaction[$i]["bankCharges"] != '0'){ ?>
	   <tr>
		<td class="rightAligned" colspan="2"><B>Bank Charges </td>
		<td colspan="2" class="rightAligned">&nbsp;<?  echo $queryTransaction[$i]["bankCharges"];?></td>
		
	  </tr>
	  <? } ?>
	  <tr>
		
		<td colspan="2" class="rightAligned"><B>Amount To Pay </td>
		<td class="centerAligned"><B>R$</td>
		<td class="rightAligned">&nbsp;<?  echo $queryTransaction[$i]["currencyTo"]." ".$localAmountProcessed;?></td>
		
	  </tr>
	  
	  <tr>
		<td colspan="4" align="center">
			<?
				if(CONFIG_CLIENT_REF == "1")
					$textToDisplay = "Remarks:".stripslashes($queryTransaction[$i]["clientRef"]);
				else
					$textToDisplay = "Free Notice Field";
			?>
		  <textarea name="ff" cols="20" rows="2" readonly="readonly"><?=$textToDisplay?></textarea>    </td>
	  </tr>
	  <tr>
		<td class="rightAligned">Tel:</td>
		<td colspan="3" class"largeFont"><b>020 8965 0116 / 020 8965 6588 </td>
	  </tr>
	  <tr>
		<td colspan="4" class="centerAligned webAdd">WWW.CENTERUNION.CO.UK</td>
	  </tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="3" id="table">
	  <tr>
	   
		<td scope="col" colspan="4" class="centerAligned" ><img id=Picture3 height="20" alt="" src="<?=CONFIG_OTHER_LOGO ?>" width="150" border=0>
	   </td>
	  </tr>
	  <tr>
		<td colspan="2" height="50" valign="bottom">_____________<br>
		  Customer signature </td>
		<td colspan="2" valign="bottom" class="centerAligned">______________<br>
		Authorized </td>
	  </tr>
	  
	</table>
	</tr>
	</td>
	
</table>
	
	<hr noshade="noshade" size="1" />
<?
	}
?>
<!-- remove this line -->
<table width="300">
<tr><td colspan="4">&nbsp;</td></tr>
  <tr>
            <td align="center" colspan="4">
		<div class='noPrint'>
			<input type="button" name="Submit2" value="Print this Receipt" onClick="print()">
			&nbsp;&nbsp;&nbsp;<a href="<?=$returnPage?>?create=Y" class="style2"><font size="2">Go to Create Transaction</font></a>
		
		</div>
			</td>
    </tr>
</table>
</body>
</html>
