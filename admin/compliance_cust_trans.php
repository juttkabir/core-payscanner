<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("javaScript.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

if(CONFIG_TRANS_ROUND_LEVEL != '')
{
	$roundLeve = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLeve = 4;
	}

$_SESSION["from1"] = "" ;
$_SESSION["to1"] = "" ;


/**
 * Fetching the Old transaction data for customer 
 * @Ticket #4185
 */
$bolViewOldTransactions = false;
if(CONFIG_COMPLIANCE_SENDER_TRANS == '1' && !empty($_REQUEST["customer"]))
{
	$strGroupRightViewSql = "select * from oldTransactionGroupRight where `group`='".getAgentType()."' and `haveRight`='Y'"; 
	$arrGroupRightViewData = selectFrom($strGroupRightViewSql);

	if(is_array($arrGroupRightViewData))
	{
		$bolViewOldTransactions = true;
		
		$to = getCountryTime(CONFIG_COUNTRY_CODE);
		
		$fromData = strtotime("-".$arrGroupRightViewData["backDays"]." days");
		/* formating the FROM date */		
		$from = date("Y",$fromData)."-".date("m",$fromData)."-".date("d",$fromData)." 00:00:00";
	}
	else
	{
		echo "<h2 align='center'>You does not have sufficient rights to view.</h2>";
		exit;
	}
}



$agentID_id = $_SESSION["loggedUserData"]["userID"];
// define page navigation parameters
if ($offset == "")
	$offset = 0;
$limit=20;

//$selection = " created, Title, firstName, middleName, lastName, accountName, Country, email,  transID,  ";


if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = "created";
$to = getCountryTime(CONFIG_COUNTRY_CODE);
/*
$month = substr($to,5,2);
$year = substr($to,0,4);
$day = substr($to,8,2);
if($month > 1)
{
	$month --;
	if($month < 10)
		$month = "0".$month;
}else{
	$month = 12;
	$year = $year - 1;
	}

 $from = $year."-".$month."-".$day." 00:00:00";
*/	
	if($_POST["Submit"] != "")
	{
		$_SESSION["from"] = $_POST["from"];
		$_SESSION["to"] = $_POST["to"];
		$_SESSION["from1"] = $_POST["from"];	// only for date formate display
		$_SESSION["to1"] = $_POST["to"];		// only for date formate display

			$dDate = explode("/",$_SESSION["from"]);
			if(count($dDate) == 3)
				$from = $dDate[2]."-".$dDate[1]."-".$dDate[0];
			else
				$from = $_SESSION["from"];
				
			$dDate2 = explode("/",$_SESSION["to"]);
			if(count($dDate2) == 3)
				$to = $dDate2[2]."-".$dDate2[1]."-".$dDate2[0];
			else
				$to = $_SESSION["to"];
				
		 $from = $from." 00:00:00";
		 $to = $to." 23:59:59";
		
	}
	
	$query = "select * from ".TBL_CUSTOMER." where customerID = '".$_GET["customer"]."'";
	
			 
	
$customer = selectFrom($query);
$allCount = count($customer);
$contentsTrans = selectMultiRecords("SELECT transID, refNumberIM, totalAmount,transAmount, transDate, benID, currencyFrom, addedBy FROM " . TBL_TRANSACTIONS . " where customerID = '".$customer["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled' ");
$countTrans = selectFrom("select count(transID) as transCount, sum(transAmount) as totalAmount from ".TBL_TRANSACTIONS." where customerID = '".$customer["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled' ");

//debug("SELECT transID, refNumberIM, totalAmount,transAmount, transDate, benID, currencyFrom, addedBy FROM " . TBL_TRANSACTIONS . " where customerID = '".$customer["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled' ");
?>
<html>
<head>
	<title>Categories List</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
	function showDetails(str) {
		if (str == 'show') {
			document.getElementById('transDetails').style.display = 'table';
		} else {
			document.getElementById('transDetails').style.display = 'none';
		}
	}
	function checkForm(theForm) {
		var a = false;
		var none = true;
		var j = 2; 
		for(i =0; i < <?=count($customer); ?>; i++){
			if(theForm.elements[j].checked == true){
				a = confirm("Are you sure you want to Disable the selected Customer?");
				none = false;
				break;
			}
			j++;
		}
		if (none) {
			a = false;
			alert("No Customer Range(s) is selected for Disable.")
		}
		return a;
	}
	function checkAllFn(theForm, field) {
		if (field == 1) {
			if (theForm.del.value == 0)
				theForm.del.value = 1;
			else
				theForm.del.value = 0;
			if (theForm.del.value == 1){
				j=2;
				for(i=0; i < <?=count($customer);?>; i++) {
					theForm.elements[j].checked = true;
					j++;
				}
			} else {
				j = 2;
				for(i=0; i< <?=count($customer);?>; i++) {
					theForm.elements[j].checked = false;
					j++;
				}
			}
		} else {
			if (theForm.app.value == 0)
				theForm.app.value = 1;
			else
				theForm.app.value = 0;
			if (theForm.app.value == 1){
				j = 3;
				for(i=0; i<<?=count($customer);?>; i++) {
					theForm.elements[j].checked = true;
					j = j+2;
				}
			} else {
				j = 3;
				for(i=0; i<<?=count($customer);?>; i++) {
					theForm.elements[j].checked = false;
					j = j+2;
				}
			}
		}
	}
	function hidePrintDivs(){
		document.getElementById('showHideDetails').style.display='none';
		document.getElementById('printDiv').style.display='none';
	}
	// end of javascript -->
	</script>
    <style type="text/css">
<!--
.style2 {color: #6699CC;
	font-weight: bold;
}
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr class="topbar"> 
    <td colspan="2"><strong><font color="#000000" size="2">Customer Transaction Compliance</font></strong></td>
  </tr>
  <tr>
    <td align="center">
		<table width="100%"  border="0">
          <tr>
            <td><fieldset>
              <legend class="style2">Search  Registration Between Dates </legend>
              <br>
			  
              <table width="95%" border="0" align="center" cellpadding="5" cellspacing="1">
                <form name="search" action="compliance_cust_trans.php?customer=<?=$_GET["customer"]?>" method="post"><tr>
				
                  <td align="center" bgcolor="#DFE6EA"><font color="#005b90">From Date </font><font color="#005b90"> 
		    		    <input name="from" type="text" id="from"  value="<? echo $_SESSION["from1"];?>" readonly>
		    		    &nbsp;<a href="javascript:show_calendar('search.from');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp; &nbsp;To Date&nbsp;
		    		    <input name="to" type="text" id="to"  value="<? echo $_SESSION["to1"];?>" readonly>
		    		    &nbsp;<a href="javascript:show_calendar('search.to');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;                    
						<input type="submit" name="Submit" value="Search" class="flat">
</font></td>
                </tr></form>
              </table>
                          <br>
            </fieldset></td>
          </tr>
        </table>
		<br>
		  <form action="compliance_cust_trans.php?customer=<?=$_GET["customer"]?>" method="post" onSubmit="return checkForm(this);">
 		<table width="800" border="0" cellspacing="1" cellpadding="1" align="center">
          <tr bgcolor="#DFE6EA"> 
        <td width="230"><font color="#005b90"><strong>Transaction Period</strong></font></td>
            <td width="70" align="center"><font color="#005b90"><strong>Customer 
              ID </strong></font></td>    
            <td width="150"><font color="#005b90"><strong>Customer Name</strong></font></td>
         <?
         if(CONFIG_COMPLIANCE_COUNTRY_REMOVE != '1')
         {
        	?>   
            <td width="100" align="left"><font color="#005b90"><strong>Country</strong></font></td>
          <?
         }
         ?>  
			<td width="100" align="left"><font color="#005b90"><strong>Registration Date</strong></font></td>
			<td width="50" align="left"><font color="#005b90"><strong>Total Transactions</strong></font></td>
			<td width="100"  align="center"><font color="#005b90"><b>Total Amount</b></font></td>			
          </tr>
          <?
			if($customer["customerID"] != ""){
		?>
          <tr valign="top" bgcolor="#eeeeee"> 
          	<td align="center"><? echo( dateFormat($from, "2")."  To  ". dateFormat($to, "2"))?></td>
            <td align="center"><? echo $customer["accountName"]; ?></td>
            <td><? echo $customer["Title"]." ".$customer["firstName"]." ".$customer["lastName"]; ?></td>
          <?
         if(CONFIG_COMPLIANCE_COUNTRY_REMOVE != '1')
         {
        	?>   
           
            <td align="left"><? echo stripslashes($customer["Country"]); ?> </td>
          <?
        	}
          ?>  
			<td align="left"><? echo stripslashes($customer["created"]); ?> </td>
			<td align="left"><? echo stripslashes($countTrans["transCount"]); ?> </td>
			<td align="center"><? echo stripslashes(round($countTrans["totalAmount"], $roundLeve)); ?> </td>	  					
          </tr>
          <?
			}
		?>
		</table>
	<br>
	<div id="showHideDetails" style="display:inline;">
	<a href="#" onClick="showDetails('show');" class="style2">Show Details</a>&nbsp;&nbsp;&nbsp;&nbsp;
	<a href="#" onClick="showDetails('hide');" class="style2">Hide Details</a>
	</div>
		<br><br>
		  	<table id="transDetails" width="800" border="0" cellspacing="1" cellpadding="1" align="center" style="display:none">
          <tr bgcolor="#DFE6EA"> 
          	<td align="center"><font color="#005b90"><strong>Serial Number </strong></font></td>
          <?
          if(CONFIG_COMPLIANCE_CUSTOM_COLUMN == '1')
          {
          ?>	
          	<td align="center"><font color="#005b90"><strong>Create Date </strong></font></td>
          <?
        	}
          ?>	
            <td align="center"><font color="#005b90"><strong>Transaction Number </strong></font></td>
             <?
          if(CONFIG_COMPLIANCE_CUSTOM_COLUMN == '1')
          {
          ?>	
          	<td align="center"><font color="#005b90"><strong>City </strong></font></td>
          	<td align="center"><font color="#005b90"><strong>Beneficiary Name </strong></font></td>
          <?
        	}
          ?>	
			<td align="center"><font color="#005b90"><strong>Amount Sent</b></font></td>			
			<?
          if(CONFIG_COMPLIANCE_CUSTOM_COLUMN == '1')
          {
          ?>	
          	<td align="center"><font color="#005b90"><strong>Currency </strong></font></td>
          	<td align="center"><font color="#005b90"><strong>Created By </strong></font></td>
          <?
        	}
          ?>	
          </tr>
   	<?	
   		if (count($contentsTrans) > 0) {
   			for ($i = 0; $i < count($contentsTrans); $i++) {
   	?>
          <tr valign="top" bgcolor="#eeeeee"> 
          	<td align="center"><? echo ($i + 1) ?></td>
          	<?
          if(CONFIG_COMPLIANCE_CUSTOM_COLUMN == '1')
          {
          ?>	
          	<td align="center"><? echo(dateFormat($contentsTrans[$i]["transDate"], "2"));?></td>
          <?
        	}
          ?>	
            <td align="center"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TransDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')" class="style2"><? echo stripslashes($contentsTrans[$i]["refNumberIM"]); ?></a> </td>
          <?
          if(CONFIG_COMPLIANCE_CUSTOM_COLUMN == '1')
          {
          ?>	
          	<td align="center"><?=$customer["City"]?></td>
          	<td align="center"><?
          		
          							$benContent = selectFrom("Select Title, firstName, middleName, lastName from ".TBL_BENEFICIARY." where benID = '".$contentsTrans[$i]["benID"]."'");
          							echo($benContent["Title"]." ".$benContent["firstName"]." ".$benContent["middleName"]." ".$benContent["lastName"]);						
          		?>
          	</td>
          <?
        	}
          ?>	  	
				<td align="center"><? echo stripslashes(round($contentsTrans[$i]["transAmount"], 4)); ?> </td>	
				<?
          if(CONFIG_COMPLIANCE_CUSTOM_COLUMN == '1')
          {
          ?>	
          	<td align="center"><?=$contentsTrans[$i]["currencyFrom"]?></td>
          	<td align="center"><?=$contentsTrans[$i]["addedBy"]?></td>
          <?
        	}
          ?>	
			
			  					
          </tr>
 	<?	
    		}
 		} else {
 	?>
          <tr valign="top" bgcolor="#eeeeee"> 
          	<td align="center" colspan="3"><i>No transactions found between <? echo( dateFormat($from, "2")."  To  ". dateFormat($to, "2"))?></i></td>
          </tr>
  <? } ?>
  		<? if(defined("CONFIG_PRINT_SENDER_TRANS_SUMMARY") && CONFIG_PRINT_SENDER_TRANS_SUMMARY=="1"){?>
          <tr valign="top"> 
          	<td align="center" colspan="8">&nbsp;
			</td>
		 </tr>
          <tr valign="top" bgcolor="#eeeeee" id="printDiv"> 
          	<td align="center" colspan="8">
  				<div align="center">
					<input type="button" name="Submit2" value="Print This Report" onClick="hidePrintDivs();print()">
				</div>
			</td>
		</tr>
		<? }?>
 		</table>
</form>
	</td>
	<td>&nbsp;</td>
  </tr>

</table>
</body>
</html>