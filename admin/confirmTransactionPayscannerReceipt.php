<?php

session_start();
include ("../include/config.php");
include ("security.php");
/* ini_set("display_errors","1");
error_reporting("1"); */
require_once("lib/audit-functions.php");

$modifyby = $_SESSION["loggedUserData"]["userID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$today = date('Y-m-d');//date("d-m-Y");
$loggedUName = $_SESSION["loggedUserData"]["name"];
$returnPage = 'add-transaction.php';

$_SESSION["start_date"] =$_POST["start_date"];
$_SESSION["end_date"] =$_POST["end_date"];


/* echo $_SESSION["start_date"];
echo $_SESSION["end_date"]; */
/*
get values for Recurring Rule 
*/

$_SESSION["DateMonth"]="";

$_SESSION["RecuringCheckbox"] = $_POST["RecuringCheckbox"];
$_SESSION["ConfirmRecurring"] = $_POST["ConfirmRecurring"];


//echo $_SESSION["ConfirmRecurring"];

if($_SESSION["RecuringCheckbox"] =="on" && $_SESSION["ConfirmRecurring"]== "ConfirmRecurring")
{ 
//$_SESSION["scheduleOptions"] =$_POST["scheduleOptions"] ;
if($_POST["scheduleOptions"]=="oneTimeTransfer")
{
$_SESSION["scheduleOptions"]="Y";
$_SESSION["start_date"] =$_POST["oneTime"];
$_SESSION["end_date"] =$_POST["oneTime"];


}
else 
{
$_SESSION["scheduleOptions"]="N";
$_SESSION["start_date"] =$_POST["start_date"];
$_SESSION["end_date"] =$_POST["end_date"];
}

if($_POST["scheduleMode"]=="M")
{
$_SESSION["DateMonth"]=$_POST["DateMonth"];

}
$_SESSION["scheduleMode"]=$_POST["scheduleMode"];

$_SESSION["scheduleAlert"] =$_POST["scheduleAlert"];
$_SESSION["scheduleRate"] =$_POST["scheduleRate"];
$_SESSION["fixedValue"]=$_POST["fixedValue"];
$_SESSION["TotalMonths"]=$_POST["TotalMonths"];

$_SESSION["Quarter"]=$_POST["Quarter"];
 }

 
 /*
 * 7897 - Premier FX
 * Rename 'Create Transaction' text in the system.
 * Enable  : string
 * Disable : "0"
 */
$strTransLabel = 'Transaction';
if(defined('CONFIG_SYSTEM_TRANSACTION_LABEL') && CONFIG_SYSTEM_TRANSACTION_LABEL!='0')
	$strTransLabel = CONFIG_SYSTEM_TRANSACTION_LABEL;

/*********** maintain logs for transactions #11429************/

	$QuerOldData = "SELECT * FROM ".TBL_TRANSACTIONS." WHERE transID ='".$_POST["transID"]."'";
	 $arrOldData = selectFrom($QuerOldData);
	 
	if(!empty($_POST["transID"]) )
	{
 
	$querUpdateTrans 	= 	"UPDATE ".TBL_TRANSACTIONS." SET
	moneyPaid			=	'".$_POST["moneyPaid"]."',
	refNumber 			=	'".$_POST["refNumber"]."',
	agentExchangeRate 	=	'".$_POST["checkManualRate"]."',
	exchangeRate 		=	'".$_POST["exchangeRate"]."',
	transactionPurpose	= 	'".$_POST["transactionPurpose"]."',
	tip 				=	'".$_POST["tip"]."' 
	WHERE transID 		= 	'".$_POST["transID"]."'
	";
	
	if(update($querUpdateTrans)){
	$querRefNumberIM = selectFrom ("SELECT refNumberIM FROM ".TBL_TRANSACTIONS ." WHERE transID = '".$_POST["transID"]."'");
	
	$descript ="Transaction ".$querRefNumberIM["refNumberIM"]." data is updated";
	activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["transID"],TBL_TRANSACTIONS,$descript);

	$id_activity_id = @mysql_insert_id();
	
	logChangeSet($arrOldData,$modifyby,"transactions","transID",$_POST["transID"],$_SESSION["loginHistoryID"]);
	
	$modifyHistoryId = mysql_insert_id();
   
    $insertActivityId = "UPDATE ".TBL_AUDIT_MODIFY_HISTORY." SET id_activity_id = ".$id_activity_id." WHERE id = ".$modifyHistoryId."";
    update($insertActivityId);	
	
	}
}
$reconcileValue                 =$_POST["WtReconcilation"];
$_SESSION["moneyPaid"] 			= $_POST["moneyPaid"];
$_SESSION["transactionPurpose"] = $_POST["transactionPurpose"];
$_SESSION["other_pur"] 			= $_POST["other_pur"];
$_SESSION["fundSources"] 		= $_POST["fundSources"];
$_SESSION["refNumber"] 			= $_POST["refNumber"];
//$_SESSION["benAgentID"] 		= $_POST["agentID"];
$_SESSION["benAgentID"] 		= $_POST["distribut"];
$_SESSION["discount"]       = $_POST["discount"];
$_SESSION["chDiscount"]     = $_POST["chDiscount"];
$_SESSION["exchangeRate"]     = $_POST["exchangeRate"];
$_SESSION["IMFee"]     = $_POST["IMFee"];

$_SESSION["bankName"] 		= $_POST["bankName"];
$_SESSION["branchCode"] 	= $_POST["branchCode"];
$_SESSION["branchAddress"] 	= trim($_POST["branchAddress"]);
$_SESSION["swiftCode"] 		= $_POST["swiftCode"];
$_SESSION["accNo"] 			= $_POST["accNo"];
$_SESSION["ABACPF"] 		= $_POST["ABACPF"];
$_SESSION["IBAN"] 			= $_POST["IBAN"];
$_SESSION["ibanRemarks"] 			= $_POST["ibanRemarks"];
$_SESSION["currencyTo"]     = $_POST['currencyTo'];
$_SESSION["customerID"]=$_POST['customerID'];

if($_POST["dDate"] != "")
	$_SESSION["transDate"] = $_POST["dDate"];

$_SESSION["question"] 			= $_POST["question"];
$_SESSION["answer"]     = $_POST["answer"];	
$_SESSION["tip"]     = $_POST["tip"];

$_SESSION["discount_request"] = $_POST["discount_request"];	
$_SESSION["bankingType"] = $_POST["bankingType"];
if($_SESSION["transType"]=="Bank Transfer")
	$_SESSION["bankCharges"] 		= $_POST["bankCharges"];
else
	$_SESSION["bankCharges"]="";
	
$_SESSION["distribut"]     = $_POST["distribut"];

if($_POST["transID"] != "")
{
	$backUrl = "add-transaction.php?msg=Y&transID=".$_POST["transID"]."&transType=".$_REQUEST["transType"]."&customerID=".$_GET["customerID"];
}
else
{
	$backUrl = "add-transaction.php?msg=Y&transType=".$_REQUEST["transType"]."&customerID=".$_GET["customerID"];
}
if(trim($_POST["transType"]) == "Pick up")
{
	//debug($_POST["transType"]."-->".$_POST["collectionPointID"]."-->".$_POST["selectDist"]."-->".$_SESSION["collectionPointID"]);
	if(trim($_POST["collectionPointID"]) == "")
	{
		insertError("Please Select Collection Point");	
		redirect($backUrl);
	}
 if(trim($_POST["selectDist"]) == "" && CONFIG_SELECT_DISTRIBUTOR_PICK_UP_TRANS == "1")
 {
 		insertError("Please Select Distributor");	
		redirect($backUrl);
 }
}
if(trim($_POST["benID"]) == "" && CONFIG_TRANS_WITHOUT_BENEFICIARY != '1')
{
	insertError(TE6);	
	redirect($backUrl);
}
if(CONFIG_ZERO_FEE == '1')
{
	if( $_POST["IMFee"] < 0)
	{
		insertError(TE2);	
		redirect($backUrl);
	}
	
}else{

	if((isset($_POST["IMFee"]) && trim($_POST["IMFee"]) == "") || $_POST["IMFee"] <= 0)
	{
		insertError(TE2);	
		redirect($backUrl);
	}
}

if((isset($_POST["localAmount"]) && trim(($_POST["localAmount"]) == "") || $_POST["localAmount"] <= 0))
{
	insertError(TE9);	
	redirect($backUrl);
}
if(isset($_POST["value_date"]) && trim($_POST["value_date"]) == "" && CONFIG_VALUE_DATE_MANDATORY == "1")
{
	insertError(TE32);	
	redirect($backUrl);
}
if(isset($_POST["benIdTypeFlag"]) && trim($_POST["benIdTypeFlag"]) == "" && strstr(CONFIG_BEN_IDTYPE_COMP_TRANSACTION_TYPES,trim($_POST["transType"])))
{
	insertError("Selected Beneficiary should have ID details for Transactin Type ".trim($_POST["transType"]).". Please Edit the Beneficiary.");	
	redirect($backUrl);
}


$cumulativeRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction')";
//debug($cumulativeRuleQuery);
$cumulativeRule = selectFrom($cumulativeRuleQuery);

$currentRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction')";
$currentRule = selectFrom($currentRuleQuery);
//debug($currentRuleQuery);
$agentType = getAgentType();
//debug ($agentType);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Confirm <?=$strTransLabel;?></title>
<style type="text/css">
td.myFormat
{
font-size:10px;	
}
td.Arial
{
font-size:10px;
font-family:"Arial",Times,serif;
border-bottom: solid 1px #000000;
border-right: solid 1px #000000;
}
table.double
{
border:double;
/*background:#EEEEEE; */
}
.style2 {
	color: black;
	font-weight: bold;
}
.noborder
{
font-size:11px;
}
table.single
{
border-style:solid;
border-width:1px;
}
td.bottom
{
border-bottom: solid 1px #000000;
font-size:10px;
}
td.sign
{
font-size:10px;
}
</style>
<script language="javascript">
function checkForm(theForm,strval){
		//alert (theForm+"-->"+strval);
		var minAmount = 0;
		var maxAmount = 0;

		var condition;
  
 
		var conditionCumulative;
		var conditionCurrent;
  	var ruleConfirmFlag=true;
  	var ruleFlag=false;
  	var transSender = '';
	if(strval == 'Yes')
	{
		transSender="transSend";
		}else{
			transSender="NoSend";
			}
	
	var accumulativeAmount = 0;
	accumulativeAmount = '<? echo $senderAccumulativeAmount ?>';
	var currentRuletransAmount = '<?=$_POST["transAmount"]?>';	
	var currentRuleAmountToCompare = 0;
	var amountToCompareTrans; 
	
// cummulative Amount Rule /////////
<?	
if($cumulativeRule["ruleID"]!='') {
    
     if($cumulativeRule["matchCriteria"] == "BETWEEN"){
		  $betweenAmount= explode("-",$cumulativeRule["amount"]);
		  $fromAmount=$betweenAmount[0];   
		  $toAmount=$betweenAmount[1]; 
?>
		conditionCumulative = <?=$fromAmount?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmount?>;
		
		<?
    }else{
?>
    amountToCompare = <?=$cumulativeRule["amount"]?>;
    conditionCumulative =	accumulativeAmount  <?=$cumulativeRule["matchCriteria"]?> amountToCompare;
 <?	}?>			
		
	
		 if(conditionCumulative)
	   {

				
	   	if(confirm("<?=$cumulativeRule["message"]?>"))
    	{
    	    ruleConfirmFlag=true;
    			//document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender;
				document.addTrans.submit();
    		
                
      }else{ 
    
    			/*	document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   					
      	addTrans.localAmount.focus();*/
       
        ruleConfirmFlag=false;
       // ruleFlag=true;
      }

    }else{ 

    	   ruleConfirmFlag=true;
    } 
 
	
<? } ?>

////////////current transaction amount///////


<? if($currentRule["ruleID"]!=''){
	   
		 
	   if($currentRule["matchCriteria"] == "BETWEEN"){
				
		  $betweenAmountCurrent= explode("-",$currentRule["amount"]);
		  $fromAmountCurrent=$betweenAmountCurrent[0];   
		  $toAmountCurrent=$betweenAmountCurrent[1]; 
		?>
		conditionCurrent = <?=$fromAmountCurrent?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmountCurrent?>;
		<?
 }else{
 	?>      
 	      
 		    currentRuleAmountToCompare = <?=$currentRule["amount"]?>;
 		    conditionCurrent =	currentRuletransAmount  <?=$currentRule["matchCriteria"]?> currentRuleAmountToCompare;
 <?	}?>
	
 
 if(conditionCurrent)
	   {

				if(confirm("<?=$currentRule["message"]?>"))
				{
					
    	     ruleConfirmFlag=true;
    			
    			/*document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();*/
    		
              
     }else{
    
    				/*document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   				   	addTrans.localAmount.focus();*/
         ruleConfirmFlag=false;
         //ruleFlag=true;
      }

    }else{ 

    	     ruleConfirmFlag=true; 
    } 
  
<? } ?>  
	   alert(ruleConfirmFlag);
		document.getElementById('confTrans').disabled=true; 
		if(ruleConfirmFlag) 
		{
	   		//document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; 																											
			document.addTrans.submit();
 		//	alert ("add-trans");
 		}
		if(ruleConfirmFlag == false)
		{
			//document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; 
			document.addTrans.submit();
		 	addTrans.localAmount.focus();
		}		
}	

function calculateOutStanding()
{
	var total = '<?=$_POST["totalAmount"]?>';
	var recieved = document.getElementById("amountRecieved").value;
	document.getElementById("outstanding").value = total - recieved;
}	
</script>
</head>
<body>
<table border="0" align="center" cellpadding="2" cellspacing="2" bordercolor="#DFE6EA" width="80%">
  <tr>
    <h2 align="left" >&nbsp;  &nbsp;&nbsp; &nbsp; Confirm <?=$strTransLabel;?></h2>
    <?        
//debug ($_SESSION["loggedUserData"]["username"]);
  $login_name= $_SESSION["loggedUserData"]["username"];
$custLogo = selectFrom("select logo  from ".TBL_ADMIN_USERS." where username ='".$_SESSION["loggedUserData"]["username"]."'");
if($custLogo["logo"] != "" && file_exists("../logos/" . $custLogo["logo"]))
	{
	
	$arr = getimagesize("../logos/" . $custLogo["logo"]);		
	$width = ($arr[0] > 200 ? 200 : $arr[0]);
	$height = ($arr[1] > 100 ? 100 : $arr[1]);	

?>
    <td align="center"><?=printLogoOnReciept($_SESSION["loggedUserData"]["userID"], $_SESSION["loggedUserData"]["username"]) ?>
    </td>
    <? 	
	}else{ ?>
    <td align="center" ><img id=Picture2 height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0></td>
    <? } ?>
    <?	
$agentsql="SELECT * FROM admin WHERE userID  ='".$_REQUEST['custAgentID']."'";
	$custAgentContents=selectFrom($agentsql);		
//debug ($custAgentContents);

?>
    <td class="sign"><? 
	if ($agentType=='SUPA' && $custLogo["logo"] != "")
	{
	//echo $custAgentContents['agentCompany']."<br>".$custAgentContents['agentAddress'].",<br>".$custAgentContents['agentAddress2'].",<br>".$custAgentContents['agentCity'].",".$custAgentContents['agentZip'].",<br>".$custAgentContents['agentCountry']."<br>";
	//echo "Tel : ".$custAgentContents['agentPhone']."<br>Fax : ".$custAgentContents['agentFax']."<br>Email : ".$custAgentContents['email']."<br>Web : ".$custAgentContents['agentURL'];
//	echo "<br>company";
	} 
	
	else {
$adminsql= "SELECT * FROM admin WHERE username = '".admin."'";
$adminContents=selectFrom($adminsql);
//echo $adminContents['agentCompany']."<br>".$adminContents['agentAddress'].",<br>".$adminContents['agentAddress2'].",<br>".$adminContents['agentCity'].",".$adminContents['agentZip'].",<br>".$adminContents['agentCountry']."<br>";
	//echo "Tel : ".$adminContents['agentPhone']."<br>Fax : ".$adminContents['agentFax']."<br>Email : ".$adminContents['email']."<br>Web : ".$adminContents['agentURL'];
//debug ($adminContents);
	}
	?>
    </td>
  </tr>
</table>
<table border="0" width="60%" class="single" align="center">
  <tr>
    <td><h4 align="Center">DEAL CONTRACT</h4>
      <form name="addTrans" method="post" action="add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend&customerID=<? echo $_GET["customerID"]?>&ruleID=<? echo $_GET['ruleID']; ?>">
	  <input type="hidden" name="startDate" id="startDate" value="<?=$_SESSION["start_date"] ?>">
	  <input type="hidden" name="endDate" id="endDate" value="<?=$_SESSION["end_date"] ?>">
        <table border="0" width="80%" align="center" class="double">
          <?
$sql = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Mobile, Email  from ".TBL_CUSTOMER." where customerID ='" . $_POST[customerID] . "'";
//debug ($sql);
$queryCust = "select customerID, Title, firstName, lastName, accountName,middleName, Address, Address1, City, State, Zip,Country, Phone, Mobile, Email, parentID  from ".TBL_CUSTOMER." where customerID ='" . $_POST[customerID] . "'";
			$customerContent = selectFrom($queryCust);
$sqlAccManager="select name from admin where userID= '" . $customerContent["parentID"] . "'";
//debug ($sql);
$accManagerContent = selectFrom($sqlAccManager);
//debug ($customerContent);

?>
          <tr border="1">
            <td width="20%" class="Arial">CLIENT NAME:</td>
            <td width="80%" class="bottom"><?= $customerContent["firstName"] . " " . $customerContent["lastName"] ?></td>
          </tr>
          <tr>
            <td class="Arial" width="20%">CLIENT NO.</td>
            <td width="80%" class="bottom"><?= $customerContent["accountName"]?></td>
          </tr>
          <tr>
            <td class="Arial" width="20%">REFERENCE:</td><?//TITLE ?><!-- DEALER -->
            <?
//$sql="select * from admin where userID= '" . $custAgentContents["userID"] . "'";
//debug ($sql);
//$userContent = selectFrom($sql);
//debug ($userContent);
?>
            <td width="80%" class="bottom"></td><?//$accManagerContent['name'];?>
          </tr>
        </table>
        <table border="0" width="80%" align="center">
          <tr>
            <td align="center" colspan="2" class="noborder">This Transaction has now been executed in accordance with your verbal instruction, which is legally binding in accordance with our Terms & Conditions.
              <table border="0" width="80%" align="center" class="double">
                <!-- Asad -->
				  <tr>
                  <td class="Arial" width="20%">TRANSACTION DATE:</td>
                  <td width="80%" class="bottom" align="left"><?=$date = date("d-m-Y"); ?></td>
                </tr>
<!--                <tr>-->
<!--                  <td class="Arial" width="20%">MATURITY DATE:</td>-->
<!--                  <td width="80%" class="bottom">--><?//= $_REQUEST['value_date']?><!--</td>-->
<!--                </tr>-->
                <tr>
                  <td class="Arial" width="20%">RECEIVE AMOUNT:</td>
                  <td width="80%" class="bottom"><?=$_REQUEST["localAmount"]. " ". $_SESSION["currencyTo"]?></td>
                </tr>
                <tr>
                  <td class="Arial" width="20%">AT A RATE OF:</td>
                  <td width="80%" class="bottom"><?= $_REQUEST['exchangeRate']?></td>
                </tr>
                <tr>
                  <td class="Arial" width="20%">SENT AMOUNT:</td>
                  <td width="80%" class="bottom"><?=$_REQUEST["transAmount"]." ".$_SESSION["currencyFrom"]?></td>
                </tr>
                <tr>
                  <td class="Arial" width="20%">TT Fee:</td>
                  <td width="80%" class="bottom">ZERO</td>
                </tr>
                <tr>
                  <td class="Arial" width="20%">BENEFICIARY REFERENCE:</td>
                  <? // $sql= "select * from transactions where customerID='" . $customerContent['customerID'] . "'";
//		debug ($sql);
		// $refContent = selectFrom($sql);
//debug ($refContent);
		
		?>
                  <td width="80%" class="bottom"><?= $_POST['tip'];?>
                    &nbsp;</td>
                </tr>
              </table>
			 </td>
          </tr>
        </table>
        <table border="0" width="80%" align="center">
          <tr>
            <td align="center" colspan="2" class="noborder">Where to instruct your Bank to send payment to us:</td>
          </tr>
        </table>
        <table border="0" width="50%" align="center" class="double">
          <?
$queryBen = "select benID, Title, firstName, lastName, middleName" . $SOFField . ", Address, Address1, City, State, Zip,Country, Phone, Email,Mobile from beneficiary where benID ='" . $_POST["benID"] . "'";

//debug ($queryBen); 
			$benificiaryContent = selectFrom($queryBen);
//debug ($benificiaryContent);
		  //Asad
		  if ($agentType == "SUPA" || $agentType == "SUBA") {
			  $accountSQl = "SELECT * FROM accounts WHERE userId = $modifyby AND currency = '" . $_POST['currencyFrom'] . "' AND showOnDeal = 'Y' ";
		  } else {
			  $agent = selectFrom("select agentID from customer where customerID = ".$customerContent['customerID']);
			  $accountSQl = "SELECT * FROM accounts WHERE userId = ".$agent["agentID"]." AND currency = '" . $_POST['currencyFrom'] . "' AND showOnDeal = 'Y' ";
		  }
	$accountRS = selectFrom($accountSQl);
	$fieldsChecked = $accountRS["fieldsChecked"];
	if(!empty($fieldsChecked))
		$fieldsCheckedArr = unserialize($fieldsChecked);
if(!empty($_POST['currencyFrom'])){
?>
		 <tr>
            <td class="Arial" width="20%">ACCOUNT NAME:</td>
            <td width="25%" class="Arial"><?=$accountRS["accountName"]?></td>
          </tr>
          <tr>
            <td class="Arial" width="20%">ACCOUNT NUMBER:</td>
            <td width="35%" class="bottom"><?=$accountRS["accounNumber"]?></td>
          </tr>
          <tr>
            <td class="Arial" width="20%">BANK:</td>
            <td width="25%" class="bottom"><?=$accountRS["bankName"]?></td>
          </tr>
		  <tr>
          <td class="Arial" width="20%">IBAN:</td>
            <td width="35%" class="bottom"><?=$accountRS["IBAN"]?></td>
          </tr>
          <tr>
            <td class="Arial" width="20%">SORT CODE:</td>
            <td width="25%" class="bottom"><?=$accountRS["sortCode"]?></td>
          <tr>
            <td class="Arial" width="20%">SWIFT CODE:</td>
            <td width="35%" class="bottom"><?=$accountRS["swiftCode"]?></td>
          </tr>
<?php /*?>		  <tr>
		  	<td class="Arial" width="20%">Bank Address:</td>
            <td width="35%" class="bottom"><?=$accountRS["bankAddress"]?></td>
		  </tr><?php */?>
		  <?php
		  $trAccountrTrading = "";
		  for($i=0;$i<count($fieldsCheckedArr);$i++){
		  		$fieldsArr = explode("|",$fieldsCheckedArr[$i]);
				if(is_array($fieldsArr)){
					$fieldsValue = $fieldsArr[0];
					$fieldsLabel = $fieldsArr[1];
				}
				else{
					$fieldsValue = $fieldsCheckedArr[$i];
				}
				if(empty($fieldsLabel))
					$fieldsLabel = $fieldsValue;
		  		$trAccountrTrading .= ' <tr>
										<td class="Arial" width="20%">'.strtoupper($fieldsLabel).':</td>
										<td width="35%" class="bottom">'.$accountRS[$fieldsValue].'</td>
									  </tr>';	
		  }
		  echo $trAccountrTrading;
}
			
/*?>        <table border="0" width="80%" align="center" class="">
          <tr>
            <td><fieldset>
              <legend class="style2">Beneficiary Details</legend>
              <table border="0" width="80%" align="center" class="double">
                <td class="Arial" width="20%">Beneficiary Name:</td>
                  <td width="25%" class="Arial"><?= $benificiaryContent["firstName"]." ".$benificiaryContent["middleName"]." ".$benificiaryContent["lastName"]		
		//= $_REQUEST["branchCode"]?>
                    &nbsp;</td>
                  <td class="Arial" width="20%">Bank Name:</td>
                  <td width="35%" class="bottom"><? //= $_REQUEST["accountType"]
		echo $_REQUEST["bankName"];
		?>
                    &nbsp;</td>
                </tr>
                <tr>
                  <td class="Arial" width="20%">IBAN</td>
                  <td width="25%" class="Arial"><? //= $_REQUEST["localAmount"]
		echo $_REQUEST["IBAN"];
		?>
                    &nbsp;</td>
                  <td class="Arial" width="20%">Account No.:</td>
                  <? $sqlbank= "select * from bankDetails where benID ='" . $_POST["benID"] . "'";
				$bankContent = selectFrom($sqlbank);
		?>
                  <td width="35%" class="bottom"><?= $_REQUEST["IBAN"];?></td>
                </tr>
                <tr>
                  <td class="Arial" width="20%">BIC/SWIFT Code </td>
                  <td width="25%" class="Arial"><? //= $_REQUEST["exchangeRate"]
		echo $_REQUEST["branchCode"];
		?>
                    &nbsp;</td>
                  <td class="Arial" width="20%">Branch Address</td>
                  <td width="35%" class="bottom"><? //= $_REQUEST["localAmount"]
		echo $_REQUEST["branchAddress"];
		?>
                    &nbsp;</td>
                </tr>
              </table>
              </fieldset></td>
          </tr>
        </table>
        <br><?php */?>
        <?php /*?><table border="0" width="80%" align="center" class="">
          <tr>
            <td><fieldset>
              <legend class="style2">Amount Details</legend>
              <table border="0" width="80%" align="center" class="double">
                <td class="Arial" width="20%">Exchange Rate</td>
                  <td width="25%" class="Arial"><? //= $_REQUEST["transType"]
		echo $_REQUEST["exchangeRate"];	?></td>
                  <td class="Arial" width="20%">Amount</td>
                  <td width="25%" class="bottom"><?= $_REQUEST["transAmount"]. " ". $_SESSION["currencyFrom"]?>
                  </td>
                </tr>
                <tr>
                  <td class="Arial" width="20%">Total Amount</td>
                  <td width="25%" class="Arial"><? //= $_REQUEST["transType"]
		echo $_REQUEST["exchangeRate"];	?>
                  </td>
                  <td class="Arial" width="20%">Local Amount</td>
                  <td width="35%" class="bottom"><?= $_REQUEST["totalAmount"]." ".$_SESSION["currencyFrom"]?>
                  </td>
                </tr>
              </table>
              <br>
              <table border="0" width="50%" align="center" class="double">
                <tr>
                  <td width="20%" class="Arial">PF Transaction No.</td>
                  <td width="35%" class="Arial">&nbsp;</td>
                </tr>
              </table>
              </fieldset></td>
          </tr>
        </table><?php */?>
        <table widh="100%" border="0" bordercolor="#FF0000" align="center">
        <!--<tr>
				<td align="center">
					<div class='noPrint'>
						<input type="button" name="Submit2" value="Print this Receipt" onClick="print()">
						&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="style2">Go to Create Transaction</a>
					</div>
				</td>
			</tr>
-->
<tr>
<td>
<table border="0" width="80%" align="center">
          <?
$queryBen = "select benID, Title, firstName, lastName, middleName" . $SOFField . ", Address, Address1, City, State, Zip,Country, Phone, Email,Mobile from beneficiary where benID ='" . $_POST["benID"] . "'";

//debug ($queryBen); 
			$benificiaryContent = selectFrom($queryBen);
//debug ($benificiaryContent);

?>
          <tr>
            <td colspan="2">I confirm that the details set out above are correct and that I will transmit the amount due to the account specified.</td><!-- I accept that the cost of failing to provide cleared settlement funds (1) one day before the maturity date of this trade may be subject to a &pound;25.00 per day late payment fee. -->
          </tr>
          <?php 
			if(CONFIG_CHANGE_PRINT_CONFIRM_DEAL_LAYOUT != '1'){
		  ?>
		  <tr>
            <td width="21%"><br/>SIGNED</td>
            <td width="79%">&nbsp;</td>
          </tr>
          <tr>
            <td width="21%"><br/>NAME (Print):</td>
            <td width="79%">&nbsp;</td>
          </tr>
          <tr>
            <td width="21%"><br/>DATED:</td>
            <td width="79%"><br/><?=date ("d/m/y"); ?></td>
          </tr>
		  <?php 
			}
		  ?>
        </table>
</td>
        </tr>
      
</table>
<table border="0" width="80%" align="center" class="double">
          <?
$queryBen = "select benID, Title, firstName, lastName, middleName" . $SOFField . ", Address, Address1, City, State, Zip,Country, Phone, Email,Mobile from beneficiary where benID ='" . $_POST["benID"] . "'";

//debug ($queryBen); 
			$benificiaryContent = selectFrom($queryBen);
//debug ($benificiaryContent);

?>
          <tr>
            <td class="Arial" colspan="2" align="center" style="font-weight:bold;font-size:14px; text-decoration:underline;">REQUEST FOR INTERNATIONAL TELEGRAPHIC TRANSFER</td>
          </tr>
          <tr>
            <td class="Arial" colspan="2" align="center">Please note that if you are making more than ONE transfer you will need to copy this page for each instruction.</td>
          </tr>
		<?php 
			if(CONFIG_CHANGE_PRINT_CONFIRM_DEAL_LAYOUT == '1'){
		?>
		  <tr>
			<td align="center" class="Arial" colspan="2" style="font-weight:bold;font-size:12px; text-decoration:underline;">
				Beneficiary Account Details
			</td>
			 </tr>
		 <?php 
			}
		 ?>
          <tr>
			  <td class="Arial" width="46%">CLIENT NUMBER:</td>
			  <td width="54%" class="Arial"><?= $customerContent["accountName"]?></td>
		  </tr>
		  <tr>
			  <td class="Arial" width="46%">BENEFICIARY NAME:</td>
			  <td width="54%" class="Arial"><?= $benificiaryContent["firstName"]." ".$benificiaryContent["middleName"]." ".$benificiaryContent["lastName"]?></td>
		  </tr>
          <tr>
			  <td class="Arial" width="46%">AMOUNT OF CURRENCY TO BE TRANSFERRED:</td>
			  <td width="54%" class="bottom"><?=$_REQUEST["localAmount"]." ".$_SESSION["currencyTo"]?></td>
          </tr>
          <tr>
            <td class="Arial" width="46%">BENEFICIARY&rsquo;S ACCOUNT NAME:</td>
            <td width="54%" class="bottom"><?=$_POST["bankName"];?></td>
          </tr>
		  <?php 
			if(CONFIG_CHANGE_PRINT_CONFIRM_DEAL_LAYOUT == '1'){
		  ?>
			<td class="Arial">ACCOUNT NO :</td>
			<td class="bottom"><?php echo $_POST["accNo"]; ?></td>
		  </tr>
		  <tr>
			<td class="Arial">BRANCH NAME / NUMBER :</td>
			<td class="bottom"><?php echo $_POST['branchCode']; ?></td>
		  </tr>
		  <tr>
			<td class="Arial">NAME AND ADDRESS OF THE BANK YOUR CURRENCY IS BEING TRANSFERRED TO :</td>
			<td class="bottom"><?php echo $_POST["bankName"]; ?></td>
		  </tr>
		  <tr>
			<td class="Arial">SWIFT/BIC Code of your bank:</td>
			<td class="bottom"><?php echo $_POST["swiftCode"]; ?></td>
		  </tr>
		  <tr>
			<td class="Arial">SORT CODE:</td>
			<td class="bottom"><?php echo $_POST["sortCode"]; ?></td>
		  </tr>
		  <tr>
			<td class="Arial">BENEFICIARY'S IBAN :<br />
				(if the payment is in euros, a full iban is required to avoid a bank administration charge)
			</td>
			<td class="bottom"><?php echo $_POST["IBAN"]; ?></td>
		  </tr>
		  <tr>
			<td class="Arial">ABA/ROUTING NUMBER (USA ONLY) :</td>
			<td class="bottom"><?php echo $_POST['routingNumber']; ?></td>
		  </tr>
		  <?php 
			}else{
		  ?>
			<td class="Arial" width="46%">BENEFICIARY&rsquo;S IBAN / ACCOUNT NO:
				IF THE PAYMENT IS IN EUROS, A FULL IBAN IS REQUIRED TO AVOID A BANK ADMINISTRATION CHARGE
			</td>
            <td width="54%" class="bottom"><? 
				//debug($_POST["IBAN"]."-->".$_POST["accNo"]);
				if($_POST["IBAN"]!='' && $_POST["accNo"]!=''){			
					echo "IBAN: ".$_POST["IBAN"]." / ". "Account No. ".$_POST["accNo"];
				}	
				elseif($_POST["IBAN"]!=''){
					echo "IBAN: ".$_POST["IBAN"];
				}
				elseif($_POST["accNo"]!=''){
					echo "Account No. ".$_POST["accNo"];
				}				
				 ?>
			</td>
          </tr>
          <tr>
            <td class="Arial" width="46%">NAME AND ADDRESS OF THE BANK YOUR CURRENCY IS BEING TRANSFERRED TO:</td>
            <td width="54%" class="bottom"><?=$_POST["branchAddress"];?></td>
		</tr>
          <tr>
            <td class="Arial" width="46%">SWIFT/BIC CODE OF THE YOUR BANK:</td>
            <td width="54%" class="bottom"><?=$_POST["swiftCode"];?></td>
          </tr>
          <tr>
            <td class="Arial" width="46%">CLEARING CODE OF THE BANK:(I.E. SORT CODE/ABA ROUTING NUMBER)</td>
            <td width="54%" class="bottom">
				<? if($_POST["sortCode"]!="" &&  $_POST["routingNumber"]!="") {
						echo $_POST["sortCode"]." / ".$_POST["routingNumber"];			
					}
					elseif($_POST["sortCode"] != ""){
						echo $_POST["sortCode"];
					}
					elseif($_POST["routingNumber"] != ""){
						echo $_POST["routingNumber"];
					}
			?>
			</td>
          </tr>
		  <?php 
			}
		  ?>
          <tr>
            <td class="Arial" width="46%">WHAT REFERENCE, IF ANY,DO YOU WANT TO QUOTE ON YOUR TRANSFER:</td>
            <td width="54%" class="bottom"><!-- BARC GB 22--><? 
			if(CONFIG_HIDE_TIP_FIELD != "1"){
				echo $_POST["tip"];
			}	
			else{
				echo "BARC GB 22";
			}
			?></td>
          </tr>
        </table>
<table widh="100%" border="0" bordercolor="#FF0000" align="center">
        <!--<tr>
				<td align="center">
					<div class='noPrint'>
						<input type="button" name="Submit2" value="Print this Receipt" onClick="print()">
						&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="style2">Go to Create Transaction</a>
					</div>
				</td>
			</tr>
-->
        <tr>
          <td align="center"><?		
		foreach ($_POST as $k =>$v)
		{
						$hiddenFields .= "<input name=\"" . $k. "\" type=\"hidden\" value=\"". $v ."\">\n";
			$str.="$k\t\t:$v<br>";
		}

echo $hiddenFields;
//debug ($str);
?>
        <tr>
          <? 
$TRANSACTION_DATE = dateFormat($today,1);
if(CONFIG_TRANS_CONDITION_ENABLED == '1')
                        	{
                        		if(defined("CONFIG_TRANS_COND"))
								{
									echo '<td class = "myFormat" align="center">'. (CONFIG_TRANS_COND).'</td>';						
								}
								else
								{
										$termsConditionSql = selectFrom("SELECT
											company_terms_conditions
										 FROM 
											company_detail
										 WHERE 
											company_terms_conditions!='' AND 
											dated = (
													 SELECT MAX(dated)
													 FROM 
														company_detail 
													WHERE 
														company_terms_conditions!=''
													 )");
								if(!empty($termsConditionSql["company_terms_conditions"]))
									$tremsConditions = addslashes($termsConditionSql["company_terms_conditions"]);
																		//eval("$tremsConditions");
									eval("\$tremsConditions = \"$tremsConditions\";");
									//echo stripslashes($tremsConditions);
							
					//echo $tremsConditions;
								}
                          		?>
          <?	if (defined('CONFIG_CONDITIONS')) {  ?>
          <A HREF="#" onClick="javascript:window.open('<?=CONFIG_CONDITIONS?>', 'Conditions', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')" CLASS="style2">Full Terms and Conditions</A>
          <?	}  ?>
          <? }else{
								
                        	 ?>
          I accept that I haven't used any illegal ways to earn money and I am paying tax regulary. I also accept that I am not going to send this money for any illegal act.
          <? } ?>
        </tr>
      
  <tr align="center">
    <td align="center"><input type="button" value="EDIT <?=strtoupper($strTransLabel);?>" onClick="document.location='add-transaction.php?transID=<? echo $_POST["transID"]?>&exchangeID=<?=$_SESSION['exchangeID']?>senderBank=<?=$_SESSION['senderBank']?>Reconcile=<? echo $_POST["transID"]?>&tempChange=Y&customerID=<? echo $_GET["customerID"]?>'" />
	
	<!--<td align="center"><input type="button" value="EDIT <?//=strtoupper($strTransLabel);?>" onClick="document.location='add-transaction.php?transID=<? //echo $_POST["transID"]?>Reconcile=<? //echo $_POST["transID"]?>'" />-->
	
	
	
      &nbsp;&nbsp;&nbsp;
       <input type="submit" id="confTrans" value="CONFIRM <?=strtoupper($strTransLabel);?>" /></td>
      
	   <input type="hidden" name="reconcile" value="<?=$reconcileValue; ?>"> 

  </tr>
  </form>
</table>
</body>
</html>
	<!--<a href="add-transaction.php?transID=<?// echo $_POST["transID"]?>&exchangeID=<?//=$_SESSION['exchangeID']?>senderBank=<?//=$_SESSION['senderBank']?>&tempChange=Y" class="style2" id="change_info_script"><?//=__("Change Information") ?></a>-->