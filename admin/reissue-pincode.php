<?
session_start();
include ("../include/config.php");
include ("security.php");
$agentType = getAgentType();
$date_time = date('d-m-Y  h:i:s A');

$_SESSION["CustomerID"] = $_GET["CustomerID"];


/*
session_register("Title");
session_register("firstName");
session_register("lastName");
session_register("middleName");
session_register("IDType");

if ($_POST["agentID"] != "")
	$_SESSION["agentID2"] = $_POST["agentID"];
if ($_POST["firstName"] != "")
	$_SESSION["firstName"] = $_POST["firstName"];
if ($_POST["lastName"] != "")
	$_SESSION["lastName"] = $_POST["lastName"];
if ($_POST["middleName"] != "")
	$_SESSION["middleName"] = $_POST["middleName"];
if ($_POST["Title"] != "")
	$_SESSION["Title"] = $_POST["Title"];
if ($_POST["IDType"] != "")
	$_SESSION["IDType"] = $_POST["IDType"];*/
?>
<html>
<head>
	<title>Add Customer Information To Re-Issue PIN Code</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
	
	if(theForm.question1.options.selectedIndex == 0){
    	alert("Please select the Question 1 from the list.");
        theForm.question1.focus();
        return false;
    }	
	if(theForm.answer1.value == "" || IsAllSpaces(theForm.answer1.value)){
    	alert("Please provide the Answer 1.");
        theForm.answer1.focus();
        return false;
    }
	if(theForm.question2.options.selectedIndex == 0){
    	alert("Please select the Question 2 from the list.");
        theForm.question2.focus();
        return false;
    }	
	if(theForm.answer2.value == "" || IsAllSpaces(theForm.answer2.value)){
    	alert("Please provide the Answer 2.");
        theForm.answer2.focus();
        return false;
    }
	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
	</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc"><strong><font color="#000066" size="2"><strong>Enter Customer Information To Re-Issue PIN Code </strong></font></strong></td>
  </tr>
  <form name="frmBenificiary" action="add-pincode-conf.php?from=<? echo $_GET["from"]?>" method="post" onSubmit="return checkForm(this);">
  <tr>
      <td align="center"> <table width="400" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr> 
            <td colspan="2" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Enter 
                    Customer Information </strong></font></td>
                </tr>
              </table></td>
          </tr>
          <? if ($_GET["msg"] != ""){ ?>
          <tr bgcolor="#ededed">
            <td colspan="2" bgcolor="#FFFFCC"><table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr>
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>Please Provide Correct Information</b><br><br></font>"; ?></td>
                </tr>
              </table></td>
          </tr>
          <? } ?>
          <tr bgcolor="#ededed"> 
            <td height="19" colspan="2" align="center"><font color="#FF0000">* Compulsory Fields</font><font color="#FF0000">&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
          </tr>
          <tr> 
            <td valign="top" colspan="2" >
              <table width="100%" cellpadding="2" cellspacing="1" border="0">
				<?
				if($agentType == "admin" || $agentType == "Call")
				{
				?>
				<?
				}
				else
				{
					echo "<input type='hidden' name='agentID' value='".$_SESSION["loggedUserData"]["userID"]."'>";
				}
				
				?>
                <tr bgcolor="#ededed">
                  <td align="left"><font color="#005b90"><strong>Personal Question 1 </strong></font></td>
                  <td align="left">
                    <SELECT name="question1" style="font-family:verdana; font-size: 11px">
                      <OPTION value="">- Select Secret Question 1-</OPTION>
                      <?
				$country = $_SESSION["country"];
				$strQuery = "SELECT * FROM  cm_questions";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
			
				while($rstRow = mysql_fetch_array($nResult))
				{
					$Question = $rstRow["question"];	
					$qID = $rstRow["qID"];														
					if($qID <=2)
					{
						if($qID == $_SESSION["question1"])
							echo "<option  value=$question1 selected >".$Question."</option>";
							
					else
						echo "<option value=$qID>".$Question."</option>";	
					}
				}
				
				?>
                    </SELECT>
                  </td>
                </tr>
                <tr bgcolor="#ededed">
                  <td align="left"><font color="#005b90"><strong>Answer 1*</strong></font></td>
                  <td align="left"><input type="text" name="answer1" value="<?=stripslashes($_SESSION["answer1"]); ?>" size="35" maxlength="100"></td>
                </tr>
                <tr bgcolor="#ededed">
                  <td align="left"><font color="#005b90"><strong>Personal Question 2 </strong></font></td>
                  <td align="left">
                    <SELECT name="question2" style="font-family:verdana; font-size: 11px">
                      <OPTION value="">- Select Secret Question 1-</OPTION>
                      <?
				
				$strQuery = "SELECT * FROM  cm_questions";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
			
				while($rstRow = mysql_fetch_array($nResult))
				{
					$Question = $rstRow["question"];	
					$qID = $rstRow["qID"];														
					if($qID >2)
					{
						if($qID == $_SESSION["question2"])
							echo "<option  value=$question2 selected >".$Question."</option>";
							
					else
						echo "<option value=$qID>".$Question."</option>";	
					}
				}
				
				?>
                    </SELECT>
                  </td>
                </tr>
                <tr bgcolor="#ededed">
                  <td align="left"><font color="#005b90"><strong>Answer 2*</strong></font></td>
                  <td align="left"><input type="text" name="answer2" value="<?=stripslashes($_SESSION["answer2"]); ?>" size="35" maxlength="100"></td>
                </tr>
                <tr bgcolor="#ededed"> 
                  <td width="39%" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
                  <td width="61%">        </td>
                </tr>
              </table></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td colspan="2" align="center">
			<input type="hidden" name="CustomerID" value="<?echo $_SESSION["CustomerID"]?>">
			<input name="Save" type="submit" value=" Save ">
              &nbsp;&nbsp; <input name="reset" type="reset" value=" Clear "> </td>
          </tr>
        </table></td>
  </tr>
</form>
</table>
</body>
</html>
