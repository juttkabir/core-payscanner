<?
session_start();
include ("../include/config.php");
$date_time = date("Y-m-d h:i:s");
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$thisPage = "add-clave-range.php?msg=Y";

$lower  = "";
$upper  = "";
$prefix = "";
$errMsg = "";
if($_GET["clID"] != '')
{
	if($_GET["update"] == 'Y')
	{
		
		$claveInfo  = selectFrom("select * from ".TBL_CLAVE_RANGE." where id = '".$_GET["clID"]."'");
		$agentID = $claveInfo["agentID"];	
		$lower  =  $claveInfo["rangeFrom"];
		$upper  =  $claveInfo["rangeTo"];
		$prefix =  $claveInfo["prefix"];
	}	
	
}
if ($_POST["Submit"] == "Save") {
	$lower = trim($_POST["claveRangeFrom"]);
	$upper = trim($_POST["claveRangeTo"]);
	$prefix = trim($_POST["prefix"]);
	$agentID = $_POST["agentID"];
	if($_GET["clID"] != '')
	{
		
		
			if (isExist("SELECT `id` FROM ".TBL_CLAVE_RANGE." WHERE agentID = '".$agentID."' AND id != '".$_GET["clID"]."' AND ((`rangeFrom` > '".$lower."' and `rangeFrom` < '".$upper."') OR (`rangeFrom` = '".$lower."' OR `rangeFrom` = '".$upper."' OR `rangeTo` = '".$lower."' OR `rangeTo` = '".$upper."') OR (`rangeFrom` < '".$lower."' and `rangeTo` > '".$lower."'))")) {
				$errMsg = ARR3;
			
			} else {
			$qInsert = "UPDATE ".TBL_CLAVE_RANGE." 
									SET `rangeFrom` = '".$lower."', 
											`rangeTo`   = '".$upper."', 
											`prefix`   = '".$prefix."', 
											`created`   = '".$date_time."' 
											where id = '".$_GET["clID"]."'";
				update($qInsert);
				insertError(ARR4);
				$thisPage = "view-clave-ranges.php?msg=Y";
				$thisPage .= "&success=Y&agentID=$agentID";
				redirect($thisPage);
			}
		
		}else{
			if (isExist("SELECT `id` FROM ".TBL_CLAVE_RANGE." WHERE agentID = '".$agentID."' AND ((`rangeFrom` > '".$lower."' and `rangeFrom` < '".$upper."') OR (`rangeFrom` = '".$lower."' OR `rangeFrom` = '".$upper."' OR `rangeTo` = '".$lower."' OR `rangeTo` = '".$upper."') OR (`rangeFrom` < '".$lower."' and `rangeTo` > '".$lower."'))")) {
				$errMsg = ARR3;
			} else {
				$qInsert = "INSERT INTO ".TBL_CLAVE_RANGE." 
									SET `agentID`   = '".$agentID."', 
											`rangeFrom` = '".$lower."', 
											`rangeTo`   = '".$upper."', 
											`prefix`   = '".$prefix."', 
											`created`   = '".$date_time."' ";
				insertInto($qInsert);
				insertError(ARR1);
				$thisPage .= "&success=Y";
				redirect($thisPage);
			}
		}
}

?>
<html>
<head>
	<title><? if($_GET["clID"] != ''){ ?>Update <? }else{ ?>Add<? } ?> Distributor's CLAVE Book Range</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
<!--

function checkForm(theForm) {
	
	if (theForm.agentID.options.selectedIndex == 0) {
		alert("Please select the <? echo (CONFIG_DIST_REF_NUMBER == "1" ? "distributor" : "agent")?>.");
		theForm.agentID.focus();
		return false;
	}

	if (theForm.claveRangeFrom.value == "" || IsAllSpaces(theForm.claveRangeFrom.value)) {
  	alert("Please provide Lower Range of Clave Book.");
    theForm.claveRangeFrom.focus();
    return false;
  } else if (!isNumeric(trim(theForm.claveRangeFrom.value))) {
  	alert("Please provide Lower Range of Clave Book in Numbers Only.");
    theForm.claveRangeFrom.focus();
    return false;
  }
  
	if (theForm.claveRangeTo.value == "" || IsAllSpaces(theForm.claveRangeTo.value)) {
  	alert("Please provide Upper Range of Clave Book.");
    theForm.claveRangeTo.focus();
    return false;
  } else if (!isNumeric(trim(theForm.claveRangeTo.value))) {
  	alert("Please provide Upper Range of Clave Book in Numbers Only.");
    theForm.claveRangeTo.focus();
    return false;
  }
  
  if (parseInt(theForm.claveRangeFrom.value) > parseInt(theForm.claveRangeTo.value)) {
  	alert("Lower Range should not be greater than Upper Range.");
    theForm.claveRangeFrom.focus();
    return false;
  }
  
	return true;
}

function isNumeric(sText) {
	var ValidChars = "0123456789";
  var IsNumber = true;
  var Char;
  for (i = 0; i < sText.length && IsNumber == true; i++) {
		Char = sText.charAt(i);
    if (ValidChars.indexOf(Char) == -1)	{
    	IsNumber = false;
    }
	}
  return IsNumber;   
}

function trim(sString) {
	while (sString.substring(0,1) == ' '){
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ') {
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
}

function IsAllSpaces(myStr) {
	while (myStr.substring(0,1) == " ")	{
  	myStr = myStr.substring(1, myStr.length);
  }
  if (myStr == "")	{
  	return true;
  }
  return false;
}

// end of javascript -->
</script>
	<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><? if($_GET["clID"] != ''){ ?>Update <? }else{?>Add <? } ?>Distributor's Clave Book Range</td>
  </tr>
  <form action="add-clave-range.php?flag=Y&clID=<?=$_GET["clID"]?>" method="post" onSubmit="return checkForm(this);">
  <tr>
      <td align="center"> 
        <table width="500" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="500" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong><? if($_GET["clID"] != ''){ ?>Update <? }else{ ?>Add <? } ?>Distributor's Clave Book Range</strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
    <? if ($errMsg != "") {  ?>
		<tr bgcolor="#ededed">
            <td colspan="2" bgcolor="#EEEEEE">
<table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo CAUTION_COLOR; ?>"><b><i><? echo CAUTION_MARK;?></i></b></font></td>
	<td width="635"><? echo "<font color='" . CAUTION_COLOR . "'><b>".$errMsg."</b><br></font>"; ?>
		  </td>
		 </tr>
		</table>
	 </td>
	</tr>
		<? } else if ($_GET["msg"] != "") { ?>
		<tr bgcolor="#ededed">
            <td colspan="2" bgcolor="#EEEEEE">
<table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
	<td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br></font>"; $_SESSION['error']=""; ?>
		  </td>
		 </tr>
		</table>
	 </td>
	</tr>
		<? } 
		if ($_GET["flag"]!="")
    {
    ?>
    	<tr bgcolor="#ededed">
      	<td height="19" colspan="2"><a class="style2" href="view-clave-ranges.php?agentID=<?=$agentID?>">Go Back</a></td>
    	</tr>
    <? 
    } 
    ?>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center"><font color="#FF0000">* Compulsory 
              Fields</font></td>
		</tr>
        <tr bgcolor="#ededed">
            <td width="152"><font color="#005b90"><strong>Select <? if(CONFIG_DIST_REF_NUMBER == '1'){?>Distributor<?}else{ ?>Agent*<? }?></strong></font></td>
            <td width="343">
            	<?
		        		if($_GET["clID"] != ''){
		        				$updateAgentInfo = selectFrom("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where userID = '".$agentID."'");	        		
		        				echo($updateAgentInfo["agentCompany"]." [".$updateAgentInfo["username"]."]");
		        				?>
		        						<input type="hidden" name="agentID" value="<?=$updateAgentInfo["userID"]?>">
		        						
		        				<?
		        		}else{    	
				            	 $select = "Select Distributor";
				            	
				            	
				            	?>
				                <select name="agentID" style="font-family:verdana; font-size: 11px">
				                  <option value="">- <? echo $select?>-</option>
									<?
				          		
				          			$agentQuery  = " select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType = 'Agent' and isCorrespondent != 'N' and agentStatus = 'Active' ";
												
											
											$agentQuery .= " order by agentCompany ";
											$agents = selectMultiRecords($agentQuery);
											for ($i=0; $i < count($agents); $i++) {
											?>
				            	   <option value="<?=$agents[$i]["userID"]; ?>" <? echo ($agents[$i]["userID"] == $agentID ? "selected" : "") ?>><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
				          		<?	}  ?>
				                </select>
				           		<?
		          		}
		           	?>     
						</td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>
            	Clave Range*</strong></font></td>
            <td>
            	<input type="text" name="claveRangeFrom" value="<? echo $lower; ?>" size="10" maxlength="16" <? echo($_GET["clID"] != '' ? "readonly": "");?>> To <input type="text" name="claveRangeTo" value="<? echo $upper; ?>" size="10" maxlength="16">
            </td>
        </tr>
       
	        <tr bgcolor="#ededed">
	            <td width="285"><font color="#005b90"><strong>Prefix</strong></font></td>
	            <td>
	            	<input type="text" name="prefix" value="<? echo $prefix; ?>"> 
	            </td>
	        </tr>
	      
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" name="Submit" value="Save">&nbsp;&nbsp; <input type="reset" value="Clear">
			</td>
		</tr>
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>