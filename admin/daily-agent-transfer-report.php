<?
//session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include("calculateBalance.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

$totalAmount="";


if ($offset == "")
	$offset = 0;
$limit=10;

if ($_GET["newOffset"] != "") {
	
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";



$agentType = getAgentType();


////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPA" && $agentType != "SUPAI")
		{
			$condition = True;	
		}
	}else{
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager")
			{
				$condition = True;
			}
		
		}
		
//////////////////////////Making of Condition///////////////

//$_SESSION["middleName"] = $_POST["middleName"];
if($_POST["Submit"] =="Search" || $_GET["search"]=="search")
{
	
	
if($condition)
{
	$query2 = "select distinct currencyTo from " . TBL_TRANSACTIONS . " as t, " . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID ";
	$queryCnt2 = "select count(distinct t.currencyTo) from ". TBL_TRANSACTIONS . " as t, " . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID ";
	 	
}else{
	$query2 = "select distinct t.currencyTo from " . TBL_TRANSACTIONS . " as t, " . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and (a.userID='".$_SESSION["loggedUserData"]["userID"]."' OR a.parentID='".$_SESSION["loggedUserData"]["userID"]."')" ;
	$queryCnt2 = "select count(distinct t.currencyTo) from " . TBL_TRANSACTIONS . " as t, " . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and (a.userID='".$_SESSION["loggedUserData"]["userID"]."' OR a.parentID='".$_SESSION["loggedUserData"]["userID"]."')" ;
}

if($agentType == "Branch Manager")
{
	$query2 .= " and a.parentID = '$parentID'";						
	$queryCnt2 .= " and a.parentID = '$parentID'";						
}


	if($_POST["Submit"]=="Search")
	{		
		$_SESSION["dgrandTotal"]="";
		$_SESSION["dCurrentTotal"]="";	

		$dfMonth = $_POST["fMonth"];
		$dfDay = $_POST["fDay"];
		$dfYear = $_POST["fYear"];
		
		$dtMonth = $_POST["tMonth"];
		$dtDay = $_POST["tDay"];
		$dtYear = $_POST["tYear"];
		
	$fromDate = $dfYear . "-" . $dfMonth . "-" . $dfDay;
	$toDate = $dtYear . "-" . $dtMonth . "-" . $dtDay;
		
		$dagentID = $_POST["agentID"];
	}
	
	if($_POST["country"]!="")
		$country = $_POST["country"];
		elseif($_GET["country"]!="") 
			$country = $_GET["country"];
		
	if($_POST["currency"]!="")
		$currency = $_POST["currency"];
		elseif($_GET["currency"]!="") 
			$currency = $_GET["currency"];	
	
	$queryDate = " (t.transDate >= '$fromDate 00:00:00' and t.transDate <= '$toDate 23:59:59')";

		$query2 .= " and $queryDate";				
		$queryCnt2 .= " and $queryDate";				
	
	
	
	
	if($dagentID != "")
	{
		if($dagentID != "all")
		{
			$query2 .= " and t.custAgentID='".$dagentID."' ";
			$queryCnt2 .= " and t.custAgentID='".$dagentID."' ";
		
		}
	}
	if($_POST["transStatus"] != "")
	{
		$transStatus = $_POST["transStatus"];
		$query2 .=" and t.transStatus = '$transStatus'";
		$queryCnt2 .=" and t.transStatus = '$transStatus'";
	}
	else
	{
		$query2 .=" and (t.transStatus !='Pending' and t.transStatus !='Processing')";
		$queryCnt2 .=" and (t.transStatus !='Pending' and t.transStatus !='Processing')";
	}
	
	if($country != "") {
		$query2 .= " and (t.fromCountry='".$country."')" ;
		$queryCnt2 .= " and (t.fromCountry='".$country."')" ;
	}
	
	if($currency != "") {
		$query2 .= " and (t.currencyFrom='".$currency."')" ;
		$queryCnt2 .= " and (t.currencyFrom='".$currency."')" ;
	}
	
$query2 .= " LIMIT $offset , $limit";
$contentsTrans2 = selectMultiRecords($query2);

$allCount = countRecords($queryCnt2);	
}
?>
<html>
<head>
	<title>Daily Agent Report</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="javascript/jquery.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript">
$(document).ready(function(){
	$("#printReport").click(function(){
		// Maintain report print logs 
		$.ajax({
			url: "maintainReportsLogs.php",
			type: "post",
			data:{
				maintainLogsAjax: "Yes",
				pageUrl: "<?=$_SERVER['PHP_SELF']?>",
				action: "P"
			}
		});
		
		$("#searchTable").hide();
		$("#pagination").hide();
		$("#actionBtn").hide();
		print();
		$("#searchTable").show();
		$("#pagination").show();
		$("#actionBtn").show();
	});
});
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">Daily Agent Report</td>
  </tr>
  
  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666" id="searchTable">
        <form action="daily-agent-transfer-report.php" method="post" name="Search">
      <tr>
            <td width="463" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search Filters 
              </strong></span></td>
        </tr>
        <tr>
        <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$dfMonth?>");
        </script>
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$dfDay?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$dfYear?>");
        </script>
        To	<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$dtMonth?>");
        </script>
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$dtDay?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$dtYear?>");
        </script><br><br>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?  
		//if($agentType=="admin" || $agentType == "Admin Manager" || $agentType == "Branch Manager")
		if($condition)
		{
		?>
              Agent Name 
              <select name="agentID" style="font-family:verdana; font-size: 11px">
					<option value="">- Select Agent-</option>
					<option value="all">All Agents</option>
					<?
	//work by Mudassar Ticket #11425
		if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){							
							$agentQuery = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'ONLY' AND agentStatus = 'Active' ";
							
					}		
		else if (CONFIG_AGENT_WITH_ALL_STATUS==1){						
							$agentQuery = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'ONLY' ";
						}	
	//work end by Mudassar Ticket #11425
							if($agentType == "Branch Manager")
							{
								$agentQuery .= " and parentID = '$parentID' ";
							}
						$agentQuery .=" order by agentCompany";
				
						$agents = selectMultiRecords($agentQuery);
						//$agents = selectMultiRecords();
						for ($i=0; $i < count($agents); $i++){
					?>
					<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
					<?
						}
					?>
				 </select>          
				 <script language="JavaScript">
				SelectOption(document.Search.agentID, "<?=$dagentID?>");
				</script>
		<? 
		}
		?>
		<select name="transStatus" style="font-family:verdana; font-size: 11px; width:100">
		  <option value=""> - Status - </option>
		  <option value="Authorize">Authorized</option>
		  <option value="amended">Amended</option>
		  <option value="Suspended">Suspended</option>
		  <option value="Cancelled">Cancelled</option>
		   <? if(CONFIG_RETURN_CANCEL == "1"){ ?>
		  <option value="Cancelled - Returned">Cancelled - Returned</option>
		  <? } ?>
		  <option value="AwaitingCancellation">Awaiting Cancellation</option>
		  <option value="Failed">Failed</option>
		  <option value="Picked up">Picked up</option>
		  <option value="Credited">Credited</option>
        </select>
        <script language="JavaScript">SelectOption(document.forms[0].transStatus, "<?=$transStatus; ?>");
        	</script>
			<?	if (CONFIG_CNTRY_CURR_FILTER == "1") {  ?>
        <br><br>	
         <select name="country" style="font-family:verdana; font-size: 11px; width:130">
				<option value=""> - Select Country - </option> 
				<?
					$queryCountry = "select distinct(countryName) from ".TBL_COUNTRY." where 1 and countryType like '%origin%' ";
					$countryData = selectMultiRecords($queryCountry);
					for($k = 0; $k < count($countryData); $k++)
					{?>
						 <option value="<?=$countryData[$k]['countryName']?>"><?=$countryData[$k]['countryName']?></option>	
					<? 
					}
				?>
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].country, "<?=$country?>");
			</script>
		&nbsp;&nbsp;
		<select name="currency" style="font-family:verdana; font-size: 11px; width:130">
				<option value="">-Select Currency-</option> 
				<?
					$queryCurrency = "select distinct(currencyName) from ".TBL_CURRENCY." where 1 ";
					$currencyData = selectMultiRecords($queryCurrency);
					for($k = 0; $k < count($currencyData); $k++)
					{?>
						 <option value="<?=$currencyData[$k]['currencyName']?>"><?=$currencyData[$k]['currencyName']?></option>	
					<? 
					}
				?>
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].currency, "<?=$currency?>");
			</script>
		
		&nbsp;&nbsp;
			<?	}  ?>
        <input type="submit" name="Submit" value="Search">
        
       </td>
      </tr>
	  </form>
    </table>
      <br>
	  <br>
      <br>
      <table width="841" border="1" cellpadding="0" bordercolor="#666666">
        <form action="daily-agent-transfer-report.php" method="post" name="trans">
          <?
			if ($allCount > 0){
		?>
          <tr>
            <td width="833"  bgcolor="#000000">
			<table width="836" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF" id="pagination">
                <tr>
                  <td width="477">&nbsp;
                    <?php /*if (count($contentsTrans2) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans2));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;*/ ?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF ."?newOffset=0&sortBy=".$_GET["sortBy"]."&total=first&country=$country&currency=$currency&search=search";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="53" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$prv&sortBy=".$_GET["sortBy"]."&total=pre&country=$country&currency=$currency&search=search"?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&total=next&country=$country&currency=$currency&search=search";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="82" align="right"> <!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&total=last";?>"--><font color="#005b90">&nbsp;</font><!--/a-->&nbsp;
                  </td>
                  <?php }
				  } ?>
                </tr>
              </table>
	    <tr>
            <td><b><font color="#000000">Amount in this report is excluding Transaction <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></b></td>
        </tr>
		 
		 
		<?
		if(count($contentsTrans2) > 0)
		{
		?>
      <tr>
            <td height="25" bgcolor="#C0C0C0" bordercolor="#000000"><span class="tab-u"><strong>&nbsp;Report 
              Date - <? if($dfMonth != "" && $fromDate < $toDate){echo "From ".$dfMonth."-".$dfDay."-".$dfYear." To ".$dtMonth."-".$dtDay."-".$dtYear;}?></strong></span></td>
        </tr>
        <tr>
            <td height="25" nowrap bgcolor="#C0C0C0" align="center"><span class="tab-u"><strong> Daily Agent Report </strong></span></td>
        </tr>                      
        
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="100%" border="0" bordercolor="#EFEFEF">
                <tr bgcolor="#FFFFFF"> 
                  <td width="177"><span class="style1">Agent Name</span></td>
                   <?
  			    
				  ?>
				  <td width="155"><span class="style1">Total Amount <?=$currencyFrom?></span></td>
				 
				  <!--td width="155"><span class="style1">Foreign Amount <?=$currencyTo?> </span></td-->
                  <td width="404"><span class="style1">Count</span></td>
                </tr>
                
                
                <? $total_count = 0;
				
				for($i=0;$i < count($contentsTrans2);$i++)
			{
				
				/////////
				
				//if($agentType == "admin" || $agentType == "Call" || $agentType == "Admin Manager" || $agentType == "Branch Manager")
				if($condition)
				{
					$query = "select distinct t.custAgentID from " . TBL_TRANSACTIONS . " as t, " . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID ";
					 	
				}
				else
				{
					$query = "select distinct t.custAgentID from " . TBL_TRANSACTIONS . " as t, " . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and (a.userID='".$_SESSION["loggedUserData"]["userID"]."' OR a.parentID='".$_SESSION["loggedUserData"]["userID"]."')" ;
				}
					
					if($agentType == "Branch Manager")
					{
						$query .= " and a.parentID = '$parentID'";						
					}
							
					$query .= " and $queryDate";				
				
				if($dagentID != "")
				{
					if($dagentID != "all")
					{
						$query .= " and t.custAgentID='".$dagentID."' ";
					}
				}
				if($country != "")
				{
					$query .= " and (t.fromCountry='".$country."')";
				}
	
				if($currency != "")
				{
					$query .= " and (t.currencyFrom='".$currency."')";
				}
				if($_POST["transStatus"]!="")
				{
					$transStatus = $_POST["transStatus"];
					$query .=" and t.transStatus = '$transStatus'";
				}
				else
				{
					$query .=" and (t.transStatus !='Pending' and t.transStatus !='Processing')";
				}
					$query .=" and t.currencyTo = '".$contentsTrans2[$i]["currencyTo"]."'";
			
			$contentsTrans = selectMultiRecords($query);
			
							
				//////////
				for($k = 0; $k< count($contentsTrans); $k++)
				{
				?>
                <tr bgcolor="#FFFFFF"> 
                  <? 
				  //$bal = selectMultiRecords("select currencyFrom, totalAmount, transAmount from ".TBL_TRANSACTIONS." where custAgentID='".$contentsTrans[$i]["custAgentID"]."' and (transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59') and (transStatus !='Pending' and transStatus !='Processing')");
				  if($_POST["transStatus"]!="")
				  {
				  	$transStatus = $_POST["transStatus"];
						$agentTransQuery = "select currencyFrom, totalAmount, transAmount from ".TBL_TRANSACTIONS." where custAgentID='".$contentsTrans[$k]["custAgentID"]."' and (transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59') and (transStatus = '$transStatus') and currencyTo = '".$contentsTrans2[$i]["currencyTo"]."'";
					}
					else
					{
						$agentTransQuery = "select currencyFrom, totalAmount, transAmount from ".TBL_TRANSACTIONS." where custAgentID='".$contentsTrans[$k]["custAgentID"]."' and (transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59') and (transStatus !='Pending' and transStatus !='Processing') and currencyTo = '".$contentsTrans2[$i]["currencyTo"]."'";
					}
					
					if($country != "")
					{
						$agentTransQuery .= " and (fromCountry='".$country."')";
					}
					
					if($currency != "")
					{
						$agentTransQuery .= " and (currencyFrom='".$currency."')";
					}
				  $balanceCal = 0;
				  $j = 0;
				   if($agentsTrans = mysql_Query($agentTransQuery)){
				 // for($j=0;$j < count($bal);$j++)
				 		while($bal = mysql_fetch_array($agentsTrans))
					  {
					  	
					  	$currencyFrom = $bal["currencyFrom"];
					  	$balanceCal += $bal["transAmount"];
					  	$j++;
						}
					}
				  $agentContent = selectFrom("select name from ". TBL_ADMIN_USERS ." where userID='".$contentsTrans[$k]["custAgentID"]."'");
				  
				  ?>
                 <td width="81" >
				  	<a href="#" onClick="javascript:window.open('view-detailed-agent-transaction.php?custAgentID=<? echo $contentsTrans[$k]["custAgentID"]?>&toDate=<? echo $toDate;?>&fromDate=<? echo $fromDate; ?>&currencyTo=<?=$contentsTrans2[$i]["currencyTo"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $agentContent["name"];?></font></strong></a>
				</td>
				 
                  <!--td width="100" bgcolor="#FFFFFF">< echo $contentsTrans[$i]["transStatus"]></td-->
                  <td width="155" bgcolor="#FFFFFF"> 
                    <? 
						$balanceCal = number_format($balanceCal, 2, '.', '');
				   echo($currencyFrom ." ". $balanceCal);
				  $totalAmount+=$balanceCal;
				 
				  ?>
                  </td>
				  <!--td width="155" bgcolor="#FFFFFF"> &nbsp;
                    <? 
				 
					
				    //echo $LAmoutn = $balanceCal * $exRate;
				    
				 
				  ?>
                  </td-->
                  <td bgcolor="#FFFFFF">&nbsp;
                
					  	<? 
                  		echo $j ;
	 // debug($i); 
	//debug($j);
	$total_count += $j; 
                  		//echo " " . $contentsTrans2[$i]["currencyTo"] ;
                  	?>
					</td>
					
                  </tr>
                <?
              }
			}
			?>
                <!--tr bgcolor="#CCCCCC">
			  <td width="100">&nbsp;</td>
			  <td width="75">&nbsp;</td>
			  <td width="100">&nbsp;</td>
			  <td>&nbsp;</td>
			  <td width="200" align="right"><span class="style3">Total:</span></td>
			  <td width="75"><strong>< echo $totalAmount></strong></td>
			  <td width="50"><strong>< echo $totalFee></strong></td>
			  <td width="75"><strong>< echo $totalComm></strong></td>
			  </tr-->
               
              </table>
		      <table width="835" border="2" bordercolor="#666666" bgcolor="#CCCCCC">
                <tr>
				
			      <td><span class="style3">Total Amount <span class="style1">
			      </span>:</span></td>
                  <td>
				   <!--<span class="style3">
                    <?//=$currencyFrom?>
                  </span>-->
				  <strong><center><?echo number_format($totalAmount,2,'.',',');?></center></strong></td>
				  
			      <!--td width="274"><span class="style1"><strong>
			        <?//=$currencyTo?>:	<?//=$foriegnAmount?>		</strong>      </span></td-->
					 <td width="128"><strong><center><? echo $total_count;?></center></strong></td>
			      <td width="109">&nbsp;</td>
					
			     
			  <!--td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td-->
			
			</tr>
			<tr>
				  <td class="style3" width="200"> Cumulative Total of Amount <span class="style1"> 
                    </span><span class="style1"> </span></td>
				  <td width="86">&nbsp; 
                    <?
				if($_GET["total"]=="first")
				{
				$_SESSION["dgrandTotal"] = $totalAmount;
				$_SESSION["dCurrentTotal"]=$totalAmount;
				}elseif($_GET["total"]=="pre"){
					$_SESSION["dgrandTotal"] -= $_SESSION["dCurrentTotal"];			
					$_SESSION["dCurrentTotal"]=$totalAmount;
				}elseif($_GET["total"]=="next"){
					$_SESSION["dgrandTotal"] += $totalAmount;
					$_SESSION["dCurrentTotal"]= $totalAmount;
				}elseif($_GET["total"]=="")
					{
					$_SESSION["dgrandTotal"]=$totalAmount;
					$_SESSION["dCurrentTotal"]=$_SESSION["dgrandTotal"];
					}
				echo number_format($_SESSION["dgrandTotal"],2,'.',',');
				?>
                  </td>
				 <td height="25" nowrap bgcolor="#C0C0C0">&nbsp;<span class="tab-u"><strong>&nbsp;There 
                    are <? echo $total_count;?>&nbsp;total Records.</span></td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			</tr>
			  				
			
		  </table></td>
        </tr>
		<tr id="actionBtn">
				<td> 
				<div align="center">
					<input type="button" name="Submit2" value="Print This Report" id="printReport">
				</div>
				</td>
			</tr>
		 <?
			} else{ // greater than zero
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Filters shown Above </font>
				</td>
			</tr>
			<? }?>
		</form>
      </table></td>
  </tr>
</table>
</body>
</html>