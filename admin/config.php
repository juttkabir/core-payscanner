<?
// [by JAMSHED]
session_start();
include ("../include/config.php");
include ("security.php");

$condition  = true;
$serverName = $_SERVER['SERVER_NAME'];

if (strstr($serverName, ".test.") || strstr($serverName, ".live.")) {
	$condition = false;	
}

$selfURL = "config.php?msg=Y&success=Y";

if ($_POST['Submit'] == 'Save') {
	foreach ($_POST as $key => $value) {
		if ($key != "Submit") {
			$QryUpdate = "UPDATE " . TBL_CONFIG . " SET 
						`value` = '" . $value . "' WHERE `id` = '" . $key . "'";
			update($QryUpdate);
		}
	}
	insertError(CONFMSG1);
	redirect($selfURL);
} else if ($_POST['Submit'] == 'Restore Default') {
	$qrySelect = "SELECT `id`, `oldValue` FROM " . TBL_CONFIG . " WHERE `client` = '" . CONFIG_CLIENT_NAME . "'";
	$resOldVal = selectMultiRecords($qrySelect);
	for ($i = 0; $i < count($resOldVal); $i++) {
		$updateWithOld = "UPDATE " . TBL_CONFIG . " SET 
						`value` = '".$resOldVal[$i]["oldValue"]."' WHERE `id` = '".$resOldVal[$i]["id"]."'";
		update($updateWithOld);
	}
	insertError(CONFMSG2);
	redirect($selfURL);
}

$query = "SELECT * FROM " . TBL_CONFIG . " WHERE `client` = '" . CONFIG_CLIENT_NAME . "' ORDER BY `id`";
$contentConfig = selectMultiRecords($query);
$allCount = count($contentConfig);

?>
<html>
<head>
	<title>Update Client Configurations</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	
<script language="javascript">

function checkForm() {

<?	for ($i = 0; $i < $allCount; $i++) {  ?>
	if (document.getElementById('<? echo $contentConfig[$i]["id"] ?>').value == "" || IsAllSpaces(document.getElementById('<? echo $contentConfig[$i]["id"] ?>').value)) {
		alert("Please don't left the field Blank.");
		document.getElementById('<? echo $contentConfig[$i]["id"] ?>').focus();
		return false;
	}
<?	}  ?>
	
	return true;
}

function IsAllSpaces(myStr) {
	while (myStr.substring(0,1) == " ") {
		myStr = myStr.substring(1, myStr.length);
	}
	if (myStr == "") {
		return true;
	}
	return false;
}

function showRow(rowID, isVisible) {
	if (isVisible) {
		document.getElementById(rowID).style.display = 'table-row';	
	} else {
		document.getElementById(rowID).style.display = 'none';
	}
}

</script>
	<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<table width="600" border="0" cellspacing="1" cellpadding="5" align="center">
  <form action="config.php" name="frmConfig" id="frmConfig" method="post">
  <tr>
      <td align="center"> 
        <table width="100%" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Manage Client Configurations</strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
		<?	if ($_GET["msg"] != "") {  ?>
		  <tr bgcolor="EEEEEE"> 
            <td colspan="2"> 
              <table width="100%" cellpadding="5" cellspacing="0" border="0">
              	<tr>
              		<td width="40" align="center">
              			<font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font>
              		</td>
              		<td width="560">
              			<? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; $_SESSION['error'] = ""; ?>
             			</td>
             		</tr>
             	</table>
             </td>
      </tr>
		<?	}  ?>
		<tr bgcolor="#ededed">
			<td colspan="2">
	<?	if ($condition) {  ?>
				<!--a href="add-confVar.php" class="style2">Add Config Variable</a-->
	<?	}  ?>
				&nbsp;
			</td>
		</tr>

<?
	if ($allCount > 0) {
		for ($i = 0; $i < $allCount; $i++) {
?>
				<tr bgcolor="#ededed">
					<td width="45%" align="right"><font color="#005b90"><strong><? echo $contentConfig[$i]["description"] ?> </strong></font></td>
					<td width="55%">
						<table width="100%" cellpadding="5" cellspacing="0" border="0">
							<tr>
								<td>
		<?	if ($contentConfig[$i]["isFlag"] == 'Y') {  ?>
						<select name="<? echo $contentConfig[$i]["id"] ?>" id="<? echo $contentConfig[$i]["id"] ?>">
							<option value="0" <? echo ($contentConfig[$i]["value"] == '0' ? "selected" : "") ?>>OFF</option>
							<option value="1" <? echo ($contentConfig[$i]["value"] == '1' ? "selected" : "") ?>>ON</option>
						</select>
		<?	} else {  ?>
						<input type="text" name="<? echo $contentConfig[$i]["id"] ?>" id="<? echo $contentConfig[$i]["id"] ?>" value='<? echo $contentConfig[$i]["value"] ?>'>
		<?	}  ?>
							</td>
							<td align="right">
						<a href="#" onMouseOver="showRow('row<? echo $contentConfig[$i]['id'] ?>', true);" onMouseOut="showRow('row<? echo $contentConfig[$i]['id'] ?>', false);" class="Style2">DETAIL</a>
						<!--a href="#" onMouseOver="return escape('<? echo $contentConfig[$i]["detailDesc"] ?>')" class="Style2">DETAIL</a-->
						</td>
					</tr>
				</table>
					</td>
				</tr>
				
				<tr bgcolor="#ededed" id="row<? echo $contentConfig[$i]['id'] ?>"  style="display:none;">
					<td width="45%" align="right">&nbsp;</td>
					<td width="55%">
						<? echo (trim($contentConfig[$i]['detailDesc']) != "" ? "<br>" . trim($contentConfig[$i]['detailDesc']) : "") ?>
					</td>
				</tr>
<?
		}	
	}
?>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">&nbsp;</td>
		</tr>
        
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" name="Submit" value="Save" onClick="return checkForm();">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="submit" name="Submit" value="Restore Default">
			</td>
		</tr>
		
      </table>
	</td>
  </tr>
</form>
</table>
<script language="JavaScript" type="text/javascript" src="wz_tooltip.js"></script>
</body>
</html>