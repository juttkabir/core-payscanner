<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];

$totalAmount="";

if($offset == "")
	$offset = 0;
//$limit=50;

if($_GET["newOffset"] != "") {
	
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";


$agentType = getAgentType();


////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPA" && $agentType != "SUPAI")
		{
			$condition = True;	
		}
	}else{
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager")
			{
				$condition = True;
			}
		
		}
		
//////////////////////////Making of Condition///////////////

//$_SESSION["middleName"] = $_POST["middleName"];
if($_POST["Submit"] =="Search"||$_GET["search"]=="search")
{
	if($condition)
	{
		$query = "select distinct currencyTo  from ". TBL_SHARE_PROFIT . " where 1";
		$queryCnt = "select count(distinct currencyTo)  from ". TBL_SHARE_PROFIT . " where 1";
		
		 	
	}
	else
	{
		$query = "select distinct currencyTo from ". TBL_SHARE_PROFIT . " where agentID = '".$_SESSION["loggedUserData"]["userID"]."'" ;
		$queryCnt = "select count(distinct currencyTo) from ". TBL_SHARE_PROFIT . " where agentID = '".$_SESSION["loggedUserData"]["userID"]."'" ;
		
	}
	


	if($_GET["search"]!="search")
	{
		
		$_SESSION["rgrandTotal"]="";
		$_SESSION["rCurrentTotal"]="";	

		$fMonth = $_POST["fMonth"];
		$rfDay = $_POST["fDay"];
		$rfYear = $_POST["fYear"];
		
		$rtMonth = $_POST["tMonth"];
		$rtDay = $_POST["tDay"];
		$rtYear = $_POST["tYear"];
		
		$ragentName = $_POST["agentName"];
	}else{
		$fMonth = $_GET["fMonth"];
		$rfDay  = $_GET["fDay"];
		$rfYear = $_GET["fYear"];
		           
		$rtMonth= $_GET["tMonth"];
		$rtDay  = $_GET["tDay"];
		$rtYear = $_GET["tYear"];
		
		$ragentName = $_GET["agentName"];
		}
	$fromDate = $fYear . "-" . $fMonth . "-" . $fDay;
	$toDate   = $tYear . "-" . $tMonth . "-" . $tDay;
	$queryDate = " and (calculationTime >= '$fromDate 00:00:00' and calculationTime <= '$toDate 23:59:59')";
	
	$query .= " $queryDate ";
	$queryCnt .= " $queryDate ";
			
	if($agentName != "")
	{
		if($agentName != "all")
		{
			$query .= " and agentID = '".$agentName."' ";
			$queryCnt .= " and agentID = '".$agentName."' ";
		}
	}	
	
	$query .= "  order by calculationTime DESC";
	$contentsCurrency = selectMultiRecords($query);
	$allCount = countRecords($queryCnt);
	
}
	
	
	

//echo $query .  count($contentsTrans);
?>
<html>
<head>
	<title>Profit Sharing Report</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}	
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#000000" size="2">Profit Sharing Report</td>
  </tr>
  
  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
        <form action="profit-sharing-report.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search Filters 
              </strong></span></td>
        </tr>
        <tr>
        <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$fDay?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$fMonth?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$fYear?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$tDay?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$tMonth?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$tYear?>");
        </script>
        <br>
		<?  
		if($condition)
		{
		?>
				  User Name 
				  <select name="agentName" style="font-family:verdana; font-size: 11px;">
			  <option value="">- Select One-</option>
			  <option value="all">All</option>
				<?
				$agentQuery = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' ";
				if($agentType == "Branch Manager")
				{
					$agentQuery .= " and parentID = '$parentID' ";
				}
				$agentQuery .=" order by agentCompany";
				
						$agents = selectMultiRecords($agentQuery);
					for ($i=0; $i < count($agents); $i++){
				?>
				<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
				<?
					}
				?>
			  </select><script language="JavaScript">
			SelectOption(document.Search.agentName, "<?=$agentName?>");
				</script>
			</select>            
		<? }//End Admin If?>
        <input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
	  <br>
      <br>
      
       <form action="profit-sharing-report.php" method="post" name="trans">
		<table width="700" border="1" cellpadding="0" bordercolor="#666666">
		<?
		if(count($contentsCurrency) > 0)
		{		
			
		?>
		<tr>
            <td height="25" bgcolor="#C0C0C0" bordercolor="#000000"><span class="tab-u"><strong>&nbsp;Report 
              Dated <? echo date("m.d.y");?></strong></span></td>
        </tr>
        <tr>
            <td height="25" nowrap bgcolor="#C0C0C0" align="center"><span class="tab-u"><strong> Profit Sharing Report </strong></span></td>
        </tr>                      
        <tr>                       
            <td height="25" nowrap bgcolor="#C0C0C0" align="center"><span class="tab-u"><? if($agentName != "all" || $agentName != ""){$agentNa = selectFrom("select username,name from " . TBL_ADMIN_USERS . " where userID='".$agentName."'");echo " ".$agentNa["username"];}else {echo(" for All agents");}?> </span></td>
        </tr>                      
        <tr>                       
            <td height="25" nowrap bgcolor="#C0C0C0" align="center">
              <span class="tab-u">
              	<? if($fMonth != "" && $fromDate < $toDate){echo "Between Dates".$fMonth."-".$fDay."-".$fYear." and ".$tMonth."-".$tDay."-".$tYear;}?>
              </span></td>
        </tr>
        <tr>
          <td nowrap bgcolor="#EFEFEF">
		  <table border="1" bordercolor="#EFEFEF" width="100%">
			
			<tr bgcolor="#FFFFFF">		 
			 
			  <td ><span class="style1">User Name </span></td>
			  <td><span class="style1">User Type</span></td>
			  <td><span class="style1">Total Transactions</td>
			  <td ><span class="style1">Income </span></td>			  
			  <td ><span class="style1">Currency </span></td>			  
			  </tr>
		    <? for($i = 0;$i < count($contentsCurrency);$i++)
			{
				$currencyTo = $contentsCurrency[$i]["currencyTo"];
				
				$queryProfit = "select count(transID) as transCount, sum(agentMargin) as Margin, agentID from ". TBL_SHARE_PROFIT . " where currencyTo = '$currencyTo' $queryDate";
				
				if($agentName != "")
				{
					if($agentName != "all")
					{
						$queryProfit .= " and agentID = '".$agentName."' ";
						
					}
				}	
	
				$queryProfit .= " group by agentID ";
				$contentsProfit = selectMultiRecords($queryProfit);		
				$totaltrans = 0;
				$totalAgentMargin = 0;
				for($j = 0; $j < count($contentsProfit);$j++)
				{	
				 $totaltrans += $contentsProfit[$j]["transCount"];
				 $totalAgentMargin += $contentsProfit[$j]["Margin"];
				 
				 $userDetail = selectFrom("select username, name, agentCompany, isCorrespondent, agentType from ".TBL_ADMIN_USERS." where userID = '".$contentsProfit[$j]["agentID"]."'");
				 $userType = "";
				 if($userDetail["agentType"] == 'Sub') 
				 {
				 	$userType .= "Sub";
				}else{
					$userType .= "Super";
					}
					
				if($userDetail["isCorrespondent"] == 'ONLY') 
				 {
				 	$userType .= " Distributor";
				}elseif($userDetail["isCorrespondent"] == 'N') {
					$userType .= " Agent";
				}else{
					$userType .= " A&D";
					}
				$userName = $userDetail["agentCompany"]." [".$userDetail["username"]."]";
				  	
				?>
				<tr bgcolor="#FFFFFF">				 
				  <td bgcolor="#FFFFFF"><?=$userName?></td>
				  <td bgcolor="#FFFFFF"> <?=$userType?></td>				 
				  <td bgcolor="#FFFFFF"><?=$contentsProfit[$j]["transCount"]?></td>
				  <td bgcolor="#FFFFFF"><? echo number_format($contentsProfit[$j]["Margin"],2,'.',',');?></td>
				  <td bgcolor="#FFFFFF"><?=$currencyTo?></td>
			    </tr>
				<?
				}
				?>
				<tr bgcolor="#FFFFFF">				 
				  <td bgcolor="#FFFFFF">Total</td>
				  <td bgcolor="#FFFFFF">&nbsp;</td>				 
				  <td bgcolor="#FFFFFF"><?=$totaltrans?></td>
				  <td bgcolor="#FFFFFF"><? echo number_format($totalAgentMargin,2,'.',',');?></td>
				  <td bgcolor="#FFFFFF">&nbsp;</td>
			    </tr>
			<?
			}
			?>
		    </table>
		 </td>
        </tr>
		<tr>
		  	<td> 
				<div align="center">
					<input type="button" name="Submit2" value="Print This Report" onClick="print()">
				</div>
			</td>
		  </tr>
		  <?
			}else{ // greater than zero
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Filters shown Above </font>
				</td>
			</tr>
			<? }?>
			</table>
		</form>
      
</body>
</html>
