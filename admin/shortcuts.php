<?

// [by JAMSHED]

session_start();
include ("../include/config.php");
include ("security.php");

$username = $_SESSION["loggedUserData"]["username"];

if ($_GET["refreshHeader"] != "") {
?>
<script language="javascript">
	parent.frames["topFrame"].window.location.reload();
</script>
<?
}

if ($_POST["Submit"] == "Delete Shortcuts") {
	if (is_array($_POST["shortcutID"])) {
		foreach ($_POST["shortcutID"] AS $key => $value) {
			$delQuery = "DELETE FROM " . TBL_SHORTCUTS . " WHERE `shortcutID` = '".$value."'";
			deleteFrom($delQuery);
		}
		insertError("Selected shortcuts are deleted successfully.");
		redirect("shortcuts.php?msg=Y&success=Y&refreshHeader=Y");
	} else {
		$msg2 = CAUTION_MARK . " Please check atleast one checkbox to delete shortcut.";	
	}
}

$SQL_Query = "SELECT * FROM " . TBL_SHORTCUTS . " WHERE `username` = '$username'";
$shortcuts = selectMultiRecords($SQL_Query);
$allCount  = count($shortcuts);

?>
<html>
<head>
	<title>Shortcuts</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
	
	var total = <? echo $allCount ?>;
	
	function checkAll() {
		
		if (document.getElementById("All").checked == true) {
			for (var i = 0; i < total; i++) {
				document.getElementById('shortcutID[' + i + ']').checked = true;
			}
		} else {
			for (var i = 0; i < total; i++) {
				document.getElementById('shortcutID[' + i + ']').checked = false;
			}
		}
		
	}
	
	function checkForm() {
		
		var flag  = false;
		
		for (var i = 0; i < total; i++) {
			if (document.getElementById('shortcutID[' + i + ']').checked == true) {
				flag = true;	
			}
		}
		
		if (flag) {
			if (confirm("Are you sure you want to delete the checked shortcut(s).")) {
				return true;
			} else {
				return false;	
			}
		} else {
			alert("No shortcut is checked for deletion.");
			return false;
		}
		
	}
		
	// end of javascript -->
	</script>
	
  <style type="text/css">
<!--
.style2 {color: #6699CC;
	font-weight: bold;
-->
  </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	<?	if ($msg2 != "") {  ?>
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
                <td colspan="2" align="center" class="tab-r"><? echo $msg2 ?></td>
              </tr>
            </table></td>
          </tr>
	<?	} else if ($_GET["msg"] != "") {  ?>
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
                <td width="40" align="center">&nbsp;</td>
                <td width="635" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"] != "" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font>&nbsp;&nbsp;<? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?></td>
              </tr>
            </table></td>
          </tr>
	<?	}  ?>
  <form action="shortcuts.php" method="post" onSubmit="return checkForm();">
  <tr>
    <td align="center">
		<table width="527" border="0" cellspacing="1" cellpadding="1" align="center">
          <tr bgcolor="#DFE6EA"> 
          <td colspan="3" bgcolor="#000000">
		  <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>My Shortcuts</strong></font></td>
			</tr>
		  </table>
		</td>
          </tr>
          <tr bgcolor="#eeeeee"> 
            <td colspan="3"><a href="add-shortcuts.php?for=shortcut" class="style2">Add New Shortcut</a></td>
          </tr>
<?	if ($allCount > 0) {  ?>
          <tr bgcolor="#DFE6EA"> 
            <td align="center"><input type="checkbox" name="All" id="All" onClick="checkAll();"></td>
            <td><font color="#005b90"><strong>Link</strong></font></td>
            <td><font color="#005b90"><strong>Menu</strong></font></td>
          </tr>
<?
			for ($i = 0 ; $i < count($shortcuts) ; $i++) {
?>
          <tr bgcolor="#eeeeee"> 
            <td align="center"><input type="checkbox" name="shortcutID[<?=$i;?>]" id="shortcutID[<?=$i;?>]" value="<? echo $shortcuts[$i]["shortcutID"] ?>"></td>
            <td><font color="#005b90"><? echo $shortcuts[$i]["link_title"] ?></font></td>
            <td><font color="#005b90"><? echo $shortcuts[$i]["menu"] ?></font></td>
          </tr>
	<?
			}
	?>
          <tr bgcolor="#eeeeee"> 
            <td colspan="3" align="center"> <input type="submit" name="Submit" value="Delete Shortcuts"> 
            </td>
          </tr>
<?	} else {  ?>
          <tr bgcolor="#eeeeee"> 
            <td colspan="3" align="center"> No shortcuts are found. </td>
          </tr>
<?	}  ?>
     </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>