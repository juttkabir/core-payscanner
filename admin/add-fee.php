<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];

$agentRepository = new \Payex\Repository\AgentRepository($app->getDb());
$agentHelper = new \Payex\Helper\AgentHelper();
$agent = $agentRepository->getById($userID);

//$agentType = getAgentType();
 $where_sql="";
if($_SESSION["loggedUserData"]['adminType']=='Agent')
{

 $where_sql="   AND userID='".	$userID."' ";
}

//print_r( $where_sql);
$countryBasedFlag = false;
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}

$_SESSION["distributor"] = '';

if($_POST["distributor"]!= "")
{
	$_SESSION["distributor"] = $_POST["distributor"];
	$_SESSION["agentID"] = '';
}

if ($_POST["feeID"] == "")
{
	// Code modified by Usman Ghani aginst ticket #3319: Minas Center - Payment Mode Commission Rates
	// Code under this condition has been modified against in order to retain the original values
	// in session variables in case if such corresponding values are not available in post request.
	$_SESSION["origCountry"] = 		!empty($_POST["origCountry"]) ? $_POST["origCountry"] : $_SESSION["origCountry"];
	$_SESSION["destCountry"] = 		!empty($_POST["destCountry"]) ? $_POST["destCountry"] : $_SESSION["destCountry"];
	$_SESSION["amountRangeFrom"] = 	!empty($_POST["amountRangeFrom"]) ? $_POST["amountRangeFrom"] : $_SESSION["amountRangeFrom"];
	$_SESSION["amountRangeTo"] = 	!empty($_POST["amountRangeTo"]) ? $_POST["amountRangeTo"] : $_SESSION["amountRangeTo"];
	$_SESSION["Fee"] = 				!empty($_POST["Fee"]) ? $_POST["Fee"] : $_SESSION["Fee"];
	$_SESSION["feeType"] = 			!empty($_POST["feeType"]) ? $_POST["feeType"] : $_SESSION["feeType"];
	$_SESSION["payinFee"] = 		!empty($_POST["payinFee"]) ? $_POST["payinFee"] : $_SESSION["payinFee"];
	$_SESSION["interval"] = 		!empty($_POST["interval"]) ? $_POST["interval"] : $_SESSION["interval"];
	$_SESSION["agentID"] = 			!empty($_POST["agentID"]) ? $_POST["agentID"] : $_SESSION["agentID"];
	$_SESSION["feeBasedOn"] = 		!empty($_POST["feeBasedOn"]) ? $_POST["feeBasedOn"] : $_SESSION["feeBasedOn"];
	$_SESSION["FeeFor"] = 		!empty($_POST["FeeFor"]) ? $_POST["FeeFor"] : $_SESSION["FeeFor"];
	$_SESSION["currencyDest"] = 	!empty($_POST["currencyDest"]) ? $_POST["currencyDest"] : $_SESSION["currencyDest"];
	$_SESSION["currencyOrigin"] = 	!empty($_POST["currencyOrigin"]) ? $_POST["currencyOrigin"] : $_SESSION["currencyOrigin"];
	$_SESSION["transactionType2"] = !empty($_REQUEST["transactionType2"]) ? $_REQUEST["transactionType2"] : $_SESSION["transactionType2"];
	$_SESSION["catID"] = !empty($_REQUEST["catID"]) ? $_REQUEST["catID"] : $_SESSION["catID"];
	// End of code modification aginst ticket #3319: Minas Center - Payment Mode Commission Rates.
}

?>
<html>
<head>
	<title>Add <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?></title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="./javascript/jquery.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {

	if(theForm.origCountry.options.selectedIndex == 0){
		alert("Please select originating Country.");
		theForm.origCountry.focus();
		return false;
	}

	if(theForm.destCountry.options.selectedIndex == 0){
		alert("Please select Destination Country.");
		theForm.destCountry.focus();
		return false;
	}

	if(theForm.amountRangeFrom.value == "" || IsAllSpaces(theForm.amountRangeFrom.value)){
    	alert("Please provide Lower range of money.");
        theForm.amountRangeFrom.focus();
        return false;
    }
	if(theForm.amountRangeTo.value == "" || IsAllSpaces(theForm.amountRangeTo.value)){
    	alert("Please provide Upper range of money.");
        theForm.amountRangeTo.focus();
        return false;
    }
  if(theForm.feeType.options.selectedIndex == 0){
		alert("Please select Fee Type.");
		theForm.feeType.focus();
		return false;
	}
<?
if(CONFIG_FEE_BASED_COLLECTION_POINT == 1){
?>
	if(theForm.feeType.value == "collectionPoint"){

		if(theForm.distributor.options.selectedIndex == 0){
			alert("Please select Distributor from the list.");
			theForm.distributor.focus();
			return false;
		}
		if(theForm.agentID.options.selectedIndex == 0){
			alert("Please select Collection Point from the list.");
			theForm.agentID.focus();
			return false;
		}
	}
<?
}
/**
 * Adding the payment mode based commssion
 * Ticket# 3319
 */
if(CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1")
{
?>
	if(theForm.paymentMode.options.selectedIndex == 0){
			alert("Please select the payment mode");
			theForm.paymentMode.focus();
			return false;
		}
<?
}
?>

/*	if(int(theForm.amountRangeTo.value) < int(theForm.amountRangeFrom.value)){
    	alert("Please provide valid Upper range of money.");
        theForm.amountRangeTo.focus();
        return false;
	}*/
	if(theForm.Fee.value == "" || IsAllSpaces(theForm.Fee.value)){
    	alert("Please provide Fee for spcified money ranges.");
        theForm.Fee.focus();
        return false;
    }

	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }

  function checkFocus(){
 	fee.origCountry.focus();
}
	// end of javascript -->
	</script>
</head>
<body onLoad="checkFocus();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">Add <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></td>
  </tr>
  <form action="add-fee-conf.php" method="post"onSubmit="return checkForm(this);" name="fee">
  <tr>
      <td align="center">
        <table width="500" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="500" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Add
                     <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
		<? if ($_GET["msg"] != ""){ ?>
		<tr bgcolor="#ededed">
            <td colspan="2" bgcolor="#EEEEEE">
<table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td><td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br></font>"; ?></td></tr></table></td></tr><? } ?>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center"><font color="#FF0000">* Compulsory
              Fields</font></td>
		</tr>





		<?php if (CONFIG_CLIENT_NAME == 'SPI Payscanner') {?>
		<!-- Code By Mahmood Ali Waseem againset ticket # 11209-->

		 <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Fee For<font color="#ff0000">*</font></strong></font></td>
            <td width="210">
			<!--onchange="ShowPaymentDetail(this.value)"-->
			<SELECT name="FeeFor" id="FeeFor" style="font-family:verdana; font-size: 11px"  onChange="document.forms[0].action='add-fee.php'; document.forms[0].submit()" >
               <OPTION value="deal_contract"  >Select FeeFor</OPTION>
				<OPTION value="deal_contract" <?php if ($_SESSION['FeeFor'] == 'deal_contract') {echo 'selected';}?> >Deal Contract</OPTION>
				<OPTION value="payments" <?php if ($_SESSION['FeeFor'] == 'payments') {echo 'selected';}?>>Payments</OPTION>
			</SELECT>
             </td>
        </tr>

		<?php if (isset($_SESSION["FeeFor"]) && $_SESSION["FeeFor"] == "payments") {?>
		<tr bgcolor="#ededed" id="p_type" >
            <td width="285"><font color="#005b90"><strong>Payment Type<font color="#ff0000">*</font></strong></font></td>
            <td width="210">

			<SELECT  name="Payment_Type" id="Payment_Type" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- Select -</OPTION>
				<OPTION value="sepa">SEPA</OPTION>
				<OPTION value="uk3day">UK 3 days</OPTION>
				<OPTION value="sameDay">UK same day</OPTION>
				<OPTION value="international">International</OPTION>


			</SELECT>
             </td>
        </tr>

		<tr bgcolor="#ededed"  id="p_charges" >
            <td width="285"><font color="#005b90"><strong>Charges<font color="#ff0000">*</font></strong></font></td>
            <td width="210">
			<!---->
			<input type="number" maxlength="16"  id="PaymentFee" size="15" value="" onkeypress="return isNumber(event)" name="Fee">
             </td>
        </tr>

		<tr bgcolor="#ededed" id="p_currency" >
            <td width="285"><font color="#005b90"><strong>Currency<font color="#ff0000">*</font></strong></font></td>
            <td>
            	<select name="PaymentCurrency" disabled>
				<option value="">-- Select Currency --</option>
						  <?

							$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";

							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());

							while($rstRow = mysql_fetch_array($nResult))
							{
								$country = $rstRow["country"];
								$currency = $rstRow["currencyName"];
								$description = $rstRow["description"];
								$erID  = $rstRow["cID"];
								if($currency == 'GBP' )
									echo "<option selected value ='$currency'> ".$currency."</option>";
								else
								    echo "<option value ='$currency'> ".$currency."</option>";
							}
						  ?>
				</select>
				<script language="JavaScript">
     			SelectOption(document.forms[0].currencyOrigin, "<?=$_SESSION["currencyOrigin"]?>");
     	  </script>
            	<!--input type="text" name="currency" size="15" maxlength="255" --></td>
        </tr>

		<tr bgcolor="#ededed" id="p_pay_per" >
            <td width="285"><font color="#005b90"><strong>Pay Per<font color="#ff0000">*</font></strong></font></td>
            <td width="210">

			<SELECT name="PayPer"  style="font-family:verdana; font-size: 11px">


				<OPTION value="payments">Payments</OPTION>
				<OPTION value="Transactions">Transaction</OPTION>
			</SELECT>
             </td>
        </tr>


		<?php }}?>

		<!-- Code By Mahmood Ali Waseem againset ticket # 11209-->

<!--///////////////////////////-->
		<?php if (isset($_SESSION["FeeFor"]) && $_SESSION["FeeFor"] == "deal_contract") {?>
				<tr bgcolor="#ededed" id="c_ocountry">
					<td width="285"><font color="#005b90"><strong>Originating Country<font color="#ff0000">*</font></strong></font></td>
					<td width="210">
						<?
						// Added by Usman Ghani aginst ticket #3319: Minas Center - Payment Mode Commission Rates
						// Get the selected value for the list box of payment modes below.
						$selectedOrigCountry = "";
						if ( !empty($_REQUEST["origCountry"]) )
						{
							$selectedOrigCountry = $_REQUEST["origCountry"];
						}
						else if ( !empty($_SESSION["origCountry"]) )
						{
							$selectedOrigCountry = $_SESSION["origCountry"];
						}

						// End of code aginst ticket #3319: Minas Center - Payment Mode Commission Rates
						?>
						<SELECT name="origCountry" id="origCountry" style="font-family:verdana; font-size: 11px">
							<OPTION value="">- Select Country-</OPTION>
							<?php
							$countryTypes = " and  countryType like '%origin%' ";
							if(CONFIG_COUNTRY_SERVICES_ENABLED
								&& !($agentType == 'SUPA' || $agentType == 'SUBA')
							){
								$countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = fromCountryId  $countryTypes order by countryName";
								if($countryBasedFlag){
									$countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE_NEW." where 1 AND countryId = fromCountryId $countryTypes order by countryName";
								}
								$countiresOrigin = selectMultiRecords($countiresQuery);
							} elseif (($agentType == 'SUPA' || $agentType == 'SUBA')){
								$countryData = [0 => $agent->getCustCountries()];
								$originationCountries = $agentHelper->getAgentRelatedCountries($countryData);
								foreach ($originationCountries as $originationCountry){
									$selected = $selectedOrigCountry == $originationCountry ? ' selected="selected"' : '';
									echo '<option value="' . $originationCountry . '"' . $selected . '>' . $originationCountry . '</option>';
								}
								unset($selected);
							} else {
								$countiresOrigin = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
							}
							$countriesOriginArr = array();
							if ($countiresOrigin){
								for ($i=0; $i < count($countiresOrigin); $i++){
									$countriesOriginArr[] = $countiresOrigin[$i]["countryId"];
									$selected = $selectedOrigCountry == $countiresOrigin[$i]["countryName"] ? " selected='selected'" : "";
									echo '<option value="' . $countiresOrigin[$i]["countryName"] . '"' . $selected . '>' . $countiresOrigin[$i]["countryName"] . '</option>';
								}
							}
							?>
						</SELECT>

						<script type="text/javascript">
							SelectOption(document.forms[0].origCountry, "<?=$_SESSION["origCountry"]?>");
						</script>
					</td>
				</tr>
				<?
				if(CONFIG_FEE_CURRENCY == '1')
				{
					?>
					<tr bgcolor="#ededed" >
						<td width="185"><font color="#005b90"><strong>Originating Currency<font color="#ff0000">*</font></strong></font></td>
						<td>
							<select name="currencyOrigin">
								<option value="">-- Select Currency --</option>
								<?

								$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
								/*							if($countryBasedFlag && !empty($countriesOriginArr)){
                                                                for($s=0;$s<count($countriesOriginArr);$s++){
                                                                    $serviceCurr = selectMultiRecords("select currencyRec from ".TBL_SERVICE_NEW." where fromCountryId='".$countriesOriginArr[$s]."' AND currencyRec!='' ");
                                                                    if(count($serviceCurr)>0){
                                                                        for($se=0;$se<count($serviceCurr);$se++){
                                                                            $currenctRecs = implode(",",$serviceCurr[$se]["currencyRec"]);
                                                                        }
                                                                    }
                                                                }
                                                                if($currenctRecs!=""){
                                                                    $currencyArr = implode(",",array_values(array_unique(explode(",",$currenctRecs))));
                                                                    $strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies where currencyName IN(".$currencyArr.") ORDER BY currencyName ";
                                                                }
                                                            }*/
								$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());

								while($rstRow = mysql_fetch_array($nResult))
								{
									$country = $rstRow["country"];
									$currency = $rstRow["currencyName"];
									$description = $rstRow["description"];
									$erID  = $rstRow["cID"];
									if($_SESSION["countryOrigin"] == $rstRow["country"] && $_SESSION["currencyOrigin"] == "")
										echo "<option selected value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";
									else
										echo "<option value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";
								}
								?>
							</select>
							<script language="JavaScript">
								SelectOption(document.forms[0].currencyOrigin, "<?=$_SESSION["currencyOrigin"]?>");
							</script>
							<!--input type="text" name="currency" size="15" maxlength="255" --></td>
					</tr>
					<?
				}
				?>
				<tr bgcolor="#ededed" id="c_dcountry">
					<td><font color="#005b90"><strong>Destination Country<font color="#ff0000">*</font></strong></font></td>
					<td>
						<?
						// Added by Usman Ghani aginst ticket #3319: Minas Center - Payment Mode Commission Rates
						// Get the selected value for the list box of payment modes below.
						$selectedDestCountry = "";
						if ( !empty($_REQUEST["destCountry"]) )
						{
							$selectedDestCountry = $_REQUEST["destCountry"];
						}
						else if ( !empty($_SESSION["destCountry"]) )
						{
							$selectedDestCountry = $_SESSION["destCountry"];
						}
						// End of code aginst ticket #3319: Minas Center - Payment Mode Commission Rates
						?>

						<SELECT name="destCountry" id="destCountry" style="font-family:verdana; font-size: 11px">
							<OPTION value="">- Select Country-</OPTION>
							<?php
							if(CONFIG_SHARE_OTHER_NETWORK == '1'){
								$sharedCountry = selectMultiRecords("select * from ".TBL_SHARE_COUNTRIES." where 1");
								$existingCountries = "";
								for($k=0; $k < count($sharedCountry); $k++){
									if($k > 0){
										$existingCountries .=",";
									}
									$existingCountries .= $sharedCountry[$k]["countryId"];
								}
							}

							//if(CONFIG_ENABLE_ORIGIN == "1"){

							$countryTypes = " and  countryType like '%destination%' ";
							//}else{
							//	$countryTypes = " ";
							//}

							if(CONFIG_COUNTRY_SERVICES_ENABLED){
								$serviceTable = TBL_SERVICE;
								if($countryBasedFlag){
									$serviceTable = TBL_SERVICE_NEW;
								}
								$countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".$serviceTable." where 1 and countryId = toCountryId  $countryTypes ";
							}else{
								$countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes ";
							}
							if(CONFIG_SHARE_OTHER_NETWORK == '1'){
								if($existingCountries != ""){
									$countiresQuery .=" and countryId not in (".$existingCountries.") ";
								}
							}
							$countiresQuery .=" order by countryName ";

							if (($agentType == 'SUPA' || $agentType == 'SUBA')){
								$countryData = [0 => $agent->getIDACountry()];
								$originationCountries = $agentHelper->getAgentRelatedCountries($countryData);
								foreach ($originationCountries as $originationCountry){
									$selected = $selectedOrigCountry == $originationCountry ? ' selected="selected"' : '';
									echo '<option value="' . $originationCountry . '"' . $selected . '>' . $originationCountry . '</option>';
								}
								unset($selected);
							}else{
								$countiresDest = selectMultiRecords($countiresQuery);
								for ($i=0; $i < count($countiresDest); $i++){
									$selected = $selectedOrigCountry == $countiresDest[$i]["countryName"] ? " selected='selected'" : "";
									echo '<option value="' . $countiresDest[$i]["countryName"] . '"' . $selected . '>' . $countiresDest[$i]["countryName"] . '</option>';
								}
							}
							?>
						</SELECT>
						<script type="text/javascript">
							SelectOption(document.forms[0].destCountry, "<?=$_SESSION["destCountry"]?>");
						</script>
					</td>
				</tr>
				<?php if(CONFIG_FEE_CURRENCY == '1'){ ?>
					<tr bgcolor="#ededed">
						<td width="185"><font color="#005b90"><strong>Destination Currency<font color="#ff0000">*</font></strong></font></td>
						<td>
							<select name="currencyDest">
								<option value="">-- Select Currency --</option>
								<?

								$strQuery = "SELECT DISTINCT(currencyName), description  FROM  currencies ORDER BY currencyName ";
								$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());

								while($rstRow = mysql_fetch_array($nResult))
								{
									$currency = $rstRow["currencyName"];
									$description = $rstRow["description"];
									$erID  = $rstRow["cID"];
									if($selectedCurrency == $currency)
									{
										echo "<option value ='$currency' selected> ".$currency." --> ".$description."</option>";
									}
									else
									{
										echo "<option value ='$currency'> ".$currency." --> ".$description."</option>";
									}
								}
								?>
							</select>
							<script language="JavaScript">
								SelectOption(document.forms[0].currencyDest, "<?=$_SESSION["currencyDest"]?>");
							</script>
							<!-- input type="text" name="currency" size="15" maxlength="255" --></td>
					</tr>
					<?
				}
				?>
				<!-- Added by Niaz Ahmad Against Ticket # 2538 at 24-10-2007-->
				<? if (CONFIG_FEE_BASED_DROPDOWN == "1") { ?>
					<tr bgcolor="#ededed" id="c_fee_base">
						<td width="285"><font color="#005b90"><strong>Fee Base<font color="#ff0000">*</font></strong></font></td>
						<td width="210"><SELECT name="feeBasedOn" id="feeBasedOn" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].action='add-fee.php'; document.forms[0].submit()">
								<OPTION value="Generic">Generic</OPTION>
								<OPTION value="transactionType">Transaction Type</OPTION>
								<? if(CONFIG_CUSTOMER_CATEGORY == 1){?>
									<OPTION value="customer">Customer Category</option>
								<? }?>
							</SELECT>
							<script language="JavaScript">
								SelectOption(document.forms[0].feeBasedOn, "<?=$_SESSION["feeBasedOn"]?>");
							</script>
						</td>
					</tr>
					<? if ($_SESSION["feeBasedOn"]== "transactionType") { ?>
						<tr bgcolor="#ededed">
							<td width="285"><font color="#005b90"><strong>Transaction Type<font color="#ff0000">*</font></strong></font></td>
							<td width="210"><SELECT name="transactionType2" id="transactionType2" style="font-family:verdana; font-size: 11px">
									<OPTION value="Pick up">Pick up</OPTION>
									<OPTION value="Bank Transfer">Bank Transfer</OPTION>
									<OPTION value="Home Delivery">Home Delivery</OPTION>
								</SELECT>
								<script language="JavaScript">SelectOption(document.forms[0].transactionType2, "<?=$_SESSION["transactionType2"]?>");</script>
							</td>
						</tr>

						<?
					} if($_SESSION["feeBasedOn"] == "customer"){
						?>

						<tr bgcolor="#ededed">
							<td><font color="#005b90"><strong>Select Customer Category<font color="#ff0000">*</font></strong></font></td>
							<td>
								<select name="catID" id="catID" style="font-family:verdana; font-size: 11px">
									<option value="">-Select One-</option>
									<?
									$custCatSql = "select id,name from ".TBL_CUSTOMER_CATEGORY." where enabled = 'Y'";
									$category = selectMultiRecords($custCatSql);
									for($k =0; $k <count($category); $k++){
										?>
										<option value="<?=$category[$k]["id"];?>" <? echo ($_SESSION["catID"]==$category[$k]["id"] ? "selected" : "")?>>
											<?  echo($category[$k]["name"]); ?></option>
									<? } ?>
								</select>
								<script language="JavaScript">
									SelectOption(document.forms[0].catID, "<?=$_SESSION["catID"];?>");
								</script>
							</td>
						</tr>

						<?
					}
				} // config if condition close

				$refreshFlag = false;
				if(CONFIG_FEE_DENOMINATOR == 1 || CONFIG_FEE_BASED_COLLECTION_POINT == 1 || CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1 || CONFIG_FEE_DISTRIBUTOR == 1)
				{
					$refreshFlag = true;
				}
				?>
				<tr bgcolor="#ededed" id="c_amount_range">
					<td width="285"><font color="#005b90"><strong>Amount Range<font color="#ff0000">*</font></strong></font></td>
					<td><input type="text" name="amountRangeFrom" value="<?=stripslashes($_SESSION["amountRangeFrom"]);?>" size="10" maxlength="16"> To <input type="text" name="amountRangeTo" value="<?=stripslashes($_SESSION["amountRangeTo"]);?>" size="10" maxlength="16"></td>
				</tr>
				<tr bgcolor="#ededed">
					<td width="285"><font color="#005b90"><strong><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?> Type<font color="#ff0000">*</font></strong></font></td>
					<td width="210">
						<?
						// Added by Usman Ghani aginst ticket #3319: Minas Center - Payment Mode Commission Rates
						// Get the selected value for the list box of payment modes below.
						$selectedFeeType = "";
						if ( !empty($_REQUEST["feeType"]) )
						{
							$selectedFeeType = $_REQUEST["feeType"];
						}
						else if ( !empty($_SESSION["feeType"]) )
						{
							$selectedFeeType = $_SESSION["feeType"];
						}
						// End of code aginst ticket #3319: Minas Center - Payment Mode Commission Rates
						?>
						<?php if ($agentType == "SUPA" || $agentType == "SUBA") { ?>
							<SELECT name="feeType" id="feeType" style="font-family:verdana; font-size: 11px" <? if($refreshFlag){echo ("onChange=\"document.forms[0].action='add-fee.php'; document.forms[0].submit();\"");}?>>
								<OPTION value="">- Select <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?> Type -</OPTION>
								<? if(CONFIG_FEE_AGENT == 1){ // removed  (&& CONFIG_FEE_DISTRIBUTOR != 1) from the existing condition against ticket #3399 Money Talks - Fee Management Module done by khola.
									?>
									<option value="agentBased" <?=($selectedFeeType == "agentBased" ? "selected='selected'" : "");?> >Agent Based <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
							</SELECT>
						<?php } else { ?>
							<SELECT name="feeType" id="feeType" style="font-family:verdana; font-size: 11px" <? if($refreshFlag){echo ("onChange=\"document.forms[0].action='add-fee.php'; document.forms[0].submit();\"");}?>>
								<OPTION value="">- Select <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?> Type -</OPTION>
								<option value="fixed">Fixed <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<option value="percent" <?=($selectedFeeType == "percent" ? "selected='selected'" : "");?> >Percentage of Amount</option>
								<? if(CONFIG_FEE_DENOMINATOR == 1){?>
									<option value="denominator" <?=($selectedFeeType == "denominator" ? "selected='selected'" : "");?> >Denomination Based <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
								<? if(CONFIG_FEE_AGENT_DENOMINATOR == 1){ // removed  (&& CONFIG_FEE_DISTRIBUTOR != 1) from the existing condition against ticket #3399 Money Talks - Fee Management Module done by khola.
									?>
									<option value="agentDenom" <?=($selectedFeeType == "agentDenom" ? "selected='selected'" : "");?> >Agent and Denomination Based <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
								<? if(CONFIG_FEE_AGENT == 1){ // removed  (&& CONFIG_FEE_DISTRIBUTOR != 1) from the existing condition against ticket #3399 Money Talks - Fee Management Module done by khola.
									?>
									<option value="agentBased" <?=($selectedFeeType == "agentBased" ? "selected='selected'" : "");?> >Agent Based <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
								<? if(CONFIG_FEE_DISTRIBUTOR == 1){?>
									<option value="distributorBased" <?=($selectedFeeType == "distributorBased" ? "selected='selected'" : "");?> >Distributor Based <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
								<? if(CONFIG_FEE_BASED_COLLECTION_POINT == 1){?>
									<option value="collectionPoint" <?=($selectedFeeType == "collectionPoint" ? "selected='selected'" : "");?> >Collection Point <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?></option>
								<? }?>
							</SELECT>
						<?php } ?>
						<script language="JavaScript">
							SelectOption(document.forms[0].feeType, "<?=$_SESSION["feeType"]?>");
						</script></td>

				</tr>

				<? if(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1
					&& ($_SESSION["feeType"] == "denominator"
						|| $_SESSION["feeType"] == "agentDenom"
						|| $_SESSION["feeType"] == "agentBased"	)) { ?>
					<tr bgcolor="#ededed">
						<td width="285"><font color="#005b90"><strong>Fee Sub Type<font color="#ff0000">*</font></strong></font></td>
						<td>
							<select name="feeSubType">
								<option value="fix"> - FIXED - </option>
								<option value="percent"> - PERCENTAGE - </option>
							</select>
						</td>
					</tr>

				<? } ?>

				<?
				if($_SESSION["feeType"] == "denominator" || $_SESSION["feeType"] == "agentDenom"){?>
					<tr bgcolor="#ededed">
						<td width="285"><font color="#005b90"><strong>Denominator[Interval]<font color="#ff0000">*</font></strong></font></td>
						<td><input type="text" name="interval" value="<?=stripslashes($_SESSION["interval"]);?>" size="15" maxlength="16"></td>
					</tr>
				<? }?>


				<? if($_SESSION["feeType"] == "agentDenom" || $_SESSION["feeType"] == "agentBased" || $_SESSION["feeType"] == "distributorBased")
				{
					?>

					<tr bgcolor="#ededed">
						<!--
                        Below  condition $feeType == "distributorBased" was CONFIG_FEE_DISTRIBUTOR == "1" initially have changed it since if CONFIG_FEE_DISTRIBUTOR and CONFIG_FEE_AGENT both configs on then both fees were not allowed to be added parellaly. Against ticket #3399 by khola
                        -->
						<td><font color="#005b90"><strong>Select <? if($_SESSION["feeType"] == "distributorBased"){echo("Distributor");}else{ echo("Agent");}?></strong></font></td>
						<td>
							<select name="agentID" style="font-family:verdana; font-size: 11px">
								<option value="">- Select One -</option>
								<?
								if($_SESSION["feeType"] == "distributorBased")
								{
									$agentQuery = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'N'";

								}else{
									$agentQuery = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY'";
								}
								if (CONFIG_IDACOUNTRY_CHECK =='1'){
									$agentQuery .= " and IDAcountry like '%".$_SESSION["destCountry"]."%' ";
								}

								$agentQuery .=  $where_sql ;

								// added by OYEHOYE

								$agentQuery .= " order by name";

								$agents = selectMultiRecords($agentQuery);

								for ($i=0; $i < count($agents); $i++){
									?>
									<option value="<?=$agents[$i]["userID"];?>"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
									<?
								}
								?>
							</select>
							<script language="JavaScript">
								SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID"]?>");
							</script>
						</td>
					</tr>
					<?
				}
				?>

				<?
				if(CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"):
					?>
					<tr bgcolor="#ededed">
						<td width="285"><font color="#005b90"><strong>Payment Mode<font color="#ff0000">*</font></strong></font></td>
						<td>
							<?
							// Added by Usman Ghani aginst ticket #3319: Minas Center - Payment Mode Commission Rates
							// Get the selected value for the list box of payment modes below.
							$selectedPaymentMode = "";
							if ( !empty($_REQUEST["paymentMode"]) )
							{
								$selectedPaymentMode = $_REQUEST["paymentMode"];
							}
							else if ( !empty($_SESSION["paymentMode"]) )
							{
								$selectedPaymentMode = $_SESSION["paymentMode"];
							}
							// End of code aginst ticket #3319: Minas Center - Payment Mode Commission Rates
							?>
							<select name="paymentMode">
								<option value="">-Payment Mode-</option>
								<option value="cash" <?=($selectedPaymentMode == "cash" ? "selected='selected'" : "");?> >Cash Payments</option>
								<option value="bank" <?=($selectedPaymentMode == "bank" ? "selected='selected'" : "");?> >Bank Payments</option>
								<option value="cheque" <?=($selectedPaymentMode == "cheque" ? "selected='selected'" : "");?> >Cheque Payments</option>
								<?
								/**
								 * @Ticket# 3564
								 */
								if(CONFIG_MONEY_PAID_OPTION_BY_CARD == 1) {
									?>
									<option value="card" <?=($selectedPaymentMode == "card" ? "selected='selected'" : "");?>>By Card</option>
								<? } ?>
							</select>
						</td>
					</tr>

					<?
				endif;
				if($_SESSION["feeType"] == "collectionPoint" && CONFIG_FEE_BASED_COLLECTION_POINT == 1)
				{
					if($agentType =="admin" || $agentType == "Admin Manager" )
					{
						?>
						<tr bgcolor="#ededed">
							<td width="30%"><font color="#005b90">
									<strong>Distributor<font color="#ff0000">*</font></strong></font></td>
							<td width="70%"><SELECT name="distributor" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].action='add-fee.php'; document.forms[0].submit();">
									<OPTION value="">- Select Distributor-</OPTION>
									<?


									$distCollectioPoint = selectMultiRecords("select distinct(a.username), a.userID, a.agentCompany from ".TBL_ADMIN_USERS." as a, ".TBL_COLLECTION." as c where a.userID = c.cp_ida_id order by a.agentCompany");
									for ($j=0; $j < count($distCollectioPoint); $j++){
										?>
										<OPTION value="<?=$distCollectioPoint[$j]["userID"];?>">
											<? echo $distCollectioPoint[$j]["username"]." [".$distCollectioPoint[$j]["agentCompany"]."]"; ?>
										</OPTION>
										<?
									}

									?>
								</SELECT>
								<script language="JavaScript">
									SelectOption(document.forms[0].distributor, "<?=$_SESSION["distributor"];?>");
								</script></td>
						</tr>
						<?
					}
					else{
						$_SESSION["distributor"] = $userID;
					}
					if($_SESSION["distributor"] != "")
					{

						$collection = selectMultiRecords("SELECT cp_id, cp_branch_name, cp_city FROM  ".TBL_COLLECTION." where cp_ida_id = '".$_SESSION["distributor"]."' ORDER BY cp_branch_name ");
						?>
						<tr bgcolor="#ededed">
							<td><font color="#005b90"><strong>Collection Point<font color="#ff0000">*</font></strong></font>
							</td>
							<td>
								<select name="agentID">
									<option value="">-- Select Collection Point --</option>
									<?
									for ($k=0; $k < count($collection); $k++)
									{

										$collAdd = $collection[$k]["cp_branch_name"]."-".$collection[$k]["cp_city"];
										$cpID  = $collection[$k]["cp_id"];

										?>
										<OPTION value="<?=$cpID;?>">
											<? echo $collAdd; ?>
										</OPTION>
										<?
									}
									?>
								</select>
								<script language="JavaScript">
									SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID"];?>");
								</script>
							</td>
						</tr>
						<?


					}
				}
				?>

				<? if(CONFIG_PAYIN_CUSTOMER == 1){?>
					<tr bgcolor="#ededed">
						<td width="285"><font color="#005b90"><strong>Regular Customer <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></strong></font></td>
						<td><input type="text" name="Fee" value="<?=stripslashes($_SESSION["Fee"]);?>" size="15" maxlength="16"> <? echo ($_SESSION["feeType"] == 'percent' ? "<font color='#005b90'><strong>%</strong></font>" : "") ?></td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="285"><font color="#005b90"><strong>Payin Book Customer <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></strong></font></td>
						<td><input type="text" name="payinFee" value="<?=stripslashes($_SESSION["payinFee"]);?>" size="15" maxlength="16"> <? echo ($_SESSION["feeType"] == 'percent' ? "<font color='#005b90'><strong>%</strong></font>" : "") ?></td>
					</tr>
				<? }else if (CONFIG_PAYIN_CUSTOMER == 2){?>
					<tr bgcolor="#ededed">
						<td width="285"><font color="#005b90"><strong>Regular Customer <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></strong></font></td>
						<td><input type="text" name="Fee" value="<?=stripslashes($_SESSION["Fee"]);?>" size="15" maxlength="16"> <? echo ($_SESSION["feeType"] == 'percent' ? "<font color='#005b90'><strong>%</strong></font>" : "") ?></td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="285"><font color="#005b90"><strong>Out City Customer <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></strong></font></td>
						<td><input type="text" name="payinFee" value="<?=stripslashes($_SESSION["payinFee"]);?>" size="15" maxlength="16"> <? echo ($_SESSION["feeType"] == 'percent' ? "<font color='#005b90'><strong>%</strong></font>" : "") ?></td>
					</tr>
				<? }else{?>
					<tr bgcolor="#ededed">
						<td width="285"><font color="#005b90"><strong><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?><font color="#ff0000">*</font></strong></font></td>
						<td><input type="text" name="Fee"  value="<?=stripslashes($_SESSION["Fee"]);?>" size="15" maxlength="16"> <? echo ($_SESSION["feeType"] == 'percent' ? "<font color='#005b90'><strong>%</strong></font>" : "") ?></td>
					</tr>
				<? }?>
				<?
				if(CONFIG_SHARE_OTHER_NETWORK == '1')
				{
					$jointClients = selectMultiRecords("select * from ".TBL_JOINTCLIENT." where 1 and isEnabled = 'Y' ");
					?>
					<tr bgcolor="#ededed">
						<td>
							<font color="#005b90"><strong>Available to</strong></font>
						</td>
						<td>
							<select name="remoteAvailable" id="remoteAvailable">
								<option value=""> Select One </option>
								<?
								for($cl = 0; $cl < count($jointClients); $cl++)
								{
									?>

									<OPTION value="<?=$jointClients[$cl]["clientId"];?>" <? echo (strstr($_SESSION["remoteAvailable"],$jointClients[$cl]["clientId"]) ? "selected"  : "")?>> <?=$jointClients[$cl]["clientName"]?></option>
									<?
								}
								?>

						</td>
					</tr>

					<?
				}
				?>
		<?php } ?>
<!--///////////////////////////-->
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" onclick="return checkCharges();" value=" Save ">&nbsp;&nbsp; <input type="reset" value=" Clear ">
			</td>
		</tr>

      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>
<!-- Add Scripts by Mahmood Ali Waseem-->
<script>
function checkCharges(){

var charges= document.getElementById('PaymentFee').value;
var Payment_Type= document.getElementById('Payment_Type').value;

if(Payment_Type==""){
  alert("Please Select Payment Type");
  return false;
}

if (charges == "" )
{
  alert('Please Select some amount in charges');
 return false;
}

}
function isNumber(event) {
    /*evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;*/


	  if(event.which == 8 || event.which == 0){
                return true;
            }
            if(event.which < 46 || event.which > 59) {
                return false;
                //event.preventDefault();
            } // prevent if not number/dot

            if(event.which == 46 && $(this).val().indexOf('.') != -1) {
                return false;
                //event.preventDefault();
            } // prevent if already dot
}

</script>