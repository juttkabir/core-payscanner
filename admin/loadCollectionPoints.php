<?
	session_start();
	include ("../include/config.php");
	include ("security.php");
	
	/**
	 * Variables from Query String
	 */
	$distributor = $_REQUEST["distributor"];
	
	if(!empty($distributor))
	{
		$sql = "SELECT
					cp_id, cp_corresspondent_name, cp_city, cp_country
				FROM
					cm_collection_point
				WHERE
					cp_ida_id = ".$distributor."
				ORDER BY
					cp_country, cp_corresspondent_name
				";
		$result = SelectMultiRecords($sql);
		$numrows = sizeof($result);
		$list = array();
		
		for($i=0; $i<$numrows; $i++)
		{
			$list[$result[$i]["cp_country"]][$i]["ID"] = $result[$i]["cp_id"];
			$list[$result[$i]["cp_country"]][$i]["NAME"] = $result[$i]["cp_corresspondent_name"];
			$list[$result[$i]["cp_country"]][$i]["CITY"] = $result[$i]["cp_city"];
		}
		
		$output = "";
		$nl = "\n";		// New line
		
		foreach($list as $countryName=>$country)
		{
			$output .= "<optgroup label=\"".$countryName."\" class=\"optGroup\">".$nl;
			
			foreach($country as $index)
			{
				$output .= "<option value=\"".$index["ID"]."\">".$index["NAME"].", ".$index["CITY"]."</option>".$nl;
			}
			
			$output .= "</optgroup>".$nl;
		}
		
		echo $output;
	}
?>