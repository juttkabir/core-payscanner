<?php 
/*
*Function Page
*Discription: the function that we are using in admin section will write in 
*in this ajaxFunction Page
* @author: mI@n @NjUm
*/

include ($_SERVER['DOCUMENT_ROOT']."/admin/lib/compliance-function.php");

function changeModeCombinationStatus($arguments)
{
//debug($arguments);

$strUpdateQuery = "UPDATE ".TBL_COMBINIATION_PAYMENT_MODE." SET `status` = '".$arguments["status"]."' Where pid='".$arguments["combinationID"]."'";

$queryUpdate=update($strUpdateQuery);
		//debug($queryUpdate);
		return $queryUpdate;

}

function saveModeCombination($arguments)
{
$arguments["saveCombination"];
$arguments["IDAcountry"];

$userId=$_SESSION["loggedUserData"]["userID"];
$argumentsVal=array();
$insertionVal='';
$insertionValID='';
for($i=0;$i<count($arguments["IDAcountry"]);$i++)
{
$val=explode("||",$arguments["IDAcountry"][$i]);
$argumentsVal[$i]=$val[0];
$argumentsValID[$i]=$val[1];
$insertionVal.=$argumentsVal[$i].',';
$insertionValID.=$argumentsValID[$i].',';
}

//debug($arguments);   
//debug($argumentsVal);
//echo $insertionVal;


//debug($insertionVal);
$strQueryInsert = "INSERT INTO 
								".TBL_COMBINIATION_PAYMENT_MODE."
								(
								  paymentModeCombination,
								  associatedPaymentModes,
								  status,
								  created,
								  createdBy 	
								 )
							values
								(
								  '".$insertionVal."',
								  '".$insertionValID."',
								  'Enable',
								  '".date('Y-m-d H:i:s')."',
								  '".$_SESSION["loggedUserData"]["userID"]."'
								 )
								 	";
									
		
		
		$Insertflag=insertInto($strQueryInsert);
		
		return $Insertflag;


}

function getModeCombinationHTML($arguments)
{

$SQL_Qry = "Select * From 
					".TBL_COMBINIATION_PAYMENT_MODE." 
					";
$arrModeTypes = SelectMultiRecords($SQL_Qry);
//debug($arrModeTypes);

	
	$html='<table width="100%" border="0"><tr bgcolor="#DFE6EA"> 
            <td width="143" height="20"><font color="#005b90"><strong>Payment Mode Combination</strong></font></td>
            <td width="100" align="center" valign="middle"><font color="#005b90"><strong>Added On</strong></font></td>
            <td width="100" align="center" valign="middle"><font color="#005b90"><strong>Added By</strong></font></td> 
			<td><center><font color="#005b90"><strong>Status</strong></font></center></td>
			<td><center><font color="#005b90"><strong>Enable/Disable</strong></font></center></td>
          </tr>';
	
	//debug($arrModeType);
			for ($i=0; $i<count($arrModeTypes); $i++){
		 $html .='
		 
		 ';
		 
		 
		 $html .='<tr valign="top" bgcolor="#eeeeee"> 
            <td>'.substr(str_replace(","," & ",$arrModeTypes[$i]["paymentModeCombination"]),0,-2).'</td>
            <td  align="center" valign="middle">'.$arrModeTypes[$i]["created"].'</td>
           	<td  align="center" valign="middle">';
		$queryName="Select name From ".TBL_ADMIN_USERS." Where userID='".$arrModeTypes[$i]["createdBy"]."'";
			$queryNameData=selectFrom($queryName);
			
			 $html .='&nbsp;'.$queryNameData["name"].'</td>';
			$html .='<td  align="center" valign="middle">'.$arrModeTypes[$i]["status"].'</td>
			<td  align="center" valign="middle"><span  style="color:#005b90;cursor:pointer" onClick="enableModeC('.$arrModeTypes[$i]["pid"].',\''.$arrModeTypes[$i]["status"].'\');">Enable</span> &nbsp;&nbsp;<span  style="color:#005b90; cursor:pointer" onClick="disableModeC('.$arrModeTypes[$i]["pid"].',\''.$arrModeTypes[$i]["status"].'\');">Disable </span></td>
          </tr>';
		 
         }

	$html .='</table>';				
echo   $html;

}

function changePayModeStatus($arguments)
{
//debug($arguments);
$arguments["ModeID"];
$arguments["rtypeMode"];
$arguments["status"];
if($arguments["status"]=='D')
{
$status='Enable';
}else
{
$status='Disable'; 
}

$strUpdateQuery = "UPDATE ".TBL_PAYMENT_MODE_LIST." SET `status` = '".$status."' Where pid='".$arguments["ModeID"]."'";
		$queryUpdate=update($strUpdateQuery);
		//debug($queryUpdate);
		return $queryUpdate;
}



/*
*Function Name:adminCheckCompliance
*Discription:it will check the the complince Check of agent and distributor
*@author: Mian Anjum
*/


function userCheckCompliance($args=array())  
{
 
//debug($arguments);
$returnVal = complainceVerify($args);
//debug($returnVal);
$proccedButtonFlag = false;
		 $userFlag = false;
		 if($args["ruleFor"] == 'agent'  || $args["ruleFor"] == 'distributor')
			 $userFlag = true;
	if($args["ruleFor"] == 'amount' && !empty($returnVal))
		 {
		 
			//FOr Amount 
		 
		 
		 
		 }
		 elseif($userFlag)
		 {
		 $user = $args["ruleFor"];
			$returnComplianceMessage = complianceMessageBox($returnVal,$user);
			//debug($returnComplianceMessage);
			 if(!empty($returnComplianceMessage))
			 {
				 $arrComp = explode("|", $returnComplianceMessage);
				 $ruleIds     = $arrComp[0];
				 $isHold      = $arrComp[1];
				 $htmlMessage = $arrComp[2];
				 $returnVal = $htmlMessage;
			 }
		 }
			 
			 
			 
		if(empty($returnVal))		
			$returnVal = "";
return $returnVal;

}

function complianceMessageBox($returnCompliance,$userType)
	{
		$matchType = '';
		$compliance_list ='';
		$match_name = '';
		$ruleId = '';
		$holdFlag = false;
		
			
		$comp_html = '<table border="1">';
		
		$returnVal = '';
		
		if(empty($returnCompliance))
		{
		$comp_html .='<tr><td colspan="4"><h3><font color="green">User Not Found in Compliance List</h3></font>';
		$comp_html .='<img height="25" width="25" onClick="closeComp();" title="Close" alt="Close" style="float: right;" src="images/delete_blue.png"></h3></td></tr>';
			
		}
		else
		{
			$comp_html .='<tr><td colspan="4" align="center"><h3><font color="red"> Compliance Match List</font>';
		$comp_html .='<img height="25" width="25" onClick="closeComp();" title="Close" alt="Close" style="float: right;" src="../ntm4/images/delete_blue.png"></h3></td></tr>';
			$comp_html .='<th>List</th><th>Name</th><th>Message</th><th>Percent</th>';
	//debug($returnCompliance);
		foreach($returnCompliance as $keyType => $valType)
		{
			//debug($keyType."-->".$valType);
			//debug($valType);
			if(!empty($keyType))
			{
				// fetch data against rule id
				$ruleData = selectFrom("select * from ".TBL_COMPLIANCE_LOGIC." where ruleID= '".$keyType."'");
				//debug("select * from ".TBL_COMPLIANCE_LOGIC." where ruleID= '".$keyType."'");
				$message = $ruleData["message"];
				$matchType = $ruleData["matchCriteria"];
				$isHold = $ruleData["isHold"];
				$applyTrans = $ruleData["applyTrans"];
				
				$ruleId .= $keyType.",";
				if($isHold == 'Y')
					$holdFlag = true;
				//debug($message);
			}
			foreach($valType as $ruleKey => $ruleVal)
			{
				//debug($ruleKey."-->".$ruleVal);
				if(is_array($ruleVal) && $ruleKey == 'L')
				{
					$arrList = $ruleVal;
				
					
					
					if(!is_array($arrList['percent']))
					{
					$percent=$arrList['percent'];
					}else{
					//debug(count($arrList['percent']));
			
					}
					
					foreach($arrList as $listKey => $listVal)
					{
						// list name e.g OFAC,HM Treasury etc
						//debug($listKey."-->".$listVal);
						
						//debug($listVal);
						$compliance_list = $listKey;
						
						foreach($listVal as $dataKey => $dataVal)
						{
							// compliance table primary key and full name array
							//debug($dataKey);
							//debug($dataVal);
							
							
					         
							foreach($dataVal as $key => $matchVal)
							{
							$comp_html .='<tr>';
								// match name value
								
								//debug($key);
								$comp_html .='<td>'.$compliance_list.'</td>';
								$comp_html .='<td>'.$matchVal.'</td>';
								$comp_html .='<td>'.$message.'</td>'; 
								if(!is_array($arrList['percent']))
								{
									$comp_html .='<td>'.$percent.'</td>'; 
								}else{
								
								$comp_html .='<td>'.number_format($arrList['percent'][$key],2).'</td>'; 
								
								}
					
								
								$comp_html .='</tr>';
								
							}
							
						}
						
					}
			  } // end if
			}
		}	
	 }	// end else condition
	
	$comp_html .='<tr><td colspan="4"><input type="button" id="compProceed" class="actionButton" style="width:160px;" name="compProceed" value="Are you sure to Proceed?" onClick="proceedToAdd(\''.$userType.'\');"></td></tr>';
		
		
		$comp_html .= '</table>';
		$comp_html .= '';
		
		$returnVal = $ruleId."|".$holdFlag."|".$comp_html;
		return $returnVal;
	}

	
function getOnChangeAccountValue($args)
{   //debug($args);
	 $dataValue=selectFrom("SELECT *  FROM accounts where id = '".$args['id']."'");
	//debug($dataValue);
	 $returnVal=$dataValue['name']."|".$dataValue['bankName']."|".$dataValue['IBAN']."|".$dataValue['accounNumber']."|".$dataValue['accountName']."|".$dataValue['currency']."|".$dataValue['sortCode']."|".$dataValue['balance']."|".$dataValue['swiftCode'];

		return $returnVal;
	
}

function recurringRateAuthentication($args)
{

$rateQuery= selectFrom("select min(fromamount) as min , max(toAmount)as Amount from fx_rates_margin where baseCurrency='".$args["baseCurrency"]."' and quotedCurrency='".$args["quotedCurrency"]."'");


return $rateQuery["Amount"]."/".$rateQuery["min"]; 
//debug($rateQuery["Amount"]);


}

function holdRecurringStatus($args)
{
   $recurID = $args["recurID"];
/* echo "Active Status ID".$recurID; */
    $res = update("update ".TBL_RECURRING_PAYMENT." set Status ='Hold' where transID='".$recurID."'");
	$descript = "Recurring Rule Hold";	
	activities($_SESSION["loginHistoryID"],"UPDATION",$recurID,TBL_TRANSACTIONS,$descript);
	
	return $res; 
	

}



?>