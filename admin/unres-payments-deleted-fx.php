<?
	session_start();
	
	$date_time = date('d-m-Y  h:i:s A');
	
	include ("../include/config.php");
	include ("security.php");
	
	if(CONFIG_CUSTOM_TRANSACTION == '1')
		$transactionPage = CONFIG_TRANSACTION_PAGE;
	else
		$transactionPage = "add-transaction.php";
///print_r($_REQUEST);
	$today = date("d/m/Y");
	if($_REQUEST["from"] == "")
	{
		$_REQUEST["from"] = $today;
	}
	if($_REQUEST["to"] == "")
	{
		$_REQUEST["to"] = $today;
	}
	
	if($_REQUEST["fromT"] == "")
	{
		$_REQUEST["fromT"] = $today;
	}
	if($_REQUEST["toT"] == "")
	{
		$_REQUEST["toT"] = $today;
	}
	//debug($_REQUEST);
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");

	$response = new Services_JSON();
	$page = $_REQUEST['page']; // get the requested page 
	$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord = $_REQUEST['sord']; // get the direction 
	
	if(!$sidx && $_REQUEST['getGrid']!='transList')
	{
		$sidx = 'importedOn DESC, id ';
	}elseif(!$sidx)
	{
		$sidx = 1;
	}
	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
			
	if($limit == 0)
	{
		$limit=50;
	}
	
	$SubmitJS		= "";
	$fromJS     	= "";
	$toJS			= "";
	$refNumberJS	="";
	$amount1JS		= "";
	$descJS	= "";

if($_REQUEST["btnAction"] == "Revoke")
{
	$payIDArray		= $_REQUEST["payIDs"];
	for ($d=0;$d < count($payIDArray);$d++)
	{
		if(isset($payIDArray[$d]))
		{
			update("update barclayspayments set isDeleted = 'N' where id = '".$payIDArray[$d]."'");
			$descript="Revoked record for Barclays Payment by ". $_SESSION["loggedUserData"]["username"];;
			activities($_SESSION["loginHistoryID"],"UPDATE",$payIDArray[$d],'barclayspayments',$descript);
			if(CONFIG_IMPORT_PAYMENT_AUDIT=="1"){
				$query_pay_action = "
								INSERT INTO payment_action(payID,action_id)
											values('".$payIDArray[$d]."', '".$_SESSION["loggedUserData"]["userID"]."')";
				insertInto($query_pay_action);
			}				
			$err = "";
		}
	}

	if(trim($err) == "")
	{
		$err = "Payment(s) Revoked successfully.";
	}
	// Number of iteration should be equal to the number of  transactions
		echo "<input type='hidden' name='payListDelete' id='payListDelete' value='".$err."'>";
		exit;
}



	
if($_REQUEST["type"] == 1)
{//tlaCode = 'REM'

	$query = "select * from barclayspayments where  isResolved='N' and isDeleted='Y' ";

	
	
	}elseif($_REQUEST["type"] == 2)
{
	$query = "select * from natwestpayments where isResolved='N' and isDeleted='Y' ";
}


//////////////Date Block //////////////////
if ($_REQUEST["from"]!="")
		$fromJS = $_REQUEST["from"];
if ($_REQUEST["to"]!="")
		$toJS = $_REQUEST["to"];


	if ($fromJS != "")
	{
		$dDateF = explode("/",$fromJS);
		if(count($dDateF) == 3)
			$fromJS = $dDateF[2]."-".$dDateF[1]."-".$dDateF[0];
	}
	if ($toJS != "")
	{
		$dDateT = explode("/",$toJS);
		if(count($dDateT) == 3)
			$toJS = $dDateT[2]."-".$dDateT[1]."-".$dDateT[0];
	}
	
			if ($fromJS != "" || $toJS != "")
		{
			if ($fromJS != "" && $toJS != "")
			{
				$queryDated = " and (importedOn >= '$fromJS 00:00:00' and importedOn <= '$toJS 23:59:59') ";	
			}
			elseif ($fromJS != "")
			{
				$queryDated = " and (importedOn >= '$fromJS 00:00:00') ";	
			}
			elseif ($toJS != "")
			{
				$queryDated = " and (importedOn <= '$toJS 23:59:59') ";	
			}	
		}
	if ($queryDated != "")
	{
		$query .= $queryDated;
	}
	
	/////////////////////Date Block ended///////////////	
	
if($_REQUEST["Submit"] == "SearchPay")
{
	//echo("--Before-".$amount1JS."---");
	if ($_REQUEST["amount1"]!=""){
	//	echo("---".$_REQUEST["amount1"]."---");
		$amount1JS = $_REQUEST["amount1"];
		$amount1JS=number_format($amount1JS,2);
	}
	
	//echo("--Before-".$amount1JS."---");
	
	if ($_REQUEST["desc"]!="")
		$descJS = $_REQUEST["desc"];
	//if ($_REQUEST["currencyPay"]!="")
		//$currencyP = $_REQUEST["currencyPay"];
	if ($_REQUEST["accountNo"]!="")
		$accountNo = $_REQUEST["accountNo"];
	
	if(!empty($_REQUEST["type"]))
	{

		if ($amount1JS != "")
		{
			$queryAmount = " and amount = $amount1JS ";	
		}
		if ($descJS != "")
		{
			$queryDesc = " and (description like '%$descJS%'  or unResReason like '%$descJS%')";	
		}	
	}

	if ($queryrefNumber != "")
	{
		$query .= $queryrefNumber;
	}
	if ($queryAmount != "")
	{
		$query .= $queryAmount;
	}
	if ($queryDesc != "")
	{
		$query .= $queryDesc;	
	}
	/*if ($currencyP != "")
	{
		$query .= " and currency = '".$currencyP."'";
	}*/
	if ($accountNo != "" && $accountNo!='null')
	{
		$query .= " and accountNo IN (".$accountNo.")";
	}
}
if(CONFIG_TRANSACTION_STATUS_AWAITING_PAYEMNT == '1')
{
	$fetchingStatus = 'Awaiting Payment';	
}else
{
	$fetchingStatus = 'Pending';	
}

//debug($query);

///////////////////Date Block ended /////////////////////////
		

//debug($queryT);
/*$contents = selectMultiRecords($query);
$allCount = countRecords($queryCnt);*/
	//$allCount = countRecords($queryCnt);
	//$count = $allCount;
	//debug($query);
if($_REQUEST["getGrid"] == "payList"){
	$result = mysql_query($query) or die(__LINE__.": ".mysql_query());
	$count = mysql_num_rows($result);
	
	//debug($_REQUEST);
	//debug($query);
	
	if($count > 0)
	{
		$total_pages = ceil($count / $limit);
		
	} else {
		$total_pages = 0;
		//debug($total_pages);
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	//debug( $limit);
	//debug( $page);
	$start = ($limit * $page) - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 *
	 * @author: Waqas Bin Hasan
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	$query .= " order by $sidx $sord LIMIT $start , $limit";
	
	$result = mysql_query($query) or die(__LINE__.": ".mysql_error());
	//debug($query);
	//debug($result);
	//debug($query);
	//print($query);
	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;

	$i=0;
	$totalAmount = 0;
	while($row = mysql_fetch_array($result))
	{
		$response->rows[$i]['id'] = $row["id"];
		
		
		
		if($row["entryDate"]== "0000-00-00")
		$entryDate='--';
		else
		$entryDate=dateFormat($row["entryDate"], "2");
/*		$bankNameSql = "select bankName from bankDetails where transID = ".$row["transID"];
		$bankNameData = selectFrom($bankNameSql);*/
		
		$response->rows[$i]['cell'] = array(
									dateFormat($row["importedOn"], "2"),
									$row["accountNo"],
									$entryDate,
									$row["tlaCode"],
									number_format($row["amount"],"2",".",""),
									$row["description"]
								);
					$totalAmount+= $row["amount"];
		$i++;
	}
		$response->rows[$i]['cell'] = array(
									'',
									'',
									'<strong>Total:</strong>',
									'<strong>'.number_format($totalAmount,"2",".","").'</strong>',
									''
								);
	echo $response->encode($response); 

}	

	
?>
