<?php
include ("../include/config.php");
include ("security.php");
include("websonic.php");
$agentType = getAgentType() ;
$condition = False;
if(CONFIG_TRANS_ROUND_LEVEL != "")
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 4;
	
	}
if($agentType == "SUPA" || $agentType == "SUPAI" || $agentType == "SUBA" || $agentType == "SUBAI" || $agentType == "TELLER")
{
	$condition = True;	
}	
$query="SELECT id, userID, date, note5, note10, note20, note50, coin1GBP, coin2GBP, coin50, coin20, coin10, coin05, coin01, total ,cashierRemarks,pettyCash FROM cashierCurrency where 1";
if($condition){
	$query .= " and userID = '".$_SESSION["loggedUserData"]["userID"]."'";
}
$query .= " ORDER BY `id` DESC";
$result=mysql_query($query) ;
@$num=mysql_numrows($result) ;
?>
<html>
<head>
<link href="images/interface.css" rel="stylesheet" type="text/css">
</head>

<style type="text/css">
.inputclass{
width:70px;
}
.style2 {
	color: #6699CC;
	font-weight: bold;
}
</style>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#000000" size="2">Daily Cash Report List </font></strong></td>
  </tr>
</table>
<br>

<a href="cashier_denomination_currency.php" class="style2">Add New Denomination</a>
<br><br>

  <table border="1" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><strong>Date</strong></td>
<?
if(!$condition){
?>
	<td><strong>Added By</strong></td>
<?
}
?>
<td><strong>Note5</strong></td>
<td><strong>Note10</strong></td>
<td><strong>Note20</strong></td>
<td><strong>Note50</strong></td>
<td><strong>Coin1GBP</strong></td>
<td><strong>Coin2GBP</strong></td>
<td><strong>Coin50</strong></td>
<td><strong>Coin20</strong></td>
<td><strong>Coin10</strong></td>
<td><strong>Coin5</strong></td>
<td><strong>Coin1</strong></td>
<td><strong>Total</strong></td>
<td><strong>Transaction Amount</strong></td>
<td><strong>Cashier Remarks</strong></td>
<td>&nbsp;</td>
</tr>
<?php
$i=0;
while($i<$num){
	$cashierTransaction = selectFrom("select sum(amount) as cashierAmount from ".TBL_CASHIER_COLLECT_AMOUNT." where cashierCurrId = '".mysql_result($result,$i,"id")."'");
	$transAmount = $cashierTransaction["cashierAmount"] + mysql_result($result,$i,"pettyCash");
	$addedBy = selectFrom("select name from ".TBL_ADMIN_USERS." where userID = '".mysql_result($result,$i,"userID")."'");
?>
<tr>
<td><a href="print-cashier_denomination_currency.php?formstate=submit&id=<?= mysql_result($result,$i,"id") ?>" class="style2"><?= mysql_result($result,$i,"date") ?></a></td>
<?
if(!$condition){
?>
	<td>&nbsp;<?= $addedBy["name"] ?></td>
<?
}
?>
<td>&nbsp;<?= mysql_result($result,$i,"note5")*5 ?></td>
<td>&nbsp;<?= mysql_result($result,$i,"note10")*10 ?></td>
<td>&nbsp;<?= mysql_result($result,$i,"note20")*20 ?></td>
<td>&nbsp;<?= mysql_result($result,$i,"note50")*50 ?></td>
<td>&nbsp;<?= mysql_result($result,$i,"coin1GBP")*1 ?></td>
<td>&nbsp;<?= mysql_result($result,$i,"coin2GBP")*2 ?></td>
<td>&nbsp;<?= mysql_result($result,$i,"coin50")*0.50 ?></td>
<td>&nbsp;<?= mysql_result($result,$i,"coin20")*0.20 ?></td>
<td>&nbsp;<?= mysql_result($result,$i,"coin10")*0.10 ?></td>
<td>&nbsp;<?= mysql_result($result,$i,"coin05")*0.05 ?></td>
<td>&nbsp;<?= mysql_result($result,$i,"coin01")*0.01 ?></td>
<td>&nbsp;<?= mysql_result($result,$i,"total") ?></td>
<td>&nbsp;<?= round($transAmount, $roundLevel); ?></td>
<td>&nbsp;<?= mysql_result($result,$i,"cashierRemarks") ?></td>
<td><a href="cashier_denomination_currency.php?formstate=submit&id=<?= mysql_result($result,$i,"id") ?>" class="style2">Edit</a></td></td>
</tr>
<?php
$i++;
}
?>
</table>
</html>