<?php

// Session Handling
session_start();

// Including files
include ("../include/config.php");
include ("security.php");
include("connectOtherDataBase.php");
include("calculateBalance.php");
include('lib/phpmailer/class.phpmailer.php');

$customerID=$_GET['customerID'];
$ruleID=$_GET['ruleID'];

 if($_POST["transID"] =="")
 {
 
if($_SESSION["ConfirmRecurring"]=="ConfirmRecurring")
{
  if($_SESSION['scheduleOptions']== 'N')
  {
  $startDate=$_POST["startDate"];
  $endDate = $_POST["endDate"];
    if ($_SESSION['scheduleMode']== 'M')
	    {
              $startDate=$_POST["startDate"];
			  $endDate = "00/00/0000" ;

        }
	 if ($_SESSION['scheduleMode']== 'Q')
	    {
              $startDate=$_POST["startDate"];
			  $endDate = "00/00/0000" ;

        }	
	if ($_SESSION['scheduleMode']== 'W')
	    {
              $startDate=$_POST["startDate"];
			  $endDate = "00/00/0000" ;

        }
  }
  else
  {
 
  $startDate = $_SESSION["start_date"];
  $endDate =  $_SESSION["end_date"];
  
  }
 
  $Sdate = explode("/", $startDate);
  $Edate = explode("/", $endDate);
  
  $start_date =$Sdate[2]."-".$Sdate[1]."-".$Sdate[0];
  $end_date = $Edate[2]."-".$Edate[1]."-".$Edate[0];
  
   echo $start_date;
  echo $end_date; 
  
  $datetime1 = date_create($start_date);
  $datetime2 = date_create($end_date);
  $interval = date_diff($datetime1, $datetime2);
  $dayRecurring = $interval->format('%a ');

  if($_SESSION["scheduleAlert"]=="email")
  {
    $alertOption ="Y";
	$alertType="Email";
  }
  else 
  {
   $alertOption ="N";
   $alertType="";
  }
  
  //echo $dayRecurring; 
//Recurring Payment Count Calculation Start 
}

//echo "<br> above On";
if($_SESSION["RecuringCheckbox"] == "on" || $_SESSION["RecuringCheckbox"] == "On")
{
   echo "Inside second if ";
  //echo "<br> schedule mode ". $_SESSION["scheduleMode"];
  //echo "<br>schedule option ".$_SESSION["scheduleOptions"]."<br>";
  //echo "Yes";
//for recurring 
  $recuring ="Y";
  if($_SESSION["scheduleOptions"]=="Y")
  {
     
     $recuringCount="1";
    
  }
  else 
  {
      
	  
	  if($_SESSION["scheduleMode"]=="D")
	  {
	    $recuringCount = $dayRecurring; 
	  
	  }
	   if($_SESSION["scheduleMode"]=="Q")
	  {
	     
	    $recuringCount = $_SESSION["Quarter"]; 
	  
	  }
	  
	  if($_SESSION["scheduleMode"]=="W")
	  {
	    //$recuringCount = round($dayRecurring/7);
		$recuringCount = $_POST["TotalWeeks"]; 
	  }
	  
	  if($_SESSION["scheduleMode"]=="M")
	  {
	    
	   $recuringCount = $_SESSION["TotalMonths"];
	   
	  }
	  
	  
	  
  
  }
  
}
else 
{
       $recuring ="N";
	  $recuringCount = "";
} 

}
//$recuringCount="3";
$reversvalue="";
				if($_POST['reverseCalculation']=='Y')
				{
					$reversvalue="Y";
				}
				else
				{
					$reversvalue="N";
				}
 
	//End Recurring Count 
// Edit Recurring Rule  	
	
	if($_REQUEST["transID"] != "" && $_SESSION["RecuringCheckbox"] == "on" && $_SESSION["ConfirmRecurring"]== "ConfirmRecurring")
	{
	// For Alert Type and Alert Option
	
	if($_SESSION["scheduleAlert"]=="email")
  {
    $alertOption ="Y";
	$alertType="Email";
  }
  else 
  {
   $alertOption ="N";
   $alertType="";
  }
	
	    $queryRecurring ="update recurring_transactions set exchangeRateType ='".$_SESSION['scheduleRate']."',exchangeRate='".$_SESSION['fixedValue']."',alertOption ='".$alertOption."', alertType='".$alertType."' where  transID = '".$_REQUEST["transID"]."' ";

   	
	update($queryRecurring);	
	
	//for Activities
	$lastId = $_REQUEST["transID"];
	
	$descript="Recurring Rule TransID".$_REQUEST["transID"]." is updated ";
			activities($_SESSION["loginHistoryID"],"Updation",$lastId,TBL_TRANSACTIONS,$descript);
	
		
	}
	
// End Editing Rule
$reconcileInput = $_POST["reconcile"];

/* If Transaction is to distribut to third part */
if(CONFIG_MIDDLE_TIER_CLIENT_ID == "3" && defined("LOCAL_PATH"))
	require LOCAL_PATH."api/etransact/soap_api.php";
$bolTestDebug = false;
if($_SESSION["loggedUserData"]["username"]=='hbs_support')
	$bolTestDebug = true;
 
// WIHC Logic Starts...
$distributorCommission = 0;

if(CONFIG_CALCULATE_WIHC == "true")
{
	/**
	 * Connect Plus Distributor Commission Logic - WHICH-EVER-IS-HIGHER-COMMISSION A.K.A. WIHC
	 * This is an independent logic, so that theoratically it has no effect on other logics and calculations on this page.
	 * It only has to be calculated separately and to be saved during transaction is saved, in a separate field.
	 * For the formula reference see ticket #3603.
	 */
	$settlementExchangeRate = getSettlementExchangeRate($_SESSION["distribut"], $_SESSION["currencyTo"]);
		
	if($settlementExchangeRate != 0)
	{
		$actualAmount = $_SESSION["localAmount"] / $settlementExchangeRate;
		$tmpWIHCInfo = calculateWIHC($actualAmount, $_SESSION["distribut"], $_SESSION["collectionPointID"]);
		$distributorCommission = round($tmpWIHCInfo["VALUE"], 2);
	}
}
// WIHC Logic Ends.
if(CONFIG_DOUBLE_ENTRY == '1')
	include("double-entry-functions.php");
$agentType = getAgentType();
$status = "";

if($_GET["transSend"]=="transSend")
{
	$transSend = $_GET["transSend"];
}else{
	$transSend = "";
	}

if(CONFIG_TRANS_ROUND_LEVEL != "")
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 4;
	}
$currencyFrom = $_POST["currencyFrom"]; 

///////////////////////History is maintained via method named 'activities'
 

if($agentType == 'TELLER')
{
	$changedBy = $_SESSION["loggedUserData"]["tellerID"];
	}else{
	$changedBy = $_SESSION["loggedUserData"]["userID"];	
}

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}

if($_GET["r"] == 'dom')///If transaction is domestic
{
$returnPage = 'add-transaction-domestic.php';
}else{
	$returnPage = $transactionPage;
	}


$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;


$agentCountry = $_POST["benCountry"];
$isAgentAnD = 'N';
$isBankAnD = 'N';

$rangeEmailAlert = false;

$countryBasedFlag = false;
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}
/**
 * @Ticket# 3622 
 * If super admin is logged in and the config for subagent is ON than 
 * the custAgentId shoud be the the agent selected from search not the super agent
 */
if($agentType == "SUPA" && CONFIG_CREATE_TRANSACTION_FOR_SUB_AGENT == 1)
{
	/* Do Nothing */	
}
else
{
	if($agentType == "SUPA" || $agentType == "SUPAI" || $agentType == "SUBA" || $agentType == "SUBAI")
	{
		$queryCust = "select *  from ".TBL_ADMIN_USERS." where userID ='".$_SESSION["loggedUserData"]["userID"]."'";/// it was using username ='$username' but now changed
		$senderAgentContent = selectFrom($queryCust);	
		$_POST["custAgentID"] = $senderAgentContent["userID"];
		$parentCustAgent =  $senderAgentContent["parentID"];
	}
}


if(trim($_POST["transType"]) == "Bank Transfer" || trim($_POST["transType"]) == "Home Delivery" || trim($_POST["transType"]) == "ATM Card")
{
		$Tran_Type = $_POST["transType"];
		$agentCountry = $_POST["benCountry"];
		/*********
		**Here we need to confirm exactly 
		**what information is needed instead of *
		********/
		$query = "select * from admin where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'N'  and isCorrespondent != ''";				
		if(CONFIG_CITY_SERVICE_ENABLED)
		{
				$benAgentIDs = $_SESSION["distribut"];
				if($benAgentIDs == "0")
				{
					$ida = selectMultiRecords($query);
					for ($j=0; $j < count($ida); $j++)
					{
						$authority = $ida[$j]["authorizedFor"];
						$toCountry = $ida[$j]["IDAcountry"];
						if(strstr($authority,"Home Delivery") && strstr($toCountry,$agentCountry))//strstr($authority,"Bank Transfer")
						{
							$benAgentIDs =  $ida[$j]["userID"];
				
						}
					}
				}
		}
		else
		{
			$ida = selectMultiRecords($query);
			for ($j=0; $j < count($ida); $j++)
			{
				$authority = $ida[$j]["authorizedFor"];
				$toCountry = $ida[$j]["IDAcountry"];
				if(strstr($authority,"Home Delivery") && strstr($toCountry,$agentCountry)){
					$benAgentIDs =  $ida[$j]["userID"];
				}
				if(strstr($authority,"ATM Card") && strstr($toCountry,$agentCountry))
					$benAgentIDs =  $ida[$j]["userID"];
	
			}
		}
		// if find no IDA against this BenCountry then assign this transaction to AdminIDA 
		
		if($benAgentIDs == '')
		{
			}
			/**
			 * Exclude the following from below if condition 
			 *   || $_POST["transType"] == "ATM Card"
			 * by this condition the distributor selected from the above query will be over rider by '0'
			 * Ticket: ATM Card Transaction type development
			 */
			if($_POST["transType"] == "Bank Transfer")
			{
				if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1")
				{
					if(!empty($_POST["distribut"]))
						$benAgentIDs = $_POST["distribut"];
					else
						$benAgentIDs = "0";
				}
				else
					$benAgentIDs = $_POST["distribut"];
				
			}
	//}
	
}


elseif(trim($_POST["transType"]) == "Pick up")
{

		$_SESSION["collectionPoint"] = $_POST["collectionPointID"];
			$queryCust = "select *  from cm_collection_point where  cp_id ='" . $_SESSION["collectionPoint"] . "'";
			$senderAgentContent = selectFrom($queryCust);
			$queryIDA="select * from ".TBL_ADMIN_USERS." where userID='".$senderAgentContent["cp_ida_id"]."'";
			$IDAContent=selectFrom($queryIDA);
				$benAgentIDs = $IDAContent["userID"];			
			if($benAgentIDs == "")
			{
			
				
			}
		
}

$_SESSION["moneyPaid"] 			= $_POST["moneyPaid"];
$_SESSION["transactionPurpose"] = $_POST["transactionPurpose"];
$_SESSION["other_pur"] 			= $_POST["other_pur"];
$_SESSION["fundSources"] 		= $_POST["fundSources"];
$_SESSION["refNumber"]      = $_POST["refNumber"];
$_SESSION["benAgentID"] 		= $benAgentIDs;
$_SESSION["discount"]           = $_POST["discount"];
$_SESSION["chDiscount"]         = $_POST["chDiscount"];
$_SESSION["bankName"] 		= $_POST["bankName"];
$_SESSION["branchCode"] 	= $_POST["branchCode"];
$_SESSION["branchName"] 	= $_POST["branchName"];
$_SESSION["branchAddress"] 	= trim($_POST["branchAddress"]);
$_SESSION["swiftCode"] 		= $_POST["swiftCode"];
$_SESSION["accNo"] 			= $_POST["accNo"];
$_SESSION["ABACPF"] 		= $_POST["ABACPF"];
$_SESSION["IBAN"] 			= $_POST["IBAN"];
$_SESSION["ibanRemarks"] 			= $_POST["ibanRemarks"];
$_SESSION["admincharges"] 	= $_POST["admincharges"];
$_SESSION["bankCharges"] 	= $_POST["bankCharges"];
$_SESSION["tip"] 	= $_POST["tip"];
$_SESSION["branchName"] 	= $_POST["branchName"];
$_SESSION["discount_request"] = $_POST["discount_request"];	
$_SESSION["accountType"] = $_POST["accountType"];	
$_SESSION["exchangeRate"]     = $_POST["exchangeRate"];
$_SESSION["IMFee"]     = $_POST["IMFee"];
$_SESSION["question"]=$_POST["question"];
$_SESSION["answer"]=$_POST["answer"];
$_SESSION["documentProvided"] = $_POST["documentProvided"];
$_SESSION["document_remarks"] = $_POST["document_remarks"];
$_SESSION["internalRemarks"] = $_POST["internalRemarks"];
$_SESSION["benIdPassword"] = $_POST["benIdPassword"];
$_SESSION["settlementRate"]     = $_POST['settlementRate'];	
$_SESSION["settlementValue"]     = $_POST['settlementValue'];	
$_SESSION["settlementCurrency"]     = $_POST['settlementCurrency'];	
$_SESSION["payPoint"]     = $_POST['payPoint'];	
$_SESSION["exRateLimitMin"]     = $_POST['exRateLimitMin'];	
$_SESSION["exRateLimitMax"]     = $_POST['exRateLimitMax'];	
$_SESSION["bankingType"] = $_POST["bankingType"];

if(CONFIG_DISPLAY_MONEY_PAID_BY_CHEQUE == false)
{
	if($_REQUEST["treatAsCheque"] == "Y")
	{
		$_SESSION["moneyPaid"] = "By Cheque";
		$_SESSION["chequeNo"] = $_REQUEST["treatChequeNum"];
		$_SESSION["chequeAmount"] = abs($_REQUEST["changeGiven"]);
		
		$_POST["chequeNo"] = $_REQUEST["treatChequeNum"];
		$_REQUEST["chequeAmount"] = abs($_REQUEST["changeGiven"]);
		$_POST["amountRecieved"] = abs($_SESSION["totalAmount"]) - abs($_REQUEST["changeGiven"]);
		$_POST["moneyPaid"] = "By Cheque";
		
	}
}


if($_SESSION["moneyPaid"] == "By Cheque")
{
	$_SESSION["chequeNo"] = $_POST["chequeNo"];

	if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
	{
		$_SESSION["chequeAmount"] = $_REQUEST["chequeAmount"];
	}
}
else if ($_SESSION["moneyPaid"] != "By Cheque")
{
	$_SESSION["chequeNo"] = "";

	if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
	{
		$_SESSION["chequeAmount"] = "";
	}
}

$chequeAmt = 0;
if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
{
	if ( !empty( $_SESSION["chequeAmount"] ) )
		$chequeAmt = $_SESSION["chequeAmount"];
}

$_SESSION["senderBank"] = $_POST["senderBank"];	

$senderAccumulativeAmount = $_POST["senderAccumulativeAmount"];
$note='';

if (CONFIG_MANUAL_CODE_WITH_RANGE == "1" && $_SESSION["compManualCode"] != "") {	// [by Jamshed]
	$qDate = "SELECT MIN(created) AS created FROM ".TBL_RECEIPT_RANGE." WHERE agentID = '".$_POST["custAgentID"]."' AND used != 'Used'";
	$qDRes = mysql_query($qDate) or die("Query Error: " . $qDate . "<br>" . mysql_error());
	$qDRow = mysql_fetch_array($qDRes);
	
	$qRanVal = "SELECT * FROM ".TBL_RECEIPT_RANGE." WHERE `agentID` = '".$_POST["custAgentID"]."' AND used != 'Used' AND `created` = '".$qDRow["created"]."'";
	$rValRes = mysql_query($qRanVal) or die("Query Error: " . $qRanVal . "<br>" . mysql_error());
	$rValRow = mysql_fetch_array($rValRes);
	$usedVal = ($rValRow["used"] == "0" ? $rValRow["rangeFrom"] : $rValRow["used"]);
	
	$rangesID = $rValRow["id"];
	
	if ($rValRow["used"] != "0") {
		$usedVal++;
	}	
	$nextRangeVal = $usedVal;
	$_SESSION["refNumber"] = $_SESSION["refNumber"] . "-" . $nextRangeVal;
	
	if (($rValRow["rangeTo"] - $nextRangeVal) == 30) {
		$rangeEmailAlert = true;
	}
	if ($nextRangeVal == $rValRow["rangeTo"]) {
		$updateRangeVal = "Used";	
	} else {
		$updateRangeVal = $nextRangeVal;
	}
}

if($_POST["transID"] != "")
{ 
	$backUrl = "$returnPage?msg=Y&transID=" . $_POST["transID"]."&calculateBy=".$_POST["calculateBy"]."&transType=".$_REQUEST["transType"]."&docWaiver=".$_REQUEST["docWaiver"]."&customerID=".$_GET["customerID"];
}
else
{ 
	$backUrl = "$returnPage?msg=Y&calculateBy=".$_POST["calculateBy"]."&transType=".$_REQUEST["transType"]."&docWaiver=".$_REQUEST["docWaiver"]."&customerID=".$_GET["customerID"];
}

if(CONFIG_CASH_PAID_CHARGES_ENABLED == "1")
{
	if($_SESSION["moneyPaid"]=='By Cash')
	{
		$cashCharges = CONFIG_CASH_PAID_CHARGES;
		}
		else{
			$cashCharges = 0;
			}
}

if(trim($_POST["customerID"]) == "")
{
	insertError(TE4);	
	redirect($backUrl);
}

if(trim($benAgentIDs) == "")
{
	$disMsg = TE5;
	if(CONFIG_SINGLE_DISTRIBUTOR_SYSTEM == "1")
		$disMsg = "Please first add a Distributor in System";
	insertError($disMsg);	
	redirect($backUrl);
}

if(trim($_POST["benID"]) == ""  && CONFIG_TRANS_WITHOUT_BENEFICIARY != '1')
{
	insertError(TE6);	
	redirect($backUrl);
}
if(isset($_POST["benIdTypeFlag"]) && trim($_POST["benIdTypeFlag"]) == ""  && strstr(CONFIG_BEN_IDTYPE_COMP_TRANSACTION_TYPES,trim($_POST["transType"])))
{
	insertError("Selected Beneficiary should have ID details for Transactin Type ".trim($_POST["transType"]).". Please Edit the Beneficiary.");	
	redirect($backUrl);
}

if(isset($_POST["benSOFFlag"]) && trim($_POST["benSOFFlag"]) == ""  && strstr(strtoupper(CONFIG_BEN_SOF_TYPE_COMP_TRANSACTION_TYPES),strtoupper(trim($_POST["transType"]))))
{
	insertError("Selected Beneficiary should have S/O,W/O,D/O value for Transactin Type  ".trim($_POST["transType"]).". Please Edit the Beneficiary.");	
	redirect($backUrl);
}
// Validity cheks for Bank Details of Beneficiary.
$bankDetailsFlag = true;
if(CONFIG_USE_TRANSACTION_DETAILS == "1" && trim($_POST["useTransDetails"]) == "No"){
	$bankDetailsFlag = false;
}
/**
 * #9983: Premier Exchange: BENEFICIARY DETAILS ON DEAL CONTRACTS
 * short description
 * this is for making every transaction as incomplete transaction
 */
if(CONFIG_FOR_INCOMPLETE_TRANSACTION == "1" && trim($_POST["useTransDetails"]) == "No"){
	$bankDetailsFlag = false;
}

if(trim($_POST["transType"]) == "Bank Transfer")
{
	if($bankDetailsFlag){
		if (CONFIG_EURO_TRANS_IBAN == "1" && $_POST["benCountryRegion"] == "European") {
			
			if (trim($_POST["IBAN"]) == "") {
				insertError(TE21);
				redirect($backUrl);	
			}
			
		}	
		elseif(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
			/**
			 * #9983: Premier Exchange: BENEFICIARY DETAILS ON DEAL CONTRACTS
			 * short description
			 * this is for making fields non-mandatory on deal contract page
			 */
			if(CONFIG_FIELDS_NON_MANDATORY_DEAL != '1'){
				if (isset($_POST["IBAN"]) && trim($_POST["IBAN"]) == "") {
					insertError(TE21);	
					redirect($backUrl);
				}
				if(isset($_POST["bankName"]) && trim($_POST["bankName"]) == "") {
					insertError(TE14);	
					redirect($backUrl);
				}
				if(isset($_POST["accNo"]) && trim($_POST["accNo"]) == "" && CONFIG_ACCNO_TRANS_NON_MANDATORY!='1') {
					insertError(TE18);	
					redirect($backUrl);
				}
				if(CONFIG_BRANCH_ADDRESS_TRANS_NON_MANDATORY!="1" && isset($_POST["branchAddress"]) && trim($_POST["branchAddress"]) == "") {
					insertError(TE16);	
					redirect($backUrl);
				}
			}
		}
		else {
			/**
			 * #9983: Premier Exchange: BENEFICIARY DETAILS ON DEAL CONTRACTS
			 * short description
			 * this is for making fields non-mandatory on deal contract page
			 */
			if(CONFIG_FIELDS_NON_MANDATORY_DEAL != '1'){
				if(trim($_POST["bankName"]) == "") {
					insertError(TE14);	
					redirect($backUrl);
				}
				if(trim($_POST["accNo"]) == "" && CONFIG_ACCNO_TRANS_NON_MANDATORY!='1') {
					insertError(TE18);	
					redirect($backUrl);
				}
				if(CONFIG_BRANCH_ADDRESS_TRANS_NON_MANDATORY!="1" && trim($_POST["branchAddress"]) == "") {
					insertError(TE16);	
					redirect($backUrl);
				}
				if(CONFIG_CPF_ENABLED == '1') {
					if(trim($_POST["ABACPF"]) == "" && strstr(CONFIG_CPF_COUNTRY , '$_POST["benCountry"]')) {
						insertError(TE20);	
						redirect($backUrl);
					}
				}
			}
		}
	}
	if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS != "1")
	{
		if(isset($_POST["distribut"]) && trim($_POST["distribut"]) == "") {
			$disMsg = "Please select a Distributor from the list";
			insertError($disMsg);	
			redirect($backUrl);
		}
	}
}

if(trim($_POST["refNumber"]) == "" && CONFIG_TRANS_MANUAL_CODE_COMP == '1')
{
	insertError(TE27);	
	redirect($backUrl);
}
if(trim($_POST["transAmount"]) == "" || $_POST["transAmount"] <= 0)
{
	insertError(TE8);	
	redirect($backUrl);
}

if(trim($_POST["exchangeRate"]) == "" || $_POST["exchangeRate"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}
/*
if(trim($_POST["exchangeID"]) == "" && CONFIG_ENABLE_DOMESTIC != '1')
{
	insertError(TE9);	
	redirect($backUrl);
}*/



if(CONFIG_ZERO_FEE == '1')
{
	if( $_POST["IMFee"] < 0)
	{
		insertError(TE2);	
		redirect($backUrl);
	}
	
}else{

	if((isset($_POST["IMFee"]) && trim($_POST["IMFee"]) == "") || $_POST["IMFee"] <= 0)
	{
		insertError(TE2);	
		redirect($backUrl);
	}
}

if((isset($_POST["localAmount"]) && trim(($_POST["localAmount"]) == "") || $_POST["localAmount"] <= 0))
{
	insertError(TE9);	
	redirect($backUrl);
}

if((isset($_POST["totalAmount"]) && trim($_POST["totalAmount"]) == "") || $_POST["totalAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(isset($_POST["transactionPurpose"]) && trim($_POST["transactionPurpose"]) == "" && CONFIG_NON_COMP_TRANS_PURPOSE != "1")
{
	insertError(TE10);	
	redirect($backUrl);
}

if(isset($_POST["fundSources"]) && trim($_POST["fundSources"]) == "" && CONFIG_NONCOMPUL_FUNDSOURCES != '1')
{
	insertError(TE11);	
	redirect($backUrl);
}
if(isset($_POST["value_date"]) && trim($_POST["value_date"]) == "" && CONFIG_VALUE_DATE_MANDATORY == "1")
{
	insertError(TE32);	
	redirect($backUrl);
}
if(isset($_POST["IMFee"]) && trim($_POST["IMFee"]) == "0" && CONFIG_COMMISION_MANDATORY == "1")
{
	insertError(TE33);	
	redirect($backUrl);
}

if(CONFIG_MONEY_PAID_NON_MANDATORY != "1" && CONFIG_CALCULATE_BY != '1'){
if(isset($_POST["moneyPaid"]) && trim($_POST["moneyPaid"]) == "")
{
	
	insertError(TE12);	
	redirect($backUrl);
}
}

if (CONFIG_REFNUM_GENERATOR == '1') {
	if (!generateRefNumber($benAgentIDs)) {
		if ($agentType == 'admin') {
			insertError("Your transaction will not be created. Please generate reference number template from left menu");
		} else {
			insertError("Your transaction will not be created. Please contact super admin for reference number");
		}
		redirect($backUrl);
	}
}

if(trim($_POST["Declaration"]) != "Y")
{
	insertError(TE13);	
	redirect($backUrl);
}

 

//////////////////Add by Erfan 
if(trim($_POST["answer"]!="") && trim($_POST["question"]==""))
{
insertError(TE23);
	redirect($backUrl);
}
if(trim($_POST["answer"]=="") && trim($_POST["question"]!=""))
{
insertError(TE24);
	redirect($backUrl);
}
//////////////////////////////////////

if(CONFIG_CHECK_MANUAL_REFNUMBER_AT_TRANS != '1'){

if($_SESSION["refNumber"] != '')
{
if($_POST["transID"] != "")
		$strRandomQuery = selectFrom("select * from ". TBL_TRANSACTIONS." where transID!='".$_POST["transID"]."' and refNumber = '".$_SESSION["refNumber"]."'");
	else
		$strRandomQuery = selectFrom("SELECT * FROM ". TBL_TRANSACTIONS ." where refNumber = '".$_SESSION["refNumber"]."'");
		  
		
	//$nRandomResult = mysql_query($strRandomQuery)or die("Invalid query: " . mysql_error()); 
	//$nRows = mysql_fetch_array($nRandomResult);
	$oldReference = $strRandomQuery["refNumber"];
	if($oldReference)
	{
	insertError(TE7);	
	redirect($backUrl);
	}
	
 }
}
$agentCurrCommFlag = false;
if(CONFIG_AGENT_CURRENCY_COMMISSION=="1"){
	$agentCurrCommFlag = true;
}

if(CONFIG_SENDER_ID_EXPIRED_WAVIER==1 )
{

	if(!empty($_REQUEST["docWaiver"]))
	{
		$wavierField=" ,is_waved_doc";
		$wavierVal=" ,'1'";
		$wavUpdate=" ,is_waved_doc='1'";
	}
	else
		$wavUpdate=" ,is_waved_doc='0'";
}

	if($_POST["transID"] == "")
	{

			
			$packageQuery="select userID, username, parentID, agentType, isCorrespondent, commPackage, agentCommission,agentCurrency from ". TBL_ADMIN_USERS." where userID = '".$_POST["custAgentID"]."'";
			$agentPackage = selectFrom($packageQuery);
			
			if($agentPackage["agentType"] == 'Sub')
			{
				$commUser = $agentPackage["parentID"];	
			}else{
				$commUser = $agentPackage["userID"];	
			}
			
			if(CONFIG_USER_COMMISSION_MANAGEMENT == '1'){
				 
				$agentCommiArray = recordAgentCommissionCountrySpecific($commUser, $_POST["transAmount"], $_POST["IMFee"], "Agent", $_POST["benCountry"]);
			
			  $agentCommi = $agentCommiArray[0];
				$commType = $agentCommiArray[1];
				$agentParentID = $agentPackage["parentID"];
				$package = $agentPackage["commPackage"];
			}else{
			 	$agentCommiArray = recordAgentCommission($commUser, $_POST["transAmount"], $_POST["IMFee"], "Agent");
			
				$agentCommi = $agentCommiArray[0];
				$commType = $agentCommiArray[1];
				$agentParentID = $agentPackage["parentID"];
				$package = $agentPackage["commPackage"];
			
				
			}
		 
		 ////
		 
		 $packageQueryDistr="select userID,commPackage, agentCommission, commPackageAnDDist, commAnDDist, parentID from ". TBL_ADMIN_USERS." where userID = '".$benAgentIDs."'";
			$distrPackage = selectFrom($packageQueryDistr);
			
			if($distrPackage["agentType"] == 'Sub')
			{
				$commUser2 = $distrPackage["parentID"];	
			}else{
				$commUser2 = $distrPackage["userID"];	
			}
			if(CONFIG_USER_COMMISSION_MANAGEMENT == '1'){
				 
				$distrCommiArray = recordAgentCommissionCountrySpecific($commUser2, $_POST["localAmount"], $_POST["IMFee"], "Distributor", $_POST["benCountry"]);
			
				$distrCommi = $distrCommiArray[0];
				$commTypeDist = $distrCommiArray[1];
				$distrParentID = $distrPackage["parentID"];
				$packagedistr = $distrPackage["commPackageAnDDist"];
			}else{
			 	$distrCommiArray = recordAgentCommission($commUser2, $_POST["transAmount"], $_POST["IMFee"], "Dist");
			
			
				$distrCommi = $distrCommiArray[0];
				$commTypeDist = $distrCommiArray[1];
				$packagedistr = $distrPackage["commPackageAnDDist"];
				$distrParentID = $distrPackage["parentID"];
			
			}
			
		 
		 ////

if(CONFIG_ZERO_FEE != '1')
{
	// if(trim($_POST["IMFee"]) < $agentCommi )
	if(trim($_POST["IMFee"]) < ($agentCommi + $distrCommi)) // Modified to this condition from the condition if(trim($_POST["IMFee"]) < $agentCommi ) since the error message is used to check agentcomm + distcomm is not greater then company commission
	{
		$comm = $distrCommi + $agentCommi;
		$comm = round($comm,$roundLevel);
		//$comm = $agentCommi;
		insertError("Agent and Distributor's Commission calculated $comm, may be greater then transaction fee.");	
		redirect($backUrl);
	}
}
if(CONFIG_DOUBLE_ENTRY == '1')
	$doubleEntryAgentComm = '';
if(CONFIG_EXCLUDE_COMMISSION == '1')
{
	
		$agentLedgerAmount = $_POST["totalAmount"] - $agentCommi;
	
	
}elseif(CONFIG_AGENT_PAYMENT_MODE == "1"){
	
						$agentPaymentModeQuery = selectFrom("select paymentMode  from ". TBL_ADMIN_USERS." where userID = '".$_POST["custAgentID"]."' ");
						$agentPaymentMode = $agentPaymentModeQuery["paymentMode"];
						if($agentPaymentMode == ''){
							insertError("Please Update Agent i.e Payment Mode.");	
							  redirect($backUrl);
							}
							if($agentPaymentMode == 'exclude')
							{
									$agentLedgerAmount = $_POST["totalAmount"] - $agentCommi;
									$doubleEntryAgentComm =   $_POST["IMFee"] - $agentCommi;
							
							}elseif($agentPaymentMode == 'include')
							{
								$agentLedgerAmount = $_POST["totalAmount"];
								$doubleEntryAgentComm = $_POST["IMFee"]; 
								
							}
	
	

	}else{
	$agentLedgerAmount = $_POST["totalAmount"];
}



/////////////For Trans Date
$dDate = explode("/",$_POST["dDate"]);
			if(count($dDate) == 3)
				$dDate = $dDate[2]."-".$dDate[1]."-".$dDate[0];
			else
				$dDate = $_POST["dDate"];
				
				$_POST["dDate"] = $dDate;
				if ($dDate == "")
				{
					$_SESSION["transDate"] = getCountryTime(CONFIG_COUNTRY_CODE);
					$_SESSION["transDate2"] = "";
				}
				else
				{
					$_SESSION["transDate"] = $dDate;
					$_SESSION["transDate2"] = $dDate;
				}
				
				
/////////////For Trans Date				
	  
		if (CONFIG_REFNUM_GENERATOR == '1') {			
			$imReferenceNumber = generateRefNumber($benAgentIDs, $_POST["collectionPointID"]);	
		} else {
		debug ("here reference number startd" , true);
		$query = "select count(transID) as cnt from ". TBL_TRANSACTIONS." where 1 ";//
		if(CONFIG_TRANS_REF == '1')
		{
			$query .= " and custAgentID = '".$_POST["custAgentID"]."'";
		}
		$nextAutoID = countRecords($query);
		$nextAutoID = $nextAutoID + 1;
		
			
		if(CONFIG_TRANS_REF == '1')
		{
			$imReferenceNumber = strtoupper($agentPackage["username"]);///For Reference Number	
			if(CONFIG_NO_DASH_REF != '1')
				{
					$imReferenceNumber .= "-";///For Reference Number	
				}
				$imReferenceNumber .= $nextAutoID ;///For Reference Number		
					
		}else {
			$imReferenceNumber = $systemPre."-". $nextAutoID ;///For Reference Number	
			if(strstr(CUSTOM_AGENT_TRANS_REF , strtoupper($agentPackage["username"])))
			{
				$imReferenceNumber = strtoupper($agentPackage["username"]) ."-". $nextAutoID ;
			}
		}
		if (CONFIG_SIX_DIGITS_TRANS_NUM == "1") {
			$nextAutoIDLen = strlen($nextAutoID) ;
			if ($nextAutoIDLen < 6) {
				$howManyZeros = (6 - $nextAutoIDLen) ;
				for ($i = 0; $i < $howManyZeros; $i++) {
					$nextAutoID = "0" . $nextAutoID ;
				}
			}
			$imReferenceNumber = $systemPre . "" . strtoupper($agentPackage["username"]) . "" . $nextAutoID ;
		}
		
		if (CONFIG_REFNUMIM_AC_TO_DDMM == "1") {	// [by Jamshed]
			$transCountForToday = countRecords("SELECT COUNT(*) FROM ".TBL_TRANSACTIONS." WHERE transDate LIKE '".date("Y-m-d")."%'");
			$incrementalNum = ($transCountForToday + 1);
			if (strlen($incrementalNum) == 1) {
				$incrementalNum = "0" . $incrementalNum;	
			}
			if (CONFIG_REFNUMIM_AC_TO_DDMMYY == "1") {
				$imReferenceNumber = date("dmy") . $incrementalNum;
			} else {
				$imReferenceNumber = date("dm") . $incrementalNum;
			}
			while(isExist("SELECT `transID` FROM ".TBL_TRANSACTIONS." WHERE refNumberIM = '".$imReferenceNumber."'")) {
				$incrementalNum++;
				if (strlen($incrementalNum) == 1) {
					$incrementalNum = "0" . $incrementalNum;	
				}
				if (CONFIG_REFNUMIM_AC_TO_DDMMYY == "1") {
					$imReferenceNumber = date("dmy") . $incrementalNum;
				} else {
					$imReferenceNumber = date("dm") . $incrementalNum;
				}
			}
		}
		
		if (CONFIG_SPINZAR_TRANS_NUM == "1") {	// [by JAMSHED]
			$qCustCntryCode = "SELECT `countryCode` FROM ".TBL_COUNTRY." WHERE countryName = '".$_POST["custCountry"]."'";
			$cntryCode = selectFrom($qCustCntryCode);
			$nextAutoIDLen = strlen($nextAutoID) ;
			if ($nextAutoIDLen < 6) {
				$howManyZeros = (6 - $nextAutoIDLen) ;
				for ($i = 0; $i < $howManyZeros; $i++) {
					$nextAutoID = "0" . $nextAutoID ;
				}
			}
			$imReferenceNumber = "S" . $cntryCode["countryCode"] . $nextAutoID;
		}
		
			$strRandomQuery = selectFrom("SELECT * FROM ". TBL_TRANSACTIONS ." where refNumberIM = '".$imReferenceNumber."'");
				  
				
			//$nRandomResult = mysql_query($strRandomQuery)or die("Invalid query: " . mysql_error()); 
			//$nRows = mysql_fetch_array($nRandomResult);
			$oldReference = $strRandomQuery["refNumberIM"];
			while($oldReference)
			{
				$nextAutoID ++;
				
				
				if(CONFIG_TRANS_REF == '1')
				{
					$imReferenceNumber = strtoupper($agentPackage["username"]);///For Reference Number	
					if(CONFIG_NO_DASH_REF != '1')
					{
						$imReferenceNumber .= "-";///For Reference Number	
					}
					$imReferenceNumber .= $nextAutoID ;///For Reference Number		
				
				}else{	
					$imReferenceNumber = $systemPre."-". $nextAutoID ;///For Reference Number	
					if(strstr(CUSTOM_AGENT_TRANS_REF , strtoupper($agentPackage["username"])))
					{
						$imReferenceNumber = strtoupper($agentPackage["username"]) ."-". $nextAutoID ;
					}
				}
				
				if (CONFIG_SIX_DIGITS_TRANS_NUM == "1") {
					$nextAutoIDLen = strlen($nextAutoID) ;
					if ($nextAutoIDLen < 6) {
						$howManyZeros = (6 - $nextAutoIDLen) ;
						for ($i = 0; $i < $howManyZeros; $i++) {
							$nextAutoID = "0" . $nextAutoID ;
						}
					}
					$imReferenceNumber = $systemPre . "" . strtoupper($agentPackage["username"]) . "" . $nextAutoID ;
				}
				
				if (CONFIG_REFNUMIM_AC_TO_DDMM == "1") {	// [by Jamshed]
					$transCountForToday = countRecords("SELECT COUNT(*) FROM ".TBL_TRANSACTIONS." WHERE transDate LIKE '".date("Y-m-d")."%'");
					$incrementalNum = ($transCountForToday + 1);
					if (strlen($incrementalNum) == 1) {
						$incrementalNum = "0" . $incrementalNum;	
					}
					if (CONFIG_REFNUMIM_AC_TO_DDMMYY == "1") {
						$imReferenceNumber = date("dmy") . $incrementalNum;
					} else {
						$imReferenceNumber = date("dm") . $incrementalNum;
					}
					while(isExist("SELECT `transID` FROM ".TBL_TRANSACTIONS." WHERE refNumberIM = '".$imReferenceNumber."'")) {
						$incrementalNum++;
						if (strlen($incrementalNum) == 1) {
							$incrementalNum = "0" . $incrementalNum;	
						}
						if (CONFIG_REFNUMIM_AC_TO_DDMMYY == "1") {
							$imReferenceNumber = date("dmy") . $incrementalNum;
						} else {
							$imReferenceNumber = date("dm") . $incrementalNum;
						}
					}
				}
				
				if (CONFIG_SPINZAR_TRANS_NUM == "1") {	// [by JAMSHED]
					$qCustCntryCode = "SELECT `countryCode` FROM ".TBL_COUNTRY." WHERE countryName = '".$_POST["custCountry"]."'";
					$cntryCode = selectFrom($qCustCntryCode);
					$nextAutoIDLen = strlen($nextAutoID) ;
					if ($nextAutoIDLen < 6) {
						$howManyZeros = (6 - $nextAutoIDLen) ;
						for ($i = 0; $i < $howManyZeros; $i++) {
							$nextAutoID = "0" . $nextAutoID ;
						}
					}
					$imReferenceNumber = "S" . $cntryCode["countryCode"] . $nextAutoID;
				}
				
				$strRandomQuery = selectFrom("SELECT * FROM ". TBL_TRANSACTIONS ." where refNumberIM = '".$imReferenceNumber."'");
					
				$oldReference = $strRandomQuery["refNumberIM"];
			}
			
		}

		if($_SESSION["act"]!="")
		{
			$status = $_SESSION["act"];
			$_SESSION["act"]="";
		}else{
			$status = "Pending";
		}	
			//
			$custContents = selectFrom("select payinBook, balance  from " . TBL_CUSTOMER . " where customerID= '". $_POST["customerID"] ."'");
			$BalanceLedger = $custContents["balance"];

			if($_POST["dDate"]!= "" && CONFIG_BACK_LEDGER_DATES == '1') //The condition is if the back date value on add-transaction.php has been entered 
			{
				
				$creationDate = $_POST["dDate"];// Changed from current date to back date, if cionfig is there
				$tran_date = $_POST["dDate"];
				
			}else{ //This is if nothing enetered in the back date field on add-transaction.php no changes made in the vlues being assigned to the variables
				$creationDate = getCountryTime(CONFIG_COUNTRY_CODE);	
				$tran_date = date("Y-m-d");
			}
 // add balance effect 
        /* $intCustomerID = $_POST["customerID"];
 		if($BalanceLedger==0){ 
 		$addBalance = "-".$_POST["totalAmount"]; 
		
 		$updateBalance = update("update ".TBL_CUSTOMER." set balance= '".$addBalance."' where customerID = '".$intCustomerID."'"); 
		
 		}else{ 
 		$newBalance = $BalanceLedger - $_POST["totalAmount"]; 
 		$updateBalance = update("update ".TBL_CUSTOMER." set balance= '".$newBalance."' where customerID = '".$intCustomerID."'"); 
 		}  */ 
if($BalanceLedger >= $_POST["totalAmount"])	
			{ 
 
		//$BalanceLedger;
				$status	= "Pending";
				$AuthorizationDate = "";
/* 			$status	= "Authorize";
				$AuthorizationDate	= $tran_date; */
				
			}else{
				$status	= "Pending";
				$AuthorizationDate = "";
			}	

if($_POST["reconcile"]=="on")
	{
	     $status="Processing";
         $is_resolved= "Y"; 
         $resolveDate = date('Y-m-d');		 
			
	}
else 
{
         $is_resolved= "N"; 
		 $resolveDate = "";
		 

}
				
	if ($_SESSION['RequestforBD'] == "Y") {
		$status = 'RequestforBD';	
	}
	if(CONFIG_FEE_DISCOUNT_REPORT == '1' && $_SESSION["discount"] != '')
	{
	 	include("fee-discount.php");
  }
  
  if(CONFIG_AGENT_OWN_RATE == '1' || (CONFIG_AGENT_OWN_MANUAL_RATE == '1' && $_POST["checkManualRate"] != '') || (CONFIG_AGENT_RATE_MARGIN == '1'))
  {
  	//echo("Agent Own Rate checking");
  	include("agentOwnRate.php");
  }
  
  
  		$agentContents = selectFrom("select username, balance,email,limitUsed,agentType,parentID,agentAccountLimit,agentCompany from " . TBL_ADMIN_USERS . " where userID = '".$_POST["custAgentID"]."'");
			$agentEmail	= $agentContents["email"];
			$closingAgentBalance = selectFrom("select closing_balance from ".TBL_ACCOUNT_SUMMARY." where user_id = '".$_POST["custAgentID"]."' and (currency = '' or currency = '".$_POST["currencyFrom"]."') and dated = (select  Max(dated) from ".TBL_ACCOUNT_SUMMARY." where user_id = '".$_POST["custAgentID"]."' and (currency = '' or currency = '".$_POST["currencyFrom"]."'))");
			
			$agentBalance	= $closingAgentBalance["closing_balance"];//$agentContents["balance"];
			$agentLimit	= $agentContents["agentAccountLimit"];
			$agentUserName = $agentContents["username"];
  
  if(CONFIG_AGENT_LIMIT == "1")
			{
				$agentValidBalance = $agentBalance + $agentLimit;
			}else{
				$agentValidBalance = $agentBalance;
				}
			
  
if(CONFIG_AGENT_LIMIT_ON_CREATE_TRANSACTION == "1"){
		
		
		if(($agentValidBalance - $_POST["totalAmount"]) < 0){
			
					insertError(TE28);
					redirect($backUrl);
			
			
			}
	
	}  
  
  
if(CONFIG_COMMISSION_STATUS_ON_CANCELATION == "1"){
			
						$agentCommission=selectFrom("select paymentMode from admin where userID = '".$_POST["custAgentID"]."'");	
						
						if($agentCommission["paymentMode"]!= ""){
							
												$agentCommissionMode= $agentCommission["paymentMode"];
							
							
						}else{
							
							
												$agentCommissionMode= "";
							
							}
				
				
				}
				
			/*****************Checking condition for shared transaction agent and Distributor************/	
			if(CONFIG_SHARE_OTHER_NETWORK == '1' )
			{
				
					$benAgentInfo = selectFrom("select fromServer from " . TBL_ADMIN_USERS . " where userID = '".$benAgentIDs."'");
					if($benAgentInfo["fromServer"] != '')
					{
						$jointClientComm = selectFrom("select * from ".TBL_JOINTCLIENT." where clientName = '".$benAgentInfo["fromServer"]."' and isEnabled = 'Y'");
													
							$checkSharedAgent = selectfrom("select id, availableAt, availableAs from ".TBL_SHAREDUSERS." where availableAt = '".$jointClientComm["clientId"]."' and localServerId  = '".$_POST["custAgentID"]."' and availableAs like 'Agent'");
							//echo("select id, availableAt, availableAs from ".TBL_SHAREDUSERS." where availableAt = '".$jointClientComm["clientId"]."' and localServerId  = '".$_POST["custAgentID"]."' and availableAs like 'Agent'");													
							if($checkSharedAgent["id"] == '')
							{
								$errMsg = "Agent of Transaction is not available at ".$jointClientComm["clientName"].". Please contact Administration";
								insertError($errMsg);
								redirect($backUrl);
								
							}
					}
				}
				/***********************************Shared transactions***********************************/
				if(CONFIG_MANAGE_CLAVE == "1")
				{
					$claveData = generateDistClave($benAgentIDs);	
					
					$claveFlag = $claveData[0];
					$claveValue = $claveData[1];
					if(!$claveFlag)
					{
							insertError($claveValue);
							redirect($backUrl);
					}
				}
				/***************for maintaining wavier log*************/
				
				
				 $sql = "INSERT INTO ".TBL_TRANSACTIONS." ( recurring_count,recurring, customerID, benID, benAgentID, custAgentID,  exchangeID, refNumber, transAmount,
											 exchangeRate, isResolved ,resolveDate,
											localAmount, IMFee, totalAmount, transactionPurpose, other_pur, fundSources, moneyPaid, 
											Declaration, addedBy, transDate, transType, toCountry, fromCountry, refNumberIM, 
											currencyFrom, 
											currencyTo, custAgentParentID, benAgentParentID,collectionPointID,bankCharges,AgentComm,CommType,admincharges, cashCharges,
											question, answer,tip, outCurrCharges, distributorComm, discountRequest, transStatus, creation_date_used, distCommPackage, internalRemarks,benIDPassword,
											agentExchangeRate,  authoriseDate,commissionMode,payPointType,branchName,bankingType,chequeNo,senderBank, settlementDistributorCommisson,remTransAmount,IsReverse $wavierField )
								VALUES   (  '".$recuringCount."','".$recuring."','".$_POST["customerID"]."', '".$_POST["benID"]."', '$benAgentIDs', 
								'".$_POST["custAgentID"]."', 
								'".$_POST["exchangeID"]."', '".$_SESSION["refNumber"]."', '".$_POST["transAmount"]."', 
								'".$_POST["exchangeRate"]."','".$is_resolved."','".$resolveDate."',
											'".$_POST["localAmount"]."', '".$_POST["IMFee"]."', '".$_POST["totalAmount"]."', 
											'".checkValues($_POST["transactionPurpose"])."','".$_POST["other_pur"]."', '".checkValues($_POST["fundSources"])."', 
											'".$_POST["moneyPaid"]."', 
											'Y', '$username', '$creationDate', '".$_POST["transType"]."', '".$_POST["benCountry"]."', 
											'".$_POST["custCountry"]."', '$imReferenceNumber', '".$_POST["currencyFrom"]."',
											 '".$_POST["currencyTo"]."', 
											'".$agentParentID."', '".$distrParentID."',
											'".$_POST["collectionPointID"]."','".$_POST["bankCharges"]."','$agentCommi','$commType','".$_POST["admincharges"]."', '$cashCharges',
											'".$_SESSION["question"]."','".$_SESSION["answer"]."','".$_SESSION["tip"]."','".$_SESSION["currencyCharge"]."','$distrCommi','".$_SESSION["discount_request"]."','$status','".$_SESSION["transDate2"]."','$commTypeDist', '".$_SESSION["internalRemarks"]."', '".$_SESSION["benIdPassword"]."',
											'".$_POST["checkManualRate"]."',  '".$AuthorizationDate."','".$agentCommissionMode."','".$_SESSION["payPoint"]."','".$_SESSION["branchName"]."','".$_SESSION["bankingType"]."','".$_SESSION["chequeNo"]."','".$_POST["senderBank"]."', ".$distributorCommission.", '".$_POST["transAmount"]."', '".$reversvalue."' $wavierVal)";


		   /**
			* #10232: Premier Exchange: Module to set Country based rules on Bank Transfer
			* Short Description
			* 
			*/
			// now rule to check currency based rule against @12703
			if(CONFIG_CHK_RULE_BANK_DETAILS == '1'){
			 
			
				/* $queryRuleBankDetails = "SELECT country.countryName, countryRuleBankDetails.* FROM countries AS country INNER JOIN ".TBL_BEN_BANK_DETAILS_RULE." AS countryRuleBankDetails on countryRuleBankDetails.benCountry = country.countryId WHERE country.countryName = '{$_POST['benCountry']}' AND countryRuleBankDetails.status = 'Enable' ORDER BY countryRuleBankDetails.updateDate DESC"; */
				
				// new query for check currency based Rule by ghazanfar
				$queryRuleBankDetails = "SELECT currency.currencyName, countryRuleBankDetails.* FROM currencies AS currency INNER JOIN ".TBL_BEN_BANK_DETAILS_RULE." AS countryRuleBankDetails on countryRuleBankDetails.currency = currency.currencyName WHERE currency.currencyName = '{$_POST['currencyTo']}' AND countryRuleBankDetails.status = 'Enable' ORDER BY countryRuleBankDetails.updateDate DESC";
     			$bankDetailsRule = selectFrom($queryRuleBankDetails);
			    
				$_POST["useTransDetails"] = "Yes";
				 
				
				if($bankDetailsRule['accountName'] == 'Y' && empty($_POST['bankName']))
					$_POST["useTransDetails"] = "No";
				/*else
					$_POST["useTransDetails"] = "YES";*/
			
				if($bankDetailsRule['accNo'] == 'Y' && empty($_POST['accNo']))
					$_POST["useTransDetails"] = "No";
				
				
				if($bankDetailsRule['branchNameNumber'] == 'Y' && empty($_POST['branchCode']))
					 $_POST["useTransDetails"] = "No";
				
				
				if($bankDetailsRule['branchAddress'] == 'Y' && empty($_POST['branchAddress']))
					$_POST["useTransDetails"] = "No";
					
				
				if($bankDetailsRule['swiftCode'] == 'Y' && empty($_POST['swiftCode']))
					$_POST["useTransDetails"] = "No";
				
					
				if($bankDetailsRule['routingNumber'] == 'Y' && empty($_POST['routingNumber']))
					$_POST["useTransDetails"] = "No";
				
				
				
				if($bankDetailsRule['iban'] == 'Y' && empty($_POST['IBAN']))
					$_POST["useTransDetails"] = "No";
	 			
				
			/* $CurrencyInRule = selectFrom("SELECT currencyName FROM currencies WHERE cID = '".$bankDetailsRule['currency']."'"); */
			/* debug("SELECT currencyName FROM currencies WHERE cID = '".$bankDetailsRule['currency']."'"); */
			
			
			/* 
			
			if($bankDetailsRule['currency'] != $_POST['currencyTo']){
				$_POST["useTransDetails"] = "No";
			} */
			}
		 
			
			//debug($sql, true);
			$bundledAmount= "update ".TBL_CUSTOMER." set accumulativeAmount = '".$senderAccumulativeAmount."' where customerID = '".$_POST["customerID"]."'";
			update($bundledAmount);
		
			
	//debug($sql,true);
		insertInto($sql);
		$transID = @mysql_insert_id();
		//debug($sql,true);

		if($transID != null)
		{
			///////////////////////////////////////////////////////////

			$compLogic = selectFrom("SELECT * FROM complianceLogic where ruleID = $ruleID");

			$complianceRule = array(
				'ruleID' => $compLogic["ruleID"],
				'applyAt' => $compLogic["applyAt"],
				'userType' => $compLogic["userType"],
				'matchCriteria' => $compLogic["matchCriteria"],
				'listID' => $compLogic["listID"],
				'isEnable' => $compLogic["isEnable"],
				'buildByUserType' => $compLogic["buildByUserType"],
				'buildByUserID' => $compLogic["buildByUserID"],
				'message' => $compLogic["message"],
				'autoBlock' => $compLogic["autoBlock"],
				'dated' => $compLogic["dated"],
				'applyTrans' => $compLogic["applyTrans"],
				'amount' => $compLogic["amount"],
				'cumulativeFromDate' => $compLogic["cumulativeFromDate"],
				'cumulativeToDate' => $compLogic["cumulativeToDate"],
				'criteria_types' => $compLogic["criteria_types"],
				'idType' => $compLogic["idtype"],
				'id_type_list' => $compLogic["id_type_list"],
				'id_type_apply_user' => $compLogic["id_type_apply_user"],
				'docCategory' => $compLogic["docCategory"],
				'doc_provided' => $compLogic["doc_provided"],
				'isHold' => $compLogic["isHold"],
				'showGrid' => $compLogic["showGrid"],
				'updated' => $compLogic["updated"],
				'CTR' => $compLogic["CTR"],
				'id_expiry' => $compLogic["id_expiry"],
				'id_upload' => $compLogic["id_upload"],
				'id_check' => $compLogic["id_check"],
				'is_proceed' => $compLogic["is_proceed"],
				'isForAPItrans' => $compLogic["isForAPItrans"],
				'userID' => $compLogic["userID"]
			);
			$compRuleJSON = json_encode($complianceRule);

			$timestamp = date("Y-m-d H:i:s");

			insertInto("INSERT INTO compliance_activities_logs (comp_user_id, comp_source_table, compRuleJSON, transID, created_on) values ($customerID, 'Transaction', '$compRuleJSON', $transID, '$timestamp')");

			///////////////////////////////////////////////////////////
		
    /********insert Recurring Rule ***************/

	if($_SESSION["RecuringCheckbox"] == "on" && $_SESSION["ConfirmRecurring"]== "ConfirmRecurring")
	{
	
	
	
   $recuringQuery = "insert into recurring_transactions (transID, Status, oneTime, Time, Value, startDate, endDate,  DateForMonth, 	exchangeRateType,
   exchangeRate, alertOption, alertType )
   VALUES ('".$transID."','Active','".$_SESSION['scheduleOptions']."',
   '".$_SESSION['scheduleMode']."','',
   '".$start_date."','".$end_date."','".$_SESSION["DateMonth"]."',
   '".$_SESSION['scheduleRate']."','".$_SESSION['fixedValue']."',
   '".$alertOption."','".$alertType."') ";
  
    insertInto($recuringQuery);
	
	 $_SESSION["oneTimeTransfer"]="";
     $_SESSION["Time"]="";
     $_SESSION["Value"]="";
     $_SESSION["startDate"]="";
     $_SESSION["endDate"]="";
     $_SESSION["DateForMonth"]="";
     $_SESSION["exchangeRateType"]="";
     $_SESSION["exchangeRate"]="";
     $_SESSION["alertOption"]="";

     
	 }
 
   /**
			 * Adding the client Refrence Number
			 * @Ticket# 3321
			 */
			if(CONFIG_CLIENT_REF == "1")
			{
				update("update ".TBL_TRANSACTIONS." set clientRef='".addslashes($_SESSION["clientRef"])."' where transID=".$transID);
			}
			if(CONFIG_IMPORTED_TOTAL_TRANSACTIONS==1)
				update("UPDATE ".TBL_CUSTOMER." SET no_of_transactions= no_of_transactions+1 WHERE customerID='".$_POST["customerID"]."' ");
	
	/** 
		 * #5365 AMB Exchange
		 * Shown Note input box at add transaction page
		 * Enable  : "1"
		 * Disabl  : "0"
		 * by Niaz Ahmad at 21-08-2009
		 */ 		
		if(CONFIG_TRANSACTION_NOTE_AT_ADD_TRANSACTION == "1")
		{
		$strTransactionNotes = addslashes($_SESSION["loggedUserData"]["userID"]."|".time()."|".$_REQUEST["transNote"]);
		$strUpdateNoteSql = "update 
							  ".TBL_TRANSACTIONS." 
						  set 
							  transaction_notes = '".$strTransactionNotes."' 
						  where 
							  transID = '".$transID."'
							  ";
	
			 update($strUpdateNoteSql);	
		}
	// Added by Niaz Ahmad at 03-07-2009 @5052- GE		
	if(CONFIG_SENDER_DOCUMENT_AT_CREATE_TRANS == "1")
	{
	$strUpdateSql = "update 
						  ".TBL_TRANSACTIONS." 
					  set 
					      custDocumentProvided = '".serialize($_SESSION["docCategory"])."' 
					  where 
					      transID = '".$transID."'
						  ";

		 update($strUpdateSql);	
	}	
			/**
			 * add the fax number field based on the custAgentId
			 * @Ticket #3638
			 */
			if(CONFIG_GE_ADD_FAX_NUMBER == 1)
			{
				$strAgentLastFaxNumberSql = "Select Max(faxNumber) as mfn from ".TBL_TRANSACTIONS." where benAgentID=".$_SESSION["benAgentID"];
				$arrAgentLastFaxNumberSql = selectFrom($strAgentLastFaxNumberSql);		
				$intFaxNumber = $arrAgentLastFaxNumberSql["mfn"];
				$intFaxNumber++;
				
				update("update ".TBL_TRANSACTIONS." set faxNumber='".$intFaxNumber."' where transID=".$transID);
			}
			/* End #3638*/
			
			/**
			 * #10341: Amb Exchange:Pin Code for Money Transfer
			 * short description
			 * this config is used for update the xpin number in the database transaction table
			 * @author: Mirza Arslan Baig
			 */
			if(CREATE_XPIN_NUMBER == '1' && $_POST["transType"] != 'Home Delivery'){
				$strSqlXpin = "UPDATE ".TBL_TRANSACTIONS." SET `xpincode` = '".$_SESSION['xpin']."' WHERE transID = '$transID' LIMIT 1";
				update($strSqlXpin);
			}
			/**
			 * In case of data entered at the confirmed trasaction page of Global Exchange Reciept page
			 * SO we can use it on the print confirmed transaction page
			 * and in case of edit trasaction
			 * @Ticket# 3547
			 */
			if(CONFIG_SAVE_RECIEVED_AMOUNT == 1)
			{
				update("update ".TBL_TRANSACTIONS." set recievedAmount='".$_POST["amountRecieved"]."' where transID=".$transID);
				//unset($_SESSION["confirmTransactionData"]);
				//$_SESSION["confirmTransactionData"] = $_POST;
			}
			/* End #3547 */

			/* #4804
				TO put service Type in transaction table
			*/
			if($countryBasedFlag)
			{
				update("update ".TBL_TRANSACTIONS." set serviceType='".$_POST["serviceType"]."' where transID='".$transID."'");
			}
			if(CONFIG_CURRENCY_DENOMINATION_ON_CONFIRM == '1')
			{
				/**
				 *	Adding the currency dinomination into database
				 */
				
				$total = $_POST['field1'] * 1 + $_POST['field10'] * 10 + $_POST['field2'] * 2 + $_POST['field20'] * 20 + $_POST['field5'] * 5 + $_POST['field50'] * 50;
				
				$currencyNotesSql = "insert into ".TBL_CURRENCYNOTES." (transId, notes1, notes10, notes2, notes20, notes5, notes50, total) values(".$transID.",".$_POST['field1'].", ".$_POST['field10'].", ".$_POST['field2'].", ".$_POST['field20'].", ".$_POST['field5'].", ".$_POST['field50'].", ".$total.")";
				//echo $currencyNotesSql;
				/**
				 * inserting into databse
				 */			
				insertInto($currencyNotesSql);
		  }
	if($agentCurrCommFlag){
		$exchangeData = getMultipleExchangeRates($_POST["custCountry"],$_POST["benCountry"],$agentPackage["agentCurrency"], 0, $agentCommi , $dDate, $_POST["currencyFrom"],$_POST["transType"],$_POST["moneyPaid"], $_POST["customerID"]);
		$exAgentID		= $exchangeData[0];
		$exAgentRate	= $exchangeData[1];	
		$exAgentValue	= $agentCommi * $exAgentRate;
		//debug($agentCommi);
		if(CONFIG_ROUND_NUMBER_ENABLED == '1')
			$exAgentValue = round($exAgentValue,$roundLevel); 	
		//debug($exchangeData);
		//debug($exAgentValue);
					//debug($agentPackage);
		}
				//done against connect plus's points of showing difference on agent commission report
			if(CONFIG_USE_EXTENDED_TRANSACTION == "1")
			{
				
				
					
				if($diffType == "WITHDRAW"){
							$actualDifference = "-".$actualDiff;
					}elseif($diffType == "DEPOSIT"){
							$actualDifference = "+".$actualDiff;	
					}
				$contentExtendedTrans = selectFrom("select id,transID from ".TBL_TRANSACTION_EXTENDED."  where transID = '".$transID."'");
				
				if($contentExtendedTrans["id"] != "")
				{	
					if(CONFIG_ENABLE_EX_RATE_LIMIT  == "1")
					{
						if($_POST["exRateLimitMin"] != '' || $_POST["exRateLimitMax"] != '')
						{
							$updateRateLimit = " , minRateAlert = '".$_POST["exRateLimitMin"]."', maxRateAlert = '".$_POST["exRateLimitMax"]."', isAlertProcessed = 'N'";
						}
					}
				if($agentCurrCommFlag){
					//debug($packageQuery);
					$updateRateLimit .=",agentCurrencyCommission= '".$exAgentValue."',agentCurrency= '".$agentPackage["agentCurrency"]."' ";
					//debug($updateRateLimit);
				}
					$TransDataQuery = "update ".TBL_TRANSACTION_EXTENDED." set
					agentID = '".$_POST["custAgentID"]."',
					margin = '".$actualDifference."',
					clave = '".$claveValue."',
					benAgentID = '".$benAgentIDs."' 
					".$updateRateLimit."
					where id = '".$contentExtendedTrans["id"]."'";
					update($TransDataQuery);
				}else{
					if(CONFIG_ENABLE_EX_RATE_LIMIT  == "1")
					{
						if($_POST["exRateLimitMin"] != '' || $_POST["exRateLimitMax"] != '')
						{
							$insertRateLimitField = " , minRateAlert, maxRateAlert, isAlertProcessed";
							$insertRateLimitValue = " , '".$_POST["exRateLimitMin"]."', '".$_POST["exRateLimitMax"]."', 'N'";
						}
					}
					if($agentCurrCommFlag){
						$insertRateLimitField .= " , agentCurrencyCommission, agentCurrency";
						$insertRateLimitValue .= " , '".$exAgentValue."', '".$agentPackage["agentCurrency"]."'";
					}							
					$TransDataQuery = "insert into ".TBL_TRANSACTION_EXTENDED." 
					(transID, agentID, margin, benAgentID, clave ".$insertRateLimitField.")VALUES
					('".$transID."', '".$_POST["custAgentID"]."', '".$actualDifference."','".$benAgentIDs."', '".$claveValue."' ".$insertRateLimitValue.")";
					$TransData = insertInto($TransDataQuery);
					
					}
			}
			
			
			if(CONFIG_EXCH_RATE_USED == '1')
			{
				if($_POST["exchangeID"] > 0)
				{
						$getExchValuesQuery = "select primaryExchange, erID from " . TBL_EXCHANGE_RATES . " where erID = '".$_POST["exchangeID"]."'";
						$getExchValues = selectFrom($getExchValuesQuery);
					$historyExRateValue = selectFrom("select max(id) as historyId from ".TBL_HISTORICAL_EXCHANGE_RATES." where erID = '".$getExchValues["erID"]."'");
					$historyExRateId = $historyExRateValue["historyId"];
						
				}else{
					$originalRate = getMultipleExchangeRates($_POST["custCountry"],$_POST["benCountry"],$_POST["currencyTo"], $benAgentIDs, 0 , 0, $_POST["currencyFrom"],$_POST["transType"],$_POST["moneyPaid"], $_POST["custAgentID"]);
					 $getExchValuesQuery = "select primaryExchange, erID from " . TBL_EXCHANGE_RATES . " where erID = '".$originalRate[0]."'";
					 $historyExRateId = 0;
					}
					
					
				$companyRateDiff = $getExchValues["primaryExchange"] - $_POST["exchangeRate"];
				$exRateUsedQuery = "insert into ".TBL_EXCH_RATE_USED."(transID, rateWithMargin, rateWithoutMargin, companyMargin, currencyFrom, currencyTo, exRateHistory )values
				('".$transID."', '".$_POST["exchangeRate"]."', '".$getExchValues["primaryExchange"]."', '".$companyRateDiff."', '".$_POST["currencyFrom"]."','".$_POST["currencyTo"]."', '".$historyExRateId."')";	
				insertInto($exRateUsedQuery);
		
			}
			
			
			

			//echo("Doocument Query  ".CONFIG_CUST_DOC_FUN." is");
			if (CONFIG_CUST_DOC_FUN == '1') { // [by JAMSHED]
				
					$documentProvided = $_SESSION["documentProvided"];	
					update("UPDATE " . TBL_CUSTOMER . " SET `documentProvided` = '".$documentProvided."', `remarks` = '".$_SESSION["document_remarks"]."' WHERE customerID = '".$_POST["customerID"]."'");
			}
			
			if (CONFIG_TOTAL_FEE_DISCOUNT == '1' && $_SESSION["discount_request"] != "" && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType))) {
				
			
				
				$queryUpdateTrans = "update ". TBL_TRANSACTIONS." set 		
									discountType = 'agent',
									discounted_amount = '".$_SESSION["discount_request"]."'						
									where transID='$transID'";	
				update($queryUpdateTrans);
			}
			
			if(CONFIG_FEE_DISCOUNT_REPORT == '1')
			{
				$qUpdateTrans = "update ". TBL_TRANSACTIONS." set 
									discountRequest = '".$discountValue."',
									discounted_amount = '".$discountValue."'		
									where transID='$transID'";	
				update($qUpdateTrans);
			}
				
			if(!empty($_POST["useTransDetails"]) && CONFIG_USE_TRANSACTION_DETAILS=="1"){
				$qUpdateTrans = "update ". TBL_TRANSACTIONS." set 
									transDetails = '".$_POST["useTransDetails"]."'
									where transID='$transID'";	
				
				if(update($qUpdateTrans)){

$mail             = new PHPMailer(); // defaults to using php "mail()"

$body             = 'Part:2 This is just a Test mail, Donot take it as official';
$body             = eregi_replace("[\]",'',$body);

$mail->From = ' ';
$address = " ";
$mail->AddAddress($address,"Imran");
$mail->Subject = "PHPMailer Test Subject via mail(), basic";
$mail->IsHTML(true);

$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; 

$mail->	Body = $body;
$mail->AddAttachment("uploads/DEAL_CONTRACT.pdf"); 
//$mail->Send();
				}
			}
			if(!empty($_POST["value_date"]) && CONFIG_VALUE_DATE_TRANSACTIONS == "1"){
				$v_date_arr = explode("/",$_POST["value_date"]);
				$v_date = $v_date_arr[2]."-".$v_date_arr[1]."-".$v_date_arr[0];
				$validValueFlag = 'N';
				//debug($creationDate);
				if(!empty($creationDate)){				
					list($dateV, $timeV) = explode(' ',$creationDate);
					if($v_date <= $dateV)
						$validValueFlag = 'Y';
				}
				//debug("*".$v_date .'<='. $dateV."*");
				$qUpdateTrans = "update ". TBL_TRANSACTIONS." set 
									valueDate = '".date("Y-m-d H:i:s",strtotime($v_date))."',
									validValue = '$validValueFlag' 
									where transID='$transID'";	
				update($qUpdateTrans);
			}
			// [by Jamshed]
			if (CONFIG_MANUAL_CODE_WITH_RANGE == "1" && $_SESSION["compManualCode"] != "") {
				$qUpdateRange = "UPDATE ".TBL_RECEIPT_RANGE." SET `used` = '".$updateRangeVal."' WHERE `id` = '".$rangesID."'";
				update($qUpdateRange);
			}
			
			$reference_Number =$_SESSION["refNumber"];
			$fromName = "$company Money Support";//SUPPORT_NAME;
			$fromEmail = SUPPORT_EMAIL;
			// getting admin email address.
			$adminContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where username= 'admin'");
			$adminEmail = $adminContents["email"];
			$adminEmail = $adminContents["email"];
			

			// Getting customer's Agent Email and other necessary Information
			/*$agentContents = selectFrom("select username, balance,email,limitUsed,agentType,parentID,agentAccountLimit,agentCompany from " . TBL_ADMIN_USERS . " where userID = '".$_POST["custAgentID"]."'");
			$agentEmail	= $agentContents["email"];
			$agentBalance	= agentBalance($_POST["custAgentID"]);//$agentContents["balance"];
			$agentLimit	= $agentContents["agentAccountLimit"];
			$agentUserName = $agentContents["username"];*/
			
			// [by Jamshed]
			if (CONFIG_MANUAL_CODE_WITH_RANGE == "1" && $_SESSION["compManualCode"] != "") {
				if($rangeEmailAlert) {
					$subject = "Receipt Book Range";
					$message = "Agent: " . $agentContents["agentCompany"] . " [" . $agentContents["username"] . "] have left only 30 pages in receipt book.";
					if($adminEmail != "") {
						sendMail($adminEmail, $subject, $message, $fromName, $fromEmail);
					}
	
				}
			}
			
		if (CONFIG_REFNUM_GENERATOR != '1') {
			if (CONFIG_IMTRANSNUM_NOT_UPDATE == '1') {
				// Do nothing	
			}elseif(CONFIG_TRANS_IMREF_FORMAT == '1')
			{
				
				    $Query="SELECT * FROM customer WHERE customerID = ".$_POST["customerID"];
				    $Qresult=mysql_query($Query);
				    
				    $id = $transID;
                    $FirstName=strtoupper(mysql_result($Qresult,0,"firstName"));
                    $LastName=strtoupper(mysql_result($Qresult,0,"lastName"));
				    $imReferenceNumber=strrev($id).substr($FirstName,0,1).substr($LastName,0,1).rand(0,9);
				    mysql_query("UPDATE ". TBL_TRANSACTIONS." SET refNumberIM='$imReferenceNumber' WHERE transID=$transID");
			// echo $FirstName." ==> ".$imReferenceNumber."<br>UPDATE ". TBL_TRANSACTIONS." SET refNumberIM='$imReferenceNumber' WHERE transID=$transID";
				//  exit();
				
			} else {
				/////////////////Checking for Duplication of Reference Number/////////////
				
				
				$strRandomQuery = selectFrom("SELECT transID, refNumberIM FROM ". TBL_TRANSACTIONS ." where refNumberIM = '".$imReferenceNumber."' and transID != '".$transID."'");
				$oldReference = $strRandomQuery["refNumberIM"];
				if($oldReference != "")
				{
					while($oldReference)
					{
						$nextAutoID ++;
						
						
						if(CONFIG_TRANS_REF == '1')
						{
							$imReferenceNumber = strtoupper($agentPackage["username"]);///For Reference Number	
							if(CONFIG_NO_DASH_REF != '1')
							{
								$imReferenceNumber .= "-";///For Reference Number	
							}
							$imReferenceNumber .= $nextAutoID ;///For Reference Number		
							
						}else{	
							$imReferenceNumber = $systemPre."-". $nextAutoID ;///For Reference Number	
							if(strstr(CUSTOM_AGENT_TRANS_REF , strtoupper($agentPackage["username"])))
							{
								$imReferenceNumber = strtoupper($agentPackage["username"]) ."-". $nextAutoID ;
							}
						}
												
						if (CONFIG_SPINZAR_TRANS_NUM == "1") {	// [by JAMSHED]
							$qCustCntryCode = "SELECT `countryCode` FROM ".TBL_COUNTRY." WHERE countryName = '".$_POST["custCountry"]."'";
							$cntryCode = selectFrom($qCustCntryCode);
							$nextAutoIDLen = strlen($nextAutoID) ;
							if ($nextAutoIDLen < 6) {
								$howManyZeros = (6 - $nextAutoIDLen) ;
								for ($i = 0; $i < $howManyZeros; $i++) {
									$nextAutoID = "0" . $nextAutoID ;
								}
							}
							$imReferenceNumber = "S" . $cntryCode["countryCode"] . $nextAutoID;
						}						
						
						$strRandomQuery = selectFrom("SELECT * FROM ". TBL_TRANSACTIONS ." where refNumberIM = '".$imReferenceNumber."'");
							
						$oldReference = $strRandomQuery["refNumberIM"];
					}
					
					mysql_query("UPDATE ". TBL_TRANSACTIONS." SET refNumberIM='$imReferenceNumber' WHERE transID=$transID");
				
				}
		
					
				//////////////////////////////////////End Checking Duplication
				}	
		}
				$_POST['imReferenceNumber']=$imReferenceNumber;
			/*if(CONFIG_AGENT_LIMIT == "1")
			{
				$agentValidBalance = $agentBalance + $agentLimit;
			}else{
				$agentValidBalance = $agentBalance;
				}*/
			
						
			$agentlimitUsed	= $agentContents["limitUsed"];
			//$agentType = $typeAgent["agentType"];
			/*if($agentType=="SUBA" || $agentType=="SUBAI")
			{
				$agentCustomer=$agentContents["parentID"];
			}else{*/
				$agentCustomer=$_POST["custAgentID"];
			//}
			// getting BEneficiray agent Email
			$agentContents = selectFrom("select email, balance from " . TBL_ADMIN_USERS . " where userID = '$benAgentIDs'");
			$benAgentEmail	= $agentContents["email"];
			$benAgentBalance =  $agentContents["balance"];
			// getting beneficiary email
			$benContents = selectFrom("select email from ".TBL_BENEFICIARY." where benID= '". $_POST["benID"] ."'");
			$benEmail = $benContents["email"];
			// Getting Customer Email
			$custContents = selectFrom("select email,payinBook, balance  from " . TBL_CUSTOMER . " where customerID= '". $_POST["customerID"] ."'");
			$custEmail = $custContents["email"];
			
			//        emailing template for rule based          //
			
           		
			 $subject = "New Transaction Created";
			$message = "Congratulations!<br>
			New transaction has been created in  $company money for processing.<br>
			$systemCode is: $imReferenceNumber <br>
		    $manualCode is :  ".$_SESSION["refNumber"]." <br><br>
		
			Thank you for using  $company Money<br>
			$company Support<br>";
			//validEmail($email)
			if($custEmail != ""){
			 // send this mail to customer
			//sendMail($custEmail, $subject, $message, $fromName, $fromEmail);
			}
			// send this one to admin 
			  if($adminEmail != "")
			{
				//sendMail($adminEmail, $subject, $message, $fromName, $fromEmail);
			} 	
			
			/* if($adminEmail != "")
			{
				sendMail($adminEmail, $subject, $message, $fromName, $fromEmail);
			} */
			if(CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED)
			{
				if($agentEmail != "")
				{
					//sendMail($agentEmail, $subject, $message, $fromName, $fromEmail);
				}
			}
			if(CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED)
			{
				if($benAgentEmail != "")
				{
					//sendMail($benAgentEmail, $subject, $message, $fromName, $fromEmail);
				}
			}
			if(CONFIG_TRANSACTION_EMAIL != '1'){
			
			if($benEmail != "")
			{
//				sendMail($benEmail, $subject, $message, $fromName, $fromEmail);
			}
			if($custEmail != "")
			{
	//			sendMail($custEmail, $subject, $message, $fromName, $fromEmail);
			}
		} 
		
			////////For History
			$descript="Transaction ".$_SESSION["refNumber"]." is added ";
			activities($_SESSION["loginHistoryID"],"INSERTION",$transID,TBL_TRANSACTIONS,$descript);
			
			
			
			
		////////////if the agent has more amount already deposited, transaction will be auto authorized.custom auto authorize config is On////	
if($BalanceLedger >= $_POST["totalAmount"])	
{ 
			
if(CONFIG_POST_PAID == "0"){
					$benAgentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '$benAgentIDs'");
		if(DEST_CURR_IN_ACC_STMNTS == "1" || CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == "1")
			{
			 $amountToLedger = $_POST["localAmount"];	
			 $currecyToLedger = $_POST["currencyTo"];
			 }else{
				$amountToLedger = $_POST["transAmount"];
				$currecyToLedger = $_POST["currencyFrom"];
				}
					
					if($_POST["dDate"]!= "" && CONFIG_BACK_LEDGER_DATES == '1')
							{
								$tran_date = $_POST["dDate"];
								$tran_date2 = date("Y-m-d");
								
							}else{
								$tran_date = date("Y-m-d");
								$tran_date2 = date("Y-m-d");
							}	
					
		if($benAgentContents["agentType"] == 'Sub')
							{
								updateSubAgentAccount($benAgentIDs, $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Distributor",$currecyToLedger,$tran_date2);
								$q = updateAgentAccount($benAgentContents["parentID"], $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Distributor",$note,$currecyToLedger, $tran_date2);
							}else{
								
								$q = updateAgentAccount($benAgentIDs, $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Distributor",$note,$currecyToLedger,$tran_date2);
							}
					
					
					}
				
			}
			
			
			
			////////////////////////////end of the case
			
			
			
			
			
			
			
				if($_POST["dDate"]!= "" && CONFIG_BACK_LEDGER_DATES == '1')
				{
					$tran_date = $_POST["dDate"];
					$tran_date2 = date("Y-m-d");
				
				}else{
					$tran_date = date("Y-m-d");
					$tran_date2 = date("Y-m-d");
				}
			
			/////////////////////////COmmission Margin For Agent//////////////
			
			if((CONFIG_AGENT_OWN_RATE == '1' || (CONFIG_AGENT_OWN_MANUAL_RATE == '1' && $_POST["checkManualRate"] != '') || (CONFIG_AGENT_RATE_MARGIN == '1')) && $actualDiff > 0)
			{
			
				$firstType = $diffType;
				$firstLabel = "Commission Margin";
				if(CONFIG_DUAL_ENTRY_IN_LEDGER == "1"){
						if($diffType == "WITHDRAW")
						{
						$firstType = 'WITHDRAW';
						$firstLable = "Agent Adjustment Lost";
						
						$secondtype = 'DEPOSIT';
						$secondLabel	= COMPANY_NAME." Adjustment Gain";
					}elseif($diffType == "DEPOSIT"){
						
						$firstType = 'DEPOSIT';
						$firstLable = 'Agent Adjustment Gain';
						
						$secondtype = 'WITHDRAW';
						$secondLabel	= 'Agent Adjustment Lost';
						}
				}
			
			
					if(CONFIG_NEGETIVE_ENTRY_IN_LEDGER == "1" && $diffType == "DEPOSIT")
					{
						/*	$agentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$_POST["custAgentID"]."'");
							if($agentContents["agentType"] == 'Sub')
							{
								updateSubAgentAccount($agentContents["userID"], $actualDiff, $transID, $secondtype, $firstLable, "Agent",$currencyFrom,$tran_date2);
						  $q = updateAgentAccount($agentContents["parentID"], $actualDiff, $transID, $secondtype, $firstLable, "Agent",$note,$currencyFrom,$tran_date2);
							}else{
								$q = updateAgentAccount($agentContents["userID"], $actualDiff, $transID, $secondtype, $firstLable, "Agent",$note,$currencyFrom,$tran_date2);
							}
				
						*/
					}
					
					/*
					else
					{
						/// Just enabling or disabling the functionality to update the agent legder in case of agent own rate 
						if(CONFIG_DISABLE_AGENT_OWN_RATE_LEDGER_ENTRY != "1")
						{
							$agentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$_POST["custAgentID"]."'");
							if($agentContents["agentType"] == 'Sub')
							{
								updateSubAgentAccount($agentContents["userID"], $actualDiff, $transID, $firstType, $firstLable, "Agent",$currencyFrom,$tran_date2);
								$q = updateAgentAccount($agentContents["parentID"], $actualDiff, $transID, $firstType, $firstLable, "Agent",$note,$currencyFrom,$tran_date2);
							}
							else
							{
								$q = updateAgentAccount($agentContents["userID"], $actualDiff, $transID, $firstType, $firstLable, "Agent",$note,$currencyFrom,$tran_date2);
							}
						}
						
						
							
										$agentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$_POST["custAgentID"]."'");
										if($agentContents["agentType"] == 'Sub')
										{
											updateSubAgentAccount($agentContents["userID"], $actualDiff, $transID, $secondtype, $firstLable, "Agent",$currencyFrom,$tran_date2);
									  $q = updateAgentAccount($agentContents["parentID"], $actualDiff, $transID, $secondtype, $firstLable, "Agent",$note,$currencyFrom,$tran_date2);
										}else{
											$q = updateAgentAccount($agentContents["userID"], $actualDiff, $transID, $secondtype, $firstLable, "Agent",$note,$currencyFrom,$tran_date2);
										}
							
						
							
					
					
					}
					*/
					else{
										$agentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$_POST["custAgentID"]."'");
										if($agentContents["agentType"] == 'Sub')
										{
											updateSubAgentAccount($agentContents["userID"], $actualDiff, $transID, $firstType, $firstLable, "Agent",$currencyFrom,$tran_date2);
									  $q = updateAgentAccount($agentContents["parentID"], $actualDiff, $transID, $firstType, $firstLable, "Agent",$note,$currencyFrom,$tran_date2);
										}else{
											/**
											 * Referenct Ticket #4064
											 * Handling Gain/Loss ledger entry for Payin Book Agent and for Non Payin Book Agent
											 * The original case is, when a sender (customer) is Payin Book Customer then the Payin Book Agent
											 * Account should be impacted with all ledger enteries related to agent ledger, otherwise the selected
											 * agent will be effected.
											 **/
											
											// Check if customer is Payin book
											$pSql = "select payinBook from ".TBL_CUSTOMER." where customerID = '".$_SESSION["customerID"]."'";
											$pResult = mysql_query($pSql) or die(__LINE__.": ".mysql_error());
											
											while($pRs = mysql_fetch_array($pResult))
											{
												$tmpPayinBook = $pRs["payinBook"];
											}
											
											if(CONFIG_PAYIN_CUST_AGENT == "1" && !empty($tmpPayinBook))
											{
												$tmpAgentAccount = CONFIG_PAYIN_AGENT_NUMBER;
												/* #5003 
													If There is no Agent Number defined for Payin customers
													Then All Agents will be dealt in same way. 
													There Ledgers will be affected accordingly if
													CONFIG_PAYIN_CUST_AGENT is ON and Customer is Payin.
												*/
												if(CONFIG_PAYIN_CUST_AGENT_ALL=="1"){
													$tmpAgentAccount =  $agentContents["userID"];
												}
											} else {
												$tmpAgentAccount = $agentContents["userID"];
											}
		/*
		 This config added by Niaz Ahmad @4691 (Muthoot) at 17-03-2009. when transaction cancell then
		 reverse entry of profit or loss made by agent in case of manual exchange rate will be done.
		*/			
						if(CONFIG_USE_EXTENDED_TRANSACTION == "1"){					
						 
						 if($diffType == "WITHDRAW")
						{
						
						$firstLable = "Agent Adjustment Lost";
						
						
					}elseif($diffType == "DEPOSIT"){
						
						$firstLable = 'Agent Adjustment Gain';
						
						 }
					
/*
* Added by Niaz Ahmad at 11-06-2009
* @5056 Now Transfer
* This config stop profit and loss entry in agent ledger account
* If remove this functionality then this config must be commented or removed
* Just change the value of config from One to Zero not affect the functionality.
*/			}					
			if(!defined("CONFIG_AT_CREATE_TRANSACTION_STOP_PROFIT_LOSS_ENTRY"))		
		           $q = updateAgentAccount($tmpAgentAccount, $actualDiff, $transID, $firstType, $firstLable, "Agent",$note,$currencyFrom,$tran_date2);
					}
										
			}
		/////<<<<<<<}
				
				
				IF(CONFIG_DUAL_ENTRY_IN_LEDGER == "1"){
					
				if($firstType == 'WITHDRAW'){
					
											if($agentContents["agentType"] == 'Sub')
										{
											updateSubAgentAccount($agentContents["userID"], $actualDiff, $transID, $secondtype, $secondLabel, "Agent",$currencyFrom,$tran_date2);
									  $q = updateAgentAccount($agentContents["parentID"], $actualDiff, $transID, $secondtype, $secondLabel, "Agent",$note,$currencyFrom,$tran_date2);
										}else{
											$q = updateAgentAccount($agentContents["userID"], $actualDiff, $transID, $secondtype, $secondLabel, "Agent",$note,$currencyFrom,$tran_date2);
										}
			
				 
							$calculatedComm = $agentCommi - $actualDiff ;
							
						}else if($firstType == 'DEPOSIT')
						{
								$calculatedComm = $agentCommi ;
						}
		
														
					}

			}
			
			
			
			////////////////////////////For Payin Customer Account/////////////
			if(CONFIG_PAYIN_CUSTOMER == "1")
			{
			 
				if($custContents["payinBook"] != "")
				{
					/**
					 * Feed the outstanding amount from the customer
					 * @Ticket #3733
					 */
					if(CONFIG_SAVE_RECIEVED_AMOUNT == 1)
					{
						if(CONFIG_SAVE_FULL_AMOUNT_IN_LEDGER == "true" && $_POST["totalAmount"] == $_POST["amountRecieved"])
						{
							$intOutstandingAmount = $_POST["totalAmount"];
						} else {
							$intOutstandingAmount = $_POST["totalAmount"] - $_POST["amountRecieved"];
						}
						
						if(CONFIG_EFFECT_LEDGERS_WITH_DOUBLE_ENTRY == "true")
						{
							$strQuery = "insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
									 values('".$_POST["customerID"]."','".$tran_date."','Transaction is Created','WITHDRAW','".$_POST["totalAmount"]."','".$transID."','".$changedBy."'
									 )";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
							
						/************agent_company_account************/
							$strQuery = "insert into ".TBL_AGENT_COMPANY_ACCOUNT."( Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
									 values( '".$tran_date."','Transaction is Created','DEPOSIT','".$_POST["totalAmount"]."','".$transID."','".$changedBy."'
									 )";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
							
							$strQuery = "insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
									 values('".$_POST["customerID"]."','".$tran_date."','Transaction Amount Received','DEPOSIT','".$_POST["amountRecieved"]."','".$transID."','".$changedBy."'
									 )";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
							
							/************agent_company_account************/
							
							$strQuery = "insert into ".TBL_AGENT_COMPANY_ACCOUNT."( Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
									 values( '".$tran_date."','Transaction Amount Received','WITHDRAW','".$_POST["amountRecieved"]."','".$transID."','".$changedBy."'
									 )";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
							
						} else {
							$strQuery = "insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
									 values('".$_POST["customerID"]."','".$tran_date."','Transaction is Created','WITHDRAW','".$intOutstandingAmount."','".$transID."','".$changedBy."'
									 )";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
							
							$strQuery = "insert into ".TBL_AGENT_COMPANY_ACCOUNT."(Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
									 values('".$tran_date."','Transaction is Created','DEPOSIT','".$intOutstandingAmount."','".$transID."','".$changedBy."'
									 )";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
							
						}
						
						$custBalance = $custContents["balance"];
						
						$custBalance = $custBalance - $intOutstandingAmount;
						$update_Balance = "update ".TBL_CUSTOMER." set balance = ".$custBalance." where customerID= '". $_POST["customerID"] ."'";
					}
					else
					{
						$strQuery = "insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
								 values('".$_POST["customerID"]."','".$tran_date."','Transaction is Created','WITHDRAW','".$_POST["totalAmount"]."','".$transID."','".$changedBy."'
								 )";						 
						$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
						$account1 = mysql_insert_id();
						 
							
						$strQuery = "insert into ".TBL_AGENT_COMPANY_ACCOUNT." ( Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
								 values('".$tran_date."','Transaction is Created','DEPOSIT','".$_POST["totalAmount"]."','".$transID."','".$changedBy."'
								 )";						 
						$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
						$account = mysql_insert_id();
						
						if(CONFIG_EFFECT_LEDGERS_WITH_DOUBLE_ENTRY == "true")
						{
							$strQuery = "insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
									 values('".$_POST["customerID"]."','".$tran_date."','Transaction Amount Received','DEPOSIT','".$_POST["amountRecieved"]."','".$transID."','".$changedBy."'
									 )";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
							$account1 = mysql_insert_id();
						 
							
							$strQuery = "insert into ".TBL_AGENT_COMPANY_ACCOUNT."( Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
									 values( '".$tran_date."','Transaction Amount Received','WITHDRAW','".$_POST["amountRecieved"]."','".$transID."','".$changedBy."'
									 )";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
							$account = mysql_insert_id();
						}
						
						$custBalance = $custContents["balance"];
						
						$custBalance = $custBalance - $_POST["totalAmount"];
						$update_Balance = "update ".TBL_CUSTOMER." set balance = ".$custBalance." where customerID= '". $_POST["customerID"] ."'";
					}
				 
					if($_POST["WtReconcilation"] == "on"){
					
					mysql_query ("UPDATE agents_customer_account SET type = 'WITHDRAW',currencytitle = '".$_POST["currencyFrom"]."' WHERE caID = ".$account1."");
					
					mysql_query ("UPDATE ".TBL_AGENT_COMPANY_ACCOUNT." SET type = 'DEPOSIT',currencytitle = '".$_POST["currencyFrom"]."' WHERE caID = ".$account."");
				 
				}
		 
					 
					update($update_Balance);
					
					if(CONFIG_PAYIN_CUST_AGENT == '1')
					{
						$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".CONFIG_PAYIN_AGENT_NUMBER."'";
						if(CONFIG_PAYIN_CUST_AGENT_ALL=="1" && isset($_POST["custAgentID"])){
							$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$_POST["custAgentID"]."'";
						}
						$agentPayinContents = selectFrom($agentPayinQuery);
						if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == '1' && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD != "1")
						{
							if(CONFIG_SAVE_RECIEVED_AMOUNT == 1)
							{
								if(CONFIG_SAVE_FULL_AMOUNT_IN_LEDGER == "true" && $_POST["localAmount"] == $_POST["amountRecieved"])
								{
									$amountToLedger = $_POST["localAmount"];
								} else {
									$amountToLedger = $_POST["localAmount"] - $_POST["amountRecieved"];
								}
							} else {
								$amountToLedger = $_POST["localAmount"];	
							}
							
							$currecyToLedger = $_POST["currencyTo"];
						}else{
							if(CONFIG_SAVE_RECIEVED_AMOUNT == 1)
							{
								if(CONFIG_SAVE_FULL_AMOUNT_IN_LEDGER == "true" && $_POST["totalAmount"] == $_POST["amountRecieved"])
								{
									$amountToLedger = $_POST["totalAmount"];
								} else {
									$amountToLedger = $_POST["totalAmount"] - $_POST["amountRecieved"];
								}
							} else {
								$amountToLedger = $_POST["totalAmount"];
							}
							
							$currecyToLedger = $_POST["currencyFrom"];
						}
							
						if($agentPayinContents["agentType"] == 'Sub')
						{
							updateSubAgentAccount($agentPayinContents["userID"], $amountToLedger, $transID, 'WITHDRAW', "Transaction Created", "Agent",$currecyToLedger,$tran_date);
							$q = updateAgentAccount($agentPayinContents["parentID"], $amountToLedger, $transID, 'WITHDRAW', "Transaction Created", "Agent",$note,$currecyToLedger,$tran_date);
							
							if(CONFIG_EFFECT_LEDGERS_WITH_DOUBLE_ENTRY == "true")
							{
								updateAgentAccount($agentPayinContents["parentID"], $_POST["totalAmount"], $transID, 'WITHDRAW', "Transaction Created", "Agent",$note,$currecyToLedger,$tran_date);
								updateAgentAccount($agentPayinContents["parentID"], $_POST["amountRecieved"], $transID, 'DEPOSIT', "Transaction Amount Received", "Agent",$note,$currecyToLedger,$tran_date);
							} else {
								$q = updateAgentAccount($agentPayinContents["parentID"], $amountToLedger, $transID, 'WITHDRAW', "Transaction Created", "Agent",$note,$currecyToLedger,$tran_date);
							}
						}else{
							if(CONFIG_EFFECT_LEDGERS_WITH_DOUBLE_ENTRY == "true")
							{
								updateAgentAccount($agentPayinContents["userID"], $_POST["totalAmount"], $transID, 'WITHDRAW', "Transaction Created", "Agent",$note,$currecyToLedger,$tran_date);
								updateAgentAccount($agentPayinContents["userID"], $_POST["amountRecieved"], $transID, 'DEPOSIT', "Transaction Amount Received", "Agent",$note,$currecyToLedger,$tran_date);
							} else {
								$q = updateAgentAccount($agentPayinContents["userID"], $amountToLedger, $transID, 'WITHDRAW', "Transaction Created", "Agent",$note,$currecyToLedger,$tran_date);
							}
						}	
					}
					
				}
				
			
			 
			}
			//////////////////////////
			
			/////////////////////For Auto Authorization
			$ledger_entry = "No";

			if(CONFIG_CHEQUE_BASED_TRANSACTIONS == "1" && $_SESSION["moneyPaid"]== "By Cheque"){
				
				//no auto authorisation in this case
				sendCustomMail($transID);
				
				
			}elseif(CONFIG_AUTO_AHTHORIZE == "1")
			{

				if($custContents["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")///////If Not a payin book customer
				{
					$agentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$_POST["custAgentID"]."'");
			
					if($agentValidBalance >= $_POST["totalAmount"])
					{
						//////////////////If Balance is enough
						
						//$status	= "Authorize";
					$status	= "Pending";
						////////////////Either to insert into Agent or A&D Account and Updating Agent Account Summary////////
						if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == '1' && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD != "1")
						{
							$amountToLedger = $_POST["localAmount"];	
							$currecyToLedger = $_POST["currencyTo"];
						}else{
							$amountToLedger = $agentLedgerAmount;
							$currecyToLedger = $_POST["currencyFrom"];
							}
						if($agentContents["agentType"] == 'Sub')
						{
							updateSubAgentAccount($agentContents["userID"], $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Agent",$currecyToLedger,$tran_date2);
							$q = updateAgentAccount($agentContents["parentID"], $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Agent",$note,$currecyToLedger,$tran_date2);
						}else{
							
							$q = updateAgentAccount($agentContents["userID"], $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Agent",$note,$currecyToLedger,$tran_date2);
						}
						
						
						/////////////////////Either to insert into Agent or A&D Account////////////////
						//$q=mysql_query($insertQuery);//////Insert into Agent Account
						if($q)
						{
							$ledger_entry = "Yes";
						}
							
						

						$currentBalance = $agentBalance	- $agentLedgerAmount;				
						update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$agentCustomer'");
						update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='".$username."', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='".$transID."'");				

						/////////////////////Added to audit trial/////////
						$descript = "Transaction is Authorized";
						activities($_SESSION["loginHistoryID"],"UPDATION",$transID,TBL_TRANSACTIONS,$descript);	
				//////////////////////////////////
				
						////////////////Either to insert into Bank or A&D Account and Updating Agent Account Summary////////
						if(DEST_CURR_IN_ACC_STMNTS == "1" || CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == "1")
						{
							$amountToLedger = $_POST["localAmount"];	
							$currecyToLedger = $_POST["currencyTo"];
						}else{
							$amountToLedger = $_POST["transAmount"];
							$currecyToLedger = $_POST["currencyFrom"];
							}
						$benAgentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '$benAgentIDs'");
						if($benAgentContents["agentType"] == 'Sub')
						{
							
							updateSubAgentAccount($benAgentIDs, $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Distributor",$currecyToLedger, $tran_date2);
							$q = updateAgentAccount($benAgentContents["parentID"], $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Distributor",$note,$currecyToLedger,$tran_date2);
						}else{
								
							$q = updateAgentAccount($benAgentIDs, $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Distributor",$note,$currecyToLedger,$tran_date2);
						}
						////////////////////////////////////////////
						$currentBalanceBank = $benAgentBalance	- $_POST["transAmount"];
						
										
					
						}
					} else if ($custContents["payinBook"] != "" && CONFIG_AUTO_PAYIN_NUMBER == "1") {/////what is purpose of this condition???

						/////transaction is auto athorized for payin book sender if Config is ON
						

						update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='".$username."', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='".$transID."'");
					}
					sendCustomMail($transID);
				}else if(CONFIG_CUSTOM_AUTHORIZE == "1"){
			
				//////////////////////////Custom authorization for the Selected Agents
				if(strstr(CONFIG_CUSTOM_AGENTS , $agentUserName))
				{
				//	echo(CONFIG_CUSTOM_AGENTS);
					if($custContents["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")///////If Not a payin book customer
					{
						
					$agentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$_POST["custAgentID"]."'");
				
						//if($agentValidBalance >= $_POST["totalAmount"])
						{
							//////////////////If Balance is enought
							
							//$status	= "Authorize";
							$status	= "Pending";
							if($_POST["dDate"]!= "" && CONFIG_BACK_LEDGER_DATES == '1')
							{
								$tran_date = $_POST["dDate"];
								$tran_date2 = date("Y-m-d");
								
							}else{
								$tran_date = date("Y-m-d");
								$tran_date2 = date("Y-m-d");
							}
							
							
							////////////////Either to insert into Agent or A&D Account////////Updating Agent Account Summary/////////
							if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == '1' && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD != "1")
							{
								$amountToLedger = $_POST["localAmount"];	
								$currecyToLedger = $_POST["currencyTo"];
							}else{
								$amountToLedger = $agentLedgerAmount;
								$currecyToLedger = $_POST["currencyFrom"];
								}
							if($agentContents["agentType"] == 'Sub')
							{
						
								updateSubAgentAccount($agentContents["userID"], $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Agent",$currecyToLedger,$tran_date);
								$q = updateAgentAccount($agentContents["parentID"], $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Agent",$note,$currecyToLedger,$tran_date);
							}else{
								
								$q = updateAgentAccount($agentContents["agentType"], $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Agent",$note,$currecyToLedger,$tran_date);
							}
							
							//////////////////////////////////////////////
							//$q=mysql_query($insertQuery);//////Insert into Agent Account
							if($q)
							{
								$ledger_entry = "Yes";
								}
							$currentBalance = $agentBalance	- $agentLedgerAmount;				
						
							//update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$agentCustomer'");
							update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='".$username."', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='".$transID."'");				

			
							/////////////////////Added to audit trial/////////
				$descript = "Transaction is Authorized";
				activities($_SESSION["loginHistoryID"],"UPDATION",$transID,TBL_TRANSACTIONS,$descript);	
							//////////////////////////////////
				
							
							////////////////Either to insert into Bank or A&D Account////////Updating Agent Account Summary/////////
							//$amount = (DEST_CURR_IN_ACC_STMNTS == "1" ? $_POST["localAmount"] : $_POST["transAmount"]);
							$benAgentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '$benAgentIDs'");
							
							if(DEST_CURR_IN_ACC_STMNTS == "1" || CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == "1")
							{
								$amountToLedger = $_POST["localAmount"];	
								$currecyToLedger = $_POST["currencyTo"];
							 }else{
								$amountToLedger = $_POST["transAmount"];
								$currecyToLedger = $_POST["currencyFrom"];
								}
								
							
							if($benAgentContents["agentType"] == 'Sub')
							{
								updateSubAgentAccount($benAgentIDs, $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Distributor",$currecyToLedger,$tran_date2);
								$q = updateAgentAccount($benAgentContents["parentID"], $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Distributor",$note,$currecyToLedger, $tran_date2);
							}else{
								
								$q = updateAgentAccount($benAgentIDs, $amountToLedger, $transID, "WITHDRAW", "Transaction Authorized", "Distributor",$note,$currecyToLedger,$tran_date2);
							}
							///////////////////////////////////
							
							
							$currentBalanceBank = $benAgentBalance	- $_POST["transAmount"];				
							//update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '$benAgentIDs'");
							}
						}
					}
					sendCustomMail($transID);
				}
			
			
			
			///////Putting value inn ledger at the time of creation of transaction
			
			if(CONFIG_LEDGER_AT_CREATION == "1" && $ledger_entry == "No")
			{
				
				if($custContents["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1" || CONFIG_GE_AFFECT_LEDGERS_WITH_PAYINBOOK == 1)///////If Not a payin book customer
				{
				$agentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$_POST["custAgentID"]."'");
				
					
					if($_POST["dDate"]!= "" && CONFIG_BACK_LEDGER_DATES == '1')
					{
						$tran_date = $_POST["dDate"];
						
					}else{
						$tran_date = date("Y-m-d");
					}
					
					
					////////////////Either to insert into Agent or A&D Account////////
					///////Updating Agent Account Summary/////////
						
					if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == '1' && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD != "1")
					{
						/* Changing the localAmount to totalAmount as bug found via ticket #3829 */
						$amountToLedger = $_POST["totalAmount"];	
						$currecyToLedger = $_POST["currencyTo"];
					}else{
						$amountToLedger = $agentLedgerAmount;
						$currecyToLedger = $_POST["currencyFrom"];
						}	
						
					if($agentContents["agentType"] == 'Sub')
					{
					
						
						updateSubAgentAccount($agentContents["userID"], $amountToLedger, $transID, "WITHDRAW", "Transaction Created", "Agent",$currecyToLedger,$tran_date);
						
						//if(CONFIG_EFFECT_LEDGERS_WITH_DOUBLE_ENTRY == "true")
						//{
						//	updateAgentAccount($agentContents["parentID"], $_POST["totalAmount"], $transID, "WITHDRAW", "Transaction Created", "Agent",$note,$currecyToLedger,$tran_date);
						//	updateAgentAccount($agentContents["parentID"], $_POST["amountRecieved"], $transID, "DEPOSIT", "Transaction Amount Received", "Agent",$note,$currecyToLedger,$tran_date);
						//} else {
							$q = updateAgentAccount($agentContents["parentID"], $amountToLedger, $transID, "WITHDRAW", "Transaction Created", "Agent",$note,$currecyToLedger,$tran_date);
						//}
					}else{
						//if(CONFIG_EFFECT_LEDGERS_WITH_DOUBLE_ENTRY == "true")
						//{
						//	updateAgentAccount($agentContents["userID"], $_POST["totalAmount"], $transID, "WITHDRAW", "Transaction Created", "Agent",$note,$currecyToLedger,$tran_date);
						//	updateAgentAccount($agentContents["userID"], $_POST["amountRecieved"], $transID, "DEPOSIT", "Transaction Amount Received", "Agent",$note,$currecyToLedger,$tran_date);
						//} else {
						/*in double entry system one entry go for trans amount and one entry for commission amount*/
						    if(CONFIG_DOUBLE_ENTRY == '1')
								$amountToLedger = $_POST["transAmount"];
							
							$q = updateAgentAccount($agentContents["userID"], $amountToLedger, $transID, "WITHDRAW", "Transaction Created", "Agent",$note,$currecyToLedger,$tran_date);
						//}
					}
					
					//$q=mysql_query($insertQuery);//////Insert into Agent Account
					
					
						$currentBalance = $agentBalance	- $agentLedgerAmount;				
					//	update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$agentCustomer'");
					
					
				}
					
				/**
				 * In case of logged in user is Admin Office / Admin staff, then
				 * maintain it ledgers as well as super/sub agents
				 * @Ticekt #4896
				 */
				if(isset($_arrAdminStaffForCreateTransactionLedgers) && in_array($agentType, $_arrAdminStaffForCreateTransactionLedgers))
				{
					//updateAgentAccount($agentContents["userID"], $amountToLedger, $transID, "WITHDRAW", "Transaction Created", "Agent",$note,$currecyToLedger,$tran_date);
					maintainAdminStaffAccount($_SESSION["loggedUserData"]["userID"], $amountToLedger, $transID, "WITHDRAW", "Transaction Created", $note, $currecyToLedger, $tran_date);
	
				}
			}
			
			
			/////////////////Teller Cashier Logic/////////
			$agentTypeTeller = getAgentType();
			
			if(CONFIG_TELLER_CREATE_TRANS == '1' && $agentTypeTeller == 'TELLER')
			{
				$teller = $_SESSION["loggedUserData"]["tellerID"];
				$insertQuery = "insert into ".TBL_TELLER_ACCOUNT." (tellerID, dated, type, amount, modified_by, TransID, description) values('$teller', '$tran_date', 'DEPOSIT', '".$_POST["totalAmount"]."', '$changedBy', '". $transID."', 'Transaction Created by Teller')";	
				insertInto($insertQuery);
				
				
			$tellerQuery="select balance from ". TBL_TELLER." where tellerID = '".$teller."'";
			$tellerContent = selectFrom($tellerQuery);
			$tellerBalance = $tellerContent["balance"];
			$tellerBalance += $_POST["totalAmount"];
			update("update ".TBL_TELLER." set balance = '".$tellerBalance."' where tellerID = '".$teller."'");
			
			}
			/////////////////////End of Teller Cashier Logic/////////
			//////Profit at exchange Rate for Spinzar//////
			if(CONFIG_EXCHNG_MARGIN_SHARE == '1')
			{
				$agentContents = selectFrom("select userID, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$_POST["custAgentID"]."'");
				if($agentContents["agentType"] == 'Sub')
				{
					shareProfit($agentContents["parentID"], $transID, 'Agent');///////For Super Agent
					shareProfit($agentContents["userID"], $transID, 'Agent');///for Sub Agent
					
				}else{
					shareProfit($agentContents["userID"], $transID, 'Agent');/////For Super Agent
					}
				
				
				$benAgentContents = selectFrom("select userID, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '$benAgentIDs'");
				if($benAgentContents["agentType"] == 'Sub')
				{
					shareProfit($benAgentContents["parentID"], $transID, 'Distributor');///////For Super Distributor
					shareProfit($benAgentContents["userID"], $transID, 'Distributor');///for Sub Distributor
					
				}else{
					shareProfit($benAgentContents["userID"], $transID, 'Distributor');/////For Super Distributor
				}
		
			}
			
			//////////////////////////////////////////////
			
			
			
			
			if(trim($_POST["transType"]) == "Bank Transfer")
			{
				$bankDetailsTable = "bankDetails";
				if ( strtoupper($_SESSION["transactionCreatedBy"]) == "CUSTOMER" )
				{
					$bankDetailsTable = "cm_bankdetails";
				}
				
				$sqlBank = "insert into " . $bankDetailsTable . "
							( benID, transID, bankName, 
							accNo, branchCode,branchName,branchAddress, 
							ABACPF , IBAN,swiftCode, accountType, Remarks, originalBankId )
							Values('".$_POST["benID"]."', '$transID',  '".$_POST["bankName"]."',
							'".$_POST["accNo"]."', '".$_POST["branchCode"]."','".$_POST["branchName"]."','".$_POST["branchAddress"]."',
							'".$_POST["ABACPF"]."',	'".$_POST["IBAN"]."',	'".$_POST["swiftCode"]."','".$_SESSION["accountType"]."', '".$_POST["ibanRemarks"]."', '".$_SESSION["bankId"]."')";
				insertInto($sqlBank);
				$bankLastInsertID = @mysql_insert_id();
					if(CONFIG_ADD_BEN_BANK_FIELDS_ON_CREATE_TRANSACTION == "1")
					{
					
						$sqlBankUpdate = " update ".$bankDetailsTable."
											set
												sortCode = '".$_POST["sortCode"]."',
												routingNumber = '".$_POST["routingNumber"]."'
											where
													bankID = '".$bankLastInsertID."'";
								
								update($sqlBankUpdate);					
					}
			}
			
			/**
			 * Developed just to cater the need for ATM Card for eTransact API Integration 
			 * For Universal Swift / Rainbow Exchange
			 */
			if($_REQUEST["transType"] == "ATM Card")
			{
				$sqlBank = "insert into bankDetails
							( benID, transID, accNo, accountType, bankName)
							Values('".$_REQUEST["benID"]."', '$transID',  '".$_REQUEST["cardNo"]."', '".$_REQUEST["cardName"]."', '".$_REQUEST["bankName"]."')";
				insertInto($sqlBank);
			}
			
			
			//$sql
	if(CONFIG_SHARE_OTHER_NETWORK == '1' )
	{
		
			$benAgentInfo = selectFrom("select fromServer from " . TBL_ADMIN_USERS . " where userID = '".$benAgentIDs."'");
			if($benAgentInfo["fromServer"] != '')
			{
				
				$jointClientComm = selectFrom("select * from ".TBL_JOINTCLIENT." where clientName = '".$benAgentInfo["fromServer"]."' and isEnabled = 'Y'");
				{
					$sharedTransComm = $_POST["IMFee"] - $agentCommi;
						//$clientComm = (($sharedTransComm * ($jointClientComm["commissionShare"])) / 100);
						if(CONFIG_SHARE_FROM_TOTAL == '1')
						{													
							$clientComm = (($_POST["IMFee"] * ($jointClientComm["commissionShare"])) / 100);
						}else{
							$clientComm = (($sharedTransComm * ($jointClientComm["commissionShare"])) / 100);
							}
						
						$clientTotalAmount = $_POST["transAmount"] + $clientComm;
						/*$agentCommi = ($clientComm * $agentPackage["agentCommission"]) / 100;
						$commType = "Percentage of Fee";
						
						update("update ".TBL_TRANSACTIONS." set AgentComm = '".$agentCommi."', CommType = '".$commType."' where transID = '".$transID."'");*/
				}
			
							
				if(CONFIG_REMOTE_LEDGER_AT_CREATE == '1' || $status == "Authorize")
				{
				$transactionDescription = "Transaction Created";
				include("createOtherNetworkTrans.php");		
				dbConnect();
				
				}
				
				
			}
			
	}
	if((CONFIG_AGENT_OWN_RATE == '1' || (CONFIG_AGENT_OWN_MANUAL_RATE == '1' && $_POST["checkManualRate"] != '') || (CONFIG_AGENT_RATE_MARGIN == '1')) && $actualDiff > 0)
	{
			IF(CONFIG_DUAL_ENTRY_IN_LEDGER == "1"){
			
					update("update transactions set AgentComm = '".$calculatedComm."' where transID = '".$transID."'");
			}			
	}
	if(CONFIG_DOUBLE_ENTRY == '1')
	{
		 $param = array(
			 "userID"=>$_POST["custAgentID"],
			 "transID"=>$transID,
			 "amount"=> $_POST["localAmount"],
			 "currencyTo"=> $_POST["currencyTo"],
			 "currencyFrom"=> $_POST["currencyFrom"],
			 "commission"=>  $doubleEntryAgentComm,
			 "description"=>"Transaction Created",
			 "created"=>$tran_date,
			 "createdBy"=>$changedBy,
			 "action"=>'CREATE_TRANSACTION'
		 );
		  updateLedger($param);
		/*At create transaction one entry of agent commission go in agent ledger and second entry of trans amount go in agent ledger*/  
	 $q = updateAgentAccount($agentContents["userID"], $doubleEntryAgentComm, $transID, "WITHDRAW", "Commission", "Agent",$note,$currecyToLedger,$tran_date);	  
 }		
	//debug($param,true);				
			$_SESSION["transType"] 		= "";
			$_SESSION["benAgentID"] 	= "";
			$_SESSION["customerID"] 	= "";
			$_SESSION["benID"] 			= "";
			$_SESSION["moneyPaid"] 		= "";
			$_SESSION["transactionPurpose"] = "";
			$_SESSION["other_pur"] = "";
			$_SESSION["fundSources"] 	= "";
			
			//if (CONFIG_MANUAL_CODE_WITH_RANGE != "1") {
				//$_SESSION["refNumber"] 		= "";
			//}Commented By Kashif To see if It makes any  sense to do this...
	
			$_SESSION["transAmount"] 	= "";
			$_SESSION["exchangeRate"] 	= "";
			$_SESSION["exchangeID"] 	= "";
			$_SESSION["localAmount"] 	= "";
			$_SESSION["totalAmount"] 	= "";
			$_SESSION["IMFee"] 			= "";
	
			// resetting Session vars for trans_type Bank Transfer
			$_SESSION["bankName"] 		= "";
			$_SESSION["branchCode"] 	= "";
			$_SESSION["branchName"] 	= "";
			$_SESSION["branchAddress"] 	= "";
			$_SESSION["swiftCode"] 		= "";
			$_SESSION["accNo"] 			= "";
			$_SESSION["ABACPF"] 		= "";
			$_SESSION["IBAN"] 			= "";
			$_SESSION["ibanRemarks"] 			= "";
			$_SESSION["accountType"] = "";
			
			
			$_SESSION["question"] 			= "";
			$_SESSION["answer"]     = "";	
			$_SESSION["tip"]     = "";	
			$_SESSION["branchName"]     = "";	
			$_SESSION["currencyCharge"]="";	
			$_SESSION["transDate"] = "";
			$_SESSION["internalRemarks"] = "";
			$_SESSION["documentProvided"] ="";	
			$_SESSION["documentChanged"]= "";
			$_SESSION["benIdPassword"] = "";
			$_SESSION["checkManualRate"] = "";
			$_SESSION["clientRef"] = "";
			$_SESSION["bankingType"] = "";
			$_SESSION["senderBank"] = "";
						
			//Changed By Kashi 11_Nov
			
// this condition is added for Batch transaction, By Imran
// now changing by Jamshed

			if ($_SESSION["amount_left"]!= "")
			{
				
			//$_SESSION["amount_left"] = $_SESSION["amount_left"] - $_POST["transAmount"];
			$_SESSION["transDate"] = $_POST["dDate"];
			}else{
				//$_SESSION["amount_left"] = $_SESSION["amount_transactions"] - $_POST["transAmount"];
				$_SESSION["transDate"] = $_POST["dDate"];
			}

// end 

		/* The pending transactions will be sent to the transHub, for delivery to eTransact */
		if(CONFIG_MIDDLE_TIER_CLIENT_ID == "3" && defined("LOCAL_PATH"))
			sendApiTransaction($transID);

	insertError("Transaction has been added successfully for processing and ".$systemPre." ".SYSTEM_CODE." is $imReferenceNumber");
			if(CONFIG_CUSTOM_RECIEPT == "1")
			{
				$backUrl = CONFIG_RECIEPT_PRINT."?msg=Y&transSend=$transSend&r=".$_GET["r"]."&customerID=".$_GET["customerID"];
				if(strtoupper($_REQUEST["transType"])=="BANK TRANSFER" && defined("CONFIG_RECIEPT_PRINT_BANK") && CONFIG_RECIEPT_PRINT_BANK!="0"){
//					Asad
					$backUrl = CONFIG_RECIEPT_PRINT_BANK."?msg=Y&transSend=$transSend&r=".$_GET["r"]."&customerID=".$_GET["customerID"];
				}
			}
			else
			{
				$backUrl = "print-confirmed-transaction.php?msg=Y&transSend=$transSend&r=".$_GET["r"]."&customerID=".$_GET["customerID"];
				// Testing purpose changed
				
				//$backUrl = "print-confirmed-transaction-k2k.php?msg=Y&transSend=$transSend&r=".$_GET["r"];	
			}
			 $backUrl .= "&success=Y&transID=$transID";

				 foreach ($_POST as $k => $v) 
			{
				if ($k != "refNumber") {
					$_SESSION["$k"] = $v;
				}
			}
			//debug($backUrl, true);
 			redirect($backUrl);
		}
	}///end of transID not equal to null
	
	
	else
	{
	
	   
		if($_SESSION["act"]!="")
		{
		$status = $_SESSION["act"];
		$_SESSION["act"]="";
		}else{
			$status = "Pending";
		}
			
		//	 echo("[".$_SESSION["act"]."]");
// Distributor Commission Logic		

	
			
			
			
			
			$packageQuery="select * from ". TBL_ADMIN_USERS." where userID = '".$_POST["custAgentID"]."'";
			$agentPackage = selectFrom($packageQuery);
		
		
			$agentParentID = $agentPackage["parentID"];
			
		$package = $agentPackage["commPackage"];
			if(CONFIG_AnD_ENABLE == '1' && $agentPackage["isCorrespondent"] == 'Y')
			{
				$isAgentAnD = 'Y';
			}
			//
			switch ($package)
				  {				  	
				  	case "001": // Fixed amount per transaction
					{
					
						$agentCommi = $agentPackage["agentCommission"];
						$commType = "Fixed Amount";
						break;
					}
				  	case "002": // percentage of total transaction amount
					{	
						
						$agentCommi = ($_POST["transAmount"]* $agentPackage["agentCommission"]) / 100;
						$commType = "Percentage of transaction Amount";
						break;
					}
				  	case "003": // percentage of IMFEE
					{
						
						$agentCommi = ($_POST["IMFee"] * $agentPackage["agentCommission"]) / 100;
						$commType = "Percentage of Fee";
						break;
					}
					case "004":
					{
					
						$agentCommi = 0;
						$commType = "None";
						break;
					}
				}				  
				   
			//
		 $agentCommi;
		
		
		
		
		$packageQueryDistr="select commPackage, agentCommission, commPackageAnDDist, commAnDDist, parentID from ". TBL_ADMIN_USERS." where userID = '".$benAgentIDs."'";
			$distrPackage = selectFrom($packageQueryDistr);
			
			$distrParentID = $distrPackage["parentID"];
			
			$packagedistr = $distrPackage["commPackage"];
			if(CONFIG_AnD_ENABLE == '1' && $distrPackage["isCorrespondent"] == 'Y')
			{
				$isBankAnD = 'Y';
				
				$packagedistr = $distrPackage["commPackageAnDDist"];
				
				switch ($packagedistr)
				  {
				  	case "001": // Fixed amount per transaction
					{
						$distrCommi = $distrPackage["commAnDDist"];
						$commTypeDist = "Fixed Amount";
						break;
					}
				  	case "002": // percentage of total transaction amount
					{
						$distrCommi = ($_POST["totalAmount"]* $distrPackage["commAnDDist"]) / 100;
						$commTypeDist = "Percentage of total Amount";
						break;
					}
				  	case "003": // percentage of IMFEE
					{
						$distrCommi = ($_POST["IMFee"] * $distrPackage["commAnDDist"]) / 100;
						$commTypeDist = "Percentage of Fee";
						break;
					}
					case "004":
					{
						$distrCommi = 0;
						$commTypeDist = "None";
						break;
					}
				}	
				
			}else{
			//
			switch ($packagedistr)
				  {
				  	case "001": // Fixed amount per transaction
					{
						$distrCommi = $distrPackage["agentCommission"];
						$commTypeDist = "Fixed Amount";
						break;
					}
				  	case "002": // percentage of transaction amount
					{
						$distrCommi = ($_POST["transAmount"]* $distrPackage["agentCommission"]) / 100;
						$commTypeDist = "Percentage of transaction Amount";
						break;
					}
				  	case "003": // percentage of IMFEE
					{
						$distrCommi = ($_POST["IMFee"] * $distrPackage["agentCommission"]) / 100;
						$commTypeDist = "Percentage of Fee";
						break;
					}
					case "004":
					{
						$distrCommi = 0;
						$commTypeDist = "None";
						break;
					}
				}	
			}			  
				   
			//
		 $distrCommi ;
		 ////
if(CONFIG_ZERO_FEE != '1')
{
	if(trim($_POST["IMFee"]) < ($agentCommi + $distrCommi))
	{
		$comm = $distrCommi + $agentCommi;
		insertError("Agent and Distributor's Commission calculated $comm, may be greater then transaction fee.");	
		redirect($backUrl);
	}
}

		
	// End
	
	
if(CONFIG_EXCLUDE_COMMISSION == '1')
{
	$agentLedgerAmount = $_POST["totalAmount"] - $agentCommi;
	
}elseif( CONFIG_AGENT_PAYMENT_MODE == "1"){
	
											$agentPaymentModeQuery = selectFrom("select paymentMode  from ". TBL_ADMIN_USERS." where userID = '".$_POST["custAgentID"]."' ");
											$agentPaymentMode = $agentPaymentModeQuery["paymentMode"];
											
												if($agentPaymentMode == 'exclude')
												{
														$agentLedgerAmount = $_POST["totalAmount"] - $agentCommi;
												
												}elseif($agentPaymentMode == 'include')
												{
														$agentLedgerAmount = $_POST["totalAmount"];
													
												}
	
}else{
	$agentLedgerAmount = $_POST["totalAmount"];
}

/*****************Checking condition for shared transaction agent and Distributor************/	
			if(CONFIG_SHARE_OTHER_NETWORK == '1' )
			{
				
					$benAgentInfo = selectFrom("select fromServer from " . TBL_ADMIN_USERS . " where userID = '".$benAgentIDs."'");
					if($benAgentInfo["fromServer"] != '')
					{
						$jointClientComm = selectFrom("select * from ".TBL_JOINTCLIENT." where clientName = '".$benAgentInfo["fromServer"]."' and isEnabled = 'Y'");
													
							$checkSharedAgent = selectfrom("select id, availableAt, availableAs from ".TBL_SHAREDUSERS." where availableAt = '".$jointClientComm["clientId"]."' and localServerId  = '".$_POST["custAgentID"]."' and availableAs like 'Agent'");
							//echo("select id, availableAt, availableAs from ".TBL_SHAREDUSERS." where availableAt = '".$jointClientComm["clientId"]."' and localServerId  = '".$_POST["custAgentID"]."' and availableAs like 'Agent'");													
							if($checkSharedAgent["id"] == '')
							{
								$errMsg = "Agent of Transaction is not available at ".$jointClientComm["clientName"].". Please contact Administration";
								insertError($errMsg);
								redirect($backUrl);
								
							}
					}
				}
/***********************************Shared transactions***********************************/
		


	///To Save the Transaction Before Being changed
	$intResultSet =	mysql_query("select * from ".TBL_TRANSACTIONS." where transID = '".$_POST["transID"]."'");
	$oldTrans = mysql_fetch_array($intResultSet, MYSQL_ASSOC);
	
	//$oldTrans = selectFrom("select * from ".TBL_TRANSACTIONS." where transID = '".$_POST["transID"]."'");

	$strAmmendedHistroy = addslashes($_SESSION["loggedUserData"]["userID"]."|".time()."|".$_REQUEST["modifyReason"]);
	
 $sql = "INSERT INTO ".TBL_AMENDED_TRANSACTIONS." (transID, customerID, benID, benAgentID, 
											custAgentID,  exchangeID, refNumber, transAmount, 
											exchangeRate, localAmount, IMFee, totalAmount,
											transactionPurpose, other_pur, fundSources, moneyPaid, 
											Declaration, addedBy, transDate, transType, 
											toCountry, fromCountry, refNumberIM, 
											currencyFrom, currencyTo, custAgentParentID, 
											benAgentParentID, collectionPointID, bankCharges,
											AgentComm,CommType,admincharges, cashCharges,
											question, answer,tip, outCurrCharges, 
											discountRequest, transStatus, verifiedBy,
											authorisedBy, cancelledBy, recalledBy, isSent,
											remarks, authoriseDate, deliveryOutDate, 
											deliveryDate,	cancelDate, rejectDate,
											failedDate, holdedBy,
											discountType, discounted_amount,
											modifiedBy, modificationDate, holdDate,
											verificationDate, unholdBy, unholdDate,distCommPackage,
											agentExchangeRate,chequeNo, history)
								VALUES   ('".$oldTrans["transID"]."', '".$oldTrans["customerID"]."', '".$oldTrans["benID"]."', '".$oldTrans["benAgentID"]."', 
								'".$oldTrans["custAgentID"]."', '".$oldTrans["exchangeID"]."', '".$oldTrans["refNumber"]."', '".$oldTrans["transAmount"]."', 
								'".$oldTrans["exchangeRate"]."', '".$oldTrans["localAmount"]."', '".$oldTrans["IMFee"]."', '".$oldTrans["totalAmount"]."', 
								'".$oldTrans["transactionPurpose"]."','".$oldTrans["other_pur"]."', '".$oldTrans["fundSources"]."', '".$oldTrans["moneyPaid"]."', 
								'Y', '".$oldTrans["addedBy"]."', '".$oldTrans["transDate"]."', '".$oldTrans["transType"]."',
								'".$oldTrans["toCountry"]."',	'".$oldTrans["fromCountry"]."', '".$oldTrans["refNumberIM"]."',
								'".$oldTrans["currencyFrom"]."', '".$oldTrans["currencyTo"]."',	'".$oldTrans["custAgentParentID"]."', 
								'".$oldTrans["benAgentParentID"]."', '".$oldTrans["collectionPointID"]."','".$oldTrans["bankCharges"]."',
								'".$oldTrans["AgentComm"]."','".$oldTrans["CommType"]."','".$oldTrans["admincharges"]."', '".$oldTrans["cashCharges"]."',
								'".$oldTrans["question"]."','".$oldTrans["answer"]."','".$oldTrans["tip"]."','".$oldTrans["outCurrCharges"]."',
								'".$oldTrans["discountRequest"]."','".$oldTrans["transStatus"]."', '".$oldTrans["verifiedBy"]."',
								'".$oldTrans["authorisedBy"]."', '".$oldTrans["cancelledBy"]."', '".$oldTrans["recalledBy"]."','".$oldTrans["isSent"]."',
								'".$oldTrans["remarks"]."','".$oldTrans["authoriseDate"]."','".$oldTrans["deliveryOutDate"]."',
								'".$oldTrans["deliveryDate"]."','".$oldTrans["cancelDate"]."','".$oldTrans["rejectDate"]."',
								'".$oldTrans["failedDate"]."','".$oldTrans["holdedBy"]."',
								'".$oldTrans["discountType"]."','".$oldTrans["discounted_amount"]."',
								'$username', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$oldTrans["holdDate"]."',
								'".$oldTrans["verificationDate"]."','".$oldTrans["unholdBy"]."','".$oldTrans["unholdDate"]."','".$oldTrans["distCommPackage"]."',
								'".$oldTrans["agentExchangeRate"]."','".$oldTrans["chequeNo"]."', '".$strAmmendedHistroy."')";
								
								insertInto($sql);

								
								
			/*
				by Aslam Shahid. (traced while doing #4804)
			*/
			$amendedLastTransID   = @mysql_insert_id(); 
			if($countryBasedFlag)
			{
				update("update ".TBL_AMENDED_TRANSACTIONS." set serviceType='".$_POST["serviceType"]."' where amendID=".$amendedLastTransID);
			}			
			
	      if ($_SESSION["transDate"] == "")
				{
					$_SESSION["transDate"] = getCountryTime(CONFIG_COUNTRY_CODE);
					$_SESSION["transDate2"] = "";
				}
				
	if(CONFIG_AGENT_OWN_RATE == '1' || (CONFIG_AGENT_OWN_MANUAL_RATE == '1' && $_POST["checkManualRate"] != '') || (CONFIG_AGENT_RATE_MARGIN == '1'))
  {
  	
  	include("agentOwnRate.php");
  	}	
  	
  	
  									if(CONFIG_DIST_REF_NUMBER == '1')
    								{
    									$generateRefNumber = true;
    									IF(CONFIG_DIST_MANUAL_REF_NUMBER == "1"){
    										
    												$generateRefNumber = false;
    														
    										}elseif($generateRefNumber){   									
    								
					    								$refNumRange = selectFrom("SELECT id,rangeFrom,rangeTo,prefix,used from ".TBL_RECEIPT_RANGE." where agentID ='".$benAgentIDs."' and rangeTo >= (rangeFrom + used) and  created =(SELECT min(created) from ".TBL_RECEIPT_RANGE." where agentID ='".$benAgentIDs."' and rangeTo >= (rangeFrom + used))");
					    								
																$limitVal = $refNumRange["rangeTo"] - ($refNumRange["rangeFrom"] + $refNumRange["used"]);
																if($refNumRange["id"] != ''){
																	
																	$value = $refNumRange["rangeFrom"] + $refNumRange["used"];
																	$refNum = $refNumRange["prefix"].$value;
																	$used = $refNumRange["used"];
																	$used = ++$used;
																	
																	update("update ".TBL_RECEIPT_RANGE." set used = '".$used."' where agentID ='".$benAgentIDs."' and id ='".$refNumRange["id"]."' ");
																	update("update ".TBL_TRANSACTIONS." set distRefNumber = '".$refNum."' where transID ='".$_POST["transID"]."'");
																	
																	
																	if($limitVal <= 10){
																		$cond = selectFrom(" SELECT id from ".TBL_RECEIPT_RANGE." where agentID ='".$benAgentIDs."' and created > (SELECT min(created) from ".TBL_RECEIPT_RANGE." where agentID ='".$benAgentIDs."' and rangeTo >= (rangeFrom + used)) ");
																		if($cond["id"] == ''){
																			
																			include ("distEmail.php");
																		}
																	}
																}else{
																	
																
																	$flag = false;
																	$errMsg = "Transaction cannot be authorized. Add a new distributor reference number range. ";
																		insertError($errMsg);	
																		redirect($backUrl);
																}
													}
      							}
  
	$statusUpdateQueryPortion = "";
  	if ( strtoupper($oldTrans["transStatus"]) == strtoupper("Authorize") )
		$statusUpdateQueryPortion = "transStatus = 'Amended', ";
	 
	 $queryUpdate = "update ". TBL_TRANSACTIONS." set 
						customerID	='".$_POST["customerID"]."', 
						benID		='".$_POST["benID"]."',
						custAgentID = '".$_POST["custAgentID"]."',
						benAgentID	='$benAgentIDs',
						exchangeID	='".$_POST["exchangeID"]."',
						refNumber 	='".$_POST["refNumber"]."',
						transAmount	='".$_POST["transAmount"]."', 
						exchangeRate='".$_POST["exchangeRate"]."', 
						localAmount	='".$_POST["localAmount"]."', 
						IMFee		='".$_POST["IMFee"]."',
						totalAmount	='".$_POST["totalAmount"]."', 
						transactionPurpose='".checkValues($_POST["transactionPurpose"])."',
						other_pur	='".$_POST["other_pur"]."',
						fundSources	='".checkValues($_POST["fundSources"])."',
						moneyPaid	='".$_POST["moneyPaid"]."', 
						transType	='".$_POST["transType"]."',
						toCountry= '".$_POST["benCountry"]."',
						fromCountry= '".$_POST["custCountry"]."',
						currencyFrom= '".$_POST["currencyFrom"]."', 
						currencyTo= '".$_POST["currencyTo"]."', 
						custAgentParentID= '".$agentParentID."', 
						benAgentParentID= '".$distrParentID."', "
						//transStatus = '$status',
						. $statusUpdateQueryPortion . "
						admincharges= '".$_POST["admincharges"]."',
						bankCharges= '".$_POST["bankCharges"]."',
						cashCharges = '$cashCharges',
						question = '".$_SESSION["question"]."',
						answer = '".$_SESSION["answer"]."',
						tip = '".$_SESSION["tip"]."',
						branchName = '".$_SESSION["branchName"]."',
						outCurrCharges = '".$_SESSION["currencyCharge"]."',
						distributorComm = '$distrCommi',
						discountRequest = '".$_SESSION["discount_request"]."',
						AgentComm = '".$agentCommi."',			
						creation_date_used = '".$_SESSION["transDate2"]."',
						collectionPointID = '".$_SESSION["collectionPoint"]."',
						distCommPackage = 	'".$commTypeDist."',
						internalRemarks = '".$_SESSION["internalRemarks"]."',
						benIDPassword = '".$_SESSION["benIdPassword"]."',
						agentExchangeRate = '".$_POST["checkManualRate"]."',
						payPointType = '".$_SESSION["payPoint"]."',
						bankingType = '".$_SESSION["bankingType"]."',
						chequeNo =    '".$_SESSION["chequeNo"]."',
						settlementDistributorCommisson = ".$distributorCommission.",
						chequeAmount = '" . $chequeAmt . "',
						IsReverse = '" . $reversvalue . "',
						senderBank = '".$_POST["senderBank"]."'
						$wavUpdate
						where transID='".$_POST["transID"]."'";
			//debug($queryUpdate,true);		
		update($queryUpdate);
	 	
		if(CONFIG_CHK_RULE_BANK_DETAILS == '1'){
		 
				$queryRuleBankDetails = "SELECT country.countryName, countryRuleBankDetails.* FROM countries AS country INNER JOIN ".TBL_BEN_BANK_DETAILS_RULE." AS countryRuleBankDetails on countryRuleBankDetails.benCountry = country.countryId WHERE country.countryName = '{$_POST['benCountry']}' AND countryRuleBankDetails.status = 'Enable' ORDER BY countryRuleBankDetails.updateDate DESC";
				debug($queryRuleBankDetails);
				$bankDetailsRule = selectFrom($queryRuleBankDetails);
				$_POST["useTransDetails"] = "Yes";
				//debug($bankDetailsRule);
				if($bankDetailsRule['accountName'] == 'Y' && empty($_POST['bankName']))
					$_POST["useTransDetails"] = "No";
				else
					$_POST["useTransDetails"] = "YES";
				
				if($bankDetailsRule['accNo'] == 'Y' && empty($_POST['accNo']))
					$_POST["useTransDetails"] = "No";
				else
					$_POST["useTransDetails"] = "YES";
				
				if($bankDetailsRule['branchNameNumber'] == 'Y' && empty($_POST['branchCode']))
					$_POST["useTransDetails"] = "No";
				else
					$_POST["useTransDetails"] = "YES";
				
				if($bankDetailsRule['branchAddress'] == 'Y' && empty($_POST['branchAddress']))
					$_POST["useTransDetails"] = "No";
				else
					$_POST["useTransDetails"] = "YES";
				
				if($bankDetailsRule['swiftCode'] == 'Y' && empty($_POST['swiftCode']))
					$_POST["useTransDetails"] = "No";
				else
					$_POST["useTransDetails"] = "YES";
					
				if($bankDetailsRule['routingNumber'] == 'Y' && empty($_POST['routingNumber']))
					$_POST["useTransDetails"] = "No";
				else
					$_POST["useTransDetails"] = "YES";
					
				
				
				if($bankDetailsRule['iban'] == 'Y' && empty($_POST['IBAN']))
					$_POST["useTransDetails"] = "No";
				else
					$_POST["useTransDetails"] = "YES";
			 
			}
		
		
		//exit('3103');
		
		sendCustomMail($_POST["transID"]);
		/*
			by Aslam Shahid. (traced while doing #4804)
		*/
		if($countryBasedFlag)
		{
			update("update ".TBL_TRANSACTIONS." set serviceType='".$_POST["serviceType"]."' where transID='".$_POST["transID"]."'");
		}
		
		/*if(CONFIG_DOUBLE_ENTRY == '1')
		{
		 $param = array(
			 "userID"=>$_POST["custAgentID"],
			 "transID"=>$_POST["transID"],
			 "amount"=> $_POST["localAmount"],
			 "currencyTo"=> $_POST["currencyTo"],
			 "currencyFrom"=> $_POST["currencyFrom"],
			 "commission"=> $_POST["IMFee"],
			 "description"=>"Transaction Amended",
			 "action"=>'AMENDED_TRANSACTION'
		 );
		  updateLedger($param);
     }	*/	
		/**
		 * Get which fields has been modify in this ammended transaction case
		 * 
		 * First get all the fields from amended table and than from transaction table
		 * compare each value from the first table with second one
		 * If any diffrence found than store it with the ammended report
		 * @Ticket #3337
		 */
		$strDiffrence = "";
		$arrNewTransactionFields = selectFrom("select * from ".TBL_TRANSACTIONS." where transID='".$_REQUEST["transID"]."'", 1);
		foreach($oldTrans as $otKey => $otVal)
			if($oldTrans[$otKey] != $arrNewTransactionFields[$otKey])
				$strDiffrence .= $otKey.":".$oldTrans[$otKey]."!".$arrNewTransactionFields[$otKey]."|";

		if(!empty($strDiffrence))
		{
			$strInsertChangedLogSql = "insert into 
										transaction_modify_history
									  (`transid`, `change_log`, `userid`) values
									  ('".$_REQUEST["transID"]."', '".addslashes($strDiffrence)."', '".$_SESSION["loggedUserData"]["userID"]."')
										";	 
			
			insertInto($strInsertChangedLogSql);
		}
			 
		if(CONFIG_CLIENT_REF == "1")
		{
				update("update ".TBL_TRANSACTIONS." set clientRef='".addslashes($_SESSION["clientRef"])."' where transID=".$_POST["transID"]);
		}
		
  // Added by Niaz Ahmad at 03-07-2009 @5052- GE		
	if(CONFIG_SENDER_DOCUMENT_AT_CREATE_TRANS == "1"){
	$strupdateSql = "update 
							".TBL_TRANSACTIONS." 
					 set 
					 		custDocumentProvided = '".serialize($_SESSION["docCategory"])."' 
					 where transID= '".$_REQUEST["transID"]."' 
					 ";
					 
	update($strupdateSql);	
	}	
		/**
		 * add the fax number field based on the custAgentId
		 * @Ticket #3638
		 */
		if(CONFIG_GE_ADD_FAX_NUMBER == 1)
		{
			
			/* If new and old customer are not same */
			$arrOldCustAgentIdData = selectFrom("Select benAgentID from ".TBL_TRANSACTIONS." where transID =".$_POST["transID"]);
			if($arrOldCustAgentIdData["benAgentID"] != $_SESSION["benAgentID"])
			{
				$strAgentLastFaxNumberSql = "Select Max(faxNumber) as mfn from ".TBL_TRANSACTIONS." where benAgentID=".$_SESSION["benAgentID"]." and transID !=".$_POST["transID"];
				$arrAgentLastFaxNumberSql = selectFrom($strAgentLastFaxNumberSql);		
				$intFaxNumber = $arrAgentLastFaxNumberSql["mfn"];
				$intFaxNumber++;
				
				update("update ".TBL_TRANSACTIONS." set faxNumber='".$intFaxNumber."' where transID=".$_POST["transID"]);
			}
		}
		/* End #3638*/
		
		if(CONFIG_CURRENCY_DENOMINATION_ON_CONFIRM == '1')
		{
				/**
				 *	Updating the currency dinomination
				 */
				
				$total = $_POST['field1'] * 1 + $_POST['field10'] * 10 + $_POST['field2'] * 2 + $_POST['field20'] * 20 + $_POST['field5'] * 5 + $_POST['field50'] * 50;
				
				$currencyNotesUpdateSql = "update ".TBL_CURRENCYNOTES." set notes1=".$_POST['field1'].", notes10=".$_POST['field10'].", notes2=".$_POST['field2'].", notes20=".$_POST['field20'].", notes5=".$_POST['field5'].", notes50=".$_POST['field50'].", total=".$total." where transId=".$_POST["transID"];

				update($currencyNotesUpdateSql);
		}
		
			/**
			 * In case of data entered at the confirmed trasaction page of Global Exchange Reciept page
			 * SO we can use it on the print confirmed transaction page
			 * and in case of edit trasaction
			 * @Ticket# 3547
			 */
			if(CONFIG_SAVE_RECIEVED_AMOUNT == 1)
			{
				update("update ".TBL_TRANSACTIONS." set recievedAmount='".$_POST["amountRecieved"]."' where transID=".$_POST["transID"]);
				//unset($_SESSION["confirmTransactionData"]);
				//$_SESSION["confirmTransactionData"] = $_POST;
			}
			/* End #3547 */

			if(!empty($_POST["useTransDetails"]) && CONFIG_USE_TRANSACTION_DETAILS=="1"){
				$qUpdateTrans = "update ". TBL_TRANSACTIONS." set 
									transDetails = '".$_POST["useTransDetails"]."'
									where transID='".$_POST["transID"]."'";	
				update($qUpdateTrans);
				//debug($qUpdateTrans);
			}
			/*if(!empty($_POST["value_date"]) && CONFIG_VALUE_DATE_TRANSACTIONS=="1"){
				$creationDate = $oldTrans["transDate"];
				$v_date_arr = explode(" ",$creationDate);
				$v_date = $v_date_arr[1]."-".$v_date_arr[2]."-".$v_date_arr[3];
				$validValueFlag = 'N';
				if(!empty($creationDate)){				
					list($dateV, $timeV) = explode(' ',$creationDate);
					if($v_date <= $dateV)
						$validValueFlag = 'Y';
				}
				$qUpdateTrans = "update ". TBL_TRANSACTIONS." set 
									valueDate = '".date("Y-m-d H:i:s",strtotime($v_date))."',
									validValue = '$validValueFlag' 
									where transID='".$_POST["transID"]."'";	
				update($qUpdateTrans);
				//debug($qUpdateTrans);
			}*/
			if($agentCurrCommFlag){
				$exchangeData = getMultipleExchangeRates($_POST["custCountry"],$_POST["benCountry"],$agentPackage["agentCurrency"], 0, $agentCommi , $dDate, $_POST["currencyFrom"],$_POST["transType"],$_POST["moneyPaid"], $_POST["customerID"]);
				$exAgentID		= $exchangeData[0];
				$exAgentRate	= $exchangeData[1];	
				$exAgentValue	= $agentCommi * $exAgentRate;
				//debug($agentCommi);
				if(CONFIG_ROUND_NUMBER_ENABLED == '1')
					$exAgentValue = round($exAgentValue,$roundLevel); 	
				//debug($exchangeData);
				//debug($exAgentValue);
			}
			if(CONFIG_USE_EXTENDED_TRANSACTION == "1")
			{
				
				
				
				$contentExtendedTrans = selectFrom("select id,transID from ".TBL_TRANSACTION_EXTENDED."  where transID = '".$_POST["transID"]."'");
				
				if($contentExtendedTrans["id"] != "")
				{	
					if(CONFIG_ENABLE_EX_RATE_LIMIT  == "1")
					{
						if($_POST["exRateLimitMin"] != '' || $_POST["exRateLimitMax"] != '')
						{
							$updateRateLimit = " , minRateAlert = '".$_POST["exRateLimitMin"]."', maxRateAlert = '".$_POST["exRateLimitMax"]."', isAlertProcessed = 'N'";
						}
					}
					if($agentCurrCommFlag){
						$updateRateLimit .=",agentCurrencyCommission= '".$exAgentValue."',agentCurrency= '".$agentPackage["agentCurrency"]."' ";
					}
					$TransDataQuery = "update ".TBL_TRANSACTION_EXTENDED." set
					agentID = '".$_POST["custAgentID"]."'
					".$updateRateLimit."
					where id = '".$contentExtendedTrans["id"]."'";
					update($TransDataQuery);
				}else{
					if(CONFIG_ENABLE_EX_RATE_LIMIT  == "1")
					{
						if($_POST["exRateLimitMin"] != '' || $_POST["exRateLimitMax"] != '')
						{
							$insertRateLimitField = " , minRateAlert, maxRateAlert, isAlertProcessed";
							$insertRateLimitValue = " , '".$_POST["exRateLimitMin"]."', '".$_POST["exRateLimitMax"]."', 'N'";
						}
					}
					if($agentCurrCommFlag){
						$insertRateLimitField .= " , agentCurrencyCommission, agentCurrency";
						$insertRateLimitValue .= " , '".$exAgentValue."', '".$agentPackage["agentCurrency"]."'";
					}								
					$TransDataQuery = "insert into ".TBL_TRANSACTION_EXTENDED." 
					(transID, agentID ".$insertRateLimitField.")VALUES
					('".$_POST["transID"]."', '".$_POST["custAgentID"]."' ".$insertRateLimitValue.")";
					$TransData = insertInto($TransDataQuery);
					
					}
			}
		
	
	$senderAccumulativeAmount = $senderAccumulativeAmount -	$oldTrans["transAmount"];
$senderAccumulativeAmount += $_POST["transAmount"];
		
		$bundledAmount= "update ".TBL_CUSTOMER." set accumulativeAmount = '".$senderAccumulativeAmount."' where customerID = '".$_POST["customerID"]."'";
		update($bundledAmount);
		
		
		if($_POST["document_remarks"] != ""){
			$updateSender = "update ".TBL_CUSTOMER." set remarks ='".$_POST["document_remarks"]."' where customerID = '".$_POST["customerID"]."'";	
			update($updateSender);
		}
		
		/* $descript =" Transaction  is updated ";
			activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["transID"],TBL_TRANSACTIONS,$descript); */		 
		
		
		
		if(CONFIG_AGENT_OWN_RATE == '1' || (CONFIG_AGENT_OWN_MANUAL_RATE == '1' && $_POST["checkManualRate"] != '')  || (CONFIG_AGENT_RATE_MARGIN == '1'))
			{
				if($oldAmount > 0)
				{
					
								
					$preAgentInfo = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$oldTrans["custAgentID"]."'");
					if($preAgentInfo["agentType"]=="Sub")
					{
						$agent=$preAgentInfo["parentID"];							
						$q = updateAgentAccount($agent, $oldAmount, $_POST["transID"], $oldType, "Commission Margin", "Agent",$note,$currencyFrom);
							 updateSubAgentAccount($preAgentInfo["userID"], $oldAmount, $_POST["transID"], $oldType, "Commission Margin", "Agent",$note,$currencyFrom);
					}else{
						$agent=$preAgentInfo["userID"];
						$q = updateAgentAccount($agent, $oldAmount, $_POST["transID"], $oldType, "Commission Margin", "Agent",$note,$currencyFrom);
					}
				}
				if($actualDiff > 0)
				{
					
					
				//echo "FIRSTtYPE".	$firstType = $diffType;
				$firstLabel = "Commission Margin";
				if(CONFIG_DUAL_ENTRY_IN_LEDGER == "1"){
						if($diffType == "WITHDRAW")
						{
						$firstType = 'WITHDRAW';
						$firstLable = "Agent Adjustment Lost";
						
						$secondtype = 'DEPOSIT';
						$secondLabel	= COMPANY_NAME." Adjustment Gain";
					}elseif($diffType == "DEPOSIT"){
						
						$firstType = 'DEPOSIT';
						$firstLable = 'Agent Adjustment Gain';
						
						$secondtype = 'WITHDRAW';
						$secondLabel	= 'Agent Adjustment Lost';
						}
			}
			
			
								if(CONFIG_NEGETIVE_ENTRY_IN_LEDGER == "1" && $diffType == "DEPOSIT"){
						
							
							/*
										$agentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$_POST["custAgentID"]."'");
										if($agentContents["agentType"] == 'Sub')
										{
											updateSubAgentAccount($agentContents["userID"], $actualDiff, $_POST["transID"], $secondtype, $firstLable, "Agent",$currencyFrom,$tran_date2);
									  $q = updateAgentAccount($agentContents["parentID"], $actualDiff, $_POST["transID"], $secondtype, $firstLable, "Agent",$note,$currencyFrom);
										}else{
											$q = updateAgentAccount($agentContents["userID"], $actualDiff, $_POST["transID"], $secondtype, $firstLable, "Agent",$note,$currencyFrom);
										}
							
							*/
							
					
					
					}else{
										$agentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$_POST["custAgentID"]."'");
										if($agentContents["agentType"] == 'Sub')
										{
											updateSubAgentAccount($agentContents["userID"], $actualDiff, $_POST["transID"], $firstType, $firstLable, "Agent",$currencyFrom,$tran_date2);
									  $q = updateAgentAccount($agentContents["parentID"], $actualDiff, $_POST["transID"], $firstType, $firstLable, "Agent",$note,$currencyFrom);
										}else{
											$q = updateAgentAccount($agentContents["userID"], $actualDiff, $_POST["transID"], $firstType, $firstLable, "Agent",$note,$currencyFrom);
										}
										
			}
					
				IF(CONFIG_DUAL_ENTRY_IN_LEDGER == "1"){
					
				if($firstType == 'WITHDRAW'){
					
											if($agentContents["agentType"] == 'Sub')
										{
											updateSubAgentAccount($agentContents["userID"], $actualDiff, $_POST["transID"], $secondtype, $secondLabel, "Agent",$currencyFrom,$tran_date2);
									  $q = updateAgentAccount($agentContents["parentID"], $actualDiff, $_POST["transID"], $secondtype, $secondLabel, "Agent",$note,$currencyFrom);
										}else{
											$q = updateAgentAccount($agentContents["userID"], $actualDiff, $_POST["transID"], $secondtype, $secondLabel, "Agent",$note,$currencyFrom);
										}
			
				 
					$calculatedComm = $agentCommi - $actualDiff ;
					
				}else if($firstType == 'DEPOSIT')
				{
						$calculatedComm = $agentCommi;
				}
					//echo "calculatedCommisssssion".$calculatedComm;
					
					//update("update transactions set AgentComm = $calculatedComm where transID = $transID");
														
					}
				
				
				}
			}
		
		$payin = selectFrom("select payinBook, customerID from " . TBL_CUSTOMER . " where customerID = '". $_POST["customerID"]."'");
		//////For Payin Book Customer
		if($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == '1')
		{
		
			$checkDiff = selectFrom("select amount, customerID from agents_customer_account where tranRefNo = '".$_POST["transID"]."'");
			/**
			 * Store the outstanding amount of the transaction
			 * @Ticket# 3733
			 */
			if(CONFIG_SAVE_RECIEVED_AMOUNT == 1)
			{
				$difference = $_POST["totalAmount"] - $_POST["amountRecieved"];
			}
			else
			{
				$difference = $checkDiff["amount"] - $_POST["totalAmount"];
			}
			
			if($difference != 0)
			{
				////Removing Old
				$checkBalance = selectFrom("select balance from ".TBL_CUSTOMER." where customerID = '".$checkDiff["customerID"]."'");						
				$newBalance = $checkBalance["balance"] + $checkDiff["amount"] ; 
				$update_Balance = "update ".TBL_CUSTOMER." set balance = '".$newBalance."' where customerID= '". $checkDiff["customerID"] ."'";
				update($update_Balance);
				
				////Updating New 
				$checkBalance = selectFrom("select balance from ".TBL_CUSTOMER." where customerID = '".$_POST["customerID"]."'");						
				if(CONFIG_SAVE_RECIEVED_AMOUNT == 1)
					$newBalance = $checkBalance["balance"] - $difference;
				else
					$newBalance = $checkBalance["balance"] - $_POST["totalAmount"] ; 
				
				$update_Balance = "update ".TBL_CUSTOMER." set balance = '".$newBalance."' where customerID= '". $_POST["customerID"] ."'";
				update($update_Balance);
			}
			/* #5003- This will udpate customer ledger account on Edt/Ament transaction.
				Analysis made by Aslam Shahid
			*/
			if(CONFIG_SAVE_RECIEVED_AMOUNT == 1)
			{
				$account="update 
							agents_customer_account 
						 set 
							amount='".$difference."',
							payment_mode = 'Transaction Amended'  
						 where 
							tranRefNo='".$_POST["transID"]."'";
				
				$account="update 
							".TBL_AGENT_COMPANY_ACCOUNT." 
						 set 
							amount='".$difference."',
							payment_mode = 'Transaction Amended'  
						 where 
							tranRefNo='".$_POST["transID"]."'";
							
			}
			else
			{
				$account="update 
							agents_customer_account 
						 set 
							amount='".$_POST["totalAmount"]."',
							payment_mode = 'Transaction Amended'  
						 where 
							tranRefNo='".$_POST["transID"]."'";
							
							$account="update 
							".TBL_AGENT_COMPANY_ACCOUNT."  
						 set 
							amount='".$_POST["totalAmount"]."',
							payment_mode = 'Transaction Amended'  
						 where 
							tranRefNo='".$_POST["transID"]."'";
							
			}
			update($account);
			
				
				if(CONFIG_PAYIN_CUST_AGENT == '1')
				{
					
					if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == '1' && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD != "1")
					{
						$amountToLedger = $_POST["localAmount"];	
						$currecyToLedger = $_POST["currencyTo"];
					}else{
						$amountToLedger = $_POST["totalAmount"];
						$currecyToLedger = $_POST["currencyFrom"];
						}	
						
					$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".CONFIG_PAYIN_AGENT_NUMBER."'";
					if(CONFIG_PAYIN_CUST_AGENT_ALL=="1" && isset($_POST["custAgentID"])){
						$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$_POST["custAgentID"]."'";
					}
					$agentPayinContents = selectFrom($agentPayinQuery);
					if($agentPayinContents["agentType"] == 'Sub')
					{
						updateSubAgentAccount($agentPayinContents["userID"], $checkDiff["amount"], $_POST["transID"], 'DEPOSIT', "Transaction Amended", "Agent",$currencyFrom);
						$q = updateAgentAccount($agentPayinContents["parentID"], $checkDiff["amount"], $_POST["transID"], 'DEPOSIT', "Transaction Amended", "Agent",$note,$currencyFrom);
						
						updateSubAgentAccount($agentPayinContents["userID"], $_POST["totalAmount"], $_POST["transID"], 'WITHDRAW', "Transaction Amended", "Agent",$currencyFrom);
						$q = updateAgentAccount($agentPayinContents["parentID"], $amountToLedger, $_POST["transID"], 'WITHDRAW', "Transaction Amended", "Agent",$note,$currecyToLedger);
						
					}else{
						
						$q = updateAgentAccount($agentPayinContents["userID"], $checkDiff["amount"], $_POST["transID"], 'DEPOSIT', "Transaction Amended", "Agent",$note,$currencyFrom);
						
						$q = updateAgentAccount($agentPayinContents["userID"], $amountToLedger, $_POST["transID"], 'WITHDRAW', "Transaction Amended", "Agent",$note,$currecyToLedger);
					}	
				}
		}
		//////////////Payin Book Ended
		
		
	if(CONFIG_LEDGER_AT_CREATION == "1" || ($oldTrans["transStatus"] != "Processing" && $oldTrans["transStatus"] != "Pending"))	
	{
		if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != '1')
		{ 
	/////////Either to insert into Agent or A&D Account////////
			if($isAgentAnD == 'Y')
			{
				$checkDiff = selectFrom("select amount, agentID from ".TBL_AnD_ACCOUNT." where TransID = '".$_POST["transID"]."'");
			}else{
				$checkDiff = selectFrom("select amount, agentID from agent_account where TransID = '".$_POST["transID"]."'");
			}
	
			/////////Either to insert into Agent or A&D Account////////
			//////Undo Previous Entries////////
			$preAgentInfo = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$oldTrans["custAgentID"]."'");
			/***************************************************************
			 * Code commented in view of bug found @Ticket #3741
			 * The below code does make an empty entry in the agent ledger, without any required fields
			 * The else part of this config has the required functionality so the if part is commented
			if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == "1" && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD != "1"){
				
							$oldLedgerAmount = $oldTrans["localAmount"];
							$currencyFrom = $oldTrans["currencyTo"];
				
				$getOldData = selectFrom("select * from agent_account where aaID = (select max(aaID) from agent_account where transID = '".$_POST["transID"]."' and type = 'WITHDRAW' and (description = 'Transaction Created' or description = 'Transaction Amended'))");
				
				$insertOldData = "insert into ".TBL_AGENT_ACCOUNT." 
			(agentID,  type, amount, modified_by, TransID, description,note,settleAmount,SID,currency) values
		('".$getOldData['agentID']."', 'DEPOSIT', '".$getOldData['amount']."', '".$getOldData['modified_by']."', '". $getOldData['TransID']."', '". $getOldData['description']."','". $getOldData['note']."','".$getOldData['settleAmount']."','".$getOldData['SID']."','".$getOldData['currency']."')";	
				
				$q1 = insertInto($insertOldData);
				
				agentSummaryAccount($getOldData['agentID'], 'DEPOSIT', $getOldData['amount'],$getOldData['currency'],$getOldData['settleAmount']); 
				
				}else{ 
			*
			****************************************/
					if(CONFIG_EXCLUDE_COMMISSION == '1')
					{
						$oldLedgerAmount = $oldTrans["totalAmount"] - $oldTrans["AgentComm"];
						
					}elseif( CONFIG_AGENT_PAYMENT_MODE == "1"){
						
								$agentPaymentModeQuery = selectFrom("select paymentMode  from ". TBL_ADMIN_USERS." where userID = '".$_POST["custAgentID"]."' ");
								$agentPaymentMode = $agentPaymentModeQuery["paymentMode"];
								
								if($agentPaymentMode == 'exclude'){
									
												$oldLedgerAmount = $oldTrans["totalAmount"] - $oldTrans["AgentComm"];
												$currencyFrom = $oldTrans["currencyFrom"];
												
								}elseif($agentPaymentMode == 'include'){
									
												$oldLedgerAmount = $oldTrans["totalAmount"];
												$currencyFrom = $oldTrans["currencyFrom"];
										
								}
						
					}else{
						$oldLedgerAmount = $oldTrans["totalAmount"];
						$currencyFrom = $oldTrans["currencyFrom"];
					}
					
					if($preAgentInfo["agentType"]=="Sub")
					{
						$agent=$preAgentInfo["parentID"];							
						$q = updateAgentAccount($agent, $oldLedgerAmount, $_POST["transID"], "DEPOSIT", "Transaction Amended", "Agent",$note,$currencyFrom);
							 updateSubAgentAccount($preAgentInfo["userID"], $oldLedgerAmount, $_POST["transID"], "DEPOSIT", "Transaction Amended", "Agent",$currencyFrom);
					}else{
						$agent=$preAgentInfo["userID"];
						$q = updateAgentAccount($agent, $oldLedgerAmount, $_POST["transID"], "DEPOSIT", "Transaction Amended", "Agent",$note,$currencyFrom);
					}
					if(CONFIG_CREATE_ADMIN_STAFF_LEDGERS == "1")
						updateAdminStaffLedger($_POST["transID"], "DEPOSIT", "Transaction Amended", $note, $currencyFrom);
		/*********** } ******/
			
			///////////Do New Entries///////
			$currAgentInfo = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$_POST["custAgentID"]."'");
			
			if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == '1' && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD != "1")
			{
				/* Changing localAmount to totalAmount as bug found in Ticket #3829*/
				$amountToLedger = $_POST["totalAmount"];	
				$currecyToLedger = $_POST["currencyTo"];
			}else{
				$amountToLedger = $agentLedgerAmount;
				$currecyToLedger = $_POST["currencyFrom"];
				}	
			
			
			if($currAgentInfo["agentType"]=="Sub")
			{
				$agent=$currAgentInfo["parentID"];
				$q = updateAgentAccount($agent, $agentLedgerAmount, $_POST["transID"], "WITHDRAW", "Transaction Amended", "Agent",$note,$currecyToLedger);
					 updateSubAgentAccount($currAgentInfo["userID"], $agentLedgerAmount, $_POST["transID"], "WITHDRAW", "Transaction Amended", "Agent",$currecyToLedger);
			}else{
				$agent=$currAgentInfo["userID"];
				$q = updateAgentAccount($agent, $amountToLedger, $_POST["transID"], "WITHDRAW", "Transaction Amended", "Agent",$note,$currecyToLedger);
			}
			if(CONFIG_CREATE_ADMIN_STAFF_LEDGERS == "1")
				updateAdminStaffLedger($_POST["transID"], "WITHDRAW", "Transaction Amended", $note, $currecyToLedger);
			/////////////////////////////
			
			
			
		
		}
	}
		if(CONFIG_POST_PAID != '1')
		{
			$checkDiff = selectFrom("select aaID, amount, bankID from bank_account where TransID = '".$_POST["transID"]."'");
			$isBank = "Y";
			if($checkDiff["aaID"] == "")
			{	
				$isBank = "N";
				$checkDiff = selectFrom("select aaID, amount, agentID from ".TBL_AnD_ACCOUNT." where TransID = '".$_POST["transID"]."' and actAs = 'Distributor'");
			}
			
			
			if($checkDiff["aaID"] != "")
			{
				//////Undo Previous Entries////////
			$preDistInfo = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$oldTrans["benAgentID"]."'");
						
			if(DEST_CURR_IN_ACC_STMNTS == "1" || CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == "1")
			{
				$amountToLedger = $oldTrans["localAmount"];	
				$currecyToLedger = $oldTrans["currencyTo"];
			}else{
				$amountToLedger = $oldTrans["transAmount"];	
				$currecyToLedger = $oldTrans["currencyFrom"];
				}	
				
			 //$amountToLedgerDist =  $oldTrans["localAmount"];	
			 //$currecyToLedgerDist = $oldTrans["currencyTo"];
			 
			if($preDistInfo["agentType"]=="Sub")
			{
				$Distributor=$preDistInfo["parentID"];							
				$q = updateAgentAccount($Distributor, $amountToLedger, $_POST["transID"], "DEPOSIT", "Transaction Amended", "Distributor",$note,$currecyToLedger);
					 updateSubAgentAccount($preDistInfo["userID"], $amountToLedger, $_POST["transID"], "DEPOSIT", "Transaction Amended", "Distributor",$currecyToLedger);
			}else{
				$Distributor=$preDistInfo["userID"];		
				$q = updateAgentAccount($Distributor, $amountToLedger, $_POST["transID"], "DEPOSIT", "Transaction Amended", "Distributor",$note,$currecyToLedger);
			}
			///////////Do New Entries///////
			if(DEST_CURR_IN_ACC_STMNTS == "1" || CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == "1")
			{
			 $amountToLedger = $_POST["localAmount"];	
			 $currecyToLedger = $_POST["currencyTo"];
			 }else{
				$amountToLedger = $_POST["transAmount"];
				$currecyToLedger = $_POST["currencyFrom"];
				}
			$currDistInfo = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$benAgentIDs."'");
			if($currDistInfo["agentType"]=="Sub")
			{
				$Distributor=$currDistInfo["parentID"];
				$q = updateAgentAccount($Distributor, $amountToLedger, $_POST["transID"], "WITHDRAW", "Transaction Amended", "Distributor",$note,$currecyToLedger);
					 updateSubAgentAccount($preDistInfo["userID"], $amountToLedger, $_POST["transID"], "WITHDRAW", "Transaction Amended", "Distributor",$currecyToLedger);
			}else{
				$Distributor=$currDistInfo["userID"];
				$q = updateAgentAccount($Distributor, $amountToLedger, $_POST["transID"], "WITHDRAW", "Transaction Amended", "Distributor",$note,$currecyToLedger);
				}
				
			}
			/////////////////////////////
				
				
				
		 }
		
		if(trim($_POST["transType"]) == "Bank Transfer")
		{
			//selectFrom("select transID from bankDetails where transID = ''");
			
			$bankDetailsTable = "bankDetails";
			if ( strtoupper($_SESSION["transactionCreatedBy"]) == "CUSTOMER" )
			{
				$bankDetailsTable = "cm_bankdetails";
			}

			if(isExist("select transID from " . $bankDetailsTable . " where transID = '".$_POST["transID"]."'"))
			{
				 $queryUpdateBank = "update " . $bankDetailsTable . " set 
										benID 		= '".$_POST["benID"]."', 
										bankName 	= '".$_POST["bankName"]."', 
										accNo		= '".$_POST["accNo"]."',
										branchCode	= '".$_POST["branchCode"]."',
										branchAddress='".$_POST["branchAddress"]."',
										ABACPF 		= '".$_POST["ABACPF"]."',
										IBAN		= 	'".$_POST["IBAN"]."',
										`swiftCode` = '".$_POST["swiftCode"]."',
										Remarks		= 	'".$_POST["ibanRemarks"]."',
										accountType = '".$_SESSION["accountType"]."'
										where transID='".$_POST["transID"]."'";
				 
				update($queryUpdateBank);
			$benID =	$_POST["benID"];
			
			if(isExist("select id from " .TBL_BEN_BANK_DETAILS. " where benId = $benID ")){
				$updateBenBankQry = "UPDATE ".TBL_BEN_BANK_DETAILS. " SET `bankName` = '{$_POST["bankName"]}', `accountNo` = '{$_POST["accNo"]}', `accountType` = '{$_POST["accountType"]}', `branchCode` = '{$_POST["branchCode"]}', `branchAddress` = '{$_POST["branchAddress"]}', `swiftCode` = '{$_POST["swiftCode"]}', `sortCode` = '{$_POST["sortCode"]}', `routingNumber` = '{$_POST["routingNumber"]}', `IBAN` = '{$_POST["IBAN"]}'  WHERE benID = '$benID'"; 
				mysql_query($updateBenBankQry) or die(mysql_error());
			}else{
				$benBankQry = "Insert into ".TBL_BEN_BANK_DETAILS. " (benId, bankName, accountNo, accountType, branchCode, branchAddress, swiftCode, sortCode, routingNumber, IBAN )
					values ($benID, '".$_POST["bankName"]."','".$_POST["account"]."','".$_POST["accountType"]."','".$_POST["branchName"]."','".$_POST["branchAddress"]."','".$_POST["swiftCode"]."', '".$_POST["sortCode"]."', '".$_POST["routingNumber"]."', '".$_POST["iban"]."')";
				mysql_query($benBankQry) or die(mysql_error());
			
}
			
			
						
					if(CONFIG_ADD_BEN_BANK_FIELDS_ON_CREATE_TRANSACTION == "1")
					{
						$sqlBankUpdate = " update ".$bankDetailsTable."
											set
												sortCode = '".$_POST["sortCode"]."',
												routingNumber = '".$_POST["routingNumber"]."'
											where
													transID='".$_POST["transID"]."'";
								update($sqlBankUpdate);		
						//	debug($sqlBankUpdate);				
					}
			
			}else{
				 $sqlBank = "insert into " . $bankDetailsTable . "
							( benID, transID, bankName, 
							accNo, branchCode,branchName, branchAddress,
							ABACPF , IBAN,swiftCode, accountType, Remarks )
							Values('".$_POST["benID"]."', '".$_POST["transID"]."',  '".$_POST["bankName"]."',
							'".$_POST["accNo"]."', '".$_POST["branchCode"]."',".$_POST["branchName"]."', '".$_POST["branchAddress"]."',
							'".$_POST["ABACPF"]."',	'".$_POST["IBAN"]."',	'".$_POST["swiftCode"]."','".$_SESSION["accountType"]."', '".$_POST["ibanRemarks"]."')";
				insertInto($sqlBank);
				}
			
			}
		
		if(CONFIG_SHARE_OTHER_NETWORK == '1' )
		{
			$sharedTrans = selectFrom("select * from ".TBL_SHARED_TRANSACTIONS." where localTrans = '".$_POST["transID"]."'");
			if($sharedTrans["sharedTransId"] != '')
			{
				$benAgentInfo = selectFrom("select fromServer from " . TBL_ADMIN_USERS . " where userID = '".$benAgentIDs."'");
				if($benAgentInfo["fromServer"] != '')
				{
					
					echo("agent commission".$agentCommi);
					echo "total share".$sharedTransComm = $_POST["IMFee"] - $agentCommi;
					//echo "clientComm".$clientComm = (($sharedTransComm * ($jointClientComm["commissionShare"])) / 100);
					if(CONFIG_SHARE_FROM_TOTAL == '1')
					{													
						$clientComm = (($_POST["IMFee"] * ($jointClientComm["commissionShare"])) / 100);
					}else{
						$clientComm = (($sharedTransComm * ($jointClientComm["commissionShare"])) / 100);
						}
					$clientTotalAmount = $_POST["transAmount"] + $clientComm;
					$transactionDescription = "Transaction Amended";
					include("amendOtherNetworkTrans.php");		
					dbConnect();
					
					
					
				}
			}
				
		}
		
		
		  $_SESSION["transType"] 		= "";
			$_SESSION["benAgentID"] 	= "";
			$_SESSION["customerID"] 	= "";
			$_SESSION["benID"] 			= "";
			$_SESSION["moneyPaid"] 		= "";
			$_SESSION["transactionPurpose"] = "";
			$_SESSION["other_pur"] = "";
			$_SESSION["fundSources"] 	= "";
			$_SESSION["transStatus"] = "";
			$_SESSION["transactionCreatedBy"] = "";
			//if (CONFIG_MANUAL_CODE_WITH_RANGE != "1") {
		//		$_SESSION["refNumber"] 		= "";
			//}Commented By Kashif To see if It makes any  sense to do this...
			$_SESSION["collectionPoint"] = "";
	
			$_SESSION["transAmount"] 	= "";
			$_SESSION["exchangeRate"] 	= "";
			$_SESSION["exchangeID"] 	= "";
			$_SESSION["localAmount"] 	= "";
			$_SESSION["totalAmount"] 	= "";
			$_SESSION["IMFee"] 			= "";
	
			// resetting Session vars for trans_type Bank Transfer
			$_SESSION["bankName"] 		= "";
			$_SESSION["branchCode"] 	= "";
			$_SESSION["branchName"] 	= "";
			$_SESSION["branchAddress"] 	= "";
			$_SESSION["swiftCode"] 		= "";
			$_SESSION["accNo"] 			= "";
			$_SESSION["ABACPF"] 		= "";
			$_SESSION["IBAN"] 			= "";
			$_SESSION["ibanRemarks"] 			= "";
			$_SESSION["accountType"] = "";
			
			
			$_SESSION["question"] 			= "";
			$_SESSION["answer"]     = "";	
			$_SESSION["tip"]     = "";	
			$_SESSION["branchName"]     = "";	
			$_SESSION["currencyCharge"] = "";
			$_SESSION["transDate"]="";
			$_SESSION["transDate2"] = "";
			$_SESSION["internalRemarks"] ="";
			$_SESSION["documentChanged"] ="";
			$_SESSION["benIdPassword"] = "";
			$_SESSION["checkManualRate"] = "";
			$_SESSION["docCategory"] = "";
			$_SESSION["useTransDetails"] = "";
			$_SESSION["valueDate"] = "";
			
			if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
			{
				$_SESSION["chequeAmount"] = "";
			}

			$_SESSION["senderBank"] = "";
// this condition is added for Batch transaction, By Imran
			if ($_SESSION["amount_left"]!= ""){
			//$_SESSION["amount_left"] = $_SESSION["amount_left"] - $_POST["transAmount"];
			$_SESSION["transDate"] = $_POST["dDate"];
			}
			else
			{
				//$_SESSION["amount_left"] = $_SESSION["amount_transactions"] - $_POST["transAmount"];
				$_SESSION["transDate"] = $_POST["dDate"];	
				}
// end
	insertError("Transaction ".$oldTrans["refNumberIM"]." has been updated successfully");	
		$_SESSION["imReferenceNumber"]= $oldTrans["refNumberIM"];
			$_SESSION["refNumber"]= $oldTrans["refNumberIM"];
		
		 if(CONFIG_CUSTOM_AMEND_RECIEPT == '1')
		{	
	 		$backUrl = CONFIG_RECIEPT_PRINT."?msg=Y&success=Y&transID=".$_POST["transID"]."&transSend=$transSend"."&customerID=".$_GET["customerID"];
			if(strtoupper($_REQUEST["transType"])=="BANK TRANSFER" && defined("CONFIG_RECIEPT_PRINT_BANK") && CONFIG_RECIEPT_PRINT_BANK!="0"){
				$backUrl = CONFIG_RECIEPT_PRINT_BANK."?msg=Y&transSend=$transSend&r=".$_GET["r"]."&customerID=".$_GET["customerID"];
			}
}else{
	$backUrl = "$returnPage?msg=Y&success=Y&transSend=$transSend&docWaiver=".$_REQUEST["docWaiver"]."&customerID=".$_GET["customerID"];
}
//	debug($backUrl, true);
// redirect($backUrl);
}
exit();
?>
