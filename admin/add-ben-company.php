<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	//$agentID = $_SESSION["loggedUserData"]["userID"];
	$payinAmount = CONFIG_PAYIN_FEE;
	$company = COMPANY_NAME;
	$created_date = date("Y-m-d");
 	if(defined("CONFIG_TRANS_TYPE_TT") && CONFIG_TRANS_TYPE_TT==1)
	{	
		$ttTransFlag=true;
	}
	if($_REQUEST["from"] == "popUp") {
		$returnPage = "add-transaction.php";
		}elseif($_REQUEST["from"] == "searchBen"){
		$returnPage = "create-bene-trans.php";
		}
	
		// @5278- Multiple ID Types
//debug($_REQUEST);
	// Fetch available ID Types
	$query = "SELECT * from id_types
				WHERE active='Y'
				AND show_on_beneficiary_as_company='Y'";

	$IDTypes = selectMultiRecords($query);
	// If its an update sender request, find the sender's ID Type values.
	if (!empty( $_REQUEST["benID"]))
	{
		// Now fetch values against available id types.
		$numberOfIDTypes = count( $IDTypes );
		for( $i = 0; $i < $numberOfIDTypes; $i++ )
		{
			$IDTypeValuesQ = "SELECT * from user_id_types
									WHERE
										id_type_id = '" . $IDTypes[$i]["id"] . "'
										AND	user_id = '" . $_REQUEST["benID"] . "'
										AND user_type = 'B'";
			$IDTypeValues = selectFrom($IDTypeValuesQ);
			if (!empty( $IDTypeValues ))
				$IDTypes[$i]["values"] = $IDTypeValues;
		}
	}

	// End  Multiple ID Types
	
	if(!empty($_REQUEST["saveInputs"]))
	{
	
		$bolDataSaved = false;
		/* Request to store or update the data of company */
		if(empty($_REQUEST["benID"]))
		{
			/* If the Id is empty than the record is new one */
			  	 
			$strInsertSql = "insert into ".TBL_BENEFICIARY." 
											(
											  firstName,
											  Address,
											  Address1,
											  Country,
											  City,
											  State,
											  email,
											  Zip,
											  Phone,
											  Mobile,
											  beneficiaryName,
											  ben_type,
											  company_msb_number,
											  customerID,
											  agentID,
											  created
											)
									 values
										   (
											 '".$_REQUEST["companyName"]."',
											 '".$_REQUEST["address"]."',
											 '".$_REQUEST["address1"]."',
											 '".$_REQUEST["country"]."',
											 '".$_REQUEST["city"]."',
											 '".$_REQUEST["state"]."',
								 			 '".$_REQUEST["email"]."',
											 '".$_REQUEST["zip"]."',
											 '".$_REQUEST["phone"]."',
											 '".$_REQUEST["mobile"]."',
											 '".$_REQUEST["firstName"]."',
											 'company',
											 '".$_REQUEST["msbNumber"]."',
											 '".$_REQUEST["customerID"]."',
											 '".$_REQUEST["agentID"]."',
											 '".$created_date."'
											)";
		
			if(mysql_query($strInsertSql))
			{
				$benID = mysql_insert_id();
				$bolDataSaved = true;
		
	  	$descript ="Beneficiary is added";
		activities($_SESSION["loginHistoryID"],"INSERTION",$benID,TBL_BENEFICIARY,$descript);
		
		$startMessage="Congratulations! You have been registered as a company of $company. <br>
		 Your registration information is as follows:";
		include ("mailBenInfo.php");
		$To = $_REQUEST["email"]; 
		$fromName = SUPPORT_NAME;
		$fromEmail = SUPPORT_EMAIL;
		$subject = "Beneficiary Account creation ";	
		
		if(CONFIG_MAIL_GENERATOR == '1'){
		sendMail($To, $subject, $data, $fromName, $fromEmail);
		}
		
	
	/************ Start Multiple ID Code *********/
	
	if ( !empty( $_REQUEST["IDTypesValuesData"] ) )
	{
		if(isset($_REQUEST["arrMultipleIds"]))
			$idTypeValuesData = unserialize(stripslashes(stripslashes($_REQUEST["arrMultipleIds"])));
		else	
			$idTypeValuesData = $_REQUEST["IDTypesValuesData"];
	
		foreach( $idTypeValuesData as $idTypeValues )
		{
			if ( !empty( $idTypeValues["id_type_id"] )
				 && !empty( $idTypeValues["id_number"] )
				 && !empty( $idTypeValues["issued_by"] )
				 && !empty( $idTypeValues["issue_date"] )
				 && !empty( $idTypeValues["expiry_date"] ) )
			{
				// If id is empty, its an insert request.
				// Insert new reocrd in this case.

				$issueDate = $idTypeValues["issue_date"]["year"] . "-" . $idTypeValues["issue_date"]["month"] . "-" . $idTypeValues["issue_date"]["day"];
				$expiryDate = $idTypeValues["expiry_date"]["year"] . "-" . $idTypeValues["expiry_date"]["month"] . "-" . $idTypeValues["expiry_date"]["day"];

				$insertQuery = "insert into user_id_types
										(
											id_type_id, 
											user_id, 
											user_type, 
											id_number, 
											issued_by, 
											issue_date, 
											expiry_date 
										)
									values
										(
											'" . $idTypeValues["id_type_id"] . "', 
											'" . $benID . "', 
											'B', 
											'" . $idTypeValues["id_number"] . "', 
											'" . $idTypeValues["issued_by"] . "', 
											'" . $issueDate . "', 
											'" . $expiryDate . "'
										)";
				insertInto( $insertQuery );
			/*
			 * @Ticket #4794
			 */
					$lastIdInsertId = @mysql_insert_id();
					if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
						update("update user_id_types set notes='".mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$lastIdInsertId."'");
					}
				}
			}
			if(!empty($lastIdInsertId))
				$bolDataSaved = true;
		}
		// End of code against #3299: MasterPayex - Multiple ID Types
		
		//$bolDataSaved = true;
	 }	
	 else
			$bolDataSaved = false;
			insertError(BE9);
  }
 else
 {
			/**
			 * Else the record is not new and required the updation
			 */
			$strUpdateSql = "update ".TBL_BENEFICIARY." 
							 set
							 	firstName = '".$_REQUEST["companyName"]."',
								Address = '".$_REQUEST["address"]."',
								Address1 = '".$_REQUEST["address1"]."',
								Country = '".$_REQUEST["country"]."',
								City = '".$_REQUEST["city"]."',
								State = '".$_REQUEST["state"]."',
								email = '".$_REQUEST["email"]."',
								Zip = '".$_REQUEST["zip"]."',
								Phone = '".$_REQUEST["phone"]."',
								Mobile = '".$_REQUEST["mobile"]."',
								beneficiaryName = '".$_REQUEST["companyName"]."',
								company_msb_number = '".$_REQUEST["msbNumber"]."'
								
							 where
							 	  	benID = '".$_REQUEST["benID"]."'
						"; 
	//debug($strUpdateSql,true);
	
	if(update($strUpdateSql)){
		$benID	= $_REQUEST["benID"];
	
	/************* Update Multiple ID Code ****/
		// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
	if ( !empty( $_REQUEST["IDTypesValuesData"] )  || !empty($_REQUEST["arrMultipleIds"]))
	{
		if(isset($_REQUEST["arrMultipleIds"]))
			$idTypeValuesData = unserialize(stripslashes(stripslashes($_REQUEST["arrMultipleIds"])));
		else	
			$idTypeValuesData = $_REQUEST["IDTypesValuesData"];

		foreach( $idTypeValuesData as $idTypeValues )
		{
			if ( !empty( $idTypeValues["id_type_id"] )
				 && !empty( $idTypeValues["id_number"] )
				 && !empty( $idTypeValues["issued_by"] )
				 && !empty( $idTypeValues["issue_date"] )
				 && !empty( $idTypeValues["expiry_date"] ) )
			{
				$issueDate = $idTypeValues["issue_date"]["year"] . "-" . $idTypeValues["issue_date"]["month"] . "-" . $idTypeValues["issue_date"]["day"];
				$expiryDate = $idTypeValues["expiry_date"]["year"] . "-" . $idTypeValues["expiry_date"]["month"] . "-" . $idTypeValues["expiry_date"]["day"];

				if ( !empty($idTypeValues["id"]) )
				{
					// If id is not empty, its an update request.
					// Update the existing record in this case.

					$updateQuery = "update user_id_types 
										set 
											id_number='" . $idTypeValues["id_number"] . "', 
											issued_by='" . $idTypeValues["issued_by"] . "', 
											issue_date='" . $issueDate . "', 
											expiry_date='" . $expiryDate . "' 
										where 
											id='" . $idTypeValues["id"] . "'";
					update( $updateQuery );
			/*
			 * @Ticket #4794
			 */
					if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
						update("update user_id_types set notes='".mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$idTypeValues["id"]."'");
						
					}
				}
				else
				{
					// If id is empty, its an insert request.
					// Insert new reocrd in this case.

					$insertQuery = "insert into user_id_types
											(
												id_type_id, 
												user_id, 
												user_type, 
												id_number, 
												issued_by, 
												issue_date, 
												expiry_date 
											)
										values
											(
												'" . $idTypeValues["id_type_id"] . "', 
												'" . $_REQUEST["benID"] . "', 
												'B', 
												'" . $idTypeValues["id_number"] . "', 
												'" . $idTypeValues["issued_by"] . "', 
												'" . $issueDate . "', 
												'" . $expiryDate . "'
											)";
					insertInto( $insertQuery );
			/*
			 * @Ticket #4794
			*/
					$lastIdInsertId = @mysql_insert_id();
  
					if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
						update("update user_id_types set notes='". mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$lastIdInsertId."'");
					}
				}
			}
		}
	}

	/*********** End Update Multiple ID Code ***********/
	
	
		   $bolDataSaved = true;
		   insertError(BE10);
	}			
			else
				$bolDataSaved = false;
}
	if($bolDataSaved){
			//echo $benID;
			//debug($ttTransFlag);
			if($ttTransFlag)
			{
				
				$CompBen="1";
				if($_REQUEST["from"]=="TTtransfer" || $_REQUEST["callFrom"]=="TT")
				{
				
				
					echo '<script>
							
							window.opener.SelectBen('.$benID.','.$CompBen.');
					
							window.close();
						</script>';	
				}
			}
		}else{
			echo 0;
	}
	if($_GET["from"] == "popUp" && CONFIG_BENEFICIARY_COMPANY_RETURN =='1')
	{
		$returnPage = "add-transaction.php";
	?>
		<script language="javascript">
		opener.document.location = "<?=$returnPage?>?msg=Y&benID=<?=$benID?>&success=Y&transID=<?=$_GET['transID']?>";
		window.close();
		</script>
	<?
	}
	exit();	
 }

	/**
	 * If the record is for editing 
	 * For this check if the company_id field is empty or not, if not than request will be for editing the record
	 */
	$strMainCaption = "Add New Company Record"; 
	if(!empty($_REQUEST["benID"]))
	{
	
		$strGetDataSql = "SELECT 
							  firstName,
							  Address,
							  Address1,
							  Country,
							  City,
							  State,
							  email,
							  Zip,
							  Phone,
							  Mobile,
							  beneficiaryName,
							  ben_type,
							  company_msb_number,
							  created
						  FROM 
						  	 ".TBL_BENEFICIARY." 
						  WHERE 
						    benID = '".$_REQUEST["benID"]."'";
		
		$arrCompanyData = selectFrom($strGetDataSql);
		
		$companyName = $arrCompanyData["firstName"];
		$address = $arrCompanyData["Address"];
		$address1 = $arrCompanyData["Address1"];
		$country = $arrCompanyData["Country"];
		$city = $arrCompanyData["City"];
		$state = $arrCompanyData["State"];
		$email = $arrCompanyData["email"];
		$zip = $arrCompanyData["Zip"];
		$phone = $arrCompanyData["Phone"];
		$mobile = $arrCompanyData["Mobile"];
		$msbNumber = $arrCompanyData["company_msb_number"];
		$strMainCaption = "Modify Existing Company Record"; 
		
		$benID	= $_REQUEST["benID"];
	}
	
	

	/**
	 * Picking up the country and city data to display in the drop down list
	 */
	$arrCountries = selectMultiRecords("select countryname from countries order by countryname");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Manage Company Record</title>
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="./javascript/jquery.selectboxutils.js"></script>
<script>
	$(document).ready(function() {
		$("#submitCompany").validate({
			rules: {
				companyName: "required",
				country: "required",
				city:  "required"
				//mobile: "required"
								
			},
			messages: {
				companyName: "<br />Please enter full name of company",
				country: "<br />Please select the country from dropdown",
				city: "<br />Please enter the city."
				//mobile: "<br />Please enter the mobile."
			},
			submitHandler: function(form) {
				
				if (checkIdTypes($("#submitCompany")) == false )
					return false;

				$("#submitCompany").ajaxSubmit(function(data) { 
				//alert(data);
				var msg = $("#companyName").val()+" saved successfully! \n Do you want to add more companies information?";
				
				if(data != 0){
		
					var from = '<?=$_REQUEST["from"]?>';
					var benID = '';
					var customerID = '<?=$_REQUEST["customerID"]?>';
					var agentID = '<?=$_REQUEST["agentID"]?>';
					var transID = '<?=$_REQUEST["transID"]?>';
					var returnPage = '<?=$returnPage?>';
					var callFrom = '<?=$_REQUEST["callFrom"]?>';
					var ttTrans  =  '<?=$ttTransFlag?>';
					var	backURL = returnPage+"?benID="+data+"&agnetID="+agentID+"&transID="+transID;				
						 if(from == 'popUp' )
						 { 
						  
						      clearFormCustomize();
							  opener.document.location = backURL;
							   window.close();
						 } else if(from == 'searchBen'){
						 		alert("beneficiary  inforamtion updated successfully");
						 		window.close();
								/*window.history.go(-1);*/
						 }
						 else if(ttTrans)
						 { 
						  	var CompBen=1;
						 
							 if(callFrom =='TT' && from=='TTtransfer' && )
							 {
								window.opener.SelectBen(data,CompBen);
				
								window.close();
							 }
						 }
						 else
						 {
							clearFormCustomize();
							if(!confirm(msg)){
								 window.close();
							}
							/*else
							{
								//alert(data);
								alert("The record could not be saved due to error, Please try again."); 
							}*/
				  }
				}else{
				    alert("The record could not be saved due to error, Please try again."); 
				}	
			});
		   }
		});
		
			
		$("#closeWindow").click(function(){
			close();
		});
	});
	
	
	
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}
   
 // function will clear input elements on each form  
    function clearFormCustomize(){  
    	  // declare element type  
      var type = null;  
      // loop through forms on HTML page  
      for (var x=0; x<document.forms.length; x++){  
        // loop through each element on form  
        for (var y=0; y<document.forms[x].elements.length; y++){  
          // define element type  
         type = document.forms[x].elements[y].type  
         // alert before erasing form element  
         //alert('form='+x+' element='+y+' type='+type);  
         // switch on element type  
         switch(type){  
           case "text":  
           case "textarea":  
           case "password":  
           //case "hidden":  
             document.forms[x].elements[y].value = "";  
             break;  
           case "radio":  
           case "checkbox":  
             document.forms[x].elements[y].checked = "";  
             break;  
           case "select-one":  
             document.forms[x].elements[y].options[0].selected = true;  
             break;  
           case "select-multiple":  
             for (z=0; z<document.forms[x].elements[y].options.length; z++){  
               document.forms[x].elements[y].options[z].selected = false;  
             }  
           break; 
         }  
       }  
     }  
   }  	
</script>
<style>
td{ font-family:Verdana, Arial, Helvetica, sans-serif;
size:8px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.remarks
{
	font-size:12px;
	text-align:center;
	font-weight:bold;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
</style>
</head>
<body>
<form name="submitCompany" id="submitCompany" action="" method="post">
	<table width="80%" border="0" cellspacing="1" cellpadding="2" align="center">
		<tr>
			<td colspan="4" align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong>			</td>
		</tr>
		<? if(!empty($msg)) { ?>
			<tr bgcolor="#000000">
				<td colspan="4" align="center" style="text-align:center; color:#EEEEEE; font-size:10px;">
					<img src="images/info.gif" />&nbsp;<?=$msg?>				</td>
			</tr>
		<? } ?>
		
		<tr bgcolor="#ededed">
			<td colspan="4" align="center" class="tdDefination" style="text-align:center">
				All fields mark with <font color="red">*</font> are compulsory Fields.			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Company Name:&nbsp;</b><font color="red">*</font></td>
			<td align="left" colspan="3"><input type="text" name="companyName" id="companyName" size="75" maxlength="150" value="<?=$companyName?>" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Address Line 1:&nbsp;</b></td>
			<td align="left" colspan="3"><input type="text" name="address" id="address" size="75" maxlength="150" value="<?=$address?>" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Address Line 2:&nbsp;</b></td>
			<td align="left" colspan="3"><input type="text" name="address1" id="address1" size="75" maxlength="150" value="<?=$address1?>" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Country:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="25%">
				<select name="country" id="country">
					<option value="">Select Country</option>
					<? 
						for($i = 0; $i < count($arrCountries); $i++) 
						{ 
							$strSelected = '';
							if($arrCountries[$i]["countryname"] == $country)
								$strSelected = ' selected="selected"';
					?>
							<option value="<?=$arrCountries[$i]["countryname"]?>" <?=$strSelected?>><?=$arrCountries[$i]["countryname"]?></option>
					<? } ?>
				</select>			</td>
			<td width="25%" class="tdDefination"><b>City:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="25%">
				<input type="text" name="city" id="city" value="<?=$city?>" />			</td>
		</tr>
		<tr bgcolor="#ededed">
		  <td class="tdDefination"><b>County:</b></td>
		  <td align="left"><input type="text" name="state" id="state" value="<?=$state?>" /></td>
		  <td class="tdDefination"><b>E-Mail:</b></td>
		  <td align="left"><input type="text" name="email" id="email" size="40" maxlength="150" value="<?=$email?>" /></td>
	  </tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Zip Code:&nbsp;</b></td>
			<td align="left" width="25%"><input type="text" name="zip" id="zip" size="20" maxlength="10" value="<?=$zip?>" /></td>
			<td class="tdDefination"><b>MSB Number &nbsp;</b></td>
			<td align="left"><input type="text" name="msbNumber" id="msbNumber" value="<?=$msbNumber?>" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Phone:&nbsp;</b></td>
			<td align="left" width="25%"><input type="text" name="phone" id="phone" size="40" maxlength="150" value="<?=$phone?>" /></td>
			<td width="25%" class="tdDefination"><b>Mobile:&nbsp;<font color="red">*</font></b></td>
			<td align="left" width="25%"><input type="text" name="mobile" id="mobile" size="40" maxlength="150" value="<?=$mobile?>" /></td>
		</tr>
		<?
		 	/*************** @5278- Multiple ID Types Code Start From Here***********/ 
			 if(CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY == 1)
			 {
				if ( !empty( $IDTypes ) )
				{
		?>
		<tr bgcolor="#ededed">
		  <td colspan="4" class="tdDefination">
		  	<div align="center">
		  		<table width="100%">
					<tr>
						 <?
							$tdNumber = 1;
							$numberOfIDTypes = count($IDTypes);
							$chkIDTypesJS = "";
							$serverDateArray["year"]  = date("Y");
							$serverDateArray["month"] = date("m");
							$serverDateArray["day"]   = date("d");
	
							for( $i = 0; $i < $numberOfIDTypes; $i++ )
							{
								$idNumber = "";
								$idTypeValuesSESS = "";
								$issuedBy = "";
	
								if( !empty($_REQUEST["IDTypesValuesData"])
									&& !empty($_REQUEST["IDTypesValuesData"][$IDTypes[$i]["id"]]))
								{
								   
									$idTypeValuesSESS = $_REQUEST["IDTypesValuesData"];
									if(isset($idTypeValuesSESS[$IDTypes[$i]["id"]]["id_number"]))
									{
										$idNumber = $idTypeValuesSESS[$IDTypes[$i]["id"]]["id_number"];
										$issuedBy = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issued_by"];
									}
								}
								else if (!empty($IDTypes[$i]["values"]))
								{
									$idNumber = $IDTypes[$i]["values"]["id_number"];
									$issuedBy = $IDTypes[$i]["values"]["issued_by"];
								}

							?>
							<td style="padding:2px; text-align:center; <?=($tdNumber==1 ? "border-right:1px solid black;" : "");?> ">
								<table border="1" cellpadding="2" cellspacing="0" align="center">
									  <tr>
										 <td colspan="2" style="border:2px solid black; font-weight:bold;">
											<?=$IDTypes[$i]["title"];?> <?=($IDTypes[$i]["mandatory"] == 'Y' ? "<span style='color:red;'>*</span>" : ""); ?>
										  </td>
									   </tr>
									   <tr>
										 <td width="80">ID Number:</td>
										  <td>
											   <input type="hidden" name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id]" 
											   value="<?=( !empty($IDTypes[$i]["values"]) ? $IDTypes[$i]["values"]["id"] : "" );?>" />
											   <input type="hidden" name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id_type_id]" 
											    value="<?=$IDTypes[$i]["id"];?>" />
												<input type="text" id="id_number_<?=$IDTypes[$i]["id"];?>" 
												 name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id_number]" value="<?=$idNumber;?>" size="40" />
											</td>
										 </tr>
										 <tr>
											<td>Issued by:</td>
											 <td>
											  		<input type="text" id="id_issued_by_<?=$IDTypes[$i]["id"];?>" 
											  		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issued_by]" value="<?=$issuedBy;?>" size="40" />
											 </td>
										 </tr>
										 <tr>
										    <td>Issue Date:</td>
											<td>
												<?
													$startYear = 1985;
													$currentYear = $serverDateArray["year"];

													$issueDay = 0;
													$issueMonth = 0;
													$issueYear = 0;

													if ( !empty($idTypeValuesSESS)
														&& !empty($idTypeValuesSESS[$IDTypes[$i]["id"]]) 
														&& isset($idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]))
													{
														$issueDay = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["day"] - 1;
														$issueMonth = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["month"] - 1;
														$issueYear = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["year"] - $startYear;
													}
													else if ( !empty($IDTypes[$i]["values"]) )
													{
														$dateArray = explode( "-", $IDTypes[$i]["values"]["issue_date"] );
														$issueDay = $dateArray[2] - 1;
														$issueMonth = $dateArray[1] - 1;
														$issueYear = $dateArray[0] - $startYear;
													}
												?>
													<select id="issue_date_day_<?=$IDTypes[$i]["id"];?>" 
															name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][day]" >
													</select>
													
													<select id="issue_date_month_<?=$IDTypes[$i]["id"];?>" 
															name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][month]" >
													</select>
													
													<select id="issue_date_year_<?=$IDTypes[$i]["id"];?>" 
															name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][year]" >
													</select>

												<script type="text/javascript">
													   initDatePicker('issue_date_day_<?=$IDTypes[$i]["id"];?>', 'issue_date_month_<?=$IDTypes[$i]["id"];?>',
													   'issue_date_year_<?=$IDTypes[$i]["id"];?>', '', '1:12', '<?=$startYear;?>:<?=$currentYear;?>', 
														<?=$issueDay;?>, <?=$issueMonth;?>, <?=$issueYear;?>);
												</script>
												</td>
											  </tr>
											  <tr>
												<td>Expiry Date:</td>
												<td>
													<?
														$expiryStartYear = $currentYear;
														$expiryDay = $serverDateArray["day"] - 1; 
														// Didn't do "less 1" here because atleast one day further from current has to be selected.
														$expiryMonth = $serverDateArray["month"] - 1;
														$expiryYear = $serverDateArray["year"] - $expiryStartYear;
	
														if ( !empty($idTypeValuesSESS)
															 && !empty($idTypeValuesSESS[$IDTypes[$i]["id"]]) 
															 && isset($idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]))
														{
															$expiryDay = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["day"] - 1;
															$expiryMonth = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["month"] - 1;
															$expiryYear = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["year"] - $expiryStartYear;
														}
														else if ( !empty($IDTypes[$i]["values"]) )
														{
															$dateArray = explode( "-", $IDTypes[$i]["values"]["expiry_date"] );
	
															$expiryStartYear = ( $dateArray[0] <= $currentYear ? $dateArray[0] : $currentYear );
	
															$expiryDay = $dateArray[2] - 1;
															$expiryMonth = $dateArray[1] - 1;
															$expiryYear = $dateArray[0] - $expiryStartYear;
														}
													?>
														<select id="expiry_date_day_<?=$IDTypes[$i]["id"];?>" 
																name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][day]" >
														</select>

														<select id="expiry_date_month_<?=$IDTypes[$i]["id"];?>"
																name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][month]" >
														</select>

														<select id="expiry_date_year_<?=$IDTypes[$i]["id"];?>" 
																name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][year]" >
														</select>

													<script type="text/javascript">
														initDatePicker('expiry_date_day_<?=$IDTypes[$i]["id"];?>', 'expiry_date_month_<?=$IDTypes[$i]["id"];?>',
														'expiry_date_year_<?=$IDTypes[$i]["id"];?>', '', '1:12', '<?=$expiryStartYear;?>:2035',
														 <?=$expiryDay;?>, <?=$expiryMonth;?>, <?=$expiryYear;?>);
													</script>
													</td>
												 </tr>
											</table>
										</td>
										   <?
											if ($tdNumber == 2 )
											{
												// Starting a new TR when two TDs are output.
												?>
										  		</tr><tr>
												<?
												$tdNumber = 1;
											}
											else
												$tdNumber++;

											$chkIDTypesJS .= "id_number_" . $IDTypes[$i]["id"] . " = document.getElementById('id_number_" . $IDTypes[$i]["id"] . "');
											id_issued_by_" . $IDTypes[$i]["id"] . " = document.getElementById('id_issued_by_" . $IDTypes[$i]["id"] . "');";

											if ( $IDTypes[$i]["mandatory"] == 'Y' )
											{
												$chkIDTypesJS .= "
															if ( id_number_" . $IDTypes[$i]["id"] . ".value=='' )
															{
																alert('Please provide " . $IDTypes[$i]["title"] . "\\'s id number detail.');
																id_number_" . $IDTypes[$i]["id"] . ".focus();
																return false;
															}
															if ( id_issued_by_" . $IDTypes[$i]["id"] . ".value=='' )
															{
																alert('Please provide " . $IDTypes[$i]["title"] . "\\'s issued by detail.');
																id_issued_by_" . $IDTypes[$i]["id"] . ".focus();
																return false;
															}
														  ";
											}
											else
											{
												$chkIDTypesJS .= "
														if ( id_number_" . $IDTypes[$i]["id"] . ".value=='' && id_issued_by_" . $IDTypes[$i]["id"] . ".value != '' )
														{
															alert('Please provide " . $IDTypes[$i]["title"] . "\\'s id number detail.');
															id_number_" . $IDTypes[$i]["id"] . ".focus();
															return false;
														}
														if ( id_number_" . $IDTypes[$i]["id"] . ".value != '' && id_issued_by_" . $IDTypes[$i]["id"] . ".value=='' )
														{
															alert('Please provide " . $IDTypes[$i]["title"] . "\\'s issued by detail.');
															id_issued_by_" . $IDTypes[$i]["id"] . ".focus();
															return false;
														}
													";
													}

													$chkIDTypesJS .= "
															issueDate = new Date();
															todayDate = new Date( issueDate );
															expiryDate = new Date();
															todayDate.setFullYear(" . $serverDateArray["year"] . ", " . ($serverDateArray["month"] - 1) . ", " .
															 $serverDateArray["day"] . " );
															issueDate.setFullYear( document.getElementById('issue_date_year_" . $IDTypes[$i]["id"] . "').value,
														    document.getElementById('issue_date_month_" . $IDTypes[$i]["id"] . "').value - 1, 
															document.getElementById('issue_date_day_" . $IDTypes[$i]["id"] . "').value );
															expiryDate.setFullYear( document.getElementById('expiry_date_year_" . $IDTypes[$i]["id"] . "').value, 
															document.getElementById('expiry_date_month_" . $IDTypes[$i]["id"] . "').value - 1, 
															document.getElementById('expiry_date_day_" . $IDTypes[$i]["id"] . "').value );
														if ( issueDate >= todayDate )
														{
															alert( 'The issue date for " . $IDTypes[$i]["title"] . " should be less than today\\'s date.' );
															document.getElementById('issue_date_day_" . $IDTypes[$i]["id"] . "').focus();
															return false;
														}
														if ( expiryDate < todayDate )
														{
															alert( 'The expiry date for " . $IDTypes[$i]["title"] . " should be greater than today\\'s date.' );
															document.getElementById('expiry_date_day_" . $IDTypes[$i]["id"] . "').focus();
															return false;
														}
													   ";
											}
										 ?>
										</tr>
									</table>
												<script type="text/javascript">
													function checkIdTypes(submitCompany )
													{
														<?
															if ( !empty($chkIDTypesJS) )
																echo $chkIDTypesJS;
														?>
														return true;
													}
												</script>
			</div>
		  </td>
	  </tr>
	      <?
				}
			} /*****End Code of - Multiple ID Types @5278 *****/
		  ?>
		
		<tr bgcolor="#ededed">
			<td colspan="4" align="center">
				<input type="submit" name="storeData" value="Submit" />
				&nbsp;&nbsp;
				<input type="reset" value="Clear Fields" />
				<input type="hidden" name="benID" value="<?=$benID?>" />
				<input type="hidden" name="from" value="<?=$_REQUEST["from"]?>" />
				<input type="hidden" name="agentID" value="<?=$_REQUEST["agentID"]?>" />
				<input type="hidden" name="customerID" value="<?=$_REQUEST["customerID"]?>" />
				<input type="hidden" name="transID" value="<?=$_REQUEST["transID"]?>" />
				<input type="hidden" name="saveInputs" value="y" />
				&nbsp;&nbsp;
				<input type="button" id="closeWindow" value="Close Window" />			</td>
		</tr>
	</table>
</form>
</body>
</html>