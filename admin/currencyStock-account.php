<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];
//echo "+".$agentType."+";
session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonthCurr");
session_register("fDayCurr");
session_register("fYearCurr");
session_register("tMonthCurr");
session_register("tDayCurr");
session_register("tYearCurr");

if($_POST["Submit"]=="Search")
{

	$_SESSION["fMonthCurr"]="";
	$_SESSION["fDayCurr"]="";
	$_SESSION["fYearCurr"]="";
	
	$_SESSION["tMonthCurr"]="";
	$_SESSION["tDayCurr"]="";
	$_SESSION["tYearCurr"]="";
			
		
	$_SESSION["fMonthCurr"]=$_POST["fMonthCurr"];
	$_SESSION["fDayCurr"]=$_POST["fDayCurr"];
	$_SESSION["fYearCurr"]=$_POST["fYearCurr"];
	
	$_SESSION["tMonthCurr"]=$_POST["tMonthCurr"];
	$_SESSION["tDayCurr"]=$_POST["tDayCurr"];
	$_SESSION["tYearCurr"]=$_POST["tYearCurr"];
	
	$_SESSION["closingBalance"] = "";
	$_SESSION["openingBalance"] = "";
	$_SESSION["currDate"] = "";	
	
}

if($_POST["buySellVal"]!="")
	$_SESSION["buySell"] =$_POST["buySellVal"];
elseif($_GET["buySellVal"]!="")
	$_SESSION["buySell"]=$_GET["buySellVal"];
else 
	$_SESSION["buySell"] = "";

if($_POST["currencyStockN"]!="")
	$_SESSION["currencyStockN"] =$_POST["currencyStockN"];
elseif($_GET["currencyStockN"]!="")
	$_SESSION["currencyStockN"]=$_GET["currencyStockN"];
else 
	$_SESSION["currencyStockN"] = "";
	
if ($offset == "")
	$offset = 0;
$limit = 30;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
      
?>
<html>
<head>
	<title>Currency Stock Account</title>
<script>
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
</SCRIPT>
<script language="javascript" src="./javascript/functions.js"></script>
<script type="text/javascript" src="jquery.js"></script>

<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	 <tr>
    <td class="topbar">GBP Currency Stock List Extended</td>
  </tr>
	<tr>
      <td width="563">
	<form action="currencyStock-account.php" method="post" name="Search">
		<center>
  		<div align="center">From 
        <? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
                
                <SELECT name="fDayCurr" size="1" id="fDayCurr" style="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fDayCurr, "<?=$_SESSION["fDayCurr"]; ?>");
        </script>
		<SELECT name="fMonthCurr" size="1" id="fMonthCurr" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fMonthCurr, "<?=$_SESSION["fMonthCurr"]; ?>");
        </script>
                <SELECT name="fYearCurr" size="1" id="fYearCurr" style="font-family:verdana; font-size: 11px">
                  <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fYearCurr, "<?=$_SESSION["fYearCurr"]; ?>");
        </script>
                To 
               
                <SELECT name="tDayCurr" size="1" id="tDayCurr" style="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].tDayCurr, "<?=$_SESSION["tDayCurr"]; ?>");
        </script>
		 <SELECT name="tMonthCurr" size="1" id="tMonthCurr" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].tMonthCurr, "<?=$_SESSION["tMonthCurr"]; ?>");
        </script>
                <SELECT name="tYearCurr" size="1" id="tYearCurr" style="font-family:verdana; font-size: 11px">
                  <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].tYearCurr, "<?=$_SESSION["tYearCurr"]; ?>");
        </script>

				 <br/>
				<br/>
					<select name="buySellVal" id="buySellVal" style="font-family:verdana; font-size: 11px">
						<option value="">- Select Type-</option>
						<option value="B">Buying</option>
						<option value="S">Selling</option>
					</select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].buySellVal, "<?=$_SESSION["buySell"]; ?>");
                                </script>
				<input type="hidden" name="currencyStockN" id="currencyStockN" value="<?=$_SESSION["currencyStockN"];?>">				
                <input type="submit" name="Submit" value="Search">
              </div>
        </center>
		<tr>
		<td>
		
      <?
	 if($_POST["Submit"]=="Search"||$_GET["search"]=="search")
	 {
	 	
		 	$Balance="";
			$buySellVal = $_SESSION["buySell"];
			$currencyStockN = $_SESSION["currencyStockN"];
			if($currencyStockN!=""){
				$strCurrencyQ = selectFrom("SELECT cID from ".TBL_CURRENCY." WHERE currencyName='".$currencyStockN."'");
				$currencyStockID = strtoupper($strCurrencyQ["currencyName"]);
								
			}
			$fromDate = $_SESSION["fYearCurr"] . "-" . $_SESSION["fMonthCurr"] . "-" . $_SESSION["fDayCurr"];
			$toDate = $_SESSION["tYearCurr"] . "-" . $_SESSION["tMonthCurr"] . "-" . $_SESSION["tDayCurr"];
			$queryDate = "(created_at >= '$fromDate 00:00:00' and created_at <= '$toDate 23:59:59')";
			
			/***
				Add filter conditions or any condition check here.
				and then use them as these are used in all queries.
			***/
			$buySellQ="";
			$currencyNQ="";
			if($buySellVal!="")
				$buySellQ = " and buy_sell='".$buySellVal."' ";
			if($currencyStockID!="")
				$currencyNQ = " and (operationCurrency='".$currencyStockID."' OR buysellCurrency='".$currencyStockID."') ";
				 	
	      // $settlementCurr  = selectFrom("select settlementCurrency from admin  where 1 and userID=".$slctID." ");	
         //$settleCurr=$settlementCurr["settlementCurrency"];  
		
			$slctID = $changedBy;
			$accountQuery="Select * from curr_exchange_account where 1 ";
			$accountQuery .= " and $queryDate";		
			$accountQuery .= $currencyNQ;
			$accountQuery .= $buySellQ;
			if(CONFIG_CANCEL_TRANS_CURR_EXCH=="1")
				$accountQuery .= " and transStatus !='Cancelled'";	
				
			$accountQueryCnt = "Select count(*) from curr_exchange_account where 1 ";
			$accountQueryCnt .= " and $queryDate";
			$accountQueryCnt .= $currencyNQ;
			$accountQueryCnt .= $buySellQ;
			if(CONFIG_CANCEL_TRANS_CURR_EXCH=="1")
				$accountQueryCnt .= " and transStatus !='Cancelled'";	
							
			$prevDateQuery .= "Select created_at from curr_exchange_account where 1 ";
			$prevDateQuery .= " and $queryDate";				
			$prevDateQuery .= $currencyNQ;
			$prevDateQuery .= $buySellQ;
			//$accountQueryCnt .= " and status != 'Provisional' ";
			if(CONFIG_CANCEL_TRANS_CURR_EXCH=="1")
				$prevDateQuery .= " and transStatus !='Cancelled'";	
			
			
			$allCount = countRecords($accountQueryCnt);
			//echo $queryBen;
			$accountQuery .= " order by id";
		 	$accountQuery .= " LIMIT $offset , $limit"; 
			$contentsAcc = selectMultiRecords($accountQuery);
			$currDate1 = $currDate2 = $currDate;
			// this is to assign date Query String in pagination value on top of records as it is not in loop of records.
			if($contentsAcc[count($contentsAcc)-1]["created_at"]!="")
				$currDate1 = $currDate = date("Y-m-d",strtotime($contentsAcc[count($contentsAcc)-1]["created_at"]));
			if($_GET["newOffset"]>$limit){
				$prevDateQuery .= " ORDER BY id LIMIT ".($offset-$limit-1)." , 1";
				$prevDateData = selectMultiRecords($prevDateQuery);
				$currDate2 = date("Y-m-d",strtotime($prevDateData[0]["created_at"]));
//				$lastaaID = $contentsAcc[count($contentsAcc)-1]["id"];
//				debug((ceil($allCount / $limit)-1)*$limit+$limit);
			}
		 //echo($accountQuery);
		
		?>	
			
		  <table width="700" border="1" cellpadding="0" bordercolor="#666666" align="center">
			<tr>
			  <td height="25" nowrap bgcolor="#6699CC">
			  <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
				  <tr>
				  <td align="left"><?php if (count($contentsAcc) > 0) {;?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
					<?=$allCount; ?>
					<?php } ;?>
				  </td>
				  <?php if ($prv >= 0) { ?>
				  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&buySellVal=".$buySellVal."&search=search&total=first";?>"><font color="#005b90">First</font></a> </td>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&currDate=$currDate2&closingBalance=$closingBalance&sortBy=".$accountQuery."&buySellVal=".$buySellVal."&search=search&total=pre";?>"><font color="#005b90">Previous</font></a> </td>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&currDate=$currDate1&closingBalance=$closingBalance&sortBy=".$accountQuery."&buySellVal=".$buySellVal."&search=search&total=next";?>"><font color="#005b90">Next</font></a>&nbsp; </td>
				  <td width="50" align="right">
						  <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&currDate=$currDate&closingBalance=$closingBalance&sortBy=".$accountQuery."&buySellVal=".$buySellVal."&search=search&total=last";?>"><font color="#005b90">Last</font></a>
					  &nbsp; </td>
				  <?php } ?>
				</tr>
				
			  </table></td>
			</tr>
			
			<?
			if(count($contentsAcc) > 0)
			{
			?>
			<tr>
			  <td nowrap bgcolor="#EFEFEF">
			  <table width="781" border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#FFFFFF">
					<td align="left" ><span class="style1">Date</span></td>
					<td align="left" ><span class="style1">Purchased via</span></td>
<?php /*?>					<td align="left" ><span class="style1">Opening Balance</span></td><?php */?>
					<td align="left" ><span class="style1">Selling Currency</span></td>
					<td align="left" ><span class="style1">Selling Value</span></td>
					<td align="left" ><span class="style1">Buying Currency</span></td>
					<td align="left" ><span class="style1">Buying Value</span></td>
					<td align="left" ><span class="style1">Rate</span></td>	
					<td align="left" ><span class="style1">Balance</span></td>
				</tr>
					
				<? 
				$preDate = "";
				$openingBalanceSwap = "";
/*				$preDate = "";
				if($_SESSION["currDate"] != "")
					$preDate = $_SESSION["currDate"];
				if($_SESSION["closingBalance"] != "")	
					$closingBalance = $_SESSION["closingBalance"];
				if($_SESSION["openingBalance"] != "")	
					$openingBalance = $_SESSION["openingBalance"];*/
				for($i=0;$i < count($contentsAcc);$i++)
				{
					///////////////////Opening and Closing////////////////
							
					$currDate = date("Y-m-d",strtotime($contentsAcc[$i]["created_at"]));
					$unclreadQuery="";
					//$openingBalance = 0;
					$openingBalanceSwap = $closingBalance;
					
					if($i==0){
						// Total Transfer Withdraw Amount before first date.
						$summaryWithdrawSQL = "SELECT sum(localAmount) as openingAmountWith FROM curr_exchange_account WHERE 1 and id<".$contentsAcc[$i]["id"];	
						$summaryWithdrawSQL .= " AND buy_sell='S' ";
						$summaryWithdrawSQL .= $buySellQ;
						$summaryWithdrawSQL .= $currencyNQ;
						if(CONFIG_CANCEL_TRANS_CURR_EXCH=="1")
							$summaryWithdrawSQL .= " and transStatus !='Cancelled'";	
						$summaryFirstWithdraw = selectFrom($summaryWithdrawSQL);
						// Total Transfer Deposit Amount before first date.
						$summaryDepositSQL = "SELECT sum(localAmount) as openingAmountDep FROM curr_exchange_account WHERE 1 and id<".$contentsAcc[$i]["id"];
						$summaryDepositSQL .= " AND buy_sell='B'";
						$summaryDepositSQL .= $buySellQ;
						$summaryDepositSQL .= $currencyNQ;
						if(CONFIG_CANCEL_TRANS_CURR_EXCH=="1")
							$summaryDepositSQL .= " and transStatus !='Cancelled'";	
						$summaryFirstDeposit = selectFrom($summaryDepositSQL);
						// Difference of Total Withdraw and Deposit is Opening for First date.
						$openingBalance =  $summaryFirstDeposit["openingAmountDep"]-$summaryFirstWithdraw["openingAmountWith"];
						$closingBalance = $openingBalance;
						$openingBalanceSwap = $openingBalance;
						if($_GET["currDate"]!="")
							$getCurrDate = date("Y-m-d",strtotime($_GET["currDate"]));
						if($currDate==$getCurrDate){ // this logic is to retain correct the opening balance if date is same on next page.

							// Re-calculates opening balance in case next page first record has same date as previous page last record
							$summaryCompositWithSQL = "SELECT sum(localAmount) as compositWith FROM curr_exchange_account WHERE 1 ";
							$summaryCompositWithSQL .= " and created_at<'".$currDate."' AND buy_sell='S' ";
							$summaryCompositWithSQL .= $buySellQ;
							$summaryCompositWithSQL .= $currencyNQ;
							if(CONFIG_CANCEL_TRANS_CURR_EXCH=="1")
								$summaryCompositWithSQL .= " and transStatus !='Cancelled'";	
							$summaryCompositWith = selectFrom($summaryCompositWithSQL);
							$summaryCompositDepSQL = "SELECT sum(localAmount) as compositDep FROM curr_exchange_account WHERE 1 ";
							$summaryCompositDepSQL .= " and created_at<'".$currDate."' AND buy_sell='B' ";
							$summaryCompositDepSQL .= $buySellQ;
							$summaryCompositDepSQL .= $currencyNQ;
							$summaryCompositDep = selectFrom($summaryCompositDepSQL);
							$openingBalance = $summaryCompositDep["compositDep"]-$summaryCompositWith["compositWith"];

							// TO exclude uncleared amounts
/*									$unclearedWithSQL = "SELECT sum(localAmount) as unclearedWith FROM curr_exchange_account WHERE 1 ";
							$unclearedWithSQL .= " and created_at='".$currDate."' AND chequeStatus='UNCLEARED' AND buy_sell='S' and id<".$contentsAcc[$i]["id"];
							$unclearedDepSQL .= $transManualQ;
							$unclearedWith = selectFrom($unclearedWithSQL);
							$unclearedDepSQL = "SELECT sum(localAmount) as unclearedDep FROM curr_exchange_account WHERE 1 ";
//										$unclearedDepSQL .= $transManualQ;
							$unclearedDep = selectFrom($unclearedDepSQL);
							$closingBalanceUncleared = $unclearedDep["unclearedDep"]-$unclearedWith["unclearedWith"];
							if($closingBalanceUncleared!="")
								$closingBalance=$openingBalance+$closingBalanceUncleared;
*/									$openingBalanceSwap = $closingBalance;
						}
					}
					else{
						if($preDate==$currDate){
							$openingBalance =  $openingBalance;
						}
						else{
							$openingBalance =  $closingBalance;	
						}
					}
					$closingBalance =  $openingBalanceSwap;
					$currentAmount = $contentsAcc[$i]["amount"];
					$preDate = $currDate;
				
					$_SESSION["currDate"] = $currDate;
						
					if($contentsAcc[$i]["buy_sell"] == "B")
					{
						$Balance = $Balance+$contentsAcc[$i]["localAmount"];
					}	
					
					if($contentsAcc[$i]["buy_sell"] == "S")
					{
						$Balance = $Balance-$contentsAcc[$i]["localAmount"];
					}
					$localCurrencyQ = selectFrom("SELECT currencyName FROM ".TBL_CURRENCY." WHERE cID='".$contentsAcc[$i]["operationCurrency"]."'");
					$localCurrencyV = strtoupper($localCurrencyQ["currencyName"]);
					$buySellCurrencyQ = selectFrom("SELECT currencyName FROM ".TBL_CURRENCY." WHERE cID='".$contentsAcc[$i]["buysellCurrency"]."'");
					$buySellCurrencyV = strtoupper($buySellCurrencyQ["currencyName"]);
					/////////////////////////Opening and Closing///////////
				//$BeneficiryID = $contentsBen[$i]["benID"];
					?>
				<tr bgcolor="#FFFFFF" title="<?=$contentsAcc[$i]["id"]?>">
					<td width="200" align="left"><strong><font color="#006699"><? 	
					$authoriseDate = $contentsAcc[$i]["created_at"];
//					echo date("F j, Y", strtotime("$authoriseDate"));
					echo date("d/m/Y", strtotime("$authoriseDate"));
					?></font></strong></td>
					<td width="200" align="left">Currency Exchange</td>
<?php /*?>					<td width="200" align="left"><? echo number_format($openingBalance,2,'.',','); ?></td> <?php */?>
					<td width="56">
					<? 
						if($contentsAcc[$i]["buy_sell"] == "S")
							echo $localCurrencyV;
						elseif($contentsAcc[$i]["buy_sell"] == "B")
							echo $buySellCurrencyV;
					?>
					</td>
					<td width="150" align="left"><?  
					if($contentsAcc[$i]["buy_sell"] == "S")
					{
						echo customNumberFormat($contentsAcc[$i]["localAmount"]);
					}
					elseif($contentsAcc[$i]["buy_sell"] == "B")
					{
						echo customNumberFormat($contentsAcc[$i]["totalAmount"]);
					}
					?></td>
					<td width="56">
					<? 
						if($contentsAcc[$i]["buy_sell"] == "S")
							echo $buySellCurrencyV;
						elseif($contentsAcc[$i]["buy_sell"] == "B")
							echo $localCurrencyV;
					?>
					</td>
					<td width="150" align="left"><? 
					if($contentsAcc[$i]["buy_sell"] == "S")
					{
						echo customNumberFormat($contentsAcc[$i]["totalAmount"]);
					}
					elseif($contentsAcc[$i]["buy_sell"] == "B")
					{
						echo customNumberFormat($contentsAcc[$i]["localAmount"]);
					}
					?></td>

					<td width="100">&nbsp;<? echo customNumberFormat($contentsAcc[$i]["rate"])?></td>

				<?
					if($contentsAcc[$i]["buy_sell"] == "S")
					{
						 
					 $closingBalance =  $closingBalance - $contentsAcc[$i]["localAmount"];
					
					}elseif($contentsAcc[$i]["buy_sell"] == "B")
					{
						$closingBalance =  $closingBalance + $contentsAcc[$i]["localAmount"];
						
					}
					 //$_SESSION["closingBalance"] = $closingBalance;
					 //$_SESSION["settleClosingBalance"] = $settleClosingBalance;
					
					 ?>
					   <td width="200" align="left"><? echo(number_format($closingBalance,2,'.',',')); ?></td>
				</tr>
				<?
				}
				?>
				<tr bgcolor="#FFFFFF">
				   <td>&nbsp;</td>
				  <td>&nbsp;</td>
<?php /*?>				   <td>&nbsp;</td><?php */?>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				   <td>&nbsp;</td>
				  <td align="center">&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
            <tr bgcolor="#FFFFFF">
              <td colspan="10">
<!--
/************************************************************************/

Here You will write your Comments or any Information

/************************************************************************/
-->			  
			  </td>

            </tr>
            <tr>
			  <td height="25" nowrap bgcolor="#6699CC" colspan="10">
			  <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
				  <tr>
				  <td align="left"><?php if (count($contentsAcc) > 0) {;?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
					<?=$allCount; ?>
					<?php } ;?>
				  </td>
				  <?php if ($prv >= 0) { ?>
				  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&buySellVal=".$buySellVal."&search=search&total=first";?>"><font color="#005b90">First</font></a> </td>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&currDate=$currDate2&closingBalance=$closingBalance&sortBy=".$accountQuery."&buySellVal=".$buySellVal."&search=search&total=pre";?>"><font color="#005b90">Previous</font></a> </td>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&currDate=$currDate1&closingBalance=$closingBalance&sortBy=".$accountQuery."&buySellVal=".$buySellVal."&search=search&total=next";?>"><font color="#005b90">Next</font></a>&nbsp; </td>
				  <td width="50" align="right">
					  <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&currDate=$currDate&closingBalance=$closingBalance&sortBy=".$accountQuery."&buySellVal=".$buySellVal."&search=search&total=last";?>"><font color="#005b90">Last</font></a>
				  &nbsp; </td>
				  <?php } ?>
				</tr>
				
			  </table></td>
			</tr>
            
            
            
            
			</table>		
            <?
			} // greater than zero
			else
			{
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Date and Agent </font>
				</td>
			</tr>
        	<?
			}
		?>
       </table>
	   
	<?	
	 }else
			{

				$_SESSION["fMonthCurr"]="";
				$_SESSION["fDayCurr"]="";
				$_SESSION["fYearCurr"]="";
				
				$_SESSION["tMonthCurr"]="";
				$_SESSION["tDayCurr"]="";
				$_SESSION["tYearCurr"]="";
			}///End of If Search
	 ?>
	 
	 </td>
	</tr>
	<? if($slctID!=''){?>
		<tr>
		  	<td> 
				<div align="center">
					<input type="button" name="Submit2" value="Print This Report" onClick="print()">
				</div>
			</td>
		  </tr>
		  <? }?>
	</form>
	</td>
	</tr>
</table>
<script language="JavaScript" type="text/javascript" src="tooltip.js"></script>
</body>
</html>