<?
	session_start();
	include ("../include/config.php");
	include ("security.php");
	
	$agentType	= getAgentType();
	$userID 	= $_SESSION["loggedUserData"]["userID"];
	$agentID	= $_SESSION["loggedUserData"]["userID"];
	
	$systemCode = SYSTEM_CODE;
	$company	= COMPANY_NAME;
	$systemPre	= SYSTEM_PRE;
	$manualCode = MANUAL_CODE;	
	
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");

	$response	= new Services_JSON();
	$page		= $_REQUEST['page']; // get the requested page 
	$limit		= $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx		= $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord		= $_REQUEST['sord']; // get the direction 
	
	if(!$sidx)
	{
		$sidx = 1;
	}				
	if($limit == 0)
	{
		$limit=50;
	}
	
if($_REQUEST["getGrid"] == "getAlertList")
{
	if($_REQUEST["Submit"] == "searchAlerts" )
	{
		$id_type_id	= $_REQUEST["id_type_id"];
		$alertType	= $_REQUEST["alertType"];
		$userType	= $_REQUEST["userType"];
		$status	  	= $_REQUEST["status"];
		
		$whrClause	= "";

		if(!empty($id_type_id))
		{
			//if($id_type_id == "All")
			//	$id_type_id = -1;
			$whrClause .= " AND id_type_id = '".$id_type_id."'";
		}
		if(!empty($alertType))
			$whrClause .= " AND alertType = '".$alertType."'";
		if(!empty($userType))
			$whrClause .= " AND userType = '".$userType."'";
		if (!empty($status))
			$whrClause .= " AND isEnable = '".$status."'";
			
	}

	$sql = "SELECT * FROM ".TBL_EMAIL_ALERT." WHERE 1 ";
	if(!empty($whrClause) && !empty($sql))
		$sql	= $sql." ".$whrClause;
	if(!empty($sql)){
		$arrAlertData = mysql_query($sql) or die(__LINE__.": ".mysql_error());
		$intAlertCount = mysql_num_rows($arrAlertData);
		
		if($intAlertCount > 0)
		{
			$total_pages = ceil($intAlertCount / $limit);
		} else {
			$total_pages = 0;
		}
		
		if($page > $total_pages)
		{
			$page = $total_pages;
		}
		
		$start = $limit * $page - $limit; // do not put $limit*($page - 1)
		if($start < 0)
		{
			$start = 0;
		}
		
		$sql .= " order by $sidx $sord LIMIT $start , $limit";
		$arrAlertData = mysql_query($sql) or die(__LINE__.": ".mysql_error());
		$response->page = $page;
		$response->total = $total_pages;
		$response->records = $intAlertCount;
	
		$i=0;

		while($rowT = mysql_fetch_array($arrAlertData))
		{
			$sr = $i+1;
			if($rowT["isEnable"] == "Y")
				$status = "Enable";
			if($rowT["isEnable"] == "N")
				$status = "Disable";
			$response->rows[$i]['id'] = $rowT["id"];
			$response->rows[$i]['cell'] = array(
										$sr,
										$rowT["report"],
										$rowT["userType"],
										$rowT["alertType"],
										$rowT["day"],
										$status,
										$rowT["message"]
									);
			$i++;
		}
		
		echo $response->encode($response); 
	}
	exit;
}

if($_REQUEST["oper"] == "edit")
{
	$id			= $_REQUEST["id"];
	$id_type_id	= $_REQUEST["id_type_id"];
	$alertType	= $_REQUEST["alertType"];
	$userType	= $_REQUEST["userType"];
	$day		= $_REQUEST["day"];
	$status	  	= $_REQUEST["status"];
	$message	= $_REQUEST["message"];
	
	$reportArr = selectFrom("SELECT title FROM id_types WHERE id=".$id_type_id);
	$report	= $reportArr["title"];
	if($id_type_id == -1)
		$report = "All";
	
	$strDupQuery = "	SELECT 
							id 
						FROM 
							".TBL_EMAIL_ALERT." 
						WHERE 
							id_type_id	='".$id_type_id."' 	AND 
							alertType	='".$alertType."' 	AND 
							userType	='".$userType."'
						"; 
	
	$arrID	= selectFrom($strDupQuery);
	if(!empty($arrID["id"]) && $id != $arrID["id"])
	{
		$message = "! Alert Already Exist.";
		$output['status']  = false;
        $output['message'] = $message; 
		echo json_encode($output);
	}
	else
	{
		$strUpdateQuery	= " UPDATE 
								".TBL_EMAIL_ALERT."
							SET
								id_type_id	='".$id_type_id."',
								report		= '".$report."',
								alertType 	='".$alertType."',
								userType 	='".$userType."',
								day 		='".$day."',
								isEnable	='".$status."',
								message 	='".$message."'
							WHERE
								id = '".$id."'
							";
							
		if(mysql_query($strUpdateQuery))
		{
			
			$alertID = $id;		
			////To record in History
			$descript = "Alert update";
			//$backURL .= "&success=Y&act=update";
			activities($_SESSION["loginHistoryID"],"INSERTION",$alertID,TBL_EMAIL_ALERT,$descript);
			
			$message = "Your Alert Data Updated Successfully.";
			$output['status']  = true;
			$output['message'] = $message; 
			echo json_encode($output);
		}
	}
}

if($_REQUEST["oper"] == "del")
{
	if(!empty($_REQUEST["id"]))
	{
		$arrRowId	= explode(",",$_REQUEST["id"]);
		foreach($arrRowId as $key=>$val)
		{	
			$strDeleteQuery	= "DELETE FROM ".TBL_EMAIL_ALERT." WHERE id='".$val."'";
			mysql_query($strDeleteQuery);
		}
		exit;
	}
}


?>