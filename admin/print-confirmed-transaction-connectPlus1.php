<?
session_start();
include ("../include/config.php");


$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$nowTime = date("F j, Y");
$transID = $_GET["transID"];
$today = date("d-m-Y");
$agentType= getAgentType();
if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}

if($_GET["r"] == 'dom')///If transaction is domestic
{
$returnPage = 'add-transaction-domestic.php';
}else{
	$returnPage = $transactionPage;
	}

$buttonValue = $_GET["transSend"];	


if($transID!='')
			{
				$queryTransaction = selectFrom("select *  from ".TBL_TRANSACTIONS." where transID ='" . $transID . "'");
			}
			if($queryTransaction["custAgentID"]!='')
			{
				$querysenderAgent = "select name,username,agentCompany,logo,agentMSBNumber,agentHouseNumber  from ".TBL_ADMIN_USERS." where userID ='" . $queryTransaction["custAgentID"] . "'";
				$custAgent=$queryTransaction["custAgentID"];
			
			


			$senderAgentContent = selectFrom($querysenderAgent);
			
			$custAgentParentID = $senderAgentContent["parentID"];
		}
		$queryCust = "select accountName,customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email,dob,IDType,IDNumber, remarks  from ".TBL_CUSTOMER." where customerID ='" . $queryTransaction["customerID"] . "'";
		$customerContent = selectFrom($queryCust);
		
		$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $queryTransaction["benID"] . "'";
		$benificiaryContent = selectFrom($queryBen);
		
		$benBankDetails = selectFrom("select * from ". TBL_BANK_DETAILS ." where transID='".$transID."'");
		
		 $queryCp = "select *  from cm_collection_point where  cp_id  ='" . $queryTransaction["collectionPointID"] . "'";
			$collectionPoint = selectFrom($queryCp);	
			$queryDistributor = "select name  from " .TBL_ADMIN_USERS. " where  userID  ='" . $collectionPoint["cp_ida_id"] . "'";
			$queryExecute = selectFrom($queryDistributor);


?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">
<!--
.border {
	border-right: 1px solid #000000;
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 1px solid #000000;
}

.labelFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
}
.dataFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	vertical-align: baseline;
}
.headingFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22px;
	font-weight: bold;
	
}
-->
</style>
</head>

<body>
<table width="75%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="headingFont">Connect Plus</td>
    <td class="labelFont">MLR Number:<? echo " ".$senderAgentContent["agentMSBNumber"] ?></td>
  </tr>
  <tr> 
    <td class="dataFont">&nbsp;<?  echo COMPANY_ADDR;  ?></td>
    <td class="labelFont">Company House:<? echo " ".$senderAgentContent["agentHouseNumber"] ?></td>
  </tr>
  <tr> 
    <td  class="dataFont">&nbsp;<? echo CONFIG_COMPANY_PHONE; ?></td>
    <td class="labelFont">&nbsp;</td>
  </tr>
   <tr> 
    <td  class="dataFont">&nbsp;<? echo CONFIG_COMPANY_FAX; ?>-Fax</td>
    <td class="labelFont">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="2" class="dataFont">&nbsp;</td>
  </tr>
</table>


<table width="75%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td width="50%"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="border">
        <tr >
          <td class="dataFont">&nbsp;<b>Sender</b>&nbsp;&nbsp; <? echo " ".$customerContent["accountName"]; ?></td>
          <td class="dataFont">&nbsp;<? echo " ".$customerContent["IDType"]; ?></td>
          <td class="dataFont">&nbsp;<? echo " ".$customerContent["IDNumber"]; ?></td>
        </tr>
        <tr class="dataFont"> 
          <td>&nbsp;<? echo  $customerContent["firstName"]." ".$customerContent["middleName"]." ".$customerContent["lastName"]?>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr class="dataFont"> 
          <td>&nbsp;<? echo $customerContent["Address"]; ?>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr class="dataFont"> 
          <td>&nbsp;<? echo $customerContent["Address1"] . $customerContent["City"] ; ?>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr class="dataFont"> 
          <td>&nbsp;<? echo $customerContent["Phone"]?>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
    <td  rowspan="2" width="50%" height="100%">
<table width="100%" border="0"  class="border" height="100%" cellpadding="3" cellspacing="3">
        <tr class="dataFont"> 
          <td><? echo $senderAgentContent["username"]; ?>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr class="dataFont"> 
          <td>Date/time</td>
          <? $date=$queryTransaction['transDate'];
    					$transDate = substr($queryTransaction['transDate'],0,10); 
    					$transTime = substr($queryTransaction['transDate'],12,19); ?>
          <td><? echo dateFormat($transDate) ?>&nbsp;</td>
          <td><? echo ($transTime) ?>&nbsp;</td>
        </tr>
        <tr class="dataFont"> 
          <td>Invoice:&nbsp;</td>
          <td>&nbsp;</td>
          <td><b><? echo $queryTransaction['refNumberIM']?>&nbsp;</td>
        </tr>
        <tr class="dataFont"> 
          <td>Rate:</td>
          <td>&nbsp;</td>
          <td><? echo $queryTransaction['exchangeRate']?>&nbsp;</td>
        </tr>
        <tr class="dataFont"> 
          <td>&nbsp;</td>
          <td>Net Amount</td>
          <td><? echo $queryTransaction['transAmount']?>&nbsp;</td>
        </tr>
        <tr class="dataFont"> 
          <td>&nbsp;</td>
          <td>Fee:&nbsp;</td>
          <td><? echo $queryTransaction['IMFee']?>&nbsp;</td>
        </tr>
        <tr class="dataFont"> 
          <td>&nbsp;</td>
          <td>Total Due&nbsp;</td>
          <td><b><? echo $queryTransaction['totalAmount']?>&nbsp;</td>
        </tr>
        <tr class="dataFont"> 
          <td colspan="3" align="center">Including commission and exchange rate&nbsp;</td>
         
        </tr>
        <tr class="dataFont"> 
          <td colspan="3" align="center">Amount to pay in: <? echo $queryTransaction['currencyTo'] ?>&nbsp;</td>
         
        </tr>
        <tr class="dataFont"> 
          <td colspan="3" align="center"><b><? echo $queryTransaction['localAmount']." ".$queryTransaction['currencyTo'] ?>&nbsp;</td>
         
        </tr>
        <tr class="dataFont"> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        
      </table></td>
  </tr>
  <tr> 
    <td width="50%"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="border">
        <tr>
          <td class="dataFont">&nbsp;<b>Recipient</b></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="dataFont">&nbsp;<b><? echo $benificiaryContent["firstName"]." ".$benificiaryContent["middleName"]." ".$benificiaryContent["lastName"]; ?>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="dataFont">&nbsp;<? echo $benificiaryContent["CPF"]?>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="dataFont">&nbsp;<? echo $queryTransaction["transType"]; ?>&nbsp;</td>
          <td>&nbsp;</td>
          <td class="dataFont">&nbsp;<? echo $queryTransaction["toCountry"]; ?></td>
        </tr>
        <?  if($queryTransaction["transType"] == "Bank Transfer")
			  { ?>
        <tr>
          <td class="dataFont">&nbsp;<? echo $benBankDetails["bankName"]." ".$benBankDetails["accNo"]; ?>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
          <tr>
          <td class="dataFont">&nbsp;<? echo $benBankDetails["swiftCode"]?>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <? }elseif($queryTransaction["transType"] == "Pick up")
			  { ?>
			  	
			  	 <tr>
          <td class="dataFont">&nbsp;
            <? echo $collectionPoint["cp_branch_name"]." ".$collectionPoint["cp_branch_address"]; ?>
            &nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
			  	  <tr>
          <td class="dataFont">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
			  	
			  	<? } ?>
      
        <tr>
          <td class="dataFont">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="dataFont">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="dataFont">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>

<table width="75%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td colspan="2"  class="dataFont"><? echo CONFIG_TRANS_COND; ?>&nbsp;</td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <br>
  <tr> 
    <td class="dataFont"><center><br>
        &#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212; <br>
        I agree.Your signature 
      </center></td>
    <td class="dataFont"><center><br>&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;<br>CASHIER</center></td>
  </tr>
  <tr>
  	<td class="dataFont" align="center" colspan="2">&nbsp;
  	</td>
  </tr>
  <tr>
  	<td class="dataFont" align="center" colspan="2">
  	<div class='noPrint'>
			<input type="submit" name="Submit2" value="Print this Receipt" onClick="print()">
			&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="style2">Go to Create Transaction</a>
		</div>
	</td>
</tr>
</table>


</body>
</html>

