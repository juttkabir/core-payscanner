<?php
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

/**
 * @Ticket# 3528
 */
include("connectOtherDataBase.php");
/* End #3528 */

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$username  = $_SESSION["loggedUserData"]["username"];
$reportName = stripslashes($_POST["reportName"]);

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
if(!empty($_REQUEST["pageUrl"]))
	maintainReportLogs($_REQUEST["pageUrl"],'E');

//debug ($reportName);
$date_time = date('-d-m-Y-h-i-s-A');
$fileName = trim($username.$date_time);
$cdate = date("d-m-y");
$fileNamed = $reportName."_".$username."_".$date_time;
//debug ($fileNamed);
	header ("Content-type: application/x-msexcel"); 
	header ("Content-Disposition: attachment; filename=$fileNamed.xls" ); 
	header ("Content-Description: PHP/INTERBASE Generated Data" );  


 $totalFields = explode(",",$_POST["totalFields"]);
 $query = stripslashes($_POST["query"]);
 $accountDate = stripslashes($_POST["accountDate"]);
 $transDate = stripslashes($_POST["transDate"]);
 $reportFrom = stripslashes($_POST["reportFrom"]);
  $cancelDate = stripslashes($_POST["cancelDate"]);
$queryDate = stripslashes($_POST["queryDate"]);
//debug ($totalFields);
//debug ($query);
if($query != "")
{

	 count($totalFields) ." QUERY ".$reportFrom;
	$contentsAcc = selectMultiRecords($query);

	$data = "<table width='100%' border='1' cellspacing='0' cellpadding='0' bordercolor='#000000'>"; 
	
	$data .="<tr>";
	foreach($totalFields as $arr) 
	{
			$data .= "<td><font face='Verdana' color='#000000' size='2'><b>$arr</b></font></td>";
	}
	$data .= "</tr>";
	
	require_once("$reportFrom");	
	
	$data.="</table>";
	$len = strlen($data);
	
	echo $data;
}

?>