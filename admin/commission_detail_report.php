<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
//debug ($_REQUEST);
include("connectOtherDataBase.php");

$custAgentID =  $_GET["custAgentID"];
$fromDate =  $_GET["fromDate"];
$toDate = $_GET["toDate"];
$agentID =  $_GET["agentID"];
$transStatus = $_GET["transStatus"];
$currencyTo = $_GET["currencyTo"];
$queryString= $_GET["queryString"];

$agentCurrCommFlag = false;
if(CONFIG_AGENT_CURRENCY_COMMISSION=="1"){
	$agentCurrCommFlag = true;
	$extraExtendedFields = "agentCurrency,agentCurrencyCommission";
}
	
if(CONFIG_CURRENCY_BASED_REPORTS == '1')
{
	$currency = "currencyFrom";
}else{
	$currency = "currencyTo";
	}
	
	
	if(CONFIG_COMMISSION_ON_RETURN == '1')
{
	$cancelEffectOn = "Cancelled - Returned";	
}else{
	$cancelEffectOn = "Cancelled";	
	}

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

if ($offset == "")
	$offset = 0;
$limit=200;

$allCount = 0;

if ($_GET["newOffset"] != "") {
	
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";

if($offset == 0)
{
	$_SESSION["dgrandTotal"]="";
	$_SESSION["dCurrentTotal"]="";	
	
	$_SESSION["dgrandTotal2"]="";
	$_SESSION["dCurrentTotal2"]="";		
	
	$_SESSION["dgrandTotalCompany"]="";
	$_SESSION["dCurrentTotalCompany"]="";
	
	
	$_SESSION["dgrandTotalAgent"]="";
	$_SESSION["dCurrentTotalAgent"]="";	
	
	$_SESSION["dgrandTotalAgentCommission"]="";
	$_SESSION["dCurrentTotalAgentCommission"]="";
}

	$CancelDate = "(t.cancelDate >= '".$_GET["fromDate"]." 00:00:00' and t.cancelDate <= '".$_GET["toDate"]." 23:59:59')";
	$queryDate = "(t.transDate >= '".$_GET["fromDate"]." 00:00:00' and t.transDate <= '".$_GET["toDate"]." 23:59:59')";
	
	$query = "select * from " . TBL_TRANSACTIONS . " as t where t.custAgentID='".$custAgentID."'";
	$queryCnt = "select count(*) from " . TBL_TRANSACTIONS . " as t where t.custAgentID='".$custAgentID."'";
	
	$query .= " and $queryDate";
	$queryCnt .= " and $queryDate";
	
 
 	
 	if($transStatus != "")
	{
		$query .= " and t.transStatus = '".$transStatus."' ";
		$queryCnt .= " and t.transStatus = '".$transStatus."' ";	
	}
 	
  $query .= " and t.$currency like '".$currencyTo."'";
	$queryCnt .= " and t.$currency like '".$currencyTo."'"; 
	
	if($_GET["queryString"] == "commission")
	{
		
			$query2 = "select * from " . TBL_TRANSACTIONS . " as t where t.custAgentID='".$custAgentID."'";
			$queryCnt2 = "select count(*) from " . TBL_TRANSACTIONS . " as t where t.custAgentID='".$custAgentID."'";
	
			$query2 .= " and $CancelDate";
			$queryCnt2 .= " and $CancelDate";
			if($transStatus != "")
			{
				$query2 .= " and t.transStatus = '".$transStatus."' ";
				$queryCnt2 .= " and t.transStatus = '".$transStatus."' ";	
			}
 	
		  $query2 .= " and t.$currency like '".$currencyTo."'";
			$queryCnt2 .= " and t.$currency like '".$currencyTo."'"; 	
					
			$query2 .= " and (t.transStatus ='".$cancelEffectOn."' and t.refundFee ='No')";
			$queryCnt2 .= " and (t.transStatus ='".$cancelEffectOn."' and t.refundFee ='No')"; 
			$query2 .= " LIMIT $offset , $limit";
			$contentsTrans2 = selectMultiRecords($query2);
			$allCount += countRecords($queryCnt2);
			
		if(CONFIG_CANCEL_REVERSE_COMM != '1')
		{
			$query .= " and NOT(t.transStatus ='".$cancelEffectOn."' and (t.refundFee ='Yes' OR t.refundFee = ''))";
			$queryCnt .= " and NOT(t.transStatus ='".$cancelEffectOn."' and (t.refundFee ='Yes' OR t.refundFee = ''))";
		}
	}else{	
		
		$query .= " and (t.transStatus !='Pending' and t.transStatus !='Processing') ";
		$queryCnt .= " and (t.transStatus !='Pending' and t.transStatus !='Processing') ";
	}
		 $exportQuery = $query;
	//if(CONFIG_CANCEL_REVERSE_COMM != '1')
	//{
 	$query .= " LIMIT $offset , $limit";
 		
 //	}
 	
 	$contentsTrans = selectMultiRecords($query);
	$allCount += countRecords($queryCnt);	 
	
	if(CONFIG_CANCEL_REVERSE_COMM == '1')
	{
		$queryCancel = "select * from " . TBL_TRANSACTIONS . " as t where t.custAgentID='".$custAgentID."'";
		$queryCancelCnt = "select count(*) from " . TBL_TRANSACTIONS . " as t where t.custAgentID='".$custAgentID."'";
		$queryCancel .= " and $CancelDate";
		$queryCancelCnt .= " and $CancelDate";
		if($_GET["queryString"] == "commission")
		{
			$queryCancel .= " and (t.transStatus ='".$cancelEffectOn."' and (t.refundFee ='Yes' OR t.refundFee = ''))";
			$queryCancelCnt .= " and (t.transStatus ='".$cancelEffectOn."' and (t.refundFee ='Yes' OR t.refundFee = ''))";
		}else{	
			$queryCancel .= " and (t.transStatus !='Pending' and t.transStatus !='Processing') ";
			$queryCancelCnt .= " and (t.transStatus !='Pending' and t.transStatus !='Processing') ";
		}
	 
	 	
	 	if($transStatus != "")
		{
			$queryCancel .= " and t.transStatus = '".$transStatus."' ";
			$queryCancelCnt .= " and t.transStatus = '".$transStatus."' ";	
		}
	 	
	 		$queryCancel .= " and t.$currency like '".$currencyTo."'";
		 $queryCancelCnt .= " and t.$currency like '".$currencyTo."'"; 	
	// 	$queryCancel .= " LIMIT $offset , $limit";
	 	
	 	$contentsCancelTrans = selectMultiRecords($queryCancel);
		$allCount += countRecords($queryCancelCnt);	 
		
	}
?>
<html>
<head>
<title>View Transaction Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./styles/admin.js"></script>
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="103%" border="0" cellspacing="1" cellpadding="5" >
  <tr> 
    <td class="topbar"><b><font color="#000000" size="2">View transaction details.</font></b></td>
  </tr>
   <?
			if ($allCount > 0){
	?>
	
	<tr>
		<td colspan="11" align="center">
	<form action="commission_detail_report.php" method="post" name="trans">
				<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
          <tr>
            <td width="477">
              <?php if ((count($contentsTrans) + count($contentsCancelTrans) + count($contentsTrans2)) > 0) {;?>
              Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans)+count($contentsCancelTrans)+count($contentsTrans2));?></b>
              of
              <?=$allCount; ?>
              <?php } ;?>
            </td>
            <?php if ($prv >= 0) { ?>
            <td width="50"> <a href="<?php print $PHP_SELF ."?newOffset=0&sortBy=".$_GET["sortBy"]."&total=first&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo&queryString=$queryString";?>"><font color="#005b90">First</font></a>
            </td>
            <td width="53" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$prv&sortBy=".$_GET["sortBy"]."&total=pre&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo&queryString=$queryString"?>"><font color="#005b90">Previous</font></a>
            </td>
            <?php } ?>
            <?php
		if ( ($nxt > 0) && ($nxt < $allCount) ) {
			$alloffset = (ceil($allCount / $limit) - 1) * $limit;
	?>
            <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&total=next&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo&queryString=$queryString";?>"><font color="#005b90">Next</font></a>&nbsp;
            </td>
            <td width="82" align="right"> <!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&total=last";?>"--><font color="#005b90">&nbsp;</font><!--/a-->&nbsp;
            </td>
            <?php }
	   ?>
          </tr>
        </table>		
			</form>  
		</td>
	 </tr> 
<? 
			}
		if(CONFIG_FEE_DEFINED == '1'){
				$value_fee = CONFIG_FEE_NAME.", ";
			}else{ 
				$value_fee = "$systemPre Fee, ";
			}
			
			if(CONFIG_REPORTS_SURCHARG == '1'){
				$value_surcharge = "Surcharges, ";
			}else{
				$value_surcharge = "";
			}
			if(CONFIG_COMMISSION_REPORT_RENAME_SENDER!="0")
				$senderLabel=CONFIG_COMMISSION_REPORT_RENAME_SENDER;
			else
				$senderLabel="Sender Name";
			if(CONFIG_FORMAT_AGENT_COMMISSION_REPORT_EXPORT == "1")
			{
				$values = "S/N,DATE,";
				
				if (CONFIG_SHOW_MANUAL_CODE == '1')
				{
					$values .= $manualCode.",";
				}
				
				
				
				$values .= strtoupper($senderLabel).",SENDER ADDRESS,SENDER PHONE NUMBER,PAYIN BOOK NUMBER,SENDER NUMBER,RECEIVER NAME,RECEIVER ADDRESS,RECEIVER PHONE NUMBER,FOREIGN AMOUNT,EXCHANGE RATE,LOCAL AMOUNT,".strtoupper($value_fee).__("Agent"). " COMMISSION, $company COMMISSION, ".strtoupper($value_surcharge)."RECEIVER COUNTRY,TRANSACTION TYPE,COLLECTION POINT,BANK NAME,RECEIVER CITY";
			//	debug ($values);
			}else{
				$values = "Date,";
				
				if (CONFIG_SHOW_MANUAL_CODE == '1')
				{
					$values .= $manualCode.",";
				}
				debug($senderLabel);
				$values .= $systemCode.", Agent Name, ".$senderLabel.", Beneficiary Name, Amount Sent, ".$value_fee."Agent Commission, $company Commission, ".$value_surcharge."Status, Pick Up Location";	
			}

			
			
?>


  <tr> 
    <td align="center"><table width="842" border="1" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
        <tr class="style1"> 
					<tr bgcolor="#FFFFFF">
			  <td width="100"><span class="style1">Date</span></td>
			  <? if (CONFIG_SHOW_MANUAL_CODE == '1') {  ?>
				  <td width="75"><span class="style1"><? echo $manualCode;?></span></td>
			  <? } ?>
			  <td width="75"><span class="style1"><? echo $systemCode;?></span></td>
			  <td width="100"><span class="style1"><?=__("Agent")?> Name </span></td>
			  <td width="100"><span class="style1"><?=$senderLabel?> </span></td>
			  <td width="100"><span class="style1">Beneficiary Name </span></td>
			  <? if(CONFIG_AGENT_COMMISSION_REPORT == "1"){  ?>
			  <td width="75"><span class="style1">Transaction Amount</span></td>
			  <? }else{ ?>
			  <td width="75"><span class="style1">Amount Sent</span></td>
			  <? }if(CONFIG_AGENT_COMMISSION_REPORT == "1"){ ?>
			  <td width="75"><span class="style1">Com</span></td>
			  <? }else{ 
			  if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1")) {
			  ?>
			  <td width="60" bgcolor="#FFFFFF"><span class="style1"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></span></td>
			  <? } 
			  }
			  ?>
		  <?php if($agentCurrCommFlag){ ?>
			  <td width="75"><span class="style1"><?=__("Agent");?> Commission</span></td>
			  <td width="75"><span class="style1"><?=__("Agent");?> Currency Commission</span></td>
		  <?php }?>
			  <? if(CONFIG_SHOW_AGENT_PROFIT == "1"){ ?>
			  <td width="75"><span class="style1">Agent:Profit Made</span></td>
			  <? } ?>
			  <? if(CONFIG_SHOW_AGENT_LOSS == "1"){ ?>
			  <td width="75"><span class="style1">Agent:Loss Incurred</span></td>
			  <? } 
			  if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1")) {
			  ?>
			  <td width="75"><span class="style1"><? echo $company;?> Commission</span></td>			  
			  <? }	 if (CONFIG_REPORTS_SURCHARG == '1') {  ?>
			  	<td width="75"><span class="style1">Surcharges</span></td>
				<?	 
				} ?>
			  <td>Status</td>
			  <td>Pick Up Location</td>
        </tr>
        <?
   
		if((count($contentsTrans) + count($contentsCancelTrans) + count($contentsTrans2)) > 0)
		{
		//////////
			$total = 0;
	   	$totalLocal = 0;
	   	$totalFee = 0;
	   	$totalAgent = 0;
		$totalAgentCurrComm = 0;
			//echo count($contentsTrans);
		 for($i=0;$i < count($contentsTrans);$i++)
		{
			if(CONFIG_SHOW_AGENT_PROFIT == "1"){
				if($extraExtendedFields)
					$extraExtendedFields .=",";
				$agentQuery = "select ".$extraExtendedFields."margin from transactionExtended where transID = '".$contentsTrans[$i]['transID']."' and margin > '0'";
			}
			elseif($agentCurrCommFlag){
				$agentQuery = "select ".$extraExtendedFields." from transactionExtended where transID = '".$contentsTrans[$i]['transID']."' ";
			}
			if(!empty($agentQuery))
				$agentProfit = selectFrom($agentQuery);
//debug($agentProfit);
		//debug ($contentsTrans);
		?>
        <tr> 
		<td> &nbsp; 
            <?  echo $contentsTrans[$i]["transDate"];  ?>
          </td>
		  <? if (CONFIG_SHOW_MANUAL_CODE == '1') {  ?>
          <td> &nbsp; 
            <?  echo $contentsTrans[$i]["refNumber"]; ?>
          </td>
		  <? } ?>
          <td>
           &nbsp; <?  echo $contentsTrans[$i]["refNumberIM"]; ?>
          </td>
          <td>
           &nbsp; <? $Collection = selectFrom("select name,agentCompany,username from admin where userID='".$contentsTrans[$i]["custAgentID"]."'");
		 //  debug ($Collection); 
           echo $Collection["agentCompany"];?>
          </td>
          <td>
           &nbsp; <? 
           
           					if($contentsTrans[$i]["createdBy"] != 'CUSTOMER'){
		           						if(CONFIG_SHARE_OTHER_NETWORK == '1')
								          {    
								           $sharedTrans = selectFrom("select * from ".TBL_SHARED_TRANSACTIONS." where localTrans = '".$contentsTrans[$i]["transID"]."' and generatedLocally = 'N'");   
								          }
								          
								          if(CONFIG_SHARE_OTHER_NETWORK == '1' && $sharedTrans["remoteTrans"] != '')
													{
														$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$sharedTrans["remoteServerId"]."'");	
														$otherClient = new connectOtherDataBase();
														$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
														
														$CollCustomer = $otherClient->selectOneRecord("select firstName, lastName from ".$jointClient["dataBaseName"].".customer where customerID = '".$contentsTrans[$i]["customerID"]."'");		
														$collben = $otherClient->selectOneRecord("select firstName, lastName, Phone from ".$jointClient["dataBaseName"].".beneficiary where benID = '".$contentsTrans[$i]["benID"]."'");		
														$otherClient->closeConnection();
														dbConnect();
													}else{
															$CollCustomer = selectFrom("select firstName, lastName from customer where customerID='".$contentsTrans[$i]["customerID"]."'");
															$collben = selectFrom("select firstName, lastName, Phone from beneficiary where benID='".$contentsTrans[$i]["benID"]."'"); 	
														}
           						
           						 echo $CollCustomer["firstName"];
           						
           						
           						}elseif($contentsTrans[$i]["createdBy"] == 'CUSTOMER')
           						{
           							$CollCustomer = selectFrom("select FirstName, LastName from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'"); echo $CollCustomer["FirstName"];
           							$collben = selectFrom("select firstName, lastName, Phone from cm_beneficiary where benID='".$contentsTrans[$i]["benID"]."'");
           							}
           				?>
          </td>
          <td>
           &nbsp; <? 
           					if($contentsTrans[$i]["createdBy"] != 'CUSTOMER'){
           						
           					echo $collben["firstName"];
           				}elseif($contentsTrans[$i]["createdBy"] == 'CUSTOMER'){
           				 echo $collben["firstName"];
           				}
           					?>
          </td>
          <td>
           &nbsp; <?  echo number_format($contentsTrans[$i]["transAmount"],2,'.',','); 
           $totalLocal = $totalLocal + $contentsTrans[$i]["transAmount"];
           $total = $total +  $contentsTrans[$i]["localAmount"];
           ?>
          </td>
		<?  if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1")) { ?>
          <td>
           &nbsp; <?  echo number_format($contentsTrans[$i]["IMFee"],2,'.',','); 
           $totalFee = $totalFee + $contentsTrans[$i]["IMFee"];
           ?>
          </td>
		  <? } ?>
          <td>
           &nbsp; <?  echo(number_format($contentsTrans[$i]["AgentComm"],2,'.',',')); 
           $totalAgent = $totalAgent + $contentsTrans[$i]["AgentComm"];
           ?>
          </td>
		  <?php if($agentCurrCommFlag){ ?>
          <td>
           &nbsp; <?  echo(number_format($agentProfit["agentCurrencyCommission"],2,'.',',')." ".$agentProfit["agentCurrency"]); 
           $totalAgentCurrComm = $totalAgentCurrComm + $agentProfit["agentCurrencyCommission"];
           ?>
          </td>
		  <?php }?>
          <? if(CONFIG_SHOW_AGENT_PROFIT == "1"){ ?>
          <td>&nbsp; 
          	
          	<? 
          	
          	echo (number_format($agentProfit["margin"],2,'.',','));
          	$totalProfit = $totalProfit + $agentProfit["margin"];
          	 ?>
          	
          	
          	</td>
          <? } ?>
          
          <? if(CONFIG_SHOW_AGENT_LOSS == "1"){ ?>
          <td>&nbsp; 
          	
          	<? 
          			 $agentLoss = selectFrom("select margin from transactionExtended where transID = '".$contentsTrans[$i]['transID']."' and margin < '0'");
          	
          	echo (number_format($agentLoss["margin"],2,'.',','));
          	$totalLoss = $totalLoss + $agentLoss["margin"];
          	 ?>
          	
          	
          	</td>
          <? }  ?>
        <?  if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1")) { ?>
           <td>
           &nbsp; <?  echo number_format(($contentsTrans[$i]["IMFee"] - $contentsTrans[$i]["AgentComm"]),2,'.',','); ?>
          </td>
          
          <? }

					$surcharges = ( $contentsTrans[$i]["bankCharges"] + $contentsTrans[$i]["cashCharges"] + $contentsTrans[$i]["admincharges"] + $contentsTrans[$i]["outCurrCharges"] ) ;
					$totalSurcharges += $surcharges ;

					?>

					<?	if (CONFIG_REPORTS_SURCHARG == '1') {  ?>
          <td>
           &nbsp; <? echo(number_format($surcharges,2,'.',',')) ; ?>
          </td>
      		<?	}  ?>
          
		  		<td>
           &nbsp; <?  echo $contentsTrans[$i]["transStatus"]; ?>
          </td>
          <? 
          if($contentsTrans[$i]["transType"] == "Pick up"){
          	
          	$pickup = selectFrom("select * from ".TBL_COLLECTION." where cp_id = '".$contentsTrans[$i]["collectionPointID"]."'");
 						$pickupLocation = $pickup["cp_branch_address"]." ".$pickup["cp_city"];
 					
 					}elseif($contentsTrans[$i]["transType"] == "Bank Transfer"){
 					
 						$pickup = selectFrom("select * from ".TBL_BANK_DETAILS." where transID = '".$contentsTrans[$i]["transID"]."'");
 						$pickupLocation = $pickup["branchAddress"];
 					
 					}elseif($contentsTrans[$i]["transType"] == "Home Delivery"){         
 						
 						if($contentsTrans[$i]["createdBy"] != 'CUSTOMER'){
           		
           		$pickup = selectFrom("select * from beneficiary where benID='".$contentsTrans[$i]["benID"]."'"); 
           		           	
           	}elseif($contentsTrans[$i]["createdBy"] == 'CUSTOMER'){
           		
           		$pickup = selectFrom("select * from cm_beneficiary where benID='".$contentsTrans[$i]["benID"]."'"); 
           		           	
           	}
 						
 						$pickupLocation = $pickup["Address"];
 					}
          ?>
           <td>
           &nbsp; <?  echo $pickupLocation; ?>
          </td>
        </tr>
        <?
			}
		?>
		
		<?
		/////////////////////////////Cancelled with No Refund
		
		for($l=0;$l < count($contentsTrans2);$l++)
		{
			
		?>
        <tr> 
		<td> &nbsp; 
		<? //dedug ($contentsTrans2); ?>
            <?  echo $contentsTrans2[$l]["cancelDate"];  ?>
          </td>
		  <? if (CONFIG_SHOW_MANUAL_CODE == '1') {  ?>
          <td> &nbsp; 
            <?  echo $contentsTrans2[$l]["refNumber"]; ?>
          </td>
		  <? } ?>
          <td>
           &nbsp; <?  echo $contentsTrans2[$l]["refNumberIM"]; ?>
          </td>
          <td>
           &nbsp; <? $Collection = selectFrom("select name from admin where userID='".$contentsTrans2[$l]["custAgentID"]."'"); echo $Collection["name"];?>
          </td>
          <td>
           &nbsp; <? 
           
           					if($contentsTrans2[$l]["createdBy"] != 'CUSTOMER'){
           						$CollCustomer = selectFrom("select firstName, lastName from customer where customerID='".$contentsTrans2[$l]["customerID"]."'"); echo $CollCustomer["firstName"];
           						}elseif($contentsTrans2[$l]["createdBy"] == 'CUSTOMER')
           						{
           							$CollCustomer = selectFrom("select FirstName, LastName from cm_customer where c_id='".$contentsTrans2[$l]["customerID"]."'"); echo $CollCustomer["FirstName"];
           							}
           				?>
          </td>
          <td>
           &nbsp; <? 
           					if($contentsTrans2[$l]["createdBy"] != 'CUSTOMER'){
           						
           					$collben = selectFrom("select firstName, lastName, Phone from beneficiary where benID='".$contentsTrans2[$l]["benID"]."'"); echo $collben["firstName"];
           				}elseif($contentsTrans2[$i]["createdBy"] == 'CUSTOMER'){
           				$collben = selectFrom("select firstName, lastName, Phone from cm_beneficiary where benID='".$contentsTrans2[$l]["benID"]."'"); echo $collben["firstName"];
           				}
           					?>
          </td>
          <td>
           &nbsp; -<?  echo number_format($contentsTrans2[$l]["transAmount"],2,'.',','); 
           $totalLocal = $totalLocal - $contentsTrans2[$l]["transAmount"];
           $total = $total -  $contentsTrans2[$l]["localAmount"];
           ?>
          </td>
          <td>
           &nbsp; <?  //echo number_format($contentsTrans2[$l]["IMFee"],2,'.',','); 
           //$totalFee = $totalFee + $contentsTrans2[$l]["IMFee"];
           ?>
          </td>
          <td>
           &nbsp; <? // echo(number_format($contentsTrans2[$l]["AgentComm"],2,'.',',')); 
           //$totalAgent = $totalAgent + $contentsTrans2[$l]["AgentComm"];
           ?>
          </td>
           <td>
           &nbsp; <? // echo($contentsTrans2[$l]["IMFee"] - $contentsTrans2[$l]["AgentComm"]); ?>
          </td>
		  		<?	if (CONFIG_REPORTS_SURCHARG == '1') {  ?>
          <td>
           &nbsp; <? //echo $surcharges ; 
           ?>
          </td>
      		<?	}  ?>
		  		<td>
           &nbsp; <?  echo $contentsTrans2[$l]["transStatus"]; ?>
          </td>
           </td>
          <? 
          if($contentsTrans2[$l]["transType"] == "Pick up"){
          	
          	$pickup = selectFrom("select * from ".TBL_COLLECTION." where cp_id = '".$contentsTrans2[$l]["collectionPointID"]."'");
 						$pickupLocation = $pickup["cp_branch_address"]." ".$pickup["cp_city"];
 					
 					}elseif($contentsTrans2[$l]["transType"] == "Bank Transfer"){
 					
 						$pickup = selectFrom("select * from ".TBL_BANK_DETAILS." where transID = '".$contentsTrans2[$l]["transID"]."'");
 						$pickupLocation = $pickup["branchAddress"];
 					
 					}elseif($contentsTrans2[$l]["transType"] == "Home Delivery"){         
 						
 						if($contentsTrans2[$l]["createdBy"] != 'CUSTOMER'){
           		
           		$pickup = selectFrom("select * from beneficiary where benID='".$contentsTrans2[$l]["benID"]."'"); 
           		           	
           	}elseif($contentsTrans2[$l]["createdBy"] == 'CUSTOMER'){
           		
           		$pickup = selectFrom("select * from cm_beneficiary where benID='".$contentsTrans2[$l]["benID"]."'"); 
           		           	
           	}
 						
 						$pickupLocation = $pickup["Address"];
 					}
 					         
          ?>
           <td>
           &nbsp; <?  echo $pickupLocation; ?>
          </td>
        </tr>
        <?
			}
		?>
		
	<?
	///////////////////	
	if(CONFIG_CANCEL_REVERSE_COMM == '1')
	{
	
		 for($i=0;$i < count($contentsCancelTrans);$i++)
		{
			if(CONFIG_SHOW_AGENT_PROFIT == "1"){
				if($extraExtendedFields)
					$extraExtendedFields .=",";
				$agentQuery = "select ".$extraExtendedFields."margin from transactionExtended where transID = '".$contentsCancelTrans[$i]['transID']."' and margin > '0'";
			}
			elseif($agentCurrCommFlag){
				$agentQuery = "select ".$extraExtendedFields." from transactionExtended where transID = '".$contentsCancelTrans[$i]['transID']."' ";
			}
			if(!empty($agentQuery))
				$agentProfit = selectFrom($agentQuery);		
		?>
        <tr> 
		<td> &nbsp; 
            <?  echo $contentsCancelTrans[$i]["cancelDate"];  ?>
          </td>
		  <? if (CONFIG_SHOW_MANUAL_CODE == '1') {  ?>
          <td> &nbsp; 
            <?  echo $contentsCancelTrans[$i]["refNumber"]; ?>
          </td>
		  <? } ?>
          <td>
           &nbsp; <?  echo $contentsCancelTrans[$i]["refNumberIM"]; ?>
          </td>
          <td>
           &nbsp; <? $Collection = selectFrom("select name from admin where userID='".$contentsCancelTrans[$i]["custAgentID"]."'"); echo $Collection["name"];?>
          </td>
          <td>
           &nbsp; <? 
           
           					if($contentsCancelTrans[$i]["createdBy"] != 'CUSTOMER'){
           						$CollCustomer = selectFrom("select firstName, lastName from customer where customerID='".$contentsCancelTrans[$i]["customerID"]."'"); echo $CollCustomer["firstName"];
           						}elseif($contentsCancelTrans[$i]["createdBy"] == 'CUSTOMER')
           						{
           							$CollCustomer = selectFrom("select FirstName, LastName from cm_customer where c_id='".$contentsCancelTrans[$i]["customerID"]."'"); echo $CollCustomer["FirstName"];
           							}
           				?>
          </td>
          <td>
           &nbsp; <? 
           					if($contentsCancelTrans[$i]["createdBy"] != 'CUSTOMER'){
           						
           					$collben = selectFrom("select firstName, lastName, Phone from beneficiary where benID='".$contentsCancelTrans[$i]["benID"]."'"); echo $collben["firstName"];
           				}elseif($contentsCancelTrans[$i]["createdBy"] == 'CUSTOMER'){
           				$collben = selectFrom("select firstName, lastName, Phone from cm_beneficiary where benID='".$contentsCancelTrans[$i]["benID"]."'"); echo $collben["firstName"];
           				}
           					?>
          </td>
          <td>
           &nbsp; -<?  echo number_format($contentsCancelTrans[$i]["transAmount"],2,'.',','); 
           $totalLocal = $totalLocal - $contentsCancelTrans[$i]["transAmount"];
           $total = $total -  $contentsCancelTrans[$i]["localAmount"];
           ?>
          </td>
		<?  if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1")) { ?>
          <td>
           &nbsp; -<?  echo number_format($contentsCancelTrans[$i]["IMFee"],2,'.',','); 
           $totalFee = $totalFee - $contentsCancelTrans[$i]["IMFee"];
           ?>
          </td>
		 <? } ?>
          <td>
           &nbsp; -<?  echo(number_format($contentsCancelTrans[$i]["AgentComm"],2,'.',',')); 
           $totalAgent = $totalAgent - $contentsCancelTrans[$i]["AgentComm"];
           ?>
          </td>
		  <?php if(agentCurrCommFlag){ ?>
          <td>
           &nbsp; -<?  echo(number_format($agentProfit["agentCurrencyCommission"],2,'.',',')." ".$agentProfit["agentCurrency"]); 
           $totalAgentCurrComm = $totalAgentCurrComm + $agentProfit["agentCurrencyCommission"];
           ?>
          </td>
		  <?php }?>
		  <?		  
		  if(CONFIG_SHOW_AGENT_PROFIT == "1"){ ?>
          <td>&nbsp; 
          	

          	-<? 
			/*
			  This work done by Niaz Ahmad at 06-03-2009 @4616 milexchange. shown - sgin with value
			  calculate  $totalProfit = $totalProfit - $agentProfit["margin"];
			  change contentsTrans to contentsCancelTrans
			*/
          			

          	
          	echo (number_format($agentProfit["margin"],2,'.',','));
          	$totalProfit = $totalProfit - $agentProfit["margin"];
			//$totalProfit = $totalProfit + $agentProfit["margin"];
         
          	 ?>
          	
          	
          	</td>
          <? } 
		  
		  ?>
          
          <? if(CONFIG_SHOW_AGENT_LOSS == "1"){ ?>
          <td>&nbsp;679 
          	
          	<? 
          			 $agentLoss = selectFrom("select margin from transactionExtended where transID = '".$contentsCancelTrans[$i]['transID']."' and margin < '0'");
          	
			$agentLoss2 = -($agentLoss["margin"]);
          	echo (number_format($agentLoss2,2,'.',','));
          	$totalLoss = $totalLoss -($agentLoss["margin"]);
			
       		 ?>
          	
          	
          	</td>
          <? } ?>
		  
		  
		  
           <td>
           &nbsp; -<?  echo(number_format(($contentsCancelTrans[$i]["IMFee"] - $contentsCancelTrans[$i]["AgentComm"]),2,'.',',')); ?>
          </td>
		  		<?

						$surcharges = ( $contentsCancelTrans[$i]["bankCharges"] + $contentsCancelTrans[$i]["cashCharges"] + $contentsCancelTrans[$i]["admincharges"] + $contentsCancelTrans[$i]["outCurrCharges"] ) ;
						$totalSurcharges -= $surcharges ;			
					?>
					<?	if (CONFIG_REPORTS_SURCHARG == '1') {  ?>
          	<td>
           	&nbsp; <? echo number_format($surcharges,2,'.',','); ?>
          	</td>
      		<?	  
			}
			?>
					<td>
           &nbsp; <?  echo $contentsCancelTrans[$i]["transStatus"]; ?>
          </td>
          <? 
          if($contentsCancelTrans[$i]["transType"] == "Pick up"){
          	
          	$pickup = selectFrom("select * from ".TBL_COLLECTION." where cp_id = '".$contentsCancelTrans[$i]["collectionPointID"]."'");
 						$pickupLocation = $pickup["cp_branch_address"]." ".$pickup["cp_city"];
 					
 					}elseif($contentsCancelTrans[$i]["transType"] == "Bank Transfer"){
 					
 						$pickup = selectFrom("select * from ".TBL_BANK_DETAILS." where transID = '".$contentsCancelTrans[$i]["transID"]."'");
 						$pickupLocation = $pickup["branchAddress"];
 					
 					}elseif($contentsCancelTrans[$i]["transType"] == "Home Delivery"){         
 						
 						if($contentsCancelTrans[$i]["createdBy"] != 'CUSTOMER'){
           		
           		$pickup = selectFrom("select * from beneficiary where benID='".$contentsCancelTrans[$i]["benID"]."'"); 
           		           	
           	}elseif($contentsCancelTrans[$i]["createdBy"] == 'CUSTOMER'){
           		
           		$pickup = selectFrom("select * from cm_beneficiary where benID='".$contentsCancelTrans[$i]["benID"]."'"); 
           		           	
           	}
 						
 						$pickupLocation = $pickup["Address"];
 					}         
          ?>
           <td>
           &nbsp; <?  echo $pickupLocation; ?>
          </td>
        </tr>
        <?
			}

	}
	/////////////////////////////Cancel Reverse Entry
	?>	
		
		
		<tr bgcolor="#CCCCCC" class="style1">
		<td>&nbsp;
    	
    </td>	
	<? if (CONFIG_SHOW_MANUAL_CODE == '1') {?>
    <td>&nbsp;</td>	
	<? } ?>
    <td>&nbsp;
    	
    </td>	
    <td>&nbsp;
    	
    </td>	
    <td>&nbsp;
    	
    </td>
    
    <td>
    	&nbsp;Total
    </td>	
    <td>
    	&nbsp; <? echo(number_format($totalLocal,2,'.',',')); ?>
    </td>	
<?  	if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1")) { ?>
	<td>
  		&nbsp; <? echo(number_format($totalFee,2,'.',',')); ?>
    </td>
	<? } ?>
    <td>
    	&nbsp;<? echo(number_format($totalAgent,2,'.',',')); ?>
    </td>	
	<?php if($agentCurrCommFlag){ ?>
    <td>
    	&nbsp;<? echo(number_format($totalAgentCurrComm,2,'.',',')); ?>
    </td>	
	<?php }?>
<? if(CONFIG_SHOW_AGENT_PROFIT == "1"){ ?>   
    <td>
    	&nbsp; <? echo(number_format($totalProfit,2,'.',',')); ?>
    </td>
    <? } 
	
	?>
    
    <? if(CONFIG_SHOW_AGENT_LOSS == "1"){ ?>   
    <td>
    	&nbsp; <? echo(number_format($totalLoss,2,'.',',')); ?>
    </td>
    <? } 
	if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1")) {
	?>    
    
    <td>
    	&nbsp;<? echo(number_format(($totalFee - $totalAgent),2,'.',',')); ?>
    </td>
    <?	}
	
	if (CONFIG_REPORTS_SURCHARG == '1') {  ?>
    <td>
    	&nbsp;<? echo number_format($totalSurcharges,2,'.',','); ?>
    </td>
  	<?	  
	}
	?>
    <td>
    	&nbsp;(Local Amount)
    </td>	
    <td>
    	&nbsp;<?  echo(number_format($total,2,'.',',')); ?>
    </td>	
    
  </tr>
  <tr>
			<td border="0" bordercolor="#FFFFFF" colspan="13">&nbsp;
				
			</td>
		</tr>
		
		
		
		
		
		<tr bgcolor="#CCCCCC" class="style1">
		<? if (CONFIG_SHOW_MANUAL_CODE == '1') {  ?>
		<td>&nbsp;</td>	
		<? } ?>
    <td>&nbsp;
    	
    </td>	
    <td>&nbsp;
    	
    </td>	
    <td>&nbsp;
    	
    </td>	
    <td>&nbsp;
    	
    </td>
    <td>
    	&nbsp; Grand Total
    </td>	
  	<td>
  		<?
				if($_GET["total"]=="first")
				{
				$_SESSION["dgrandTotal"] = $totalLocal;
				$_SESSION["dCurrentTotal"]=$totalLocal;
				}elseif($_GET["total"]=="pre"){
					$_SESSION["dgrandTotal"] -= $_SESSION["dCurrentTotal"];			
					$_SESSION["dCurrentTotal"]=$totalLocal;
				}elseif($_GET["total"]=="next"){
					$_SESSION["dgrandTotal"] += $totalLocal;
					$_SESSION["dCurrentTotal"]= $totalLocal;
				}elseif($_GET["total"]=="")
					{
					$_SESSION["dgrandTotal"]=$totalLocal;
					$_SESSION["dCurrentTotal"]=$_SESSION["dgrandTotal"];
					}
				////For Destination Currency Total
				
				if($_GET["total"]=="first")
				{
				$_SESSION["dgrandTotal2"] = $total;
				$_SESSION["dCurrentTotal2"]=$total;
				}elseif($_GET["total"]=="pre"){
					$_SESSION["dgrandTotal2"] -= $_SESSION["dCurrentTotal2"];			
					$_SESSION["dCurrentTotal2"]=$total;
				}elseif($_GET["total"]=="next"){
					$_SESSION["dgrandTotal2"] += $total;
					$_SESSION["dCurrentTotal2"]= $total;
				}elseif($_GET["total"]=="")
					{
					$_SESSION["dgrandTotal2"]=$total;
					$_SESSION["dCurrentTotal2"]=$_SESSION["dgrandTotal2"];
					}
				
				
				
				if($_GET["total"]=="first")
				{
					$_SESSION["dgrandTotalAgent"] = $totalAgent;
					$_SESSION["dCurrentTotalAgent"]=$totalAgent;
					$_SESSION["dgrandTotalAgentCommission"]=$totalAgentCurrComm;
					$_SESSION["dCurrentTotalAgentCommission"]=$totalAgentCurrComm;
				}elseif($_GET["total"]=="pre"){
					$_SESSION["dgrandTotalAgent"] -= $_SESSION["dCurrentTotalAgent"];			
					$_SESSION["dCurrentTotalAgent"]=$totalAgent;
					$_SESSION["dgrandTotalAgentCommission"]-=$_SESSION["dCurrentTotalAgentCommission"];
					$_SESSION["dCurrentTotalAgentCommission"]=$totalAgentCurrComm;
				}elseif($_GET["total"]=="next"){
					$_SESSION["dgrandTotalAgent"] += $totalAgent;
					$_SESSION["dCurrentTotalAgent"]= $totalAgent;
					$_SESSION["dgrandTotalAgentCommission"]+=$totalAgentCurrComm;
					$_SESSION["dCurrentTotalAgentCommission"]=$totalAgentCurrComm;
				}elseif($_GET["total"]=="")
					{
					$_SESSION["dgrandTotalAgent"]=$totalAgent;
					$_SESSION["dCurrentTotalAgent"]=$_SESSION["dgrandTotalAgent"];
					$_SESSION["dgrandTotalAgentCommission"]=$totalAgentCurrComm;
					$_SESSION["dCurrentTotalAgentCommission"]=$_SESSION["dgrandTotalAgentCommission"];
					}
				////For Destination Currency Total
				$companyTotal = ($totalFee - $totalAgent);
				if($_GET["total"]=="first")
				{
				$_SESSION["dgrandTotalCompany"] = $companyTotal;
				$_SESSION["dCurrentTotalCompany"]=$companyTotal;
				}elseif($_GET["total"]=="pre"){
					$_SESSION["dgrandTotalCompany"] -= $_SESSION["dCurrentTotalCompany"];			
					$_SESSION["dCurrentTotalCompany"]=$companyTotal;
				}elseif($_GET["total"]=="next"){
					$_SESSION["dgrandTotalCompany"] += $companyTotal;
					$_SESSION["dCurrentTotalCompany"]= $companyTotal;
				}elseif($_GET["total"]=="")
					{
					$_SESSION["dgrandTotalCompany"]=$companyTotal;
					$_SESSION["dCurrentTotalCompany"]=$_SESSION["dgrandTotalCompany"];
					}
				
				
				
				?>
			<? if(CONFIG_AGENT_COMMISSION_REPORT_GRANDTOTAL == "1"){?>	
  		 &nbsp; <? echo(number_format($_SESSION["dgrandTotal"]+$totalFee,2,'.',',')); }else{?>
  		 &nbsp; <? echo(number_format($_SESSION["dgrandTotal"],2,'.',',')); }?>
    </td>
    <td>&nbsp;
    	
    </td>	
   
	<td>
    	&nbsp;<? echo(number_format($_SESSION["dgrandTotalAgent"],2,'.',',')); ?>
    </td>	
	<? if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1")) { ?>
	<?php if($agentCurrCommFlag){ ?>
    <td>
    	&nbsp;<? echo(number_format($_SESSION["dgrandTotalAgentCommission"],2,'.',',')); ?>
    </td>
	<?php }
	}
	?>
    <? if(CONFIG_SHOW_AGENT_PROFIT == "1"){ ?>   
    <td>&nbsp;
    	
    </td>
    <? } ?>
    <? if(CONFIG_SHOW_AGENT_LOSS == "1"){ ?>   
    <td>&nbsp;
    	
    </td>
    <? } 
	if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1")) {
	?>
    
    
    <td>
    	&nbsp;<? echo(number_format($_SESSION["dgrandTotalCompany"],2,'.',',')); ?>
    </td>
     <?	} if (CONFIG_REPORTS_SURCHARG == '1') {  ?>
    <td>&nbsp;
    	
    </td>
  	<?	}  ?>
  	<td>&nbsp;
    	
    </td>	
    <td>
    	&nbsp;<?  echo(number_format($_SESSION["dgrandTotal2"],2,'.',',')); ?>
    </td>	
    
  </tr>
		
		
		
		
		
  <? }
  

			?>
      </table></td>
  </tr>
  <tr>
    <td align="center">
	<div align="center">
	<form name="form1" method="post" action="">                  
		<input type="submit" name="Submit2" value="Print This Transaction" onClick="print()">
		<input type="button" name="export" value="Generate Excel" onClick="action='export_excel.php'; submit();">
		<input type="hidden" name="totalFields" value="<?=$values?>">
		<input type="hidden" name="query" value="<?=$exportQuery?>">
		<input type="hidden" name="transDate" value="<?=$queryDate?>">
		<input type="hidden" name="accountDate" value="<?=$CancelDate?>">
		<input type="hidden" name="reportName" value="View-Transaction-Details">
		<input type="hidden" name="reportFrom" value="<? if(CONFIG_FORMAT_AGENT_COMMISSION_REPORT_EXPORT == "1"){echo("export_commission_detail_express.php");}else{echo("export_commission_detail.php");} ?>">
		<input type="hidden" name="custAgentID" value="<?=$custAgentID?>">
		<input type="hidden" name="transStatus" value="<?=$transStatus?>">
		<input type="hidden" name="currencyTo" value="<?=$currencyTo?>">
		<input type="hidden" name="currency" value="<?=$currency?>">
		<input type="hidden" name="queryString" value="<?=$queryString?>">
	</form>
	</div>
	</td>
  </tr>
</table>
</body>
</html>