<?php
	session_start();
	include ("../include/config.php");
	include ("security.php");


	$date_time = date('d-m-Y  h:i:s A');
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	/*
		#5544
		Dropdown for agents is displayed showing all Agents.
		Also ledger will be affected accordingly.
		by A.Shahid
	*/
	$showAgents = false;
	if(CONFIG_USE_PAYPOINT_AGENT_DROPDOWN=="1"){
		$showAgents = true;
	}
	
	if($_REQUEST["get"] == "serviceCategories")
	{
		$strCategories = '';
		$categoryQuery="select categoryID,categoryName from  categoriesPaypoint where serviceID = '".$_REQUEST["serviceID"]."'";
		$categoryRS=selectMultiRecords($categoryQuery);
		if(count($categoryRS)>0){
			$strCategories .= '<option value="">Select Paypoint Category</option>';
			for($c=0;$c<count($categoryRS);$c++)
			{
				$strCategories .= '<option value="'.$categoryRS[$c]["categoryID"].'">'.$categoryRS[$c]["categoryName"].'</option>';
			}
		}else{
			$strCategories .= '<option value="">Select Paypoint Category</option>';
		}
		echo $strCategories;
		exit;
 	}
	$currHeading = "Create Paypoint Transaction.";
/*	$currDefault = "";
	$currDataArr = SelectMultiRecords("select cID,country,currencyName,count(currencyName) as inter_curr 
											from currencies 
											where cID IN 
										(select buysellCurrency 
											from curr_exchange order by id DESC) 
											group by currencyName");
	$buy_rate = "0.0";
	$sell_rate = "0.0";
	$buySellCurrName = "";
	$firstCurrency = $currDataArr[0]['cID'];
	$totalCurrencyName = "GBP";
	if(!empty($firstCurrency)){
		$currRateArr 	 = selectFrom("select * from curr_exchange where buysellCurrency = ".$firstCurrency." order by id DESC");
		$buy_rate = round($currRateArr['buying_rate'],4);
		$sell_rate = round($currRateArr['selling_rate'],4);
		$buySellCurrName = $currDataArr[0]['currencyName'];
	}*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Create Paypoint Transaction</title>
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="jquery.cluetip.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script>
	$(document).ready(function(){

		$("#loading").ajaxStart(function(){
		   $(this).show();
		   $("#storeBuyForm").attr("disabled",true);
		   $("#storeSellForm").attr("disabled",true);
		});
		$("#loading").ajaxComplete(function(request, settings){
		   $(this).hide();
		   $("#storeBuyForm").attr("disabled",false);
		   $("#storeSellForm").attr("disabled",false);
		});
		$("#customer_input").autocomplete('searchCustomer-ajax.php', {
				mustMatch: true,
				maxItemsToShow : -1
			}
		);
		$("#customer_input").result(function(event, data, formatted) {
			if (data)
			{
				//alert(data);
				$("#customerID").val(data[1]);
				$("#customer_input").val(data[0]);
				$("#customerDetails").load("searchCustomer-ajax.php?cusdi="+$("#customerID").val()+"&getCustomerDetails=get");
				$("#customerDetails").show();
				$("#showHideCustomerDetailBtn").show();
			}
		});
		
		$("#showHideCustomerDetailBtn").click(function () {
		 
			if($("#showHideCustomerDetailBtn").html() == "[-]")
			{
				$("#customerDetails").hide();
				$("#showHideCustomerDetailBtn").html("[+]");
			}
			else
			{
				$("#customerDetails").show();
				$("#showHideCustomerDetailBtn").html("[-]");
			}
		});
		$("#newCustomerBtn").click(function(){
			window.open ("/admin/add-customer.php","AddCustomer","location=1,scrollbars=1,width=900,height=450"); 
		});
		
		$('img').cluetip({splitTitle:'|'});
		
		
		/* vaildations of the fields start */

		$("#order_form").validate({
			rules: {
/*				customer_input: "required",
				buysellCurrency: {
					required: true
				},*/
				totalAmountBought: {
					required: true,
					number: true,
					min   :0.0000001
				},
				transAmount: {
					required: true,
					number: true,
					min   :0.0000001
				},
				serviceID: {
					required: true
				}
			},
			messages: {
/*				customer_input: "<br />Please select the customer for this Paypoint Transaction",
				buysellCurrency: "<br />Select the currency to Buy/Sell&nbsp;",*/
				transAmount: {
					required: "<br />Please enter transaction amount",
					number: "<br />Please provide the valid amount.",
					min: "<br />Amount sould be greater than 0"
				},
				serviceID:{
					required: "<br />Please select an Option"
				}
			},
			submitHandler: function(form) {
				var confirmSubmit = confirm("Are you sure to Proceed ?");
				if(confirmSubmit==false){
					return false;
				}
				$("#order_form").ajaxSubmit(function(data) { 
					if(data != "E")
					{
						alert("Paypoint Transaction with Ref # "+data+" has been created successfully.");
						document.location.reload();
/*						var resData  = data.split("|");
						var rid      = resData[0];
						var opCurrId = resData[1];
						var bsCurId  = resData[2];*/
						//$("#order_form").clearForm();
						window.open ("/admin/receipt-paypoint-trans.php?transID="+data,"Paypoint Transaction Receipt","location=0,scrollbars=1,width=350,height=550"); 
					}
					else
						alert("The record could not be saved due to some error."); 
				});
		   }
		});
/*		$("input[@name='manualExchangeRate']").change(function(){
			processManualExhCheck();
		});
		$("#buy_rate").blur()
			if($("input[@name='manualExchangeRate']:checked").val()=="Y"){
				calculateCurrencyAmount(1);
			}
		}
		$("#sell_rate").blur()
			if($("input[@name='manualExchangeRate']:checked").val()=="Y"){
				calculateCurrencyAmount(1);
			}
		}*/
	});
	function roundNumber(rnum, rlength) { // Arguments: number to round, number of decimal places
	  var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
	  // Output the result to the form field (change for your purposes)
	  return newnumber; 
	}
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}
</script>
<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style>
td{ font-family:Verdana, Arial, Helvetica, sans-serif;
font-size:12px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.links {
	background-color:#FFFFFF; 
	font-size:10px; 
	text-decoration:none;
}
</style>
</head>
<body>
<div id="loading" style="display:none; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:12px; color: #FFFFFF; background-color:#FF0000; position:absolute; z-index:1; bottom:0; right:0; ">&nbsp;Loading....&nbsp;</div>
<form name="order_form" id="order_form" action="add-paypoint-transaction-conf.php" method="post">
	<table width="80%" border="0" cellspacing="1" cellpadding="2" align="center">
		<tr>
			<td colspan="5" align="center" bgcolor="#DFE6EA">
				<h2><?=$currHeading?></h2>
			</td>
		</tr>
		<tr bgcolor="#ddedff">
			<td colspan="5" align="center" class="tdDefination" style="text-align:center">
				All fields marked with <font color="red">*</font> are compulsory Fields.
			</td>
		</tr>
<?		 if($showAgents)
		 {
?>
			<tr bgcolor="#ddedff">
				<td width="37%" class="tdDefination"><b>Select an Agent :&nbsp;</b></td>
				<td colspan="4" align="left">
			<?

						/** 
						 * #5544
						 * Default agent for paypint transactions.
						 * CONFIG_USE_PAYPOINT_AGENT_DROPDOWN should also be ON for this.
						 * by Aslam Shahid
						 */ 
						$disabledAgents="";
						$custAgentIDV="";
						if(defined("CONFIG_PAYPOINT_DEFAULT_AGENT_ID") && CONFIG_PAYPOINT_DEFAULT_AGENT_ID!="0"){
							$custAgentIDV = CONFIG_PAYPOINT_DEFAULT_AGENT_ID;
							$disabledAgents = "disabled='disabled'";
						}
					  ?>
					<select name="custAgentIDV" id="custAgentIDV" <?=$disabledAgents;?> onchange="document.getElementById('custAgentID').value=this.value;">
					  <option value="">- Select Agent -</option>
					  <?
					  $agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and (isCorrespondent ='N' OR isCorrespondent ='Y') ";
						$agentQuery .= "order by agentCompany";
						$agents = selectMultiRecords($agentQuery);
						for ($i=0; $i < count($agents); $i++){
					?>
					  <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
					  <?
						}
					?>
					</select>
					<input type="hidden" name="custAgentID" id="custAgentID" value="<?=$custAgentIDV?>" />
					<script language="JavaScript">
				SelectOption(document.forms[0].custAgentIDV, "<?=$custAgentIDV; ?>");
									</script>
	
			</td></tr>
       <? } ?>
		<tr bgcolor="#ddedff">
			<td colspan="5" align="right">
				<a href="javascript:void(0)" id="newCustomerBtn" class="links">[ Add New Customer ]</a>			</td>
		</tr>
		<tr bgcolor="#ddedff">
			<td width="37%" class="tdDefination"><b>Search Customer by Name :&nbsp;</td>
			<td colspan="4" align="left">
				<input type="text" name="customer_input" id="customer_input" size="30" maxlength="150" />
				<input type="hidden" name="customerID" id="customerID" value="" />
				&nbsp;
			<a href="javascript:void(0)" id="showHideCustomerDetailBtn" style="display:none; text-decoration:none" title="Show Hide Customer Details">[-]</a>			</td>
		</tr>
		<tr bgcolor="#ddedff">
			<td colspan="5" id="customerDetails" style="display:none">&nbsp;</td>
		</tr>
		<tr bgcolor="#ddedff">
		<table  width="80%" border="0" cellspacing="1" cellpadding="2" align="center" id="showCurrencyDetails">
		<tr bgcolor="#ddedff">
			<td colspan="3" align="left">
				<fieldset>
					<legend>Transaction Details</legend>
					<table cellpadding="0" cellspacing="0" border="0"  width="100%">
					<tr>
					<td>
					<table align="left" width="100%">
						<tr bgcolor="#ddedff">
							<td width="27%" height="46" class="tdDefination"><b>Service Type :&nbsp;</b><font color="red">*</font></td>
							<td align="left" width="33%">
							<? 
								$contentsService = selectMultiRecords("select serviceID,serviceName from servicesPaypoint order by serviceName");	
								$serviceIdFirst = $contentsService[0]["serviceID"];
								$serviceNameFirst = $contentsService[0]["serviceName"];
							?>
							<select name="serviceID" id="serviceID" onchange='$("#categoryID").load("add-paypoint-transaction.php?get=serviceCategories&serviceID="+this.value);'>
							<option value="">Select Paypoint Service</option>
							<? for($sA=0;$sA<count($contentsService);$sA++){?>
								<option value="<?=$contentsService[$sA]["serviceID"]?>"><?=$contentsService[$sA]["serviceName"]?></option>
							<? }?>
							</select>
							
							&nbsp;
							<img src="images/info.gif" title="Note about Service Selection | You can select the Service and below Categories will load corresponding categories." />							</td>
							</td> 
						</tr>
						<tr bgcolor="#ddedff">
							<td width="27%" height="46" class="tdDefination"><b>Categories for above selected Service :&nbsp;</b></td>
							<td align="left" width="33%">
							<select name="categoryID" id="categoryID">
							<option value="">Select Paypoint Category</option>
							<? 
							$strQueryCat = "SELECT categoryID,categoryName FROM  categoriesPaypoint where serviceID = '".$serviceIdFirst."'  ORDER BY categoryName";
							$strQueryCatR = selectMultiRecords($strQueryCat);

							  for($cr=0;$cr<count($strQueryCatR);$cr++){
									echo "<option  value ='".$strQueryCatR[$cr]["categoryID"]."'> ".$strQueryCatR[$cr]["categoryName"]."</option>";
							  }
							  ?>	
							</select>&nbsp;
							<img src="images/info.gif" title="Note about Category Selection | These are categories related to selected Servie as above." />							</td>
						</tr>
						<tr bgcolor="#ddedff">
							<td width="27%" height="48" class="tdDefination"><b>Amount Paid :&nbsp;</b><font color="red">*</font></td>
							<td align="left" width="33%">
							<input type="text" name="transAmount" id="transAmount" size="20" maxlength="7" />
								<?php /*?><input name="buySellCurrLable3" id="buySellCurrLable3" type="text" style="border:0px; color:#990033; background-color:#ddedff; font-weight:bold; font-size:14px;text-align:right;" size="3" readonly  value="<?=$buySellCurrName?>" /><?php */?>
								<input name="currencyT" id="currencyT" type="text" style="border:0px; color:#339966; background-color:#ddedff; font-weight:bold; font-size:14px;text-align:right;" size="3" readonly  value="<?=$totalCurrencyName?>" />
							</td>
						</tr>
<?php /*?>						<tr bgcolor="#ddedff" valign="top">
							<td width="27%" class="tdDefination"><b>Total Amount : </b><font color="red">*</font></td>
							<td align="left" width="33%">
							<input type="text" name="totalAmount" id="totalAmount" size="20" maxlength="150" value="" readonly/>
								<input name="currency" id="currency" type="text" style="border:0px; color:#339966; background-color:#ddedff; font-weight:bold; font-size:14px;text-align:right;" size="3" readonly  value="<?=$totalCurrencyName?>" />
							</td>
						</tr><?php */?>
						<tr bgcolor="#ddedff" valign="top">
							<td width="27%" class="tdDefination"><strong>Description/Remarks : </strong></td>
							<td align="left" width="33%">
							<textarea name="remarks" id="remarks" rows="3" cols="20"></textarea>
							</td>
						</tr>
					</table>
					</td>
					<td width="20%">
					</td></tr>
					</table>
				</fieldset>			</td>
		</tr>		
		<tr bgcolor="#ddedff">
			<td colspan="5" align="center">
				<input type="submit" id="submitCurrExch" name="submitCurrExch" value=" Process " />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="reset" value="Clear Fields" />
				<input type="hidden" id="action" name="action" value="addBuySell" />
				<input type="hidden" id="storeBuySellForm" name="storeBuySellForm" value="B" />
				<input type="hidden" name="co" value="a" />

			</td>
		</tr>
		</table>
		</tr>
	</table>
</form>
</body>
</html>