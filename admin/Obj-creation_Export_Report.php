<?php
	/** Export Reports Viewed History (Reports Logs)
	 *  @pakage Report
	 *  @subpaakge Report View History
	 *  @author Kalim ul Haq
	 *  copyrigth(c) HBS Technology (Pvt.) Ltd.
	 */
	session_start(); 
	include("../include/config.php");
	include("security.php");
	ini_set("memory_limit","1024M");
	$date_time	= date('d-m-Y  h:i:s');
	$systemCode = SYSTEM_CODE;
	$company 	= COMPANY_NAME;
	$systemPre 	= SYSTEM_PRE; 
	$manualCode = MANUAL_CODE;
	$agentType 	= getAgentType();
	$parentID 	= $_SESSION["loggedUserData"]["userID"];
	
	/**	maintain report logs 
	 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
	 *	If not exist enter it in order to maintain report logs
	 */
	if(!empty($_REQUEST["pageUrl"]))
		maintainReportLogs($_REQUEST["pageUrl"],'E');
		
		if(!empty($_REQUEST["query"]))
			$strQuery	= $_REQUEST["query"];

		$contentsTrans = selectMultiRecords($strQuery);
	
	//debug($contentsTrans);
	
	//export format
	header("Content-type: application/x-msexcel"); 
	header("Content-Disposition: attachment; filename=Object_Creation_Audit_Log.xls" ); 
	header("Content-Description: PHP/INTERBASE Generated Data" );
	
	$strMainStart 	= '<table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#000000">';
	$strMainEnd 	= '</table>';
	$strStrongStart	= '<strong>';
	$strStrongEnd	= '</strong>';
	$strRowStart	= '<tr>';
	$strRowEnd		= '</tr>';
	$strHeadingStart= '<th>';
	$strHeadingEnd	= '</th>';
	$strCellStart	= '<td>';
	$strCellEnd		= '</td>';
	$strCellColSpan	= '<td colspan="6">';
	$strTextCell	= '<td align="left" style=\'mso-number-format:"\@"\'>';
	$strAmountCell	= '<td align="right" style=\'mso-number-format:"\#\,\#\#0\.00"\'>';
	$strRateCell	= '<td align="right" style=\'mso-number-format:"\#\,\#\#0\.0000"\'>';
	$strDateTimeCell= '<td align="left" style=\'mso-number-format:"yyyy\-mm\-dd\ hh\:mm\:ss"\'>';
	$strCountCell	= '<td align="center" style=\'mso-number-format:"0"\'>';
	
	$strExportData	= '';
	
	$strExportData	.= $strMainStart;
	
	$strExportData	.= $strRowStart;
	$strExportData	.= $strCellColSpan.$strStrongStart.'Object Creation Audit Logs'.$strStrongEnd.$strCellEnd;
	$strExportData	.= $strRowEnd;
	//debug($contentsTrans);
	if(count($contentsTrans) > 0)

	{
		$strExportData	.= $strRowStart;
		$strExportData	.= $strHeadingStart.'Action'.$strHeadingEnd;
		$strExportData	.= $strHeadingStart.'Action By'.$strHeadingEnd;
		$strExportData	.= $strHeadingStart.'Action On'.$strHeadingEnd;
		$strExportData	.= $strHeadingStart.'User Type'.$strHeadingEnd;
		$strExportData	.= $strHeadingStart.'Action Date/Time'.$strHeadingEnd;
		$strExportData	.= $strHeadingStart.'IP Address'.$strHeadingEnd;
		$strExportData	.= $strHeadingStart.'Description'.$strHeadingEnd;
		$strExportData	.= $strRowEnd;
			
		foreach($contentsTrans as $key=>$val)
		{
		
		$action_on=explode('.',$val["table_name"]);
	
						$strTableName = $action_on[1];
					if($strTableName== 'admin'){
						$action_on_id= $val["action_for"];
						    $ActionOnSelect="Select * from admin where userID='".$action_on_id."'";
						    $ActionForResponse = selectFrom($ActionOnSelect);
							$ActionForName = $ActionForResponse['username'];
							//echo $ActionForName;
	}
						elseif($strTableName== 'teller')
						{
						$action_on_id= $val["action_for"];
						 $ActionOnSelect="Select * from teller where tellerID='".$action_on_id."'";
							$ActionForResponse = selectFrom($ActionOnSelect);
							$ActionForName = $ActionForResponse['logiName'];
							//echo $ActionForName;
						}
						/* else if($val["login_type"]== "customer"){
						$querActionFor =  mysql_query("SELECT access_ip FROM ".TBL_LOGIN_HISTORY." WHERE custID = ".$val["action_for"]."") ;
						
						$fetchActionFor = mysql_fetch_array($querActionFor);
						} */
			$strExportData	.= $strRowStart;
			$strExportData	.= $strTextCell.$val["activity"].$strCellEnd;
			$strExportData	.= $strTextCell.$val["login_name"].$strCellEnd;
			$strExportData	.= $strTextCell.$val["action_for"].=$strCellEnd; 
			$strExportData	.= $strTextCell.$val["login_type"].$strCellEnd;
			$strExportData	.= $strDateTimeCell.$val["activity_time"].$strCellEnd;
 			if(!empty($val["access_ip"])){
			$strExportData	.= $strTextCell.$val["access_ip"].$strCellEnd;
			}
			else if(empty($val["access_ip"])){
			$strExportData	.= $strTextCell."".$strCellEnd;
			}
			$strExportData	.= $strTextCell.$val["description"].$strCellEnd;
			$strExportData	.= $strRowEnd;
			$ActionForName='';
		}
		
		$strExportData	.= $strRowStart;
		$strExportData	.= $strTextCell.' '.$strCellEnd;
		$strExportData	.= $strTextCell.' '.$strCellEnd;
		$strExportData	.= $strTextCell.' '.$strCellEnd;
		$strExportData	.= $strTextCell.' '.$strCellEnd;
		$strExportData	.= $strTextCell.' '.$strCellEnd;
		$strExportData	.= $strTextCell.' '.$strCellEnd;
		$strExportData	.= $strTextCell.' '.$strCellEnd; 
		$strExportData	.= $strRowEnd;
	}
	else
	{
		$strExportData	.= $strRowStart;
		$strExportData	.= $strTextCell.'No History Found!'.$strCellEnd;
		$strExportData	.= $strRowEnd;
	}
	
	$strExportData	.= $strMainEnd;
	//debug($strExportData);
	echo $strExportData;
