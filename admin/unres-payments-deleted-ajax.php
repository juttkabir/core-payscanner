<?php
session_start();
/**
* @package Payex
* @subpackage Reports
* Short Description
* This page shows the listing of unresolved payments which are deleted from 
* resolve payments screen
*/
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("javaScript.php");
$agentType = getAgentType();

$today = date("d/m/Y");

$modifyby = $_SESSION["loggedUserData"]["userID"];
/*
$currencyies = selectMultiRecords("SELECT DISTINCT(currencyName) FROM currencies");
*/

$barclaysRec = selectMultiRecords("SELECT DISTINCT(accountNo) FROM barclayspayments WHERE accountNo !=''");
?>
<html>
<head>
<title>Unresolved Deleted Payments</title>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee/grid.css" title="coffee" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/grid.celledit.js" type="text/javascript"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var tAmt = 0;
	var lAmt = 0;
	var extraParams;
	jQuery(document).ready(function(){
		var lastSel;
		var maxRows = 10;
		jQuery("#payList").jqGrid({
			url:'unres-payments-deleted.php?type=<?=$_GET["type"];?>&getGrid=payList&nd='+new Date().getTime(),
			datatype: "json",
			height: 270, 
			colNames:[
				'Date', 
				'Bank Account',
				'Original Statement date',
				'CODE', 
				'Amount', 
				'Description'
			],
			colModel:[
				{name:'importedOn',index:'importedOn', width:60, height:30, sorttype:'date', datefmt:'d-m-Y'},
				{name:'accountNo',index:'accountNo', width:50, align:"left"},
				{name:'entryDate',index:'entryDate', width:90, height:30, sorttype:'date', datefmt:'d-m-Y'},
				{name:'tlaCode',index:'tlaCode', width:40, align:"center"},
				{name:'amount',index:'amount', width:50, align:"center",  formatter: 'number'},
				{name:'description',index:'description', width:300, align:"left"}
			],
			imgpath:gridimgpath, 
			rowNum: maxRows,
			rowList: [10,20,50,100,150,200,250,300],
			pager: jQuery('#pagernav'),
//			sortname: 'importedOn',
			viewrecords: true,
			multiselect:true,
			sortorder: "ASC",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			caption: "Unresolved deleted payments"
		
		});
		
		/*
		
			footerrow : true,
			userDataOnFooter: true, 
			altRows: true,
			loadComplete: function() {
            var footerData2 = sumAmountPaid();
            var data = {'TotAL':footerData2};
jQuery("#payList").jqGrid('addRowData','11', data, 'last');
            //jQuery("#payList").footerData('set',footerData2, false);
            alert("Footer data=="+footerData2);
            //var footerRow = jQuery("#payList").footerData('set',footerData, false);
       }
		*/
		function sumAmountPaid() {
			alert("came here");
        // read grid data
        var thedataTemp = jQuery("#payList").getDataIDs();
        var thedata = jQuery("#payList").getRowData(thedataTemp[0]);
        var thesum = 0;
     /* var ii=0;
       var colNames=new Array();
            for (var i in thedata){colNames[ii]=i;
          			alert("First Val"+(colNames[ii]));
          			ii++;
            	}  */
        
        for(var i=0;i < thedataTemp.length; i++) {
        	thedata = jQuery("#payList").getRowData(thedataTemp[i]);
        	thesum += parseFloat(thedata['amount']);
        }
        this.name = 'Total:';
        // format and round the number
        if(thesum == 0) {
            //this.amount = '0.00';
            thesum = '0.00';
        } else {
            thesum = roundNumber(thesum, 2);
        }
        this.amount = thesum;
        alert("sum is "+thesum); 
        return thesum;
      }
    function roundNumber(rnum, rlength) { // Arguments: number to round, number of decimal places
        var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
        return newnumber;
    }
    
			jQuery("#btnActionDel").click( function(){
			if($(this).val()=="Revoke"){
				var payIDs = jQuery("#payList").getGridParam('selarrrow');
				if(payIDs!=''){
					payIDs = payIDs.toString();
					var confirmDel = confirm("Are you sure to Revoke selected Payment Record(s)");
					if(confirmDel==false){
						return false;
					}
					var allPayIdsArr = payIDs.split(",");
					$("#payListDeleteData").load("unres-payments-deleted.php?type="+<?=$_GET['type']?>, { 'payIDs[]': allPayIdsArr,'btnAction': 'Revoke'},		
					 function(){
						alert($("#payListDelete").val());
						gridReload('payList');
					});
				}
				else{
					alert("Please Select Payment(s) to Revoke");
					$("#allPayIdsV").val('');
					return false;
				}
				$("#allPayIdsV").val('');
				gridReload('deletePayList');
			}
		});		
		
	
		
		
		//jQuery('a').cluetip({splitTitle: '|'});
		jQuery(".searchPayObj").each(function(i, val){
			jQuery($(this)).bind('mousedown select keyup', function() {
				gridReload('payList');
			});
		});
	
	});
	function deletePayments(pids){
		var amountPay   =0;
		var pids = pids.toString();
		$("#allPayIdsV").val(pids);
		
	}
    
	
	function gridReload(grid)
	{
		var theUrl = "unres-payments-deleted.php";
		
		if(grid=='payList' || grid=='bothList' || grid=='deletePayList'){
			var extraParam="?type="+<?=$_GET["type"]?>;
			var from = jQuery("#from").val();
			var to = jQuery("#to").val();
			var amount1 = jQuery("#amount1").val();
			var accountNo = jQuery("#accountNo").val();
			var desc = jQuery("#desc").val();
			//var currencyPay1 = jQuery("#currencyPay").val();
			//if(grid=='payList'){
				extraParam = extraParam + "&from="+from+"&to="+to+"&desc="+desc+"&amount1="+amount1+"&accountNo="+accountNo+"&Submit=SearchPay&getGrid=payList";
			/*}
			else{
				extraParam = extraParam + "&getGrid=payList";
			}*/
			//alert("First one==="+extraParam);
			jQuery("#payList").setGridParam({
				url: theUrl+extraParam,
				page:1
			}).trigger("reloadGrid");
		}

	}
	
	function uniqueArr(arrayName)
	{
		var newArray=new Array();
		label:for(var i=0; i<arrayName.length;i++ )
		{  
			for(var j=0; j<newArray.length;j++ )
			{
				if(newArray[j]==arrayName[i]) 
					continue label;
			}
			newArray[newArray.length] = arrayName[i];
		}
		return newArray;
	}
	
</script>
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000}
.style1 {color: #005b90}
.style4 {color: #005b90; font-weight: bold; }
.deleteBtn{float:right;}
.searchObj{}
.linkClass {
	color:#000000;
	font-weight:bold;
	text-decoration:underline;
}
-->
</style>
<script language="javascript">
/*
function clearDates()
{
	document.frmSearch.from.value = "";
	document.frmSearch.to.value = "";
	return true;
}
*/
</script>
</head>
<body>
	<form name="frmSearch" id="frmSearch">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc" colspan="2"><b><strong><font color="#FFFFFF">Unresolved  Barclays Deleted payment</font></strong></b></td>
  </tr>
  <tr>
	<td valign='top'>
	<table  border="0" style="float:left; vertical-align:top" >
     
    <tr>
    <td align="center">
      <table border="1" cellpadding="5" bordercolor="#666666">
      <tr>
            <td nowrap bgcolor="#6699cc"><span class="tab-u"><strong>Payments Search Filter<? echo $from2 . $to;?></strong></span></td>
        </tr>
        <tr>
				
                  <td nowrap align="center">From Date 
                  	<input name="from" type="text" id="from" value="<?=$today?>" readonly class="searchPayObj">
                  	<!--input name="from" type="text" id="from" value="<?=$from1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('frmSearch.from');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp; &nbsp;To Date&nbsp;
		    		    <input name="to" type="text" id="to" value="<?=$today?>" readonly  class="searchPayObj">
		    		    <!--input name="to" type="text" id="to" value="<?=$to1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('frmSearch.to');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
		    		    <!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->
		    		    </td>
                </tr>
        <tr>
            <td nowrap align="center"><!--input name="refNumber" type="text" id="refNumber" value=<?=$refNumber?>-->
		  				Amount 
              <input name="amount1" type="text" id="amount1"  class="searchPayObj">
              <!--input name="amount1" type="text" id="amount1" value=<?=$amount1?>-->
			  &nbsp;&nbsp;&nbsp;
              Description 
              <input name="desc" type="text" id="desc"  class="searchPayObj">
              <!--input name="desc" type="text" id="desc" value=<?=$desc?>-->
		</td>
      </tr>
	   <tr>
           <td>
		      Select Account
              <select name="accountNo" id="accountNo" class="searchPayObj" size="4" multiple="multiple">
				  <option value="">- Select Account -</option>
			  	  <? if(count($barclaysRec)>0){?>
					  <? for($a=0;$a<count($barclaysRec);$a++){?>
					  <option value="<?=trim($barclaysRec[$a]["accountNo"])?>"><?=trim($barclaysRec[$a]["accountNo"])?></option>
					  <? }?>
				  <? }?>
			  </select>
			  &nbsp;&nbsp;&nbsp;
				<input type="hidden" name="type" id="type" value="<?=$_GET["type"];?>">		 
		 </td>
		</tr>
	  <tr>
         <td nowrap align="center">
        
				<input type="hidden" name="type" id="type" value="<?=$_GET["type"];?>">		 
		 </td>
		</tr>
		 <tr><td align="center"><input type="button" name="Submit" value="Search Payments" onClick="gridReload('payList')">
		</td></tr>
    </table>
  	</td>
	</tr>
    
    <tr><td>&nbsp;</td></tr>
	<tr>
    <td valign="top">
    			<table id="payList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernav" class="scroll" style="text-align:center;"></div>
				<div id="payListResolveData" style="visibility:hidden;"></div>
				<div id="payListDeleteData" style="visibility:hidden;"></div>
				<div id="transListCancelData" style="visibility:hidden;"></div>
	</td>
    </tr>
	  <tr bgcolor="#FFFFFF">
		<td align="center">
		<input type='hidden' name='totTrans' id='totTrans'>
		<input type='hidden' name='recOptIds' id='recOptIds' value="">
		<input type="hidden" name="allPayIdsV" id="allPayIdsV" value="">
		<input type="hidden" name="allTransIDsV" id="allTransIDsV" value="">
		<input type="hidden" name="userType" id="userType" value="customer">		
		<input name="btnActionDel" id="btnActionDel" type="button"  value="Revoke"  class="deleteBtn" ></td>
		
	  </tr>
</table>
	</td>
	<td valign='top'>
	<table  border="0" style="float:left; vertical-align:top;margin-top:20px;" >
    
    
    <tr><td>&nbsp;</td></tr>
	<tr>
    <td valign="top">
    			<table id="transList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernavT" class="scroll" style="text-align:center;"></div>
	</td>
    </tr>

</table>

</td>
  </tr>
</table>
</form>
</body>
</html>