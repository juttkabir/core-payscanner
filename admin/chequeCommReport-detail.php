<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include("connectOtherDataBase.php");

$custAgentID =  $_GET["custAgentID"];
$fromDate =  $_GET["from_date"];
$toDate = $_GET["to_date"];
$agentID =  $_GET["creator"];
$queryString= $_GET["queryString"];


$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

if ($offset == "")
	$offset = 0;
$limit=200;

$allCount = 0;

if ($_GET["newOffset"] != "") {
	
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";

	$query = "select order_id, o.cheque_currency, cheque_ref, o.company_id, cheque_no, created_on, estimated_date, paid_on, cheque_amount, paid_amount, status, name, created_by,fee 
				from cheque_order o, company c
				where c.company_id = o.company_id ";
	$queryCnt = "select count(*)
				from cheque_order o, company c
				where c.company_id = o.company_id ";
	$strFilters = "";
	
	if(!empty($_REQUEST["from_date"]))
	{
		$date = explode("/",$_REQUEST["from_date"]);
		$strFilters .= " and created_on >= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 00:00:00'";
	}
	
	if(!empty($_REQUEST["to_date"]))
	{
		$date = explode("/",$_REQUEST["to_date"]);
		$strFilters .= " and created_on <= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 23:59:59'";
	}
	
	if(!empty($_REQUEST["creator"])){
		$creatorQuery = "select userID from admin where username='".$_REQUEST["creator"]."'";
		$arrCreatorDetail = selectFrom($creatorQuery);
		$strFilters .= " and created_by like '".$arrCreatorDetail["userID"]."|%'";
	}

	$query .=  $strFilters;
	$queryCnt .= $strFilters;
	 $exportQuery = $query;
	//if(CONFIG_CANCEL_REVERSE_COMM != '1')
	//{
 	$query .= " LIMIT $offset , $limit";
 		
 //	}
 	
 	$contentsCheque = selectMultiRecords($query);
	$allCount += countRecords($queryCnt);	 

?>
<html>
<head>
<title>View Cheque Commission Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./styles/admin.js"></script>
<style type="text/css">
<!--
.style1 {
	color: #6699CC;
	font-weight: bold;
	font-size:12px;
}
.style2 {
	background-color:lightgoldenRodYellow;
}

-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="103%" border="0" cellspacing="1" cellpadding="5" >
  <tr> 
    <td class="topbar"><b><font color="#000000" size="2">View Cheque Commission details.</font></b></td>
  </tr>
   <?
			if ($allCount > 0){
	?>
	
	<tr>
		<td colspan="11" align="center">
	<form action="commission_detail_report.php" method="post" name="trans">
				<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
          <tr>
            <td width="477">
              <?php if ((count($contentsCheque) + count($contentsCancelTrans) + count($contentsCheque2)) > 0) {;?>
              Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsCheque)+count($contentsCancelTrans)+count($contentsCheque2));?></b>
              of
              <?=$allCount; ?>
              <?php } ;?>
            </td>
            <?php if ($prv >= 0) { ?>
            <td width="50"> <a href="<?php print $PHP_SELF ."?newOffset=0&sortBy=".$_GET["sortBy"]."&total=first&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo&queryString=$queryString";?>"><font color="#005b90">First</font></a>
            </td>
            <td width="53" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$prv&sortBy=".$_GET["sortBy"]."&total=pre&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo&queryString=$queryString"?>"><font color="#005b90">Previous</font></a>
            </td>
            <?php } ?>
            <?php
		if ( ($nxt > 0) && ($nxt < $allCount) ) {
			$alloffset = (ceil($allCount / $limit) - 1) * $limit;
	?>
            <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&total=next&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo&queryString=$queryString";?>"><font color="#005b90">Next</font></a>&nbsp;
            </td>
            <td width="82" align="right"> <!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&total=last";?>"--><font color="#005b90">&nbsp;</font><!--/a-->&nbsp;
            </td>
            <?php }
	   ?>
          </tr>
        </table>		
			</form>  
		</td>
	 </tr> 
<? 
			}
?>


  <tr> 
    <td align="center"><table width="842" border="1" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC"  class="style2"> 
        <tr class="style1"> 
				<tr bgcolor="#FFFFFF"><td colspan="8" align="center" height="50"><span class="style1">Detailed Cheque Commission Report <?=($_REQUEST["creator"]!="" ? "for ".$_REQUEST["creator"]." " : " ")?> <?=(!empty($_REQUEST["from_date"]) && !empty($_REQUEST["to_date"]) ? "From : ".$_REQUEST["from_date"]." To : ".$_REQUEST["to_date"] :"")?> </span></td></tr>
					<tr bgcolor="#FFFFFF">
			  <td width="100"><span class="style1">Date</span></td>
			  <td width="75"><span class="style1">Cheque Reference</span></td>
			  <td width="100"><span class="style1">Cheque Number</span></td>
			  <td width="100"><span class="style1">Client</span></td>
			  <td width="100"><span class="style1">Amount Paid</span></td>
			  <td width="75"><span class="style1">Commission</span></td>
			  <td width="75"><span class="style1">Value</span></td>
			  <td><span class="style1">Status</span></td>
        </tr>
        <?
   
		if(count($contentsCheque) > 0)
		{
		//////////
			$total = 0;
	   	$totalPaidAmount = 0;
	   	$totalFee = 0;
	   	$totalAgent = 0;
			//echo count($contentsCheque);
		 for($i=0;$i < count($contentsCheque);$i++)
			{
			$totalPaidAmount += $contentsCheque[$i]["paid_amount"];
		   	$totalFee += $contentsCheque[$i]["fee"];
			$totalAmount += $contentsCheque[$i]["cheque_amount"];
		?>
        <tr> 
		<td> &nbsp; 
            <? echo $contentsCheque[$i]["created_on"];  ?>
          </td>
          <td> &nbsp; 
            <? echo $contentsCheque[$i]["cheque_ref"]; ?>
          </td>
          <td> &nbsp; 
            <? echo $contentsCheque[$i]["cheque_no"]; ?>
          </td>
          <td>
           &nbsp; <? echo $contentsCheque[$i]["name"]; ?>
          </td>
          <td>
           &nbsp; <? echo (number_format($contentsCheque[$i]["paid_amount"],2,'.',',')); ?>
          </td>
          <td>
           &nbsp; <?  echo (number_format($contentsCheque[$i]["fee"],2,'.',','));
           ?>
          </td>
          <td>
           &nbsp; <?  echo (number_format($contentsCheque[$i]["cheque_amount"],2,'.',','));
           ?>
          </td>
           <td>
           &nbsp; <?  echo $arrChequeStatues[$contentsCheque[$i]["status"]] ?>
          </td>
        </tr>
        <?
			}
		}
		?>
		<tr bgcolor="#CCCCCC" class="style1">
		<td colspan="4">&nbsp;</td>	

    <td  class="style1">
    	&nbsp; <? echo(number_format($totalPaidAmount,2,'.',',')); ?>
    </td>	
  	<td class="style1">
  		&nbsp; <? echo(number_format($totalFee,2,'.',',')); ?>
    </td>
    <td class="style1">
    	&nbsp;<? echo(number_format($totalAmount,2,'.',',')); ?>
    </td>	
	<td>&nbsp;</td>	
  </tr>
  <tr>
			<td border="0" bordercolor="#FFFFFF" colspan="9">&nbsp;
				
			</td>
		</tr>
	  <tr>
    <td align="center" colspan="8">
	<div align="center">
	<form name="form1" method="post" action="">                  
		<input type="submit" name="Submit2" value="Print This Report" onClick="print()">
		<!--<input type="button" name="export" value="Generate Excel" onClick="action='<?=ADMIN_ACCOUNT_URL?>export_excel.php'; submit();">-->
		<input type="hidden" name="queryString" value="<?=$queryString?>">
	</form>
	</div>
	</td>
  </tr>
</table>
</body>
</html>