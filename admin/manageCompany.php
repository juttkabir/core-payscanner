<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];

	/* How many maximum days can be required to clear the cheque */
	$_intMaxDays = 15;
	/* Default Value to be assigned to no of days */
	$_intDefaultDays = 7;


	if($_REQUEST["ajaxCity"] == "get")
	{
		//$("#city").load("manageCompany.php?ajaxCity=get&cntry="+this.value);
	
		$strCities = '';
		if(!empty($_REQUEST["cntry"]))
		{
			$cityQuery="select distinct(city) from  ".TBL_CITIES." where country = '".$_REQUEST["cntry"]."' order by city";
			$cityRS=selectMultiRecords($cityQuery);
			for($c=0;$c<count($cityRS);$c++)
			{
				$strCities .= '<option value="'.$cityRS[$c]["city"].'">'.$cityRS[$c]["city"].'</option>';
			}
		}
		else
		{
			$strCities .= '<option value="">- Select City -</option>';
		}
	
		echo $strCities;
		exit;
	}


	//debug($_REQUEST);
	if(!empty($_REQUEST["saveInputs"]))
	{
		$bolDataSaved = false;
		/* Request to store or update the data of company */
		if(empty($_REQUEST["company_id"]))
		{
			/**
			 * If the Id is empty than the record is new one
			 */
			$strInsertSql = "insert into company 
								(name, address, city, country, zip, phone, fax, email, contact_person, no_of_days)
							 values
								('".$_REQUEST["name"]."','".$_REQUEST["address"]."','".$_REQUEST["city"]."','".$_REQUEST["country"]."',
								 '".$_REQUEST["zip"]."','".$_REQUEST["phone"]."','".$_REQUEST["fax"]."','".$_REQUEST["emailadd"]."',
								 '".$_REQUEST["contact_person"]."', '".$_REQUEST["no_of_days"]."')";
			
			if(mysql_query($strInsertSql))
			{
				$intLastId = mysql_insert_id();
				//echo 1;//|$intLastId;
				/*
				if(!empty($intLastId))
					$msg = "The company <b>".$_REQUEST["name"]."</b> is registered successfully.";
				else
					$msg = "There is some error, Please try again.";
				*/
				$bolDataSaved = true;
			}
			else
				$bolDataSaved = false;
			
		}
		else
		{
			/**
			 * Else the record is not new and required the updation
			 */
			$strUpdateSql = "update company 
							 set
							 	name = '".$_REQUEST["name"]."',
								address = '".$_REQUEST["address"]."',
								city = '".$_REQUEST["city"]."',
								country = '".$_REQUEST["country"]."',
								zip = '".$_REQUEST["zip"]."',
								phone = '".$_REQUEST["phone"]."',
								fax = '".$_REQUEST["fax"]."',
								email = '".$_REQUEST["emailadd"]."',
								contact_person = '".$_REQUEST["contact_person"]."',
								no_of_days = '".$_REQUEST["no_of_days"]."'
							 where
							 	company_id = '".$_REQUEST["company_id"]."'
						"; 
			if(update($strUpdateSql))
				$bolDataSaved = true;
			else
				$bolDataSaved = false;
		}
		
		if($bolDataSaved)
			echo $_REQUEST["company_id"];
		else
			echo "E";

		exit;
	}
	
	
	/**
	 * If the record is for editing 
	 * For this check if the company_id field is empty or not, if not than request will be for editing the record
	 */
	$strMainCaption = "Add New Company Record"; 
	if(!empty($_REQUEST["company_id"]))
	{
		$strGetDataSql = "select * from company where company_id='".$_REQUEST["company_id"]."'";
		$arrCompanyData = selectFrom($strGetDataSql);
		//debug($arrCompanyData);
		
		$name = $arrCompanyData["name"];
		$address = $arrCompanyData["address"];
		$city = $arrCompanyData["city"];
		$country = $arrCompanyData["country"];
		$zip = $arrCompanyData["zip"];
		$phone = $arrCompanyData["phone"];
		$fax = $arrCompanyData["fax"];
		$email = $arrCompanyData["email"];
		$contact_person = $arrCompanyData["contact_person"];
		$no_of_days = $arrCompanyData["no_of_days"];
		
		$strMainCaption = "Modify Existing Company Record"; 
	}
	
	

	/**
	 * Picking up the country and city data to display in the drop down list
	 */
	$arrCountries = selectMultiRecords("select countryname from countries order by countryname");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Manage Company Record</title>
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>

<script>
	$(document).ready(function() {
		$("#submitCompany").validate({
			rules: {
				name: "required",
				country: "required",
				city:  "required",
				emailadd: {
					//required: true,
					email: true
				},
				no_of_days: {
					min: 0
				}
			},
			messages: {
				name: "<br />Please enter full name of company",
				country: "<br />Please select the country from dropdown",
				city: "<br />Please enter the city.",
				emailadd: "<br />Please enter valid email address",
				no_of_days: {
					min: "<br />Minimum number of days cannot be less than 0"
				}
			},
			submitHandler: function(form) {
				
				$("#submitCompany").ajaxSubmit(function(data) { 
	
				var msg = $("#name").val()+" saved successfully! \n Do you want to add more companies information?";
	
				if(data != "E")
				{
					//$("#finalResult").show();
					//alert(msg);
					var cmpid = data;
					window.opener.$('#companyDetails').load("searchCompany.php?cmpdi="+cmpid+"&getCompanyDetails=get",
										{}, function(){
											if(window.opener.$("#post_ex_dt"))
											{
												window.opener.$('#estimated_date').val(window.opener.$("#post_ex_dt").val());
												window.opener.$('#estimated_date').dpSetStartDate(window.opener.$("#post_ex_dt").val());
											}	
										});
					$("#submitCompany").clearForm();
					if(!confirm(msg))
						window.close();
				}
				else
				{
					alert(data);
					alert("The record could not be saved due to error, Please try again."); 
				}
					
			});
		   }
		});
		
		
		<? /* if(!empty($_REQUEST["company_id"])) { ?>
			$.ajax({
			   type: "GET",
			   url: "manageCompany.php",
			   data: "ajaxCity=get&cntry="+$("#country").val(),
			   success: function(msg){
					$("#city").html( msg );
					SelectOption(document.getElementById("city"), "<?=$city?>");
			   }
			 });
		<? }*/ ?>

		
		
		$("#closeWindow").click(function(){
			close();
		});
	});
	
	
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}

	
</script>
<style>
td{ font-family:Verdana, Arial, Helvetica, sans-serif;
size:8px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
</style>
</head>
<body>
<form name="submitCompany" id="submitCompany" action="<?=$_SERVER["PHP_SELF"]?>" method="post">
	<table width="70%" border="0" cellspacing="1" cellpadding="2" align="center">
		<tr>
			<td colspan="4" align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong>
			</td>
		</tr>
		<? if(!empty($msg)) { ?>
			<tr bgcolor="#000000">
				<td colspan="4" align="center" style="text-align:center; color:#EEEEEE; font-size:10px;">
					<img src="images/info.gif" />&nbsp;<?=$msg?>
				</td>
			</tr>
		<? } ?>
		
		<tr bgcolor="#ededed">
			<td colspan="4" align="center" class="tdDefination" style="text-align:center">
				All fields mark with <font color="red">*</font> are compulsory Fields.
			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Company Name:&nbsp;</b><font color="red">*</font></td>
			<td align="left" colspan="3"><input type="text" name="name" id="name" size="100" maxlength="150" value="<?=$name?>" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Address:&nbsp;</b></td>
			<td align="left" colspan="3"><input type="text" name="address" id="address" size="100" maxlength="150" value="<?=$address?>" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Country:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="25%">
				<select name="country" id="country">
					<option value="">Select Country</option>
					<? 
						for($i = 0; $i < count($arrCountries); $i++) 
						{ 
							$strSelected = '';
							if($arrCountries[$i]["countryname"] == $country)
								$strSelected = ' selected="selected"';
					?>
							<option value="<?=$arrCountries[$i]["countryname"]?>" <?=$strSelected?>><?=$arrCountries[$i]["countryname"]?></option>
					<? } ?>
				</select>
			</td>
			<td width="25%" class="tdDefination"><b>City:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="25%">
				<input type="text" name="city" id="city" value="<?=$city?>" />
			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Zip Code:&nbsp;</b></td>
			<td align="left" width="25%"><input type="text" name="zip" id="zip" size="20" maxlength="10" value="<?=$zip?>" /></td>
			<td width="50%" class="tdDefination" colspan="2">&nbsp;</td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Phone:&nbsp;</b></td>
			<td align="left" width="25%"><input type="text" name="phone" id="phone" size="40" maxlength="150" value="<?=$phone?>" /></td>
			<td width="25%" class="tdDefination"><b>Fax:&nbsp;</b></td>
			<td align="left" width="25%"><input type="text" name="fax" id="fax" size="40" maxlength="150" value="<?=$fax?>" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>E-Mail:&nbsp;</b></td>
			<td align="left" width="25%"><input type="text" name="emailadd" id="emailadd" size="40" maxlength="150" value="<?=$email?>" /></td>
			<td width="25%" class="tdDefination"><b>Contact Person:&nbsp;</b></td>
			<td align="left" width="25%"><input type="text" name="contact_person" id="contact_person" size="40" maxlength="150" value="<?=$contact_person?>" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Days Required To Clear Cheque:&nbsp;</b></td>
			<td align="left" width="25%">
				<select name="no_of_days" id="no_of_days">
					<?
						for($days = 0; $days < $_intMaxDays; $days++)
						{
							$strDaysSelected = '';					
							if($no_of_days != "")//if(!empty($no_of_days))	
							{
								if($no_of_days == $days)
									$strDaysSelected = 'selected="selected"';
							}
							elseif($_intDefaultDays == $days)
								$strDaysSelected = 'selected="selected"';
					?>
						<option value="<?=$days?>" <?=$strDaysSelected?>><?=$days?></option>
					<?
						}
					?>
				</select>
			</td>
			<td width="50%" class="tdDefination" colspan="2">&nbsp;</td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="4" align="center">
				<input type="submit" name="storeData" value="Submit" />
				&nbsp;&nbsp;
				<input type="reset" value="Clear Fields" />
				<input type="hidden" name="company_id" value="<?=$_REQUEST["company_id"]?>" />
				<input type="hidden" name="saveInputs" value="y" />
				&nbsp;&nbsp;
				<input type="button" id="closeWindow" value="Close Window" />
			</td>
		</tr>
	</table>
</form>
</body>
</html>