<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$nowTime = getCountryTime(CONFIG_COUNTRY_CODE);
$today = date('d-m-Y  h:i:s A');

if ($_SESSION["benAgentID"] == '') {
	$_SESSION["benAgentID"] = $_POST['distribut'];
}

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}

if(trim($_POST["transType"]) == "Bank Transfer" || trim($_POST["transType"]) == "Home Delivery")
{
		$agentCountry = $_POST["benCountry"];
		$query = "select * from admin where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'N'  and isCorrespondent != ''";				
		if(CONFIG_CITY_SERVICE_ENABLED)
		{
				$benAgentIDs = $_SESSION["distribut"];
				if($benAgentIDs == "0")
				{
					$ida = selectMultiRecords($query);
					for ($j=0; $j < count($ida); $j++)
					{
						$authority = $ida[$j]["authorizedFor"];
						$toCountry = $ida[$j]["IDAcountry"];
						if(strstr($authority,"Home Delivery") && strstr($toCountry,$agentCountry))
						{
							$benAgentIDs =  $ida[$j]["userID"];
				
						}
					}
				}
		}
		else
		{
		$ida = selectMultiRecords($query);
		for ($j=0; $j < count($ida); $j++)
		{
			$authority = $ida[$j]["authorizedFor"];
			$toCountry = $ida[$j]["IDAcountry"];
			if(strstr($authority,"Home Delivery") && strstr($toCountry,$agentCountry))
			{
				$benAgentIDs =  $ida[$j]["userID"];
				
				}
		}
	}
		// if find no IDA against this BenCountry then assign this transaction to AdminIDA 
			if($_POST["transType"] == "Bank Transfer")
			{
				$benAgentIDs = $_POST["distribut"];
				
			}
}
elseif(trim($_POST["transType"]) == "Pick up")
{

		$_SESSION["collectionPoint"] = $_POST["collectionPointID"];
			$queryCust = "select *  from cm_collection_point where  cp_id ='" . $_SESSION["collectionPoint"] . "'";
			$senderAgentContent = selectFrom($queryCust);
			$queryIDA="select * from ".TBL_ADMIN_USERS." where userID='".$senderAgentContent["cp_ida_id"]."'";
			$IDAContent=selectFrom($queryIDA);
			$benAgentIDs = $IDAContent["userID"];			
}

$_SESSION["benAgentID"] = $benAgentIDs;
	
$_SESSION["BenTempcurrencyTo"] =  $_SESSION["currencyTo"];

$agentType = getAgentType();
if($agentType == "SUPA" || $agentType == "SUPAI" || $agentType == "SUBA" || $agentType == "SUBAI")
{
	$queryCust = "select *  from ".TBL_ADMIN_USERS." where username ='$username'";
	$senderAgentContent = selectFrom($queryCust);	
	$_POST["custAgentID"] = $senderAgentContent["userID"];
}

$_SESSION["moneyPaid"] 			= $_POST["moneyPaid"];
$_SESSION["transactionPurpose"] = $_POST["transactionPurpose"];
$_SESSION["other_pur"] 			= $_POST["other_pur"];
$_SESSION["fundSources"] 		= $_POST["fundSources"];
$_SESSION["refNumber"] 			= $_POST["refNumber"];
$_SESSION["discount"]       = $_POST["discount"];
$_SESSION["chDiscount"]     = $_POST["chDiscount"];
$_SESSION["exchangeRate"]     = $_POST["exchangeRate"];
$_SESSION["IMFee"]     = $_POST["IMFee"];

$_SESSION["bankName"] 		= $_POST["bankName"];
$_SESSION["branchCode"] 	= $_POST["branchCode"];
$_SESSION["branchAddress"] 	= trim($_POST["branchAddress"]);
$_SESSION["swiftCode"] 		= $_POST["swiftCode"];
$_SESSION["accNo"] 			= $_POST["accNo"];
$_SESSION["ABACPF"] 		= $_POST["ABACPF"];
$_SESSION["IBAN"] 			= $_POST["IBAN"];
$_SESSION["ibanRemarks"] 			= $_POST["ibanRemarks"];
$_SESSION["currencyTo"]     = $_POST['currencyTo'];
$_SESSION["transType"] = $_POST["transType"];
$_SESSION["documentProvided"] = $_POST["documentProvided"];
$_SESSION["document_remarks"] = $_POST["document_remarks"];

if($_POST["dDate"] != "")
{
	$_SESSION["transDate"] = $_POST["dDate"];
}

$_SESSION["question"] 			= $_POST["question"];
$_SESSION["answer"]     = $_POST["answer"];	
$_SESSION["tip"]     = $_POST["tip"];

$_SESSION["discount_request"] = $_POST["discount_request"];	

if($_SESSION["transType"]=="Bank Transfer")
	{		
		$_SESSION["bankCharges"] 		= $_POST["bankCharges"];
	}
	else
	{
		$_SESSION["bankCharges"]="";
	}

if($_POST["transID"] != "")
{
	$backUrl = "$transactionPage?msg=Y&transID=" . $_POST["transID"];
}
else
{
	$backUrl = "$transactionPage?msg=Y";
}
if(trim($_POST["transType"]) == "")
{
	insertError(TE3);	
	redirect($backUrl);
}

if(trim($_POST["transType"]) == "Pick up")
{
	if(trim($_POST["collectionPointID"]) == "")
	{
		insertError("Please Select Collection Point");	
		redirect($backUrl);
	}
}


if(trim($_POST["customerID"]) == "")
{
	insertError(TE4);	
	redirect($backUrl);
}
if(trim($_POST["distribut"]) == "" && trim($_POST["transType"]) == "Pick up")
{
	insertError(TE5);	
	redirect($backUrl);
}

if(trim($_POST["benID"]) == "")
{
	insertError(TE6);	
	redirect($backUrl);
}



// Validity cheks for Bank Details of Beneficiary.
if(trim($_POST["transType"]) == "Bank Transfer")
{
	if (CONFIG_EURO_TRANS_IBAN == "1" && $_POST["benCountryRegion"] == "European") {
		
		if (trim($_POST["IBAN"]) == "") {
			insertError(TE21);
			redirect($backUrl);	
		}
		
	}	else {
		
		if(trim($_POST["bankName"]) == "") {
			insertError(TE14);	
			redirect($backUrl);
		}
		if(trim($_POST["accNo"]) == "") {
			insertError(TE18);	
			redirect($backUrl);
		}
		if(trim($_POST["branchAddress"]) == "") {
			insertError(TE16);	
			redirect($backUrl);
		}
		if(CONFIG_CPF_ENABLED == '1') {
			if(trim($_POST["ABACPF"]) == "" && strstr(CONFIG_CPF_COUNTRY , '$_POST["benCountry"]')) {
				insertError(TE20);	
				redirect($backUrl);
			}
		}
		
	}
	if(trim($_POST["distribut"]) == "") {
		insertError("Please select a Distributor from the list");	
		redirect($backUrl);
	}
}

if($_POST["notService"] == "Home Delivery")
{
	insertError(TE22);	
	redirect($backUrl);
}

if(trim($_POST["transAmount"]) == "" || $_POST["transAmount"] <= 0)
{
	insertError(TE8);	
	redirect($backUrl);
}

if(trim($_POST["exchangeRate"]) == "" || $_POST["exchangeRate"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["exchangeID"]) == "")
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["IMFee"]) == "" || $_POST["IMFee"] <= 0)
{
	insertError(TE2);	
	redirect($backUrl);
}

if(trim($_POST["localAmount"]) == "" || $_POST["localAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}
if($_POST["benCountry"] == 'Pakistan' || $_POST["benCountry"] == 'India' || $_POST["benCountry"] == 'Poland' || $_POST["benCountry"] == 'Brazil' || $_POST["benCountry"] == 'Philippines')
{

}
else
{
	if($_SESSION["chDiscount"]=="" && (trim($_POST["IMFee"]) == "" || $_POST["IMFee"] <= 0))
	{
		insertError(TE9);	
		redirect($backUrl);
	}
}
if(trim($_POST["totalAmount"]) == "" || $_POST["totalAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["transactionPurpose"]) == "" && CONFIG_NON_COMP_TRANS_PURPOSE != "1")
{
	insertError(TE10);	
	redirect($backUrl);
}

if(trim($_POST["fundSources"]) == "" && CONFIG_NONCOMPUL_FUNDSOURCES != '1')
{
	insertError(TE11);	
	redirect($backUrl);
}

if(trim($_POST["moneyPaid"]) == "")
{
	insertError(TE12);	
	redirect($backUrl);
}


$refNumber = str_replace(" ","",$_POST["refNumber"]);
$_SESSION["refNumber"] = $refNumber;
$_POST["refNumber"] = $refNumber;


if(trim($_POST["Declaration"]) != "Y")
{
	insertError(TE13);	
	redirect($backUrl);
}
	if($_POST["transID"] != "")
		$strRandomQuery = selectFrom("select * from ". TBL_TRANSACTIONS." where transID!='".$_POST["transID"]."' and refNumber = '".$_SESSION["refNumber"]."'");
	else
		$strRandomQuery = selectFrom("SELECT * FROM ". TBL_TRANSACTIONS ." where refNumber = '".$_SESSION["refNumber"]."'");
		  
		
	$oldReference = $strRandomQuery["refNumber"];
	if($oldReference)
	{
	insertError(TE7);	
	redirect($backUrl);
	}
if ($_POST["t_amount"] != "")
	$_SESSION["amount_transactions"] = $_POST["t_amount"];
if ($_POST["l_amount"] != "")
	$_SESSION["amount_left"] = $_POST["l_amount"];
	
if ($_SESSION["amount_left"] == "0")
{
	$_SESSION["amount_transactions"] = "";
	$_SESSION["amount_left"] = "";	
}

$strAddressQuery = selectFrom("select * from ". TBL_ADMIN_USERS." where userID = '".$_SESSION["benAgentID"]."'");
$Admresult = $strAddressQuery;

include ("javaScript.php");
$queryBen = "select * from ".TBL_BENEFICIARY." where benID = '" . $_POST["benID"] . "'";
$benificiaryContent = selectFrom($queryBen);
			
$queryCust = "select * from ".TBL_CUSTOMER." where customerID = '" . $_POST["customerID"] . "'";
$customerContent = selectFrom($queryCust);			

?>
<head>
<link href="styles/opalreceipt.css" rel="stylesheet" type="text/css" />
</head>
<table width="700" border="0" cellspacing="0" cellpadding="0"  align="center">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td rowspan="4">
    <img height="<?=RECEIPT_LOGO_HEIGHT?>" alt="" src="<? echo CONFIG_LOGO?>" width="<?=RECEIPT_LOGO_WIDTH?>" align="top"></p>    </td>
  </tr>
  <tr>
    <td class="headingformat"><span style="border-bottom: 3px double;"><? echo COMPANY_INT_NAME ?></span></td>
  </tr>
  <tr>
    <td class="toptext">Reg. No 4741528 MSB Reg. No 12154036</td>
  </tr>
  <tr>
    <td class="headingformat01"><? echo COMPANY_ADDR ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%"  border="0" cellspacing="0" cellpadding="0" class="text">
      <tr>
        <?
					if ($_POST["dDate"] != "") {
						
					} else {
						
	        	$showdDate1 = explode("-", $nowTime) ;
	        	$showdDate2	= explode(" ", $showdDate1[2]) ;
	        	$showNowTime = $showdDate2[1] ;
	        	$showDay = $showdDate2[0] ;
	        	$showMon = $showdDate1[1] ;
	        	$showYear = $showdDate1[0] ;
	        	$showNowDate = $showDay . "/" . $showMon . "/" . $showYear ;
        	}
        ?>
        <td align="right" class="textformat2">Date &nbsp;&nbsp;<span class="boxformat">
          <? echo ($_POST["dDate"] != "" ? $_POST["dDate"] : $showNowDate) ?></span></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%"  border="2" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%" class="textformat">Sender </td>
        <td width="70%" class="textformat2"><? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></td>
      </tr>
      <tr>
        <td class="textformat2">Document type, number</td>
        <td class="textformat2"><? echo $customerContent["IDType"] . ", " . $customerContent["IDNumber"] ?></td>
      </tr>
      <tr>
        <td class="textformat2">Document valid upto</td>
        <td class="textformat2">
        	<?
        		if ($customerContent["IDExpiry"] != "0000-00-00") {
        			$dDate    = explode("-", $customerContent["IDExpiry"]);
        			$showDate = $dDate[2] . "/" . $dDate[1] . "/" . $dDate[0];
        			echo $showDate;
        		} else {
        			echo "&nbsp;";	
        		}
        	?>
        </td>
      </tr>
      <tr>
        <td class="textformat2">Address, phone number</td>
         <td class="textformat2"><? echo $customerContent["Address"] . " " . $customerContent["Address1"] . ", " . $customerContent["Phone"] ?></td>
      </tr>
   	<?	if ($_SESSION["document_remarks"] != "") {  ?>
      <tr>
        <td class="textformat2">Remarks</td>
        <td class="textformat2"><? echo $_SESSION["document_remarks"] ?></td>
      </tr>
    <?	}  ?>
    </table></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="textformat2">Entrusts company "<? echo COMPANY_NAME ?>" to realize money transfer:</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">
        <table width="100%"  border="2" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%" align="center" class="textformat2">Amount</td>
        <td width="10%" align="center" class="textformat2">Currency</td>
        <td width="60%" align="center" class="textformat2">Amount (written)</td>
      </tr>
      <tr>
      <?
      	if (CONFIG_TRANS_ROUND_NUMBER == "1" && strstr(CONFIG_ROUND_NUM_FOR, $_POST["moneyPaid"].",")) {
      		$localAmount = round($_POST["localAmount"]);	
      	} else if (CONFIG_TRANS_ROUND_NUMBER == "1") {
      		$localAmount = round($_POST["localAmount"], 2);
      	}
      ?>
        <td align="center" class="textformat2"><? echo $localAmount ?></td>
        <td align="center" class="textformat2"><? echo $_POST["currencyTo"] ?></td>
     	<?
     		$currDescQry = "SELECT `description` FROM " . TBL_CURRENCY . " WHERE `currencyName` = '".$_POST["currencyTo"]."'";
     		$currDescRes = selectFrom($currDescQry);
     	?>
        <td align="center" class="textformat2"><? echo number_to_word($localAmount) . " " . $currDescRes["description"]; ?></td>
      </tr>
	  <tr>
        <td rowspan="2" class="textformat">To be paid out in </td>
        <td colspan="2" height="40" class="textformat2">
        	<? echo $Admresult["agentCountry"] . ", " . $Admresult["agentCity"] . ", " . $Admresult["name"] . ", " . $Admresult["agentAddress"] . " " . $Admresult["agentAddress2"] . ", " . $Admresult["agentPhone"] ?>
        </td>
      </tr>
	  <tr>
	    <td colspan="2" align="center" class="textformat3">Country, City, Bank's name, Address, Tel Number</td>
	    </tr>
      <tr>
        <td rowspan="2" class="textformat">To Receiver </td>
        <td colspan="2" height="40" class="textformat2"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
      </tr>
	  <tr>
	    <td colspan="2" align="center" class="textformat3">Surname, Given name, Patronymic (if any), Additional information</td>
	    </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="textformat2">Paid for services </td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%"  border="2" cellspacing="0" cellpadding="0">
  <tr>
    <td height="31" align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" class="textformat2">Currency</td>
    <td align="center" class="textformat2">Amount (written)</td>
  </tr>
   <tr>
   	<?
   		if (CONFIG_TRANS_ROUND_NUMBER == "1" && strstr(CONFIG_ROUND_NUM_FOR, $_POST["moneyPaid"].",")) {
   			$transAmount = round($_POST["transAmount"]);
   			$totalAmount = round($_POST["totalAmount"]);
   		} else if (CONFIG_TRANS_ROUND_NUMBER == "1") {
   			$transAmount = round($_POST["transAmount"], 2);
   			$totalAmount = round($_POST["totalAmount"], 2);
   		}
   	?>
    <td align="center" class="textformat2">Amount: </td>
    <td align="center" class="textformat2"><? echo number_format($transAmount,CONFIG_TRANS_ROUND_LEVEL,'.',','); ?></td>
    <td align="center" class="textformat2"><? echo $_POST["currencyFrom"] ?></td>
     	<?
     		$currDescQry = "SELECT `description` FROM " . TBL_CURRENCY . " WHERE `currencyName` = '".$_POST["currencyFrom"]."'";
     		$currDescRes = selectFrom($currDescQry);
     	?>
    <td rowspan="3" align="center" class="textformat2"><? echo number_to_word($totalAmount) . " " . $currDescRes["description"]; ?></td>
  </tr>
    <tr>
      <td align="center" class="textformat2">Comission: </td>
      <td align="center" class="textformat2"><? echo number_format($_POST["IMFee"],CONFIG_TRANS_ROUND_LEVEL,'.',','); ?></td>
      <td align="center" class="textformat2"><? echo $_POST["currencyFrom"] ?></td>
      </tr>
    <tr>
      <td align="center" class="textformat2">Total: </td>
      <td align="center" class="textformat2"><? echo number_format($totalAmount,CONFIG_TRANS_ROUND_LEVEL,'.',','); ?></td>
      <td align="center" class="textformat2"><? echo $_POST["currencyFrom"] ?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
    <tr>
      <td colspan="2" class="textformat2">This Money Transfer is private financial support and it is not related to any commercial purposes.</td>
    </tr>
    <tr>
    <td colspan="2">&nbsp;</td>
    </tr>
  <tr>
    <td colspan="2" align="center">
        <form name="addTrans" action="add-transaction-conf.php" method="post">
				  <input name="benAgentParentID" type="hidden" value="<? echo $benAgentParentID?>">
				  <input name="custAgentParentID" type="hidden" value="<? echo $custAgentParentID?>">
				  <input type="hidden" name="imReferenceNumber" value="<?=$imReferenceNumber;?>">
				  <input type="hidden" name="refNumber" value="<?=$_SESSION["refNumber"];?>">
				  <input type="hidden" name="mydDate" value="<?=$nowTime;?>">
					<input name="distribut" type="hidden" value="<? echo $_POST["distribut"];?>">				  				  
				  
	  <?
	  		foreach ($_POST as $k=>$v) 
			{
				$hiddenFields .= "<input name=\"" . $k . "\" type=\"hidden\" value=\"". $v ."\">\n";
				$str .= "$k\t\t:$v<br>";
			}
		
echo $hiddenFields;
	  ?>

				<input type="submit" name="Submit" value="Confirm Order">
			&nbsp;&nbsp;&nbsp;<a href="<?=$transactionPage?>?transID=<? echo $_POST["transID"]?>" class="style2">Change Information</a>
            </form>
			</td>
    </tr>
</table></td>
  </tr>
</table>
