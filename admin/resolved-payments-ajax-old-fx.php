<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("javaScript.php");
$agentType = getAgentType();

$today = date("d/m/Y");

$modifyby = $_SESSION["loggedUserData"]["userID"];
$currencyies = selectMultiRecords("SELECT DISTINCT(currencyName) FROM currencies");

$barclaysRec = selectMultiRecords("SELECT DISTINCT(accountNo) FROM barclayspayments WHERE accountNo !=''");
?>
<html>
<head>
<title>Resolved Payments for Item-Wise Reconciliation</title>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee/grid.css" title="coffee" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/grid.celledit.js" type="text/javascript"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var tAmt = 0;
	var lAmt = 0;
	var extraParams;
	var tranactionSource = "payex";
	jQuery(document).ready(function(){
		var lastSel;
		var maxRows = 11;
		
			jQuery("#transList").jqGrid({	
			url:'resolved-payments-action-old.php?getGrid=transList&nd='+new Date().getTime(),
			datatype: "json",
			height: 370, 
			colNames:[
				'Total Amount',
				'City', 
				'Reference Code', 
				'Sender Name',
				'Beneficiary Name',
				'Date',
				'Description',
				'Bank Account',
				'Resolved Date',
				<!--'Amount to Agent',-->
				'Resolved By'
			],
			colModel:[
				{name:'totalAmount',index:'totalAmount', width:60, align:"center"},
				{name:'City',index:'City', width:90, align:"left"},
				{name:'refNumberIM',index:'refNumberIM', width:80, align:"center"},
				{name:'name',index:'name', width:90, align:"left"},
				{name:'beneficiaryname',index:'name', width:90, align:"left"},
				{name:'transDate',index:'transDate', width:70, height:30, sorttype:'date', datefmt:'d-m-Y'},
				{name:'description',index:'description', width:80, align:"left"},
				{name:'bankAccount',index:'bankAccount', width:80, align:"left"},
				{name:'resolvedDate',index:'resolvedDate', width:70, height:30, sorttype:'date', datefmt:'d-m-Y'},
				<!--{name:'agentAmount',index:'agentAmount', width:60, height:30, align:"left"},-->
				{name:'resolvedBy', index:'resolvedBy', width:80, height:30, align:"left"}
			],
			imgpath:gridimgpath, 
			rowNum: maxRows,
			rowList: [10,20,50,100,150,200,250,300],
			pager: jQuery('#pagernavT'),
//			sortname: 'transDate',
			viewrecords: true,
			multiselect:true,
			sortorder: "desc",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			caption: "Transactions",
			hidedlg: true,
			}); 
		/*$grid->navigator = true; 
// Enable excel export 
$grid->setNavOptions('navigator', array("excel"=>true,"add"=>false,"edit"=>false,"del"=>false,"view"=>false)); */

jQuery("#transList").jqGrid('navGrid', '#pager',{view:true, del:false, add:false, edit:false, excel:true})
                .navButtonAdd('#pager',{
                                caption:"Export to Excel", 
                                buttonicon:"ui-icon-save", 
                                onClickButton: function(){ 
                                  exportExcel();
                                }, 
                                position:"last"
                            });


		jQuery("#transList").setColumns();
		$("input:radio").click(function(){
			//alert("this is the alert for"+$(this).val());
					  
				tranactionSource = $(this).val();
					  
				jQuery("#transList").setColumns();
					   
					$("#payListDeleteData").load("resolved-payments-action-old.php?type="+<?=$_GET['type']?>, {'tranactionSource': tranactionSource},
						 function(){
							gridReload('transList');
					});
			});
			jQuery("#btnAddTrans").click( function(){
				if($(this).val() == "Submit")
				{
					var reference = $("#txtTransRef").val();
					var amount = $("#txtTransAmount").val();
					if(reference.length < 1)
					{
						alert("Reference can not be Empty");
					}else{
						//alert(reference+"--Reference ___ amount+"+amount);
						//alert("tranactionSource1"+tranactionSource);
						//&getGrid=transList
						$("#payListDeleteData").load("resolved-payments-action-old.php?type="+<?=$_GET['type']?>, { 'btnAction': 'AddTransaction','amount': amount, 'reference':reference, 'tranactionSource': tranactionSource},
							 function(){
							
							gridReload('transList');
						});
						    $("#txtTransRef").val("");
								$("#txtTransAmount").val("");

					}
				}else{
					//alert("Not Desired Value"+$(this).val());	
				}
			});
		jQuery("#btnActionDel").click( function(){
			if($(this).val()=="Delete"){
				var payIDs = jQuery("#payList").getGridParam('selarrrow');
				if(payIDs!=''){
					payIDs = payIDs.toString();
					var confirmDel = confirm("Are you sure to delete selected Payment Record(s)");
					if(confirmDel==false){
						return false;
					}
					var allPayIdsArr = payIDs.split(",");
					$("#payListDeleteData").load("resolved-payments-action-old.php?type="+<?=$_GET['type']?>, { 'payIDs[]': allPayIdsArr,'btnAction': 'Delete'},
						 function(){
						alert($("#payListDelete").val());
						gridReload('payList');
					});
				}
				else{
					alert("Please Select Payment(s) to delete");
					$("#allPayIdsV").val('');
					return false;
				}
				$("#allPayIdsV").val('');
				gridReload('deletePayList');
			}
		});		
		

		
		jQuery("#btnAction").click( function(){
			if($(this).val()=="Resolve"){
				var payIDs = jQuery("#payList").getGridParam('selarrrow');
				var transIDs = jQuery("#transList").getGridParam('selarrrow');
				if(payIDs!='' && transIDs!=''){
					if(reconcileCheck(payIDs,transIDs)==false){
						return false;
					}
					var allPayIds = $("#allPayIdsV").val();
					var allPayIdsArr = allPayIds.split(",");
					var allTransIDs = $("#allTransIDsV").val();
					var allTransIdsArr = allTransIDs.split(",");
					var userType = $("#userType").val();
					
							var from = jQuery("#from").val();
							var to = jQuery("#to").val();
							var fromT = jQuery("#fromT").val();
							var toT = jQuery("#toT").val();
			


					$("#payListResolveData").load("resolved-payments-action-old.php?type="+<?=$_GET['type']?>, { 'payIDs[]': allPayIdsArr,'transIDs[]': allTransIdsArr,'btnAction': 'Resolve','userTypes': userType, 'from':from, 'to':to, 'fromT':fromT, 'toT':toT},
						 function(){
						//alert($("#payListResolve").val()); Success Message is removed
						gridReload('bothList');
					});
				}
				else{
					alert("Please Select Rows from both sides");
					$("#allPayIdsV").val('');
					$("#allTransIDsV").val('');
					return false;
				}

				$("#userType").val("customer");
				//gridReload('bothList');
			}
		});
		//jQuery('a').cluetip({splitTitle: '|'});
		jQuery(".searchPayObj").each(function(i, val){
			jQuery($(this)).bind('select change keyup', function() {
				gridReload('payList');
			});
		});
		/*mouseup*/
		jQuery(".searchTransObj").each(function(i, val){
			jQuery($(this)).bind('select change keyup', function() {
				gridReload('transList');
			});
		});
	});
	
	function exportAllExcel()
	{
			var theUrl = "resolved-payments-action-old.php";
			var extraParam='';
			var fromT = jQuery("#fromT").val();
			var toT = jQuery("#toT").val();
			var amountT = jQuery("#amountT").val();
			var searchBy = jQuery("#searchType").val();
			var searchName = jQuery("#searchName").val();
			//var currencyTrans1 = jQuery("#currencyTrans").val();
			var paymentMod = jQuery("#paymentMod").val();
			var destCountry = jQuery("#destCountry").val();
			var accountNo = jQuery("#accountNo").val();
			$.post(theUrl, { fromT:fromT, toT:toT, amountT:amountT, searchBy:searchBy, searchName:searchName, paymentMod:paymentMod, destCountry:destCountry, tranactionSource:tranactionSource, accountNo:accountNo, Submit:"SearchTrans", getGrid:"transList", returnType:"export"},
			 function(html){
    				document.testForm.csvBuffer.value=html;
            document.testForm.method='POST';
            document.testForm.action='excelExportGrid.php';  // send it to server which will open this contents in excel file
            document.testForm.target='_blank';
            document.testForm.submit();
 			 });
	}
	
        function exportExcel()
        {
            var mya=new Array();
            mya=jQuery("#transList").getDataIDs();  // Get All IDs
            var data=jQuery("#transList").getRowData(mya[0]);     // Get First row to get the labels
            var colNames=new Array(); 
            var ii=0;
            for (var i in data){colNames[ii++]=i;}    // capture col names
            var html="";
            for(j=0;j<colNames.length;j++)
                    {
                    html=html+colNames[j]+"\t"; // output each column as tab delimited
                    }
                html=html+"\n";
            for(i=0;i<mya.length;i++)
                {
                data=jQuery("#transList").getRowData(mya[i]); // get each row
                for(j=0;j<colNames.length;j++)
                    {
                    html=html+data[colNames[j]]+"\t"; // output each column as tab delimited
                    }
                html=html+"\n";  // output each row with end of line

                }
            html=html+"\n";  // end of line at the end
            html=html.replace(/<br>/gi, ", ");
						html=html.replace(/<p.*>/gi, "\n");
						html=html.replace(/<(?:.|\s)*?>/g, "");
						
            document.testForm.csvBuffer.value=html;
            document.testForm.method='POST';
            document.testForm.action='excelExportGrid.php';  // send it to server which will open this contents in excel file
            document.testForm.target='_blank';
            document.testForm.submit();
        }

	
       
    function reconcileCheck(pids,tids){
		var amountPay   =0;
		var amountTrans =0;
		var pids = pids.toString();
		var tids = tids.toString();
		$("#allPayIdsV").val(pids);
		$("#allTransIDsV").val(tids);
		var allPayIds	=new Array();
		var allTransIds	=new Array();
		//var allPayCurr = new Array();
		//var allTransCurr = new Array();		
		if(pids!=null)
			allPayIds   = pids.split(",");
		if(tids!=null)
			allTransIds = tids.split(",");
			
		for(var p=0;p<allPayIds.length;p++){
			var payG	= jQuery("#payList").getRowData(allPayIds[p]);
			amountPay	= amountPay + parseFloat(payG.amount);
			//allPayCurr[p]  = payG.currency;
		}
		for(var t=0;t<allTransIds.length;t++){
			var transG	= jQuery("#transList").getRowData(allTransIds[t]);
			amountTrans = amountTrans + parseFloat(transG.totalAmount);
			//allTransCurr[t]  = transG.currencyTrans;
		}
		/*for(var c1=0;c1<allTransCurr.length;c1++){
			for(var c2=0;c2<allPayCurr.length;c2++){
				var transCurr = allTransCurr[c1];
				var payCurr = allPayCurr[c2];
				if(transCurr.toString()!=payCurr.toString()){
					alert("Currencies do not match from both sides.Please select same currency Amounts.");
					$("#allPayIdsV").val('');
					$("#allTransIDsV").val('');
					return false;
				}
			}
		}*/
		if(amountPay!=amountTrans){
			alert("Amounts should match.\nTotal Payment Amounts = "+amountPay+"\nTotal Transaction Amount = "+amountTrans+"\nPlease Select again.");
			$("#allPayIdsV").val('');
			$("#allTransIDsV").val('');
			return false;
		}
	}
	
	function gridReload(grid)
	{
		var theUrl = "resolved-payments-action-old.php";
		
		

		if(grid=='transList' || grid=='bothList'){
			var extraParam='';
			var fromT = jQuery("#fromT").val();
			var toT = jQuery("#toT").val();
			var amountT = jQuery("#amountT").val();
			var searchBy = jQuery("#searchType").val();
			var searchName = jQuery("#searchName").val();
			//var currencyTrans1 = jQuery("#currencyTrans").val();
			var paymentMod = jQuery("#paymentMod").val();
			var destCountry = jQuery("#destCountry").val();
			
			var accountNo = jQuery("#accountNo").val();
			//if(grid=='transList'){
				//$("#userType").val(searchBy);
				extraParam = "?fromT="+fromT+"&toT="+toT+"&amountT="+amountT+"&searchBy="+searchBy+"&searchName="+searchName+"&paymentMod="+paymentMod+"&destCountry="+destCountry+"&tranactionSource="+tranactionSource+"&accountNo="+accountNo+"&Submit=SearchTrans&getGrid=transList";
			/*}
			else{
				extraParam = "?getGrid=transList";
			}*/
			//alert("Second one==="+extraParam);
			
			jQuery("#transList").setGridParam({
				url: theUrl+extraParam,
				page:1
			}).trigger("reloadGrid");
			
			
		}
	}
	function pausecomp(millis) 
	{
	var date = new Date();
	var curDate = null;
	
	do { curDate = new Date(); } 
	while(curDate-date < millis);
	} 
	function getTransaction(extraParam2)
	{
		var theUrl = "resolved-payments-action-old.php";
		//pausecomp(5000);
			//alert(theUrl+"---Before Load here---"+extraParam2);
			jQuery("#transList").setGridParam({
				url: theUrl+extraParam2,
				page:1
			}).trigger("reloadGrid");
			//alert("Load here"+suggest);	
	}
	function reconciledOptions(id){
		var ret 	= jQuery("#payListList").getRowData(id);
		var recOptIds  = jQuery("#recOptIds").val();
		
		var recArr = new Array();
		if(recOptIds!=''){
			recArr = recOptIds.split(',');
			//alert('id='+id);
			//alert('Length of Array = '+recArr.length +' index[0]='+recArr[0]+' index[1]='+recArr[1]);
			//alert('Exists = '+recArr.indexOf(id));
			if(recArr.indexOf(id)==-1){
				recArr.push(id);
				recArr.join(',');
				jQuery("#recOptIds").val(recArr);
			}
		}
		else{
			jQuery("#recOptIds").val(id);
		}
		alert('After recOptIds='+jQuery("#recOptIds").val());
		//+" Reconcile="+ret.mark+" Username="+ret.username+" UserType="+ret.userType+" Notes="+ret.notes
	}
	function uniqueArr(arrayName)
	{
		var newArray=new Array();
		label:for(var i=0; i<arrayName.length;i++ )
		{  
			for(var j=0; j<newArray.length;j++ )
			{
				if(newArray[j]==arrayName[i]) 
					continue label;
			}
			newArray[newArray.length] = arrayName[i];
		}
		return newArray;
	}
	
</script>
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000}
.style1 {color: #005b90}
.style4 {color: #005b90; font-weight: bold; }
.deleteBtn{float:right;}
.searchObj{}
.linkClass {
	color:#000000;
	font-weight:bold;
	text-decoration:underline;
}
-->
</style>
<script language="javascript">
/*
function clearDates()
{
	document.frmSearch.from.value = "";
	document.frmSearch.to.value = "";
	return true;
}
*/

</script>
</head>
<body>
	<form name="testForm" id="testForm">
		<input type="hidden" value="" name="csvBuffer" id="csvBuffer" />
	</form>
	<form name="frmSearch" id="frmSearch">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc" colspan="2"><b><strong><font color="#FFFFFF">Resolved  Barclays payment</font></strong></b></td>
  </tr>
  <tr>
	<td valign='top'>
	<table  border="0" style="float:left; vertical-align:top" >
  <tr>
    <td valign="top">
    			<table id="payList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernav" class="scroll" style="text-align:center;"></div>
				<div id="payListResolveData" style="visibility:hidden;"></div>
				<div id="payListDeleteData" style="visibility:hidden;"></div>
				<div id="transListCancelData" style="visibility:hidden;"></div>
	</td>
    </tr>
	  <tr bgcolor="#FFFFFF">
		<td align="center">
		<input type='hidden' name='totTrans' id='totTrans'>
		<input type='hidden' name='recOptIds' id='recOptIds' value="">
		<input type="hidden" name="allPayIdsV" id="allPayIdsV" value="">
		<input type="hidden" name="allTransIDsV" id="allTransIDsV" value="">
		<input type="hidden" name="userType" id="userType" value="customer">		
		
	  </tr>
</table>
	</td>
	<td valign='top'>
	<table  border="0" style="float:left; vertical-align:top;margin-top:20px;" >
    <tr>
    <td align="center" width="100%">
      <table border="1" cellpadding="5" bordercolor="#666666" width="80%" align="center" id="searchFilters">
      <tr>
            <td nowrap bgcolor="#6699cc"><span class="tab-u"><strong>Transactions Search Filter</strong></span></td>
        </tr>
        <tr>
		  <td nowrap align="center">From Date 
				<input name="fromT" type="text" id="fromT" value="<?=$today?>" readonly  class="searchTransObj">
				&nbsp;<a href="javascript:show_calendar('frmSearch.fromT');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp; &nbsp;To Date&nbsp;
				<input name="toT" type="text" id="toT" value="<?=$today?>" readonly  class="searchTransObj">
				&nbsp;<a href="javascript:show_calendar('frmSearch.toT');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
				<!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->
			</td>
        </tr>
        <tr>
	      <td nowrap align="center"> 
              Search Type
              <select name="searchType" id="searchType" class="searchTransObj">
				  <option value="customer">Transaction</option>
				  <option value="agent">Agent</option>
				  <option value="distributor">Distributor</option>
			  </select>
			  
			  &nbsp;&nbsp;&nbsp;Reference Number/Username 
              <input name="searchName" type="text" id="searchName" class="searchTransObj">
		</td>
      </tr>
      <tr>
	      <td nowrap align="center"> 
	      	  Destination Countries	<SELECT name="destCountry" id="destCountry" multiple size="3" style="font-family:verdana; font-size: 11px" class="searchTransObj">
            <OPTION value="">- Select Country-</OPTION>
          
	      	  	<?
	      	  		$countryTypes = " and  countryType like '%destination%' ";
		          	if(CONFIG_COUNTRY_SERVICES_ENABLED){
									$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = toCountryId  $countryTypes order by countryName");
								}
								else
								{
									$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
								}
								for ($i=0; $i < count($countires); $i++){
							?>
			            <OPTION value="<? echo("'".$countires[$i]["countryName"]."'"); ?>">
			            <?=$countires[$i]["countryName"]; ?>
			            </OPTION>
			            <?
								}
							?>
			          </SELECT>
              Payment Mode
              <select name="paymentMod" id="paymentMod" class="searchTransObj">
           <option value="">Select One</option>   	
				  <option value="By Cash">By Cash</option>
				  <option value="By Cheque">By Cheque</option>
				  <option value="By Bank Transfer">By Bank Transfer</option>
			  </select>
			  &nbsp;
			    Select Account
              <select name="accountNo" id="accountNo" class="searchTransObj" size="4" multiple="multiple">
				  <option value="">- Select Account -</option>
			  	  <? if(count($barclaysRec)>0){?>
					  <? for($a=0;$a<count($barclaysRec);$a++){?>
					  <option value="<?=$barclaysRec[$a]["accountNo"]?>"><?=$barclaysRec[$a]["accountNo"]?></option>
					  <? }?>
				  <? }?>
			  </select>
		</td>
      </tr>
	  <tr>
		  <td nowrap="nowrap" align="center">
	  	  Amount 
            <input name="amountT" type="text" id="amountT" class="searchTransObj">
			&nbsp;&nbsp;&nbsp;
              Select Currency
              <select name="currencyTrans" id="currencyTrans" class="searchTransObj">
				  <option value="">Select Currency</option>
			  	  <? if(count($currencyies)>0){?>
					  <? for($ct=0;$ct<count($currencyies);$ct++){?>
					  <option value="<?=$currencyies[$ct]["currencyName"]?>"><?=$currencyies[$ct]["currencyName"]?></option>
					  <? }?>
				  <? }?>
			  </select>
			  
			    
			  &nbsp;&nbsp;
			<input type="button" name="Submit" value="Search Transactions" onClick="gridReload('transList')">
			<input type="hidden" name="type" id="type" value="<?=$_GET["type"];?>">
		  </td>
	  </tr>
	  
    </table>
  	</td>
	</tr>
    
    <tr><td>
    		<table id="searchFiltersSecond">
    				<tr>
    					<td>
    						<input type="radio" name="tranactionSource" id="tranactionSource1" value="payex">Payex Transactions  
    					</td>
    					<td>
    						<input type="radio" name="tranactionSource" id="tranactionSource2" value="unistream">Unistream Transactions  
    					</td>
    					<td>	
    						<input type="radio" name="tranactionSource" id="tranactionSource3" value="anelik">Anelik Transactions
    					</td>	
    				</tr>
    			
    		</table>	
    </td></tr>
	<tr>
    <td valign="top">
    			<table id="transList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernavT" class="scroll" style="text-align:center;"></div>
	</td>
    </tr>
    <tr>
    	<td align="center">
    		<span id="buttons">
    		<input type="button" name="export" value="Export to Print" onClick="printScreen();">
			<input type="button" name="export" value="Export Grid to Excel" onClick="exportExcel()">
			<input type="button" name="export" value="Export All to Excel" onClick="exportAllExcel()">
            </span>
		  </td>
	  </tr>
    

</table>

</td>
  </tr>
</table>
</form>
</body>
</html>
<script language="javascript">
function printScreen()
{
	document.getElementById("searchFilters").style.display = "none";
	document.getElementById("searchFiltersSecond").style.display = "none";
	document.getElementById("buttons").style.display = "none";
	window.print();
	document.getElementById("searchFilters").style.display = "block";
	document.getElementById("buttons").style.display = "block";
	document.getElementById("searchFiltersSecond").style.display = "block";	
}
</script>