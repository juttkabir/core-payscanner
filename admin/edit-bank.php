<?
session_start();
include ("../include/config.php");
include ("security.php");

if($agentType == "TELLER")
{
	$parentID = $_SESSION["loggedUserData"]["cp_ida_id"];
	$userID = $_SESSION["loggedUserData"]["cp_ida_id"];
}
else
{
	$parentID = $_SESSION["loggedUserData"]["userID"];
	$userID = $_SESSION["loggedUserData"]["userID"];
}

$isCorrespondent = $_SESSION["loggedUserData"]["isCorrespondent"];
$adminType = $_SESSION["loggedUserData"]["adminType"];
$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];


/**
 * Initializing the session variables
 */
if(!isset($_GET["msg"]))
{
	$_SESSION["name1"] = "";
	$_SESSION["branchAddress1"] = "";
	$_SESSION["swiftCode1"] = "";
	$_SESSION["bankId1"] = "";
	$_SESSION["country1"] = "";
	$moneyPaid = "";	
}
/** Money paid distinguishes whether banks are added as sending bank or beneficiary bank **/
if(isset($_GET["moneyPaid"])){
	$moneyPaid = 	$_GET["moneyPaid"];
} 
if(!empty($_REQUEST["id"]) /*&& $_GET["msg"] == ""*/)
{
	$benBanksDataQry = "select * from ".TBL_BANKS." where id=".$_REQUEST["id"];
	$banBanksData = selectFrom($benBanksDataQry);

	$_SESSION["id"] = $_REQUEST["id"];

	$_SESSION["name1"] = $banBanksData["name"];
	$_SESSION["branchAddress1"] = $banBanksData["branchAddress"];
	$_SESSION["swiftCode1"] = $banBanksData["swiftCode"];
	$_SESSION["bankId1"] = $banBanksData["bankId"];
	$_SESSION["country1"] = $banBanksData["country"];
	$_SESSION["branchCode1"] = $banBanksData["branchCode"];
	
	$moneyPaid = "By Bank Transfer";
}

/**Handling Labels**/

$label = $_SESSION["id"]!= "" ? "Update" : "Add";
if($moneyPaid == "By Bank Transfer"){
	$labelPageHeading = $label." UK Bank";
	$labelBankDetail = "Bank Details";
	$labelBankId = "Sort Code";
	$countryReadonly = "readonly = 'readonly'";
}else{
	$labelPageHeading = $label." Bank Info";
	$labelBankDetail = "Beneficiary Bank Details";
	$labelBankId = "Bank Id";
	$countryReadonly = "";
} 
/**End of label handling**/  
    	
?>
<html>
<head>
	<title>Bank Information</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) 
{
	if(document.getElementById("bankId").value == "")
	{
		alert("Please enter the bank Id.");
		document.getElementById("bankId").focus();
		return false;
	}
	
	if(document.getElementById("name").value == "")
	{
		alert("Please enter the bank name.");
		document.getElementById("name").focus();
		return false;
	}
	if(document.getElementById("branchAddress").value == "")
	{
		alert("Please enter the branch address.");
		document.getElementById("branchAddress").focus();
		return false;
	}
	if(document.getElementById("country").selectedIndex == 0)
	{
		alert("Please select a country.");
		document.getElementById("country").focus();
		return false;
	}
	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }

	// end of javascript -->
</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
	<table width="100%" border="0" cellspacing="1" cellpadding="5">
	  <tr>
		<td class="topbar">
			
			<strong><font color="#000000" size="2"><?=$labelPageHeading?></font></strong>
		</td>
	  </tr>
  <form name="frmBenificiary" action="add-bank-conf.php?moneyPaid=<?=$moneyPaid?>&custCountry=<?=$_GET["custCountry"]?>" method="post">
		<input type="hidden" name="id" value="<?=$_REQUEST["id"]?>" />		
		<tr bgcolor="#ededed">
		<?
		if(isset($_GET["msg"])){
		?>
			<td width="40" align="center" colspan="4"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font>
            	<? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?>
			</td>
		<?
		}else{
		?>	
          	<td colspan="4" align="center">&nbsp;</td>
		<? } ?>
          </tr>
		<tr bgcolor="#ededed">
			<td colspan="4"><font color="#005b90"><b><?=$labelBankDetail; ?></b></font></td>
		</tr>
		<tr bgcolor="#ededed"> 
		  <td width="25%">
			  <font color="#005b90"><b>Bank Id</b><font color="red">*</font></font>&nbsp;
		  </td>
		  <td width="25%">
			<input type="text" name="bankId" id="bankId" value="<?=$_SESSION["bankId1"]?>" maxlength="30">
		  </td>
		  <td width="25%" height="20" align="right">
			<font color="#005b90"><b>Bank Name<font color="red">*</font>&nbsp;</b></font>
		  </td>
		  <td width="25%">
			<input type="text" name="name" id="name" value="<?=$_SESSION["name1"]?>" maxlength="30">
		  </td>
		</tr>
		<tr bgcolor="#ededed"> 
            <td width="25%" align="left"><font color="#005b90"><b>Country</b></font><font color="red">*</font></td>
            <td width="25%">
            	<?
           		if($adminType == "Agent")
               		$queryCountry = selectFrom("select IDAcountry from ".TBL_ADMIN_USERS." where userID = '".$parentID."'");
                else
                  	$queryCountry = selectFrom("select IDAcountry from ".TBL_ADMIN_USERS." where userID = '".$userID."'");

                $allCountries = explode(",",$queryCountry["IDAcountry"]);
            	?>
            	<select name="country" id="country">
            		<option>Select Country</option>
					<option value="United Kingdom" <?=$_SESSION["country1"] == "United Kingdom"?"selected":""?>>United Kingdom</option>
            	</select>
            </td>
            <?
            if($moneyPaid == "By Bank Transfer"){
            ?>
            	<td width="25%" align="right"><font color="#005b90"><b>Branch Name<font color="red">*</font></b></font></td>
            	<td width="25%">
            		<input type="text" name="branchAddress" id="branchAddress" value="<?=$_SESSION["branchAddress1"]?>" maxlength="30">	</td>
            <?
            }else{
            ?>
            	<td width="25%" align="left">&nbsp;</td>
            	<td width="25%">&nbsp;</td>
            <?
            }
        	    ?>
          </tr>

		<tr bgcolor="#ededed"> 
		  <td width="25%">
			  <font color="#005b90"><b>Sort Code</b></font>&nbsp;
		  </td>
		  <td width="25%">
			<input type="text" name="branchCode" id="branchCode" value="<?=$_SESSION["branchCode1"]?>" maxlength="30">
		  </td>
		  <td width="25%" height="20" align="right">&nbsp;</td>
		  <td width="25%">&nbsp;</td>
		</tr>
		  
		  
					
          <tr bgcolor="#ededed"> 
            <td width="100%" height="20" align="center" colspan="4">
			
			<input name="Save" type="submit" value=" Save "  onClick="return checkForm(document.frmBenificiary);">
        	&nbsp;&nbsp; 
			<input name="reset" type="reset" value="Clear">            	
            </td>
          </tr>
                
         </table></td>
          </tr>
        </table></td>
  </tr>
</form>
</table>
</body>
</html>
