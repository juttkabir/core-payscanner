<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$SQL_Qry = "select * from ".TBL_BANK_CHARGES." where chargeID='".$_GET["chargeID"]."'";
$chargesData = selectFrom($SQL_Qry);






if($_POST["chargeType"] != "")
{
	$chargeType = $_POST["chargeType"];

}else{
	$chargeType = $chargesData["chargeType"];

}

if($_POST["agentID"] != "" && $_POST["feeBasedOn"] == "Distributor")
{
	$agentNo = $_POST["agentID"];
}else{
	$agentNo = $chargesData["agentNo"];
}

if($_POST["amountRangeFrom"] != "")
{
	$amountRangeFrom = $_POST["amountRangeFrom"];
}else{
	$amountRangeFrom = $chargesData["amountRangeFrom"];
}

if($_POST["amountRangeTo"] != "")
{
	$amountRangeTo = $_POST["amountRangeTo"];
}else{
	$amountRangeTo = $chargesData["amountRangeTo"];
}

if($_POST["Charges"] != "")
{
	$Charges = $_POST["Charges"];
}else{
	$Charges = $chargesData["Charges"];
}

if($_POST["chargeBasedOn"] != "")
{
	$chargeBasedOn = $_POST["chargeBasedOn"];
}else{
	$chargeBasedOn = $chargesData["chargeBasedOn"];
}

?>
<html>
<head>
	<title>Update Bank Charges</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}	
	
function checkForm(theForm) {
	if(theForm.amountRangeFrom.value == "" || IsAllSpaces(theForm.amountRangeFrom.value)){
    	alert("Please provide Lower range of money.");
        theForm.amountRangeFrom.focus();
        return false;
    }
	if(theForm.amountRangeTo.value == "" || IsAllSpaces(theForm.amountRangeTo.value)){
    	alert("Please provide Upper range of money.");
        theForm.amountRangeTo.focus();
        return false;
    }
	/*if(theForm.amountRangeTo.value < theForm.amountRangeFrom.value){
    	alert("Please provide valid Upper range of money.");
        theForm.amountRangeTo.focus();
        return false;
	}*/
	 if(theForm.chargeType.options.selectedIndex == 0){
		alert("Please select Charges Type.");
		theForm.chargeType.focus();
		return false;
	}
	if(theForm.Charges.value == "" || IsAllSpaces(theForm.Charges.value)){
    	alert("Please provide Fee for spcified money ranges.");
        theForm.Charges.focus();
        return false;
    }
	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
	</script>
	<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#FFFFFF" size="2">Update <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></strong></td>
  </tr>
  <form action="add-bank-charges-conf.php?origCountry=<?=$_GET["origCountry"];?>&destCountry=<?=$_GET["destCountry"];?>&chargeType=<?=$_GET["chargeType"];?>&Submit=<?=$submit;?>" method="post"onSubmit="return checkForm(this);">
  <input type="hidden" name="chargeID" value="<?=$_GET["chargeID"]; ?>">
  <tr>
      <td align="center"> 
        <table width="500" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="500" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Update 
                    <? echo $systemPre;?> Bank Charges</strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
		<? if ($_GET["msg"] != ""){ ?>
		<tr bgcolor="#ededed"><td colspan="2" bgcolor="#FFFFCC"><table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td><td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; ?></td></tr></table></td></tr><? } ?>
		<tr bgcolor="#ededed">
			<td colspan="2"><a class="style2" href="charges-list.php?origCountry=<?=$_GET["origCountry"];?>&destCountry=<?=$_GET["destCountry"];?>&chargeType=<?=$_GET["chargeType"];?>&Submit=<?=$submit;?>">Go Back</a></td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center"><font color="#FF0000">* Compulsory 
              Fields</font></td>
		</tr>
        <tr bgcolor="#ededed">
            <td width="312"><font color="#005b90"><strong>Originating Country<font color="#ff0000">*</font></strong></font></td>
            <td width="183"><input type="hidden" name="origCountry" value="<?=$chargesData["origCountry"] ?>"><?=$chargesData["origCountry"] ?></td>
        </tr>
        
        <tr bgcolor="#ededed">
            <td width="312"><font color="#005b90"><strong>Originating Currency<font color="#ff0000">*</font></strong></font></td>
            <td width="183"><input type="hidden" name="currencyOrigin" value="<?=$chargesData["currencyOrigin"] ?>"><?=$chargesData["currencyOrigin"] ?></td>
        </tr>
        
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Destination Country<font color="#ff0000">*</font></strong></font></td>
          <td><input type="hidden" name="destCountry" value="<?=$chargesData["destCountry"] ?>">
            <?=$chargesData["destCountry"] ?></td>
        </tr>
         
        <tr bgcolor="#ededed">
            <td width="312"><font color="#005b90"><strong>Destination Currency<font color="#ff0000">*</font></strong></font></td>
            <td width="183"><input type="hidden" name="currencyDest" value="<?=$chargesData["currencyDest"] ?>"><?=$chargesData["currencyDest"] ?></td>
        </tr>
        
        </tr>
        <!-- Added by Niaz Ahmad Against Ticket # 2538 at 24-10-2007-->
        <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Charges For<font color="#ff0000">*</font></strong></font></td>
            <td width="210"><SELECT name="chargeBasedOn" id="chargeBasedOn" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].action='update-bank-charges.php?chargeID=<?=$_GET["chargeID"]?>&&origCountry=<?=$chargesData["origCountry"];?>&destCountry=<?=$chargesData["destCountry"];?>&chargeType=<?=$_GET["chargeType"];?>'; document.forms[0].submit()">
                <OPTION value="Generic" <? if($chargesData["chargeBasedOn"] =="Generic"){ echo("selected");}?> >Generic</OPTION>
                <OPTION value="Distributor" <? if($chargesData["chargeBasedOn"] =="Distributor"){ echo("selected");}?> >Distributor</OPTION>
             
             </SELECT>
             <script language="JavaScript">
		           	SelectOption(document.forms[0].chargeBasedOn, "<?=$chargeBasedOn?>");
		       	  </script>
          </td>
      </tr>
     <? if($chargeBasedOn == "Distributor") {
     	$agentQuery = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'N'";
     	if (CONFIG_IDACOUNTRY_CHECK =='1'){
				$agentQuery .= " and IDAcountry like '%".$chargesData["destCountry"]."%' ";
			}
				$agentQuery .= " order by agentCompany";				
				$agents = selectMultiRecords($agentQuery);			
			
     	 ?>
      <tr bgcolor="#ededed">
       		<td><font color="#005b90"><strong>Select Distributor</strong></font></td>
	       			<td>
	       				<select name="agentID" style="font-family:verdana; font-size: 11px">
								<option value="">- Select One -</option>
								<?
									 	for ($i=0; $i < count($agents); $i++){
									?>
								<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
								<?
										}
									?>
							  </select>
							  <script language="JavaScript">
		         			SelectOption(document.forms[0].agentID, "<?=$agentNo?>");
		          	</script>
		          </td>
       			</tr>
      
      <? 
      } 
      
      ?>
        
        <tr bgcolor="#ededed">
            <td width="312"><font color="#005b90"><strong>Amount Range<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="amountRangeFrom" value="<?=$amountRangeFrom?>" size="10" maxlength="16"> To <input type="text" name="amountRangeTo" value="<?=$amountRangeTo?>" size="10" maxlength="16"></td>
        </tr>
       <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Charges Type<font color="#ff0000">*</font></strong></font></td>
            <td width="210">
            	<SELECT name="chargeType" id="chargeType" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- Select Charges Type -</OPTION>
								<option value="fixed" <? if($chargeType == "fixed"){ echo("selected");}?>>Fixed Charges</option>
								<option value="percent" <? if($chargeType == "percent"){ echo("selected");} ?>>Percentage of Amount</option>
							</SELECT>
            </td>
		   </tr> 
        
       
        <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Charges<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="Charges" value="<?=$Charges?>" size="15" maxlength="16"></td>
        </tr>
        
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" value=" Save ">&nbsp;&nbsp; <input type="reset" value=" Clear ">
			</td>
		</tr>
		
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>