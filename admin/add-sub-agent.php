<?
session_start();
include ("../include/config.php");
include ("security.php");
if($_REQUEST["ajaxCity"] == "get")
{
	if(!empty($_REQUEST["cntry"]))
	{
		$cityQuery="select distinct(city) from  ".TBL_CITIES." where country = '".$_REQUEST["cntry"]."' order by city";
		$cityRS=selectMultiRecords($cityQuery);
		for($c=0;$c<count($cityRS);$c++)
		{
			$strCities .= "<option value='".$cityRS[$c]["city"]."'>".$cityRS[$c]["city"]."</option>";
		}
    }
	else
	{
		$strCities .= "<option value=''></option>";
	}
	echo $strCities;
	exit;
}
if($_REQUEST["ajaxServices"] == "get")
{
	$strServices = '';
	if(!empty($_REQUEST["cntry"]))
	{
		$countryIDRS = selectFrom("select countryId from ".TBL_COUNTRIES." where countryName = '".$_REQUEST["cntry"]."'");
		$serviceQuery="select DISTINCT(serviceAvailable) from  ".TBL_SERVICE_NEW." where toCountryId = '".$countryIDRS["countryId"]."' ";
		$serviceRS=selectMultiRecords($serviceQuery);
		for($c=0;$c<count($serviceRS);$c++)
		{
			$selected = "";
			if($c==0)
				$selected = "selected";
			$strServices .= '<option value="'.$serviceRS[$c]["serviceAvailable"].'" '.$selected.' ">'.$serviceRS[$c]["serviceAvailable"].'</option>';
		}
    }

	echo $strServices;
	exit;
}
// include ("javaScript.php");
$date_time = date('d-m-Y  h:i:s A');
$agentType2 = getAgentType();
$supAgentID = $_SESSION["loggedUserData"]["userID"];
$countryBasedFlag = false;
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}

if($_POST["supAgentID"] != '')
{
	$supAgentID = $_POST["supAgentID"];
}

/*	#4966 - AMB Exchange
	Operator ID is added in AnD form.
	to be displayed in Western Union Output export file.
	by Aslam Shahid
*/
$operatorFlag = false;
if(CONFIG_OPERATOR_ID_AnD=="1" && $_REQUEST["ida"]=="ND" ){
	$operatorFlag = true;
}
/*	#4966 
	Company Type dropdown added on agents form and Trading field also added.
	by Aslam Shahid
*/
$companyTypeFlag = false;
$tradingNameFlag = false;
if(CONFIG_COMPANY_TYPE_DROPDOWN=="1"){
	$companyTypeFlag = true;
}
if(CONFIG_TRADING_COMPANY_NAME=="1"){
	$tradingNameFlag = true;
}
$type="sub";  ///Prefer to use this one
$_GET["type"]="sub";  ///Assigned to avoid unexpected mistakes
?>
<html>
<head>
	<title>Title</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/custom.css">
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>

	<title>Add Sub Agent/Distributor</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="javaScript.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
	<?
	if($_GET["type"] == "sub")
	{
	?>

	if(theForm.supAgentID.options.selectedIndex == 0){
    	alert("Please select super Parent User.");
        theForm.supAgentID.focus();
        return false;
    }
	<?
	}
	else if($_GET["type"] == "sub" && $_GET["ida"] == "Y")
	{
	?>
			if(theForm.supAgentID.options.selectedIndex == 0){
    	alert("Please select super distributor against which you want to add sub distributor.");
        theForm.supAgentID.focus();
        return false;
    }
			if(theForm.agentCompany.value == "" || IsAllSpaces(theForm.agentCompany.value)){
    	alert("Please provide the sub distributor name.");
        theForm.agentCompany.focus();
        return false;
    }
    if(theForm.agentContactPerson.value == "" || IsAllSpaces(theForm.agentContactPerson.value)){
    	alert("Please provide the sub distributor's contact person name.");
        theForm.agentContactPerson.focus();
        return false;
    }
	if(theForm.agentAddress.value == "" || IsAllSpaces(theForm.agentAddress.value)){
    	alert("Please provide the sub distributor's address line 1.");
        theForm.agentAddress.focus();
        return false;
    }
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the sub distributor's country from the list.");
        theForm.Country.focus();
        return false;
    }
   if(theForm.agentPhone.value == "" || IsAllSpaces(theForm.agentPhone.value)){
    	alert("Please provide the sub distributor's phone number.");
        theForm.agentPhone.focus();
        return false;
    }
    <?
  }
  else if($_GET["ida"] == "Y")
  {
  	?>
	if(theForm.agentCompany.value == "" || IsAllSpaces(theForm.agentCompany.value)){
    	alert("Please provide the distributor name.");
        theForm.agentCompany.focus();
        return false;
    }
   if(theForm.agentContactPerson.value == "" || IsAllSpaces(theForm.agentContactPerson.value)){
    	alert("Please provide the distributor's contact person name.");
        theForm.agentContactPerson.focus();
        return false;
    }
	if(theForm.agentAddress.value == "" || IsAllSpaces(theForm.agentAddress.value)){
    	alert("Please provide the distributor's address line 1.");
        theForm.agentAddress.focus();
        return false;
    }
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the distributor's country from the list.");
        theForm.Country.focus();
        return false;
    }
   if(theForm.agentPhone.value == "" || IsAllSpaces(theForm.agentPhone.value)){
    	alert("Please provide the distributor's phone number.");
        theForm.agentPhone.focus();
        return false;
    }
   <?
  }
  ?>
<?	if((CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y") || (CONFIG_MANUAL_SUB_AnD_NUMBER=="1" && $_GET["ida"] == "ND")){  ?>
  if(theForm.manualAgentNum.value == "" || IsAllSpaces(theForm.manualAgentNum.value)){
	<? if(CONFIG_MANUAL_SUB_AnD_NUMBER=="1" && $_GET["ida"] == "ND"){ ?>
	    	alert("Please provide the AnD's login name.");
	<? } else{?>
	    	alert("Please provide the agent's login name.");
	<? }?>
        theForm.manualAgentNum.focus();
        return false;
  }
<?	}  ?>
  if(theForm.agentCompany.value == "" || IsAllSpaces(theForm.agentCompany.value)){
    	alert("Please provide the agent's company name.");
        theForm.agentCompany.focus();
        return false;
    }
	if(theForm.agentContactPerson.value == "" || IsAllSpaces(theForm.agentContactPerson.value)){
    	alert("Please provide the agent's contact person name.");
        theForm.agentContactPerson.focus();
        return false;
    }
	if(theForm.agentAddress.value == "" || IsAllSpaces(theForm.agentAddress.value)){
    	alert("Please provide the agent's address line 1.");
        theForm.agentAddress.focus();
        return false;
    }
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the agent's country from the list.");
        theForm.Country.focus();
        return false;
    }
	
	<? if(CONFIG_CITY_MANDATORY == "1") {?>
	
	 if(theForm.City.value == "" || IsAllSpaces(theForm.City.value)){
    	alert("Please provide the Agnet City Name.");
        theForm.City.focus();
        return false;
    }
	<? } ?>	
	<?
		/***** Code added by Usman Ghani aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
		if ( defined("CONFIG_POST_CODE_MENDATORY")
			&& CONFIG_POST_CODE_MENDATORY == 1 )
		{
			?>
				if ( theForm.postcode.value == "" )
				{
					alert("Please provide post code.");
					theForm.postcode.focus();
					return false;
				}
			<?
		}
		/***** End of code aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
	?>
/*	
	if(theForm.City.options.selectedIndex == 0){
    	alert("Please select the agent's city from the list.");
        theForm.City.focus();
        return false;
    }*/
	if(theForm.agentPhone.value == "" || IsAllSpaces(theForm.agentPhone.value)){
    	alert("Please provide the agent's phone number.");
        theForm.agentPhone.focus();
        return false;
    }
<?	
/**
 * To make field mandatory (CONFIG based) if it not distributor
 * @Ticket #4158
 */
 
	if (CONFIG_AGENT_ACCOUNT_LIMIT == '1' && CONFIG_AGENT_LIMIT =='1' && $_GET["ida"]!="Y"){ ?>

	if(theForm.agentAccountLimit.value == "" || IsAllSpaces(theForm.agentAccountLimit.value)){
    	alert("Please provide the agent's account limit.");
        theForm.agentAccountLimit.focus();
        return false;
    }
<? } ?>

	if(theForm.IDAcountry.value == "" || IsAllSpaces(theForm.IDAcountry.value)){
    	alert("Please select atleast one country.");
        theForm.IDAcountry.focus();
        return false;
    }
	
	
<?	if (CONFIG_BACK_DATED == '1' && $_GET["ida"] != "Y" && $agentType == 'admin') {  ?>
	if (document.getElementById('rights').value == 'Backdated') {
		if (document.getElementById('backDays').value != '') {
			if (document.getElementById('backDays').value == '0') {
				alert("Please provide back days other than zero");
				document.getElementById('backDays').focus();
				return false;
			}
			if (!isNumeric(document.getElementById('backDays').value)) {
				alert("Please provide the positive numeric back days");
				document.getElementById('backDays').focus();
				return false;
			}
		}
	}
<?	}  ?>

<?	if ($_REQUEST["ida"] == "ND" && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1") {  /* For sub A&D*/ ?>
	if(document.getElementById("settlementCurrencyAsAgent").value == "")
	{
    	alert("Please select Settlement Currency As Agent.");
        document.getElementById("settlementCurrencyAsAgent").focus();
        return false;
    }
	
	if(document.getElementById("settlementCurrencyAsDistributor").value == "")
	{
    	alert("Please select Settlement Currency As Distributor.");
        document.getElementById("settlementCurrencyAsDistributor").focus();
        return false;
    }
<? } ?>

	<? /** Code added by khola against 3541. The settlement currency code was missing from the add/update sub agent page where it exists for super agents **/ 
	if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1") 
	{ 
	?>
 		if(theForm.currencyOrigin.selectedIndex == 0){
	  	alert("Please provide the agent's settlement currency.");
    	theForm.currencyOrigin.focus();
   		return false;
    }
  <? 
  } 
  /** End of code against ticket 3541 **/     
  ?>
   	
	<?
		if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
			&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1 )
		{
			/***** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents *****/
		?>	
			assocDistRadioButton = document.getElementById("letMeSelectAssocDist");
			if ( assocDistRadioButton != null && assocDistRadioButton != undefined )
			{
				if( assocDistRadioButton.checked == true )
				{
					// In this case, Atleast one distributor should be selected.
					// User is not allowed to select all the distributors.
					// If user selects all the distributors, prompt him/her to check the "All" distributors radio button.
					distListControl = document.getElementById("distList");
					totalDistributors = distListControl.options.length
					numberOfSelectedDist = countSelectedItems( distListControl );
					if ( numberOfSelectedDist < 1 )
					{
						alert( "Please select atleast one distributor." );
						assocDistRadioButton.focus();
						return false;
					}
					else if ( numberOfSelectedDist == totalDistributors )
					{
						alert( "If you want all the distributors to be associated with this agent,\nplease click on the 'All' distributors option." );
						assocDistRadioButton.focus();
						return false;
					}
				}
			}
		<?
		}
		/***** End of code against #3428: Connect Plus - Select Distributors for Agents *****/
	?>

	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
   
function clearExpDate() {	// [by Jamshed]
	document.frmAgents.dDate.value = "";	
}

function isNumeric(strString) {
   //  check for valid numeric strings	
   
	var strValidChars = "0123456789";
	var strChar;
	var blnResult = true;
	
	if (strString.length == 0) {
		return false;
	}
	
	//  test strString consists of valid characters listed above
	for (i = 0; i < strString.length && blnResult == true; i++)	{
	  strChar = strString.charAt(i);
	  if (strValidChars.indexOf(strChar) == -1)	{
	     blnResult = false;
	  }
	}
	return blnResult;
}

function showBackDays() {
	if (document.getElementById('rights').value == 'Backdated') {
		document.getElementById('howmanybackdays').style.visibility = 'visible';
		document.getElementById('howmanybackdays').style.position = 'relative';
	} else {
		document.getElementById('backDays').value = '';
		document.getElementById('howmanybackdays').style.visibility = 'hidden';
		document.getElementById('howmanybackdays').style.position = 'absolute';
	}
}
   function checkFocus(){
  <? if($_GET["ida"] == "Y"){ ?>
		frmAgents.agentCompany.focus();
	<? }else{ ?>
	frmAgents.manualAgentNum.focus();
 <? } ?>
	
}
	// end of javascript -->
function processCompanyType(){
	var compTypeID = document.getElementById("companyType");
	if(compTypeID!=null){
		if(compTypeID.value!="MSB"){
			var compVal = "3rd Party";
		}
		else{
			var compVal = "MSB";
		}
		var s = 1;
		while(document.getElementById("companyLabel_"+s)!=null){
			document.getElementById("companyLabel_"+s).innerHTML = compVal;
			s++;
		}
	}
}
	</script>
	<?
		if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
			&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1 )
		{
			/***** Script added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents *****/
			$onloadJS = "onLoadJS(); ";
		?>
			<script>
				
				function onLoadJS()
				{
					updateAssocDistControls();
				}
		
				function updateAssocDistControls()
				{
					if( document.getElementById("letMeSelectAssocDist").checked == true )
						document.getElementById("distList").disabled = false;
					else
						document.getElementById("distList").disabled = true;
				}
		
				function countSelectedItems( listControl )
				{
					totalItems = listControl.options.length;
					numberOfSelectedItems = 0;
					for (i = 0; i < totalItems; i++)
					{
						if ( listControl.options[i].selected == true )
							numberOfSelectedItems++;
					}
					return numberOfSelectedItems;
				}
				</script>
		<?
		}
		/***** End of code against #3428: Connect Plus - Select Distributors for Agents ****/
	?>
</head>
<body onLoad="<?=(!empty($onloadJS) ? $onloadJS : ""); ?> checkFocus();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	<tr>
		<td class="topbar"><strong><font class="topbar_tex">Add Sub <?
					if($_GET["ida"] == "Y"){
						echo("Distributor");
					}elseif($_GET["ida"]== "ND"){
						echo("AnD");
					}else{
						echo __("Agent");
					}?>

				</font></strong></td>
	</tr>
	<form name="frmAgents" action="add-sub-agent-conf.php?type=<?=$type ?>&ida=<?=$_GET["ida"]?>" method="post" onSubmit="return checkForm(this);" enctype="multipart/form-data">
		<tr>
			<td align="center">
				<table width="528" border="0" cellspacing="1" cellpadding="2" align="center">
					<tr>
						<td colspan="2" bgcolor="#000000">
							<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
								<tr>
									<td align="center" bgcolor="#EEEEEE"> <font color="#000066" size="2"><strong>Add
												<?
												if($_GET["ida"] == "Y")
												{
													?>
													Sub Distributor
												<? }elseif($_GET["ida"] == "ND")
												{
													?>
													Sub AnD
													<?
												}else{
													?>
													Sub <?=__("Agent");?>
													<?
												}

												?>
											</strong></font></td>
								</tr>
							</table>
						</td>
					</tr>
					<? if ($_GET["msg"] != ""){ ?>
						<tr bgcolor="#ededed">
							<td colspan="2">
								<table width="100%" cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
										<td><font color="#FF0000">
												<? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; $_SESSION['error'] = ""; ?>
											</font></td>
									</tr>
								</table>
							</td>
						</tr>
					<? } ?>
					<tr bgcolor="#EEEEEE">
						<td height="19" colspan="2" align="center"><font color="#FF0000">*
								Compulsory Fields</font></td>
					</tr>
					<?

					if($type == "sub")
					{
						if($agentType2 == 'SUPA' || $agentType2 == 'SUPI' || $agentType2 == 'SUPAI'){
							?>
							<tr bgcolor="#ededed">
								<td width="169"><font color="#005b90"><strong>

											<? if($_GET["ida"] == "Y"){?>
											Super Distributor<font color="#ff0000">
												<? }else {?>
												Super <?=__("Agent");?><font color="#ff0000">
													<? }?>
										</strong></font></td>
								<td width="268">
									<input type="text" name="supAgentID" value="<?=stripslashes($_SESSION["loggedUserData"]["username"]." [".$_SESSION["loggedUserData"]["agentCompany"]."]"); ?>" size="35" maxlength="255" readonly>
									<input type="hidden" name="supAgentID" value="<?=stripslashes($supAgentID); ?>" size="35" maxlength="255">
								</td>
							</tr>
							<?
						}else{

							$nAgentType = $_GET["type"];
							?>
							<tr bgcolor="#ededed">
								<td><font color="#005b90"><strong>
											<? if($_GET["ida"] == "Y"){?>
												Select Distributor<font color="#ff0000">*</font>
											<? }elseif($_GET["ida"] == "ND"){?>
												Select AnD <font color="#ff0000">*</font>
											<? }else {?>
												Select <?=__("Agent");?><font color="#ff0000">*</font>
											<? }?>
										</strong></font></td>
								<td>
									<select name="supAgentID" id="supAgentID" style="font-family:verdana; font-size: 11px; width:226">
										<option value="">- Select One -</option>
										<?
										if($_GET["ida"] == "Y")
										{

											$agents = selectMultiRecords("select userID, agentCompany, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent = 'ONLY' order by agentCompany");
										}
										elseif($_GET["ida"] == "ND")
										{

											$agents = selectMultiRecords("select userID, agentCompany, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent = 'Y' order by agentCompany");
										}
										else
										{
											$agents = selectMultiRecords("select userID, agentCompany, username from ".TBL_ADMIN_USERS." where parentID > 0 and agentType='Supper' and isCorrespondent = 'N' order by agentCompany");
										}
										for ($i=0; $i < count($agents); $i++){
											if($agents[$i]["userID"] == $supAgentID)
											{
												?>
												<option value="<? echo $supAgentID; ?>"selected>
													<? echo $agents[$i]["username"]." [".$agents[$i]["agentCompany"]."]"; ?>
												</option>
												<?
											}
											else
											{
												?>
												<option value="<? echo $agents[$i]["userID"]; ?>">
													<? echo $agents[$i]["username"]." [".$agents[$i]["agentCompany"]."]"; ?>
												</option>
												<?
											}
										}
										?>
									</select>
								</td>
							</tr>
							<? if($companyTypeFlag){?>
								<tr bgcolor="#ededed">
									<td width="156">
										<font color="#005b90"><strong>Company Type* </strong></font>
									</td>
									<td width="361">
										<SELECT name="companyType" id="companyType" style="font-family:verdana; font-size: 11px" onChange="processCompanyType();">
											<OPTION value="MSB">MSB</OPTION>
											<OPTION value="3rd party">3rd Party</OPTION>
										</SELECT>
										<script language="JavaScript">
											SelectOption(document.forms[0].companyType, "<?=$_POST["companyType"]; ?>");
										</script>
									</td>
								</tr>
							<? }?>
							<? if($tradingNameFlag){?>
								<tr bgcolor="#ededed" id="tradingDIV">
									<td width="156"><font color="#005b90">
											<strong>
												Trading Name
											</strong>
										</font>
									</td>
									<td width="361">
										<input type="text" name="tradingName" value="<?=stripslashes($_POST["tradingName"]); ?>" size="35" maxlength="255">
									</td>
								</tr>
								<?
							}
						}
					}
					?>


					<?
					if((CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y") || (CONFIG_MANUAL_SUB_AnD_NUMBER=="1" && $_GET["ida"] == "ND"))
					{
						$logineName = __("Agent")." Login Name";
						if(CONFIG_MANUAL_SUB_AnD_NUMBER=="1" && $_GET["ida"] == "ND")
							$logineName = "AnD Login Name";
						?>
						<tr bgcolor="#ededed">
							<td width="156"><font color="#005b90"><strong><?=$logineName;?><font color="#ff0000">*</font></strong></font></td>
							<td>
								<input type="text" name="manualAgentNum" value="<?=stripslashes($_POST["manualAgentNum"]); ?>" size="35" maxlength="100">
							</td>
						</tr>
						<?
					}
					?>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>
									<? if($_GET["ida"] == "Y"){?>
										Distributor Name<font color="#ff0000">*</font>
									<? }elseif($_GET["ida"] == "ND"){?>
										AnD Name<font color="#ff0000">*</font>
									<? }else {?>
										Company Name<font color="#ff0000">*</font>
									<? }?>
								</strong></font></td>
						<td width="361">
							<input type="text" name="agentCompany" value="<?=stripslashes($_POST["agentCompany"]); ?>" size="35" maxlength="255">
						</td>
					</tr>
					<? if(CONFIG_DISTRIBUTOR_ALIAS == "1" && $_GET["ida"] == "Y") {?>
						<tr bgcolor="#ededed">
							<td width="156"><font color="#005b90"><strong> Distributor Alias</strong></font></td>
							<td>
								<input type="text" name="distAlias" value="<?=stripslashes($_SESSION["distAlias"]); ?>" size="35" maxlength="100">
							</td>
						</tr>
					<? } ?>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong> Contact Person<font color="#ff0000">*</font></strong></font></td>
						<td>
							<input type="text" name="agentContactPerson" value="<?=stripslashes($_POST["agentContactPerson"]); ?>" size="35" maxlength="100">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156" valign="top"><font color="#005b90"><b>Address Line
									1<font color="#ff0000">*</font></b></font></td>
						<td>
							<input type="text" name="agentAddress" value="<?=stripslashes($_POST["agentAddress"]); ?>" size="35" maxlength="254">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156" valign="top"><font color="#005b90"><b>Address Line
									2</b></font></td>
						<td>
							<input type="text" name="agentAddress2" value="<?=stripslashes($_POST["agentAddress2"]); ?>" size="35" maxlength="254">
						</td>
					</tr>

					<tr bgcolor="#ededed">
						<td width="156" valign="top"><font color="#005b90"><b>Country<font color="#ff0000">*</font></b></font></td>
						<td>
							<SELECT name="Country" id="Country" style="font-family:verdana; font-size: 11px" onChange='$("#cityDiv").load("add-sub-agent.php?ajaxCity=get&cntry="+this.value);<? if($_GET["ida"] == "Y" && $countryBasedFlag){?>$("#servicesDiv").load("add-sub-agent.php?ajaxServices=get&cntry="+this.value); <? } ?>' >
								<OPTION value="">- Select Country-</OPTION>

								<?
								if(CONFIG_ENABLE_ORIGIN == "1"){

									if($_GET["ida"] == "Y"){
										$countryTypes = " and countryType like '%destination%' ";
									}else{
										$countryTypes = " and countryType like '%origin%' ";
										$agentCountry = " ";
									}

								}else{
									$countryTypes = " ";
									$agentCountry = " and countryName ='United Kingdom' ";
								}
								if(CONFIG_COUNTRY_SERVICES_ENABLED){
									if($_GET["ida"] == "Y"){
										$serviceTable = TBL_SERVICE;
										if($countryBasedFlag){
											$serviceTable = TBL_SERVICE_NEW;
										}
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".$serviceTable." where  countryId = toCountryId  $countryTypes order by countryName");
									}
									else
									{
										$countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes $agentCountry order by countryName";
										if($countryBasedFlag){
											$countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY.", ".TBL_SERVICE_NEW." where 1 and  countryId = fromCountryId $countryTypes $agentCountry order by countryName";
										}
										$countires = selectMultiRecords($countiresQuery);
									}
								}else
								{
									if($_GET["ida"] == "Y"){
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
									}
									else
									{
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes $agentCountry order by countryName");
									}
								}


								for ($i=0; $i < count($countires); $i++){
									?>
									<OPTION value="<?=$countires[$i]["countryName"]; ?>">
										<?=$countires[$i]["countryName"]; ?>
									</OPTION>
									<?
								}
								?>
							</SELECT>
							<script language="JavaScript">
								SelectOption(document.forms[0].Country, "<?=$_POST["Country"]; ?>");
							</script>
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156" valign="top"><font color="#005b90"><b>City</b></font>
							<? if(CONFIG_CITY_MANDATORY == "1"){ ?><font color="#ff0000">*</font><? } ?>
						</td>
						<td>
							<? if(CONFIG_BENEFICIARY_CITY_DROPDOWN == "1") { ?>
								<select name="City" id="cityDiv">
									<option value="">- Select City-</option>
								</select>
								<script language="JavaScript">
									SelectOption(document.forms[0].City, "<?=$_POST["City"]; ?>");
								</script>
							<? } else { ?>
							<input type="text" name="City" value="<?=$_POST[City];?>" size="35" maxlength="255">
							<? } ?>
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156" valign="top"><font color="#005b90"><b>
									<? if ($_POST["Country"] == "United States") echo "Zip"; else echo "Postal"; ?>
									Code</b></font>
							<?
							/***** Code added by Usman Ghani aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
							if ( defined("CONFIG_POST_CODE_MENDATORY")
								&& CONFIG_POST_CODE_MENDATORY == 1 )
							{
								?>
								<strong><font color="#ff0000">*</font></strong>
								<?
							}
							/***** End of code aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
							?>
						</td>
						<td>
							<input type="text" name="postcode" value="<?=stripslashes($_POST["agentZip"]); ?>" size="35" maxlength="15">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>Phone<font color="#ff0000">*</font></strong></font></td>
						<td>
							<input type="text" name="agentPhone" value="<?=stripslashes($_POST["agentPhone"]); ?>" size="35" maxlength="32">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>Fax</strong></font></td>
						<td>
							<input type="text" name="agentFax" value="<?=stripslashes($_POST["agentFax"]); ?>" size="35" maxlength="32">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>Email</strong></font></td>
						<td>
							<input type="text" name="email" value="<?=stripslashes($_POST["email"]); ?>" size="35" maxlength="255">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>Company URL</strong></font></td>
						<td>
							<input type="text" name="agentURL" value="<?=stripslashes($_POST["agentURL"]); ?>" size="35" maxlength="255">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong><label id="companyLabel_1">MSB</label> Number</strong></font>
							<? if(CONFIG_REMOVE_MSB_TEXT_FROM_ADD_AGENT_DISTRIBUTOR !="1"){?>
								<br><font size="1">OR (State Distributor license or permission from
									Ministry of Finance)</font>
							<? } ?>
						</td>
						<td valign="top">
							<input type="text" name="agentMSBNumber" value="<?=stripslashes($_POST["agentMSBNumber"]); ?>" size="35" maxlength="255">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong><label id="companyLabel_2">MSB</label> Expiry</strong></font></td>
						<td>
							<SELECT name="msbDay" size="1" style="font-family:verdana; font-size: 11px">
								<OPTION value="">Day</OPTION>
								<?
								for ($Day=1;$Day<32;$Day++)
								{
									if ($Day<10)
										$Day="0".$Day;
									echo "<option value=\"$Day\">$Day</option>\n";
								}
								?>
							</select>
							<script language="JavaScript">
								SelectOption(document.forms[0].msbDay, "<?echo $msbDay;?>");
							</script>
							<SELECT name="msbMonth" size="1" style="font-family:verdana; font-size: 11px">
								<OPTION value="">Month</OPTION>
								<OPTION value="01">Jan</OPTION>
								<OPTION value="02">Feb</OPTION>
								<OPTION value="03">Mar</OPTION>
								<OPTION value="04">Apr</OPTION>
								<OPTION value="05">May</OPTION>
								<OPTION value="06">Jun</OPTION>
								<OPTION value="07">Jul</OPTION>
								<OPTION value="08">Aug</OPTION>
								<OPTION value="09">Sep</OPTION>
								<OPTION value="10">Oct</OPTION>
								<OPTION value="11">Nov</OPTION>
								<OPTION value="12">Dec</OPTION>
							</SELECT>
							<script language="JavaScript">
								SelectOption(document.forms[0].msbMonth, "<? echo $msbMonth; ?>");
							</script>
							<SELECT name="msbYear" size="1" style="font-family:verdana; font-size: 11px">
								<OPTION value="" selected>Year</OPTION>
								<?
								$cYear=date("Y");
								for ($Year=$cYear; $Year<=($cYear+20);$Year++)
								{
									echo "<option value=\"$Year\">$Year</option>\n";
								}
								?>
							</SELECT>
							<script language="JavaScript">
								SelectOption(document.forms[0].msbYear, "<?echo $msbYear?>");
							</script>
						</td>
					</tr>

					<? if(CONFIG_AGENT_HOUSE_NUMBER == "1"){?>
						<tr bgcolor="#ededed">
							<td width="156"><font color="#005b90"><strong>House Number</strong></font></td>
							<td valign="top">
								<input type="text" name="agentHouseNumber" value="<?=stripslashes($_SESSION["agentHouseNumber"]); ?>" size="35" maxlength="255">
							</td>
						</tr>
					<? } ?>

					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>Company Registration
									Number</strong></font></td>
						<td>
							<input type="text" name="agentCompRegNumber" value="<?=stripslashes($_POST["agentCompRegNumber"]); ?>" size="35" maxlength="100">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>Company Director</strong></font></td>
						<td>
							<input type="text" name="agentCompDirector" value="<?=stripslashes($_POST["agentCompDirector"]); ?>" size="35" maxlength="100">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>Director Address</strong></font></td>
						<td>
							<input type="text" name="agentDirectorAdd" value="<?=stripslashes($_POST["agentDirectorAdd"]); ?>" size="35" maxlength="255">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156" valign="top"><font color="#005b90"><strong>Director
									Proof ID</strong></font></td>
						<td valign="top">
							<input type="radio" name="agentProofID" value="Passport" <? if ($_POST["agentProofID"] == "Passport" || $_SESSION["agentProofID"] == "") echo "checked"; ?>>
							Passport <br>
							<input type="radio" name="agentProofID" value="Driving License" <? if ($_POST["agentProofID"] == "Driving License") echo "checked"; ?>>
							Driving License <br>
							<input type="radio" name="agentProofID" value="Other" <? if ($_POST["agentProofID"] == "Other") echo "checked"; ?>>
							Other please specify
							<input type="text" name="otherProofID" value="<?=$_POST[otherProofID]; ?>" size="15">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>ID Expiry</strong></font></td>
						<td>
							<?	if (CONFIG_FREETEXT_IDFIELDS == '1') {  ?>
							<input name="dDate" type="text" id="dDate" value="<? echo (!is_array($_SESSION["dDate"]) ? $_SESSION["dDate"] : "") ?>"> &nbsp;&nbsp;&nbsp;&nbsp;<i>DD-MM-YYYY</i>
							<?	} elseif (CONFIG_IDEXPIRY_WITH_CALENDAR == "1") {  ?>
							<input name="dDate" type="text" id="dDate" readonly value="<? echo (is_array($_SESSION["dDate"]) ? implode("/", $_SESSION["dDate"]) : $_SESSION["dDate"]) ?>">
								&nbsp;<a href="javascript:show_calendar('frmAgents.dDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>
								&nbsp;&nbsp;<input type="button" name="btnClearDate" value="Clear" onClick="clearExpDate();">
							<?	} else { ?>
								<SELECT name="idDay" size="1" style="font-family:verdana; font-size: 11px">
									<OPTION value="">Day</OPTION>
									<?
									for ($Day=1;$Day<32;$Day++)
									{
										if ($Day<10)
											$Day="0".$Day;
										echo "<option value=\"$Day\">$Day</option>\n";
									}
									?>
								</select>
								<script language="JavaScript">
									SelectOption(document.forms[0].idDay, "<?echo $idDay;?>");
								</script>
								<SELECT name="idMonth" size="1" style="font-family:verdana; font-size: 11px">
									<OPTION value="">Month</OPTION>
									<OPTION value="01">Jan</OPTION>
									<OPTION value="02">Feb</OPTION>
									<OPTION value="03">Mar</OPTION>
									<OPTION value="04">Apr</OPTION>
									<OPTION value="05">May</OPTION>
									<OPTION value="06">Jun</OPTION>
									<OPTION value="07">Jul</OPTION>
									<OPTION value="08">Aug</OPTION>
									<OPTION value="09">Sep</OPTION>
									<OPTION value="10">Oct</OPTION>
									<OPTION value="11">Nov</OPTION>
									<OPTION value="12">Dec</OPTION>
								</SELECT>
								<script language="JavaScript">
									SelectOption(document.forms[0].idMonth, "<? echo $idMonth; ?>");
								</script>
								<SELECT name="idYear" size="1" style="font-family:verdana; font-size: 11px">
									<OPTION value="" selected>Year</OPTION>
									<?
									$cYear=date("Y");
									for ($Year=$cYear; $Year<=($cYear+20);$Year++)
									{
										echo "<option value=\"$Year\">$Year</option>\n";
									}
									?>
								</SELECT>
								<script language="JavaScript">
									SelectOption(document.forms[0].idYear, "<?echo $idYear?>");
								</script>
							<?	}  ?>
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156" valign="top"><font color="#005b90"><strong> Document
									Provided</strong></font></td>
						<td>
							<p>
								<input name="agentDocumentProvided[0]" type="checkbox" id="agentDocumentProvided[0]" value="MSB License" <? echo (strstr($_POST["agentDocumentProvided"],"MSB License") ? "checked"  : "")?>>
								<label id="companyLabel_3">MSB</label> License <br>
								<input name="agentDocumentProvided[1]" type="checkbox" id="agentDocumentProvided[1]" value="Company Registration Certificate" <? echo (strstr($_POST["agentDocumentProvided"],"Company Registration Certificate") ? "checked"  : "")?>>
								Company Registration Certificate <br>
								<input name="agentDocumentProvided[2]" type="checkbox" id="agentDocumentProvided[2]" value="Utility Bill" <? echo (strstr($_POST["agentDocumentProvided"],"Utility Bill") ? "checked"  : "")?>>
								Utility Bill <br>
								<input name="agentDocumentProvided[3]" type="checkbox" id="agentDocumentProvided[3]" value="Director Proof of ID" <? echo (strstr($_POST["agentDocumentProvided"],"Director Proof of ID") ? "checked"  : "")?>>
								Director Proof of ID</p>
						</td>
					</tr>
					<? if($operatorFlag){?>
						<tr bgcolor="#ededed">
							<td width="156"><font color="#005b90"><strong>Operator ID</strong></font></td>
							<td>
								<input type="text" name="OperatorID" value="<?=stripslashes($_POST["OperatorID"]); ?>" size="35" maxlength="255">
							</td>
						</tr>
					<? }?>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>Bank Name</strong></font></td>
						<td>
							<input type="text" name="agentBank" value="<?=stripslashes($_POST["agentBank"]); ?>" size="35" maxlength="255">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>Bank Account Name</strong></font></td>
						<td>
							<input type="text" name="agentAccountName" value="<?=stripslashes($_POST["agentAccountName"]); ?>" size="35" maxlength="255">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>Bank Account Number</strong></font></td>
						<td>
							<input type="text" name="agentAccounNumber" value="<?=stripslashes($_POST["agentAccounNumber"]); ?>" size="35" maxlength="255">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>Bank Branch Code</strong></font></td>
						<td>
							<input type="text" name="agentBranchCode" value="<?=stripslashes($_POST["agentBranchCode"]); ?>" size="35" maxlength="255">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>Bank Branch Name</strong></font></td>
						<td>
							<input type="text" name="agentBranchName" value="<?=stripslashes($_POST["agentBranchBranch"]); ?>" size="35" maxlength="255">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>Bank Account Type</strong></font></td>
						<td>
							<select name="agentAccountType">
								<option value="Current">Current</option>
								<option value="Saving">Saving</option>
								<option value="Other">Other</option>
							</select>
							<script language="JavaScript">
								SelectOption(document.forms[0].agentAccountType, "<? echo $_POST["agentAccountType"]; ?>");
							</script>
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td width="156" height="24"><font color="#005b90"><strong>Account
									Currency</strong></font></td>
						<td height="24">
							<SELECT NAME="agentCurrency" style="font-family:verdana; font-size: 11px">
								<?

								$strQuery = "SELECT DISTINCT(currencyName), description  FROM  currencies ORDER BY currencyName ";
								$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());

								while($rstRow = mysql_fetch_array($nResult))
								{
									$country = $rstRow["country"];

									$currency = $rstRow["currencyName"];
									$description = $rstRow["description"];
									$erID  = $rstRow["cID"];
									echo "<option value ='$currency'> ".$currency." --> ".$description."</option>";
								}
								?>
							</SELECT>
							<script language="JavaScript">
								SelectOption(document.forms[0].agentCurrency, "<? echo $_POST["agentCurrency"]; ?>");
							</script>
						</td>
					</tr>

					<!--tr bgcolor="#ededed">
                      <td width="156" justify="middle" height="20"><font color="#005b90"></font></td>
                      <td height="20"> </td>
                    </tr-->

					<? if (CONFIG_AGENT_LIMIT == 1){ ?>
						<tr bgcolor="#ededed">
							<td width="156"><font color="#005b90"><strong><?=__("Agent")?> Account Limit
										<?
										/**
										 * To make field mandatory (CONFIG based) if it not distributor
										 * @Ticket #4158
										 */

										if (CONFIG_AGENT_ACCOUNT_LIMIT == '1' && $_GET["ida"]!="Y"){ ?>
										<font color="#ff0000">*</font></strong></font>
								<? } ?>
								</strong>
								</font>
							</td>
							<td>
								<input type="text" name="agentAccountLimit" value="<?=stripslashes($_POST["agentAccountLimit"]); ?>" size="35" maxlength="15">
							</td>
						</tr>
					<? }
					if(CONFIG_USER_COMMISSION_MANAGEMENT == '1' && $_GET["ida"] != "Y"){
					}else{
						?>
						<tr bgcolor="#ededed">
							<td><font color="#005b90"><strong>Commission package</strong></font></td>
							<td>
								<select name="commPackage" id="commPackage">
									<option value="001">Fixed Amount</option>
									<option value="002">Percentage of Transaction Amount</option>
									<option value="003">Percentage of Fee</option>
									<option value="004">None</option>
								</select>
								<script language="JavaScript">
									SelectOption(document.forms[0].commPackage, "<?=$_POST["commPackage"]; ?>");
								</script>
							</td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="156"><font color="#005b90"><strong>Commission Percentage/Fee</strong></font></td>
							<td>
								<input type="text" name="agentCommission" value="<?=stripslashes($_POST["agentCommission"]); ?>" size="35" maxlength="6">
							</td>
						</tr>
					<? }?>
					<tr bgcolor="#ededed">
						<td width="156"><font color="#005b90"><strong>Status</strong></font></td>
						<td>
							<input type="radio" name="agentStatus" value="New" <? if ($_POST["agentStatus"] == "" || $_POST["agentStatus"] == "New") echo "checked"; ?>>
							New
							<input type="radio" name="agentStatus" value="Active" <? if ($_POST["agentStatus"] == "Active") echo "checked"; ?>>
							Active
							<input type="radio" name="agentStatus" value="Disabled" <? if ($_POST["agentStatus"] == "Disabled") echo "checked"; ?>>
							Disabled
							<input type="radio" name="agentStatus" value="Suspended" <? if ($_POST["agentStatus"] == "Suspended") echo "checked"; ?>>
							Suspended</td>
					</tr>
					<tr bgcolor="#ededed">
						<td><font color="#005b90"><strong>Correspondent</strong></font></td>
						<td>
							<select name="correspondent" id="correspondent">
								<? if($_GET["ida"] == "ND") {?> <option value="Y">Both Agent and Distributor</option><!--Can't distribute transactions--><? } ?>
								<? if($_GET["ida"] == "Y"){ ?><option value="ONLY">Distributor</option><!--Only distribute transactions--><? } ?>
								<? if($_GET["ida"] != "Y" && $_GET["ida"] != "ND") {?> <option value="N">Agent</option><!--Can't distribute transactions--><? } ?>

							</select>
							<script language="JavaScript">
								SelectOption(document.forms[0].correspondent, "<?=$_POST["isCorrespondent"]; ?>");
							</script>
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td><font color="#005b90"><strong>Select country to <? if ($_GET["ida"] == "Y") { echo "Assign distribution"; }else{ echo "Send money"; } ?><font color="#ff0000">*</font></strong></font></td>
						<td>Hold Ctrl key for multiple selection<br>
							<SELECT name="IDAcountry[]" size="4" multiple id="IDAcountry" style="font-family:verdana; font-size: 11px" >
								<?

								if(CONFIG_ENABLE_ORIGIN == "1"){


									$countryTypes = " and countryType like '%destination%' ";

								}else{
									$countryTypes = " ";

								}
								if(CONFIG_COUNTRY_SERVICES_ENABLED){
									$serviceTable = TBL_SERVICE;
									if($countryBasedFlag){
										$serviceTable = TBL_SERVICE_NEW;
									}
									$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".$serviceTable." where  countryId = toCountryId  $countryTypes order by countryName");

								}else
								{
									$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");

								}

								//$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
								for ($i=0; $i < count($countires); $i++){
									?>
									<OPTION value="<?=$countires[$i]["countryName"]; ?>" <? echo (strstr($_POST["IDAcountry"],$countires[$i]["countryName"]) ? "selected"  : "")?>>
										<?=$countires[$i]["countryName"]; ?>
									</OPTION>
									<?
								}
								?>
							</SELECT>
						</td>
					</tr>

<!--					//////////////////////////////////////////////////-->
					<tr bgcolor="#ededed">
						<?php
							if ($_GET["type"] === "sub" && !isset($_GET["ida"])){
						?>
								<td>
									<font color="#005b90"><strong>Select associated distributor <font color="#ff0000">*</font></strong></font>
								</td>
								<td>
									<SELECT name="associatedDistributor[]" size="4" multiple id="associatedDistributor" style="font-family:verdana; font-size: 11px">
										<?
										$query="";
										if ($agentType2 === "SUPA" || $agentType2 === "SUBA" ) {
											$query="select name, agentCompany from admin where userID='$parentID' or  parentID = '$parentID'";
										}  else {
											$query="SELECT name , agentCompany FROM `admin` WHERE `adminType` = 'Agent' AND (`agentType` = 'Supper' or `agentType` = 'Sub') AND `isCorrespondent` = 'ONLY' and agentStatus='Active'";
										}

										$result=selectMultiRecords($query);
										foreach($result as $row){
											echo "<option value='".$row['name']."(".$row['agentCompany'].")'>".$row['name']."(".$row['agentCompany'].")</option>";
										}
										?>
									</SELECT>
								</td>
								<?php
							} else if($_GET["type"] === "sub" && $_GET["ida"] === "Y") {
						?>
								<td>
									<font color="#005b90"><strong>Select associated agents <font color="#ff0000">*</font></strong></font>
								</td>
						<td>
							<SELECT name="associatedAgent[]" size="4" multiple id="associatedAgent" style="font-family:verdana; font-size: 11px">
								<?
								$query="";
								if ($agentType2 === "SUPA" || $agentType2 === "SUBA" ) {
									$query="select name, agentCompany from admin where userID='$parentID' or  parentID = '$parentID'";
								}  else {
									$query="SELECT name , agentCompany FROM `admin` WHERE `adminType` = 'Agent' AND (`agentType` = 'Supper' or `agentType` = 'Sub') AND `isCorrespondent` = 'N' and agentStatus='Active'";
								}

								$result=selectMultiRecords($query);
								foreach($result as $row){
									echo "<option value='".$row['name']."(".$row['agentCompany'].")'>".$row['name']."(".$row['agentCompany'].")</option>";
								}
								?>
							</SELECT>
						</td>
						<?php
							} else {
						?>
								<td>

								</td>
						<?php
							}
						?>
					</tr>
<!--					////////////////-->
					<tr bgcolor="#ededed">
						<td>
							<font color="#005b90"><strong>Select services </strong></font>
						</td>
						<td>
							<SELECT name="services[]" size="4" multiple id="services" style="font-family:verdana; font-size: 11px">
								<option value="W">Website</option>
								<option value="O">Online</option>
								<option value="BO">Back Office</option>
								<option value="G">Gateway</option>
							</SELECT>
						</td>

					</tr>
<!--					///////////-->
					<tr bgcolor="#ededed">
						<td><font color="#005b90"><strong>Company Logo </strong></font></td>
						<td>
							<input type="file" name="logo">
						</td>
					</tr>
					<tr bgcolor="#ededed">
						<td><font color="#005b90"><strong>Authorised for the services</strong></font></td>
						<td> Hold Ctrl key for multiple selection<br>
							<SELECT name="authorizedFor[]" size="4" id="serviceDiv" multiple style="font-family:verdana; font-size: 11px" >
								<?
								if(trim($_POST["authorizedFor"]) == "")
								{?>
									<option value="Pick up" selected>Pick up</option>
									<?
								}
								else
								{
									?>
									<option value="Pick up" <? echo (strstr($_POST["authorizedFor"],"Pick up") ? "selected"  : "")?>>Pick
										up</option>
									<?
								}
								?>
								<option value="Bank Transfer" <? echo (strstr($_POST["authorizedFor"],"Bank Transfer") ? "selected"  : "")?>>Bank
									Transfer</option>
								<option value="Home Delivery" <? echo (strstr($_POST["authorizedFor"],"Home Delivery") ? "selected"  : "")?>>Home
									Delivery</option>
							</SELECT>
						</td>
					</tr>

					<?
					if($_REQUEST["ida"] == "ND" && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1")
					{
						?>
						<tr bgcolor="#ededed">
							<td><font color="#005b90"><strong>Settlement Currency</strong>(As Agent)</font><font color="red">*</font></td>
							<td>
								<select name="settlementCurrencyAsAgent" id="settlementCurrencyAsAgent">
									<option value="">-- Select Currency --</option>
									<?
									$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
									$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());

									while($rstRow = mysql_fetch_array($nResult))
									{
										$country = $rstRow["country"];
										$currency = $rstRow["currencyName"];
										$description = $rstRow["description"];
										$erID  = $rstRow["cID"];

										echo "<option value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";
									}
									?>
								</select>
								</select>
								<script language="JavaScript">SelectOption(document.forms[0].settlementCurrencyAsAgent, "<?=$_SESSION["settlementCurrencyAsAgent"]; ?>");</script>
							</td>
						</tr>

						<tr bgcolor="#ededed">
							<td><font color="#005b90"><strong>Settlement Currency</strong>(As Distributor)</font><font color="red">*</font></td>
							<td>
								<select name="settlementCurrencyAsDistributor" id="settlementCurrencyAsDistributor">
									<option value="">-- Select Currency --</option>
									<?
									$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
									$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());

									while($rstRow = mysql_fetch_array($nResult))
									{
										$country = $rstRow["country"];
										$currency = $rstRow["currencyName"];
										$description = $rstRow["description"];
										$erID  = $rstRow["cID"];

										echo "<option value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";
									}
									?>
								</select>
								</select>
								<script language="JavaScript">SelectOption(document.forms[0].settlementCurrencyAsDistributor, "<?=$_SESSION["settlementCurrencyAsDistributor"]; ?>");</script>
							</td>
						</tr>
						<?
					}
					else
					{

						/** Code added by khola against 3541. The settlement currency code was missing from the add/update sub agent page where it exists for super agents **/
						if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1") { ?>
							<tr bgcolor="#ededed">
								<td><font color="#005b90"><strong>Settlement Currency<font color="#ff0000">*</font></strong></font></td>
								<td>
									<select name="currencyOrigin">
										<option value="">-- Select Currency --</option>
										<?
										$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
										$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());

										while($rstRow = mysql_fetch_array($nResult))
										{
											$country = $rstRow["country"];
											$currency = $rstRow["currencyName"];
											$description = $rstRow["description"];
											$erID  = $rstRow["cID"];

											echo "<option value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";
										}
										?>
									</select>
									<script language="JavaScript">
										SelectOption(document.forms[0].currencyOrigin, "<?=$_POST["currencyOrigin"] ?>");
									</script>
								</td>
							</tr>
							<?
						}
						/** End of code against ticket 3541 **/
					}
					?>




					<?
					/*** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents ****/
					if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
						&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1
						&& $_REQUEST["ida"] != "Y")
					{
						?>
						<tr bgcolor="#ededed">
							<td><font color="#005b90"><strong>Select Distributors</strong></font></td>
							<td>
								<?
								//debug( selectMultiRecords("select * from admin where isCorrespondent='ONLY'") );
								if ( !empty($_REQUEST["assocDistributors"]) )
									$assocDistIDs = implode( ",", $_REQUEST["assocDistributors"] );

								$allDistributors = selectMultiRecords("select userID, name, username from admin where isCorrespondent='ONLY' and parentID > '0' and adminType='Agent' and agentStatus='Active'");
								?>
								<input type="radio" onClick="updateAssocDistControls();" name="associateAllDistributors" value="1" <?=( empty($assocDistIDs) ? "checked='checked'" : "" ); ?> /> All &nbsp;&nbsp
								<input type="radio" onClick="updateAssocDistControls();" id="letMeSelectAssocDist" name="associateAllDistributors" value="0" <?=( !empty($assocDistIDs) ? "checked='checked'" : "" ); ?> /> Let Me Select<br />
								Hold Control key for multiple selection<br />
								<select id="distList" name="assocDistributors[]" multiple="multiple" size="6">
									<?
									foreach( $allDistributors as $distData )
										echo "<option " . ( in_array($distData["userID"], $assocDistIDs) ? "selected='selected'" : "" ) . " value='" . $distData ["userID"] . "'>" . $distData ["name"] . " [" . $distData ["username"] . "]</option>";
									?>
								</select>
							</td>
						</tr>
						<?
					}
					/*** End of code #3428: Connect Plus - Select Distributors for Agents ****/
					?>

					<? if(CONFIG_AGENT_PAYMENT_MODE == "1"){ ?>
						</tr>
						<tr bgcolor="#ededed">
							<td><font color="#005b90"><strong>Payment Mode</strong></font></td>
							<td><select name="agentPaymentMode" id="agentPaymentMode">
									<option value="include">At End Of Month</option>
									<option value="exclude">At Source</option>

								</select>
								<script language="JavaScript">
									SelectOption(document.forms[0].agentPaymentMode, "<?=$_SESSION["agentPaymentMode"]; ?>");
								</script>
							</td>
						</tr>
					<? } ?>
					<tr bgcolor="#ededed">
						<td><font color="#005b90"><strong>Access From IP</strong></font></td>
						<td>For multiple please separate by comma.
							<input name="accessFromIP" type="text" id="accessFromIP" size="40" maxlength="255">
						</td>
					</tr>
					<? if(CONFIG_UPLOAD_DOCUMENT_ADD_SUB_AGENT == "1") { ?>
						<tr bgcolor="#ededed">
							<td align="left"><font color="#005b90"><b>Upload Documents&nbsp;</b></font></td>
							<td ><input type="checkbox" name="uploadImage" value="Y" <?=($_SESSION["uploadImageSub"]=="Y")?'checked="checked"':''?>>
								<input type="hidden" name="mode" value="Add">
							</td>
						</tr>
						<?
					}
					?>
					<? if (CONFIG_BACK_DATED == "1" && $_GET["ida"] != "Y"){ ?>
						<tr bgcolor="#ededed">
							<td><font color="#005b90"><strong>Right for Backdated</strong></font></td>
							<td>
								<select name="rights" id="rights" <? if ($agentType2 == 'admin') { ?>onChange="showBackDays();"<? } ?>>
									<option value="">No</option>
									<option value="Backdated">Yes</option>
								</select>
							</td>
						</tr>

						<?	if(CONFIG_PAYIN_CUSTOMER == '1') {?>
							<tr bgcolor="#ededed">
								<td width="17%"><font color="#005b90"><b>Payin Book&nbsp;</b></font></td>
								<td width="26%"><input type="checkbox" name="ch_payinBook" <? if($_SESSION["ch_payinBook"]!="") echo "checked"; ?> ></td>
							</tr>
							<tr bgcolor="#ededed">
								<td width="20%"><font color="#005b90"><b>Payin Book No.&nbsp;</b></font></td>
								<td width="37%"><input type="textbox" name="payinBook" value="<?=$_SESSION["payinBook"];?>" maxlength="<?=CONFIG_PAYIN_NUMBER ?>"></td>
							</tr>
						<? }?>


						<tr bgcolor="#ededed" id="howmanybackdays" style="visibility:hidden;position:absolute;">
							<td><font color="#005b90"><strong>How Many Back Days</strong></font></td>
							<td> <input type="text" name="backDays" id="backDays" size="3"> <i>By default,</i> 1</td>
						</tr>
					<? } ?>
					<? if (CONFIG_ONLINE_AGENT == "1" && $_GET["ida"] != "Y")
					{
						?>
						<tr bgcolor="#ededed">
							<td>
								<font color="#005b90"><strong>Assigned to Online Senders</strong></font>
							</td>
							<td>
								<select name="isOnline" id="isOnline">
									<option value="N" <? echo($_POST["isOnline"]=="Y" ? "" : "selected")?>>No</option>
									<option value="Y" <? echo($_POST["isOnline"]=="Y" ? "selected"  : "")?>>Yes</option>
							</td>
						</tr>

						<?
					}
					?>
					<?

					if(CONFIG_MANAGE_CLAVE == '1' && $_GET["ida"] == "Y")
					{
						?>

						<tr bgcolor="#ededed">
							<td>
								<font color="#005b90"><strong>Use Clave Range</strong></font>
							</td>
							<td>
								<select name="hasClave" id="hasClave">
									<option value="N" <? echo($_POST["hasClave"]=="Y" ? "" : "selected")?>>No</option>
									<option value="Y" <? echo($_POST["hasClave"]=="Y" ? "selected"  : "")?>>Yes</option>
							</td>
						</tr>
						<?
					}
					?>

					<? if(CONFIG_NOTES_AGENT_DIST == "1"){ ?>

						<tr bgcolor="#ededed">
							<td width="20%"><font color="#005b90"><b>Notes&nbsp;</b></font></td>
							<td ><input type="textarea" name="notes" value="<?=$_POST["notes"];?>" ></td>

						</tr>
					<? } ?>
					<tr bgcolor="#ededed">
						<td colspan="2" align="center">
							<input type="submit" value=" Save ">
							&nbsp;&nbsp;
							<input type="reset" value=" Clear ">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</form>
</table>
</body>
</html>