<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
if ($offset == "")
    $offset = 0;
$limit=20;

if ($_GET["newOffset"] != "") {
    $offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
$agentType2 = getAgentType();

$notifications=selectMultiRecords("SELECT * FROM notifications,admin,partnerList WHERE notifications.userID=admin.userID AND notifications.partnerID=partnerList.`APIPatrner/ThirdParty_ID` ORDER BY dateCreated DESC");
?>
<html>
<head>
    <title>Partner List</title>
    <script language="javascript" src="./javascript/functions.js"></script>
    <META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
    <LINK href="images/interface_admin.css" type=text/css rel=stylesheet>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" >
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <link rel="stylesheet" href="css/q-search.css">
    <link rel="stylesheet" href="css/custom.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" >
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Include the above in your HEAD tag -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <script language="javascript" src="./javascript/functions.js"></script>
    <link href="images/interface.css" rel="stylesheet" type="text/css">
    <script language="javascript">
        <!--
            function SelectOption(OptionListName, ListVal)
            {
                for (i=0; i < OptionListName.length; i++)
                {
                    if (OptionListName.options[i].value == ListVal)
                    {
                        OptionListName.selectedIndex = i;
                        break;
                    }
                }
            }
    </script>
    <script language="JavaScript">
        <!--
        function CheckAll()
        {

            var m = document.trans;
            var len = m.elements.length;
            if (document.trans.All.checked==true)
            {
                for (var i = 0; i < len; i++)
                {
                    m.elements[i].checked = true;
                }
            }
            else{
                for (var i = 0; i < len; i++)
                {
                    m.elements[i].checked=false;
                }
            }
        }
        -->
    </script>
    <style>
    .heading{
        margin-left: 83px;
        font-size: 15px;
        height: 40px;
        margin-top: -25px;

    }
        .record{
            margin-left: 83px;
            font-size: 11px;
            height: 50px;
            margin-top: -25px;

        }

    .dropbtn {
        background-color: #4CAF50;
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;
    }

    .dropdown {
        position: relative;
        display: inline-block;
        top: -7px;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .dropdown-content a:hover {background-color: #ddd;}

    .dropdown:hover .dropdown-content {display: block;}

    .dropdown:hover .dropbtn {background-color: #3e8e41;}
    </style>
</head>
<body>
<div>
    <div class="alert quick-search">Partner List</div>
    <div >
        <div><br>
            <div>
                <form action="partner-list.php" method="post" name="Search">
                    <div class="alert search">Search Filters</div>
                    <div class="form">
                        <br/><br/><br/><br/>
                        <div class="row">
                            <div class="col col-4">
                                <select class="form-control" name="searchByCountry" id="searchBy">
                                    <option>- Country -</option>
                                    <?php
                                        $query=selectMultiRecords("select * from countries order by countryName");
                                        foreach ($query as $row) {
                                            echo "<option>".$row['countryName']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col col-4">
                                <select class="form-control" name="searchByType" id="searchBy">
                                    <option>- Type -</option>
                                    <option>Sending</option>
                                    <option>Receiving</option>
                                </select>
                            </div>
                            <div class="col col-4"></div>
                        </div>
                        <div class="row" style="margin-left: -500px;margin-top: 60px">
                            <div class="col col-2"></div>
                            <div class="col col-2">
                                <input class="form-control submit-button"  type="submit" value="Search" name="search">
                            </div>
                            <div class="col col-2">
                                <input class="form-control submit-button"  type="button" value="Clear" name="clear">
                            </div>
                            <div class="col col-4"></div>
                        </div>
                    </div>
                </form>
            </div>
            <br>
            <div>
                <div class="alert submenu">&nbsp;There
                    are <?php
                    if($_POST['searchByType'] || $_POST['searchByCountry']) {
                        $query=selectMultiRecords("SELECT * FROM partnerList,clientList where  partnerList.clientID=clientList.ClientID and (payexRole = '" . $_POST['searchByType']."' or country='".$_POST['searchByCountry']."')");
                        $allCount=count($query);
                    }
                    else {
                        $query=selectMultiRecords("SELECT * FROM partnerList,clientList WHERE partnerList.clientID=clientList.ClientID");
                        $allCount=count($query);
                    }
                    echo $allCount;
                    ?> records.
                </div>
            </div>
            <br>
            <div class="row form-style-5 record" style="max-width: 1200px;">
                <div class="col col-9">
                    <?php
                    if (count($query) > 0) {;?>
                        Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($query));?></b>
                        of
                        <?=$allCount; ?>
                    <?php } ;?>
                </div>
                <?php
                if(!($agentType == 'SUPA'||$agentType == 'SUBA')) {?>
                <div class="col col-3">
                    <div class="count" style="background-color: red;width: 15px; height: 15px; ;border-radius: 15px; color: white;padding:1px 1px 2px 4px;visibility: hidden;position: relative; top: -8px"></div>
                    <div class="dropdown" onmouseover="notificationSeen();">
                        <i class="fa fa-bell" style="font-size: 18px; color: #009688">  Notification</i>
                        <div class="dropdown-content">
                            <?php
                            for ($i=0 ; $i< 2 ; $i++){?>
                                <?php
                                $row = $notifications[$i];
                                echo "<div style='padding:0px 5px 0px 5px; width: 250px ' >
                                <h5>".$row['APIPatrner/ThirdParty']."</h5>
                                <p>  
                                  ".$row['notificationTemplate']."
                                </p>
                                ".$row['dateCreated']."
                            </div><hr style='border-color: black;'>";
                            }
                            ?>
                            <a href='javascript:' onclick='allNotification();'>see more</a>
                            <script>
                                function allNotification() {
                                    window.open("notifications.php", '_blank', 'scrollbars=yes,toolbar=no,location=no,status=no,menubar=no,resizeable=yes,height=500,width=800');
                                }
                            </script>
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>
                <?php if ($prv >= 0) { ?>
                    <div class="col col-6">
                        <a href="<?php print $PHP_SELF . '?newOffset=0&sortBy='.$_GET['sortBy'];?>"><font color="#005b90">First</font></a>
                    </div>
                    <div class="col col-6">
                        <a href="<?php print $PHP_SELF . '?newOffset=$prv&sortBy='.$_GET['sortBy'];?>"><font color="#005b90">Previous</font></a>
                    </div>
                <?php } ?>
                <?php
                if ( ($nxt > 0) && ($nxt < $allCount) ) {
                    $alloffset = (ceil($allCount / $limit) - 1) * $limit;
                    ?>
                    <div class="col col-6">
                        <a href="<?php print $PHP_SELF . '?newOffset=$nxt&sortBy='.$_GET['sortBy'];?>"><font color="#005b90">Next</font></a>&nbsp;
                    </div>
                    <div class="col col-6">
                        <a href="<?php print $PHP_SELF . '?newOffset=$alloffset&sortBy='.$_GET['sortBy'];?>"><font color="#005b90">Last</font></a>&nbsp;
                    </div>
                <?php } ?>
            </div>
            <br><br>
            <div class="row form-style-5 heading" style="max-width: 1200px;">
                <div class="col col-2">
                    <a href="#"><font color="#005b90"><strong>Client Name</strong></font></a>
                </div>
                <div class="col col-1">
                    <a href="#"><font color="#005b90"><strong>Type</strong></font></a>
                </div>
                <div class="col col-2">
                    <a href="#"><font color="#005b90"><strong>Connection Type</strong></font></a>
                </div>
                <div class="col col-1">
                    <a href="#"><font color="#005b90"><strong>Countries</strong></font></a>
                </div>
                <div class="col col-1">
                    <a href="#"><font color="#005b90"><strong>Services</strong></font></a>
                </div>
                <?php
                if(!($agentType2=="SUPA" || $agentType2=="SUBA")){
                    ?>
                    <div class="col col-1">
                        <a href="#"><font color="#005b90"><strong>SPI</strong></font></a>
                    </div>
                    <div class="col col-1">
                        <a href="#"><font color="#005b90"><strong>Status</strong></font></a>
                    </div>
                    <div class="col col-2">
                        <a href="#"><font color="#005b90"><strong>Operations</strong></font></a>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="col col-2">
                        <a href="#"><font color="#005b90"><strong>Status</strong></font></a>
                    </div>
                    <div class="col col-2">
                        <a href="#"><font color="#005b90"><strong>Operations</strong></font></a>
                    </div>
                    <?php
                }
                ?>
            </div>
            <?php
            foreach ($query as $row) {
                echo "<br><br>
                <div class='row form-style-5 record' style='max-width: 1200px;'>
                
                <div class='col col-2 '>
                    <font ><strong>" . $row['APIPatrner/ThirdParty'] . "</strong></font>
                </div>
                <div class='col col-1'>
                    <font ><strong>" . $row['payexRole'] . "</strong></font>
                </div>
                <div class='col col-2'>
                    <font ><strong>" . $row['type'] . "</strong></font>
                </div>
                <div class='col col-1'>
                    <font ><strong>" . $row['country'] . "</strong></font>
                </div>
                <div class='col col-1'>
                    <font><strong></strong></font>
                </div>";

                $partnerID=$row['APIPatrner/ThirdParty_ID'];
                if(!($agentType2=="SUPA" || $agentType2=="SUBA")){
                echo "<div class='col col-1'>
                    <font><strong></strong></font>
                </div>
                <div class='col col-1'>
                    <font ><strong>" . $row['status'] . "</strong></font>
                </div>
                <div class='col col-2'>";
                    $status=$row['status'];
                    $cofigurPartner="cofigurPartner($partnerID,'$status')";
                if($row['status']=='Disable') {
                    echo "<font><a href='javascript:' onclick=$cofigurPartner style='color: #000;font-weight: bold'>Configuration </a>|<a href='partner-list-conf.php?partnerID=".$partnerID."' style='color: #000;font-weight: bold' > Activate </a></font>";
                } else {
                    echo "<font><a href='javascript:' onclick=$cofigurPartner style='color: #000;font-weight: bold'>Configuration </a>|<a href='partner-list-conf.php?partnerID=".$partnerID."&flag=disable' style='color: #000;font-weight: bold'> Disable </a></font>";
                }
                    ?>
                    <script>
                        function cofigurPartner(partnerID, status) {
                            window.open("configure-partner.php?partnerID="+partnerID+"&status='"+status+"'", '_blank', 'scrollbars=yes,toolbar=no,location=no,status=no,menubar=no,resizeable=yes,height=500,width=800');
                        }
                    </script>
                    <?
                echo "</div>";
                } else {
                    echo "<div class='col col-2'>
                        <strong>Request sent to admin to".$row['status']." </strong>
                </div >";
                    echo "<div class='col col-2'>";
                if($row['status']=='Disable') {
                    $requestsend="requestsend($partnerID,'activate')";
                    echo "<font><a href='javascript:' onclick=$requestsend style='color: #000;font-weight: bold'> Activate </a>| <a href='javascript:' onclick='updatePartner(".$row['APIPatrner/ThirdParty_ID'].")' style='color: #000;font-weight: bold'> Update</a></font>";
                } else {
                    $requestsend="requestsend($partnerID,'disable')";
                    echo "<font><a href='javascript:' onclick=$requestsend style='color: #000;font-weight: bold'> Disable </a>| <a href='javascript:' onclick='updatePartner(".$row['APIPatrner/ThirdParty_ID'].")' style='color: #000;font-weight: bold'> Update</a></font>";
                }?>
                    <script>
                        function requestsend(partnerID, flag) {
                            window.open("partner-list-conf.php?partnerID="+partnerID+"&flag="+flag, '_blank', 'scrollbars=no ,toolbar=no,location=no,status=no,menubar=no,resizeable=yes,height=200,width=500');
                        }
                        function updatePartner(partnerID) {
                            window.open("update-partner.php?partnerID="+partnerID, '_blank', 'scrollbars=no ,toolbar=no,location=no,status=no,menubar=no,resizeable=yes,height=200,width=500');
                        }
                    </script>
            <?
                echo "</div>";
                }

            echo "</div>";
            }
            ?>
            <br><br>
            <div class="row form-style-5 record" style="max-width: 1200px;">
                <div class="col col-12">
                    <?php if (count($query) > 0) {;?>
                        Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($query));?></b>
                        of
                        <?=$allCount; ?>
                    <?php } ;?>
                </div>
                <?php if ($prv >= 0) { ?>
                    <div class="col col-6">
                        <a href="<?php print $PHP_SELF . '?newOffset=0&sortBy='.$_GET['sortBy'];?>"><font color="#005b90">First</font></a>
                    </div>
                    <div class="col col-6">
                        <a href="<?php print $PHP_SELF . '?newOffset=$prv&sortBy='.$_GET['sortBy'];?>"><font color="#005b90">Previous</font></a>
                    </div>
                <?php } ?>
                <?php
                if ( ($nxt > 0) && ($nxt < $allCount) ) {
                    $alloffset = (ceil($allCount / $limit) - 1) * $limit;
                    ?>
                    <div class="col col-6">
                        <a href="<?php print $PHP_SELF . '?newOffset=$nxt&sortBy='.$_GET['sortBy'];?>"><font color="#005b90">Next</font></a>&nbsp;
                    </div>
                    <div class="col col-6">
                        <a href="<?php print $PHP_SELF . '?newOffset=$alloffset&sortBy='.$_GET['sortBy'];?>"><font color="#005b90">Last</font></a>&nbsp;
                    </div>
                <?php } ?>
            </div>
        </div>
</div>
</body>
</html>
<script>
    $(document).ready(function(){

        function load_unseen_notification(view = '')
        {
            $.ajax({
                url:"fetch.php",
                method:"POST",
                data:{view:view},
                dataType:"json",
                success:function(data)
                {
//                    console.log(data.unseen_notification[0].count);
//                    $('.dropdown').html(data.notification);
                    if(data.unseen_notification[0].count > 0)
                    {
                        $('.dropdown-content').html(data.notificationTemplate);
                        $('.count').css('visibility','visible').html(data.unseen_notification[0].count);
                    }
                }
            });
        }

        load_unseen_notification();
        setInterval(function(){
            load_unseen_notification();
        }, 5000);

    });
</script>
<script>
    function notificationSeen() {
        $.ajax({
            url:"changeNotification.php",
            method:"POST",
            success:function(data)
            {
                $('.count').css('visibility','hidden').html('');
                load_unseen_notification('yes');
            }
        });
    }
</script>
    