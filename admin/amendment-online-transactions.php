<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$agentID = $_SESSION["loggedUserData"]["userID"];

$_SESSION["back"] = "";
$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
	$offset = 0;
		
if($limit == 0)
	$limit=50;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
	
if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}

if ( CONFIG_CUSTOM_CUSTOMER_TRANSACTION == 1 )
{
	$customerTransactionPage = CONFIG_CUSTOMER_TRANSACTION_PAGE;
}
else
{
	$customerTransactionPage = "add-customer-transaction.php";
}
	
$moneyPaid = "";

	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	/*
	To empty transaction Variables
	
	*/
	$_SESSION["transType"] 		= "";
	$_SESSION["benAgentID"] 	= "";
	$_SESSION["customerID"] 	= "";
	$_SESSION["benID"] 			= "";
	$_SESSION["moneyPaid"] 		= "";
	$_SESSION["transactionPurpose"] = "";
	$_SESSION["other_pur"] = "";
	$_SESSION["fundSources"] 	= "";
	$_SESSION["refNumber"] 		= "";

	$_SESSION["transAmount"] 	= "";
	$_SESSION["exchangeRate"] 	= "";
	$_SESSION["exchangeID"] 	= "";
	$_SESSION["localAmount"] 	= "";
	$_SESSION["totalAmount"] 	= "";
	$_SESSION["IMFee"] 			= "";

	// resetting Session vars for trans_type Bank Transfer
	$_SESSION["bankName"] 		= "";
	$_SESSION["branchCode"] 	= "";
	$_SESSION["branchAddress"] 	= "";
	$_SESSION["swiftCode"] 		= "";
	$_SESSION["accNo"] 			= "";
	$_SESSION["ABACPF"] 		= "";
	$_SESSION["IBAN"] 			= "";
	
	$_SESSION["question"] 			= "";
	$_SESSION["answer"]     = "";	
	$_SESSION["tip"]     = "";	
	$_SESSION["currencyCharge"] = "";
	$_SESSION["transDate"]="";

// When search button is clicked. Then filteres are used to display Records.
if($_POST["Submit"] == "Search" || $_GET["search"] == "Search")
{
	$queryonline = "select * from ". TBL_TRANSACTIONS." as t where 1 ";
	$queryonlineCnt = "select count(transID) from ". TBL_TRANSACTIONS." as t where 1 ";

	if ($_POST["moneyPaid"] != "") {
		$moneyPaid = $_POST["moneyPaid"];
	} else if ($_GET["moneyPaid"] != "") {
		$moneyPaid = $_GET["moneyPaid"];
	}
	
	if($_POST["transID"] != "")
	{
		if($_POST["transID"] != "")
		{
			$id = $_POST["transID"];
		}
		if($_POST["searchBy"] != "")
		{
			$by = $_POST["searchBy"];
		}
	}
	elseif($_GET["transID"] != "")
	{
		if($_GET["transID"] != "")
		{
			$id = $_GET["transID"];
		}
		if($_GET["searchBy"] != "")
		{
			$by = $_GET["searchBy"];
		}
	}
		
	if($_POST["nameType"] != ""){
		$nameType = $_POST["nameType"];
	}
	elseif($_GET["nameType"] != "")
	{
		$nameType = $_GET["nameType"];
	}

	if($id != "")
	{
		switch($by)
		{
			case 0:
			{
				$queryonline = "select *  from ". TBL_TRANSACTIONS . " as t where (t.refNumber = '$id' OR  t.refNumberIM = '$id') ";
				$queryonlineCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t where (t.refNumber = '$id' OR  t.refNumberIM = '$id') ";
				break;
			}
			case 1:
			{
				if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){
					if($nameType == "fullName"){
						$name = split(" ",$id);
						$nameClause = "firstName LIKE '$name[0]' and lastName LIKE  '$name[1]' ";
					}else{
						$nameClause = " ".$nameType." LIKE '".$id."%' ";
					}
					$queryonline = "select *  from ". TBL_TRANSACTIONS . " as t, cm_customer as cm where t.customerID =  cm.c_id AND ".$nameClause." ";
					$queryonlineCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t, cm_customer as cm where t.customerID =  cm.c_id AND ".$nameClause." ";
				}else{
					$queryonline = "select *  from ". TBL_TRANSACTIONS . " as t, cm_customer as cm where t.customerID =  cm.c_id  ";
					$queryonlineCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t, cm_customer as cm where t.customerID =  cm.c_id ";
					$fn="FirstName";
					$mn="MiddleName";
					$ln="LastName"; 		
					$alis="cm";
					$q=searchNameMultiTables($id,$fn,$mn,$ln,$alis);
					$queryonline .= $q;
					$queryonlineCnt .= $q;
				}
/*				if($agentType == "Branch Manager"){
					$queryonline .= " and t.custAgentParentID ='$agentID' ";
				}
				$queryonline .= "  order by t.transDate DESC";
				$countOnlineRec = count($onlinecustomer2 = selectMultiRecords($queryonline));
				//echo("Count got is  ->".$countOnlineRec."--");
				if($offset < $countOnlineRec)
				{
					$queryonline .= " LIMIT $offset , $limit";
					$onlinecustomer = selectMultiRecords($queryonline);
				}*/
				break;
			}
			case 2:
			{
				if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){
					if($nameType == "fullName"){
						$name = split(" ",$id);
						$nameClause = "firstName LIKE '$name[0]' and lastName LIKE  '$name[1]' ";
					}else{
						$nameClause = " ".$nameType." LIKE '".$id."%'";
					}
					$queryonline = "select *  from ". TBL_TRANSACTIONS . " as t,  cm_beneficiary as cb where t.benID =  cb.benID and $nameClause ";
					$queryonlineCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t,  cm_beneficiary as cb where t.benID =  cb.benID and $nameClause ";
				}else{
					// searchNameMultiTables function (Niaz)
					$fn="firstName";
					$mn="middleName";
					$ln="lastName"; 		
					$alis="cb";
					$q=searchNameMultiTables($id,$fn,$mn,$ln,$alis);
					$queryonline = "select *  from ". TBL_TRANSACTIONS . " as t,  cm_beneficiary as cb where t.benID =  cb.benID ".$q." ";
					$queryonlineCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t,  cm_beneficiary as cb where t.benID =  cb.benID ".$q." ";
					//$query = "select *  from ". TBL_TRANSACTIONS . " t, ". TBL_BENEFICIARY ." b where t.benID =  b.benID and (b.firstName like '$id%' OR b.lastName like '$id%') and t.createdBy != 'CUSTOMER' ";
					//$queryonline = "select *  from ". TBL_TRANSACTIONS . " as t,  cm_beneficiary as cb where t.benID =  cb.benID and (cb.firstName like '$id%' OR cb.lastName like '$id%') and t.createdBy = 'CUSTOMER' ";
				}
	/*			if($agentType == "Branch Manager"){
					$queryonline .= " and t.custAgentParentID ='$agentID' ";
				}
				$queryonline .= "  order by t.transDate DESC";
				$countOnlineRec = count($onlinecustomer2 = selectMultiRecords($queryonline));
				//echo("Count got is  ->".$countOnlineRec."--");
				if($offset < $countOnlineRec)
				{
					 $queryonline .= " LIMIT $offset , $limit";
				$onlinecustomer = selectMultiRecords($queryonline);
				}*/
				break;
			}
			case 3:
			{
				$queryonline = "select *  from ". TBL_TRANSACTIONS . " t, ". TBL_ADMIN_USERS ." u where t.custAgentID =  u.userID and u.name like '$id%' ";
				$queryonlineCnt = "select count(transID) from ". TBL_TRANSACTIONS . " t, ". TBL_ADMIN_USERS ." u where t.custAgentID =  u.userID and u.name like '$id%' ";
				break;
			}

		}
	}
	
	// Forcing to display only Authorized,Amended transactions of CUSTOMER by ashahid.
	if($queryonline!=""){
		$queryonline .= " and (t.transStatus ='Authorize' || t.transStatus ='Amended') AND t.createdBy = 'CUSTOMER' ";
		$queryonlineCnt .= " and (t.transStatus ='Authorize' || t.transStatus ='Amended') AND t.createdBy = 'CUSTOMER' ";
	}
	if($agentType == "Branch Manager"){
		$queryonline .= " and custAgentParentID ='$agentID' ";
		$queryonlineCnt .= " and custAgentParentID ='$agentID' ";
	}
	
	if ($moneyPaid != "") {
		$queryonline .= " and (t.moneyPaid = '".$moneyPaid."')";
		$queryonlineCnt .= " and (t.moneyPaid = '".$moneyPaid."')";
	}
	if(CONFIG_SHARE_OTHER_NETWORK == '1')
	{
		$queryonline .= " and t.transID not in (select localTrans from ".TBL_SHARED_TRANSACTIONS." where generatedLocally = 'N')";	
		$queryonlineCnt.= " and t.transID not in (select localTrans from ".TBL_SHARED_TRANSACTIONS." where generatedLocally = 'N') ";
	}

$countOnlineRec = countRecords($queryonlineCnt);
$totalContent = $countOnlineRec;
$queryonline .= " LIMIT $offset , $limit";
$onlinecustomer = selectMultiRecords($queryonline);
if($_GET["allCount"] == ""){
	$allCount = $countOnlineRec;
}else{
	$allCount = $_GET["allCount"];
}
$rangeOffset = count($onlinecustomer)+$offset;


/* $other = $offset + $limit;
 if($other > $countOnlineRec)
 {
	if($offset < $countOnlineRec)
	{
		$offset2 = 0;
		$limit2 = $offset + $limit - $countOnlineRec;	
	}elseif($offset >= $countOnlineRec)
	{
		$offset2 = $offset - $countOnlineRec;
		$limit2 = $limit;
	}
  $query .= " LIMIT $offset2 , $limit2";

	$contentsTrans = selectMultiRecords($query);
 }*/
}
else{
	$queryonline = "select * from ". TBL_TRANSACTIONS." as t where 1 ";
	$queryonlineCnt = "select count(transID) from ". TBL_TRANSACTIONS." as t where 1 ";

	// Forcing to display only Authorized,Amended transactions of CUSTOMER by ashahid.
	if($queryonline!=""){
		$queryonline .= " and (t.transStatus ='Authorize' || t.transStatus ='Amended') AND t.createdBy = 'CUSTOMER' ";
		$queryonlineCnt .= " and (t.transStatus ='Authorize' || t.transStatus ='Amended') AND t.createdBy = 'CUSTOMER' ";
	}		
	if($agentType == "Branch Manager"){
		$query .= " and custAgentParentID ='$agentID' ";
		$queryonlineCnt .= " and custAgentParentID ='$agentID' ";
	}
	if(CONFIG_SHARE_OTHER_NETWORK == '1')
	{
		$queryonline .= " and t.transID not in (select localTrans from ".TBL_SHARED_TRANSACTIONS." where generatedLocally = 'N')";	
		$queryonlineCnt.= " and t.transID not in (select localTrans from ".TBL_SHARED_TRANSACTIONS." where generatedLocally = 'N') ";
	}
	
	$allCount = countRecords($queryonlineCnt);
	
	$queryonline .= " order by transDate DESC";
 	$queryonline .= " LIMIT $offset , $limit";	
	$onlinecustomer = selectMultiRecords($queryonline);
	
	$rangeOffset = count($onlinecustomer)+$offset;
}


$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

?>
<html>
<head>
	<title>Amend Transactions</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}	
-->
</script>


<script language="JavaScript">
<!--

function nameTypeCheck(){
	
	
			
			
			
			if(document.getElementById('searchBy').value == '1' || document.getElementById('searchBy').value == '2'){
					
				
				document.getElementById('nameTypeRow').style.display = '';
			}	else{
				
				
				document.getElementById('nameTypeRow').style.display = 'none';
				}
			
}

-->
</script>
    <style type="text/css">

<table>
	<tr>
.style2 {
	color: #005B90;
	font-weight: bold;
}
-->
    </style>
</head>
<body onLoad="nameTypeCheck();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="C0C0C0"><strong><font color="#FFFFFF" size="2">Amend Transactions</font></strong></td>
  </tr>
  	<tr>
		<td>
			<table width="255" border="1" cellpadding="5" bordercolor="#666666" align="center">
        <form action="amendment-online-transactions.php" method="post" name="Search">
          <tr>
            <td width="286" bgcolor="#C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
          </tr>
          <tr>
            <td align="center" nowrap>
            	<input name="transID" type="text" id="transID" value="<?=$id?>" size="15" style="font-family:verdana; font-size: 11px; width:100"> 
				<?
				$senderLabel = "Sender";
				$senderLabelN = $_arrLabels[$senderLabel];	
				$AgentLabel = "Agent";
				$AgentLabelN = $_arrLabels[$AgentLabel];	
				?>
              <select name="searchBy" id="searchBy" onChange="nameTypeCheck();">
                  <option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
                  <option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By <?=$senderLabelN;?> Name</option>
                  <option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
						<?	if($agentType == "admin" || $agentType == "System" || $agentType == "Call" || $agentType == "Admin Manager")
							{
							?>
							 	<option value="3" <? echo ($_POST["searchBy"] == 3 ? "selected" : "")?>>By <?=$AgentLabelN;?> Name</option>
							<?
							}
						?>
              </select>
		<?	if (CONFIG_PAYMENT_MODE_FILTER == "1") {  ?>
			<br>Payment Mode
      <select name="moneyPaid" id="moneyPaid" style="font-family:verdana; font-size: 11px;" >
          <option value="">- Select Mode -</option>
          <option value="By Cash" <? echo ($moneyPaid == "By Cash" ? "selected" : "") ?>>By Cash</option>
          <option value="By Cheque" <? echo ($moneyPaid == "By Cheque" ? "selected" : "") ?>>By Cheque</option>
          <option value="By Bank Transfer" <? echo ($moneyPaid == "By Bank Transfer" ? "selected" : "") ?>>By Bank Transfer</option>
		  </select>
		<?	}  ?>
	</tr>
	
	<? if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){ ?>
   	
			   	<tr align="center" id="nameTypeRow"><td>
			   		Name Type: <select name="nameType">
			   			<option value="firstName" <? echo ($nameType == "firstName" ? "selected" : "")?>>First Name</option>
			   			<option value="lastName" <? echo ($nameType == "lastName" ? "selected" : "")?>>Last Name</option>
			   			<option value="fullName" <? echo ($nameType == "fullName" ? "selected" : "")?>>First Name & Last Name</option>
   		
  	</td></tr>
  	<?} ?>
	
	
	<tr>
		<td align="center">
                <input type="submit" name="Submit" value="Search"></td>
 				</td>      
          </tr>
        </form>
      </table>
		</td>
	</tr>
  <tr>
    <td align="center"><? //if($agentType!='admin'){echo ("You can only update the transaction within one hour of creation.");}?><br>      <br>
		
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
        <form action="amendment-online-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">
			<tr>
    		<td>
    			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td width="250">
                    <?php if (count($onlinecustomer) > 0) {?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($rangeOffset);?></b>
                    of
                    <?=$allCount; ?>
                  </td>
                <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&search=Search&transID=$id&moneyPaid=$moneyPaid&searchBy=$by&nameType=".$nameType."&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&search=Search&transID=$id&moneyPaid=$moneyPaid&searchBy=$by&nameType=".$nameType."&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>
                  </td>
				  				
                <?php } ?>
                <?php
								if ( ($nxt > 0) && ($nxt < $allCount) ) {
									$alloffset = (ceil($allCount / $limit) - 1) * $limit;
								?>
                  <td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&search=Search&transID=$id&moneyPaid=$moneyPaid&searchBy=$by&nameType=".$nameType."&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&transID=$id&moneyPaid=$moneyPaid&searchBy=$by&search=Search&nameType=".$nameType."&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
				 					
                  <?php } 
                	}
                  ?>
                </tr>
              </table>
    		</td>
    	</tr>
	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo(count($onlinecustomer));?> records to Update.</span></td>
        </tr>
		<?
		if(count($onlinecustomer) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?> </span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
				<?
					if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
					{
						?>
							<td><span class="style1">Cash Amount</span></td>
							<td><span class="style1">Cheque Amount</span></td>
						<?
					}
				?>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			   <td><span class="style1">Sender Name </span></td>
			  <td><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1">Sender Agent</span></td>
			  <td width="50" align="center">&nbsp;</td>
			  </tr>
			<? for($i=0;$i < count($onlinecustomer);$i++)
			{
				?>
               <tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $onlinecustomer[$i]["transID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumberIM"]?></td>
				  <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($onlinecustomer[$i]["totalAmount"])?></td>
				  
				  <? 
				  if($onlinecustomer[$i]["createdBy"] == "CUSTOMER") {
                $transBene = selectFrom("select * from cm_beneficiary  where benID='".$onlinecustomer[$i]["benID"]."'");
		 						$benefName = $transBene["firstName"];
		 					  $benefName .= " ";
							  $benefName .= $transBene["lastName"];
							  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$i]["custAgentID"]."'");
               $customerContent = selectFrom("select firstName, lastName from cm_customer where c_id ='".$onlinecustomer[$i]["customerID"]."'");?>
              <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
              <td width="100" bgcolor="#FFFFFF"><? echo $benefName; ?></td>
              <? }else{?>
		  
				  	<? $customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$onlinecustomer[$i]["customerID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <? $beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$onlinecustomer[$i]["benID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  <? } ?>
				  
				  <? $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$i]["custAgentID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo ucfirst($agentContent["name"])?></td>
				  
				  <? if($onlinecustomer[$i]["createdBy"] == "CUSTOMER") {?>
				  	  <td width="50" align="center" bgcolor="#FFFFFF"><a href="<?=$customerTransactionPage;?>?transID=<? echo $onlinecustomer[$i]["transID"]?>&act=Amended&back=amendment-online-transactions.php" class="style2"><strong><font color="#006699">Amend</strong></font></a></td>
					<? }else{ ?>
						<td width="50" align="center" bgcolor="#FFFFFF"><a href="<?=$transactionPage?>?transID=<? echo $onlinecustomer[$i]["transID"]?>&act=Amended&back=amendment-online-transactions.php"><strong><font color="#006699">Amend</strong></font></a></td>
						<? 
						}
						?>
			    </tr>
                <?
			}
			?> 
			
			<tr bgcolor="#FFFFFF">
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td bgcolor="#FFFFFF">&nbsp;</td>
			  <td width="100" align="center">&nbsp;</td>
			  <td width="50" align="center">&nbsp;</td>
			  <td width="50" align="center">&nbsp;</td>
			  <td width="50" align="center">&nbsp;</td>
			  <td width="50" align="center">&nbsp;</td>
			  </tr>
			<?
			} // greater than zero
			?>
          </table></td>
        </tr>
		</form>
    </table></td>
  </tr>

</table>
</body>
</html>