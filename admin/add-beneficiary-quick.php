<?
session_start();
include ("../include/config.php");
include ("security.php");
include ("javaScript.php");
$agentType = getAgentType();
if($agentType == "TELLER")
{
	$parentID = $_SESSION["loggedUserData"]["cp_ida_id"];
	}else{
	$parentID = $_SESSION["loggedUserData"]["userID"];
}
$date_time = date('d-m-Y  h:i:s A');
$userDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where username='$username'");
 			//echo("select IDAcountry from ".TBL_ADMIN_USERS." where userID = '".$parentID."'");

$countryBasedFlag = false;
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}
	$beneficiaryNameFlag = false;
if(defined("CONFIG_BENEFICIARY_NAME_WHOLE_FIELD_BEN") && CONFIG_BENEFICIARY_NAME_WHOLE_FIELD_BEN=="1"){
	$beneficiaryNameFlag = true;
}
session_register("agentID2");
session_register("Title");
session_register("firstName");
session_register("lastName");
session_register("middleName");
session_register("IDType");
session_register("IDNumber");
session_register("Otherid_name");
session_register("Otherid");
session_register("Address");
session_register("Address1");
session_register("Country");
session_register("City");
session_register("Zip");
session_register("State");
session_register("Phone");
session_register("Mobile");
session_register("bentEmail");
session_register("benID");
session_register("from");
session_register("amount_transactions");
session_register("Citizenship");
session_register("IDissuedate");
session_register("IDexpirydate");
session_register("secMiddleName");
session_register("S/O");
session_register("sonOfType");
session_register("iban");
session_register("ibanorbank");
session_register("serviceType");
session_register("beneficiaryName");
if ( is_array( $_REQUEST["IDTypesValuesData"] ) )
{
	$_SESSION["IDTypesValuesData"] = $_REQUEST["IDTypesValuesData"];
}
if ($_GET["val"] != "")
	$_SESSION["amount_transactions"] = $_GET["val"];

$cpf = 0;
if($_POST["sel"] == "country")
{
	if ($_POST["Country"] == CONFIG_CPF_COUNTRY)
	{
		$cpf = 1;	
	}else{
		$_SESSION["CPF"] = "";
	}
}

if ($_GET["notEndSessions"] != "" && $_GET["success"] == "" && CONFIG_CPF_ENABLED == "1" && strtoupper($_SESSION["Country"]) == strtoupper(CONFIG_CPF_COUNTRY))
{
	$cpf = 1;	
}

$_SESSION["benID"] = $_GET["benID"];
$_SESSION["from"] = $_GET["from"];

$benID = $_SESSION["benID"];
if($_GET["r"]!=""){
	$_SESSION["error"]="";
}
// This is to ensure session values assigned when page refreshed like country change or something like that.
if($_GET["benID"] == "" && $_GET["msg"] == "")
{
	if ($_GET["notEndSessions"] == "")
	{
	$_SESSION["Title"] = "";
	$_SESSION["other_title"] = "";
	$_SESSION["firstName"] = "";
	$_SESSION["lastName"] = "";
	$_SESSION["middleName"] = "";
	$_SESSION["S/O"];
	$_SESSION["sonOfType"];
	$_SESSION["secMiddleName"] = "";
	$_SESSION["IDType"] = "";
	$_SESSION["IDNumber"] = "";
	$_SESSION["Address"] = "";
	$_SESSION["Address1"] = "";
	$_SESSION["Address2"] = "";
	$_SESSION["Country"] = "";
	$_SESSION["City"] = "";
	$_SESSION["Zip"] = "";
	$_SESSION["State"] = "";
	$_SESSION["Phone"] = "";
	$_SESSION["Mobile"] = "";
	$_SESSION["benEmail"] = "";
	$_SESSION["bankName"] 		= "";
	$_SESSION["bankCode"] 		= "";
	$_SESSION["branchName"] 		= "";
	$_SESSION["branchCity"] 		= "";
	$_SESSION["branchCode"] 	= "";
	$_SESSION["branchAddress"] 	= "";
	$_SESSION["swiftCode"] 		= "";
	$_SESSION["accNo"] 			= "";
	$_SESSION["ABACPF"] 		= "";
	$_SESSION["IBAN"] 			= "";
	$_SESSION["transType"] = "";
	$_SESSION["Citizenship"] = "";
	$_SESSION["IDissuedate"] = "";
	$_SESSION["IDexpirydate"] = "";
	$_SESSION["userType"] = "";
	$_SESSION["CPF"] = "";
	$_SESSION["iban1"] = "";
	$_SESSION["ben_bank_id"] = "";
	$_SESSION["bankName1"] = "";
	$_SESSION["account1"] = "";
	$_SESSION["branchCode1"] = "";
	$_SESSION["branchAddress1"] = "";
	$_SESSION["swiftCode1"] = "";
	$_SESSION["country1"] = "";
	$_SESSION["bankId1"] = "";
	$_SESSION["iban"] = "";
	$_SESSION["ibanorbank"] = "";
	$_SESSION["emailAddress"] = "";
	$_SESSION["serviceType"] = "";
	$_SESSION["beneficiaryName"] = "";
}

if ($_POST["from"] != "")
	$_SESSION["from"] = $_POST["from"];

if ($_POST["Citizenship"] != "")
	$_SESSION["Citizenship"] = $_POST["Citizenship"];

	if ($_POST["IDissuedate"] != "")
	$_SESSION["IDissuedate"] = $_POST["IDissuedate"];

	if ($_POST["IDexpirydate"] != "")
	$_SESSION["IDexpirydate"] = $_POST["IDexpirydate"];
	
	
if ($_POST["benID"] != "")
	$_SESSION["benID"] = $_POST["benID"];


if ($_POST["agentID"] != "")
	$_SESSION["agentID2"] = $_POST["agentID"];
//elseif ($_POST["agentID"] == "")
	//$_SESSION["agentID2"] = "";


if ($_POST["firstName"] != "")
	$_SESSION["firstName"] = $_POST["firstName"];

//elseif ($_POST["firstName"] == "")
	//$_SESSION["firstName"] = "";
if ($_POST["lastName"] != "")
	$_SESSION["lastName"] = $_POST["lastName"];
//elseif ($_POST["lastName"] == "")
	//$_SESSION["lastName"] = "";
if ($_POST["middleName"] != "")
	$_SESSION["middleName"] = $_POST["middleName"];

if ($_POST["S/O"] != "")
	$_SESSION["S/O"] = $_POST["S/O"];

if ($_POST["sonOfType"] != "")
	$_SESSION["sonOfType"] = $_POST["sonOfType"];
	
if($_POST["secMiddleName"] != "")
	$_SESSION["secMiddleName"] = $_POST["secMiddleName"];	
//elseif ($_POST["middleName"] == "")
	//$_SESSION["middleName"] = "";
if(CONFIG_TITLE_OFF != "1")
{
	if ($_POST["Title"] != "")
		$_SESSION["Title"] = $_POST["Title"];
}

if ($_POST["other_title"] != "")
	$_SESSION["other_title"] = $_POST["other_title"];
//elseif ($_POST["Title"] == "")
	//$_SESSION["Title"] = "";
if ($_POST["IDType"] != "")
	$_SESSION["IDType"] = $_POST["IDType"];
//elseif ($_POST["IDType"] == "")
	//$_SESSION["IDType"] = "";
if ($_POST["IDNumber"] != "")
	$_SESSION["IDNumber"] = $_POST["IDNumber"];
//elseif ($_POST["IDNumber"] == "")
	//$_SESSION["IDNumber"] = "";
	if ($_POST["Otherid_name"] != "")
	$_SESSION["Otherid_name"] = $_POST["Otherid_name"];
//elseif ($_POST["Otherid_name"] == "")
	//$_SESSION["Otherid_name"] = "";
if ($_POST["Otherid"] != "")
	$_SESSION["Otherid"] = $_POST["Otherid"];
//elseif ($_POST["Otherid"] == "")
	//$_SESSION["Otherid"] = "";
if ($_POST["Address"] != "")
	$_SESSION["Address"] = $_POST["Address"];
//elseif ($_POST["Address"] == "")
	//$_SESSION["Address"] = "";
if ($_POST["Address1"] != "")
	$_SESSION["Address1"] = $_POST["Address1"];
//elseif ($_POST["Address1"] == "")
	//$_SESSION["Address1"] = "";
if ($_POST["Country"] != "")
	$_SESSION["Country"] = $_POST["Country"];

if($countryBasedFlag){
	if($_SESSION["Country"]=="")
		$_SESSION["Country"] = $_GET["Country"];
	if($_REQUEST["collectionPointID"]!="")
		$collectionPointIDV = $_REQUEST["collectionPointID"];
}
if($beneficiaryNameFlag){
	if ($_POST["beneficiaryName"] != "")
		$_SESSION["beneficiaryName"] = $_POST["beneficiaryName"];
}

if(!empty($_POST["CPF"]))
	$_SESSION["CPF"]=$_POST["CPF"];
	
	
//elseif ($_POST["Country"] == "")
	//$_SESSION["Country"] = "";
if ($_POST["City"] != "")
	$_SESSION["City"] = $_POST["City"];
//elseif ($_POST["City"] == "")
	//$_SESSION["City"] = "";
if ($_POST["Zip"] != "")
	$_SESSION["Zip"] = $_POST["Zip"];
//elseif ($_POST["Zip"] == "")
	//$_SESSION["Zip"] = "";
if ($_POST["State"] != "")
	$_SESSION["State"] = $_POST["State"];
//elseif ($_POST["State"] == "")
	//$_SESSION["State"] = "";
if ($_REQUEST["Phone"] != "")
	$_SESSION["Phone"] = $_REQUEST["Phone"];
//elseif ($_POST["Phone"] == "")
	//$_SESSION["Phone"] = "";
if ($_POST["Mobile"] != "")
	$_SESSION["Mobile"] = $_POST["Mobile"];
//elseif ($_POST["Mobile"] == "")
	//$_SESSION["Mobile"] = "";
if ($_POST["benEmail"] != "")
	$_SESSION["benEmail"] = $_POST["benEmail"];
//elseif ($_POST["benEmail"] == "")
	//$_SESSION["benEmail"] = "";
	
if ($_POST["userType"] != "")
	$_SESSION["userType"] = $_POST["userType"];

if ($_POST["iban"] != "")
	$_SESSION["iban"] = $_POST["iban"];


if (!empty($_GET["ben_bank_id"]))
	$_SESSION["ben_bank_id"] = $_GET["ben_bank_id"];

if(!empty($_POST["bankName"]))
	$_SESSION["bankName1"] = $_POST["bankName"];	
if(!empty($_POST["account"]))
	$_SESSION["account1"] = $_POST["account"];	
if(!empty($_POST["branchName"]))
	$_SESSION["branchCode1"] = $_POST["branchName"];	
if(!empty($_POST["iban"]))
	$_SESSION["iban1"] = $_POST["iban"];	
if(!empty($_POST["swiftCode"]))
	$_SESSION["swiftCode1"] = $_POST["swiftCode"];	
if(!empty($_POST["accountType"]))
	$_SESSION["accountType1"] = $_POST["accountType"];	
if(!empty($_POST["branchAddress"]))
	$_SESSION["branchAddress1"] = $_POST["branchAddress"];	
if(!empty($_POST["bankId"]))
	$_SESSION["bankId1"] = $_POST["bankId"];	
if(!empty($_POST["country1"]))
	$_SESSION["country1"] = $_POST["country1"];	
if(!empty($_POST["ibanbank"]))
	$_SESSION["ibanorbank"] = $_POST["ibanbank"];	
if(!empty($_REQUEST["serviceType"]) && isset($_REQUEST["serviceType"]))
	$_SESSION["serviceType"] = $_REQUEST["serviceType"];
	
	$getCustomerID = $_REQUEST["customerID"];
//debug($_SESSION["serviceType"]);
//debug($_SESSION["Country"]);
}
elseif(!empty($benID)) // Edit Beneificiary
{
$strQuery = "SELECT * FROM beneficiary where benID = $benID";
	$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
	$rstRow = mysql_fetch_array($nResult);				
	//debug($rstRow);
		$_SESSION["agentID2"] = $rstRow["agentID"];
		$_SESSION["firstName"] = $rstRow["firstName"];
		$_SESSION["lastName"] = $rstRow["lastName"];
		$_SESSION["middleName"] = $rstRow["middleName"];
		$_SESSION["S/O"] = $rstRow["SOF"];
		$_SESSION["sonOfType"] = $rstRow["SOF_Type"];
		$_SESSION["secMiddleName"] = $rstRow["secMiddleName"];
		$_SESSION["Title"] = $rstRow["Title"];
		$_SESSION["other_title"] = $rstRow["other_title"];
		$_SESSION["IDType"] = $rstRow["IDType"];
		$_SESSION["IDNumber"] = $rstRow["IDNumber"];
		$_SESSION["Otherid_name"] = $rstRow["otherId_name"];
		$_SESSION["Otherid"] = $rstRow["otherId"];
		$_SESSION["Address"] = $rstRow["Address"];
		$_SESSION["Address1"] = $rstRow["Address1"];
		$_SESSION["Address2"] = $rstRow["address2"];
		$_SESSION["Country"] = $rstRow["Country"];
		$_SESSION["City"] = $rstRow["City"];
		$_SESSION["CPF"] = $rstRow["CPF"];
		$_SESSION["Zip"] = $rstRow["Zip"];
		$_SESSION["State"] = $rstRow["State"];
		$_SESSION["Phone"] = $rstRow["Phone"];
		$_SESSION["Mobile"] = $rstRow["Mobile"];
		$_SESSION["emailAddress"] = $rstRow["email"];
		$_SESSION["Citizenship"] = $rstRow["Citizenship"];
		$_SESSION["IDissuedate"] = $rstRow["IDissuedate"];
		$_SESSION["IDexpirydate"] = $rstRow["IDexpirydate"];
		$_SESSION["userType"] = $rstRow["userType"];
		$_SESSION["iban"] = $rstRow["IBAN"];
		$_SESSION["ibanorbank"] = $rstRow["IbanOrBank"];
		if($beneficiaryNameFlag){
			$_SESSION["beneficiaryName"] = $rstRow["beneficiaryName"];
		}
		$getCustomerID = $rstRow["customerID"];
		if($countryBasedFlag){
			$_SESSION["serviceType"] = $rstRow["serviceType"];
			$collectionPointIDV = $rstRow["collectionPointID"];
			if($_GET["collectionPointID"]!="" && $_GET["collectionPointID"]!=0)
				$collectionPointIDV = $_GET["collectionPointID"];
		}
		//debug($_SESSION["serviceType"]);
		/**
		 * If the beneficiary is with bank details
		 */
//		if(CONFIG_BEN_BANK_DETAILS == "1")
		if(CONFIG_BEN_BANK_DETAILS == "1")
		{
			/**
			 * Fetching IBAN number from the beneficiary table
			 */

			$_SESSION["iban"] = $rstRow["IBAN"];

			$benBanksDataQry = "select * from ".TBL_BEN_BANK_DETAILS." where benID=$benID";
			$banBanksData = selectFrom($benBanksDataQry);

			if(count($banBanksData) > 0)
			{
				$_SESSION["account1"] = $banBanksData["accountNo"];
				$_SESSION["accountType1"] = $banBanksData["accountType"];
				
				$_SESSION["branchCode1"] = $banBanksData["branchCode"];
				$_SESSION["branchAddress1"] = $banBanksData["branchAddress"];
				$_SESSION["swiftCode1"] = $banBanksData["swiftCode"];
				$_SESSION["iban1"]  = $banBanksData["IBAN"];
				$benBanksDataQry = "select * from ".TBL_BANKS." where id='".$banBanksData["bankId"]."'";
				$banBanksData = selectFrom($benBanksDataQry);
				
				/**
				 * Storing the bank id in case of edit beneficuary
				 */
				$_SESSION["ben_bank_id"] = $banBanksData["id"];
				
				$_SESSION["bankName1"] = $banBanksData["name"];
				/*
				$_SESSION["branchCode1"] = $banBanksData["branchCode"];
				$_SESSION["branchAddress1"] = $banBanksData["branchAddress"];
				$_SESSION["swiftCode1"] = $banBanksData["swiftCode"];
				*/
				$_SESSION["bankId1"] = $banBanksData["bankId"];
				$_SESSION["country1"] = $banBanksData["country"];
			}
			elseif(!empty($_SESSION["ben_bank_id"]))
			{
				/**
				 * Getting all the bank fields based on the selected bank from bank search
				 */
				$benBanksDataQry = "select * from ".TBL_BANKS." where id=".$_SESSION["ben_bank_id"];
				$banBanksData = selectFrom($benBanksDataQry);

				$_SESSION["bankName1"] = $banBanksData["name"];
				//$_SESSION["branchCode1"] = $banBanksData["branchCode"];
				//$_SESSION["branchAddress1"] = $banBanksData["branchAddress"];
				//$_SESSION["swiftCode1"] = $banBanksData["swiftCode"];
				$_SESSION["bankId1"] = $banBanksData["bankId"];
				$_SESSION["country1"] = $banBanksData["country"];
			}
			//print_r($_SESSION);
		}
		if(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER == "1"){
				/**
				 * Fetching IBAN number from the beneficiary table
				 */
	
				$benBanksDataQry = "select * from ".TBL_BEN_BANK_DETAILS." where benID=$benID";
				$banBanksData = selectFrom($benBanksDataQry);
				if($banBanksData["bankId"] != "")
				{
					$_SESSION["account1"] = $banBanksData["accountNo"];
					$_SESSION["accountType1"] = $banBanksData["accountType"];
					
					$_SESSION["branchCode1"] = $banBanksData["branchCode"];
					$_SESSION["branchAddress1"] = $banBanksData["branchAddress"];
					$_SESSION["swiftCode1"] = $banBanksData["swiftCode"];
					
					$benBanksDataQry = "select * from ".TBL_BANKS." where id='".$banBanksData["bankId"]."'";
					$banBanksData = selectFrom($benBanksDataQry);
					
					/**
					 * Storing the bank id in case of edit beneficuary
					 */
					$_SESSION["ben_bank_id"] = $banBanksData["id"];
					
					$_SESSION["bankName1"] = $banBanksData["name"];
					$_SESSION["country1"] = $banBanksData["country"];
				}
				elseif(!empty($_SESSION["ben_bank_id"]))
				{
					/**
					 * Getting all the bank fields based on the selected bank from bank search
					 */
					$benBanksDataQry = "select * from ".TBL_BANKS." where id=".$_SESSION["ben_bank_id"];
					$banBanksData = selectFrom($benBanksDataQry);
	
					$_SESSION["bankName1"] = $banBanksData["name"];
					$_SESSION["country1"] = $banBanksData["country"];
				}
		}

   if (CONFIG_CPF_ENABLED == "1" && strtoupper($_SESSION["Country"]) == strtoupper(CONFIG_CPF_COUNTRY))
   {
   	$cpf = 1;	
   }
   if($_POST["sel"] == "country")
	{
		if ($_POST["Country"] != CONFIG_CPF_COUNTRY)
		{
			$cpf = 0;	
			
		}
	}

	if($_POST["Country"] != "")		 
		  $_SESSION["Country"] = $_POST["Country"];
	else
		{
		  $_SESSION["Country"] = $rstRow["Country"];
		  if($_SESSION["Country"] == "")
		  	$_SESSION["Country"] = $_POST["Country"];
		}
	
	if($_POST["transType"] == "Select Transaction Type")		 
		  $_SESSION["transType"] = $_POST["transType"];
	else
		{
		  $_SESSION["transType"] =$rstRow["transType"];
		  if($_SESSION["transType"] == "")
		  	$_SESSION["transType"] = $_POST["transType"];
		}
		
		$_SESSION["Country"] = ucfirst(strtolower($_SESSION["Country"]));
		
		$strQuery = "SELECT * FROM cm_bankdetails where benID = $benID";
		$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
		$rstRow = mysql_fetch_array($nResult);				
	
		$_SESSION["bankName"] = $rstRow["bankName"];
		$_SESSION["branchCode"] = $rstRow["branchCode"];
		$_SESSION["bankCode"] = $rstRow["BankCode"];
		$_SESSION["branchCity"] = $rstRow["BranchCity"];
		$_SESSION["branchAddress"] = $rstRow["branchAddress"];
		$_SESSION["branchName"] = $rstRow["BranchName"];
		$_SESSION["swiftCode"] = $rstRow["swiftCode"];
		$_SESSION["accNo"] = $rstRow["accNo"];
		$_SESSION["ABACPF"] = $rstRow["ABACPF"];
		$_SESSION["IBAN"] = $rstRow["IBAN"];

}
if($_POST["Country"] != "")	
	{
		
		if($_SESSION["Country"] != $_POST["Country"])
			{
			
				$_SESSION["bankName"]="";
				$_SESSION["branchName"]="";
				$_SESSION["branchAddress"]="";
				$_SESSION["branchCode"]="";
			}
		 // if($_SESSION["Country"]=='India')	
		  //	$_SESSION["branchCode"]="";
		  $_SESSION["Country"] = $_POST["Country"];
		  
		  
	
	}
	
	/*else
		{
		  $_SESSION["Country"] = $rstRow["Country"];
		  if($_SESSION["Country"] == "")
		  	{
		    	$_SESSION["Country"] = $_POST["Country"];
				$_SESSION["branchCode"]="";
			}
		}*/
////////Bank Name

if ($_POST["bankName"] != "")
		{
		$_SESSION["bankName"] = $_POST["bankName"];
				$_SESSION["branchName"]="";
				$_SESSION["branchAddress"]="";
				$_SESSION["branchCode"]="";
		}
/*else
		{
		  $_SESSION["bankName"] = $rstRow["bankName"];
		  if($_SESSION["bankName"] == "")
		  {
		    	$_SESSION["bankName"] = $_POST["bankName"];
				$_SESSION["branchCode"]="";
			}
		}
	*/	
/////Bank Code
/*if ($_POST["bankCode"] != "")
	{
	$_SESSION["bankCode"] = $_POST["bankCode"];
	$_SESSION["branchCode"]="";
	}*/
/*else
		{
		  $_SESSION["bankCode"] = $rstRow["BankCode"];
		  if($_SESSION["bankCode"] == "")
		  	{
		    	$_SESSION["bankCode"] = $_POST["bankCode"];
				$_SESSION["branchCode"]="";
			}	
		}*/
//////Branch Address			
	if ($_POST["branchAddress"] != "")
		{
		$_SESSION["branchAddress"] = $_POST["branchAddress"];
		$_SESSION["branchCode"]="";
		
		}
		
	/*else
		{
		  $_SESSION["branchAddress"] = $rstRow["branchAddress"];
		  if($_SESSION["branchAddress"] == "")
		  	{
		    	$_SESSION["branchAddress"] = $_POST["branchAddress"];
				$_SESSION["branchCode"]="";
			}	
		}*/
/////Branch Name
	if ($_POST["branchName"] != "")
		{
		$_SESSION["branchName"] = $_POST["branchName"];
		$_SESSION["branchCode"]="";
		$_SESSION["branchAddress"]="";
		}
		
	/*else
		{
		  $_SESSION["branchName"] = $rstRow["BranchName"];
		  if($_SESSION["branchName"] == "")
		  {
		    	$_SESSION["branchName"] = $_POST["branchName"];
				$_SESSION["branchCode"]="";
			}	
		}*/
///////		
if (CONFIG_CPF_ENABLED == "1" && strtoupper($_SESSION["Country"]) == strtoupper(CONFIG_CPF_COUNTRY))
{
	if($_POST["Submit"] == "Validate")
	{
		
		/**
		 * This method expires and now updated with client specified criteria through code
		 *		 $checkValidity = validCPF($_POST["CPF"]);
		 */
		require_once("lib/classValidateCpfCpnj.php");
		$validate = new VALIDATE; 
		/*
		if(!$validate->cpf($_POST["CPF"]))
		{
			insertError(BE18);	
			
		}else{
			insertError(BE19);
			$_GET["success"] ="Y";	
			}
		*/
		
		if($_POST["cpfcpnj"] == "CPF")
		{
			if(!$validate->cpf($_POST["CPF"]) && $_POST["CPF"] != "00000000000")
			{
				insertError(BE18);	
				
			}
			else
			{
				insertError(BE19);	
				$_GET["success"] ="Y";
			}
		}
		elseif($_POST["cpfcpnj"] == "CPNJ")
		{
			if(!$validate->cnpj($_POST["CPF"]) && $_POST["CPF"] != "00000000000000")
			{
				insertError("This is not a valid CNPJ number");	
				
			}
			else
			{
				insertError("CNPJ number is valid");	
				$_GET["success"] ="Y";
			}
		}
		
		
		
			$_SESSION["CPF"] = $_POST["CPF"];
			$_GET["msg"] = "Y";
			
	}
}	

if(!empty($_GET["ben_bank_id"]))
{
	$_SESSION["ben_bank_id"] = $_GET["ben_bank_id"];
	/**
	 * Getting all the bank fields based on the selected bank from bank search
	 */
	$benBanksDataQry = "select * from ".TBL_BANKS." where id=".$_SESSION["ben_bank_id"];
	$banBanksData = selectFrom($benBanksDataQry);
	
	$_SESSION["bankName1"] = $banBanksData["name"];
	//$_SESSION["branchCode1"] = $banBanksData["branchCode"];
	//$_SESSION["branchAddress1"] = $banBanksData["branchAddress"];
	//$_SESSION["swiftCode1"] = $banBanksData["swiftCode"];
	$_SESSION["country1"] = $banBanksData["country"];
	$_SESSION["bankId1"] = $banBanksData["bankId"];
	
}
$routineDataFlag = false;
//debug($_SESSION["Country"]);
if(defined("CONFIG_COUNTRY_BASED_ROUTINE_TRANSACTION") && CONFIG_COUNTRY_BASED_ROUTINE_TRANSACTION=="1"){
	$countryRights = ($_SESSION["Country"]!="" ? $_SESSION["Counry"]:$_REQUEST["Country"]);
	if ( defined("CONFIG_DEFAULT_COUNTRY") && CONFIG_DEFAULT_COUNTRY == 1 && defined("CONFIG_DEFAULT_COUNTRY_VALUE") && ($countryRights=="" || empty($countryRights))){
		$countryRights == CONFIG_DEFAULT_COUNTRY_VALUE;
	}
	$queryRights = "select * from setRoutineRight where country = '".$countryRights."' and rightValue ='CountryRtn' and status='Enabled'";
	$routineData = selectFrom($queryRights);
	if($routineData["rightValue"]!=""){
		$routineDataFlag = true;
	}
		
}

$showIbanBankSelectRow = true;
$useIbanBankStructure  = true;
$useServiceType = "";
if($countryBasedFlag){
	$destCountryService = $_SESSION["Country"];
	$useServiceType = $_SESSION["serviceType"];
	if ( defined("CONFIG_DEFAULT_COUNTRY") && CONFIG_DEFAULT_COUNTRY == 1 && defined("CONFIG_DEFAULT_COUNTRY_VALUE") && $destCountryService=="")
	{
		$destCountryService = CONFIG_DEFAULT_COUNTRY_VALUE;
	}
	$destCountryQSer = selectFrom("select countryId from ".TBL_COUNTRY." where countryName = '".$destCountryService."'");
	$fromCountryQSer = selectFrom("select countryId from ".TBL_COUNTRY." as count,".TBL_CUSTOMER." as cust where count.countryName = cust.Country and cust.customerID='".$getCustomerID."'");
	$contentsService = selectMultiRecords("select serviceId,serviceAvailable from " . TBL_SERVICE_NEW." where 1 and fromCountryId = '".$fromCountryQSer["countryId"]."' and toCountryId = '".$destCountryQSer["countryId"]."' and currencyRec!=''");
	$allServices = array();
	for($as=0;$as<count($contentsService);$as++){
		$allServices[] = $contentsService[$as]["serviceAvailable"];
	}
	if($contentsService[0]["serviceAvailable"]!=""){
		if(in_array($_SESSION["serviceType"],$allServices)){
			$useServiceType = $_SESSION["serviceType"];
		}
		else{
			$useServiceType = $contentsService[0]["serviceAvailable"];
		}
	}
	else{
		$useServiceType = "";
	}
	if($_GET["serviceType"]!=""){
		$useServiceType = $_GET["serviceType"];
	}
	$showIbanBankSelectRow = false;
	if(strtoupper($useServiceType)=="IBAN"){
		$_SESSION['ibanorbank'] = "iban";
	}
	elseif(strtoupper($useServiceType)=="BANK TRANSFER" || strtoupper($useServiceType)=="BANK DETAILS" ){
		$_SESSION['ibanorbank'] = "bank";
	}
	elseif(strtoupper($useServiceType)=="BOTH"){
		$showIbanBankSelectRow = true;
		if($_SESSION['ibanorbank']!="" && ($_SESSION['ibanorbank']=="iban" || $_SESSION['ibanorbank']=="bank")){
			$_SESSION['ibanorbank'] = $_SESSION['ibanorbank'];
		}
		if($_GET['ibanorbank']!=""){
			$_SESSION['ibanorbank'] = $_GET['ibanorbank'];
		}
	}
	else{
		$useIbanBankStructure  = false;
	}
/*	debug($_SESSION["ben_bank_id"]);
	$_SESSION["ben_bank_id"] = $_GET["ben_bank_id"];
	debug($_SESSION["ben_bank_id"]);*/
	/**
	 * Getting all the bank fields based on the selected bank from bank search
	 */
//		$benBanksDataQry = "select * from ".TBL_BEN_BANK_DETAILS." where id='".$_SESSION["ben_bank_id"]."'";
/*	$benBanksDataQry = "select * from ".TBL_BANKS." where id='".$_SESSION["ben_bank_id"]."'";
	if(!empty($benID) && $_GET["ben_bank_id"]=="")
		$benBanksDataQry = "select bb.id,bb.bankId,b.name,bb.branchCode,bb.branchAddress,bb.accountNo,bb.swiftCode,bb.accountType from ".TBL_BANKS." as b,".TBL_BEN_BANK_DETAILS." as bb where b.id=bb.bankId AND bb.benID='".$benID."'";
	$banBanksData = selectFrom($benBanksDataQry);
	$_SESSION["bankName1"] = $banBanksData["name"];
	$_SESSION["swiftCode1"] = $banBanksData["swiftCode"];
	$_SESSION["branchCode1"] = $banBanksData["branchCode"];
	$_SESSION["branchAddress1"] = $banBanksData["branchAddress"];
	$_SESSION["account1"] = $banBanksData["accountNo"];
	$_SESSION["accountType1"] = $banBanksData["accountType"];
	$_SESSION["country1"] = $_SESSION["Country"];*/


	// MasterPayex - Multiple ID Types

	// Fetch available ID Types
	if(defined("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY") && CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY=="1" && $countryBasedFlag){
		$query = "select * from id_types
					where active='Y'
					and show_on_ben_page='Y' ";	
		if($useServiceType!=""){
			$idTypeServices = selectFrom("select multiple_ids from " . TBL_SERVICE_NEW." where 1 and fromCountryId = '".$fromCountryQSer["countryId"]."' and toCountryId = '".$destCountryQSer["countryId"]."' and currencyRec!='' AND serviceAvailable ='".$useServiceType."'");
			if($idTypeServices["multiple_ids"]!=""){
				$idTypeServicesVal = substr($idTypeServices["multiple_ids"],0,strlen($idTypeServices["multiple_ids"])-1);
				if($countryBasedFlag)
					$query.= " AND id IN(".$idTypeServicesVal.") ";
					$IDTypes = selectMultiRecords( $query );
			}
		}
		// If its an update sender request, find the sender's ID Type values.
		if ( !empty( $_REQUEST["benID"] ) )
		{
			// Now fetch values against available id types.
			$numberOfIDTypes = count( $IDTypes );
			for( $i = 0; $i < $numberOfIDTypes; $i++ )
			{
				$IDTypeValuesQ = "select * from user_id_types
										where
											id_type_id = '" . $IDTypes[$i]["id"] . "'
										and	user_id = '" . $_REQUEST["benID"] . "'
										and user_type = 'B'";
				$IDTypeValues = selectFrom( $IDTypeValuesQ );
				if ( !empty( $IDTypeValues ) )
				{
					$IDTypes[$i]["values"] = $IDTypeValues;
				}
			}
		}
	}
/*
 * @Ticket #4794
 */
	$userIdTypesNotesFlag = false;
	if(defined("CONFIG_SHOW_MULTIPLE_ID_TYPES_NOTES") && (CONFIG_SHOW_MULTIPLE_ID_TYPES_NOTES=="1" || strstr(strtoupper(CONFIG_SHOW_MULTIPLE_ID_TYPES_NOTES),$agentType))){
		$userIdTypesNotesFlag = true;
	}

	// End of code against #3299: MasterPayex - Multiple ID Types
}

$phoneMandatoryCountFlag = false;
if(CONFIG_BEN_PHONE_MANDATORY_ONLY_UK=="1"){
	$phoneMandatoryCountFlag = true;
}
?>
<html>
<head>
<title>Add Quick Beneficiary</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
<? if($countryBasedFlag){  ?>
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the Beneficiary's country from the list.");
        theForm.Country.focus();
        return false;
    }
	if(document.getElementById('serviceType') == null){
    	alert("Please Use Services for this Country before creating Beneficiary. Or select Other Country.");
        theForm.Country.focus();
        return false;	
	}
	if(document.getElementById('serviceType').value == ""){
    	alert("Please select the Beneficiary's Service Type.");
        theForm.serviceType.focus();
        return false;
    }
<? } ?>	

<? if(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER == "1" && $useIbanBankStructure){  ?>	
	var ibanOrBank = document.getElementById('IbanOrBank').value;
	var ibnstatus = document.getElementById('ibnStatus').value;
	var ibanValue = document.getElementById("iban").value;
	var selection = theForm.ibanbank; // radio buttons
	var ibanbankRadio = "";
	for (i=0; i<selection.length; i++){
		if (selection[i].checked == true){
			ibanbankRadio = "y";
		}
	}
	var serviceTypeObj = document.getElementById("serviceType");
	var serviceTypeVal = "";
	if(serviceTypeObj!=null){
		var serviceTypeVal = serviceTypeObj.value;
	}
	if(serviceTypeVal=="IBAN" || serviceTypeVal=="Bank Transfer"){
		ibanbankRadio="y";
	}
	if(ibanbankRadio !=""){
		if(ibanOrBank == "i")
		{		
			if(ibanValue=="" || ibnstatus==0)
			{
					alert('The IBAN number '+ document.getElementById("iban").value +' is invalid.');
					theForm.iban.focus();
					return false;
			}
	
		}
		else if(ibanOrBank == "b"){
			if(theForm.bankName.value == "")
			{
				alert("Please enter the bank name.");
				theForm.bankName.focus();
				return false;
			}
			if(theForm.account.value == "")
			{
				alert("Please enter the account number.");
				theForm.account.focus();
				return false;
			}
			/* if(theForm.country1.value == "")
			{
				alert("Please enter the bank country.");
				theForm.country1.focus();
				return false;
			} */
			if(theForm.branchAddress.value == "")
			{
				alert("Please enter the branch address.");
				theForm.branchAddress.focus();
				return false;
			}
		}
	}
	else{
		if(serviceTypeObj!=null && serviceTypeVal==""){
			alert("Please select an Option from Service(s).");			
		}
		else{
			alert("Please select an Option for IBAN or Bank Details.");
		}
		return false;
	}
<? } ?>
	<?
	if (CONFIG_CUST_NAME_SEQUENCE == '1') {  
	?>
		if(theForm.lastName.value == "" || IsAllSpaces(theForm.lastName.value)){
    	alert("Please provide the Beneficiary's <? echo (CONFIG_SURNAME == "1" ? CONFIG_SURNAME_VAL : "Last Name")?>.");
        theForm.lastName.focus();
        return false;
    }	
    if(theForm.firstName.value == "" || IsAllSpaces(theForm.firstName.value)){
    	alert("Please provide the Beneficiary's first name.");
        theForm.firstName.focus();
        return false;
    }
	<?
	}else{
	?>
		if(theForm.firstName.value == "" || IsAllSpaces(theForm.firstName.value)){
    	alert("Please provide the Beneficiary's first name.");
        theForm.firstName.focus();
        return false;
    }
	 if(theForm.lastName.value == "" || IsAllSpaces(theForm.lastName.value)){
    	alert("Please provide the Beneficiary's <? echo (CONFIG_SURNAME == "1" ? CONFIG_SURNAME_VAL : "Last Name")?>.");
        theForm.lastName.focus();
        return false;
    }	
	<?
	}
	?>
<?
	if (defined("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY") && CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY == '1' && !empty( $IDTypes ))
	{
		?>
			if ( checkIdTypes( theForm ) != true ){
				return false;
			}	
		<?
	}
	?>
	<?
		/***** Code added by Usman Ghani aginst ticket #3471 Now Transfer - Beneficiary Address *****/
		if ( defined("CONFIG_BENEFICIARY_ADDRESS_MENDATORY")
			&& CONFIG_BENEFICIARY_ADDRESS_MENDATORY == 1 )
		{
		?>
			if(theForm.address.value == "")
			{
				alert("Please provide the Beneficiary's address.");
				theForm.address.focus();
				return false;
			}
		<?
		}
		/***** End of code aginst ticket #3471 Now Transfer - Beneficiary Address *****/
	?>
	
   
  <? 
if(CONFIG_ID_MANDATORY == '1'){?>   
	if(theForm.IDNumber.value == "" || IsAllSpaces(theForm.IDNumber.value)){
  alert("Please provide the Beneficiary's ID Number.");
  theForm.IDNumber.focus();
  return false;
  }
<?
}?>  
    	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the Beneficiary's country from the list.");
        theForm.Country.focus();
        return false;
    }



<?	
if ( CONFIG_COMPUL_FOR_AGENTLOGINS == '1' && ($agentType == 'SUBA' || $agentType == 'SUBAI' || $agentType == 'SUPA' || $agentType == 'SUPAI')) {  
?>
	if(theForm.Address.value == "" || IsAllSpaces(theForm.Address.value)){
    	alert("Please provide the customer's address line 1.");
        theForm.Address.focus();
        return false;
    }
	<? /*against #3639 by khola*/
	if ( CONFIG_POSTCODE_NOTCOMPUL_FOR_AGENTLOGINS != 1 ) {
	?>
		if(theForm.Zip.value == "" || IsAllSpaces(theForm.Zip.value)){
    		alert("Please provide the customer's Zip/Postal Code.");
	        theForm.Zip.focus();
    	    return false;
    	}
	<?
	}
	?>
<?	
}  
?>


<? if(CONFIG_CITY_NON_COMPULSORY != '1'){  ?>
 	        if(theForm.City.value == ""){
 	        alert("Please provide the Beneficiary's city.");
 	        theForm.City.focus();
 	        return false;
 	    }
 	 <? }?>

<?
	if ( defined("CONFIG_POST_CODE_MENDATORY") && CONFIG_POST_CODE_MENDATORY == 1 /* against #3467 by Usman Ghani for now transfer */)
	{
	?>
		if(theForm.Zip.value == "" || IsAllSpaces(theForm.Zip.value))
		{
			alert("Please provide the customer's Zip/Postal Code.");
			theForm.Zip.focus();
			return false;
		}
	<?		
	}
?>
	
    <? if ($cpf == 1){ ?>
	if(theForm.CPF.value == "" || IsAllSpaces(theForm.CPF.value)){
    	alert("Please provide the CPF number.");
        theForm.CPF.focus();
        return false;
    }	
  <? } ?>



 <? if($phoneMandatoryCountFlag){  ?>
	 <? if($_SESSION["Country"]=="United Kingdom"){  ?>
		if(theForm.Phone.value == "" || IsAllSpaces(theForm.Phone.value)){
			alert("Please provide the Beneficiary's phone number.");
			theForm.Phone.focus();
			return false;
		}
	<? } ?>
 <? }elseif(CONFIG_PHONE_NON_COMPULSORY != '1'){  ?>
	if(theForm.Phone.value == "" || IsAllSpaces(theForm.Phone.value)){
    	alert("Please provide the Beneficiary's phone number.");
        theForm.Phone.focus();
        return false;
    }
 <? }?>
<?
if(CONFIG_MOBILE_MANDATORY == '1')
{?>
	if(theForm.Mobile.value == "" || IsAllSpaces(theForm.Mobile.value)){
    	alert("Please provide the beneficiary's mobile number.");
        theForm.Mobile.focus();
        return false;
  }
<?
}?>

<?
	// Added by Usman Ghani aginst ticket #3473 Now Transfer - Phone/Mobile validation
	
	if ( defined("CONFIG_PHONE_NUMBER_NUMERIC")
		&& CONFIG_PHONE_NUMBER_NUMERIC == 1 )
	{
		?>
		/*
			if ( validatePhoneMobile() == false )
				return false;

			if ( theForm.phoneType != null && theForm.phoneType.value == "" )
			{
				alert("Please select phone type.");
				theForm.phoneType.focus();
				return false;
			}
		*/
		<?
	}
	// End of code against ticket #3473 Now Transfer - Phone/Mobile validations
?>
 <? if($phoneMandatoryCountFlag &&  defined("CONFIG_PHONES_AS_DROPDOWN") && CONFIG_PHONES_AS_DROPDOWN == 1 ){  ?>
	 <? if($_SESSION["Country"]=="United Kingdom"){  ?>
			if ( theForm.phoneType != null && theForm.phoneType.value == "" )
			{
				alert("Please select phone type.");
				theForm.phoneType.focus();
				return false;
			}
			
			var Phone=document.getElementById("Phone");
			if(document.getElementById("phoneType").value == "phone")
			{
				if((Phone.value==null)||(Phone.value==""))
				{
					alert("Please Enter your Phone Number");
					Phone.focus();
					return false;
				}
				if (phoneNumberRelatedValidation(Phone.value, "p")==false)
				{
					alert("Please Enter a Valid Phone Number");
					Phone.focus();
					return false;
				}
			}
			else if(document.getElementById("phoneType").value == "mobile")
			{
				if((Phone.value==null)||(Phone.value==""))
				{
					alert("Please Enter your Mobile Number");
					Phone.focus();
					return false;
				}
				if (phoneNumberRelatedValidation(Phone.value, "m")==false)
				{
					alert("Please Enter a Valid Mobile Number");
					Phone.focus();
					return false;
				}
			}
<?
	// Added by Usman Ghani aginst ticket #3472: Now Transfer - Phone/Mobile
		}
	}elseif ( defined("CONFIG_PHONES_AS_DROPDOWN") && CONFIG_PHONES_AS_DROPDOWN == 1 )
	{
		?>
		/*
			if ( theForm.Phone != null && theForm.Phone.value == "" )
			{
				alert("Please provide phone number.");
				theForm.Phone.focus();
				return false;
			}
		*/
			if ( theForm.phoneType != null && theForm.phoneType.value == "" )
			{
				alert("Please select phone type.");
				theForm.phoneType.focus();
				return false;
			}
			
			var Phone=document.getElementById("Phone");
			if(document.getElementById("phoneType").value == "phone")
			{
				if((Phone.value==null)||(Phone.value==""))
				{
					alert("Please Enter your Phone Number");
					Phone.focus();
					return false;
				}
				if (phoneNumberRelatedValidation(Phone.value, "p")==false)
				{
					alert("Please Enter a Valid Phone Number");
					Phone.focus();
					return false;
				}
			}
			else if(document.getElementById("phoneType").value == "mobile")
			{
				if((Phone.value==null)||(Phone.value==""))
				{
					alert("Please Enter your Mobile Number");
					Phone.focus();
					return false;
				}
				if (phoneNumberRelatedValidation(Phone.value, "m")==false)
				{
					alert("Please Enter a Valid Mobile Number");
					Phone.focus();
					return false;
				}
			}
		<?
	}
	// End of code against ticket #3472: Now Transfer - Phone/Mobile
?>
	<? if(CONFIG_IBAN_ON_QUICK_BEN == "1"){  ?>
		
		//alert($("#ibanbank").val());
		//alert($('input[@name="ibanbank"]:checked').val());
		
		if($('input[@name="ibanbank"]:checked').val() == "iban")
		{
			validateIban();
			if( document.getElementById('ibnStatus').value == 0 )
			{
				alert('The IBAN number '+ document.getElementById("iban").value +' is invalid.');
				theForm.iban.focus();
				return false;
			}
		}
<?
}

	// Javascript code to validate bank details goes here.
	if(CONFIG_BEN_BANK_DETAILS == "1")
	{
		?>
			/*
			if ( theForm.bankId.value == "" )
			{
				alert('Please provide bank ID.');
				theForm.bankId.focus();
				return false;
			}
			*/
			if(theForm.iban.value != "")
			{
				//validateIban();
				if( document.getElementById('ibnStatus').value == 0 )
				{
						alert('The IBAN number '+ document.getElementById("iban").value +' is invalid.');
						return false;
				}
			}
					
			if(theForm.bankName.value == "")
			{
				alert("Please enter the bank name.");
				theForm.bankName.focus();
				return false;
			}
			/*
			if(theForm.branchName.value == "")
			{
				alert("Please enter the branch name / number.");
				theForm.branchName.focus();
				return false;
			}
			*/
			if(theForm.account.value == "")
			{
				alert("Please enter the account number.");
				theForm.account.focus();
				return false;
			}
			/*
			if(theForm.branchAddress.value == "")
			{
				alert("Please enter the branch address.");
				theForm.branchAddress.focus();
				return false;
			}
			*/
			if(theForm.country1.value == "")
			{
				alert("Please enter the bank country.");
				theForm.country1.focus();
				return false;
			}
		<? 
	}
?>
return true;
}
<?
	// Added by Usman Ghani aginst ticket #3473 Now Transfer - Phone/Mobile validation
	
	if ( defined("CONFIG_PHONE_NUMBER_NUMERIC")
		&& CONFIG_PHONE_NUMBER_NUMERIC == 1 )
	{
		?>
			function validatePhoneMobile()
			{
				Phone = document.getElementById("Phone");
				Mobile = document.getElementById("Mobile");
				if ( Phone != null && Phone.value != ""
					&& !( /^\d+$/.test(Phone.value) ) ) // Checking, through regular expression, if its not a numeric value.
				{
					alert("Please provide only numerals in phone number.");
					Phone.focus();
					return false;
				}

				if ( Mobile != null && Mobile.value != ""
					&& !( /^\d+$/.test(Mobile.value) ) ) // Checking, through regular expression, if its not a numeric value.
				{
					alert("Please provide only numerals in mobile number.");
					theForm.Mobile.focus();
					return false;
				}
				return true;
			}

			function phoneEventHandler( e )
			{
				keynum = -1;
				if(window.event) // IE
				{
					keynum = e.keyCode;
				}
				else if(e.which) // Netscape/Firefox/Opera
				{
					keynum = e.which;
				}

				if ( 
						keynum > -1
					&& 	keynum != 8 	// Backspace
					&& 	keynum != 13	// Enter
					&& 	keynum != 27	// Escape
					 )
				{
					validatePhoneMobile();
				}
			}
		<?
	}
	// End of code against ticket #3473 Now Transfer - Phone/Mobile validations
?>
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
   
function clearExpDate() {	// [by Jamshed]
	document.frmBenificiary.IDexpirydate.value = "";	
}


function newCaption()
{
	if(document.getElementById("userType"))
	{
	var userType = document.getElementById("userType").value;	
		
	if(userType == "Business User" )
	{
		document.getElementById("caption").innerHTML = "<font color='#005b90'><b>CNPJ Number<font color='#ff0000'>*</font>&nbsp;</b></font>";	
		document.getElementById("cpfcpnj").value = "CPNJ";
	}
	else
	{
		document.getElementById("caption").innerHTML = "<font color='#005b90'><b>CPF Number<font color='#ff0000'>*</font>&nbsp;</b></font>";	
		document.getElementById("cpfcpnj").value = "CPF";
	}
}
}

function checkNumber()
{
	if(document.getElementById("userType"))
		newCaption();
	
	
	
<? /*
	if(!empty($_SESSION["userType"]))
	{ 
		if($_SESSION["userType"] == "Business User")
		{
?>
			document.getElementById("caption").innerHTML = "<font color='#005b90'><b>CNPJ Number<font color='#ff0000'>*</font>&nbsp;</b></font>";	
			document.getElementById("cpfcpnj").value = "CPNJ";	
<? 
		}
		else
		{
?>
			document.getElementById("caption").innerHTML = "<font color='#005b90'><b>CPF Number<font color='#ff0000'>*</font>&nbsp;</b></font>";	
			document.getElementById("cpfcpnj").value = "CPF";
<?
		}
	}
	*/
	if($_SESSION['ibanorbank']=="iban"){?>
//	$("#formSubmitButton").attr('disabled', true); 
		validateIban();
		
	<? }?>
}

function validateIban()
{
	var vts;
	vts = document.getElementById("iban").value;
	if(vts == "")
	{
    	document.getElementById('ibnStatus').value=0;
		document.getElementById("benIbanValidStatus").innerHTML = "";
		document.getElementById('formSubmitButton').disabled = true;	
	}
	else{
			$.ajax({
			   type: "GET",
			   url: "validateIban.php",
			   data: "iban_num="+vts,
			   beforeSend: function(XMLHttpRequest){
			   document.getElementById('formSubmitButton').disabled = true;
			   },
			   success: function(msg){
			    if(msg == 1)
			    {
			    	document.getElementById('ibnStatus').value=1;
			    	document.getElementById("benIbanValidStatus").innerHTML = "<font color='green'>The IBAN number is valid.</font>";
			    	document.getElementById('formSubmitButton').disabled = false;
			    }
			    else
			    {
			    	document.getElementById('ibnStatus').value=0;
			    	document.getElementById("benIbanValidStatus").innerHTML = "<font color='red'>You have entered an invalid IBAN number.</font>";
			    	document.getElementById('formSubmitButton').disabled = true;
			    }
			   }
			 });
	}	

		
/*	}
	else
	{
		document.getElementById('ibnStatus').value=1;
		document.getElementById("benIbanValidStatus").innerHTML = "";
		//document.getElementById('formSubmitButton').disabled = true;
	}*/
	
}


// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;
var maxDigitsInIPhoneNumber = 11;

var validWorldPhoneChars = phoneNumberDelimiters;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}
function trim(s)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not a whitespace, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (c != " ") returnString += c;
    }
    return returnString;
}
function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function phoneNumberRelatedValidation(strPhone, strType)
{
	var strPhone=trim(strPhone);
	var s=stripCharsInBag(strPhone,validWorldPhoneChars);
	if(strType == "p")
		return (isInteger(s) && (s.length == minDigitsInIPhoneNumber || s.length == maxDigitsInIPhoneNumber));
	else
		return (isInteger(s) && s.length == maxDigitsInIPhoneNumber);
}
function showHideIbanBank(id){
	
	var IbanOrBankOrReset = id.value;
	var selection = document.frmBenificiary.ibanbank; // radio buttons
	if(IbanOrBankOrReset == "iban"){
		document.getElementById("bankData").style.display="none";
		document.getElementById("ibanData").style.display="block";
		document.getElementById("IbanOrBank").value="i";
		validateIban();
	}
	else if(IbanOrBankOrReset == "bank"){
		document.getElementById("ibanData").style.display="none";
		document.getElementById("bankData").style.display="block";
		document.getElementById("IbanOrBank").value="b";
		document.getElementById('formSubmitButton').disabled = false;
	}
	// This functionality if Clear Button is pressed.
	else if(IbanOrBankOrReset == "reset"){
		document.getElementById("searchBankName").value="";
		document.getElementById("country1").value="";
		document.getElementById("bankName").value="";
		document.getElementById("swiftCode").value="";
		document.getElementById("branchName").value="";
		document.getElementById("branchAddress").value="";
		document.getElementById("account").value="";
		document.frmBenificiary.accountType[0].checked=true;
		
		var selection = document.frmBenificiary.ibanbank; // radio buttons
		for (i=0; i<selection.length; i++){
			selection[i].checked = false;
		}
		
		document.getElementById("iban").value="";
		document.getElementById("ibnStatus").value=0;
		document.getElementById("benIbanValidStatus").innerHTML = "";
		// hide both details IBAN and Bank if any of them is displayed.
		document.getElementById("bankData").style.display="none";
		document.getElementById("ibanData").style.display="none";
		document.getElementById('formSubmitButton').disabled = false;
	}
}

// end of javascript -->
	</script>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="./javascript/jquery.selectboxutils.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<!-- <body> -->
<body onLoad="checkNumber();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	<tr>
		<td class="topbar"><strong><font color="#000000" size="2">
			<? if($_SESSION["benID"]!= ""){ echo("Update Beneficiary");} else { echo("Add Beneficiary"); } ?>
			</font></strong></td>
	</tr>
	<form id="frmBenificiary" name="frmBenificiary" action="add-beneficiary-quick-conf.php?from=<? echo $_GET["from"];?>&agentId2=<? echo $_GET["agentID"];?>&transID=<? echo $_GET["transID"]?>&r=<?=$_GET["r"]?>" method="post">
		<input type="hidden" name="benID" value="<? echo $_SESSION["benID"];?>">
		<input type="hidden" name="from" value="<? echo $_SESSION["from"];?>">
		<tr>
			<td align="center"><table width="686" border="0" cellspacing="1" cellpadding="2" align="center">
					<tr>
						<td colspan="2" bgcolor="#000000"><table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
								<tr>
									<td align="center" bgcolor="#DFE6EA"><font color="#000066" size="2"><strong>
										<? if($_SESSION["benID"]!= ""){ echo("Update Beneficiary");} else { echo("Add Beneficiary"); } ?>
										</strong></font></td>
								</tr>
							</table></td>
					</tr>
					<? if ($_GET["msg"] != "" || $_SESSION["error"] != ""){ ?>
					<tr bgcolor="#ededed">
						<td colspan="2" bgcolor="#EEEEEE"><table width="100%" cellpadding="5" cellspacing="0" border="0">
								<tr>
									<td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
									<td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; $_SESSION['error']=""; ?></td>
								</tr>
							</table></td>
					</tr>
					<? } ?>
					<tr bgcolor="#ededed">
						<td height="19" colspan="2" align="center"><font color="#FF0000">* Compulsory Fields</font><font color="#FF0000">&nbsp;&nbsp;&nbsp;&nbsp;+ Can be Compulsory Fields with respect to Country Selection </font></td>
					</tr>
					<tr>
					  <td valign="top" colspan="2" bgcolor="#000000">
						<table width="100%" cellpadding="2" cellspacing="1" border="0">
						
								<tr bgcolor="#ededed">
									<td width="17%" height="20" align="right"><font color="#005b90"><b>Country<font color="#ff0000">*</font>&nbsp;</b></font></td>
									<td width="26%"><input type="hidden" name="sel" value="">
									<?php
									if (CONFIG_CPF_ENABLED == "1"){ ?>
										<select name="Country" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].sel.value = 'country'; document.forms[0].action='add-beneficiary-quick.php?from=<? echo $_GET["from"];?>&agentID=<?=$_GET["agentID"];?>&customerID=<?=$_GET["customerID"];?>&transID=<? echo $_GET["transID"];?>&r=<?=$_GET["r"]?>&serviceType=<?=$_REQUEST["serviceType"]?>&collectionPointID=<?=$collectionPointIDV?>'; document.forms[0].submit();">
											<? }else{ ?>
											<select name="Country" style="font-family:verdana; font-size: 11px" <? if(CONFIG_SHOW_STATES == "1"){?> onChange="document.forms[0].sel.value = 'country'; document.forms[0].action='add-beneficiary-quick.php?from=<? echo $_GET["from"];?>&agentID=<?=$_GET["agentID"];?>&customerID=<?=$_GET["customerID"];?>&transID=<? echo $_GET["transID"];?>&r=<?=$_GET["r"]?>&serviceType=<?=$_REQUEST["serviceType"]?>&collectionPointID=<?=$collectionPointIDV?>'; document.forms[0].submit();" <? } ?>>
											<? } ?>
											<option value="">- Select Country-</option>
					<?	
						$benCountries = "";
						if (defined("CONFIG_BEN_COUNTRIES_LIST") && CONFIG_BEN_COUNTRIES_LIST!="0"){
							$benCountries = explode(",", CONFIG_BEN_COUNTRIES_LIST);
							$benCountries = array_filter($benCountries);
							for ($k = 0; $k < count($benCountries); $k++) {
								$selected = "";
								if(trim($benCountries[$k]) == $_SESSION["Country"])
									$selected = "selected='selected'";
					?>
								<option value="<? echo trim($benCountries[$k]) ?>" <?=$selected?>>
									<? echo trim($benCountries[$k]) ?>
								</option>
					<?	
							}
						}elseif ($agentType == "admin" || $agentType == "PAYING BOOK CUSTOMER"){
                      		if(CONFIG_ENABLE_ORIGIN == "1"){
                	
			                	
			                		$countryTypes = " and countryType like '%destination%' ";
			                		
			                                	
			                }else{
			                		$countryTypes = " ";
			                	
			                } 
                      
                   if(CONFIG_COUNTRY_SERVICES_ENABLED){
							   $countryServiceQ = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = toCountryId  $countryTypes order by countryName";
							   if($countryBasedFlag){
								   $countryServiceQ = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE_NEW." where 1 and countryId = toCountryId  $countryTypes order by countryName";
							   }
								$countires = selectMultiRecords($countryServiceQ);
								}
							else
								{
									$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
								}
					//$countires = selectMultiRecords("select distinct country from cities where 1 order by country");
					for ($i=0; $i < count($countires); $i++)
					{
						$selected = "";
						if($countires[$i]["countryName"] == $_SESSION["Country"])
						{
							// Code modified by Usman Ghani against ticket #3469 Now Transfer - Default Beneficiary Country
							$selected = "selected='selected'";
						}
						else if ( empty( $_SESSION["Country"] ) )
						{
							// Code added by Usman Ghani aginst ticket #3469 Now Transfer - Default Beneficiary Country
							if ( defined("CONFIG_DEFAULT_COUNTRY") && CONFIG_DEFAULT_COUNTRY == 1 && defined("CONFIG_DEFAULT_COUNTRY_VALUE"))
							{
								if ( $countires[$i]["countryName"] == CONFIG_DEFAULT_COUNTRY_VALUE )
									$selected = "selected='selected'";
							}
							// End of code aginst ticket #3469 Now Transfer - Default Beneficiary Country
						}
						?>
											<option  value="<? echo $countires[$i]["countryName"]; ?>" <?=$selected;?> > <? echo $countires[$i]["countryName"]; ?> </option>
											<?
					}
				}
				else
				{
					
					$IDAcountry	= selectFrom("select IDAcountry from ".TBL_ADMIN_USERS." where userID = '".$parentID."'");
					$myCountries = explode(",", $IDAcountry["IDAcountry"]);
					for ($i = 0; $i < count($myCountries); $i++)
					{
						if ($myCountries[$i] == $_SESSION["Country"])
						{
						?>
											<option  value="<? echo $_SESSION["Country"];?>"  selected> <? echo $myCountries[$i]; ?> </option>
											<?
								}
								else
								{
									?>
											<option value="<?=$myCountries[$i]; ?>">
											<?=$myCountries[$i]; ?>
											</option>

											<?
								}
					}
				}
				?>
										</select>									</td>
<? 
if($countryBasedFlag && $useServiceType!=""){
?>
									<td height="20" align="right"><font color="#005b90"><b>Services<font color="#ff0000">*</font>&nbsp;</b></font></td>
									<td>
									<select style="font-family: verdana; font-size: 11px;" id="serviceType" name="serviceType" onChange="document.forms[0].sel.value = 'country'; document.forms[0].action='add-beneficiary-quick.php?from=<? echo $_GET["from"];?>&agentID=<?=$_GET["agentID"];?>&customerID=<?=$_GET["customerID"];?>&transID=<? echo $_GET["transID"];?>&r=<?=$_GET["r"]?>&collectionPointID=<?=$collectionPointIDV?>'; document.forms[0].submit();">
											<? $selected = "";	?>
										<? for($sA=0;$sA<count($contentsService);$sA++){?>
											<option value="<?=$contentsService[$sA]["serviceAvailable"]?>"><?=$contentsService[$sA]["serviceAvailable"]?></option>
										<? }?>
										</select>
<script language="JavaScript">SelectOption(document.forms[0].serviceType, "<?=$useServiceType; ?>");</script>
									</td>
<? } elseif($countryBasedFlag){?>
									<td height="20" align="left" colspan="2"><font color="#005b90"><b>Services not avaiable for this Country</b></font></td>
<? }else{?>
									<td height="20" align="left" colspan="2">&nbsp;</td>
<? }?>

								</tr>	
<?
		/**
   		 * @ticket# 4506
		 * Now Beneficiaries will have IBAN and Bank details displayed
		 * Controlled via a radio button.
   		 */

if(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER == "1")
{ 
	// This scheme is made to hadle radio buttons and its data displayed 
	// in different cases.
//debug($_SESSION['ibanorbank']);
	$displayTable = "style='display:block'";
	$checkRadioIban = "checked='checked'";
	$showIbanTable = "style='display:none'";
	$showBankTable = "style='display:none'";
	$showRadioIban = "";
	$showRadioBank = "";
	
	if($_SESSION['ibanorbank'] == "iban"){
		$showRadioIban = $checkRadioIban;
		$showIbanTable = $displayTable;
	}
	elseif($_SESSION["ibanorbank"]=="bank" ){
		$showRadioBank = $checkRadioIban;
		$showBankTable = $displayTable;
	}

?>
 <!-- This is layour for selecting IBAN or BANK Details. -->
<? if($useIbanBankStructure){?>
			<? if($showIbanBankSelectRow){?>
						<tr bgcolor="#ededed" id="selectOptions">
							<td colspan="2" align="right"><font color="#005b90"><b>Select an Option :</b></font> </td>
							<td align="left"><input type="radio" id="ibanbank" name="ibanbank" value="iban" onClick="showHideIbanBank(this);"	<?=$showRadioIban; ?>>
								&nbsp;&nbsp;<b>IBAN</b> </td>
							<td align="left"><input type="radio" id="ibanbank"  name="ibanbank" value="bank" onClick="showHideIbanBank(this);" <?=$showRadioBank; ?>>
								&nbsp;&nbsp;<b>Bank Details</b> </td>
						</tr>
			<? }else{
	if($useServiceType=="IBAN"){
		echo "<input type='hidden' id='ibanbank' name='ibanbank' value='iban' ><script>showHideIbanBank(ibanbank);</script>";
	}
	elseif($useServiceType=="Bank Transfer"){
		echo "<input type='hidden' id='ibanbank' name='ibanbank' value='bank'><script>showHideIbanBank(ibanbank);</script>";
	}
			}?>
 <!-- Layout for IBAN/BANK DETAILS Starts. -->
						<tr bgcolor="#ededed">
											<td colspan="4">
 <!-- IBAN layout starts. -->
											<table cellpadding="2" cellspacing="1" border="1" id="ibanData"  <?=$showIbanTable?> width="100%">
													<tr bgcolor="#ededed">
														<td height="20" width="15%" align="right"><font color="#005b90"><? //echo $showIbanTable." ";?><b>IBAN</b></font>&nbsp;<font color="red">*</font> </td>
														<td  width="25%" ><input type="text" name="iban" id="iban" value="<?=$_SESSION["iban"];?>" maxlength="50" onBlur="validateIban();" size="42">														</td>
														<td colspan="2"><div id="benIbanValidStatus"></div>
		 
															<input type="hidden" id="ibnStatus" name="ibnStatus" value="0">														</td>
													</tr>
												</table>
 <!-- IBAN layout ends. -->
 <!-- Bank Details layout starts. -->
											<table cellpadding="2" cellspacing="1" border="1" id="bankData"  <?=$showBankTable?> width="100%">
													<tr>
														<td colspan="4" style="background:#CCCCCC;"><font color="#005b90"><b>Beneficiary Bank Details</b></font></td>
													</tr>
													<!--tr bgcolor="#ededed">
														<td width="50%" colspan="2" align="right"><font color="#005b90"><b>Country</b></font><font color="red">*</font>&nbsp; </td>
														<td width="50%" colspan="2" align="left">
															<select name="country1" id="country1">
																<option value="">- Select Country -</option>
																<?		
											$benBankCountrySql = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = toCountryId  $countryTypes order by countryName";
										   if($countryBasedFlag){
											   $benBankCountrySql = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE_NEW." where 1 and countryId = toCountryId  $countryTypes order by countryName";
										   }
											$myCountries = selectMultiRecords($benBankCountrySql);
										
											for ($i = 0; $i < count($myCountries); $i++)
											{
												if ($myCountries[$i]["countryName"] == $_SESSION["country1"])
												{
										?>
																<option  value="<?=$_SESSION["country1"];?>" selected>
																<?=$myCountries[$i]["countryName"]?>
																</option>
																<?
												}
												else
												{
										?>
																<option value="<?=$myCountries[$i]["countryName"]; ?>">
																<?=$myCountries[$i]["countryName"]; ?>
																</option>
																<?
												}
											} 
										
										?>
															</select>
														</td>
													</tr -->
													<tr bgcolor="#ededed"> 
														<td width="100%" align="center" colspan="4" valign="middle">
															<font color="#005b90"><b>Bank Name</b></font>
															<input type="text" size="10" name="searchBankName" id="searchBankName" />
															<input type="button" onClick="if(document.frmBenificiary.searchBankName.value == '' || IsAllSpaces(document.frmBenificiary.searchBankName.value)) { alert ('Please provide bank name to search.') } else { window.open('search-bank.php?from=add-beneficiary-quick.php&search=Y&transID=<?=$_GET["transID"]?>'+'&benID=<?=$_GET["benID"]?>&bankName='+ document.frmBenificiary.searchBankName.value +'&benCountry=<?=$_SESSION["Country"]?>&customerID=<?=$getCustomerID?>&serviceType=<?=$useServiceType?>&ibanorbank=<?=$_SESSION["ibanorbank"]?>&collectionPointID=<?=$collectionPointIDV?>', 'searchBank', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740'); document.forms[0].sel.value = 'country'; document.forms[0].action='add-beneficiary-quick.php?from=<? echo $_GET["from"];?>&agentID=<?=$_GET["agentID"];?>&customerID=<?=$_GET["customerID"];?>&transID=<? echo $_GET["transID"];?>&r=<?=$_GET["r"]?>'; }" value="Search" name="Submit"/>
															
															</td>
													</tr>
													<tr bgcolor="#ededed">
														<td width="17%" height="20" align="right"><font color="#005b90"><b>Bank Name<font color="red">*</font>&nbsp;</b></font> </td>
														<td width="26%"><input type="text" name="bankName" id="bankName" value="<?=$_SESSION["bankName1"]?>" maxlength="30">														</td>
														<td width="20%"><font color="#005b90"><b>Swift Code</b></font>&nbsp; </td>
														<td width="37%"><input type="text" name="swiftCode" id="swiftCode" value="<?=$_SESSION["swiftCode1"]?>" maxlength="30">														</td>
													</tr>
													<tr bgcolor="#ededed">
														<td width="17%" height="20" align="right"><font color="#005b90"><b>Branch Code/Name&nbsp;</b></font> </td>
														<td width="26%"><input type="text" name="branchName" id="branchName" value="<?=$_SESSION["branchCode1"]?>" maxlength="30">														</td>
														<td width="20%"><font color="#005b90" ><b>Branch Address<font color="red">*</font></b></font>&nbsp; </td>
														<td width="37%"><input type="text" name="branchAddress" id="branchAddress" value="<?=$_SESSION["branchAddress1"]?>" maxlength="30" >														</td>
													</tr>
													<tr bgcolor="#ededed">
														<td width="17%" height="20" align="right"><font color="#005b90"><b>Account Number<font color="red">*</font>&nbsp;</b></font> </td>
														<td width="26%"><input type="text" name="account" id="account" value="<?=$_SESSION["account1"]?>" maxlength="30">														</td>
														<td width="20%">&nbsp;</td>
														<td width="37%">&nbsp;</td>
													</tr>
													<tr bgcolor="#ededed">
														<td width="17%" height="20" align="right" valign="top"><font color="#005b90"><b>Account Type&nbsp;</b></font> </td>
														<td width="26%"><?
											if($_SESSION["accountType1"] == "Savings")
											{
												$svingAcnt = "Checked";
												$crntAcnt = "";
											}
											else
											{
												$svingAcnt = "";
												$crntAcnt = "Checked";
											}
										?>
															<input type="radio" name="accountType" id="accountType" value="Current" maxlength="30" <?=$crntAcnt?>>
															&nbsp;Current <br />
															<input type="radio" name="accountType" id="accountType" value="Savings" maxlength="30" <?=$svingAcnt?>>
															&nbsp;Saving </td>
														<td width="20%">&nbsp;</td>
														<td width="37%">&nbsp;
															<input type="hidden" name="usedBankId" value="<?=$_SESSION["ben_bank_id"]?>" />														</td>
															<input type="hidden" name="country1" id="country1" value="<?=$_SESSION["Country"]?>" />
													</tr>
													<?
							//}
						?>
												</table>
 <!-- Bank Details layout ends. -->
 <!-- This hidden field decides on which Layout validation is to be done for saving data. -->
							</td>
						</tr>
								<input type="hidden" id="IbanOrBank" name="IbanOrBank" value="<?=($_SESSION["ibanorbank"]=="bank"?"b":"i")?>">
<? 
}
elseif($countryBasedFlag && ($useServiceType == "Pick up" || $useServiceType == "Home Delivery"))
{ 
?>
						<tr bgcolor="#ededed">
									<td width="17%" height="20" align="right"><font color="#005b90"><b>Collection Point Name</b></font></td>
									<td width="26%" colspan="3" align="left"><INPUT NAME="agentCity" TYPE="text" ID="agentCity" SIZE="15">
									&nbsp;&nbsp;&nbsp;
			                          <INPUT TYPE="button" NAME="SubmitColl" VALUE="Search Collection Point" onClick=" if(document.frmBenificiary.agentCity.value == '' || IsAllSpaces(document.frmBenificiary.agentCity.value)) { alert ('Please provide Collection Point to search.') } else { window.open('search-agent-city.php?from=add-beneficiary-quick.php&agentCity=' + document.frmBenificiary.agentCity.value + '&transID=<? echo $_GET["transID"]?>' +  '&benID=<?=$_GET["benID"]?>&ibanorbank=<?=$_SESSION["ibanorbank"]?>&benCountry=<? echo $_SESSION["Country"]?>'+  '&customerID=<?=$getCustomerID?>&serviceType=<?=$useServiceType?>&serviceType=<?=$useServiceType?>&exchangeRate=<? echo $_SESSION["exchangeRate"]?>'+  '&calculateBy=<? echo $_SESSION["calculateBy"]?>&agentID=<?=$_GET["agentID"]?>', 'agentCity', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=800') }">
									  </td>
						</tr>			  
					  
                      <? 
		
		$queryCust = "select *  from cm_collection_point as c, admin a where c.cp_ida_id = a.userID And cp_id  ='".$collectionPointIDV."' and a.agentStatus = 'Active'";
			$senderAgentContent = selectFrom($queryCust);
			$senderAgentContent["cp_id"];
			$dist = $senderAgentContent["cp_ida_id"];
/*			$_SESSION["distribut"] = $dist;
			$_SESSION["benAgentID"] = $dist;*/
			 $queryDistributor = "select *  from " .TBL_ADMIN_USERS. " where  userID  ='" . $senderAgentContent["cp_ida_id"] . "'";
			$queryExecute = selectFrom($queryDistributor);
			
			if($senderAgentContent["cp_id"] != "")
			{
		?>
					<tr bgcolor="#ededed">
					<td colspan="4">
					<TABLE WIDTH="100%" BORDER="1" CELLPADDING="2" CELLSPACING="2">                         <TR>
											<TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Collection point name</FONT></TD>
											<TD WIDTH="200"><? echo $senderAgentContent["cp_corresspondent_name"]?> </TD>
											<TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Contact Person </FONT></TD>
											<TD WIDTH="200"><? echo $senderAgentContent["cp_contact_person_name"]?></TD>
										  </TR>
											 <? if(CONFIG_DISTRIBUTOR_ALIAS != "1") { ?>
										  <TR>
										   <TD WIDTH="150" ALIGN="right"><FONT CLASS="style2">Distributor</FONT></TD>
											<TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["name"]?></TD>
										  </TR> 
										  <? } ?>
										 
										 <? if(CONFIG_DISTRIBUTOR_ALIAS == "1" && $agentType != "SUPA") { ?>
										  <TR>
										   <TD WIDTH="150" ALIGN="right"><FONT CLASS="style2">Distributor<? if($queryExecute["distAlias"] != ''){ echo "Alias"; }?></FONT></TD>
											 <? if($queryExecute["distAlias"] != '') { ?>
											   <TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["distAlias"]?></TD>
											 <? }else{ ?>
												<TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["agentCompany"]?></TD>
											 <? } ?>  
										  </TR> 
										  <? } ?>
										  
										  <? if(CONFIG_DISTRIBUTOR_ALIAS == "1" && $agentType == "SUPA") {?>
										 <TR>
											<TD WIDTH="150" ALIGN="right"><FONT CLASS="style2">Distributor Alias</FONT></TD>
											<TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["distAlias"]?></TD>
										 </TR> 
											  <? } ?>
										  <tr>
											<td width="150" align="right"><font color="#005b90">Branch Address</font></td>
											<td width="200"><? echo $senderAgentContent["cp_branch_address"]?></td>
											<td width="100" align="right"><font color="#005b90">Location ID</font></td>
											<td width="200"><? echo $senderAgentContent["cp_branch_no"]?></td>
										  </tr>
										 
										  <TR>
											<TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">City</FONT></TD>
											<TD WIDTH="200"><? echo $senderAgentContent["cp_city"]?></TD>
											<TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Country</FONT></TD>
											<TD WIDTH="200"><? echo $senderAgentContent["cp_country"]?></TD>
										  </TR>
										  <TR>
											<TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Phone</FONT></TD>
											<TD WIDTH="200"><? echo $senderAgentContent["cp_phone"]?></TD>
											<TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Fax</FONT></TD>
											<TD WIDTH="200"><? echo $senderAgentContent["cp_fax"]?></TD>
										  </TR> 
										</TABLE>  
					</td></tr>                    
<input type="hidden" name="collectionPointID" id="collectionPointID" value="<?=$senderAgentContent["cp_id"]?>">
                      <?
			  }
		}
}
			  ?>
						 <?
						/**
						 * @ticket# 3474
						 
						if(CONFIG_IBAN_ON_QUICK_BEN == "1"){
					   ?> 
            		<tr bgcolor="#ededed"> 
					  <td width="20%" align="right">
						  <font color="#005b90"><b>IBAN</b></font>&nbsp;<font color="red">*</font>
					  </td>
					  <td width="37%">
					  	<input type="text" name="iban" id="iban" value="<?=$_SESSION["iban"]?>" maxlength="50" onBlur="validateIban();" size="35">
					  </td>
					  <td width="43%" height="20" align="left" colspan="2">
					  	<div id="benIbanValidStatus"></div>
					  	<input type="hidden" id="ibnStatus" value="0">
					  </td>
					</tr>
					  <?
					  }
					  */
					  ?> 
							<tr bgcolor="#ededed">
								<?
								if(CONFIG_TITLE_OFF != "1")
								{
								?>
									<td width="17%" height="20" align="right"><font color="#005b90"><b>Title&nbsp; </b></font></td>
									<td width="26%"><select name="Title" onChange="var titleSessValue='<?=$_SESSION[Title]?>'; if (document.forms[0].Title.value == 'Other') { document.forms[0].action = 'add-beneficiary-quick.php?from=<? echo $_GET["from"]?>'; document.forms[0].submit(); } else if (titleSessValue == 'Other') { document.forms[0].action = 'add-beneficiary-quick.php?from=<? echo $_GET["from"]?>'; document.forms[0].submit(); } ">
											<option value="Mr.">Mr.</option>
											<option value="Miss.">Miss</option>
											<option value="Dr.">Dr.</option>
											<option value="Mrs.">Mrs.</option>
											<option value="Ms.">Ms.</option>
											<option value="Other">Other</option>
										</select>
										<script language="JavaScript">SelectOption(document.forms[0].Title, "<?=$_SESSION["Title"]; ?>");</script></td>
								<?
									}else {
								 ?>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<?
                }
         	 if (CONFIG_CUST_NAME_SEQUENCE == '1') {
           ?>
									<td align="right"><font color="#005b90"><b><? echo (CONFIG_SURNAME == "1" ? CONFIG_SURNAME_VAL : "Last Name")?><font color="#ff0000">*</font>&nbsp;</b></font></td>
									<td><input type="text" name="lastName" value="<?=$_SESSION["lastName"]; ?>" maxlength="25"></td>
									<?
           }else{ 
           ?>
									<td width="20%" align="right"><font color="#005b90"><b>First 
										Name<font color="#ff0000">*</font>&nbsp;</b></font></td>
									<td width="37%"><input type="text" name="firstName" value="<?=$_SESSION["firstName"]; ?>" maxlength="25"></td>
									<?
           }
           ?>
								</tr>
				<? 
					if($_SESSION["Title"] == "Other" && CONFIG_TITLE_OFF != "1")
					{ ?>
						<tr  bgcolor="#ededed">
							<td align="right"><font color="#005b90"><b>Other Title </b></font></td>
							<td><input type="text" name="other_title" value="<?=$_SESSION["other_title"]; ?>" maxlength="25"></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<? } ?>
						
						<tr bgcolor="#ededed"> 
						  <td width="17%" height="20" align="right"><font color="#005b90"><b>
						  	<? echo (CONFIG_CUSTBEN_MIDNAME == "1" ? CONFIG_CUSTBEN_MIDNAME_TITLE : "Middle Name") ?>&nbsp;</b></font>						  </td>
						  <td width="26%"><input type="text" name="middleName" value="<?=$_SESSION["middleName"]; ?>" maxlength="25"></td>
						  <? if (CONFIG_CUST_NAME_SEQUENCE == '1') {  ?>
							  <td width="20%" align="right"><font color="#005b90"><b>First 
								Name<font color="#ff0000">*</font>&nbsp;</b></font></td>
								<td width="37%"><input type="text" name="firstName" value="<?=$_SESSION["firstName"]; ?>" maxlength="25"></td>
						<?	} else {  ?>
						  <td width="20%" align="right"><font color="#005b90"><b><? echo (CONFIG_SURNAME == "1" ? CONFIG_SURNAME_VAL : "Last Name")?><font color="#ff0000">*</font>&nbsp;</b></font></td>
						  <td width="37%"><input type="text" name="lastName" value="<?=$_SESSION["lastName"]; ?>" maxlength="25"></td>
						<?	}  ?>
						</tr>
						<?	if($beneficiaryNameFlag){ ?>
								<tr  bgcolor="#ededed">
									<td align="right"><font color="#005b90"><b>Beneficiary Name </b></font></td>
									<td><input type="text" name="beneficiaryName" id="beneficiaryName" value="<?=$_SESSION["beneficiaryName"]; ?>" maxlength="50" size="50"></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
						<? }?>
							   <?  if(CONFIG_BENEFICIARY_SECOND_MIDDLE == "1"){  ?>
										
										<tr bgcolor="#ededed">
											<td width="20%" align="right"><font color="#005b90"><b>Second Middle Name</b></font></td>
							<td width="37%"><input type="text" name="secMiddleName" value="<?=$_SESSION["secMiddleName"]; ?>" maxlength="25"></td>
											<td width="20%">&nbsp; </td>
							<td width="37%">&nbsp;</td>
						  </tr>
				  				
				  				<? } ?>        		
            		          		
            		 <?  if(CONFIG_SO_BEN == "1"){  ?>
				  				
				  				<tr bgcolor="#ededed">
				  				<td width="20%">&nbsp; </td>
                  <td width="37%">&nbsp;</td>
                  	<td width="20%" align="right">
						<select id="sonOfType"	name="sonOfType">
							<option value="S/O" <? echo ($_SESSION["sonOfType"] == "S/O" ? "selected" : "") ?>>S/O</option>
							<option value="D/O" <? echo ($_SESSION["sonOfType"] == "D/O" ? "selected" : "") ?>>D/O</option>
							<option value="W/O" <? echo ($_SESSION["sonOfType"] == "W/O" ? "selected" : "") ?>>W/O</option>
						</select>					</td>
                  <td width="37%"><input type="text" name="S/O" value="<?=$_SESSION["S/O"]; ?>" maxlength="25"></td>
                </tr>
				  				
				  <? } ?>
            		<tr bgcolor="#ededed">
            			<td colspan="4">&nbsp;</td>
            		</tr>
				<?
					// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
					if (defined("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY") && CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY == 1 && $countryBasedFlag)
					{
						if ( !empty( $IDTypes ) )
						{
						?>
							<tr bgcolor="#ededed">
								<td colspan="4">
									<table width="100%">
										<tr>
										<? 
											$tdNumber = 1;
											$numberOfIDTypes = count( $IDTypes );

											$chkIDTypesJS = "";

											$serverDateArray["year"] = date("Y");
											$serverDateArray["month"] = date("m");
											$serverDateArray["day"] = date("d");
											for( $i = 0; $i < $numberOfIDTypes; $i++ )
											{
												$idNumber = "";
												$idTypeValuesSESS = "";
												$issuedBy = "";
												$notes  = "";
												if( !empty( $_SESSION["IDTypesValuesData"] )
													&& !empty( $_SESSION["IDTypesValuesData"][$IDTypes[$i]["id"]] ) )
												{
												
													$idTypeValuesSESS = $_SESSION["IDTypesValuesData"];

													if(is_array($idTypeValuesSESS))
													{
														$idNumber = $idTypeValuesSESS[$IDTypes[$i]["id"]]["id_number"];
														$issuedBy = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issued_by"];
														if($userIdTypesNotesFlag){
															$notes = $idTypeValuesSESS[htmlentities($IDTypes[$i]["values"]["notes"])];
														}
													}
												}
												else if (!empty($IDTypes[$i]["values"]))
												{
													$idNumber = $IDTypes[$i]["values"]["id_number"];
													$issuedBy = $IDTypes[$i]["values"]["issued_by"];
													if($userIdTypesNotesFlag){
														$notes = htmlentities($IDTypes[$i]["values"]["notes"]);
													}
												}
												$mandatoryCheck = $IDTypes[$i]["mandatory"];
												 if(defined("CONFIG_MULTI_ID_MANDATORY_SEPARATE") && CONFIG_MULTI_ID_MANDATORY_SEPARATE=="1"){
													//$mandatoryCheck = $IDTypes[$i]["mandatoyBeneficiary"];
													$mandatoryCheck = "N";
													$serviceMultiId = selectFrom("select sim,benMandatory from service_ID_Mandatory where  serviceAvailable='" . $useServiceType . "' AND multiple_id='".$IDTypes[$i]["id"]."'");
													if($serviceMultiId["sim"]!=""){
														$mandatoryCheck = $serviceMultiId["benMandatory"];
													}
												 }
												?>
													<td style="padding:2px; text-align:center; <?=($tdNumber==1 ? "border-right:1px solid black;" : "");?> ">
														<table border="1" cellpadding="2" cellspacing="0" align="center">
														  <tr>
															<td colspan="2" style="border:2px solid black; font-weight:bold;"><?=$IDTypes[$i]["title"];?> <?=($mandatoryCheck == 'Y' ? "<span style='color:red;'>*</span>" : ""); ?></td>
														  </tr>
														  <tr>
															<td width="80">ID Number:</td>
															<td>
															
	<input type="hidden" name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id]" value="<?=( !empty($IDTypes[$i]["values"]) ? $IDTypes[$i]["values"]["id"] : "" );?>" />
																<input type="hidden" name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id_type_id]" value="<?=$IDTypes[$i]["id"];?>" />
																<input type="text" id="id_number_<?=$IDTypes[$i]["id"];?>" name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id_number]" value="<?=$idNumber;?>" size="40" />
															</td>
														  </tr>
														  <tr>
															<td>Issued by:</td>
															<td><input type="text" id="id_issued_by_<?=$IDTypes[$i]["id"];?>" name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issued_by]" value="<?=$issuedBy?>" size="40" /></td>
														  </tr>
													<? if($userIdTypesNotesFlag){?>	  
														  <tr>
															<td>Notes:</td>
															<td><input type="text" id="id_notes_<?=$IDTypes[$i]["id"];?>" name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][notes]" value="<?=$notes?>" size="40" /></td>
														  </tr>
													<? }?>
														  <tr>
															<td>Issue Date:</td>
															<td>
																<?
																	$startYear = 1985;
																	$currentYear = $serverDateArray["year"];

																	$issueDay = 0;
																	$issueMonth = 0;
																	$issueYear = 0;

																	if ( !empty($idTypeValuesSESS)
																		&& !empty($idTypeValuesSESS[$IDTypes[$i]["id"]]) )
																	{
																	if(is_array($idTypeValuesSESS)){
																		$issueDay = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["day"] - 1;
																		$issueMonth = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["month"] - 1;
																		$issueYear = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["year"] - $startYear;                                 
																	}
																	}
																	else if ( !empty($IDTypes[$i]["values"]) )
																	{
																		$dateArray = explode( "-", $IDTypes[$i]["values"]["issue_date"] );
																		$issueDay = $dateArray[2] - 1;
																		$issueMonth = $dateArray[1] - 1;
																		$issueYear = $dateArray[0] - $startYear;
																	}
																?>
																<select id="issue_date_day_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][day]" >
																</select>
																
																<select id="issue_date_month_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][month]" >
																</select>
																
																<select id="issue_date_year_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][year]" >
																</select>

																<script type="text/javascript">
																	initDatePicker('issue_date_day_<?=$IDTypes[$i]["id"];?>', 'issue_date_month_<?=$IDTypes[$i]["id"];?>', 'issue_date_year_<?=$IDTypes[$i]["id"];?>','', '1:12', '<?=$startYear;?>:<?=$currentYear;?>', <?=$issueDay;?>, <?=$issueMonth;?>, <?=$issueYear;?>);
																</script>
															</td>
														  </tr>
														  <tr>
															<td>Expiry Date:</td>
															<td>
																<?
																	$expiryStartYear = $currentYear;
																	$expiryDay = $serverDateArray["day"] - 1; // Didn't do "less 1" here because atleast one day further from current has to be selected.
																	$expiryMonth = $serverDateArray["month"] - 1;
																	$expiryYear = $serverDateArray["year"] - $expiryStartYear;

																	if ( !empty($idTypeValuesSESS)
																		 && !empty($idTypeValuesSESS[$IDTypes[$i]["id"]]) )
																	{
																	if(is_array($idTypeValuesSESS)){
																		$expiryDay = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["day"] - 1;
																		$expiryMonth = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["month"] - 1;
																		$expiryYear = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["year"] - $expiryStartYear;                          
																	}
																	}
																	else if ( !empty($IDTypes[$i]["values"]) )
																	{
																		$dateArray = explode( "-", $IDTypes[$i]["values"]["expiry_date"] );

																		$expiryStartYear = ( $dateArray[0] <= $currentYear ? $dateArray[0] : $currentYear );

																		$expiryDay = $dateArray[2] - 1;
																		$expiryMonth = $dateArray[1] - 1;
																		$expiryYear = $dateArray[0] - $expiryStartYear;
																	}
																?>
																<select id="expiry_date_day_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][day]" >
																</select>

																<select id="expiry_date_month_<?=$IDTypes[$i]["id"];?>"
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][month]" >
																</select>

																<select id="expiry_date_year_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][year]" >
																</select>

																<script type="text/javascript">
																	initDatePicker('expiry_date_day_<?=$IDTypes[$i]["id"];?>', 'expiry_date_month_<?=$IDTypes[$i]["id"];?>', 'expiry_date_year_<?=$IDTypes[$i]["id"];?>','', '1:12', '<?=$expiryStartYear;?>:2035', <?=$expiryDay;?>, <?=$expiryMonth;?>, <?=$expiryYear;?>);
																</script>
															</td>
														  </tr>
														</table>
													</td>
												<?
													if ( $tdNumber == 2 )
													{
														// Starting a new TR when two TDs are output.
														?>
															</tr><tr>
														<?
														$tdNumber = 1;
													}
													else
														$tdNumber++;

													$chkIDTypesJS .= "id_number_" . $IDTypes[$i]["id"] . " = document.getElementById('id_number_" . $IDTypes[$i]["id"] . "');
																			id_issued_by_" . $IDTypes[$i]["id"] . " = document.getElementById('id_issued_by_" . $IDTypes[$i]["id"] . "');";

													if ( $mandatoryCheck == 'Y' )
													{
														$chkIDTypesJS .= "
																			if ( id_number_" . $IDTypes[$i]["id"] . ".value=='' )
																			{
																				alert('Please provide " . $IDTypes[$i]["title"] . "\\'s id number detail.');
																				id_number_" . $IDTypes[$i]["id"] . ".focus();
																				return false;
																			}
																			if ( id_issued_by_" . $IDTypes[$i]["id"] . ".value=='' )
																			{
																				alert('Please provide " . $IDTypes[$i]["title"] . "\\'s issued by detail.');
																				id_issued_by_" . $IDTypes[$i]["id"] . ".focus();
																				return false;
																			}
																		";
														if($userIdTypesNotesFlag){
															$chkIDTypesJS .= "
																				if ( id_notes_" . $IDTypes[$i]["id"] . ".value=='' )
																				{
																					alert('Please provide " . $IDTypes[$i]["title"] . "\\'s notes detail.');
																					id_notes_" . $IDTypes[$i]["id"] . ".focus();
																					return false;
																				}
																			";
														}
													}
													else
													{
														$chkIDTypesJS .= "
																			if ( id_number_" . $IDTypes[$i]["id"] . ".value=='' && id_issued_by_" . $IDTypes[$i]["id"] . ".value != '' )
																			{
																				alert('Please provide " . $IDTypes[$i]["title"] . "\\'s id number detail.');
																				id_number_" . $IDTypes[$i]["id"] . ".focus();
																				return false;
																			}
																			if ( id_number_" . $IDTypes[$i]["id"] . ".value != '' && id_issued_by_" . $IDTypes[$i]["id"] . ".value=='' )
																			{
																				alert('Please provide " . $IDTypes[$i]["title"] . "\\'s issued by detail.');
																				id_issued_by_" . $IDTypes[$i]["id"] . ".focus();
																				return false;
																			}
																		";
													}

													$chkIDTypesJS .= "
																		issueDate = new Date();
																		todayDate = new Date( issueDate );
																		expiryDate = new Date();

																		todayDate.setFullYear( " . $serverDateArray["year"] . ", " . ($serverDateArray["month"] - 1) . ", " . $serverDateArray["day"] . " );
																		issueDate.setFullYear( document.getElementById('issue_date_year_" . $IDTypes[$i]["id"] . "').value, document.getElementById('issue_date_month_" . $IDTypes[$i]["id"] . "').value - 1, document.getElementById('issue_date_day_" . $IDTypes[$i]["id"] . "').value );
																		expiryDate.setFullYear( document.getElementById('expiry_date_year_" . $IDTypes[$i]["id"] . "').value, document.getElementById('expiry_date_month_" . $IDTypes[$i]["id"] . "').value - 1, document.getElementById('expiry_date_day_" . $IDTypes[$i]["id"] . "').value );

																		if ( issueDate >= todayDate )
																		{
																			alert( 'The issue date for " . $IDTypes[$i]["title"] . " should be less than today\\'s date.' );
																			document.getElementById('issue_date_day_" . $IDTypes[$i]["id"] . "').focus();
																			return false;
																		}
																		
																		if ( expiryDate <= todayDate )
																		{
																			alert( 'The expiry date for " . $IDTypes[$i]["title"] . " should be greater than today\\'s date.' );
																			document.getElementById('expiry_date_day_" . $IDTypes[$i]["id"] . "').focus();
																			return false;
																		}
																	";
											}
										?>
										</tr>
									</table>
									<script type="text/javascript">
										function checkIdTypes( theForm )
										{
											<?
												if ( !empty($chkIDTypesJS) )
													echo $chkIDTypesJS;
											?>
											return true;
										}
									</script>
								</td>
							</tr>
						<?
						}
					} // End of code against #3299: MasterPayex - Multiple ID Types
	else{
		if(CONFIG_ID_MANDATORY == '1'){?>
            		  <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" rowspan="3" align="right" <? echo (CONFIG_IDTYPE_DROPDOWN != "1" ? "valign='top'" : "") ?>><font color="#005b90"><b>ID 
                    Type<font color="#ff0000">*</font>&nbsp;</b></font></td>
                  <td width="26%" rowspan="3">
                  <?	if (CONFIG_IDTYPE_DROPDOWN == "1") {  ?>
                  	<select name="IDType">
                  		<option value="">- Select ID Type -</option>
                	<?
                		if (CONFIG_IDTYPES_FROM_LIST == "1") {
                			$idTypeList = explode(",", CONFIG_IDTYPES_LIST);
                			for ($i = 0; $i < count($idTypeList); $i++) {
                	?>
											<option value="<? echo $idTypeList[$i] ?>" <? echo ($_SESSION["IDType"] == $idTypeList[$i] ? "selected" : "") ?>><? echo $idTypeList[$i] ?></option>
											<?	
                			}
                		} else {
                	?>
											<option value="Passport"  <? echo ($_SESSION["IDType"] == "Passport" ? "selected" : "") ?>>Passport</option>
											<option value="ID Card" <? echo ($_SESSION["IDType"] == "ID Card" ? "selected" : "") ?>>ID Card</option>
											<option value="Driving License" <? echo ($_SESSION["IDType"] == "Driving License" ? "selected" : "") ?>>Driving License</option>
											<?	}  ?>
										</select>
										<?	} else {  
                  			if (CONFIG_IDTYPES_FROM_LIST == "1") {
                  				$idTypeList = explode(",", CONFIG_IDTYPES_LIST);
                  				for ($i = 0; $i < count($idTypeList); $i++) {
                  ?>
										<input type="radio" name="IDType" value="<? echo $idTypeList[$i] ?>" <? echo ($_SESSION["IDType"] == $idTypeList[$i] ? "checked" : "") ?>>
										<? echo $idTypeList[$i] ?><br>
										<?				
                  				}
                  			} else {
                  ?>
										<input type="radio" name="IDType" value="Passport" "checked"<? if ($_SESSION["IDType"] == "Passport") echo "checked"; ?>>
										Passport <br>
										<input type="radio" name="IDType" value="ID Card" <? if ($_SESSION["IDType"] == "ID Card") echo "checked"; ?>>
										ID Card<br>
										<input type="radio" name="IDType" value="Driving License" <? if ($_SESSION["IDType"] == "Driving License") echo "checked"; ?>>
										Driving Licence
										<?	
                  	}
                  }  
                  ?>									</td>
									<td width="20%" align="right" valign="top"><font color="#005b90"><b>ID 
										Number <font color="#ff0000">*</font>&nbsp;</b></font></td>
									<td width="37%" valign="top"><input type="text" name="IDNumber" value="<?=$_SESSION["IDNumber"]; ?>" maxlength="50"></td>
								</tr>
								<tr bgcolor="#ededed">
									<td align="right" valign="top"><font color="#005b90"><b>Other ID Name</b></font> </td>
									<td width="37%" valign="top"><input type="text" name="Otherid_name" value="<?=$_SESSION["Otherid_name"]?>" maxlength="32"></td>
								</tr>
								<tr bgcolor="#ededed">
									<td align="right" valign="top"><font color="#005b90"><b>Other ID</b></font> </td>
									<td width="37%" valign="top"><input type="text" name="Otherid" value="<?=$_SESSION["Otherid"]; ?>" maxlength="15"></td>
								</tr>
								<tr bgcolor="#ededed">
									<td colspan="4">&nbsp;</td>
								</tr>
								<? }
								}
								?>
								<?
				if(CONFIG_ELEMINATE_ADDRESS_QUICK_BENEFICIARY != "0"){
				//Config for Beacon Creast Transfer
				?>
								<tr bgcolor="#ededed">
									<td width="17%" height="20" align="right"><font color="#005b90"><b>
										<? /*To display House No for richalmond*/ 
                  	
                    	if(defined('ADDRESS_LINE1_LABEL'))
                    		{
                    			echo(ADDRESS_LINE1_LABEL);
                    		}else{
                    			echo("Address Line");
                    			}
                    ?>
										<? if(CONFIG_DISABLE_ADDRESS2 != '1')
                    {
                    	if(!defined('ADDRESS_LINE1_LABEL'))
                    	{ 
                    		                  			
                    			echo("1"); 
                    			}
                    	}?>
										<? if (CONFIG_COMPUL_FOR_AGENTLOGINS == '1' && ($agentType == 'SUBA' || $agentType == 'SUBAI' || $agentType == 'SUPA' || $agentType == 'SUPAI')) { echo "<font color='#ff0000'>*</font>"; } ?>
										<?
							/***** Code added by Usman Ghani aginst ticket #3471 Now Transfer - Beneficiary Address *****/
							if ( defined("CONFIG_BENEFICIARY_ADDRESS_MENDATORY")
								&& CONFIG_BENEFICIARY_ADDRESS_MENDATORY == 1 )
							{
							?>
										<strong><font color="#ff0000">*</font></strong>
										<?
							}
							/***** End of code aginst ticket #3471 Now Transfer - Beneficiary Address *****/
						?>
										&nbsp;</b></font></td>
									<td <? if(CONFIG_DISABLE_ADDRESS2 != '1'){echo('width="26%"');}else{echo('width="76%" colspan="3"');} ?>><input type="text" id="address" name="Address" value="<?=$_SESSION["Address"]; ?>" maxlength="254" <? if(CONFIG_DISABLE_ADDRESS2 == '1'){echo('size="90"'); }?>></td>
									<? if(CONFIG_DISABLE_ADDRESS2 != '1')
               {
               	?>
									<td width="20%" align="right"><font color="#005b90"><b>
										<? if(CONFIG_ADDRESS_LINE2_LABEL == "1"){ echo ADDRESS_LINE2_LABEL; }else{ ?>
										Address Line 2&nbsp;
										<? } ?>
										</b></font></td>
									<td width="37%"><input type="text" name="Address1" value="<?=$_SESSION["Address1"]; ?>" maxlength="254"></td>
									<?
                }
                ?>
								</tr>
								<?
				 }else{
				 ?>
								<tr bgcolor="#ededed">
									<td width="17%" height="20" rowspan="4" align="right" <? echo (CONFIG_IDTYPE_DROPDOWN != "1" ? "valign='top'" : "") ?>><font color="#005b90"><b>ID Type+&nbsp;</b></font></td>
									<td width="26%" rowspan="4"><?	if (CONFIG_IDTYPE_DROPDOWN == "1") {  ?>
										<select name="IDType">
											<option value="">- Select ID Type -</option>
											<option value="Passport" <? echo ($_SESSION["IDType"] == "Passport" ? "selected" : "") ?>>Passport</option>
											<option value="ID Card" <? echo ($_SESSION["IDType"] == "ID Card" ? "selected" : "") ?>>ID Card</option>
											<option value="Driving License" <? echo ($_SESSION["IDType"] == "Driving License" ? "selected" : "") ?>>Driving License</option>
										</select>
										<?	} else {  ?>
										<input type="radio" name="IDType" value="Passport" <? if ($_SESSION["IDType"] == "Passport" ) echo "checked"; ?>>
										Passport <br>
										<input type="radio" name="IDType" value="ID Card" <? if ($_SESSION["IDType"] == "ID Card") echo "checked"; ?>>
										<? if(CONFIG_ID_TYPE_NATIONAL_ID == "1"){ ?>
										National ID
										<? }else{ ?>
										ID Card
										<? } ?>
										<br>
										<input type="radio" name="IDType" value="Driving License" <? if ($_SESSION["IDType"] == "Driving License") echo "checked"; ?>>
										Driving Licence
										<?	}  ?>									</td>
								<tr bgcolor="#ededed">
									<td width="20%" align="right" valign="top"><font color="#005b90"><b>ID Number+&nbsp;</b></font></td>
									<td width="37%" valign="top"><input type="text" name="IDNumber" value="<?=$_SESSION["IDNumber"]; ?>" maxlength="32">									</td>
								</tr>
								<tr bgcolor="#ededed">
									<td width="20%" align="right" valign="top"><font color="#005b90"><b>Other ID Name </b> </font></td>
									<td valign="top"><input name="Otherid_name" type="text" value="<?=$_SESSION["Otherid_name"]; ?>" maxlength="15"></td>
								</tr>
								<tr bgcolor="#ededed">
									<td width="20%" align="right" valign="top"><font color="#005b90"><b>Other ID </b></font></td><td valign="top"><input name="Otherid" type="text" value="<?=$_SESSION["Otherid"]; ?>" maxlength="15"></td>
								</tr>
								<?
				 }	 
				 ?>
								<tr bgcolor="#ededed">
									<td width="20%" align="right"><font color="#005b90"><b> Zip/Postal Code
										<?
						if ( (CONFIG_COMPUL_FOR_AGENTLOGINS == '1' && ($agentType == 'SUBA' || $agentType == 'SUBAI' || $agentType == 'SUPA' || $agentType == 'SUPAI') && CONFIG_POSTCODE_NOTCOMPUL_FOR_AGENTLOGINS != 1 /*against #3639 by khola*/)
							 || ( defined("CONFIG_POST_CODE_MENDATORY") && CONFIG_POST_CODE_MENDATORY == 1 /* against #3467 by Usman Ghani */ ) )
						{
							echo "<font color='#ff0000'>*</font>";
						}
					?>
										&nbsp;</b></font></td>
									<td width="37%"><input type="text" name="Zip" value="<? echo stripslashes($_SESSION["Zip"]); ?>" maxlength="16"></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr bgcolor="#ededed">
									<?
								if(CONFIG_CITIZENSHIP == 1){
								?>
									<td width="17%" height="20" align="right"><font color="#005b90"><b>Citizenship</b></font></td>
									<td width="26%"><input type="text" name="Citizenship" value="<?=$_SESSION["Citizenship"]; ?>" maxlength="11"></td>
									<td width="17%" height="20" align="right">&nbsp;</td>
									<td width="26%">&nbsp;</td>
								</tr>
								<tr bgcolor="#ededed">
									<? 	
                  
                  if ($cpf == 1){ ?>
									<td width="17%" height="20" align="right"><font color="#005b90"><b>User Type</b></font></td>
									<td width="26%"><select name="userType" id="userType" onChange="newCaption();">
											<option value="Private User" <?=($_SESSION["userType"]=="Private User"?"selected":"")?>>Private User</option>
											<option value="Business User" <?=($_SESSION["userType"]=="Business User"?"selected":"")?>>Business User</option>
										</select>									</td>
									<td width="17%" height="20" align="right"><font color="#005b90"><b>CPF Number<font color="#ff0000">*</font>&nbsp;</b></font></td>
									<td width="26%"><input type="text" id="CPF" name="CPF" value="<?=$_SESSION["CPF"]; ?>" maxlength="20"></td>
									<?	} else {  ?>
									<td width="17%" height="20" align="right" colspan="4">&nbsp;</td>
									<?	}  ?>
								</tr>
								<? 
				  }else{
				  ?>
								<? 	if ($cpf == 1){ ?>
								<td width="17%" height="20" align="right"><font color="#005b90"><b>User Type</b></font><font color="red">*</font></td>
									<td width="26%"><select name="userType" id="userType" onChange="newCaption();">
											<option value="Private User" <?=($_SESSION["userType"]=="Private User"?"selected":"")?>>Private User</option>
											<option value="Business User" <?=($_SESSION["userType"]=="Business User"?"selected":"")?>>Business User</option>
										</select>									</td>
									<td width="17%" height="20" align="right"><div id="caption"><font color="#005b90"><b>CPF Number<font color="#ff0000">*</font>&nbsp;</b></font></div></td>
									<td width="26%"><input type="text" name="CPF" value="<?=$_SESSION["CPF"]; ?>" maxlength="20">
										<input type="hidden" id="cpfcpnj" name="cpfcpnj" value="CPF">
										<input type="submit" value="Validate" name="Submit" onClick=" document.forms[0].action='add-beneficiary-quick.php?from=<? echo $_GET["from"];?>&agentId2=<? echo $_GET["agentID"];?>&transID=<? echo $_GET["transID"]?>';document.forms[0].sel.value = 'country';	" />									</td>
									<?	} else {  ?>
									<td align="right" colspan="4">&nbsp;</td>
									<?	}  ?>
									<?
					}
					?>
								</tr>
								<tr bgcolor="#ededed">
									<? if(CONFIG_SHOW_STATES == "1"){?>
									<td width="17%" height="20" align="right"><font color="#005b90"><b>State</b></font>
									<td width="26%"><?php
								if(CONFIG_DEFAULT_COUNTRY == "1")
								{
									$countryName = CONFIG_DEFAULT_COUNTRY_VALUE;
									$stateQuery = "select distinct(state) from cities where 1 and state != '' and country = '".$countryName."' order by state";
								}
								else
									$stateQuery = "select distinct(state) from cities where state != '' and country = '".$_POST["Country"]."' order by state";

								//debug($stateQuery);
								$state = selectMultiRecords($stateQuery);
							?>
										<select name="State" style="font-family:verdana; font-size: 11px">
											<option value="">- Select State-</option>
											<? for ($i=0; $i < count($state); $i++){ ?>
											<option value="<?=$state[$i]["state"]; ?>">
											<?=$state[$i]["state"]?>
											</option>
											<? } ?>
										</select>
										<script language="JavaScript">SelectOption(document.forms[0].State, "<?=$_SESSION["State"]; ?>");</script>									</td>
									<? 
					}else{ ?>
									<td width="17%" height="20" align="right"><font color="#005b90"><b> <? echo (CONFIG_STATE_NAME!="CONFIG_STATE_NAME" ? CONFIG_STATE_NAME : "County"); ?>&nbsp;</b></font></td>
									<td width="26%"><input type="text" name="State" value="<?=$_SESSION["State"]; ?>" maxlength="25"></td>
									<?
				  }
				  ?>
									<td width="20%" align="right"><font color="#005b90"><b>City
										<? if(CONFIG_CITY_NON_COMPULSORY != '1'){  ?>
										<font color="#ff0000">*</font>
										<? } ?>
										</b></font></td>
									<td width="37%"><input type="text" name="City" value="<?=$_SESSION["City"]; ?>" maxlength="25"></td>
								</tr>
								<?
					// Added by Usman Ghani aginst ticket #3473 Now Transfer - Phone/Mobile validation
					$jsCode = "";
					if ( defined("CONFIG_PHONE_NUMBER_NUMERIC")
						&& CONFIG_PHONE_NUMBER_NUMERIC == 1 )
					{
						$jsCode = " onkeyup='phoneEventHandler(event);' ";
					}
					// End of code against ticket #3473 Now Transfer - Phone/Mobile validation

					// Added by Usman Ghani aginst ticket #3472: Now Transfer - Phone/Mobile
					if ( CONFIG_PHONES_AS_DROPDOWN == 1 )
					{ 	
						$phoneMandatory = "";
						if($phoneMandatoryCountFlag && $_SESSION["Country"]=="United Kingdom"){
							$phoneMandatory = "<font color='#ff0000'>*</font>";
						}
					?>
								<tr bgcolor="#ededed">
									<td width="17%" height="20" align="right"><font color="#005b90"><b>Phone<?=$phoneMandatory?></b></font> </td>
									<td align="left" colspan="3"><?
								$officePhoneSelected = "";
								$mobilePhoneSelected = "";
								$phoneNumber = ( !empty($_SESSION["Phone"])  ? $_SESSION["Phone"] : ( !empty($_SESSION["Mobile"])  ? $_SESSION["Mobile"] : "" ) );

								if ( !empty($_POST["phoneType"]) )
								{
									if ( $_POST["phoneType"] == "phone" )
										$officePhoneSelected = "selected='selected'";
									else if ( $_POST["phoneType"] == "mobile" )
										$mobilePhoneSelected = "selected='selected'";
								}
								else
								{
									if ( !empty($_SESSION["Phone"]) )
										$officePhoneSelected = "selected='selected'";
									else if ( !empty($_SESSION["Mobile"]) )
										$mobilePhoneSelected = "selected='selected'";
								}
							?>
										<input type="text" id="Phone" name="Phone"  value="<?=$phoneNumber; ?>" maxlength="30" />
										&nbsp;Type
										<select id="phoneType" name="phoneType">
											<option value="">Select One</option>
											<option value="phone" <?=$officePhoneSelected;?> >Home Phone</option>
											<option value="mobile" <?=$mobilePhoneSelected;?> >Mobile Phone</option>
										</select>									</td>
								</tr>
								<tr bgcolor="#ededed">
									<td width="17%" height="20" align="right"><font color="#005b90"><b>Email</b></font> </td>
									<td width="20%" align="left"><input type="text" name="emailAddress" maxlength="30" value="<?=$_SESSION["emailAddress"]?>" />									</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
					</tr>
								<?
					} // End of code aginst ticket #3472: Now Transfer - Phone/Mobile
					else // The code, which was previously implemented, has been shifted into the following else clause after implementation of ticket #3472: Now Transfer - Phone/Mobile

					{
					?>
								<tr bgcolor="#ededed">
									<td width="17%" height="20" align="right"><font color="#005b90"><b>Phone
										<? if(CONFIG_PHONE_NON_COMPULSORY != '1'){ ?>
										<font color="#ff0000">*</font>
										<? } ?>
										</b></font></td>
									<td width="26%"><input type="text" name="Phone" <?=$jsCode;?>  value="<?=$_SESSION["Phone"]; ?>" maxlength="30"></td>
									<?	if (CONFIG_MOBILE_FIELD_OFF == '1') {  ?>
									<td width="20%">&nbsp;</td>
									<td width="37%">&nbsp;</td>
									<?	} else {  ?>
									<td width="20%" align="right"><font color="#005b90"><b>Mobile<font color="#ff0000">
										<? if(CONFIG_MOBILE_MANDATORY == '1'){ ?>
										*
										<? }?>
										</font></b></font></td>
									<td width="37%"><input type="text" name="Mobile" <?=$jsCode;?>  value="<?=$_SESSION["Mobile"]; ?>" maxlength="25"></td>
									<?	}  ?>
								</tr>
								
								<?
					}
					?>

								<?
           		/**
           		 * @ticket# 3474
         		 */

if(CONFIG_BEN_BANK_DETAILS == "1" && !empty($_SESSION["Country"])) {
						?>
						<tr>
								<td colspan="4" style="background:#CCCCCC;"><font color="#005b90"><b>Beneficiary Bank Details</b></font></td>
							</tr>
						<tr bgcolor="#ededed"> 
								<td width="100%" align="center" colspan="4" valign="middle">
									<font color="#005b90"><b>Bank Name</b></font>
									<input type="text" size="10" name="searchBankName" id="searchBankName" />
									<input type="button" onClick="if(document.frmBenificiary.searchBankName.value == '' || IsAllSpaces(document.frmBenificiary.searchBankName.value)) { alert ('Please provide bank name to search.') } else { window.open('search-bank.php?from=add-beneficiary-quick.php&search=Y&transID=<?=$_GET["transID"]?>'+'&benID=<?=$_GET["benID"]?>&bankName='+ document.frmBenificiary.searchBankName.value +'&swiftCode=&benCountry=<?=$_SESSION["Country"]?>', 'searchBank', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740'); document.forms[0].sel.value = 'country'; document.forms[0].action='add-beneficiary-quick.php?from=<? echo $_GET["from"];?>&agentID=<?=$_GET["agentID"];?>&customerID=<?=$_GET["customerID"];?>&transID=<? echo $_GET["transID"];?>&r=<?=$_GET["r"]?>'; }" value="Search" name="Submit"/>
									
									 &nbsp;&nbsp;&nbsp;<a onClick=" window.open('add-bank.php?from=popUp&opener=left&transID=<?=$_GET["transID"]?>','', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" href="javascript:;"><b style="color:#000000">Add New Bank</b></a>								</td>
					  		</tr>
						<?
								 if(!empty($_SESSION["ben_bank_id"]))
									$readFlag = "readonly";
							?>
						<tr bgcolor="#ededed"> 
									  <td width="17%" height="20" align="right">
										<font color="#005b90"><b>Bank Id&nbsp;</b></font>									  </td>
									  <td width="26%">
										<input type="text" name="bankId" id="bankId" value="<?=$_SESSION["bankId1"]?>" maxlength="30" <?=$readFlag?>>									  </td>
									  <td width="20%">
										  <font color="#005b90"><b>Country</b></font><font color="red">*</font>&nbsp;									  </td>
									  <td width="37%">
									  
									  
									  <? if(!empty($_SESSION["ben_bank_id"])) {?>
										<input type="text" name="country1" id="country1" value="<?=$_SESSION["country1"]?>" maxlength="30" readonly="readonly" />
									  <? }else{ ?>			
										<select name="country1" id="country1"> 
											<option value="">- Select Country -</option>
										<?		
											$benBankCountrySql = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = toCountryId  $countryTypes order by countryName";
										   if($countryBasedFlag){
											   $benBankCountrySql = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE_NEW." where 1 and countryId = toCountryId  $countryTypes order by countryName";
										   }
											$myCountries = selectMultiRecords($benBankCountrySql);
										
											for ($i = 0; $i < count($myCountries); $i++)
											{
												if ($myCountries[$i]["countryName"] == $_SESSION["country1"])
												{
										?>
												  <option  value="<?=$_SESSION["country1"];?>" selected><?=$myCountries[$i]["countryName"]?></option>
										<?
												}
												else
												{
										?>
												  <option value="<?=$myCountries[$i]["countryName"]; ?>"><?=$myCountries[$i]["countryName"]; ?></option>					
										<?
												}
											} 
										
										?>		
										</select>		
									<? } ?>
									  
										<!--input type="text" name="country1" id="country1" value="<?=$_SESSION["country1"]?>" maxlength="30" <?=$readFlag?> -->									  </td>
									</tr>
						<tr bgcolor="#ededed"> 
									  <td width="17%" height="20" align="right">
										<font color="#005b90"><b>Bank Name<font color="red">*</font>&nbsp;</b></font>									  </td>
									  <td width="26%">
										<input type="text" name="bankName" id="bankName" value="<?=$_SESSION["bankName1"]?>" maxlength="30" <?=$readFlag?>>									  </td>
									  <td width="20%">
										  <font color="#005b90"><b>Swift Code</b></font>&nbsp;									  </td>
									  <td width="37%">
										<input type="text" name="swiftCode" id="swiftCode" value="<?=$_SESSION["swiftCode1"]?>" maxlength="30">									  </td>
									</tr>
						<tr bgcolor="#ededed"> 
									  <td width="17%" height="20" align="right">
										<font color="#005b90"><b>Branch Code/Name&nbsp;</b></font>									  </td>
									  <td width="26%">
										<input type="text" name="branchName" id="branchName" value="<?=$_SESSION["branchCode1"]?>" maxlength="30">									  </td>
									  <td width="20%">
										  <font color="#005b90"><b>Branch Address</b></font>&nbsp;									  </td>
									  <td width="37%">
										<input type="text" name="branchAddress" id="branchAddress" value="<?=$_SESSION["branchAddress1"]?>" maxlength="30" >									  </td>
									</tr>
						<tr bgcolor="#ededed"> 
									  <td width="17%" height="20" align="right">
										<font color="#005b90"><b>Account Number<font color="red">*</font>&nbsp;</b></font>									  </td>
									  <td width="26%">
										<input type="text" name="account" id="account" value="<?=$_SESSION["account1"]?>" maxlength="30">									  </td>
									  <td width="20%">
										  <font color="#005b90"><b>IBAN</b></font>&nbsp;									  </td>
									  <td width="37%">
										<input type="text" name="iban" id="iban" value="<?=$_SESSION["iban1"]?>" maxlength="30" onBlur="validateIban();">
										<div id="benIbanValidStatus"></div>
										<input type="hidden" id="ibnStatus" value="0">									  </td>
									</tr>
						<tr bgcolor="#ededed"> 
								  <td width="17%" height="20" align="right" valign="top">
									<font color="#005b90"><b>Account Type<font color="red">*</font>&nbsp;</b></font>								  </td>
								  <td width="26%">
									<?
										if($_SESSION["accountType1"] == "Savings")
										{
											$svingAcnt = "Checked";
											$crntAcnt = "";
										}
										else
										{
											$svingAcnt = "";
											$crntAcnt = "Checked";
										}
									?>
									<input type="radio" name="accountType" id="accountType" value="Current" maxlength="30" <?=$crntAcnt?>>&nbsp;Current
									<br />
									<input type="radio" name="accountType" id="accountType" value="Savings" maxlength="30" <?=$svingAcnt?>>&nbsp;Saving								  </td>
								  <td width="20%">&nbsp;								  </td>
								  <td width="37%">&nbsp;
									<input type="hidden" name="usedBankId" value="<?=$_SESSION["ben_bank_id"]?>" />								  </td>
								</tr>
<? } ?>
						<tr bgcolor="#ededed">
									<td width="17%" height="20" align="right">&nbsp;</td>
									<td width="26%">&nbsp;</td>
									<td width="20%" align="right"><font color="#005b90"><b>&nbsp;</b></font></td>
									<td width="37%">&nbsp;</td>
								</tr>
						</table>
					</td>
					</tr>
					<tr bgcolor="#ededed">
						<td colspan="2" align="center"><input type="hidden" name="customerID" value="<? echo $_SESSION["customerID"]?>">
							<input name="Save" id="formSubmitButton" type="submit" value=" Save "  onClick="return checkForm(document.frmBenificiary);">
							&nbsp;&nbsp;
<?
		if(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER == "1"){
			$buttonType = 'button';
			$functionCalled = "onClick=\"showHideIbanBank(this);\"";
		}
?>
							<input id="reset" name="reset" type="<?=($buttonType!="" ? $buttonType : "reset");?>" value=" Clear " <?=($functionCalled !="" ? $functionCalled : "");?>>
						</td>
					</tr>
				</table></td>
		</tr>
	</form>
</table>
</body>
</html>
