<?php
session_start();
include ("../include/config.php");
require_once("lib/phpmailer/class.phpmailer.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];

$partnerID=$_GET['partnerID'];
$query= selectFrom("SELECT `APIPatrner/ThirdParty` FROM partnerList WHERE `APIPatrner/ThirdParty_ID`=$partnerID");
$partnerName=$query['APIPatrner/ThirdParty'];

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
$headers .= "X-Mailer: PHP v".phpversion()."\r\n";

if($agentType == 'SUPA' || $agentType == 'SUBA') {
    if($_GET['flag']=='disable'){
        $action = "deactivate";
    }
    else {
        $action = "activate";
    }
    $emailContent = selectFrom("select * from email_templates left join events on email_templates.eventID=events.id where email_templates.eventID='17' and email_templates.status = 'Enable' order by updatedDate desc");
    $message1 = $emailContent["body"];
    $query=selectFrom("select agentContactPerson, agentCompany from admin where userID=$parentID");
    $varEmail = array("{action}","{partnerName}","{agentContactPerson}","{companyName}");
    $contentEmail = array($action,$partnerName,$query['agentContactPerson'],$query['agentCompany']);
    $messageT = str_replace($varEmail,$contentEmail,$message1);

    $subject = $emailContent["subject"];
    $customerMailer = new PHPMailer();
    $strBody = $messageT;
    $fromName  = $emailContent["fromName"];
    $fromEmail  = $emailContent["fromEmail"];

    $customerMailer->FromName =  $fromName;
    $customerMailer->From =  $fromEmail;
    if(CONFIG_SEND_EMAIL_ON_TEST == "1")
    {
        $customerMailer->AddAddress(CONFIG_TEST_EMAIL,'');
    }
    else
    {
//        $customerMailer->AddAddress('Asim.tariq@payscanner.com;wajiha.luqman@hbstech.co.uk','');//Asim.tariq@payscanner.com;wajiha.luqman@hbstech.co.uk
        if($emailContent['cc'])
            $customerMailer->AddAddress($emailContent['cc'],'');

        //Add BCC
        if($emailContent['bcc'])
            $customerMailer->AddBCC($emailContent['bcc'],'');

    }
    $customerMailer->Subject = $subject;
    $customerMailer->IsHTML(true);
    $customerMailer->Body = $strBody;

    if(CONFIG_EMAIL_STATUS == "1")
    {
        $custEmailFlag = $customerMailer->Send();
    }
    insertInto("INSERT INTO notifications VALUES (0, $parentID,$partnerID,'$messageT','".date('Y-m-d H:i:s')."',0)");
    echo "Your request has been sent";
} else {
    if($_GET['flag']=='disable'){
        $action = "deactivate";
        update("update partnerList set status='Disable' where `APIPatrner/ThirdParty`='$partnerName'");

    }
    else {
        $action = "activate";
        update("update partnerList set status='Enable' where `APIPatrner/ThirdParty`='$partnerName'");
    }
}
//header("Location: partner-list.php");
?>