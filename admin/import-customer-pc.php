<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
require_once './cm_xls/Excel/reader.php';

//$agentType = getAgentType();

Function ExcelSerialDateToDMY($nSerialDate)
{
    // Excel/Lotus 123 have a bug with 29-02-1900. 1900 is not a
    // leap year, but Excel/Lotus 123 think it is...
    if ($nSerialDate == 60)
    {
        $nDay    = 29;
        $nMonth    = 2;
        $nYear    = 1900;

        return;
    }
    else if ($nSerialDate < 60)
    {
        // Because of the 29-02-1900 bug, any serial date 
        // under 60 is one off... Compensate.
        $nSerialDate++;
    }

    // Modified Julian to DMY calculation with an addition of 2415019
    $l = $nSerialDate + 68569 + 2415019;
    $n = (int)(( 4 * $l ) / 146097);
            $l = $l - (int)(( 146097 * $n + 3 ) / 4);
    $i = (int)(( 4000 * ( $l + 1 ) ) / 1461001);
        $l = $l - (int)(( 1461 * $i ) / 4) + 31;
    $j = (int)(( 80 * $l ) / 2447);
     $nDay = $l - (int)(( 2447 * $j ) / 80);
        $l = (int)($j / 11);
        $nMonth = $j + 2 - ( 12 * $l );
    $nYear = 100 * ( $n - 49 ) + $i + $l;
    
     return($nYear."-".$nMonth."-".$nDay);
}


if($_POST["Submit"] != "")
{
	if ($csvFile=='none')
	{
		$msg = CAUTION_MARK . " Please select file to import.";
	}
	$ext = strtolower(strrchr($csvFile_name,"."));
	if ($ext == ".txt" || $ext == ".csv" || $ext == ".xls" )
	{
		if (is_uploaded_file($csvFile))
		{
			$Pict="$username-" . date("Y-m-d") . $ext;
			//move_uploaded_file($csvFile, "../uploads/" . $Pict );

			// Parsing file and inserting into DB
			//$filename= "../uploads/$Pict";
			$filename = $csvFile;
			$data = new Spreadsheet_Excel_Reader();
			// Set output Encoding.
			$data->setOutputEncoding('CP1251');
			$data->read($filename);
			error_reporting(E_ALL ^ E_NOTICE);
			$tran_date = date("Y-m-d");
			
			$counter = 0;
			$counter2 = 0;
			$counterEmptyNo = 0;
			$emptyNoMsg = "";
			for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) 
			{
				
				$data1[1]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][1]);  //customer number
				$data1[2]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][2]);  //Title
				$data1[3]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][3]);  //firstName*
				$data1[4]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][4]);  //middleName
				$data1[5]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][5]);	// lastName*
				$data1[6]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][6]);	//Address*
				
				$data1[7]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][7]);  //Address1
				$data1[8]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][8]);	// Country*
				$data1[9]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][9]);	// City*
				$data1[10]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][10]);  //Zip
				$data1[11]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][11]);  //State
				$data1[12]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][12]);  //Phone*
				$data1[13]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][13]);  //Mobile
				$data1[14]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][14]);  //email
				$data1[15]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][15]);  //dob
				$data1[16]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][16]);  //IDType
				
				
				$data1[17]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][17]);  //IDNumber
				$data1[18]  = $data->sheets[0]['cells'][$i][18];  //IDExpiry
				$data1[19]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][19]);  //otherId
				$data1[20]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][20]);  //otherId_name
				
				$password = createCode();
				
				if($data1[15] == '')
				{
					$date = 0000-00-00;
				}
				else
				{
					$date = ExcelSerialDateToDMY($data1[15]);
				}
				
				if($data1[18] == '')
				{
					$date1 = 0000-00-00;
				}
				else
				{
					$date1 = ExcelSerialDateToDMY($data1[18]); 
				}
				
				$customerName = $data1[3] . " " . $data1[4] . " " . $data1[5];
				$field = "customerName,Address,City,Phone";
				$values = $customerName.",".$data1[6].",".$data1[9].",".$data1[12];
				$tble = "customer";
				$retValue = "customerID";
				
				$var = checkExistence($field,$values,$tble,$retValue);
				if($data1[1] == ""){
					$emptyNoMsg .= " $customerName has empty customer Number.";
					$counterEmptyNo ++;
					
				}else{
					/*if($var == '')
					{*/
				
					$Querry_Sqls = "INSERT INTO ".TBL_CUSTOMER." (password, Title, firstName, middleName, lastName, Address, 
					Address1, Country, City, Zip, State, Phone, Mobile, email, dob, acceptedTerms, IDType, 
					IDNumber, IDExpiry, otherId, otherId_name, created, customerName) VALUES 
					('$password','".$data1[2]."', '".$data1[3]."', '".$data1[4]."', '".$data1[5]."', '".$data1[6]."',
					 '".$data1[7]."', '".$data1[8]."', '".$data1[9]."', '".$data1[10]."', '".$data1[11]."', '".$data1[12]."', '".$data1[13]."', '".$data1[14]."', '".$date."','Yes','".$data1[16]."',
					  '".$data1[17]."', '".$date1."', '".$data1[19]. "','" .$data1[20]. "','".$tran_date."','".$customerName."')";
					
					insertInto($Querry_Sqls);
	        		
										
					$customerID = mysql_insert_id();
					
					
						$customerCode = "C-".$data1[1];
						$numberQuery = " update ".TBL_CUSTOMER." set accountName = '".$customerCode."' where customerID = '".$customerID."'";
						update($numberQuery);	
		
					$counter ++;
					/*}	
					else
					{
						$counter2 ++;
					}*/
				}
			
			}
			$msg = " $counter Customers are Imported Successfully."; //There were $counter2 duplicate values.";
			
			//exit;
				// END
		}
		else
		{
			$msg = CAUTION_MARK . " Your file in not uploaded due to some error.";
		}
	}
	else
	{
		$msg = CAUTION_MARK . " File format must be .txt, .csv or .dat";
	}	
}


// $logoContent = selectFrom("select logo from " . TBL_ADMIN_USERS. " where username='$username'");



?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: normal;
}
-->
</style>
<body>
<script language="javascript">
function checkForm(theForm) {
	if(theForm.transID.value == "" || IsAllSpaces(theForm.transID.value)){
    	alert("Please provide transaction ID.");
        theForm.transID.focus();
        return false;
    }
	if(theForm.subject.value == "" || IsAllSpaces(theForm.subject.value)){
    	alert("Please provide the subject for complaint.");
        theForm.subject.focus();
        return false;
    }
	if(theForm.details.value == "" || IsAllSpaces(theForm.details.value)){
    	alert("Please provide the details of complaint.");
        theForm.details.focus();
        return false;
    }
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }

</script>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc"><b><strong><font color="#FFFFFF">Import Senders </font></strong></b></td>
  </tr>
  <tr>
    <td>
	<table width="800"  border="0">
  <tr>
    <td  valign="top"><fieldset>
    <legend class="style2">Import Senders </legend>
    <br>
			<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
			<form name="addComplaint" action="import-customer-pc.php" method="post" enctype="multipart/form-data">
			  
			  <tr align="center" class="tab-r">
			    <td colspan="2">Please follow the instructions on the Readme.txt file.</td>
			    </tr>
			  <tr>
				<td width="25%" align="center" bgcolor="#DFE6EA" colspan="2">
				<a href="#" onClick="javascript:window.open('import-readme.php?page=custben', 'ReadMe', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=250,width=650,top=200,left=170')" class="style2">
				Readme.txt
			  </a>	
				</td>
			  </tr>
			  <tr>
				<td width="25%" align="center" bgcolor="#DFE6EA" colspan="2">
				<a href="xls/sampleTempSenderPC.xls" class="style2">Sample Template</a>	
				</td>
			  </tr>
			  <tr>
				<td width="25%" align="right" bgcolor="#DFE6EA">Excel File </td>
			    <td width="75%" bgcolor="#DFE6EA">			      <input name="csvFile" type="file" id="csvFile" size="15"></td>
			  </tr>
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">&nbsp;</td>
			    <td align="center" bgcolor="#DFE6EA"><input name="Submit" type="submit" class="flat" value="Submit"></td>
		      </tr></form>
		      <? if($msg!= "")
			  {
			  ?>
			  <tr align="center">
			  	<? if($msg!= " $counter Customers are Imported Successfully."){ ?>
			    <td colspan="2"><font color="#CC0000"><strong><? echo $msg?></strong><br></td>
			    	<? } else { ?>
			    	 <td colspan="2"><b><font color= <? echo SUCCESS_COLOR ?> ><? echo $msg?></font></b><br></td>
			    	 	<? } ?>
			    </tr>
			  
			  <?
			  }
			  ?>
			   <? if($emptyNoMsg!= "")
			  {
			  ?>
			  <tr align="center">
			  	<td colspan="2"><font color="#CC0000"><strong><? echo $emptyNoMsg?></strong><br></td>
			  </tr>
			  
			  <?
			  }
			  ?>
		      
			</table>
		    <br>
        </fieldset></td>
    <td width="431" valign="top">
	<? if ($str != "")
	{
	?>
	
	<fieldset>
    <legend class="style2">Import Senders Results </legend>
    <br>
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
      <form name="addComplaint" action="import-customer-pc.php" method="post" enctype="multipart/form-data">
        <tr>
          <td valign="top" bgcolor="#DFE6EA"><font color="#3366FF"><? echo $str?></font></td>
          </tr>
          
      </form>
    </table>
    <br>
    </fieldset>
	<?
	}
	?>
	</td>
  </tr>
  
  
</table>

	</td>
  </tr>
</table>
</body>
</html>
