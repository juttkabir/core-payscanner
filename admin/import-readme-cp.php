<!-- <?	if ($_GET["page"] == "custben") {  ?> -->
Follow the instructions to correctly upload collection points:
<br><br>
NOTE: The upload file needs to be created in excel. 
<br><br>
1. Please click on the "Sample Template" link.<br>
2. The sequence of column is most important.<br>
3. Select distributor from drop down.  <br>
4. If you want to import a new collection point then that collection point data should have a <br>
unique branch number otherwise if collection point against that branch number already exists against the selected distributor collection point data will be updated.
<!--<?	} elseif ($_GET["page"] == "exchrate") {  ?>
Follow the instructions to correctly upload the exchange rate:
<br><br>
1. Please click on the "Sample Template" link to download sample file.<br>
2. The upload file needs to be created in excel.<br>
3. The excel file should be same as Sample Template
<?	}  ?> -->