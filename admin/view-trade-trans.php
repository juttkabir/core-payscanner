<?
session_start();
/** 
 *  Short Description
 * 	This page is used to diplay details about a certain trade
 *  @package Trading
 *  @subpackage Edit/Cancel Trade
 *  @copyright HBS Tech. (Pvt) Ltd.
 */
include ("../include/config.php");
$date_time = date('Y-m-d  h:i:s');
dbConnect();
$username=loggedUser();
$acid=$_GET["acid"];
//debug($_REQUEST);
if(!empty($_REQUEST["acid"]))
{
$accountDetails = selectFrom("select * from account_details where acid ='".$_REQUEST["acid"]."'");
//debug ($agentDetails);
}
if(!empty($_REQUEST["acid"]) && $_REQUEST["cancel"] == 'Cancel')
{
	$strInsertSql = "update account_details 
											set
											  note   = '".$_REQUEST["cancelReason"]."',
											  status = 'TC',
											  updated = '".$date_time."'
											  where acid ='".$_REQUEST["acid"]."'";
			if(update($strInsertSql))
				$msg = "The record is deleted successfully.";
}
?>
<html>
<head>
	<title>View Trade Transaction</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css"><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.goback
{
	font-size:12px;
	text-align:left;
	font-weight:bold;
}
-->
</style></head>
<body>
<table width="35%" border="0" align="center" cellpadding="5" cellspacing="1">
 
    <td align="center">
		<table width="426" border="0" cellspacing="1" cellpadding="2" align="center">
	
       <? if(!empty($msg)){?>
		 <tr bgcolor="#ededed" class="goback">
			 <td  width="140"><a href="update-trade.php"><font color="#000000">Go Back</font></a></td>
			 <td><?=$msg?></td>
		 </tr>
		  <tr>
    <? } ?>
	    <tr bgcolor="#ededed">
          <td colspan="2"><div align="center"><strong>View Trade Transaction </strong></div></td>
          </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Date</strong></font></td>
            <td><?=stripslashes($accountDetails["created"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Currency Buy </strong></font></td>
            <td><?=stripslashes($accountDetails["currencyBuy"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Amount Buy </b></font></td>
			<?php
			/**
			 *	Condition to change the number format of amount buy
			 *	if the below config is enabled and defined
			 */
			if( defined("CONFIG_ROUND_NUMBER_TRADING") && CONFIG_ROUND_NUMBER_TRADING != "0" )
			{ ?>
				<td><?=number_format($accountDetails["amountBuy"],2); ?></td>
	  <?php }
	  		else
			{ ?>
            	<td><?=stripslashes($accountDetails["amountBuy"]); ?></td>
	  <?php } ?>
        </tr>
        <tr bgcolor="#ededed">
           <td width="159" valign="top"><font color="#005b90"><b>Exchange Rate </b></font></td>
		   <?php
		    /**
			 *	Condition to change the number format of exchange rate
			 *	if the below config is enabled and defined
			 */
		   if( defined("CONFIG_ROUND_NUMBER_TRADING") && CONFIG_ROUND_NUMBER_TRADING != "0" )
			{ ?>
				<td><?=number_format($accountDetails["exchangeRate"],4); ?></td>
	  <?php }
	  		else
			{?>
           		<td><?=stripslashes($accountDetails["exchangeRate"]); ?></td>
	  <?php } ?>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Currency  Sell </b></font></td>
            <td><?=$accountDetails["currencySell"]; ?></td>
        </tr>
		
        <tr bgcolor="#ededed">
          <td width="159" valign="top"><strong><font color="#005b90">Amount Sell </font></strong></td> 	
		   <?php
		    /**
			 *	Condition to change the number format of amount sell
			 *	if the below config is enabled and defined
			 */
		   if( defined("CONFIG_ROUND_NUMBER_TRADING") && CONFIG_ROUND_NUMBER_TRADING != "0" )
		   { ?>
				<td><?=number_format($accountDetails["amountSell"],2); ?></td>
	 <?php }
     	   else
		   {?>		
          		<td><?=$accountDetails["amountSell"];?></td>
	 <?php } ?>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Credit Account </b></font></td>
            <td><?=stripslashes($accountDetails["accountBuy"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Debit Account </strong></font></td>
            <td><?=stripslashes($accountDetails["accountSell"]); ?></td>
        </tr>        
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Description</strong></font></td>
            <td><?=stripslashes($accountDetails["description"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Trade Number </strong></font></td>
            <td><?=stripslashes($accountDetails["transNumber"]); ?></td>
       </tr>
       <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Reference Number </strong></font></td>
            <td><?=stripslashes($accountDetails["refNumberIM"]); ?></td>
       </tr>
		 <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>System Trade No</strong></font></td>
            <td><?=stripslashes($accountDetails["sys_Trade_No"]); ?></td>
        </tr>
		<tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Trade Category</strong></font></td>
            <td><?
			 if($accountDetails["trade_Category"] == "")
				{
				echo "System";
				}
			elseif($accountDetails["trade_Category"] == "S")
				{
				echo "Statement";
				}  
			
			//=stripslashes($accountDetails["trade_Category"]);

			?></td>
        </tr>
       <? if($_REQUEST["action"] == 'cancel') { ?>
	   <form name="cancelTrans" action="view-trade-trans.php">		   	
		<tr bgcolor="#ededed">
			<td width="159" valign=top><strong>Cancel Reason</strong><font color="red">*</font></td>
            <td width="272"><textarea name="cancelReason" cols="40" rows="4" style="font-family: verdana; font-size: 11px"><?=stripslashes($_REQUEST["cancelReason"]); ?></textarea></td>
            <tr bgcolor="#ededed"> 
		  	<td></td>
            <td>
			<input type="hidden" name="acid" value="<?=$_REQUEST["acid"]?>">
			<input type="submit" name="cancel" value="Cancel">
			</td>
          </tr>       	
	   </form>
	   <? } ?>
      </table>
	</td>
  </tr>
</table>
</body>
</html>
