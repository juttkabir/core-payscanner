<?php

session_start();

include ("../include/config.php");
include ("security.php");

$filters = "";
$agentName = $_REQUEST["agentName"];
/**
 * Date filter
 */
$fromDate = $_REQUEST["fromDate"];
$toDate = $_REQUEST["toDate"];
$exportType = $_REQUEST["exportType"];

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
if(!empty($_REQUEST["pageUrl"]))
	maintainReportLogs($_REQUEST["pageUrl"],'E');

if($exportType!=""){
	$Balance="";
	//if($_REQUEST["Submit"]=="search")
	$slctID = $_REQUEST["agentID"];
	$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
	$accountQuery = "Select * from ".TBL_AnD_ACCOUNT." where agentID = $slctID";
	$accountQuery .= " and $queryDate";				
	$accountQuery .= " and status != 'Provisional' ";

	/* adding the currency criteria */
	if($_REQUEST["currency"]!="")
		$accountQuery .= " and currency = '".$_REQUEST["currency"]."' ";

	$contentsAcc = selectMultiRecords($accountQuery." order by aaID ");
}
	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	if($exportType == "xls")
	{
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=AnDAccountStatementMT-".time().".xls"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
	}
	elseif($exportType == "csv")
	{
		header("Content-Disposition: attachment; filename=AnDAccountStatementMT-".time().".csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "";
		$strColumnEnd   = ",";

	}
	else
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Export AnD Account Statement</title>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;
	$strFullHtml .= $strRowStart;

	$strFullHtml .= $strColumnStart.$strBoldStart."DATE".$strBoldClose.$strColumnEnd;
	if(CONFIG_AGENT_STATEMENT_BALANCE == '1') {
		$strFullHtml .= $strColumnStart.$strBoldStart."Opening Balance".$strBoldClose.$strColumnEnd;
	}
	$strFullHtml .= $strColumnStart.$strBoldStart."Modified By".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Money In".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Money Out".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Transaction".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Description".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Note".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Status".$strBoldClose.$strColumnEnd;
	if(CONFIG_AGENT_STATEMENT_BALANCE == '1') {
		$strFullHtml .= $strColumnStart.$strBoldStart."Closing Balance".$strBoldClose.$strColumnEnd;
	}
	$strFullHtml .= $strRowEnd;

	$strFullHtml .= $strRowStart;
	$preDate = "";
	$oldDate = "";
	$openingBalanceSwap = "";
	for($s=0;$s < count($contentsAcc);$s++)
	{
		if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
		{
			$currDate = $contentsAcc[$s]["dated"];
			if($currDate != $preDate && CONFIG_SWAP_CLOSING_TO_OPENING!="1")
			{
				if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settleCurr!='') {
					$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate' and currency='".$settleCurr."' ");
				}
				else if($_REQUEST["currency"]!=""){
					$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate' and currency = '".$_REQUEST["currency"]."' ");
				}
				else{
					$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate'");
				}
				$FirstDated = $datesFrom["datedFrom"];
				$openingBalance = 0;
				
				if($FirstDated != "")
				{
					$account1 = "Select opening_balance, closing_balance from ".TBL_ACCOUNT_SUMMARY." where 1";
					$account1 .= " and dated = '$FirstDated'";				
					$account1 .= " and  user_id = '".$slctID."' ";	
					if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == "1" && $settleCurr!='')
						$account1 .= " and  currency='".$settleCurr."' ";		
					if($_REQUEST["currency"]!="")
						$account1 .=" and currency = '".$_REQUEST["currency"]."' ";								

					$contents1 = selectFrom($account1);
					
					if($currDate == $FirstDated){
						$openingBalance = $contents1["opening_balance"];
					}
					else{
						$openingBalance =  $contents1["closing_balance"];
				
					}
				}
				
				
				$closingBalance = $openingBalance;
				$preDate = $currDate;
				$_SESSION["openingBalance"] = $openingBalance;
				//$_SESSION["settleOpeningBalance"] = $settleOpeningBalance;
				$_SESSION["currDate"] = $currDate;

			}
		/* # 4635 - To show opening balances of same currencies. and swaping closing of previous date
		 to opening of next date otherwise keeping same(ashahid)
		 */
		else if(defined("CONFIG_SWAP_CLOSING_TO_OPENING") && CONFIG_SWAP_CLOSING_TO_OPENING == "1"){
				if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settleCurr!='') {
					$summaryFirst = selectFrom("select id as firsID from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated = '".$currDate."' and currency='".$settleCurr."'  order by id");
				}
				else if($_REQUEST["currency"]!=""){
					$summaryFirst = selectFrom("select id as firsID from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated = '".$contentsAcc[$s]["dated"]."' and currency = '".$_REQUEST["currency"]."' order by id");
					//debug("select id as firsID from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated = '".$currDate."' and currency = '".$_REQUEST["currency"]."' order by id");
				}
				else{
					$summaryFirst = selectFrom("select id as firsID from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated ='".$currDate."' order by id");
				}
				$firsID = $summaryFirst["firsID"];
				if($_REQUEST["currency"]!="" && $firsID!=""){
					$account1 = "Select opening_balance, closing_balance from ".TBL_ACCOUNT_SUMMARY." 
											where id = '".$firsID."'" ; 
					$contents1 = selectFrom($account1);
					if($preDate == $currDate){
						$openingBalance = $openingBalanceSwap;
						$closingBalance = $closingBalance;
						//debug($oldDate ."=". $currentDate);
					}
					else if($preDate < $currDate && $preDate!=""){
						$openingBalance =  $closingBalance;
						$closingBalance = $openingBalance;
						//debug($oldDate ."<". $currentDate);
					}
					else{
						$openingBalance = $contents1["opening_balance"];
						$closingBalance = $openingBalance;								
					}
					$openingBalanceSwap = $openingBalance;
					$preDate = $currDate;
				}

			}
		}
		//$BeneficiryID = $contentsBen[$s]["benID"];
		$currSending = $contentsAcc[$s]["currency"];
		$authoriseDate = ($exportType!="csv" ? date("F j, Y", strtotime($contentsAcc[$s]["dated"])):date("F j Y", strtotime($contentsAcc[$s]["dated"])));
		if(CONFIG_VIEW_CURRENCY_LABLES == "1")
			$currencyLable = $contentsAcc[$s]["currency"];
		else
			$currencyLable = "";
		$moneyIn="";
		$moneyOut="";
		if($contentsAcc[$s]["type"] == "DEPOSIT")
		{
			$Balance = $Balance+$contentsAcc[$s]["amount"];
			$moneyIn = $contentsAcc[$s]["amount"];
		}
		if($contentsAcc[$s]["type"] == "WITHDRAW")
		{
			$Balance = $Balance-$contentsAcc[$s]["amount"];
			$moneyOut = $contentsAcc[$s]["amount"];
		}
		if(strstr($contentsAcc[$s]["description"], "by Teller"))
		{
			$q=selectFrom("SELECT * FROM ".TBL_TELLER." WHERE tellerID ='".$contentsAcc[$s]["modified_by"]."'"); 
			
		}
		else{
			$q=selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." WHERE userID ='".$contentsAcc[$s]["modified_by"]."'"); 
		}
		$trans = selectFrom("SELECT refNumberIM FROM ".TBL_TRANSACTIONS." WHERE transID ='".$contentsAcc[$s]["TransID"]."'"); 
		if($exportType == "csv")
			$strFullHtml .= "";
		else
			$strFullHtml .= "<tr class='".$contentsAcc[$s]["status"]."'>";

// Data Rows Starts
		$strFullHtml .= $strColumnStart.$authoriseDate.$strColumnEnd;
		if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
		{
			$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($openingBalance,2,'.',','):number_format($openingBalance,2,'.','')).$strColumnEnd;
		}
		$strFullHtml .= $strColumnStart.$q["name"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.($moneyIn !="" ? $moneyIn." ".$currencyLable:"").$strColumnEnd;
		$strFullHtml .= $strColumnStart.($moneyOut !="" ? $moneyOut." ".$currencyLable:"").$strColumnEnd;
		$strFullHtml .= $strColumnStart.$trans["refNumberIM"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$contentsAcc[$s]["description"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$contentsAcc[$s]["note"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$contentsAcc[$s]["status"].$strColumnEnd;
		if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
		{
			if($contentsAcc[$s]["type"] == "WITHDRAW")
				$closingBalance =  $closingBalance - $contentsAcc[$s]["amount"];
			elseif($contentsAcc[$s]["type"] == "DEPOSIT")
				$closingBalance =  $closingBalance + $contentsAcc[$s]["amount"];

			$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($closingBalance,2,'.',','):number_format($closingBalance,2,'.','')).$strColumnEnd;
		}
		$strFullHtml .= $strRowEnd;
// Data Row Ends
	}

// Breakline Starts
	$strFullHtml .= $strRowStart;
	if(CONFIG_AGENT_STATEMENT_BALANCE == '1') {
		$strFullHtml .= $strColumnStart."".$strColumnEnd;
		$strFullHtml .= $strColumnStart."".$strColumnEnd;
	}
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strRowEnd;
// Breakline Ends

// Total Runnig Row starts
	if(defined("CONFIG_DISPLAY_TOTAL_RUNNING_AnD_STATEMENT") && CONFIG_DISPLAY_TOTAL_RUNNING_AnD_STATEMENT=="1") 
	{
	$strFullHtml .= $strRowStart;
		if(CONFIG_AGENT_STATEMENT_BALANCE == '1') {
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
		}
		$strFullHtml .= $strColumnStart."".$strColumnEnd;
		$strFullHtml .= $strColumnStart."".$strColumnEnd;
		$strFullHtml .= $strColumnStart."".$strColumnEnd;
		$strFullHtml .= $strColumnStart."Total Balance:".$strColumnEnd;
		$strFullHtml .= $strColumnStart."".$strColumnEnd;
		$strFullHtml .= $strColumnStart."".$strColumnEnd;
		$strFullHtml .= $strColumnStart."Total Balance:".$strColumnEnd;
		$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($Balance,2,'.',','):number_format($Balance,2,'.',''))." ".$currSending.$strColumnEnd;
		$strFullHtml .= $strRowEnd;
	}
// Total Runnig Row starts
	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlClosing;

	echo $strFullHtml;
?>