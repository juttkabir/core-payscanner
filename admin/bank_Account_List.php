<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("javaScript.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];
$agentID = $_SESSION["loggedUserData"]["userID"];
$currentDate= date('Y-m-d',strtotime($date_time));
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$parentID = $_SESSION["loggedUserData"]["userID"];

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");


////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPI" && $agentType != "SUPAI")
		{
			$condition = True;	
		}
	}else{
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager")
			{
				$condition = True;
			}
		
		}
		
//////////////////////////Making of Condition///////////////

$fromDatevalue=$_POST["fromDate"];
		$toDatevalue=$_POST["toDate"];
		
	if(!empty($fromDatevalue)){
		$fromDate = str_replace('/', '-', $fromDatevalue);
		$fromDate= date("Y-m-d",strtotime($fromDate));
	}
	else
		$fromDate=$currentDate;
	if(!empty($toDatevalue)){
		$toDate = str_replace('/', '-', $toDatevalue);
		$toDate= date("Y-m-d",strtotime($toDate));
	}
	else
		$toDate=$currentDate;
		
//		 $fromDateReport=date("m-d-Y",strtotime($fromDate));
//  $toDateReport=date("m-d-Y",strtotime($toDate));

$fromDateReport=$fromDate;
$toDateReport=$toDate;

if($_POST["Submit"]=="Search")
{
	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";		
		
	/*$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];
	*/
	
		$_SESSION["type"]=$_POST["type"];
	
	
	if ($_SESSION["type"]== 'Deposited'){
		
		$transData = " and TransID = ''";
		$transtype='DEPOSIT';
		
		//$transtype='Manually Deposited';
		//$transtype_second='Barclays Payment';		 	
	}
	else if ($_SESSION["type"]== 'Refund'){
		
		$transData = "  and TransID != ''";
		$transtype='DEPOSIT';
	//	$transtype='Transaction Cancelled';
		//$transtype_second='Transaction Rejected';	
	}
	else {
		$transtype='WITHDRAW';
		//$transtype_second='Transaction Amended';	
		}	
	
}

if ($_POST['destcurrency'] != "") {
	$destcurrency = $_POST['destcurrency'] ;	
} else if ($_GET['destcurrency'] != "") {
	$destcurrency = $_GET['destcurrency'] ;	
}

//session_register["agentID2"];
$_SESSION["agentID2"] = "";
if($_POST["agentID"]!=""){
	
	$_SESSION["agentID2"] = $_POST["agentID"];
	
}elseif($_GET["agentID"]!=""){
	
	$_SESSION["agentID2"]=$_GET["agentID"];
	
}elseif($agentType == 'SUPI'){
	
	$_SESSION["agentID2"] = $changedBy;
	
	
	}
		

if ($offset == "")
	$offset = 0;
$limit=50;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
?>
<html>
<head>
	<title>Agent Account</title>
<script>
var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function checkForm(Search) {

if(Search.agentID.options.selectedIndex == 0){
    	alert("Please select Distributor from the list.");
        Search.agentID.focus();
        return false;
    }
}


function delete_confirm(id)
{
  if(confirm("Are you sure you want to delete this item?"))
    return true;
  else
    return false;
}
</SCRIPT>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	<tr>
      <td width="563">
      	<table width="100%" border="0" cellspacing="1">
      		<tr>
            <td class="topbar"><strong><font color="#000000" size="2">Distributor Statement </font></strong></td>
           </tr>
      		<tr>
      			<td colspan="2">
			<form action="bank_Account_List.php" method="post" name="Search" onSubmit="return checkForm(this);">
        <div align="center">
	 From Date 
                  	<input name="fromDate" type="text" id="from" value="<?=$fromDateReport?>" readonly >
                  	<!--input name="from" type="text" id="from" value="<?=$from1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('Search.fromDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" title="Select Date From|Please select date from calendar"><img src="images/show-calendar.gif" width=24 height=15 border=0> </a>&nbsp; &nbsp;To Date&nbsp;
		    		    <input name="toDate" type="text" id="to" value="<?=$toDateReport?>" readonly >
		    		    <!--input name="to" type="text" id="to" value="<?=$to1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('Search.toDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" title="Select Date To|Please select date from calendar"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
		    		    <!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->		    		    
              
        </td>
            </tr>
            <tr>
            	<td align="center">
            		
       <?    
       if($condition)//101
			  {
			  ?>
			  Select Distributor 
			  <select name="agentID" style="font-family:verdana; font-size: 11px"  <? if (DEST_CURR_IN_ACC_STMNTS == "1") { ?>onchange="document.Search.submit();"<? } ?>>
				<option value="">- Select Bank -</option>
				<? if($agentType != "SUPI Manager" && DEST_CURR_IN_ACC_STMNTS != "1") {?>
				<option value="All">- All Banks -</option>
				<?
		    	}
				
			//work by Mudassar Ticket #11425
				if ($agentType == "SUPA" || $agentType == "SUBA"){
					$selected=selectMultiRecords("select linked_Distributor from admin where userID='$parentID' or  parentID = '$parentID'");
					$row=$selected[0];
					$array=explode(", ", $row["linked_Distributor"]);
					$userID="";
					for($i=0;$i<count($array);$i++){
						$row=$array[$i];
						if ($i==count($array)-1)
							$userID.="'$row'";
						else
							$userID.="'$row', ";
					}
					$agentQuery = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where userID in ($userID)";
				} else if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){
					$agentQuery = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where 1 and parentID > 0 and adminType='Agent' and isCorrespondent != 'N' AND agentStatus = 'Active' ";
			} else if (CONFIG_AGENT_WITH_ALL_STATUS==1){
					$agentQuery = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where 1 and parentID > 0 and adminType='Agent' and isCorrespondent != 'N' ";
				}
//work end by Mudassar Ticket #11425	

				if(CONFIG_DISTRIBUTOR_ASSOCIATED_MANAGEMENT == '1' && $agentType == "SUPI Manager" )
				{
							
											$linkedDistQuery = selectFrom("select linked_Distibutor from admin where userID = '".$agentID."'");
											
											$linkedDistArray = explode("," , trim($linkedDistQuery["linked_Distibutor"],","));
			
		
								for($i = 0; $i < count($linkedDistArray); $i++)
								{
   
   
					$distIDArrayQuery = selectFrom("select userID,agentCompany,username from admin where 1 and username = '".$linkedDistArray[$i]."'");
						$distIDArray[$i] = $distIDArrayQuery["userID"];
						$distIDArray[$i] = "'".$distIDArray[$i]."'";
								}
  
					$linkedDist = implode("," , $distIDArray );
									
					$agentQuery .=" and userID IN (".$linkedDist." )";
			
		
	
			}
				
				
				
						$agentQuery .=" order by agentCompany";
				
						$agents = selectMultiRecords($agentQuery);
						
						for ($i=0; $i < count($agents); $i++){
					?>
				<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
				<?
						}
					?>
			  </select>
			  <script language="JavaScript">
				SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID2"]; ?>");
									</script>
		<? }?>
		   
               </td>
            </tr>
           <tr>
            	<td align="center">
            		Select Type 
                 
                 <select name="type" style="font-family:verdana; font-size: 11px">
                  <option value="">- Select Type-</option>
                   <option value="Deposited">Deposited</option>
                   <option value="Refund">Refund</option>
                   <option value="Withdraw">Withdraw</option>                  
                </select>
                  <script language="JavaScript">
         	SelectOption(document.forms[0].type, "<?=$_SESSION["type"]; ?>");
                                </script>
                  </td>
            </tr>
            
            
            <tr><td align="center">
            	
            	
          <?	if (DEST_CURR_IN_ACC_STMNTS == "1" && $_SESSION["agentID2"] != "") {  ?>
					
					Currency
					<? 
					if ($_SESSION["agentID2"] != "") {
										
						
						if(CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == '1'){
							 $settlementCurr   = selectFrom("select userID,settlementCurrency from ".TBL_ADMIN_USERS." where userID = '".$_SESSION["agentID2"]."' ");	
              
						}else{
							
							 //$query = "select distinct(currency) from exchangerate where rateFor = 'settlement' and rateValue = '".$_SESSION["agentID2"]."'";
						   	$queryCurrency1 = "SELECT DISTINCT(c.currency) FROM ".TBL_COUNTRY." AS c , ".TBL_SERVICE." AS s 
																  WHERE c.countryId = s.toCountryId " ;
						   $queryCurrency = selectMultiRecords($queryCurrency1);
						}
						$distIDACountries = selectFrom("SELECT `IDAcountry` FROM ".TBL_ADMIN_USERS." WHERE `userID` = '".$_SESSION["agentID2"]."'") ;						
						$arrDistCountries = explode(",", $distIDACountries['IDAcountry']);
						$strCountries = implode ("','", $arrDistCountries);
					 $query = "select DISTINCT(s.currencyRec) from countries as c, services as s where c.countryId = s.toCountryId and c.countryName IN ('".$strCountries."') ";
					  $queryCurrency = selectMultiRecords($query);
					  
						$strCurrency = "";
						for($j = 0; $j < count($queryCurrency); $j++)
						{
							if($j > 0)
								
								$strCurrency .= ", ";
								$strCurrency .= trim($queryCurrency[$j]["currencyRec"]);
						}
														
					$arrCurrency = explode (", ",$strCurrency);
					$uniqueCurrencyList1 = array_unique($arrCurrency);	
					
				 foreach($uniqueCurrencyList1 as $value)
					{
					     $uniqueCurrencyList[]=$value;
				  }
				
				  
					} else {

						$queryCurrency = "SELECT DISTINCT(c.currency) FROM ".TBL_COUNTRY." AS c , ".TBL_SERVICE." AS s 
																WHERE c.countryId = s.toCountryId " ;
					} ?>
					
					
					
					<select name="destcurrency" style="font-family:verdana; font-size: 11px; width:130">
			    
			     <?  if(CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == '1'){ ?>
			      
							 <option value="<?=trim($settlementCurr["settlementCurrency"])?>"><?=trim($settlementCurr["settlementCurrency"])?></option>	
							
			     	<?  
			      }else{
						
						for($k = 0; $k < count($uniqueCurrencyList); $k++)
						{?>
							 <option value="<?=trim($uniqueCurrencyList[$k])?>"><?=trim($uniqueCurrencyList[$k])?></option>	
						<? 
						}
					}	
						
				?>
		</select>
		<? 
			//echo $arrDistCountries[4];
	
			//echo $currencyData[1];
		
		?>
		<script language="JavaScript">SelectOption(document.forms[0].destcurrency, "<?=$destcurrency?>");
			</script>
		
		&nbsp;&nbsp;
		<?	}  ?>
            	
            	
            	
            	</td></tr>
            
            
            <tr>
            	<td  width="563" align="center"> 		
          <input type="submit" name="Submit" value="Search">
        </div>
      </form> 
		</td>
            </tr>
        </table>
		<tr>
		<td>
		
    
      <?
	 if($_POST["Submit"]=="Search"||$_GET["search"]=="search")
	 {
	 	$Balance="";
		if($_GET["agentID"]!="")
			$slctID=$_GET["agentID"];
		elseif($_GET["agentID"]=="") 
			{
				$slctID=$_POST["agentID"];
				$_SESSION["grandTotal"]="";
				$_SESSION["CurrentTotal"]="";
			}
			
			//$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
			//$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
			$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
			
		//	if($slctID!='')
				{
									
					if($condition)//101
					{
						if($slctID!='')
						{
							$accountQuery="Select * from bank_account where 1";
							$accountQueryCnt = "Select count(*) from bank_account where 1";
							if($slctID!='All')
							{
								$accountQuery .= " and bankID = $slctID";
								$accountQueryCnt .= " and bankID = $slctID";
							}
							$accountQuery .= " and $queryDate";	
							$accountQueryCnt .= " and $queryDate";	
							if ($_SESSION["type"]!=""){
					if ($_SESSION["type"]=="Withdraw"){
					$accountQuery .= " and type like '$transtype'";
					$accountQueryCnt .= " and type like '$transtype'";
					}
					else{
				 $accountQuery .= $transData;
				 $accountQuery .= " and type like '$transtype'";
				 
				 $accountQueryCnt .= $transData;
				 $accountQueryCnt .= " and type like '$transtype'";
				}
			}
							//$contentsAcc = selectMultiRecords($accountQuery);
							
					if ($destcurrency != "" && DEST_CURR_IN_ACC_STMNTS == "1") {
								$accountQuery .= " and (currency = '$destcurrency' OR currency = '')";
								$accountQueryCnt .= " and (currency = '$destcurrency' OR currency = '')";
							}
					$allCount = countRecords($accountQueryCnt);
					//echo $queryBen;
					$accountQuery .=  "  order by dated DESC ";
					$strSqlForExport = $accountQuery;
					$accountQuery .= " LIMIT $offset , $limit";
					$contentsAcc = selectMultiRecords($accountQuery);
					
						}
					
							 
					}else
					{
						
						$accountQuery = "Select * from bank_account where 1";
						$accountQuery .= " and bankID = $changedBy";
						$accountQuery .= " and $queryDate";
					if ($destcurrency != "" && DEST_CURR_IN_ACC_STMNTS == "1") {
						$accountQuery .= " and (currency = '$destcurrency' OR currency = '')";
						}
						
						$accountQueryCnt = "Select count(*) from bank_account where 1";
						$accountQueryCnt .= " and bankID = $changedBy";
						$accountQueryCnt .= " and $queryDate";
				if ($destcurrency != "" && DEST_CURR_IN_ACC_STMNTS == "1") {
						$accountQueryCnt .= " and (currency = '$destcurrency' OR currency = '')";
						}
							
						if ($_SESSION["type"]!=""){
					if ($_SESSION["type"]=="Withdraw"){
					$accountQuery .= " and type like '$transtype'";
					
					$accountQueryCnt .= " and type like '$transtype'";
					}
					else{
				 $accountQuery .= $transData;
				 $accountQuery .= " and type like '$transtype'";
				 
				 $accountQueryCnt .= $transData;
				 $accountQueryCnt .= " and type like '$transtype'";
				}
			}
						
						//$contentsAcc = selectMultiRecords($accountQuery);
			
					$allCount = countRecords($accountQueryCnt);
					//echo $queryBen;
					$accountQuery .=  "  order by dated DESC ";
					$strSqlForExport = $accountQuery;
					$accountQuery .= " LIMIT $offset , $limit ";
						echo $accountQuery;
					$contentsAcc = selectMultiRecords($accountQuery);
					
					}
					//if( $_SESSION["tYear"]!="" && $fromDate < $toDate)
						//{
									
						//}
					$strSqlForExport = addslashes($strSqlForExport);
					//debug($strSqlForExport);
				}	
					
			
	?>
      <div align="center">
        <table width="758" height="278" border="1" cellpadding="0" bordercolor="#666666">
          <tr> 
            <td height="25" nowrap bgcolor="#6699CC"> <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
                <tr> 
                  <td align="left">
                    <?php if (count($contentsAcc) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                 <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=first";?>"><font color="#005b90">First</font></a> </td>
              <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=pre";?>"><font color="#005b90">Previous</font></a> </td>
               <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                   <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=next";?>"><font color="#005b90">Next</font></a>&nbsp; </td>
              <td width="50" align="right"><!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=last";?>"><font color="#005b90">Last</font></a-->&nbsp; </td>
              <?php } ?>
                </tr>
              </table></td>
          </tr>
          <?
		if(count($contentsAcc) > 0)
		{
			if ($_SESSION["type"]== 'Withdraw' || $_SESSION["type"]== 'Refund' ){
		?>
          <tr> 
            <td nowrap bgcolor="#EFEFEF"> <table width="100%" height="181" border="0" bordercolor="#EFEFEF">
               <tr bgcolor="#FFFFFF">
					  <td align="left" ><span class="style1">Date</span></td>
					   <td align="left" ><span class="style1"><?=$systemCode ?></span></td>
					  <td align="left" ><span class="style1"><?=$manualCode ?></span></td>		
					  	<? if(CONFIG_SHOW_ONLY_LOCAL_AMOUNT == "1" && ($agentType != 'SUPI' && $agentType != 'SUBI')){?>			 
					  <td align="left" ><span class="style1">Transaction Amount</span></td>
					  	<?}?>
						<td align="left" ><span class="style1">Foreign Amount</span></td>
						 <? if(CONFIG_SHOW_ONLY_LOCAL_AMOUNT == "1" && ($agentType != 'SUPI' && $agentType != 'SUBI')){?>
						<td align="left" ><span class="style1">Exchange Rate</span></td>						
						<? } ?>
						<td align="left" ><span class="style1">Description</span></td>
						
				</tr>
				
				<? for($i=0;$i < count($contentsAcc);$i++)
				{
				//$BeneficiryID = $contentsBen[$i]["benID"];
					?>
				<tr bgcolor="#FFFFFF">
					<td width="200" align="left"><strong><font color="#006699"><? 
					$authoriseDate = $contentsAcc[$i]["dated"];
					echo date("F j, Y", strtotime("$authoriseDate"));
					?></font></strong></td>					
					</td>
					<? if($contentsAcc[$i]["TransID"] > 0){
							
							$trans = selectFrom("SELECT refNumberIM,refNumber,transAmount,localAmount,exchangeRate,IMFee FROM ".TBL_TRANSACTIONS." WHERE transID ='".$contentsAcc[$i]["TransID"]."'"); 
							?>
				<td><a href="#" onClick="javascript:window.open('ida-trans-details.php?transID=<? echo $contentsAcc[$i]["TransID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $trans["refNumberIM"]; ?></font></strong></a></td>
				<? }else{?>
				<td>&nbsp;</td>
				<? }?>
				
				<td width="56">
					&nbsp;<? echo $trans["refNumber"]; ?>
				</td>
			<? if(CONFIG_SHOW_ONLY_LOCAL_AMOUNT == "1" && ($agentType != 'SUPI' && $agentType != 'SUBI')){?>
				<td width="56">
					&nbsp;<? echo customNumberFormat($trans["transAmount"]); ?>
				</td>
				<? } ?>
				<td width="56">
					&nbsp;<? echo customNumberFormat($trans["localAmount"]); ?>
				</td>
					 <? if(CONFIG_SHOW_ONLY_LOCAL_AMOUNT == "1" && ($agentType != 'SUPI' && $agentType != 'SUBI')){?>
				<td width="56">
					&nbsp;<? echo $trans["exchangeRate"]; ?>
				</td>				
				<? } ?>
									
					<!--td width="200" align="left">
					<? 
					if(strstr($contentsAcc[$i]["description"], "by Teller"))
					{
						$q=selectFrom("SELECT * FROM ".TBL_COLLECTION." WHERE cp_id ='".$contentsAcc[$i]["modified_by"]."'"); 
						
					}
					else
						$q=selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." WHERE userID ='".$contentsAcc[$i]["modified_by"]."'"); 
					echo $q["name"]; ?></td>
					<td width="150" align="left"><?  
					if($contentsAcc[$i]["type"] == "DEPOSIT")
					{
						echo customNumberFormat($contentsAcc[$i]["amount"]);
						$Balance = $Balance+$contentsAcc[$i]["amount"];
					}
					?></td-->
					<!--td align="left"><? 
					if($contentsAcc[$i]["type"] == "WITHDRAW")
					{
						echo customNumberFormat($contentsAcc[$i]["amount"]);
						$Balance = $Balance-$contentsAcc[$i]["amount"];
					}
					?><td-->
				<td width="56">
					&nbsp;<? echo $contentsAcc[$i]["description"]; ?>
				</td>
				
					</tr>
				<?
				}
				?>
				<tr bgcolor="#FFFFFF">
				   <td>&nbsp;</td>
				  <td>&nbsp;</td>
				   <td>&nbsp;</td>
				  <td align="center">&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
					 <td>&nbsp;</td>
					 
					 
				</tr>	
				
				
				
				
				
				
                <? /* for($i=0;$i<count($contentsAcc);$i++)
			{
			//$BeneficiryID = $contentsBen[$i]["benID"];
				?>
                <tr bgcolor="#FFFFFF"> 
                  <td width="230" align="left"><strong><font color="#006699">
                    <? 
			  	$authoriseDate = $contentsAcc[$i]["dated"];
			  	echo date("F j, Y", strtotime("$authoriseDate"));
			  	?>
                    </font></strong></td>
                  <td width="231" align="left">
                    <? 
                    if(strstr($contentsAcc[$i]["description"], "by Teller"))
										{
											$q=selectFrom("SELECT * FROM ".TBL_COLLECTION." WHERE cp_id ='".$contentsAcc[$i]["modified_by"]."'"); 
											
										}
										else
                    $q=selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." WHERE userID ='".$contentsAcc[$i]["modified_by"]."'"); echo $q["name"]; //echo $contentsAcc[$i]["modified_by"];  ?>
                  </td>
                  <td width="210" align="left">
                    <?  
			  	if($contentsAcc[$i]["type"] == "DEPOSIT")
			  	{
			  		echo $contentsAcc[$i]["amount"];
					$Balance = $Balance+$contentsAcc[$i]["amount"];
			  	}
			  	?>
                  </td>
                  <td align="left">
                    <? 
			  	if($contentsAcc[$i]["type"] == "WITHDRAW")
			  	{
			  		echo $contentsAcc[$i]["amount"];
					$Balance = $Balance-$contentsAcc[$i]["amount"];
			  	}
			  	?>
                  </td>
                </tr>
                <?
			}
			?>
                <tr bgcolor="#FFFFFF"> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td align="center">&nbsp;</td>
                </tr>
                <tr bgcolor="#FFFFFF"> 
                  <td colspan="3" align="left"><b>Page Total Amount</td>
                  <td align="left"><? echo  number_format($Balance,2,'.',','); ?></td>
                </tr>
                <tr>            		
                			<td colspan="8"> 
                <table>
                	<tr>
	                  <td class="style3" width="130"> Cumulative Total : </td>
	                  <td width="100"> 
                    <?
												if($_GET["total"]=="first")
												{
												$_SESSION["grandTotal"] = $Balance;
												$_SESSION["CurrentTotal"]=$Balance;
												}elseif($_GET["total"]=="pre"){
													$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
													$_SESSION["CurrentTotal"]=$Balance;
												}elseif($_GET["total"]=="next"){
													$_SESSION["grandTotal"] += $Balance;
													$_SESSION["CurrentTotal"]= $Balance;
												}else
													{
													$_SESSION["grandTotal"] = $Balance;
													$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
													}
												echo number_format($_SESSION["grandTotal"],2,'.',',');
												?>
												</td>
												<td >
														<? if ($_SESSION["grandTotal"] < 0 ){ ?>
																<font color="red"><strong><?=$company ?> has to pay to bank/distributor</strong></font>
																<? 
															}		
															if ($_SESSION["grandTotal"] > 0 )
															{		
															?>
															<strong><?=$company ?> Credit Balance  </strong>
															<? } */?>
													
													
												<!--/td>
                </tr-->
              </table>
                  </td>
                </tr>
                <tr bgcolor="#FFFFFF"> 
                  <td colspan="5"> 
                    <!--
/************************************************************************/

Here You will write your Comments or any Information

/************************************************************************/
-->
                  </td>
                </tr>
              </table>
              <?
			}
			else {
				?>
				
				 <tr> 
            <td nowrap bgcolor="#EFEFEF"> <table width="809" height="181" border="0" bordercolor="#EFEFEF">
                <tr bgcolor="#FFFFFF"> 
                  <td align="left" ><span class="style1">Date</span></td>
                  <td align="left"><span class="style1">Modified By</span></td>
                  <td align="left"><span class="style1">Money In</span></td>
                  <td width="120" align="left"><span class="style1">Money Out</span></td>
                </tr>
                <? for($i=0;$i<count($contentsAcc);$i++)
			{
			//$BeneficiryID = $contentsBen[$i]["benID"];
				?>
                <tr bgcolor="#FFFFFF"> 
                  <td width="230" align="left"><strong><font color="#006699">
                    <? 
			  	$authoriseDate = $contentsAcc[$i]["dated"];
			  	echo date("F j, Y", strtotime("$authoriseDate"));
			  	?>
                    </font></strong></td>
                  <td width="231" align="left">
                    <? 
                    if(strstr($contentsAcc[$i]["description"], "by Teller"))
										{
											$q=selectFrom("SELECT * FROM ".TBL_COLLECTION." WHERE cp_id ='".$contentsAcc[$i]["modified_by"]."'"); 
											
										}
										else
                    $q=selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." WHERE userID ='".$contentsAcc[$i]["modified_by"]."'"); echo $q["name"]; //echo $contentsAcc[$i]["modified_by"];  ?>
                  </td>
                  <td width="210" align="left">
                    <?  
			  	if($contentsAcc[$i]["type"] == "DEPOSIT")
			  	{
			  		if(DEST_CURR_IN_ACC_STMNTS =="1"){
			  			echo $contentsAcc[$i]["currency"]." ";
			  			}
			  		echo customNumberFormat($contentsAcc[$i]["amount"]);
					$Balance = $Balance+$contentsAcc[$i]["amount"];
			  	}
			  	?>
                  </td>
                  <td align="left">
                    <? 
			  	if($contentsAcc[$i]["type"] == "WITHDRAW")
			  	{
			  		if(DEST_CURR_IN_ACC_STMNTS =="1"){
			  			echo $contentsAcc[$i]["currency"]." ";
			  			}
			  		echo customNumberFormat($contentsAcc[$i]["amount"]);
					$Balance = $Balance-$contentsAcc[$i]["amount"];
			  	}
			  	?>
                  </td>
                </tr>
                <?
			}
			?>
                <tr bgcolor="#FFFFFF"> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td align="center">&nbsp;</td>
                </tr>
                <tr bgcolor="#FFFFFF"> 
                  <td colspan="3" align="left"><b>Page Total Amount</td>
                  <td align="left"><? echo  number_format($Balance,2,'.',','); ?></td>
                </tr>
                <tr>            		
                			<td colspan="8"> 
                <table>
                	<tr>
	                  <td class="style3" width="130"> Cumulative Total : </td>
	                  <td width="100"> 
                    <?
												if($_GET["total"]=="first")
												{
												$_SESSION["grandTotal"] = $Balance;
												$_SESSION["CurrentTotal"]=$Balance;
												}elseif($_GET["total"]=="pre"){
													$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
													$_SESSION["CurrentTotal"]=$Balance;
												}elseif($_GET["total"]=="next"){
													$_SESSION["grandTotal"] += $Balance;
													$_SESSION["CurrentTotal"]= $Balance;
												}else
													{
													$_SESSION["grandTotal"] = $Balance;
													$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
													}
												echo number_format($_SESSION["grandTotal"],2,'.',',');
												?>
												</td>
												<td >
														<? if ($_SESSION["grandTotal"] < 0 ){ ?>
																<font color="red"><strong><?=$company ?> has to pay to bank/distributor</strong></font>
																<? 
															}		
															if ($_SESSION["grandTotal"] > 0 )
															{		
															?>
															<strong><?=$company ?> Credit Balance  </strong>
															<? } ?>
													
													
												</td>
                </tr>
              </table>
                  </td>
                </tr>
                <tr bgcolor="#FFFFFF"> 
                  <td colspan="5"> 
                    <!--
/************************************************************************/

Here You will write your Comments or any Information

/************************************************************************/
-->
                  </td>
                </tr>
				<? if(CONFIG_EXPORT_DISTRIBUTOR_STATEMENT_REPORT == 1) { ?>
				<tr bgcolor="#FFFFFF"> 
                  <td colspan="5" align="center">
				  	<form action="exportDistriburtorStatementReport.php" method="post" target="_blank">
				  		<input type="hidden" name="da" value="<?=$strSqlForExport?>" />
                        <input type="hidden" name="pageUrl" value="<?=$_SERVER['PHP_SELF']?>" />
						<input type="submit" value="Export Transactions" />
					</form>
				  </td>
                </tr>
				<? } ?>
              </table>
				
				
				
				
				<? }
			
			
			
			
			} // greater than zero
			else
			{
			?>
          <tr> 
            <td nowrap bgcolor="#EFEFEF"> <table width="810" border="0" bordercolor="#EFEFEF">
                 <tr bgcolor="#FFFFFF">
					  <td align="left" ><span class="style1">Date</span></td>
					   <td align="left" ><span class="style1"><?=$systemCode ?></span></td>
					  <td align="left" ><span class="style1"><?=$manualCode ?></span></td>	
					  	 <? if(CONFIG_SHOW_ONLY_LOCAL_AMOUNT == "1" && ($agentType != 'SUPI' && $agentType != 'SUBI')){?>				 
					  <td align="left" ><span class="style1">Local Amount</span></td>
					  <? } ?>
					  	 
						<td align="left" ><span class="style1">Foreign Amount</span></td>
						
							 <? if(CONFIG_SHOW_ONLY_LOCAL_AMOUNT == "1" && ($agentType != 'SUPI' && $agentType != 'SUBI')){?>
						<td align="left" ><span class="style1">Exchange Rate</span></td>						
						<? } ?>
						<td align="left" ><span class="style1">Description</span></td>
						<td align="left" ><span class="style1">Note</span></td>
						<td align="left" ><span class="style1">Status</span></td>
				</tr>
                <tr bgcolor="#FFFFFF"> 
                  <td colspan="9">&nbsp;</td>
                </tr>
			  </table>
			  </td>
          </tr>
          <?
			}if($slctID!='')
				{
					if($agentType == "admin" || $agentType == "Admin Manager")//101
					{
						?>
					  <tr> 
						<td bgcolor="#c0c0c0"><span class="child"><a HREF="add_Bank_Account.php?agent_Account=<?=$_SESSION["agentID2"];?>&modifiedBy=<?=$_SESSION["loggedUserData"]["userID"];?>" target="mainFrame" class="tblItem style4"><font color="#990000">Deposit 
						  Money</font></a></span></td>
					  </tr>
					  <? 
				  	}
				 }
				?>
        </table>
        <?	
	 }///End of If Search
	 ?>
      </div></td>
	</tr>
	
</table>
</body>
</html>