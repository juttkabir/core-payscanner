<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();

$_SESSION["adminType"]=$_POST["adminType"];

if ($_GET["act"] == "addAdmin") {
	$_SESSION["loginName"] = "";
	$_SESSION["password1"] = "";
	$_SESSION["password2"] = "";
	$_SESSION["name"] = "";
	$_SESSION["rights"] = "";
	$_SESSION["email"] = "";
	$_SESSION["adminType"] = "";
	$_SESSION["agentStatus"] = "";
	$_SESSION["custCountries"] = "";
	$_SESSION["IDAcountry"] = "";
	$_SESSION["accessFromIP"] = "";	
	$_SESSION["linkedAgent"] ="";
}
?>
<html>
<head>
	<title>Add Admin Staff</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
	
	function showDetails(str) {
	
		if (str == 'show') {
			
			
			document.getElementById('countryToSendMoney').style.display = '';
			document.getElementById('enable/disable').style.display = 'none';
			document.getElementById('createTransaction').style.display = '';
			document.getElementById('backTransaction').style.display = '';
		} else {
			document.getElementById('createAgent').style.display = 'none';
			document.getElementById('countryToSendMoney').style.display = 'none';
			document.getElementById('enable/disable').style.display = '';
			document.getElementById('createTransaction').style.display = 'none';
			document.getElementById('backTransaction').style.display = 'none';
			
		}
	}
	

		



function getUserType(adminType) {
	
   // returns the value of the selected radio button or "" if no button is selected
     if (adminType[0]) {
					
 // if the button group is an array (one button is not an array)
      for (var i=0; i<adminType.length; i++) {
         if (adminType[i].checked) {
           var j = i;
           var UserID = adminType[i].value;
           
         }
      }
     
   } else {
      if (adminType.checked) { return 0; } // if the one button is checked, return zero
   }
   
   
  

		
		
		if(UserID =='MLRO'){
			
			document.addAdmin.adminRights1.disabled=true;
			document.addAdmin.adminRights2.disabled=true;			
			document.addAdmin.adminRights7.disabled=true;
			document.addAdmin.adminRights3.disabled=true;
			
			
			
			
			}
			if(UserID =='Admin'){
		
			document.addAdmin.adminRights3.disabled=true;
			document.addAdmin.adminRights5.disabled=false;
			
			document.addAdmin.adminRights1.disabled=false;
			document.addAdmin.adminRights2.disabled=false;		
			
			document.addAdmin.adminRights6.disabled=false;
						
			}
		
			if(UserID =='Admin Manager'){
	
			document.addAdmin.adminRights0.disabled=true;
			document.addAdmin.adminRights1.disabled=true;
			document.addAdmin.adminRights2.disabled=true;
			document.addAdmin.adminRights3.disabled=true;
			document.addAdmin.adminRights5.disabled=true;
			
	
			document.addAdmin.adminRights6.disabled=false;
						
			}
			
			if(UserID =='SUPI Manager'){
			
			document.addAdmin.adminRights0.disabled=false;
			document.addAdmin.adminRights1.disabled=false;
			document.addAdmin.adminRights2.disabled=false;
			document.addAdmin.adminRights3.disabled=false;
		
			document.addAdmin.adminRights5.disabled=false;
			document.addAdmin.adminRights6.disabled=false;
			
			
						
			}
			
			if(UserID =='Collector'){
			
			document.addAdmin.adminRights0.disabled=false;
			document.addAdmin.adminRights1.disabled=false;
			document.addAdmin.adminRights2.disabled=false;
			document.addAdmin.adminRights3.disabled=false;
			
			document.addAdmin.adminRights5.disabled=false;
			document.addAdmin.adminRights6.disabled=false;
						
			}
			
			if(UserID =='Support'){
		
			document.addAdmin.adminRights5.disabled=false;				
			document.addAdmin.adminRights0.disabled=false;
			document.addAdmin.adminRights1.disabled=false;
			document.addAdmin.adminRights2.disabled=false;
			document.addAdmin.adminRights3.disabled=false;
			
			document.addAdmin.adminRights6.disabled=false;
			
		
			
			
			}
			
			if(UserID =='Call'){
			
			
			document.addAdmin.adminRights5.disabled=false;
			
			document.addAdmin.adminRights0.disabled=false;
			document.addAdmin.adminRights1.disabled=false;
			document.addAdmin.adminRights2.disabled=false;
			document.addAdmin.adminRights3.disabled=false;
		
			document.addAdmin.adminRights6.disabled=false;
			
						
			}
				if(UserID =='Branch Manager'){
			
			document.addAdmin.adminRights0.disabled=true;
			document.addAdmin.adminRights1.disabled=true;
			document.addAdmin.adminRights2.disabled=true;
			document.addAdmin.adminRights3.disabled=true;
			document.addAdmin.adminRights5.disabled=true;
						
			}
				
return UserID;
}


function distManager(show) {
	

	if(show == 'Y'){
		
		document.addAdmin.linkedDistributor.disabled= false;
		
		}if(show == 'N'){
			
			
			document.addAdmin.linkedDistributor.disabled= true;
			
			}
	
		
	
	}

	function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
	var flag = 0;
	if(theForm.loginName.value == "" || IsAllSpaces(theForm.loginName.value)){
    	alert("Please provide login name for the admin.");
        theForm.loginName.focus();
        return false;
    }
	if(theForm.password1.value == "" || IsAllSpaces(theForm.password1.value)){
    	alert("Please provide the admin's password.");
        theForm.password1.focus();
        return false;
    }
	if(theForm.password2.value == "" || IsAllSpaces(theForm.password2.value)){
    	alert("Please provide the admin's confirm password.");
        theForm.password2.focus();
        return false;
    }
	if(theForm.password2.value != theForm.password1.value){
    	alert("Both password fields must poses the same value.");
        theForm.password1.focus();
        return false;
    }
  if(theForm.name.value == "" || IsAllSpaces(theForm.name.value)){
    	alert("Please provide Full Name.");
        theForm.name.focus();
        return false;
    }
	/*if(!theForm.elements[4].checked && !theForm.elements[5].checked && !theForm.elements[6].checked && !theForm.elements[7].checked && !theForm.elements[8].checked && !theForm.elements[9].checked){
    	if(theForm.adminType[3].checked != true)
		{
			alert("Please assign atleast one right to the admin.");
    	    theForm.elements[4].focus();
        	return false;
		}
    }*/
	if(theForm.email.value == "" || IsAllSpaces(theForm.email.value)){
    	alert("Please provide the admin's email address.");
        theForm.email.focus();
        return false;
    }
    <? if(CONFIG_VERIFY_TRANSACTION_ENABLED) { ?>	
    for (j=0;j<6;j++)
    <? }else{ ?>
    for (j=0;j<5;j++)
    <? } ?>
    {
    	if(theForm.elements[j+12].checked == true)
    	{
    		flag = 1;
    	}
    }
    for (k=0;k<theForm.adminType.length;k++)
    {
    	if (theForm.adminType[k].checked)
    	{
    		adminTypeValue = 	theForm.adminType[k].value;
    	}	
    }
  /* if (flag == 0 && adminTypeValue == "Admin")
    {
    	<? if(CONFIG_BACK_DATED == '1'){ ?>
			alert("Please assign atleast one right to the User (whether you selected 'Backdated Transaction' or not).");
		<? }else{?>
    	alert("Please assign atleast one right to the User.");
    <? } ?>
        theForm.elements[12].focus();
        return false;
    }
     else {
	}*/
<?	if (CONFIG_CUST_COUNTRIES == "1") {  ?>
	if(theForm.custCountries.value == "" || IsAllSpaces(theForm.custCountries.value)){
  	alert("Please select atleast one country for customers.");
      theForm.custCountries.focus();
      return false;
  }
<?	}  ?>
	if(getUserType(document.addAdmin.adminType)!="MLRO" && (theForm.IDAcountry.value == "" || IsAllSpaces(theForm.IDAcountry.value ) )){
    	alert("Please provide the country to send money."+getUserType(document.addAdmin.adminType));
        theForm.IDAcountry.focus();
        return false;
    }
<?	if (CONFIG_BACK_DATED == '1' && $agentType == 'admin') {  ?>
	if (document.getElementById('backdated').checked == true) {
		if (document.getElementById('backDays').value != '') {
			if (document.getElementById('backDays').value == '0') {
				alert("Please provide back days other than zero");
				document.getElementById('backDays').focus();
				return false;
			}
			if (!isNumeric(document.getElementById('backDays').value)) {
				alert("Please provide the positive numeric back days");
				document.getElementById('backDays').focus();
				return false;
			}
		}
	}
<?	}  ?>
	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }


function isNumeric(strString) {
   //  check for valid numeric strings	
   
	var strValidChars = "0123456789";
	var strChar;
	var blnResult = true;
	
	if (strString.length == 0) {
		return false;
	}
	
	//  test strString consists of valid characters listed above
	for (i = 0; i < strString.length && blnResult == true; i++)	{
	  strChar = strString.charAt(i);
	  if (strValidChars.indexOf(strChar) == -1)	{
	     blnResult = false;
	  }
	}
	return blnResult;
}

function showBackDays() {
	if (document.getElementById('backdated').checked == true) {
		document.getElementById('howmanybackdays').style.visibility = 'visible';
		document.getElementById('howmanybackdays').style.position = 'relative';
	} else {
		document.getElementById('backDays').value = '';
		document.getElementById('howmanybackdays').style.visibility = 'hidden';
		document.getElementById('howmanybackdays').style.position = 'absolute';
	}
}
function checkFocus(){

	addAdmin.loginName.focus();
}

	// end of javascript 
	</script>
</head>
<body onLoad="checkFocus();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font class="topbar_tex">Add Admin Staff</font></strong></td>
  </tr>
  <form action="add-admin-conf.php" method="post" onSubmit="return checkForm(this);" name="addAdmin">
  <tr>
    <td align="center">
		<table width="480" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr> 
            <td colspan="2" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Add 
                    Admin Staff</strong></font></td>
                </tr>
              </table></td>
          </tr>
          <? if ($_GET["msg"] != ""){ ?>
          <tr bgcolor="#EEEEEE">
            <td colspan="2" bgcolor="#EEEEEE"><table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr>
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; ?></td>
                </tr>
              </table></td>
          </tr>
          <? } ?>
          <tr bgcolor="#ededed"> 
            <td height="19" colspan="2" align="center"><font color="#FF0000">* 
              Compulsory Fields</font></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Login Name<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="loginName" value="<?=stripslashes($_SESSION["loginName"]); ?>"  size="35" maxlength="32"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Password<font color="#ff0000">*</font></strong></font></td>
            <td><input type="password" name="password1" value="<?=stripslashes($_SESSION["password1"]); ?>" size="35" maxlength="16"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Confirm Password<font color="#ff0000">*</font></strong></font></td>
            <td><input type="password" name="password2" value="<?=stripslashes($_SESSION["password2"]); ?>" size="35" maxlength="16"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144" valign="top"><font color="#005b90"><b>Full Name<font color="#ff0000">*</font></b></font></td>
            <td><input type="text" name="name" value="<?=stripslashes($_SESSION["name"]); ?>" size="35" maxlength="255"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Email<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="email" value="<?=stripslashes($_SESSION["email"]); ?>" size="35" maxlength="255"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Admin Type</strong></font></td>
            <td><table width="100%"  border="0" cellspacing="0" cellpadding="3">
              <tr>
<? if (IS_BRANCH_MANAGER){ ?>
                <td><input type="radio" name="adminType" id="adminType" value="Branch Manager" <? if ($_SESSION["adminType"] == "Branch Manager") echo "checked"; ?> onClick="javascript:getUserType(document.addAdmin.adminType);showDetails('show');distManager('N');"></td>
                <td>Branch Manager </td>                
<? } else { ?>
                <td><input type="hidden" name="hiddenval">&nbsp;</td>
                <td>&nbsp;</td>
<? } ?>
              	<td><input type="radio" name="adminType" id="adminType" value="Collector" <? if ($_SESSION["adminType"] == "Collector") echo "checked"; ?> onClick="javascript:showDetails('show');getUserType(document.addAdmin.adminType);distManager('N');"></td>
                <td>Collector </td>                
              </tr>
              <tr>
                <td><input type="radio" name="adminType" id="adminType" value="Admin" <? if ($_SESSION["adminType"] == "Admin") echo "checked"; ?> onClick="javascript:showDetails('show');getUserType(document.addAdmin.adminType);distManager('Y');"></td>
                <td>
                	<?
						if(CONFIG_LABEL_BASIC_USER == '1')
						{
						echo(CONFIG_BASIC_USER_NAME);	
					}else{
						echo("Basic User");
						}                	
                	?> </td>
                <td><input type="radio" name="adminType" id="adminType" value="Call" <? if ($_SESSION["adminType"] == "Call") echo "checked"; ?> onClick="javascript:showDetails('show');getUserType(document.addAdmin.adminType);distManager('N');"></td>
                <td>Call center Staff </td>
              </tr>
              <tr>
                <td><input type="radio" name="adminType" id="adminType" value="Admin Manager" <? if ($_SESSION["adminType"] == "Admin Manager") {echo "checked"; } ?> onClick="javascript:showDetails('show');getUserType(document.addAdmin.adminType);distManager('N');"></td>
                <td>Admin Manager </td>                
                <td><input type="radio" name="adminType" id="adminType" value="Support" <? if ($_SESSION["adminType"] == "Support") {echo "checked"; } ?> onClick="javascript:showDetails('show');getUserType(document.addAdmin.adminType);distManager('N');"></td>
                <td>Support Staff </td>                
              </tr>
              <tr>
                <td><input type="radio" name="adminType" id="adminType" value="SUPI Manager" <? if ($_SESSION["adminType"] == "SUPI Manager") {echo "checked"; } ?> onClick="javascript:showDetails('show');getUserType(document.addAdmin.adminType);distManager('Y');"></td>
                <td>Distributor Managment </td>
                <? if(CONFIG_ADMIN_TYPE_MLRO == "1" && $agentType != "MLRO"){ ?>
                <td id="MLRO"><input type="radio" name="adminType" value="MLRO" id="adminType" <? if ($_SESSION["adminType"] == "MLRO") echo "checked"; ?> onClick="javascript:getUserType(document.addAdmin.adminType);showDetails('Hide');distManager('N');" ></td>
                <td>MLRO </td>  
                        <? } ?>
              </tr>
            </table>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144" valign="top"><font color="#005b90"><b>Rights<font color="#ff0000">*</font></b></font></td>
            <td><table width="100%" border="0">
				<tr>
					<td id="createAgent"><input name="adminRights0" id="adminRights0" type="checkbox" value="Create Agent" <? if (substr_count($_SESSION["rights"], "Create Agent")) echo "checked"; ?>> Create Agent</td>
					<td><input name="adminRights1" id="adminRights1" type="checkbox" value="Delete Agent" <? if (substr_count($_SESSION["rights"], "Delete Agent")) echo "checked"; ?>> Delete Agent</td>
				</tr>
				<tr>
					<td><input name="adminRights2" id="adminRights2" type="checkbox" value="Suspend Agent" <? if (substr_count($_SESSION["rights"], "Suspend Agent")) echo "checked"; ?>> Suspend Agent</td>
					<td id="createTransaction"><input name="adminRights5" id="adminRights5" type="checkbox" value="Create Transaction" <? if (substr_count($_SESSION["rights"], "Create Transaction")) echo "checked"; ?>> Create Transaction</td>
				</tr>
				<tr>
					<td><input name="adminRights3" id="adminRights3" type="checkbox" value="Authorise Transaction" <? if (substr_count($_SESSION["rights"], "Authorise Transaction")) echo "checked"; ?>> Authorise Transaction</td>
					<? if(CONFIG_VERIFY_TRANSACTION_ENABLED)
          {?>
					<td><input name="adminRights4" id="adminRights4" type="checkbox" value="Verify Transaction" <? if (substr_count($_SESSION["rights"], "Verify Transaction")) echo "checked"; ?>> Verify Transaction</td>
					<? 
					}else { ?>
					<td>&nbsp;</td>
					<? } ?>
					
				</tr>
				<? if(CONFIG_BACK_DATED == '1'){ ?>
				<tr>
					<td colspan="2" id="backTransaction"><input name="adminRights6" id="adminRights6" id="backdated" type="checkbox" value="Backdated" <? if (substr_count($_SESSION["rights"], "Backdated")) echo "checked"; ?> <? if ($agentType == 'admin') { ?>onClick="showBackDays();"<? } ?>> Backdated Transaction</td>
				</tr>
				<tr id="howmanybackdays" style="visibility:hidden;position:absolute;">
					<td colspan="2">How Many Back Days <input type="text" name="backDays" id="backDays" size="3"> <i>By default,</i> 1</td>
				</tr>
				<? } ?>
				<tr id="enable/disable">
					<td><input name="adminRights7" id="adminRights7" type="checkbox" value="Enable/Disable Sender" <? if (substr_count($_SESSION["rights"], "Enable/Disable Sender")) echo "checked"; ?>> Enable/Disable Sender</td>
				</tr>
				
			
			</table></td>
          </tr>
         
          <tr bgcolor="#ededed">
            <td><font color="#005b90"><strong>Status</strong></font></td>
            <td><input type="radio" name="agentStatus" value="New" <? if ($_SESSION["agentStatus"] == "" || $_SESSION["agentStatus"] == "New") echo "checked"; ?>>
              New
                <input type="radio" name="agentStatus" value="Active" <? if ($_SESSION["agentStatus"] == "Active") echo "checked"; ?>>
                Active
                <input type="radio" name="agentStatus" value="Disabled" <? if ($_SESSION["agentStatus"] == "Disabled") echo "checked"; ?>>
                Disabled
                <input type="radio" name="agentStatus" value="Suspended" <? if ($_SESSION["agentStatus"] == "Suspended") echo "checked"; ?>>
                Suspended</td>
          </tr>
      	<!-- Niaz -->
      	 <?	if (CONFIG_CUST_COUNTRIES == "1") {  ?>
         <tr bgcolor="#ededed"> 
           <td width="144"><font color="#005b90"><strong>Select Country for Customers</strong></font></td>
            <td>
								Hold Ctrl key for multiple selection<br>
              <SELECT name="custCountries[]" size="4" multiple id="custCountries" style="font-family:verdana; font-size: 11px" >
                <?
               if (CONFIG_CANADIAN_COUNTRY_OPTION == "1" && ($agentType == "SUPAI" || $agentType == "SUPA" || $agentType == "SUBAI" || $agentType == "SUBA"))
               {
               	$Agent_Country = selectFrom("select agentCountry from ".TBL_ADMIN_USERS." where userID = '".$_SESSION["loggedUserData"]["userID"]."'");
               	if ($Agent_Country["agentCountry"] == "Canada")
               	{
               	?>
               	<option value="Canada">Canada</option>
               	<?		
               	}
               	else
               	{
               		if(CONFIG_ENABLE_ORIGIN == "1"){
        	
            			$countryTypes = " and countryType like '%origin%' ";
            			$agentCountry = " ";
            	           	
            		}else{
            			$countryTypes = " ";
            			$agentCountry = " and countryName ='United Kingdom' ";
            		}
							$countires = selectMultiRecords("select countryName, countryId from ".TBL_COUNTRY." where 1 $countryTypes $agentCountry order by countryName");
					for ($i=0; $i < count($countires); $i++){
						?>
              <option value="<?=$countires[$i]["countryName"]; ?>" <? echo (strstr($_SESSION["custCountries"], $countires[$i]["countryName"]) ? "selected"  : "")?>>
              <?=$countires[$i]["countryName"]; ?>
              </option>
              <?
						}
               	}
              }
              else
              {
               if(CONFIG_ENABLE_ORIGIN == "1"){
        	
            
            		$countryTypes = " and countryType like '%origin%' ";
            		$agentCountry = " ";
            	           	
            }else{
            		$countryTypes = " ";
            		$agentCountry = " and countryName ='United Kingdom' ";
            	}
					$countires = selectMultiRecords("select countryName, countryId from ".TBL_COUNTRY." where 1 $countryTypes $agentCountry order by countryName");
					for ($i=0; $i < count($countires); $i++){
				?>
                      <option value="<?=$countires[$i]["countryName"]; ?>" <? echo (strstr($_SESSION["custCountries"], $countires[$i]["countryName"]) ? "selected"  : "")?>>
                      <? echo $countires[$i]["countryName"]; ?>
                      </option>
                      <?
					}
				}
				?>
              </SELECT>
                	</td>
          </tr>
        <?	}  ?>
        <? if ($_POST["adminType"]!= "MLRO") { ?>
          <tr bgcolor="#ededed" id="countryToSendMoney"> 
            <td width="144"><font color="#005b90"><strong>Select Country to Send money</strong></font></td>
            <td>
								Hold Ctrl key for multiple selection<br>
              <SELECT name="IDAcountry[]" size="4" multiple id="IDAcountry" style="font-family:verdana; font-size: 11px" >
                
                <?
                
                if(CONFIG_ENABLE_ORIGIN == "1"){
                	
                	
                		$countryTypes = " and countryType like '%destination%' ";
                	                                	
                	}else{
                		$countryTypes = " ";
                	
                	}
                if(CONFIG_COUNTRY_SERVICES_ENABLED){
	                
	                	
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where  countryId = toCountryId  $countryTypes order by countryName");
									
								}else
								{
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
									
								}
                
					for ($i=0; $i < count($countires); $i++){
				?>
                <OPTION value="<?=$countires[$i]["countryName"]; ?>" <? echo (strstr($_SESSION["IDAcountry"],$countires[$i]["countryName"]) ? "selected"  : "")?>> 
                <?=$countires[$i]["countryName"]; ?>
                </OPTION>
                <?
					}
				?>
              </SELECT>
                	</td>
          </tr>
          <? } ?>
        <?
        if(CONFIG_ADMIN_ASSOCIATE_AGENT == '1')
        {
        ?>  
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Select Associated Agent to Create Transaction</strong></font></td>
            <td>
				<?
				if ( CONFIG_VIEW_ASSOCIATED_AGENT_TRANS != 1 ) 
				{
					$selectBoxType = "size='4' multiple";
				?>

					Hold Ctrl key for multiple selection<br>
				<?
				}else
				{
					$selectBoxType = "";	
				}
				?>
              <SELECT name="linkedAgent[]" <?=$selectBoxType?> id="linkedAgent" style="font-family:verdana; font-size: 11px" >
                
                <?
                
                	$agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY'";
					
					if($agentType == "Branch Manager")
					{
						$agentQuery .= " and parentID = '$changedBy'";				
						
					}
					$agentQuery .= "order by agentCompany";
					$agents = selectMultiRecords($agentQuery);
                
					for ($i=0; $i < count($agents); $i++){
						if(!empty($agents[$i]["username"])){
				?>
              				<OPTION value="<?=$agents[$i]["username"]; ?>" <? echo (strstr($_SESSION["linkedAgent"],$agents[$i]["username"]) ? "selected"  : "")?>>
                			<? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?>
               
                			</OPTION>
                <?
						}
					}
				?>
              </SELECT>
                	</td>
          </tr>
          <?
        }
        ?>
         <? 
           if(CONFIG_DISTRIBUTOR_ASSOCIATED_MANAGEMENT == '1' )
        {
        ?>  
          <tr bgcolor="#ededed" id="DistributorList"> 
            <td width="144"><font color="#005b90"><strong>Select Associated Distributor </strong></font></td>
            <td>
				Hold Ctrl key for multiple selection<br>
              <SELECT name="linkedDistributor[]" size="4" multiple id="linkedDistributor" style="font-family:verdana; font-size: 11px" >
                
                <?
                
                	 $agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where 1 and parentID > 0 and adminType='Agent' and isCorrespondent = 'ONLY'";
					
					if($agentType == "Branch Manager")
					{
						$agentQuery .= " and parentID = '$changedBy'";				
						
					}
					$agentQuery .= "order by agentCompany";
					$agents = selectMultiRecords($agentQuery);
                
					for ($i=0; $i < count($agents); $i++){
				?>
                <OPTION value="<?=$agents[$i]["username"]; ?>" <? echo (strstr($_SESSION["linkedAgent"],$agents[$i]["username"]) ? "selected"  : "")?>>
                	<? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?>
               
                </OPTION>
                <?
					}
				?>
              </SELECT>
              
                	</td>
          </tr>
          <?
        } 
        ?>
         
          
        <!--Niaz-->
          
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Access From IP</strong></font></td>
            <td><input type="text" name="accessFromIP" id="accessFromIP" value="<?=stripslashes($_SESSION["accessFromIP"]); ?>" size="35" maxlength="255"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td colspan="2" align="center"> <input type="submit" value=" Save ">
              &nbsp;&nbsp; <input type="reset" value=" Clear "> </td>
          </tr>
        </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>
