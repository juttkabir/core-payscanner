<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
// include ("javaScript.php");
//echo $selectedCurrency = $_GET["currency"];
/**
 * The following few lines of code (http referer related) are just a 2 minutes trick
 * to track the user's state related to this script. The actual issue is that there's
 * no way used to maintain the flow level of the target logic and we can't assess the
 * event this script is initiated and when the logical flow ends.
 *
 * So, I am introducing this trick to save the time and don't want to interrupt the
 * original workings, HOWEVER THIS ISN'T A TOTAL FOOL-PROOF SOLUTION. And should be
 * more solid.
 *
 * These are just notes and should be removed when the target is achieved.
 */
$httpReferer = parse_url($_SERVER['HTTP_REFERER']);

if($httpReferer["path"] != "/admin/add-rate.php")
{
    /**
     * These variables need to reset or removed whenever this script is initiated (started from 0).
     */
    unset($_SESSION["defaultRecievingCurrency"], $_SESSION["defaultCurrencyOrigin"], $_SESSION["defaultCountryOrigin"], $_SESSION["defaultRecievingCountry"]);
}

$relatedRateId = ($_GET["relatedRateId"] != "" ? $_GET["relatedRateId"] : $_POST["relatedRateId"]);
$countryBasedFlag=false;
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
    $countryBasedFlag=true;
}
$agentType = getAgentType();
$loggedUserID = $_SESSION["loggedUserData"]["userID"];

$_SESSION["country"] = $_POST["country"];
$_SESSION["countryOrigin"] = $_POST["countryOrigin"];
$_SESSION["rateFor"] = $_POST["rateFor"];
$_SESSION["agentID"] = $_POST["agentID"];
$historicalID = $_GET["historicalID"];
$_SESSION["currency"] = $_POST["currency"];
//echo("session Currency".$_SESSION["currency"]."-Post Currency".$_POST["currency"]);
$_SESSION["currencyOrigin"] = $_POST["currencyOrigin"];
$_SESSION["intermediateCurrency"] = $_POST["intermediateCurrency"];
$_SESSION["intermediateExchangeRate"] = $_POST["intermediateExchangeRate"];
$_SESSION["intermediateDestExchangeRate"] = $_POST["intermediateDestExchangeRate"];
$_SESSION["agentID"] = $_POST["agentID"];

if(isset($_REQUEST["defaultRecievingCurrency"]))
    $_SESSION["defaultRecievingCurrency"] = $_REQUEST["defaultRecievingCurrency"];

if(isset($_REQUEST["defaultCurrencyOrigin"]))
    $_SESSION["defaultCurrencyOrigin"] = $_REQUEST["defaultCurrencyOrigin"];

if(isset($_REQUEST["defaultCountryOrigin"]))
    $_SESSION["defaultCountryOrigin"] = $_REQUEST["defaultCountryOrigin"];

if(isset($_REQUEST["defaultRecievingCountry"]))
    $_SESSION["defaultRecievingCountry"] = $_REQUEST["defaultRecievingCountry"];

if($_POST["marginType"] != '')
{
    $_SESSION["marginType"] = $_POST["marginType"];
}


if($_POST["agentMarginType"] != '')
{
    $_SESSION["agentMarginType"] = $_POST["agentMarginType"];
}

if($_POST["agentMarginValue"] != '')
{
    $_SESSION["agentMarginValue"] = $_POST["agentMarginValue"];
}

if ($_SESSION["exchangeDate"] != "")
{
    $dbDate = explode("-",$_SESSION["exchangeDate"]);
    if(count($dbDate) == 3)
    {
        $dbDate = $dbDate[2]."/".$dbDate[1]."/".$dbDate[0];
        $_SESSION["exchangeDate"] = $dbDate;
    }
}
if($_POST["_24hrMarginValue"] != "")
{
    $_SESSION["_24hrMarginValue"] = $_POST["_24hrMarginValue"];
}
if($_POST["_24hrMarginType"] != "")
{
    $_SESSION["_24hrMarginType"] = $_POST["_24hrMarginType"];
}

if ($_GET['message'] == "")
{
    $_SESSION["exchangeDate"] = ($_POST["dDate"] != "" ? $_POST["dDate"] : $_SESSION["exchangeDate"]);
}
else if ($_GET['message'] != "")
{
    $_SESSION["exchangeDate"] = "";
    $_SESSION["marginPercentage"] = "";
    $_SESSION["primaryExchange"] = "";
    $_SESSION["agentMarginValue"] = "";
    $_SESSION["_24hrMarginValue"] = "";
    $_SESSION["_24hrMarginType"] = "";
}


if($_SESSION["rateFor"] == "bank24hr" && !empty($_SESSION["currencyOrigin"]) && !empty($_SESSION["countryOrigin"]) && !empty($_SESSION["country"]) && !empty($_SESSION["currency"]))
{
    $dbSql = "select * from ".TBL_EXCHANGE_RATES." where country='".$_SESSION["country"]."' and currency='".$_SESSION["currency"]."' and countryOrigin='".$_SESSION["countryOrigin"]."' and currencyOrigin='".$_SESSION["currencyOrigin"]."' and rateFor=\"generic\" order by dated desc limit 1 ";
    $rs = mysql_query($dbSql);

    $dbSqlDataSet = selectFrom($dbSql);
    /**
     * $dbSqlDataSet = mysql_fetch_array($dbSql);
     * echo count($dbSqlDataSet);
     * debug($dbSqlDataSet);
     */

    if(mysql_num_rows($rs) < 1)
        echo "<script>alert('Please first enter generic value exchange rate against these conuntry and currency combination.');</script>";
}

/*
  This config added by Niaz Ahmad at 10-04-2009 @4658.
  get the updated upper exchange value from the system in case of rateFor is agent.
*/
if(CONFIG_AGENT_BASED_EXCHANGE_RATE_GENERIC_UPPER_VALUE == "1")
{
    if($_SESSION["rateFor"] == "agent" &&  !empty($_SESSION["currencyOrigin"]) && !empty($_SESSION["countryOrigin"]) && !empty($_SESSION["country"]) && !empty($_SESSION["currency"]))
    {
        $dbSql2 = "select * from ".TBL_EXCHANGE_RATES." where country='".$_SESSION["country"]."' and currency='".$_SESSION["currency"]."' and countryOrigin='".$_SESSION["countryOrigin"]."' and currencyOrigin='".$_SESSION["currencyOrigin"]."' and rateFor=\"generic\" and updationDate  = (select Max(updationDate ) from ".TBL_EXCHANGE_RATES." where country='".$_SESSION["country"]."' and currency='".$_SESSION["currency"]."' and countryOrigin='".$_SESSION["countryOrigin"]."' and currencyOrigin='".$_SESSION["currencyOrigin"]."' and rateFor=\"generic\") ";
        $rs = mysql_query($dbSql2);
        $dbSqlDataSet2 = selectFrom($dbSql2);
    }
}
?>
<html>
<head>
    <title>Add Exchange Rate</title>
    <script language="javascript" src="./javascript/functions.js"></script>
    <script language="javascript" src="javaScript.js"></script>

    <script type="text/javascript" src="jquery.js"></script>

    <link href="images/interface.css" rel="stylesheet" type="text/css">
    <script language="javascript" type="text/javascript">
        <!--
        function SelectOption(OptionListName, ListVal)
        {
            for (i=0; i < OptionListName.length; i++)
            {
                if (OptionListName.options[i].value == ListVal)
                {
                    OptionListName.selectedIndex = i;
                    break;
                }
            }
        }
        function checkForm(theForm) {
            <? if(CONFIG_ADD_SUB_AGENT_RATE != "1"){ ?>
            if(theForm.rateFor.value == 'agent' && theForm.agentID.value == '' ){
                alert("Please select Agent.");
                theForm.agentID.focus();
                return false;
            }
            <? } ?>

            if(theForm.rateFor.options.selectedIndex == 0){
                alert("Please select Exchange Rate Based On.");
                theForm.rateFor.focus();
                return false;
            }

            if(document.getElementById("rateFor").value == "distributor"){
                if(theForm.agentID.options.selectedIndex == 0)
                {
                    alert("Please select Distributor.");
                    theForm.agentID.focus();
                    return false;
                }
            }else if(document.getElementById("rateFor").value == "transType")
            {
                if(theForm.agentID.options.selectedIndex == 0)
                {
                    alert("Please select Transaction Type.");
                    theForm.agentID.focus();
                    return false;
                }
            }else if(document.getElementById("rateFor").value == "MoneyPaid")
            {
                if(theForm.agentID.options.selectedIndex == 0)
                {
                    alert("Please select Money Paid option from the list.");
                    theForm.agentID.focus();
                    return false;
                }
            }

            <? if(CONFIG_SETTLEMENT_COMMISSION == "1"){ ?>

            if(theForm.defaultCurrencyOrigin.options.selectedIndex == 0){
                alert("Please Select Default Originating Currency.");
                theForm.defaultCurrencyOrigin.focus();
                return false;
            }

            if(theForm.defaultCountryOrigin.options.selectedIndex == 0){
                alert("Please Select Default Originating Country.");
                theForm.defaultCountryOrigin.focus();
                return false;
            }

            if(theForm.defaultRecievingCurrency.options.selectedIndex == 0){
                alert("Please Select Default Recieving Currency.");
                theForm.defaultRecievingCurrency.focus();
                return false;
            }

            if(theForm.defaultRecievingCountry.options.selectedIndex == 0){
                alert("Please Select Default Recieving Country.");
                theForm.defaultRecievingCountry.focus();
                return false;
            }

            <? } ?>
            if(theForm.countryOrigin.options.selectedIndex == 0){
                alert("Please select the Originating Country.");
                theForm.countryOrigin.focus();
                return false;
            }

            if(theForm.country.options.selectedIndex == 0){
                alert("Please select the Destination Country.");
                theForm.country.focus();
                return false;
            }

            if(!$("#denominationBased").attr('checked'))
            {
                if(theForm.primaryExchange.value == "" || IsAllSpaces(theForm.primaryExchange.value))
                {
                    alert("Please provide primary exchnage rate.");
                    theForm.primaryExchange.focus();
                    return false;
                }
                if(theForm.marginPercentage.value == "" || IsAllSpaces(theForm.marginPercentage.value)){
                    alert("Please provide margin percentage.");
                    theForm.marginPercentage.focus();
                    return false;
                }
            }
            if(theForm.currency.options.selectedIndex == 0){
                alert("Please provide Currency Name.");
                theForm.currency.focus();
                return false;
            }
            if(theForm.rateFor.value == 'intermediate'){
                if(theForm.agentID.options.selectedIndex == 0){
                    alert("Please select distributor.");
                    theForm.agentID.focus();
                    return false;
                } }

            if(theForm.rateFor.value == 'intermediate'){
                if(theForm.intermediateExchangeRate.value == ''){
                    alert("Please provide primary intermediate exchange rate.");
                    theForm.intermediateExchangeRate.focus();
                    return false;
                } }

            if(theForm.rateFor.value == 'intermediate'){
                if(theForm.intermediateDestExchangeRate.value == ''){
                    alert("Please provide secondary intermediate exchange rate.");
                    theForm.intermediateDestExchangeRate.focus();
                    return false;
                } }

            if(theForm.rateFor.value == 'intermediate'){
                if(theForm.intermediateCurrency.options.selectedIndex == 0){
                    alert("Please select intermediate currency.");
                    theForm.intermediateCurrency.focus();
                    return false;
                } }

            <?
            if(CONFIG_24HR_BANKING == "1"){
            ?>
            if(theForm.rateFor.value == 'bank24hr'){
                if(theForm._24hrMarginValue.value == "" || IsAllSpaces(theForm._24hrMarginValue.value)){
                    alert("Please provide 24 hr banking margin.");
                    theForm._24hrMarginValue.focus();
                    return false;
                }
            }

            if(theForm.rateFor.value == 'bank24hr'){
                if(theForm._24hrMarginType.options.selectedIndex == 0){
                    alert("Please select 24 hr banking margin type.");
                    theForm._24hrMarginType.focus();
                    return false;
                }
            }
            <?
            }
            ?>
            return checkRatePattern(theForm);

            <? if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1") { ?>
            if($("#denominationBased").attr('checked'))
                return checkDenomiationRates();
            else
                return true;
            <? }else{ ?>
            return true;
            <? } ?>


        }
        function IsAllSpaces(myStr){
            while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
            }
            if (myStr == ""){
                return true;
            }
            return false;
        }


        function checkRatePattern(theForm){
            var checkStr=document.forms.addRate.primaryExchange.value;
            var SplitResult = checkStr.split(".");
            if(SplitResult[1].length > 4)
            {
                alert("Enter four decimal place digit");
                theForm.primaryExchange.focus();
                return false;
            }else{
                return true;
            }
        }

        function calculateNet()
        {

            if(document.forms.addRate.rateFor.value == 'intermediate'){
                calculateIntermediateRate();
            }

            var Margintype = '';
            <?
            if(CONFIG_EXCHNG_MARGIN_TYPE == '1')
            { ?>
            Margintype = document.forms.addRate.marginType.value;
            <?	}?>
            var MarginValue = 0;
            if(Margintype == 'fixed')
            {
                MarginValue = (document.forms.addRate.primaryExchange.value - document.forms.addRate.marginPercentage.value);
            }else{
                MarginValue = (document.forms.addRate.primaryExchange.value - (document.forms.addRate.primaryExchange.value * document.forms.addRate.marginPercentage.value) / 100);
            }
            <?
            if(CONFIG_AGENT_RATE_MARGIN == '1' && $_SESSION["rateFor"] == 'agent')
            {
            ?>
            document.addRate.agentNet.value = MarginValue;

            agentMarginType = document.addRate.agentMarginType.value;

            var MarginValue2 = 0;
            if(agentMarginType == 'fixed')
            {
                MarginValue2 = (MarginValue - document.addRate.agentMarginValue.value);
            }else{
                MarginValue2 = (MarginValue - (MarginValue * document.addRate.agentMarginValue.value) / 100);
            }

            return MarginValue2;
            <?
            }
            ?>

            return	MarginValue;
        }

        function calculate24hrExRate()
        {
            var Margintype = '';
            <?
            if(CONFIG_EXCHNG_MARGIN_TYPE == '1')
            { ?>
            Margintype = document.forms.addRate.marginType.value;
            <?
            }
            ?>
            var Margintype24hr = document.forms.addRate._24hrMarginType.value;
            var exRate = 0.0;
            var primaryEx = 0.0;
            var margin24hr = 0.0;
            if(document.forms.addRate.primaryExchange.value != ''){
                primaryEx = parseFloat(document.forms.addRate.primaryExchange.value);
            }
            if(document.forms.addRate._24hrMarginValue.value != ''){
                margin24hr = parseFloat(document.forms.addRate._24hrMarginValue.value);
            }

            if(Margintype24hr == 'lower'){
                exRate = primaryEx - margin24hr;
            }else if(Margintype24hr == 'upper'){
                exRate = primaryEx + margin24hr;
            }

            if(Margintype == 'fixed')
            {
                exRate = (exRate - document.forms.addRate.marginPercentage.value);
            }else{
                exRate = (exRate - (exRate * document.forms.addRate.marginPercentage.value) / 100);
            }
            return	exRate;
        }

        function calculateIntermediateRate()
        {
            var intermediateRate = '';

            document.forms.addRate.primaryExchange.value = (document.forms.addRate.intermediateExchangeRate.value * document.forms.addRate.intermediateDestExchangeRate.value);

            //document.addRate.primaryExchange.value = intermediateRate;

            //return	intermediateRate;
        }

        function checkFocus(){
            //document.getElementById("countryOrigin").focus();
            if(document.getElementById("country").value != "")
                document.getElementById("currency").disabled=false;

            <? if(1) { ?>
            document.getElementById("net").value= calculateNet();
            <? } ?>

            updateGUI();

        }

        function reloadPage()
        {
            document.forms.addRate.action='add-rate.php';
            document.forms.addRate.submit();
        }

        function updateGUI()
        {
            if($("#denominationBased").attr('checked'))
            {
                $("#intervalRow").show();
                $("#rangeRow").show();

                $("#detailRow").show();

                $("#normalRateRow1").hide();
                $("#normalRateRow2").hide();
                $("#normalRateRow3").hide();
                $("#normalRateRow4").hide();

            }
            else
            {
                $("#intervalRow").hide();
                $("#rangeRow").hide();

                $("#detailRow").hide();

                $("#normalRateRow1").show();
                $("#normalRateRow2").show();
                $("#normalRateRow3").show();
                $("#normalRateRow4").show();


            }
        }

        function checkDenomiationRates()
        {
            var totalElements = Number(document.getElementById("totalInterval").value);
            for(var j=0; j<totalElements; j++)
            {
                if(document.getElementById("intervalRate"+j).value == "")
                {
                    alert("Please provide denomination based exchange rate.");
                    document.getElementById("intervalRate"+j).focus();
                    return false;
                }
            }
            return true
        }
        // end of javascript -->
    </script>
</head>
<body onLoad="checkFocus();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
    <tr>
        <td class="topbar">Add Exchange
            Rates </td>
    </tr>
    <form action="add-rate-conf.php" method="post" onSubmit="return checkForm(this);" name="addRate">
        <input type="hidden" name="historicalID" value="<?=$historicalID;?>">
        <tr>
            <td align="center">
                <table width="600" border="0" cellspacing="1" cellpadding="2" align="center">
                    <tr>
                        <td colspan="2" bgcolor="#000000">
                            <table width="600" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                                <tr>
                                    <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Add
                                                Exchange Rates</strong></font></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <? if ($_GET["msg"] != ""){
                        if ($_GET["next"] == "Y"){?>

                            <tr bgcolor="EEEEEE">
                                <td colspan="2">
                                    <table width="100%" cellpadding="5" cellspacing="0" border="0">
                                        <tr><td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                                            <td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>Add Next Exchange Rate of the Combination</b><br><br></font>"; $_SESSION['error'] = ""; ?>
                                            </td></tr></table>
                                </td></tr>


                        <? }else{ ?>
                            <tr bgcolor="EEEEEE">
                                <td colspan="2">
                                    <table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td><td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; $_SESSION['error'] = ""; ?></td></tr></table></td></tr>
                        <? }} ?>
                    <tr bgcolor="#ededed">
                        <td colspan="2" align="center"><font color="#FF0000">* Compulsory
                                Fields</font></td>
                    </tr>
                    <tr bgcolor="#ededed">
                        <td width="185"><font color="#005b90"><strong>Date</strong></font></td>
                        <td><input name="dDate" type="text" id="dDate" readonly value="<? echo $_SESSION["exchangeDate"];?>">&nbsp;<a href="javascript:show_calendar('addRate.dDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a></td>
                    </tr>
                    <?
                    if(CONFIG_AGENT_OWN_RATE == '1' && ($agentType == 'SUPA' || $agentType == 'SUPAI' || $agentType == 'SUBA' ))
                    {


                        if(CONFIG_ADD_SUB_AGENT_RATE == "1" && $agentType == 'SUPA'){
                            ?>


                            <tr bgcolor="#ededed">
                            <td><font color="#005b90"><strong>Select Agent</strong></font></td>
                            <td>
                                <select name="agentID" style="font-family:verdana; font-size: 11px">
                                    <option value="">- Select One -</option>
                                    <?


                                    $agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Sub' and isCorrespondent != 'Only' and (agentStatus='Active' or agentStatus='New') and parentID = '".$loggedUserID."' order by agentCompany");
                                    for ($i=0; $i < count($agents); $i++){
                                        ?>
                                        <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
                                        <?
                                    }

                                    ?>
                                </select>
                                <script language="JavaScript" type="text/javascript">
                                    SelectOption(document.forms.addRate.agentID, "<?=$_SESSION["agentID"] ?>");
                                </script>
                                <input type="hidden" name="rateFor" value="agent">
                                <i>If not selected, by default adds agent's own rate.</i>
                            </td>

                            <?
                        }
                        else
                        {

                            //if($agentType == "SUBA" || $agentType == "SUBAI")
                            $parentID = $_SESSION["loggedUserData"]["parentID"];

                            ?>
                            <input type="hidden" name="rateFor" value="agent" />
                            <input type="hidden" name="agentID" value="<?=$parentID?>" />

                            <?
                        }
                    }else{
                        ?>
                        <tr bgcolor="#ededed">
                            <td><font color="#005b90"><strong>Exchange Rate Based on<font color="#ff0000">*</font></strong></font></td>
                            <td>
                                <select name="rateFor" id="rateFor" style="font-family:verdana; font-size: 11px" onChange=" reloadPage(); ">
                                    <option value="">- Select One -</option>
                                    <option value="generic">- Generic Rate -</option>
                                    <?
                                    if(CONFIG_24HR_BANKING == "1"){
                                        ?>
                                        <option value="bank24hr">- 24 hr Banking -</option>
                                        <?
                                    }
                                    ?>
                                    <? if(($relatedRateId != '' && CONFIG_SETTLEMENT_COMMISSION == "1") || CONFIG_EXCHNG_DIST_BASED == "1"){?>
                                        <option value="distributor">- Distributor -</option>
                                    <? } ?>

                                    <?
                                    if(CONFIG_EXCHNG_TRANS_TYPE == '1')
                                    {
                                        ?>
                                        <option value="transType">- Transaction Type -</option>
                                        <?
                                    }elseif(CONFIG_EXCHANGE_MONEY_PAID == '1'){
                                        ?>
                                        <option value="MoneyPaid">- Money Paid -</option>
                                    <? } ?>
                                    <?
                                    if(CONFIG_AGENT_OWN_RATE == '1')
                                    {
                                        ?>
                                        <option value="agent">- Agent -</option>
                                        <?
                                    }
                                    if(CONFIG_CUSTOMER_CATEGORY == '1'){
                                        ?>
                                        <option value="customer">- Customer Category -</option>
                                    <? } ?>

                                </select>
                                <script language="JavaScript" type="text/javascript">SelectOption(document.forms.addRate.rateFor,"<?=$_SESSION["rateFor"]; ?>");</script>
                                <? if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1") { ?>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" name="denominationBased" id="denominationBased" value="Y" <?=$_REQUEST["denominationBased"] == "Y"?'checked="checked"':''?> onClick="reloadPage();updateGUI();" />
                                    <b>Denomination Based Exchange Rate</b>
                                <? } ?>
                            </td>
                        </tr>
                        <tr bgcolor="#ededed" id="rangeRow" style="display:none">
                            <td valign="top"><font color="#005b90"><strong>Amount Range<font color="#ff0000">*</font></strong></font></td>
                            <td>
                                <b>From:</b>&nbsp;<input type="text" name="amountFrom" value="<?=$_REQUEST["amountFrom"]?>" />
                                <br />
                                <b>Upto: </b>&nbsp;<input type="text" name="amountUpto" value="<?=$_REQUEST["amountUpto"]?>"  />
                            </td>
                        </tr>
                        <tr bgcolor="#ededed" id="intervalRow" style="display:none">
                            <td><font color="#005b90"><strong>Denomination Interval<font color="#ff0000">*</font></strong></font></td>
                            <td>
                                <input type="text" name="denominationInterval" value="<?=$_REQUEST["denominationInterval"]?>" onBlur="reloadPage();" />
                            </td>
                        </tr>


                        <? if($_SESSION["rateFor"] == "distributor" || $_SESSION["rateFor"] == "intermediate")
                        {
                            ?>

                            <tr bgcolor="#ededed">
                                <td><font color="#005b90"><strong>Select Distributor<font color="#ff0000">*</font></strong></font></td>
                                <td>
                                    <select name="agentID" style="font-family:verdana; font-size: 11px">
                                        <option value="">- Select One -</option>
                                        <?
                                        if(CONFIG_SUB_DISTRIBUTOR_AT_EXCHANGE_RATE == 1)
                                        {
                                            echo "<optgroup label='Super Distributors'>";
                                        }

                                        $agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'N' and (agentStatus='Active' or agentStatus='New') order by agentCompany");
                                        for ($i=0; $i < count($agents); $i++){
                                            ?>
                                            <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
                                            <?
                                        }
                                        if(CONFIG_SUB_DISTRIBUTOR_AT_EXCHANGE_RATE == 1)
                                        {

                                            echo "</optgroup><optgroup label='Sub Distributors'>";

                                            $agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Sub' and isCorrespondent != 'N' and (agentStatus='Active' or agentStatus='New') order by agentCompany");
                                            for ($i=0; $i < count($agents); $i++)
                                            {
                                                ?>
                                                <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
                                                <?

                                            }
                                            echo "</optgroup>";
                                        }
                                        ?>
                                    </select>
                                    <script language="JavaScript" type="text/javascript">
                                        SelectOption(document.forms.addRate.agentID, "<?=$_SESSION["agentID"] ?>");
                                    </script>
                                </td>
                            </tr>

                            <?
                        }elseif($_SESSION["rateFor"] == "money")
                        {
                            ?>
                            <tr bgcolor="#ededed">
                                <td width="185"><font color="#005b90"><strong>Money Range From </strong></font></td>
                                <td><input type="text" name="from" value="<?=stripslashes($_POST["from"]); ?>" size="15" maxlength="32"></td>
                            </tr>
                            <tr bgcolor="#ededed">
                                <td width="185"><font color="#005b90"><strong>Money Range To </strong></font></td>
                                <td><input type="text" name="to" value="<?=stripslashes($_POST["to"]); ?>" size="15" maxlength="32"></td>
                            </tr>
                            <?
                        }elseif($_SESSION["rateFor"] == "transType" && CONFIG_EXCHNG_TRANS_TYPE == '1')
                        {
                            ////////////////  Here agentID is to represent Transaction Type or Distributor ID  //////////////////
                            ?>

                            <tr bgcolor="#ededed">
                                <td><font color="#005b90"><strong>Select Transaction Type<font color="#ff0000">*</font></strong></font></td>
                                <td>
                                    <select name="agentID" style="font-family:verdana; font-size: 11px">
                                        <option value="">- Select One -</option>
                                        <option value="Pick up">- Pick up -</option>
                                        <option value="Bank Transfer">- Bank Transfer -</option>
                                        <option value="Home Delivery">- Home Delivery -</option>
                                    </select>
                                    <script language="JavaScript" type="text/javascript">
                                        SelectOption(document.forms.addRate.agentID, "<?=$_SESSION["agentID"] ?>");
                                    </script>
                                </td>
                            </tr>

                            <?
                        }elseif($_SESSION["rateFor"] == "MoneyPaid" ){
                            ?>

                            <tr bgcolor="#ededed">
                                <td><font color="#005b90"><strong>Select Money Paid<font color="#ff0000">*</font></strong></font></td>
                                <td>
                                    <select name="agentID" style="font-family:verdana; font-size: 11px">
                                        <option value="">- Select One -</option>
                                        <option value="By cash">- By cash -</option>
                                        <option value="By Bank Transfer">- By Bank Transfer -</option>

                                    </select>
                                    <script language="JavaScript" type="text/javascript">
                                        SelectOption(document.forms.addRate.agentID, "<?=$_SESSION["agentID"] ?>");
                                    </script>
                                </td>
                            </tr>
                        <? }elseif($_SESSION["rateFor"] == "agent")
                        {
                            ?>

                            <tr bgcolor="#ededed">
                                <td><font color="#005b90"><strong>Select Agent<font color="#ff0000">*</font></strong></font></td>
                                <td>
                                    <select name="agentID" style="font-family:verdana; font-size: 11px">
                                        <option value="">- Select One -</option>
                                        <?
                                        if(CONFIG_SUB_AGENT_AT_EXCHANGE_RATE == 1)
                                        {
                                            echo "<optgroup label='Super Agents'>";
                                        }

                                        $agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'Only' and (agentStatus='Active' or agentStatus='New') order by agentCompany");
                                        for ($i=0; $i < count($agents); $i++){
                                            ?>
                                            <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
                                            <?
                                        }
                                        if(CONFIG_SUB_AGENT_AT_EXCHANGE_RATE == 1)
                                        {
                                            echo "</optgroup><optgroup label='Sub Agents'>";

                                            $agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Sub' and isCorrespondent != 'Only' and (agentStatus='Active' or agentStatus='New') order by agentCompany");
                                            for ($i=0; $i < count($agents); $i++){
                                                ?>
                                                <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
                                                <?
                                            }
                                        }
                                        ?>
                                    </select>
                                    <script language="JavaScript" type="text/javascript">
                                        SelectOption(document.forms.addRate.agentID, "<?=$_SESSION["agentID"] ?>");
                                    </script>
                                </td>
                            </tr>

                            <?
                        }elseif($_SESSION["rateFor"] == "customer")
                        {
                            ?>
                            <tr bgcolor="#ededed">
                                <td><font color="#005b90"><strong>Select Customer Category<font color="#ff0000">*</font></strong></font></td>
                                <td>
                                    <select name="agentID" id="agentID" style="font-family:verdana; font-size: 11px">
                                        <option value="">-Select One-</option>
                                        <?
                                        $custCatSql = "select id,name from ".TBL_CUSTOMER_CATEGORY." where enabled = 'Y'";
                                        $category = selectMultiRecords($custCatSql);
                                        for($k =0; $k <count($category); $k++){
                                            ?>
                                            <option value="<?=$category[$k]["id"]; ?>" <? echo ($_SESSION["agentID"]==$category[$k]["id"] ? "selected" : "")?>>
                                                <?  echo($category[$k]["name"]); ?></option>
                                        <? } ?>
                                    </select>
                                    <script language="JavaScript">
                                        SelectOption(document.forms[0].custCategory, "<?=$_SESSION["agentID"]; ?>");
                                    </script>
                                </td>
                            </tr>
                            <?
                        }
                        ?>



                        <?
                    }
                    ?>


                    <? if(CONFIG_SETTLEMENT_COMMISSION == "1"){ ?>
                        <tr bgcolor="#ededed">
                            <td width="185"><font color="#005b90"><strong>Sending Default Currency<font color="#ff0000">*</font></strong></font></td>
                            <td width="210">
                                <select name="defaultCurrencyOrigin" onChange="document.forms.addRate.action='add-rate.php'; document.forms.addRate.submit();">
                                    <option value="">-- Select Currency --</option>
                                    <?
                                    if($relatedRateId != '')
                                    {
                                        $strQuery = "SELECT  DefaultSendingCurrency,defaultSendingCountry FROM relatedExchangeRate where id = '".$relatedRateId."' ";
                                        $nResult = selectFrom($strQuery);
                                        $country1 = $nResult["defaultSendingCountry"];
                                        $currencyOriginName = $nResult["DefaultSendingCurrency"];
                                        $description = $nResult["description"];

                                        echo "<option value ='$currencyOriginName' selected='selected'> ".$currencyOriginName." --> ".$country1." --> ".$description."</option>";

                                    }else{
                                        $strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
                                        $nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());

                                        while($rstRow = mysql_fetch_array($nResult))
                                        {
                                            $country1 = $rstRow["country"];
                                            $currencyOriginName = $rstRow["currencyName"];
                                            $description = $rstRow["description"];
                                            $erID  = $rstRow["cID"];
                                            if($_SESSION["countryOrigin"] == $rstRow["country"] && $_SESSION["currencyOrigin"] == "")
                                                echo "<option selected value ='$currencyOriginName'> ".$currencyOriginName." --> ".$country1." --> ".$description."</option>";
                                            else
                                                echo "<option value ='$currencyOriginName'> ".$currencyOriginName." --> ".$country1." --> ".$description."</option>";
                                        }
                                    }
                                    ?>
                                </select>
                                <script language="JavaScript" type="text/javascript">
                                    SelectOption(document.forms.addRate.defaultCurrencyOrigin, "<?=$_SESSION["defaultCurrencyOrigin"]?>");
                                </script>
                            </td>
                        </tr>

                        <tr bgcolor="#ededed">
                            <td width="185"><font color="#005b90"><strong>Sending Default Country<font color="#ff0000">*</font></strong></font></td>
                            <td width="210"><SELECT name="defaultCountryOrigin" style="font-family:verdana; font-size: 11px">
                                    <OPTION value="">- Select Country-</OPTION>
                                    <?
                                    if($relatedRateId != '')
                                    {
                                        $strQuery = "SELECT  DefaultSendingCurrency,defaultSendingCountry FROM relatedExchangeRate where id = '".$relatedRateId."' ";
                                        $strQueryCountry = selectFrom($strQuery);

                                        $Scountry = $nResult["defaultSendingCountry"];
                                        $currencyOriginName = $nResult["DefaultSendingCurrency"];
                                        $description = $nResult["description"];
                                        ?>
                                        <OPTION value="<?=$strQueryCountry["defaultSendingCountry"]; ?>" selected='selected' ><?=$strQueryCountry["defaultSendingCountry"]; ?></OPTION>
                                        <?
                                    } else {
                                        if(CONFIG_COUNTRY_SERVICES_ENABLED)
                                        {
                                            $countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where  countryId = fromCountryId  and  countryType like '%origin%' order by countryName");
                                        } else {
                                            $countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where  countryType like '%origin%' order by countryName");
                                        }

                                        for ($i=0; $i < count($countires); $i++)
                                        {
                                            ?>
                                            <OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
                                            <?
                                        }
                                    }
                                    ?>
                                </SELECT>

                                <script language="JavaScript" type="text/javascript">
                                    SelectOption(document.forms.addRate.defaultCountryOrigin, "<?=$_SESSION["defaultCountryOrigin"] ?>");
                                </script>
                            </td>
                        </tr>

                        <tr bgcolor="#ededed">
                            <td width="185"><font color="#005b90"><strong>Recieving Default Currency<font color="#ff0000">*</font></strong></font></td>
                            <td width="210">
                                <select name="defaultRecievingCurrency" onChange="document.forms.addRate.action='add-rate.php'; document.forms.addRate.submit();">
                                    <option value="">-- Select Currency --</option>
                                    <?
                                    if($relatedRateId != '')
                                    {
                                        $strQuery = "SELECT  defaultRecievingCurrency,defaultRecievingCountry FROM relatedExchangeRate where id = '".$relatedRateId."' ";
                                        $nResult = selectFrom($strQuery);

                                        $ReCountry = $nResult["defaultRecievingCountry"];
                                        $currencyDestName = $nResult["defaultRecievingCurrency"];
                                        $description = $nResult["description"];

                                        echo "<option value ='$currencyDestName' selected='selected'> ".$currencyDestName." --> ".$ReCountry." --> ".$description."</option>";
                                    }else{
                                        $strQuery = "SELECT DISTINCT(currencyName), description  FROM  currencies ORDER BY currencyName ";
                                        $nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());

                                        while($rstRow = mysql_fetch_array($nResult))
                                        {
                                            $defaultCurrencyName = $rstRow["currencyName"];
                                            $description = $rstRow["description"];
                                            $erID  = $rstRow["cID"];
                                            if($selectedCurrency == $currencyName)
                                            {
                                                echo "<option value ='$defaultCurrencyName' selected> ".$defaultCurrencyName." --> ".$description."</option>";
                                            } else {
                                                echo "<option value ='$defaultCurrencyName'> ".$defaultCurrencyName." --> ".$description."</option>";
                                            }
                                        }
                                    }
                                    ?>
                                </select>

                                <script language="JavaScript" type="text/javascript">
                                    SelectOption(document.forms.addRate.defaultRecievingCurrency, "<?=$_SESSION["defaultRecievingCurrency"]?>");
                                </script>
                            </td>
                        </tr>
                        <tr bgcolor="#ededed">
                            <td width="185"><font color="#005b90"><strong>Recieving Default Country<font color="#ff0000">*</font></strong></font></td>
                            <td width="210">
                                <SELECT name="defaultRecievingCountry"  style="font-family:verdana; font-size: 11px">
                                    <OPTION value="">- Select Country-</OPTION>
                                    <?
                                    if($relatedRateId != '')
                                    {
                                        $strQuery = "SELECT  defaultRecievingCurrency,defaultRecievingCountry FROM relatedExchangeRate where id = '".$relatedRateId."' ";
                                        $nResultQuery = selectFrom($strQuery);
                                        ?>
                                        <OPTION value="<?=$nResultQuery["defaultRecievingCountry"]; ?>" selected='selected'><?=$nResultQuery["defaultRecievingCountry"]; ?></OPTION>
                                        <?
                                    } else {
                                        if(CONFIG_COUNTRY_SERVICES_ENABLED)
                                        {
                                            $countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where  countryId = toCountryId  and  countryType like '%destination%' order by countryName");
                                        } else {
                                            $countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where  countryType like '%destination%' order by countryName");
                                        }

                                        for ($i=0; $i < count($countires); $i++)
                                        {
                                            ?>
                                            <OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
                                            <?
                                        }
                                    }
                                    ?>
                                </SELECT>
                                <script language="JavaScript" type="text/javascript">
                                    SelectOption(document.forms.addRate.defaultRecievingCountry, "<?=$_SESSION["defaultRecievingCountry"] ?>");
                                </script>
                            </td>
                        </tr>

                    <? } ?>
                    <tr bgcolor="#ededed">
                        <td width="185"><font color="#005b90"><strong>Originating Country<font color="#ff0000">*</font></strong></font></td>
                        <td width="210"><SELECT name="countryOrigin" id="countryOrigin" style="font-family:verdana; font-size: 11px" onChange="getCurrencyOrigin();">
                                <OPTION value="">- Select Country-</OPTION>
                                <?
                                if(CONFIG_COUNTRY_SERVICES_ENABLED){
                                    $queryCountry = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where  countryId = fromCountryId  and  countryType like '%origin%' order by countryName";
                                    if($countryBasedFlag){
                                        $queryCountry = "select distinct(countryName), countryId from ".TBL_COUNTRY.", ".TBL_SERVICE_NEW."  where  countryId = fromCountryId  AND countryType like '%origin%' order by countryName";
                                    }
                                    $countires = selectMultiRecords($queryCountry);
                                }
                                else
                                {
                                    $countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where  countryType like '%origin%' order by countryName");
                                }
                                for ($i=0; $i < count($countires); $i++){
                                    ?>
                                    <OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
                                    <?
                                }
                                ?>
                            </SELECT>
                            <script language="JavaScript" type="text/javascript">
                                SelectOption(document.forms.addRate.countryOrigin, "<?=$_SESSION["countryOrigin"] ?>");
                                function getCurrencyOrigin(){
                                    var originCountry = document.getElementById("countryOrigin").value;
                                    $.ajax({
                                        url: "add-rate-ajax-conf.php",
                                        data: {country: originCountry},
                                        type: "GET",
                                        success: function (data){
                                            document.getElementById("currencyOrigin").innerHTML = data;
                                        }
                                    });
                                }
                            </script>
                        </td>
                    </tr>
                    <tr bgcolor="#ededed">
                        <td width="185"><font color="#005b90"><strong>Originating Currency<font color="#ff0000">*</font></strong></font></td>
                        <td>
                            <select name="currencyOrigin" id="currencyOrigin" onChange="document.forms.addRate.action='add-rate.php'; document.forms.addRate.submit();">
                                <option value="">-- Select Currency --</option>
                                <?
                                /* #5955-premier exchange by Niaz Ahmad*/
                                if(CONFIG_CUSTOME_SENDING_RECEIVING_CURRENCIES == '1'){
                                    $strQuery = "SELECT DISTINCT(currencyName), description FROM  currencies currencyName 
							WHERE currency_for = 'S' OR currency_for = 'B' ORDER BY currencyName";
                                }else{
                                    $strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
                                }
                                $nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());

                                while($rstRow = mysql_fetch_array($nResult))
                                {
                                    $countryO = $rstRow["country"];
                                    $currencyO = $rstRow["currencyName"];
                                    $description = $rstRow["description"];
                                    $erID  = $rstRow["cID"];
                                    if($_SESSION["countryOrigin"] == $rstRow["country"] && $_SESSION["currencyOrigin"] == "")
                                        echo "<option selected value ='$currencyO'> ".$currencyO." --> ".$countryO." --> ".$description."</option>";
                                    else
                                        echo "<option value ='$currencyO'> ".$currencyO." --> ".$countryO." --> ".$description."</option>";
                                }
                                ?>
                            </select>
                            <script language="JavaScript" type="text/javascript">
                                SelectOption(document.forms.addRate.currencyOrigin, "<?=$_SESSION["currencyOrigin"] ?>");
                            </script>
                            <!--input type="text" name="currency" size="15" maxlength="255" --></td>



                    </tr>
                    <tr bgcolor="#ededed">
                        <td width="185">

                            <font color="#005b90"><strong>Destination Country<font color="#ff0000">*</font></strong></font></td>
                        <td width="210"><SELECT name="country" id="country" style="font-family:verdana; font-size: 11px" onchange='getCurrencyDestination();'>
                                <OPTION value="">- Select Country-</OPTION>
                                <?
                                if(CONFIG_SHARE_OTHER_NETWORK == '1')
                                {
                                    $sharedCountry = selectMultiRecords("select * from ".TBL_SHARE_COUNTRIES." where 1");

                                    $existingCountries = "";
                                    for($k=0; $k < count($sharedCountry); $k++)
                                    {
                                        if($k > 0)
                                        {
                                            $existingCountries .=",";
                                        }
                                        $existingCountries .= $sharedCountry[$k]["countryId"];
                                    }
                                }

                                if(CONFIG_COUNTRY_SERVICES_ENABLED){
                                    $countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where  countryId = toCountryId  and  countryType like '%destination%' ";
                                    if($countryBasedFlag){
                                        $countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE_NEW." where  countryId = toCountryId  and  countryType like '%destination%' ";
                                    }
                                }else{
                                    $countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." where  countryType like '%destination%' ";
                                }
                                if(CONFIG_SHARE_OTHER_NETWORK == '1')
                                {
                                    if($existingCountries != "")
                                    {
                                        $countiresQuery .=" and countryId not in (".$existingCountries.") ";
                                    }
                                }

                                $countiresQuery .=" order by countryName ";
                                $countires = selectMultiRecords($countiresQuery);
                                for ($i=0; $i < count($countires); $i++){
                                    ?>
                                    <OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
                                    <?
                                }
                                ?>
                            </SELECT>
                            <script language="JavaScript" type="text/javascript">
                                SelectOption(document.forms.addRate.country, "<?=$_SESSION["country"]?>");
                                function getCurrencyDestination(){
                                    if(this.value != "") document.getElementById("currency").disabled=false;
                                    var destinationCountry = document.getElementById("country").value;
                                    $.ajax({
                                        url: "add-rate-ajax-conf.php",
                                        data: {country: destinationCountry},
                                        type: "GET",
                                        success: function (data){
                                            document.getElementById("currency").innerHTML = data;
                                        }
                                    });
                                }
                            </script></td>
                    </tr>



                    <tr bgcolor="#ededed">
                        <td width="185"><font color="#005b90"><strong>Destination Currency<font color="#ff0000">*</font></strong></font></td>
                        <td>
                            <select name="currency" id="currency" onChange="document.forms.addRate.action='add-rate.php'; document.forms.addRate.submit();" <? echo ($_SESSION["rateFor"] == "bank24hr" ? " disabled" :''); ?>>
                                <option value="">-- Select Currency --</option>
                                <?
                                /* #5955-premier exchange by Niaz Ahmad*/
                                if(CONFIG_CUSTOME_SENDING_RECEIVING_CURRENCIES == '1'){
                                    $strQuery = "SELECT DISTINCT(currencyName), description FROM  currencies currencyName 
							WHERE currency_for = 'R' OR currency_for = 'B' ORDER BY currencyName";
                                }else{
                                    $strQuery = "SELECT DISTINCT(currencyName), description  FROM  currencies ORDER BY currencyName ";
                                }
                                $nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
                                while($rstRow = mysql_fetch_array($nResult))
                                {
                                    $currencyName = $rstRow["currencyName"];
                                    $description = $rstRow["description"];
                                    $erID  = $rstRow["cID"];
                                    if($selectedCurrency == $currencyName)
                                    {
                                        echo "<option value ='$currencyName' selected> ".$currencyName." --> ".$description."</option>";
                                    }
                                    else
                                    {
                                        echo "<option value ='$currencyName'> ".$currencyName." --> ".$description."</option>";
                                    }
                                }
                                ?>
                            </select>

                            <script language="JavaScript" type="text/javascript">
                                SelectOption(document.forms.addRate.currency, "<?=$_SESSION["currency"]?>");
                            </script>
                            <!-- input type="text" name="currency" size="15" maxlength="255" --></td>
                    </tr>



                    <tr bgcolor="#ededed" id="normalRateRow1">
                        <td width="185"><font color="#005b90"><strong><? if(CONFIG_ENABLE_RATE == "1"){echo(CONFIG_PIMARY_RATE);}else{echo("Primary Exchange");} ?><font color="#ff0000">*</font></strong></font></td>

                        <? if($_SESSION["rateFor"] == "agent" && CONFIG_AGENT_BASED_EXCHANGE_RATE_GENERIC_UPPER_VALUE == "1" ){ ?>
                            <td><input type="text" name="primaryExchange" id="primaryExchange" value="<? if (!empty($dbSqlDataSet2["primaryExchange"])) { echo $dbSqlDataSet2["primaryExchange"]; }else { echo stripslashes($_SESSION["primaryExchange"]); } ?>" size="15" maxlength="32" onBlur="javascript:document.addRate.net.value=calculateNet();" <? echo (($_SESSION["rateFor"] == "agent" && !empty($dbSqlDataSet2["primaryExchange"])) ? "readonly=readonly" :''); ?> ></td>
                        <? }else{ ?>
                            <td><input type="text" name="primaryExchange" id="primaryExchange" value="<? if (!empty($dbSqlDataSet["primaryExchange"])) { echo $dbSqlDataSet["primaryExchange"]; }else { echo stripslashes($_SESSION["primaryExchange"]); } ?>" size="15" maxlength="32" onBlur="javascript:document.addRate.net.value=calculateNet();" <? echo (($_SESSION["rateFor"] == "intermediate" ||$_SESSION["rateFor"] == "bank24hr") ? "readonly=readonly" :''); ?> ></td>
                        <? } ?>
                    </tr>
                    <?
                    if(CONFIG_EXCHNG_MARGIN_TYPE == '1'){
                        ?>
                        <tr bgcolor="#ededed"  id="normalRateRow2">
                            <td width="185"><font color="#005b90"><strong>Margin Type<font color="#ff0000">*</font></strong></font></td>
                            <td>
                                <select name="marginType" style="font-family:verdana; font-size: 11px" onChange="javascript:document.addRate.net.value=calculateNet();"<? echo ($_SESSION["rateFor"] == "bank24hr" ? " disabled" :''); ?>>
                                    <option value="percent" <?=(!empty($dbSqlDataSet["marginType"]) && $dbSqlDataSet["marginType"] == "percent" ? "selected" : "") ?>>- Percentage Margin -</option>
                                    <option value="fixed" <?=(!empty($dbSqlDataSet["marginType"]) && $dbSqlDataSet["marginType"] == "fixed" ? "selected" : "") ?>>- Fixed Margin -</option>
                                </select>
                                <?
                                if($_SESSION["rateFor"] == "bank24hr"){
                                    ?>
                                    <input type="hidden" name="marginType" value=<? echo $_SESSION["marginType"]; ?> >
                                    <?
                                }
                                ?>
                                <script language="JavaScript" type="text/javascript">SelectOption(document.forms.addRate.marginType,"<?=$_SESSION["marginType"]; ?>");</script>
                            </td>
                        </tr>
                        <?
                    }
                    ?>



                    <tr bgcolor="#ededed" id="normalRateRow3">
                        <td width="185"><font color="#005b90"><strong>Margin <? if(CONFIG_EXCHNG_MARGIN_TYPE != '1'){echo("Percentage");}?><font color="#ff0000">*</font></strong></font></td>
                        <td><input type="text" name="marginPercentage" value="<? if (!empty($dbSqlDataSet["marginPercentage"]) || $dbSqlDataSet["marginPercentage"] == 0) { echo $dbSqlDataSet["marginPercentage"]; } else { stripslashes($_SESSION["marginPercentage"]); } ?>" size="15" maxlength="32" onBlur="javascript:document.addRate.net.value=calculateNet();"<? echo ($_SESSION["rateFor"] == "bank24hr" ? " readonly=readonly" :''); ?>></td>
                    </tr>
                    <?
                    if(CONFIG_AGENT_RATE_MARGIN == '1' && $_SESSION["rateFor"] == "agent")
                    {
                        ?>
                        <tr bgcolor="#ededed">
                            <td><font color="#005b90"><strong>Exchange Rate </strong></font>(Before Agent Margin)</td>
                            <td><input name="agentNet" type="text" id="agentNet" value="" size="15" maxlength="32" readonly></td>
                        </tr>
                        <tr bgcolor="#ededed">
                            <td width="185"><font color="#005b90"><strong>Agent Margin Type<font color="#ff0000">*</font></strong></font></td>
                            <td>
                                <select name="agentMarginType" style="font-family:verdana; font-size: 11px" onChange="javascript:document.addRate.net.value=calculateNet();">
                                    <option value="percent">- Percentage Margin -</option>
                                    <option value="fixed">- Fixed Margin -</option>
                                </select>
                                <script language="JavaScript" type="text/javascript">SelectOption(document.forms.addRate.agentMarginType,"<?=$_SESSION["agentMarginType"]; ?>");</script>
                            </td>
                        </tr>
                        <tr bgcolor="#ededed">
                            <td width="185"><font color="#005b90"><strong>Agent Margin value</strong></font></td>
                            <td><input type="text" name="agentMarginValue" value="<?=stripslashes($_SESSION["agentMarginValue"]); ?>" size="15" maxlength="32" onBlur="javascript:document.addRate.net.value=calculateNet();"></td>
                        </tr>
                        <?
                    }
                    ?>
                    <tr bgcolor="#ededed"  id="normalRateRow4">
                        <td><font color="#005b90"><strong><? if(CONFIG_ENABLE_RATE == "1"){echo(CONFIG_NET_RATE);}else{echo("Net Exchange");} ?></strong></font></td>
                        <td><input name="net" type="text" id="net" value="" size="15" maxlength="32" readonly></td>
                    </tr>

                    <?
                    if(CONFIG_SHARE_OTHER_NETWORK == '1')
                    {
                        $jointClients = selectMultiRecords("select * from ".TBL_JOINTCLIENT." where 1 and isEnabled = 'Y' ");
                        ?>
                        <tr bgcolor="#ededed">
                            <td>
                                <font color="#005b90"><strong>Available to</strong></font>
                            </td>
                            <td>
                                <select name="remoteAvailable" id="remoteAvailable">
                                    <option value=""> Select One </option>
                                    <?
                                    for($cl = 0; $cl < count($jointClients); $cl++)
                                    {
                                        ?>

                                        <OPTION value="<?=$jointClients[$cl]["clientId"]; ?>" <? echo (strstr($_SESSION["remoteAvailable"],$jointClients[$cl]["clientId"]) ? "selected"  : "")?>> <?=$jointClients[$cl]["clientName"]?></option>
                                        <?
                                    }
                                    ?>

                            </td>
                        </tr>

                        <?
                    }
                    ?>
                    <input type="hidden" name="relatedRateId" value=<? echo $relatedRateId; ?> >

                    <?
                    if($_SESSION["rateFor"] == "bank24hr"){
                        ?>
                        <tr bgcolor="#ededed">
                            <td><font color="#005b90"><strong>24 hr Banking Margin<font color="#ff0000">*</font></strong></font></td>
                            <td>
                                <input name="_24hrMarginValue" type="text" id="_24hrMarginValue" value="<?=$_SESSION["_24hrMarginValue"]?>" size="15" onBlur="javascript:document.addRate._24hrExRate.value=calculate24hrExRate();">&nbsp;
                                <select name="_24hrMarginType" style="font-family:verdana; font-size: 11px" onChange="javascript:document.addRate._24hrExRate.value=calculate24hrExRate();">
                                    <option value="">-Select margin type-</option>
                                    <option value="lower">Lower</option>
                                    <option value="upper">Upper</option>
                                </select><font color="#ff0000">*</font>
                                <script language="JavaScript" type="text/javascript">
                                    SelectOption(document.forms.addRate._24hrMarginType,"<?=$_SESSION["_24hrMarginType"]; ?>");
                                </script>
                            </td>
                        </tr>
                        <tr bgcolor="#ededed">
                            <td><font color="#005b90"><strong>24 hr Banking Exchange Rate</strong></font></td>
                            <td>
                                <input name="_24hrExRate" type="text" id="_24hrExRate" value="" size="15" maxlength="32" readonly>
                            </td>
                        </tr>
                        <?
                    }

                    if($_REQUEST["denominationBased"] == "Y" && $_REQUEST["amountFrom"] != "" && !empty($_REQUEST["amountUpto"]) && !empty($_REQUEST["denominationInterval"]))
                    { /*
		$intTotalIntervals = ceil(($_REQUEST["amountUpto"] - $_REQUEST["amountFrom"])/$_REQUEST["denominationInterval"]);
	?>
		<tr bgcolor="#ededed" id="detailRow">
			<td align="left" colspan="2">
				<input type="hidden" name="totalInterval" id="totalInterval" value="<?=$intTotalIntervals?>" />
				<fieldset>
					<legend>Denomination Based Exchange Rate</legend>
					<table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
						<tr>
							<th width="30%">From</th>
							<th width="30%">To</th>
							<th width="40%">Exchange Rate</th>
						</tr>

						<? for($in = 0; $in < $intTotalIntervals; $in++)
						   {
						   		$upToAmountVal = ($_REQUEST["denominationInterval"]*$in)+$_REQUEST["denominationInterval"]+($_REQUEST["amountFrom"]-1) + 0.99;

								if($upToAmountVal > $_REQUEST["amountUpto"])
									$upToAmountVal = $_REQUEST["amountUpto"];

						?>
							<tr bgcolor="#ededed">
								<td align="center"><?=$_REQUEST["amountFrom"]+($_REQUEST["denominationInterval"]*$in)?></td>
								<td align="center"><?=($_REQUEST["denominationInterval"]*$in)+$_REQUEST["denominationInterval"]+($_REQUEST["amountFrom"]-1).".99"?></td>
								<td align="center">
									<?=$upToAmountVal?>
								</td>
								<td align="center">
									<input type="text" name="intervalRate<?=$in?>" id="intervalRate<?=$in?>" value="<?=$_REQUEST["intervalRate".$in]?>" style="text-align:right" />
								</td>
							</tr>
						<? } ?>
					</table>
				</fieldset>

			</td>
		</tr>

	<? */ } ?>



                    <?
                    if($_REQUEST["denominationBased"] == "Y" && $_REQUEST["amountFrom"] >= 0 && !empty($_REQUEST["amountUpto"]) && !empty($_REQUEST["denominationInterval"]))
                    {
                        $intTotalIntervals = ceil(($_REQUEST["amountUpto"] - $_REQUEST["amountFrom"])/$_REQUEST["denominationInterval"]);
                        ?>
                        <tr bgcolor="#ededed" id="detailRow">
                            <td align="left" colspan="2">
                                <input type="hidden" name="totalInterval" id="totalInterval" value="<?=$intTotalIntervals?>" />
                                <fieldset>
                                    <legend>Denomination Based Exchange Rate</legend>
                                    <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                                        <tr>
                                            <th width="30%">From</th>
                                            <th width="30%">To</th>
                                            <th width="40%">Exchange Rate</th>
                                        </tr>

                                        <? for($in = 0; $in < $intTotalIntervals; $in++) { ?>
                                            <tr bgcolor="#ededed">
                                                <td align="center"><?=$_REQUEST["amountFrom"]+($_REQUEST["denominationInterval"]*$in)?></td>
                                                <td align="center"><?=($_REQUEST["denominationInterval"]*$in)+$_REQUEST["denominationInterval"]?></td>
                                                <td align="center">
                                                    <input type="text" name="intervalRate<?=$in?>" id="intervalRate<?=$in?>" value="<?=$_REQUEST["intervalRate".$in]?>" style="text-align:right" />
                                                </td>
                                            </tr>
                                        <? } ?>
                                    </table>
                                </fieldset>

                            </td>
                        </tr>

                    <? } ?>


                    <tr bgcolor="#ededed">
                        <td colspan="2" align="center">
                            <input type="submit" value="Save">&nbsp;&nbsp; <input type="reset" value=" Clear ">
                        </td>
                    </tr>


                </table>
            </td>
        </tr>
    </form>
</table>
</body>
</html>