<?
session_start();
include ("../include/config.php");
$date_time = date('Y-m-d h:i:s A');
$limit_date = date('Y-m-d');
include ("security.php");
$transID = (int) $_GET["transID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
global $isCorrespondent;
global $adminType;

$agentType = getAgentType();

if($_GET["Submit"]== "Search")
{
	$Submit = "Search";
	if($_GET["fromDate"]!="")
	{
		$fromDate = $_GET["fromDate"];
	}
	if($_GET["toDate"]!="")
	{
		$toDate = $_GET["toDate"];
	}	
	if($_GET["type"]!="")
	{
		$type = $_GET["type"];
	}	
	if($_GET["id"]!="")
	{
		$id = $_GET["id"];
	}	
	if($_GET["by"]!="")
	{
		$by = $_GET["by"];
	}	
}

if($transID > 0)
{
	$contentTrans = selectFrom("select * from " . TBL_TRANSACTIONS . " where transID='".$transID."'");
	$createdBy = $contentTrans["createdBy"];
}
else
{
	redirect("suspend-transaction.php");
}

if($createdBy != 'CUSTOMER')
{
?>
<html>
<head>
<title>View Transaction Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../styles/admin.js"></script>
<script language="javascript">
	<!-- 
var custTitle = "Testing";
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm()
{
	if(document.form1.changeStatusTo.value == "")
	{
		alert("Please Select the Status.");
		form1.changeStatusTo.focus();
		return false;	
	}
	if(document.form1.remarks.value == "")
	{
		alert("Please Write your Remarks to suspend the transaction.");
		form1.remarks.focus();
		return false;	
	}	
}
	// end of javascript -->
	</script>
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000; font-weight: bold; }
.style4 {color: #6699cc}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<form name="form1" method="post" action="ch-transStatus-conf.php?transID=<? echo $_GET["transID"]?>" onSubmit="return checkForm();">
 
<table width="103%" border="0" cellspacing="1" cellpadding="5"> 
  <tr>
    <td bgcolor="#C0C0C0"><b><font color="#FFFFFF" size="2">View transaction details.</font></b></td>
  </tr>
   
  	<tr>
		<td>
			<a class="style2" href="suspend-transaction.php?transID=<?=$id;?>&transType=<?=$type;?>&searchBy=<?=$by;?>&fromDate=<?=$fromDate;?>&toDate=<?=$toDate;?>&Submit=<?=$Submit;?>">Go Back</a>
		</td>
	</tr>
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
          <tr>
            <td><fieldset>
            <legend class="style2">Transaction Details </legend>
            <table border="0">
              <tr>
                <td width="146" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
                <td width="196" height="20"><? echo $contentTrans["transType"]?></td>
                <td width="153" align="right"><font color="#005b90">Status</font></td>
                <td width="181"><? echo $contentTrans["transStatus"]?></td>
              </tr>
              <tr>
                <td width="146" height="20" align="right"><font class="style2"><? echo $systemCode; ?> </font></td>
                <td width="196" height="20"><strong><? echo $contentTrans["refNumberIM"]?></strong>
                </td>
                <td width="153" height="20" align="right"><font color="#005b90"><? echo $manualCode; ?> </font> </td>
                <td width="181" height="20"><? echo $contentTrans["refNumber"]; ?></td>
              </tr>
              <tr>
                <td width="146" height="20" align="right"><font color="#005b90">Transaction Date </font></td>
                <td width="196" height="20"><? 
				$TansDate = $contentTrans['transDate'];
				if($TansDate != '0000-00-00 00:00:00')
					echo date("F j, Y", strtotime("$TansDate"));
				
				?>
                </td>
                <td width="153" height="20" align="right"><font color="#005b90">Authorization Date </font></td>
                <td width="181" height="20"><? 
				$authoriseDate = $contentTrans['authoriseDate'];
				if($authoriseDate != '0000-00-00 00:00:00')
					echo date("F j, Y", strtotime("$authoriseDate"));				
				?></td>
              </tr>			  
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Sender Company</legend>
            <br>
            <table border="0" align="center">
              <tr>
                <td colspan="4" align="center" bgcolor="#D9D9FF"><img height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>"></td>
              </tr>
            </table>
            <br>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
              <legend class="style2">Sender Details </legend>
              <table border="0">
                <? 
		
			 $queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email  from ".TBL_CUSTOMER." where customerID ='" . $contentTrans[customerID] . "'";
			$customerContent = selectFrom($queryCust);
			if($customerContent["customerID"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Sender Name</font></td>
                  <td colspan="3"><? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></td>
                </tr>
				<?
				if($isCorrespondent == 'N' && $adminType != 'Agent')
				{
				?>				
                <tr>
                  <td width="150" align="right"><font color="#005b90">Address</font></td>
                  <td colspan="3"><? echo $customerContent["Address"] . " " . $customerContent["Address1"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                  <td width="200"><? echo $customerContent["Zip"]?></td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="224"><? echo $customerContent["Country"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Phone</font></td>
                  <td width="200"><? echo $customerContent["Phone"]?></td>
                  <td width="100" align="right"><font color="#005b90">Email</font></td>
                  <td width="224"><? echo $customerContent["Email"]?></td>
                </tr>
			<?
				}
				else{
				?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">&nbsp;</font></td>
                  <td width="200">&nbsp;</td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="224"><? echo $customerContent["Country"]?></td>
                </tr>				
				<?
				}
			  }
			  ?>
              </table>
              </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Beneficiary Agent Details</legend>
            <table border="0">
              <? 
			$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $contentTrans["benAgentID"] . "'";
			$senderAgentContent = selectFrom($querysenderAgent);
			if($senderAgentContent["userID"] != "")
			{
		?>
              <tr>
                <td width="150" align="right"><font color="#005b90">Agent Name</font></td>
                <td><? echo $senderAgentContent["name"]?> </td>
                <td align="right"><font color="#005b90">Contact Person </font></td>
                <td><? echo $senderAgentContent["agentContactPerson"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Address</font></td>
                <td colspan="3"><? echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Company</font></td>
                <td width="200"><? echo $senderAgentContent["agentCompany"]?></td>
                <td width="100" align="right"><font color="#005b90">Country</font></td>
                <td width="222"><? echo $senderAgentContent["agentCountry"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Phone</font></td>
                <td width="200"><? echo $senderAgentContent["agentPhone"]?></td>
                <td width="100" align="right"><font color="#005b90">Email</font></td>
                <td width="222"><? echo $senderAgentContent["email"]?></td>
              </tr>
              <?
			  }
			  ?>
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
            <table border="0" bordercolor="#006600">
              <? 
		
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email, IDType, IDNumber  from ".TBL_BENEFICIARY." where benID ='" . $contentTrans["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
		?>
              <tr> 
                <td width="156" height="20" align="right"><font class="style2">Beneficiary Name</font></td>
                <td width="186" ><? echo $benificiaryContent["firstName"] ?></td>
                <td width="138" height="20" align="right"><font class="style2">Last Name</font></td>
                <td width="196"><? echo $benificiaryContent["lastName"] ?></td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Address</font></td>
                <td width="186"><? echo $benificiaryContent["Address"] ?></td>
                
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Postal 
                  / Zip Code</font></td>
                <td width="186" height="20"><? echo $benificiaryContent["Zip"]?></td>
                <td width="138" align="right"><font color="#005b90">Country</font></td>
                <td width="196"><? echo $benificiaryContent["Country"]?> </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Phone</font></td>
                <td width="186" height="20"><? echo $benificiaryContent["Phone"]?></td>
                <td width="138" align="right"><font color="#005b90">Email</font></td>
                <td width="196"><? echo $benificiaryContent["Email"]?></td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font class="style2">ID Description </font></td>
                <td width="186" ><? echo $benificiaryContent["IDType"] ?></td>
                <td width="138" height="20" align="right"><font class="style2">ID Serial No.</font></td>
                <td width="196"><? echo  $benificiaryContent["IDNumber"] ?></td>
              </tr>
              <?
				}
			  if($contentTrans["transType"] == "Bank Transfer")
			  {
		  			$benBankDetails = selectFrom("select * from bankDetails where transID='".$transID."'");
					?>
              <tr> 
                <td width="156" height="20" class="style2">Beneficiary Bank Details: </td>
                <td width="186" height="20">&nbsp;</td>
                <td width="138" height="20">&nbsp;</td>
                <td width="196" height="20">&nbsp;</td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Bank 
                  Name</font></td>
                <td width="186" height="20"><? echo $benBankDetails["bankName"]; ?></td>
                <td width="138" align="right"><font color="#005b90">Acc Number</font></td>
                <td width="196"><? echo $benBankDetails["accNo"]; ?> </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Branch 
                  Code</font></td>
                <td width="186" height="20"><? echo $benBankDetails["branchCode"]; ?> 
                </td>
                <td width="138" align="right"><font color="#005b90"> 
                  <?
				if($benificiaryContent["Country"] == "United States")
				{
					echo "ABA Number*";
				}
				elseif($benificiaryContent["Country"] == "Brazil")
				{
					echo  "CPF Number*";
				}
				?>
                  </font></td>
                <td width="196">
                  <?
				if($benificiaryContent["Country"] == "United States" || $benificiaryContent["Country"] == "Brazil")
				{
				?>
                  <? echo $benBankDetails["ABACPF"]; ?> 
                  <?
				}
				?>
                </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Branch 
                  Address</font> </td>
                <td width="186" height="20"><? echo $benBankDetails["branchAddress"]; ?> 
                </td>
                <td width="138" height="20"  align="right"><font color="#005b90">&nbsp; 
                  </font></td>
                <td width="196" height="20">&nbsp; 
                  <!--echo $benBankDetails["IBAN"];-->
                </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="186" height="20">&nbsp;
                  <!-- echo $benBankDetails["swiftCode"]; -->
                </td>
                <td width="138" height="20" align="right" title="For european Countries only">&nbsp;</td>
                <td width="196" height="20" title="For european Countries only">&nbsp;</td>
              </tr>
              <?
  }
  			  if($contentTrans["transType"] == "Pick up")
			  {
					$benAgentID = $contentTrans["benAgentID"];
					$collectionPointID = $contentTrans["collectionPointID"];
					$queryCust = "select *  from cm_collection_point where  cp_id  = $collectionPointID";
					$senderAgentContent = selectFrom($queryCust);			

					?>
              <tr> 
                <td colspan="4" class="style2">Collection Point Details: </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font class="style2">Agent 
                  Name</font></td>
                <td width="186" height="20"><? echo $senderAgentContent["cp_corresspondent_name"]; ?></td>
                <td width="138" align="right"><font class="style2">Address</font></td>
                <td width="196"  valign="top"><strong><? echo $senderAgentContent["cp_branch_address"]; ?></strong> 
                </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font class="style2">Contact 
                  Person </font></td>
                <td width="186" height="20"><strong><? echo $senderAgentContent["cp_corresspondent_name"]; ?></strong>
                </td>
                <td width="138" align="right"><font class="style2">Branch Code</font></td>
				<td width="196" colspan="3"><strong><? echo $senderAgentContent["cp_ria_branch_code"]?></strong></td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font class="style2">Compnay </font> </td>
                <td width="186" height="20"><strong><? echo $senderAgentContent["cp_corresspondent_name"]; ?></strong> 
                </td>
                <td width="138" align="right"><font class="style2">Country</font></td>
                <td width="196"><strong><? echo $senderAgentContent["cp_country"]?></strong></td>
              </tr>
              <tr> 
                <td width="156" align="right"><font class="style2">Phone</font></td>
                <td width="186"><strong><? echo $senderAgentContent["cp_phone"]?></strong></td>
                <td width="138" height="20" align="right"><font class="style2">City</font></td>
                <td width="196" height="20"><strong><?  echo $senderAgentContent["cp_city"]; ?></strong>
                </td>
              </tr>
              <?
  }
  ?>
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table border="0">
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90"><font color="#005b90"><? echo $manualCode; ?></font></font></td>
                <td height="20" width="200"><? echo $contentTrans["refNumber"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Amount</font></td>
                <td width="225" height="20" ><? echo $contentTrans["transAmount"]?>  </td>
              </tr>
			  <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Exchange Rate</font></td>
                <td width="200" height="20"><? echo $contentTrans["exchangeRate"]?>                  </td>
                <td width="100" height="20" align="right"><font color="#005b90">Local Amount</font></td>
                <td width="225" height="20"><strong><? 
	
																	
				 echo $contentTrans["localAmount"]?>
								<? 
				  if(is_numeric($contentTrans["currencyTo"]))
				  	  echo "";
				  else
				  	  echo "in ".$contentTrans["currencyTo"]; 
				  ?>

				              </strong>  </td>
			  </tr>
				<?
				if($isCorrespondent == 'N' && $adminType != 'Agent')
				{
				?>				  
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
                <td width="200" height="20"><? echo $contentTrans["IMFee"]?></td>
                <td width="100" height="20" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="225" height="20"><? echo $contentTrans["totalAmount"]?></td>
              </tr>
			  <?
			  }			  
			  ?>
			  <? if($contentTrans["transType"] == "Bank Transfer"){
            	?>
        	<tr> 
          <td width="150" height="20" align="right"><font color="#005b90">Bank Charges</font></td>
          <td width="200" height="20"><? echo $contentTrans["bankCharges"]?> </td>
          <td width="100" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
          <td width="200" height="20">&nbsp;</td>
        </tr>
        	<?
        }
        	?>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Purpose</font></td>
                <td width="200" height="20"><? echo $contentTrans["transactionPurpose"]?>
                </td>
                <td width="100" align="right"><font color="#005b90">Funds Sources</font></td>
                                <td width="225"><? echo $contentTrans["fundSources"]?>
                </td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Money Paid</font></td>
                <td width="200" height="20"><? echo $contentTrans["moneyPaid"]?>
                </td>
     <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                                <td width="225">&nbsp;<!-- echo $contentTrans["bankCharges"]-->
                </td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90"><? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?></font></td>
                <td align="left"><? echo $contentTrans["tip"]?></td>	
                 <? if(CONFIG_CURRENCY_CHARGES == '1')
			  		{?>
					  <td align="right"><font color="#005b90"> Currency Charges </font></td>
					  <td align="left"><?=$contentTrans["outCurrCharges"]?></td>
					  <? }else{
					   ?>
					   <td>&nbsp;</td>
					  <td>&nbsp;</td>			  
					  <? 
					  }
					  ?>	                         
              </tr>
            </table>
            </fieldset></td>
          </tr>
<?
$get=$_GET["act"];
if($_GET["act"]=="")
	{
	$_GET["act"]="suspend";
	}
	if($_GET["act"]=="suspend" || $_GET["act"]=="amend" || $_GET["act"]=="cancel")
	{
?>		  
		  
		  
          <tr>
            <td align="left"><fieldset>
            <legend class="style3">Change Transaction Status </legend>
            <br>
			<table border="0">
							<? 
				if(CONFIG_SECRET_QUEST_ENABLED){	
						if ($contentTrans["question"]!=""){				
				?>
				<tr>
					<td width="150" align="right"><font color="#005b90">Secret Question </font></td>
          <td width="150" height="20" align="left" valign="top"><? echo $contentTrans["question"];?></td>
         
        </tr>
        <tr>
         <td width="150" align="right"><font color="#005b90">Answer is Compulsory to change the Status of Transaction. </font></td>
          <td width="196" align="left"><input type="text" name="answer"  id="answer"></td>
        </tr>
        <?
      }}
        ?>

              <tr>
                <td width="150" height="20" align="right">Change To: </td>
                <td height="20" align="left"><select name="changeStatusTo" class="flat" id="changeStatusTo" onChange="document.form1.action='ch-transStatus.php?transID=<?=$transID;?>&id=<?=$id;?>&type=<?=$type;?>&by=<?=$by;?>&fromDate=<?=$fromDate;?>&toDate=<?=$toDate;?>&Submit=<?=$Submit;?>'; document.form1.submit();">
                <option value="">-Select Status-</option>
					<?
					if($contentTrans["transType"] == "Pick up")
					{
						if($_GET["act"]=="suspend")
						{
							?>
							  <option value="Suspended" <? echo ($_POST["changeStatusTo"]=="Suspended" ? "Selected" : "")?>>Suspended</option>
							<?
						}elseif($_GET["act"]=="amend")
						{
							?>
							  <option value="Picked up">Picked up</option>
							  <option value="Failed">Failed</option>
							  <option value="Suspicious">Suspicious</option>
							  <option value="Rejected">Rejected</option>
							<?
						}elseif($_GET["act"]=="cancel")
						{
						?>
							  <!--option value="Picked up">Picked up</option-->
							  <option value="Reject Cancel">Reject Cancel</option>
							  <option value="Cancelled">Confirm Cancel</option>
							  
						<?
						}
					}
					elseif($contentTrans["transType"] == "Bank Transfer")
					{
						if($_GET["act"]=="suspend")
						{
							?>
							  <option value="Suspended" <? echo ($_POST["changeStatusTo"]=="Suspended" ? "Selected" : "")?>>Suspended</option>
							<?
						}elseif($_GET["act"]=="amend")
						{

						?>
						  <option value="Credited">Credited</option>
						  <option value="Failed">Failed</option>
						  <option value="Suspicious">Suspicious</option>
						<?
						}
						elseif($_GET["act"]=="cancel")
						{
						?>
							  <!--option value="Credited">Credited</option-->
							  <option value="Reject Cancel">Reject Cancel</option>
							  <option value="Cancelled">Confirm Cancel</option>
							  
						<?
						}
					}
					elseif($contentTrans["transType"] == "Home Delivery")
					{
						if($_GET["act"]=="suspend")
						{
							?>
							  <option value="Suspended" <? echo ($_POST["changeStatusTo"]=="Suspended" ? "Selected" : "")?>>Suspended</option>
							<?
						}elseif($_GET["act"]=="amend")
						{
						?>
						  <option value="Delivered">Delivered</option>
						  <option value="Out for Delivery">Out for Delivery</option>
						  <option value="Sent By Courier">Sent By Courier</option>					  
						  <option value="Failed">Failed</option>
						  <option value="Suspicious">Suspicious</option>
						  <option value="Rejected">Rejected</option>
						<?
						}elseif($_GET["act"]=="cancel")
						{
						?>
							  <!--option value="Delivered">Delivered</option-->
							  <option value="Reject Cancel">Reject Cancel</option>
							  <option value="Cancelled">Confirm Cancel</option>
							  
						<?
						}
					}
					?>
                </select><script language="JavaScript">SelectOption(document.form1.changeStatusTo, "<?=$_POST["changeStatusTo"]; ?>");</script></td>
              </tr><?
				  if($_POST["changeStatusTo"] == "Sent By Courier")
				  {?>
              <tr>
                <td width="150" height="20" align="right">
				
				  	Courier Tracking Number 
				
                  </td>
                <td height="20" align="left"><input name='trackingNum' type='text'></td>
              </tr>
			  <?
			  
			  }
				  ?>
				  <?
				  if($_POST["changeStatusTo"] == "Suspended" || $_POST["changeStatusTo"] == "Suspicious" || $_POST["changeStatusTo"] == "Rejected" || $_POST["changeStatusTo"] == "On Hold" || $_POST["changeStatusTo"]=="Cancelled" || $_POST["changeStatusTo"]=="Reject Cancel")
				  {?>
              <tr>
                <td width="150" height="20" align="right" valign="top">Your Remarks* </td>
                <td width="196"><textarea name="remarks" cols="30" rows="6" id="remarks"></textarea></td>
              </tr>
			  <?
			  }
			  
			  ?>
              <tr align="center">
                  <td height="20" colspan="2"> <input name="Submit" type="submit" class="flat" value="Submit"></td>
              </tr></form> 
            </table>
            
            <br>
            </fieldset></td>
          </tr>
        </table></td>
    </tr>
<?
}
?>
</table>
</body>
</html>
<?
}
else
{
?>
<html>
<head>
<title>View Transaction Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./styles/admin.js"></script>

<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000; font-weight: bold; }
.style4 {color: #6699cc}
-->
    </style>
<script language="javascript">
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm()
{
	if(document.form1.changeStatusTo.value == "")
	{
		alert("Please Select the Status.");
		form1.changeStatusTo.focus();
		return false;	
	}
	if(document.form1.remarks.value == "")
	{
		alert("Please Write your Remarks to suspend the transaction.");
		form1.remarks.focus();
		return false;	
	}	
}
</script>	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="103%" border="0" cellspacing="1" cellpadding="5">
  <form name="form1" method="post" action="ch-transStatus-conf.php?transID=<? echo $_GET["transID"]?>" onSubmit="return checkForm();">
   
  <tr>
    <td bgcolor="#6699cc"><b><font color="#FFFFFF" size="2">View transaction details.</font></b></td>
  </tr>
  <tr>
		<td>
				<? 
					if($_GET["back"]=="suspend-transaction")
					{?>
					<a class="style2" href="suspend-transaction.php?transType=<? echo $_GET["type"];?>&searchBy=<? echo $_GET["by"];?>&transID=<? echo $_GET["id"];?>&submit=Search&act=<? echo $_GET["act"]?>">Go Back</a>
					<? }elseif($_GET["back"]=="cancel_Transactions"){?>
				 <a class="style2" href="cancel_Transactions.php?transType=<? echo $_GET["type"];?>&searchBy=<? echo $_GET["by"];?>&transID=<? echo $_GET["id"];?>&submit=Search&act=<? echo $_GET["act"]?>">Go Back</a>
				 	<? } ?>
		</td>
	</tr>
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
          <tr>
            <td><fieldset>
            <legend class="style2">Transaction Details </legend>
            <table width="650" border="0">
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
                <td width="200" height="20"><? echo $contentTrans["transType"]?></td>
                <td width="100" align="right"><font color="#005b90">Status</font></td>
                <td width="200"><? echo $contentTrans["transStatus"]?></td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font class="style2"><? echo $systemCode; ?> </font></td>
                <td width="200" height="20"><strong><? echo $contentTrans["refNumberIM"]?></strong>
                </td>
                <td width="100" height="20" align="right"><font color="#005b90"><? echo $manualCode; ?></font></td>
                <td width="200" height="20"><? echo $contentTrans["refNumber"]?></td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Date </font></td>
                <td width="200" height="20"><? 
				$TansDate = $contentTrans['transDate'];
				if($TansDate != '0000-00-00 00:00:00')
					echo date("F j, Y", strtotime("$TansDate"));
				
				?>
                </td>
                <td width="150" height="20" align="right"><font color="#005b90">Authorization Date </font></td>
                <td width="150" height="20"><? 
				 
				$authoriseDate = $contentTrans['authoriseDate'];
				if($authoriseDate != '0000-00-00 00:00:00')
				{
					if($authoriseDate != '0000-00-00 00:00:00')
						echo date("F j, Y", strtotime("$authoriseDate"));				
				}
				?></td>
              </tr>				  
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td align="center"><fieldset>
            <legend class="style2">Sender Company</legend>
            <br>
            <table border="0" align="center">
              <tr>
                <td colspan="4" align="center" bgcolor="#D9D9FF"><img height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>"></td>
              </tr>
            </table>
            <br>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
              <legend class="style2">Sender Details </legend>
              <table width="650" border="0">
                <? 
		
			$queryCust = "select * from cm_customer where c_id ='" . $contentTrans["customerID"] . "'";
			$customerContent = selectFrom($queryCust);
			if($customerContent["c_id"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Sender Name</font></td>
                  <td colspan="3"><? echo $customerContent["FirstName"] . " " . $customerContent["MiddleName"] . " " . $customerContent["LastName"] ?></td>
                </tr>
                <tr>
                  <td width="150" align="right">&nbsp;</td>
                  <td width="200">&nbsp;</td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="200"><? echo $customerContent["c_country"]?></td>
                </tr>
                <?
			  }
			  ?>
            </table>
              </fieldset></td>
          </tr>
		  			  
          <tr>
            <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
                    <table width="650" border="0" bordercolor="#006600">
                      <? 
		
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email, IDType, NICNumber  from cm_beneficiary where benID ='" . $contentTrans["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
		?>
               <tr> 
                <td width="156" height="20" align="right"><font class="style2">Beneficiary Name</font></td>
                <td width="186" ><? echo $benificiaryContent["firstName"] ?></td>
                <td width="138" height="20" align="right"><font class="style2">Last Name</font></td>
                <td width="196"><? echo  $benificiaryContent["lastName"] ?></td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Address</font></td>
                <td width="186" ><? echo $benificiaryContent["Address"] ?></td>
                
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Postal 
                  / Zip Code</font></td>
                <td width="186" height="20"><? echo $benificiaryContent["Zip"]?></td>
                <td width="138" align="right"><font color="#005b90">Country</font></td>
                <td width="196"><? echo $benificiaryContent["Country"]?> </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Phone</font></td>
                <td width="186" height="20"><? echo $benificiaryContent["Phone"]?></td>
                <td width="138" align="right"><font color="#005b90">Email</font></td>
                <td width="196"><? echo $benificiaryContent["Email"]?></td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font class="style2">ID Description </font></td>
                <td width="186" ><? echo $benificiaryContent["IDType"] ?></td>
                <td width="138" height="20" align="right"><font class="style2">ID Serial No.</font></td>
                <td width="196"><? echo $benificiaryContent["NICNumber"] ?></td>
              </tr>
                      <?
				}
			  if($contentTrans["transType"] == "Bank Transfer")
			  {
		  			$benBankDetails = selectFrom("select * from cm_bankdetails where benID='".$contentTrans["benID"]."'");
					?>
                      <tr>
                        <td colspan="4" class="style2">Beneficiary Bank Details: </td>
                        
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Bank Name</font></td>
                        <td width="200" height="20"><? echo $benBankDetails["bankName"]; ?></td>
                        <td width="100" align="right"><font color="#005b90">Acc Number</font></td>
                        <td width="200"><? echo $benBankDetails["accNo"]; ?>                        </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Branch Code</font></td>
                        <td width="200" height="20"><? echo $benBankDetails["branchCode"]; ?>                        </td>
                        <td width="100" align="right"><font color="#005b90">
                          <?
				if($benificiaryContent["Country"] == "United States")
				{
					echo "ABA Number*";
				}
				elseif($benificiaryContent["Country"] == "Brazil")
				{
					echo  "CPF Number*";
				}
				?>
                          </font></td>
                        <td width="200"><?
				if($benificiaryContent["Country"] == "United States" || $benificiaryContent["Country"] == "Brazil")
				{
				?>
                          <? echo $benBankDetails["ABACPF"]; ?>
                          <?
				}
				?></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Branch Address</font> </td>
                        <td width="200" height="20"><? echo $benBankDetails["branchAddress"]; ?>                        </td>
		<td width="100" height="20"  align="right"><!--font color="#005b90">IBAN Number</font--> &nbsp;</td>
						<td width="200" height="20">&nbsp; <? //echo $benBankDetails["IBAN"]; ?></td>

                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><!--font color="#005b90">Swift Code</font--></td>
                        <td width="200" height="20"> &nbsp;<? // echo $benBankDetails["swiftCode"]; ?>                        </td>
                        <td width="100" height="20" align="right" title="For european Countries only">&nbsp;</td>
                        <td width="200" height="20" title="For european Countries only">&nbsp;</td>
                      </tr>
 <?
  }

			  if($contentTrans["transType"] == "Pick up")
			  {
					$benAgentID = $contentTrans["benAgentID"];
					$collectionPointID = $contentTrans["collectionPointID"];
					$queryCust = "select *  from cm_collection_point where  cp_id  = $collectionPointID";
					$senderAgentContent = selectFrom($queryCust);			

					?>
                      <tr>
                        <td colspan="4" class="style2">Collection Point Details: </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font class="style2">Agent Name</font></td>
                        <td width="200" height="20"><strong><? echo $senderAgentContent["cp_corresspondent_name"]; ?></strong></td>
                        <td width="100" align="right"><font class="style2">Address</font></td>
                        <td width="200" rowspan="2" valign="top"><strong><? echo $senderAgentContent["cp_branch_address"]; ?></strong>                        </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font class="style2">Contact Person </font></td>
                        <td width="200" height="20"><strong><? echo $senderAgentContent["cp_corresspondent_name"]; ?></strong>                        </td>
						<td width="150" align="right"><font class="style2">Branch Code</font></td>
						<td width="200" colspan="3"><strong><? echo $senderAgentContent["cp_ria_branch_code"]?></strong></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font class="style2">Compnay </font> </td>
                        <td width="200" height="20"><strong><? echo $senderAgentContent["cp_corresspondent_name"]; ?></strong>                        </td>
						<td width="100" align="right"><font class="style2">Country</font></td>
						<td width="200"><strong><? echo $senderAgentContent["cp_country"]?></strong></td>                       
                      </tr>
					  <tr> 
						<td width="150" align="right"><font class="style2">Phone</font></td>
						<td width="200"><strong><? echo $senderAgentContent["cp_phone"]?></strong></td>
					    <td width="100" height="20" align="right"><font class="style2">City</font></td>
                        <td width="200" height="20"><strong><?  echo $senderAgentContent["cp_city"]; ?></strong></td>
					  </tr>
					  
 <?
  }
  ?>
            </table>
              </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table width="650" border="0">
              <tr>
                <td height="20" align="right"><font color="#005b90">Exchange Rate</font></td>
                <td height="20"><? echo $contentTrans["exchangeRate"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Amount</font></td>
                <td width="200" height="20" ><? echo $contentTrans["transAmount"]?>                </td>
              </tr>
			  <tr>
			    <td height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
			    <td height="20"><? echo $contentTrans["IMFee"]?> </td>
                <td width="100" height="20" align="right"><font class="style2">Local Amount</font></td>
                <td width="200" height="20"><strong><? echo $contentTrans["localAmount"]?>     				<? 
				  if(is_numeric($contentTrans["currencyTo"]))
				  	  echo "";
				  else
				  	  echo "in ".$contentTrans["currencyTo"]; 
				  ?>
           </strong></td>
			  </tr>
          <tr>
            <td height="20" align="right"><font color="#005b90">Transaction Purpose</font></td>
            <td height="20"><? echo $contentTrans["transactionPurpose"]?> </td>
            <td width="100" height="20" align="right"><font color="#005b90">Total Amount</font></td>
            <td width="200" height="20"><? echo $contentTrans["totalAmount"]?> </td>
         </tr>
         <? if($contentTrans["transType"] == "Bank Transfer"){
            	?>
        	<tr> 
	          <td width="150" height="20" align="right"><font color="#005b90">Bank Charges</font></td>
	          <td width="200" height="20"><? echo $contentTrans["bankCharges"]?> </td>
	          <td width="100" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
	          <td width="200" height="20">&nbsp;</td>
        </tr>
        	<?
        }
        	?>
              <tr>
                <td height="20" align="right"><font color="#005b90">Money Paid</font></td>
                <td height="20"><? echo $contentTrans["moneyPaid"]?> </td>
                <td width="100" align="right"><!--font color="#005b90">Bank Charges</font-->&nbsp;</td>
                                <td width="200"><? //echo $contentTrans["bankCharges"]?>&nbsp;
                </td>
              </tr>
			  			<tr>
                <td width="150" align="right"><font color="#005b90"><? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?> </font></td>
                <td align="left"><? echo $contentTrans["tip"]?></td>
                 <? if(CONFIG_CURRENCY_CHARGES == '1')
			  		{?>
					  <td align="right"><font color="#005b90"> Currency Charges </font></td>
					  <td align="left"><?=$contentTrans["outCurrCharges"]?></td>
					  <? }else{
					   ?>
					   <td>&nbsp;</td>
					  <td>&nbsp;</td>			  
					  <? 
					  }
					  ?>	           	              
              </tr>
              <tr>
                <td width="150" height="20" align="right">&nbsp;</td>
                <td width="200" height="20">&nbsp;</td>
                <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200">&nbsp;</td>
              </tr>
            </table>
            </fieldset></td>
          </tr>
<?
	$get=$_GET["act"];
	if($_GET["act"]=="")
	{
	$_GET["act"]="suspend";
	}
	if($_GET["act"]=="suspend")
	{
?>		  
          <tr>
            <td align="left"><fieldset>
            <legend class="style3">Change Transaction Status </legend>
            <br>
			<table border="0">
			
				<? 
				if(CONFIG_SECRET_QUEST_ENABLED){
					if ($contentTrans["question"]!=""){
				?>
				<tr>
					<td width="150" align="right"><font color="#005b90">Secret Question </font></td>
          <td width="150" height="20" align="left" valign="top"><? echo $contentTrans["question"];?></td>
         
        </tr>
        <tr>
         <td width="150" align="right"><font color="#005b90">Answer is Compulsory to change the Status of Transaction. </font></td>
          <td width="196" align="left"><input type="text" name="answer"  id="answer"></td>
        </tr>
				
        <?
      }}
        ?>
              <tr>
                <td width="150" height="20" align="right">Change To: </td>
                <td height="20" align="left"><select name="changeStatusTo" class="flat" id="changeStatusTo" onChange="document.form1.action='ch-transStatus.php?transID=<?=$transID;?>&id=<?=$id;?>&type=<?=$type;?>&by=<?=$by;?>&fromDate=<?=$fromDate;?>&toDate=<?=$toDate;?>&Submit=<?=$Submit;?>'; document.form1.submit();">
                <option value="">-Select Status-</option>
					<?
					if($contentTrans["transType"] == "Pick up")
					{
						if($_GET["act"]=="suspend")
						{
							?>
							  <option value="Suspended" <? echo ($_POST["changeStatusTo"]=="Suspended" ? "Selected" : "")?>>Suspended</option>
							<?
						}elseif($_GET["act"]=="amend")
						{
							?>
							  <option value="Picked up">Picked up</option>
							  <option value="Failed">Failed</option>
							  <option value="Suspicious">Suspicious</option>
							  <option value="Rejected">Rejected</option>
							<?
						}elseif($_GET["act"]=="cancel")
						{
						?>
							  <option value="Picked up">Picked up</option>
							  <option value="Cancelled">Confirm Cancel</option>
							  
						<?
						}
					}
					elseif($contentTrans["transType"] == "Bank Transfer")
					{
						if($_GET["act"]=="suspend")
						{
							?>
							  <option value="Suspended" <? echo ($_POST["changeStatusTo"]=="Suspended" ? "Selected" : "")?>>Suspended</option>
							<?
						}elseif($_GET["act"]=="amend")
						{

						?>
						  <option value="Credited">Credited</option>
						  <option value="Failed">Failed</option>
						  <option value="Suspicious">Suspicious</option>
						<?
						}
						elseif($_GET["act"]=="cancel")
						{
						?>
							  <option value="Credited">Credited</option>
							  <option value="Cancelled">Confirm Cancel</option>
							  
						<?
						}
					}
					elseif($contentTrans["transType"] == "Home Delivery")
					{
						if($_GET["act"]=="suspend")
						{
							?>
							  <option value="Suspended" <? echo ($_POST["changeStatusTo"]=="Suspended" ? "Selected" : "")?>>Suspended</option>
							<?
						}elseif($_GET["act"]=="amend")
						{
						?>
						  <option value="Delivered">Delivered</option>
						  <option value="Out for Delivery">Out for Delivery</option>
						  <option value="Sent By Courier">Sent By Courier</option>					  
						  <option value="Failed">Failed</option>
						  <option value="Suspicious">Suspicious</option>
						  <option value="Rejected">Rejected</option>
						<?
						}elseif($_GET["act"]=="cancel")
						{
						?>
							  <option value="Delivered">Delivered</option>
							  <option value="Cancelled">Confirm Cancel</option>
							  
						<?
						}
					}
					?>
                </select><script language="JavaScript">SelectOption(document.form1.changeStatusTo, "<?=$_POST["changeStatusTo"]; ?>");</script></td>
              </tr><?
				  if($_POST["changeStatusTo"] == "Sent By Courier")
				  {?>
              <tr>
                <td width="150" height="20" align="right">
				
				  	Courier Tracking Number 
				
                  </td>
                <td height="20" align="left"><input name='trackingNum' type='text'></td>
              </tr>
			  <?
			  
			  }
				  ?>
				  <?
				  if($_POST["changeStatusTo"] == "Suspended" || $_POST["changeStatusTo"] == "Suspicious" || $_POST["changeStatusTo"] == "Rejected" || $_POST["changeStatusTo"] == "On Hold")
				  {?>
              <tr>
                <td width="150" height="20" align="right" valign="top">Your Remarks* </td>
                <td width="213"><textarea name="remarks" cols="30" rows="6" id="remarks"></textarea></td>
              </tr>
			  <?
			  
			  }
			  ?>
              <tr align="right">
                <td height="20" colspan="2">
                  <input name="Submit" type="submit" class="flat" value="Submit">
                  </td>
              </tr></form> 
            </table>
            
            <br>
            </fieldset></td>
          </tr>
		  
		  
        </table></td>
    </tr>
<?
}
?>
</table>

</body>
</html>
<?
}
?>
