<?
session_start();
include ("../include/config.php");
include ("security.php");
$agentType = getAgentType();

$filters = "";

/**
 * If no currency selected
 */
if(empty($_REQUEST["currency"]))
	$_REQUEST["currency"] = "GBP";

if($_REQUEST["Submit"])
{
	/**
	 * Date filter
	 */
	$filters = " and";
	 
	$fromDate = $_REQUEST["fYear"] . "-" . $_REQUEST["fMonth"] . "-" . $_REQUEST["fDay"];
	$toDate = $_REQUEST["tYear"] . "-" . $_REQUEST["tMonth"] . "-" . $_REQUEST["tDay"];
	$filters .= " (transactions.transDate BETWEEN '$fromDate 00:00:00' AND '$toDate 23:59:59')";

}

	if(!empty($_REQUEST["agent"]))
	{
		$filters .= " and transactions.custAgentID = '".$_REQUEST["agent"]."' ";
	}
	if(!empty($_REQUEST["currency"]))
	{
		$filters .= " and transactions.currencyFrom = '".$_REQUEST["currency"]."' ";
	}
	if(!empty($_REQUEST["paymentMode"]))
	{
		$filters .= " and transactions.moneyPaid = '".$_REQUEST["paymentMode"]."' ";
	}

$sql = "
				SELECT 
					username, 
					moneyPaid, 
					transactions.transDate,
					sum( totalAmount ) as total , 
					sum( recievedAmount ) as recieved , 
					(totalAmount - recievedAmount) AS outstanding
				FROM 
					transactions,
					admin
				WHERE 
					totalAmount != recievedAmount
					AND recievedAmount !=0
					AND admin.userID = transactions.custAgentID
					". $filters ."
				GROUP BY 
					custAgentID, 
					moneyPaid
					";
		
		$fullRS = SelectMultiRecords($sql);
		$optimizedData = array();
		
		for($j=0; $j < sizeof($fullRS); $j++)
		{
			
			$optimizedData[$fullRS[$j]["username"]][$fullRS[$j]["moneyPaid"]] = $fullRS[$j]["recieved"];
			$optimizedData[$fullRS[$j]["username"]]["total"] += $fullRS[$j]["total"];
			$optimizedData[$fullRS[$j]["username"]]["recieved"] += $fullRS[$j]["recieved"];
			$optimizedData[$fullRS[$j]["username"]]["outstanding"] += $fullRS[$j]["outstanding"];
		}
		
		
function printCurrency()
{

	if(!empty($_REQUEST["currency"]))
		echo $_REQUEST["currency"]." ";

}
		
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Agent Outstanding Balance Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}

.reportHeader {
	font-size: 24px;
	font-weight: bold;
	height: 50px;
}

.columnHeader {
	border: solid #cccccc 1px;
	font-weight: bold;
	background-color: #EEEEEE;
}

.bankName {
	font-size: 16px;
	font-weight: bold;
}

.transactions {
	border-bottom: solid #cccccc 1px;
	border-top: solid #cccccc 1px;
}

.subTotal {
	font-size: 16px;
	font-weight: bold;
	border-bottom: solid #bbbbbb 2px;
}

.reportSummary {
	font-weight: bold;
	border: solid #bbbbbb 1px;
}
-->
</style>
<script>
	<!--
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}
	-->
</script>
</head>
<body>
	
	<table border="0" align="center" cellpadding="5" cellspacing="1" >
		<tr>
			<td class="reportHeader">
				Agent Outstanding Balance Report<br /></td>
		</tr>
      <tr>
      	<td align="center">
  					<b>SEARCH FILTER</b>    				
      	</td>
    	</tr>
      <tr>
			  <form name="search" method="post" action="outstandingBalanceReport.php">
          <td width="100%" colspan="4" bgcolor="#EEEEEE" align="center">
											<b>From </b>
											<? 
											$month = date("m");
											
											$day = date("d");
											$year = date("Y");
											?>
									
							        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
						          <?
										  	for ($Day=1;$Day<32;$Day++)
												{
													if ($Day<10)
													$Day="0".$Day;
													echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
												}
										  ?>
							        </select>
							        <script language="JavaScript">
							         	SelectOption(document.search.fDay, "<?=(!empty($_POST["fDay"])?$_REQUEST["fDay"]:"")?>");
							        </script>
											<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
							          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
							          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
							          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
							          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
							          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
							          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
							          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
							          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
							          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
							          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
							          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
							          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
							        </SELECT>
							        <script language="JavaScript">
							         	SelectOption(document.search.fMonth, "<?=(!empty($_POST["fMonth"])?$_REQUEST["fMonth"]:"")?>");
							        </script>
							        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
							          <?
											  	$cYear=date("Y");
											  	for ($Year=2004;$Year<=$cYear;$Year++)
													{
														echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
													}
											  ?>
							        </SELECT> 
											<script language="JavaScript">
							         	SelectOption(document.search.fYear, "<?=(!empty($_POST["fYear"])?$_REQUEST["fYear"]:"")?>");
							        </script>
							        <b>To	</b>
							        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
							          <?
											  	for ($Day=1;$Day<32;$Day++)
													{
														if ($Day<10)
														$Day="0".$Day;
														echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
													}
											  ?>
							        </select>
											<script language="JavaScript">
							         	SelectOption(document.search.tDay, "<?=(!empty($_POST["tDay"])?$_REQUEST["tDay"]:"")?>");
							        </script>
											<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
							
							          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
							          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
							          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
							          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
							          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
							          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
							          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
							          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
							          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
							          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
							          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
							          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
							        </SELECT>
											<script language="JavaScript">
							         	SelectOption(document.search.tMonth, "<?=(!empty($_POST["tMonth"])?$_REQUEST["tMonth"]:"")?>");
							        </script>
							        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
						          <?
										  	$cYear=date("Y");
										  	for ($Year=2004;$Year<=$cYear;$Year++)
												{
													echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
												}
										  ?>
							        </SELECT>
											<script language="JavaScript">
							        SelectOption(document.search.tYear, "<?=(!empty($_POST["tYear"])?$_REQUEST["tYear"]:"")?>");
							        </script>
							        <br /><br />
											&nbsp;&nbsp;
										  <b> Agent Name </b>
									  	<select name="agent" style="font-family: verdana; font-size: 11px;">
												  <option value="">All</option>
			  									<?
												  	$agentQuery = "select userID, username from ". TBL_ADMIN_USERS." where adminType='Agent' and isCorrespondent != 'ONLY' and agentStatus='Active' order by username";
												  	$allAgentsData = SelectMultiRecords($agentQuery);
												  	foreach($allAgentsData as $k => $v)
												  	{
												  		if($_REQUEST["agent"] == $v["userID"])
												  			echo "<option value='$v[userID]' selected>$v[username]</option>";
												  		else
												  			echo "<option value='$v[userID]'>$v[username]</option>";
												  	}
												  ?>
										</select>      
										<br /><br />
										<b> Currency </b>
										&nbsp;
										<select name="currency" style="font-family: verdana; font-size: 11px;">
			  									<?
												  	$currencyQuery = "SELECT DISTINCT(currencyName) FROM  currencies ORDER BY currencyName ";
												  	$currencyData = SelectMultiRecords($currencyQuery);
												  	for($k=0;$k < count($currencyData);$k++)
												  	{
												  		if($_REQUEST["currency"] == $currencyData[$k]["currencyName"])
												  			echo "<option value='".$currencyData[$k]["currencyName"]."' selected>".$currencyData[$k]["currencyName"]."</option>";
												  		else
												  			echo "<option value='".$currencyData[$k]["currencyName"]."'>".$currencyData[$k]["currencyName"]."</option>";
												  	}
												  ?>
										</select>
										&nbsp;&nbsp;&nbsp;
										<b> Payment Mode</b>
										&nbsp;
										<select name="paymentMode" style="font-family: verdana; font-size: 11px;">
											  <option value="">All</option>
			  								<option value="By Cash" <?=$_REQUEST["paymentMode"] == "By Cash"?"selected":"" ?>>By Cash</option>
			  								<option value="By Bank Transfer" <?=$_REQUEST["paymentMode"] == "By Bank Transfer"?"selected":"" ?>>By Bank Transfer</option>
			  								<option value="By Card" <?=$_REQUEST["paymentMode"] == "By Card"?"selected":"" ?>>By Card</option>
			  								<option value="By Cheque" <?=$_REQUEST["paymentMode"] == "By Cheque"?"selected":"" ?>>By Cheque</option>
										</select>
										<br /><br />
										
					          <input type="submit" name="Submit" value="Process">
									</td>
							</form>
					    </tr>
					</table>
				
	<table width="90%" cellpadding="2" cellspacing="0" align="center">
		<!-- Report Header Starts -->
		<tr>
			<td colspan="6" class="reportHeader">
				&nbsp;<br /></td>
			<td colspan="3" align="right">&nbsp;</td>
		</tr>
		<!-- Report Header Ends -->
		<!-- Column Header Starts -->
		<tr>
			<td class="columnHeader">Agent</td>
			<td class="columnHeader">Total Amount</td>
			<td class="columnHeader">Received Amount</td>
			<?
				if(empty($_REQUEST["paymentMode"]))
				{
			?>
				<td class="columnHeader">Cash Received</td>
				<td class="columnHeader">Cheque Received</td>
				<td class="columnHeader">Bank Payments Received</td>
				<td class="columnHeader">Card Payments Received</td>
			<? }elseif($_REQUEST["paymentMode"]  == "By Cash") { ?>
					<td class="columnHeader">Cash Received</td> 
			<? }elseif($_REQUEST["paymentMode"]  == "By Cheque") { ?>
				<td class="columnHeader">Cheque Received</td>
			<? }elseif($_REQUEST["paymentMode"]  == "By Bank Transfer") { ?>
			<td class="columnHeader">Bank Payments Received</td>
			<? }elseif($_REQUEST["paymentMode"]  == "By Card") { ?>
			<td class="columnHeader">Card Payments Received</td>
			<? } ?>
			<td class="columnHeader">Outstanding Balance</td>
		</tr>
		<!-- Column Header Ends -->
		<!-- Transactions Data Starts -->
		<!-- Bank Name Starts -->
		<?
			
			foreach($optimizedData as $key => $val)
			{
		?>

		<tr>
			<td class="transactions"><?=$key?></td>
			<td class="transactions"><?=number_format($val["total"],2,".",","). printCurrency()?></td>
			<td class="transactions"><?=number_format($val["recieved"],2,".",","). printCurrency()?></td>
			<?
				if(empty($_REQUEST["paymentMode"]))
				{
			?>
				<td class="transactions"><?=number_format(!empty($val["By Cash"])? $val["By Cash"] :"0",2,".",","). printCurrency()?></td>
				<td class="transactions"><?=number_format(!empty($val["By Cheque"])? $val["By Cheque"] :"0",2,".",",".printCurrency())?></td>
				<td class="transactions"><?=number_format(!empty($val["By Bank Transfer"])? $val["By Bank Transfer"]:"0",2,".",",").printCurrency()?></td>
				<td class="transactions"><?=number_format(!empty($val["By Card"])? $val["By Card"]:"0",2,".",",").printCurrency()?></td>
					
			<? }elseif($_REQUEST["paymentMode"]  == "By Cash") { ?>
			<td class="transactions"><?=number_format(!empty($val["By Cash"])? $val["By Cash"] :"0",2,".",",").printCurrency()?></td>
			<? }elseif($_REQUEST["paymentMode"]  == "By Cheque") { ?>
			<td class="transactions"><?=number_format(!empty($val["By Cheque"])? $val["By Cheque"] :"0",2,".",",").printCurrency()?></td>
			<? }elseif($_REQUEST["paymentMode"]  == "By Bank Transfer") { ?>
			<td class="transactions"><?=number_format(!empty($val["By Bank Transfer"])? $val["By Bank Transfer"]:"0",2,".",",").printCurrency()?></td>
			<? }elseif($_REQUEST["paymentMode"]  == "By Card") { ?>
			<td class="transactions"><?=number_format(!empty($val["By Card"])? $val["By Card"]:"0",2,".",",").printCurrency()?></td>
			<? } ?>
			<td class="transactions"><?=number_format($val["outstanding"],2,".",",").printCurrency()?></td>
		</tr>
<?
			
		}
/**
 * If no records to show
 */
 if(count($fullRS) < 1)
 {
?>
	<tr>
			<td class="transactions" align="center" colspan="9">No Records found!</td>
	</tr>
<?
	}
?>

</table>

</body>
</html>
