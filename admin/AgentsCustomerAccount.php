<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

/*
Added by Mian Anjum
*/

//print_r($agentType);
$where_sql='';
if($agentType=='SUPA' || $agentType=='SUPI')
{

$where_sql="  AND c.agentID='".$changedBy."'  ";
}


session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");
//session_register["agentID2"];

if($_POST["Submit"]=="Search")
{
	
	$_SESSION["grandTotal"]="";
	$_SESSION["CurrentTotal"]="";


	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";
			
		
	$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];
	
}

if($_POST["custID"]!="")
	$_SESSION["custID"] =$_POST["custID"];
	elseif($_GET["custID"]!="")
		$_SESSION["custID"]=$_GET["custID"];
	else 
		$_SESSION["custID"] = "";
		
	
if ($offset == "")
	$offset = 0;
$limit=20;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;

?>
<HTML>
<HEAD>
	<TITLE>Agent's Sender Account</TITLE>
<SCRIPT>
var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function delete_confirm(id)
{
  if(confirm("Are you sure you want to delete this item?"))
    return true;
  else
    return false;
}
function checkForm(theForm)
{
	<? if(CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT != "1"){	?>
	if(theForm.custID.value == "")
	{
		alert("Please provide Payin Book or Customer Reference.");
		theForm.custID.focus();
		return false;	
	}	
	<? } ?>
	return true;
}
</SCRIPT>
<SCRIPT LANGUAGE="javascript" SRC="./javascript/functions.js"></SCRIPT>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">
function clearCheque(recId)
{
	var confirmClear = confirm("Are you sure you want to Clear this Cheque ?");
	if(!confirmClear){
		return true;
	}
	$.ajax
	({
		type: "GET",
		url: "clearChequeCustomer.php",
		data: "id=" + recId,
		beforeSend: function(XMLHttpRequest)
		{
			$("#status"+recId).text("Processing...");
		},
		success: function(responseText)
		{
			$("#status"+recId).html(responseText);
		}
	});
}
</script>


<LINK HREF="images/interface.css" REL="stylesheet" TYPE="text/css">
    <STYLE TYPE="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </STYLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1"></HEAD>
<BODY>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="5">
  <!--tr>
    <td class="topbar"><strong><font color="#FFFFFF" size="2">Agent's Sender Account Report</font></strong></td>
  </tr-->
  <? if ($_GET["msg"] == "Y"){ ?>
		  <TR>
            <TD><TABLE WIDTH="100%" BORDER="0" CELLPADDING="5" CELLSPACING="0" BGCOLOR="#EEEEEE">
              <TR>
                <TD WIDTH="40" ALIGN="center"><FONT SIZE="5" COLOR="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><B><I><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></I></B></FONT></TD>
                <TD WIDTH="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?></TD>
              </TR>
            </TABLE></TD>
          </TR>
		  <?
		  }
		  ?>
  <tr>
    <td align="center">
           	
	<FORM ACTION="AgentsCustomerAccount.php" METHOD="post" NAME="Search" onSubmit="return checkForm(this);">
		 <TABLE BORDER="1" CELLPADDING="5" BORDERCOLOR="#666666">
		<TR>
            <TD WIDTH="463" nowrap BGCOLOR="#C0C0C0"><SPAN CLASS="tab-u"><STRONG>Search Filters 
              </STRONG></SPAN></TD>
        </TR>
        <TR>
        <TD ALIGN="center" nowrap>
  		From 
        <? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
                
                <SELECT NAME="fDay" SIZE="1" ID="fDay" STYLE="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </SELECT>
                <SCRIPT LANGUAGE="JavaScript">
         	SelectOption(document.forms[0].fDay, "<?=$_SESSION["fDay"]; ?>");
        </SCRIPT>
				<SELECT NAME="fMonth" SIZE="1" ID="fMonth" STYLE="font-family:verdana; font-size: 11px">
                  <OPTION VALUE="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION VALUE="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION VALUE="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION VALUE="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION VALUE="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION VALUE="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION VALUE="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION VALUE="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION VALUE="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION VALUE="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION VALUE="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION VALUE="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <SCRIPT LANGUAGE="JavaScript">
         		SelectOption(document.forms[0].fMonth, "<?=$_SESSION["fMonth"]; ?>");
        	</SCRIPT>
                <SELECT NAME="fYear" SIZE="1" ID="fYear" STYLE="font-family:verdana; font-size: 11px">
                  <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
                </SELECT>
                <SCRIPT LANGUAGE="JavaScript">
         	SelectOption(document.forms[0].fYear, "<?=$_SESSION["fYear"]; ?>");
        </SCRIPT>
                To 
               
                <SELECT NAME="tDay" SIZE="1" ID="tDay" STYLE="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </SELECT>
                <SCRIPT LANGUAGE="JavaScript">
         	SelectOption(document.forms[0].tDay, "<?=$_SESSION["tDay"]; ?>");
        </SCRIPT>
		 <SELECT NAME="tMonth" SIZE="1" ID="tMonth" STYLE="font-family:verdana; font-size: 11px">
                  <OPTION VALUE="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION VALUE="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION VALUE="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION VALUE="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION VALUE="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION VALUE="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION VALUE="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION VALUE="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION VALUE="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION VALUE="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION VALUE="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION VALUE="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <SCRIPT LANGUAGE="JavaScript">
         	SelectOption(document.forms[0].tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </SCRIPT>
                <SELECT NAME="tYear" SIZE="1" ID="tYear" STYLE="font-family:verdana; font-size: 11px">
                  <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
                </SELECT>
                <SCRIPT LANGUAGE="JavaScript">
         	SelectOption(document.forms[0].tYear, "<?=$_SESSION["tYear"]; ?>");
        </SCRIPT>
                <BR>
                Customer Reference
                <INPUT NAME="custID" TYPE="text" ID="custID" SIZE="15" STYLE="font-family:verdana; font-size: 11px; width:100" VALUE="<?=$_SESSION["custID"];?>">
                <BR>
					 <?
				 	if(CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT == "1")
					{
				?>
						<br />
						Cheque Number: <input type="text" name="chequeNumber" id="chequeNumber" size="30" value="<?=$_REQUEST["chequeNumber"]?>" />
				<?
					}
				 ?>
				<BR>
				
                <INPUT TYPE="submit" NAME="Submit" VALUE="Search">
              </TD>
      </TR>
    </TABLE>
      <BR>
	  <BR>
      <BR>
		
      <?
	 if($_POST["Submit"]=="Search"||$_GET["search"]=="search")
	 {
	 	$Balance="";
		if($_GET["search"]=="search")
			$slctID=$_GET["custID"];
		else
			{
				$slctID=$_POST["custID"];	
			}
			
			$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
			$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
			$queryDate = "(a.Date >= '$fromDate 00:00:00' and a.Date <= '$toDate 23:59:59')";
		
	if(CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT != "1"){		 
		if ($slctID !=""){
			$accountQuery = "Select a.caID,a.customerID,a.currencytitle, a.Date, a.tranRefNo, a.payment_mode, a.Type, a.amount, a.modifiedBy, a.note,a.chequeNumber, a.chequeStatus from agents_customer_account as a, customer as c where a.customerID = c.customerID and  c.accountName like '$slctID'";
				 $accountQuery .= " and $queryDate";				
			$accountQueryCnt = "Select count(a.customerID) from agents_customer_account as a, customer as c where a.customerID = c.customerID and  c.accountName like '$slctID'";
			$accountQueryCnt .= " and $queryDate";
			
			if(CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT == "1")
			 {
			 	$chequeNumber = trim($_REQUEST["chequeNumber"]);
			 
			 	if(!empty($chequeNumber))
				{
					$accountQuery .= " and chequeNumber ='".$chequeNumber."' ";
					$accountQueryCnt .= " and chequeNumber ='".$chequeNumber."' ";
				}
			 }				
			
			
			$allCount = countRecords($accountQueryCnt);
			//echo $queryBen;
		$accountQuery .= " LIMIT $offset , $limit"; 
		$contentsAcc = selectMultiRecords($accountQuery);
		}
	}	
	
	if(CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT == "1"){	
		$chequeNumber = trim($_REQUEST["chequeNumber"]);	 
		if (!empty($slctID) && empty($chequeNumber)){
			$accountQuery = "Select a.caID,a.currencytitle,a.customerID, a.Date, a.tranRefNo, a.payment_mode, a.Type, a.amount, a.modifiedBy, a.note,a.chequeNumber, a.chequeStatus from agents_customer_account as a, customer as c where a.customerID = c.customerID and  c.accountName like '$slctID'";
							
			$accountQueryCnt = "Select count(a.customerID) from agents_customer_account as a, customer as c where a.customerID = c.customerID and  c.accountName like '$slctID'";
			
			}elseif(empty($slctID) && !empty($chequeNumber)){
			$accountQuery = "Select a.caID,a.currencytitle,a.customerID, a.Date, a.tranRefNo, a.payment_mode, a.Type, a.amount, a.modifiedBy, a.note,a.chequeNumber, a.chequeStatus from agents_customer_account as a, customer as c where a.customerID = c.customerID and  chequeNumber = '".$chequeNumber."'";
							
			$accountQueryCnt = "Select count(a.customerID) from agents_customer_account as a, customer as c where a.customerID = c.customerID and  chequeNumber = '".$chequeNumber."'";
			
			}elseif(!empty($slctID) && !empty($chequeNumber)){
				$accountQuery = "Select a.caID,a.currencytitle,a.customerID, a.Date, a.tranRefNo, a.payment_mode, a.Type, a.amount, a.modifiedBy, a.note,a.chequeNumber, a.chequeStatus from agents_customer_account as a, customer as c where a.customerID = c.customerID and  c.accountName like '$slctID' and chequeNumber = '".$chequeNumber."'";
							
			$accountQueryCnt = "Select count(a.customerID) from agents_customer_account as a, customer as c where a.customerID = c.customerID and  c.accountName like '$slctID' and chequeNumber = '".$chequeNumber."'";
			}
			//$where_sql
				$accountQuery.=$where_sql;
				///print_r($accountQuery);
		if(!empty($slctID) || !empty($chequeNumber)){	
		

			$accountQuery .= " and $queryDate";
			$accountQueryCnt .= " and $queryDate";
			$allCount = countRecords($accountQueryCnt);
			$accountQuery .= " LIMIT $offset , $limit"; 
			$contentsAcc = selectMultiRecords($accountQuery);
			
		 
		
		}else
		{
		}
	}	
	
		?>	
			
		  <TABLE WIDTH="700" BORDER="1" CELLPADDING="0" BORDERCOLOR="#666666" ALIGN="center">
			<TR>
			  <TD HEIGHT="25" nowrap BGCOLOR="#6699CC">
			  <TABLE WIDTH="100%" BORDER="0" CELLPADDING="2" CELLSPACING="0" BORDERCOLOR="#666666" BGCOLOR="#FFFFFF">
				  <TR>
				  <TD ALIGN="left"><?php if (count($contentsAcc) > 0) {;?>
					Showing <B><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></B> of
					<?=$allCount; ?>
					<?php } ;?>
				  </TD>
				  <?php if ($prv >= 0) { ?>
				  <TD WIDTH="50"><A HREF="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&custID=".$slctID."&search=search&total=first";?>"><FONT COLOR="#005b90">First</FONT></A> </TD>
				  <TD WIDTH="50" ALIGN="right"><A HREF="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$accountQuery."&custID=".$slctID."&search=search&total=pre";?>"><FONT COLOR="#005b90">Previous</FONT></A> </TD>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <TD WIDTH="50" ALIGN="right"><A HREF="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$accountQuery."&custID=".$slctID."&search=search&total=next";?>"><FONT COLOR="#005b90">Next</FONT></A>&nbsp; </TD>
				  <TD WIDTH="50" ALIGN="right"><!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$accountQuery."&custID=".$slctID."&search=search&total=last";?>"><font color="#005b90">Last</font></a-->&nbsp; </TD>
				  <?php } ?>
				</TR>
			  </TABLE></TD>
			</TR>
			
			<?
			if(count($contentsAcc) > 0)
			{
			?>
			<TR>
			<TD>
				<TABLE>
					<TR>
						<TD WIDTH="300">
							Cumulative Total <? 
							$customerBalance = selectFrom("select customerID,balance from ".TBL_CUSTOMER." where accountName = '".$slctID."'");
							//$allAmount=calculateBalanceDateCustomer($contentsAcc[0]["customerID"], $fromDate, $toDate);  
							//$allAmount = $customerBalance["balance"];
							$allAmount = calculateBalanceDateCustomer($customerBalance["customerID"],$fromDate,$toDate);
							echo number_format($allAmount,2,'.',',');
							?>
			</TD>
			<TD>
				<? if ($allAmount < 0 ){ ?>
				<FONT COLOR="red"><STRONG>The agent's Sender has to pay the money </STRONG></FONT>
				<? 
			}		
			if ($allAmount > 0 )
			{		
			?>
			<STRONG> Agent's Sender Balance  </STRONG>
			<? } ?>
			</TD>
		</TR>
			</TABLE>
			</TD>
		</TR>
			<TR>
			  <TD nowrap BGCOLOR="#EFEFEF">
			  <TABLE WIDTH="781" BORDER="0" BORDERCOLOR="#EFEFEF">
				<TR BGCOLOR="#FFFFFF">
						  <TD ALIGN="left" ><SPAN CLASS="style1">Date</SPAN></TD>
						  <TD ALIGN="left"><SPAN CLASS="style1">Modified By</SPAN></TD>
						  
						<TD ALIGN="left"><SPAN CLASS="style1">Money In</SPAN></TD>					  
						<TD WIDTH="213" ALIGN="left"><SPAN CLASS="style1">Money Out</SPAN></TD>
						<TD WIDTH="213" ALIGN="left"><SPAN CLASS="style1">Currency</SPAN></TD>
						<?
							if(CONFIG_SHOW_RECEIVED_AMOUNT_COLUMN)
							{
								$totalReceivedAmount = 0;
						?>
								<TD WIDTH="213" ALIGN="left"><SPAN CLASS="style1">Received</SPAN></TD>
						<?
							}
						?>
						<TD ALIGN="left"><SPAN CLASS="style1">Notes</SPAN></TD>
						<? if(CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT == "1"){?>
						<TD ALIGN="left"><SPAN CLASS="style1">Cheque#</SPAN></TD>
						<TD ALIGN="left"><SPAN CLASS="style1">Cheque Status</SPAN></TD>
						<? } ?>
						<TD WIDTH="120"  align="left"><SPAN CLASS="style1">Transaction</SPAN></TD>
						<TD WIDTH="96" ALIGN="left"><SPAN CLASS="style1">Description</SPAN></TD>
				</TR>
				<? for($i=0;$i < count($contentsAcc);$i++)
				{
					
				//$BeneficiryID = $contentsBen[$i]["benID"];
					?>
				<TR BGCOLOR="#FFFFFF">
					<TD WIDTH="200" ALIGN="left"><STRONG><FONT COLOR="#006699"><? 
					$authoriseDate = $contentsAcc[$i]["Date"];
					echo date("F j, Y", strtotime("$authoriseDate"));
				
					?></FONT></STRONG></TD>
					<TD WIDTH="200" ALIGN="left">
					<?  
					
					$agents = selectFrom("select userID, username, name from ".TBL_ADMIN_USERS." where userID = '".$contentsAcc[$i]["modifiedBy"]."'");
					echo($agents["name"]." [".$agents["username"]."]");
					
					?></TD>
					<TD WIDTH="150" ALIGN="left"><?  
					if($contentsAcc[$i]["Type"] == "DEPOSIT")
					{
						echo $contentsAcc[$i]["amount"];
						$Balance = $Balance+$contentsAcc[$i]["amount"];
					$totalDeposit += $contentsAcc[$i]["amount"];
					}
					?></TD>
					<TD ALIGN="left"><? 
					if($contentsAcc[$i]["Type"] == "WITHDRAW")
					{
						echo $contentsAcc[$i]["amount"];
						$Balance = $Balance-$contentsAcc[$i]["amount"];
					$totalWithdraw += $contentsAcc[$i]["amount"];
					}
					?></TD>
					<TD>
					
					<?PHP 
					
					echo $contentsAcc[$i]["currencytitle"]; ?>
					</TD>
					
					<?
						if(CONFIG_SHOW_RECEIVED_AMOUNT_COLUMN)
						{
							$receivedAmount = 0;
							
							if(!empty($contentsAcc[$i]["tranRefNo"]) && $contentsAcc[$i]["payment_mode"] != "Transaction is Cancelled")
							{
								$sql = "SELECT
											recievedAmount
										FROM
											transactions
										WHERE
											transID = ".$contentsAcc[$i]["tranRefNo"]."
										";
								$result = mysql_query($sql) or die(mysql_error());
								
								while($rs = mysql_fetch_array($result))
								{
									$receivedAmount = $rs["recievedAmount"];
								}
								
								$totalReceivedAmount += $receivedAmount;
							}
					?>
							<TD><?=$receivedAmount;?></TD>
					<?
						}
					?>
					
					<TD>&nbsp;<? echo $contentsAcc[$i]["note"]; ?></TD>
					
				<?	if(CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT == "1")
				{
			    ?>
					<td width="56">&nbsp;<?=$contentsAcc[$i]["chequeNumber"]?></td>
					<td width="56">&nbsp;
						<?
							if($contentsAcc[$i]["chequeStatus"] == "UNCLEARED")
							{
			
								/** 
								 * Only Admin can clear the cheques 
								 * @Ticket #4017
								 */								 						 
								if($agentType == "admin" || $agentType == "Admin Manager" || $agentType == "Admin" || $agentType == "Call")
									echo "<div id='status".$contentsAcc[$i]["caID"]."'><a href='#' style='{color:#0000ff}' title='Click to CLEAR the cheque.' onClick=\"javacript: clearCheque(".$contentsAcc[$i]["caID"].");\">".$contentsAcc[$i]["chequeStatus"]."</a></div>";
								else
									echo $contentsAcc[$i]["chequeStatus"];
							} 
							else {
								echo $contentsAcc[$i]["chequeStatus"];
							}
						?>
					</td>
			<?
				}
			?>
					
					<? if($contentsAcc[$i]["tranRefNo"] != ""){
							$trans = selectFrom("SELECT refNumberIM FROM ".TBL_TRANSACTIONS." WHERE transID ='".$contentsAcc[$i]["tranRefNo"]."'"); 
							
				?>
	
				<TD><A HREF="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsAcc[$i]["tranRefNo"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><STRONG><FONT COLOR="#006699"><? echo $trans["refNumberIM"]; ?></FONT></STRONG></A></TD>
				<? }else{?>
				<TD>&nbsp;</TD>
				<? }?>
				<TD WIDTH="56">
					&nbsp;<? echo $contentsAcc[$i]["payment_mode"]; ?>
				</TD>
					</TR>
				<?
				}
				?>
			  <TR BGCOLOR="#FFFFFF">
				   <TD>&nbsp;</TD>
				  <TD>&nbsp;</TD>
				   <TD>&nbsp;</TD>
				  <TD ALIGN="center">&nbsp;</TD>
				  <TD>&nbsp;</TD>
				  <TD>&nbsp;</TD>
				  <TD>&nbsp;</TD>
				  <?
			  		if(CONFIG_SHOW_RECEIVED_AMOUNT_COLUMN)
			  		{
			  	?>
					  <TD>&nbsp;</TD>
					<?
						}
					?>
				  <? if(CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT == "1"){?>	
					    <TD>&nbsp;</TD>
						<TD>&nbsp;</TD>
					<? } ?>
				</TR>
				
				<TR BGCOLOR="#FFFFFF">
				  <TD ALIGN="left"><B>Page Total Amount</B></TD>
	<TD>&nbsp;</TD>
						<TD><? echo  number_format($totalDeposit,2,'.',','); ?></TD>
				  <TD ALIGN="left"><? echo  number_format($totalWithdraw,2,'.',','); ?></TD>
				  	<?
				  		if(CONFIG_SHOW_RECEIVED_AMOUNT_COLUMN)
				  		{
				  	?>
				  			<TD><?=number_format($totalReceivedAmount, 2, ".", ",")?></TD>
				  	<?
				  		}
				  	?>
						<TD>&nbsp;</TD>
						<TD>&nbsp;</TD>
						<TD>&nbsp;</TD>
					<? if(CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT == "1"){?>	
					    <TD>&nbsp;</TD>
						<TD>&nbsp;</TD>
					<? } ?>
				</TR>			
				<TR> 
					<!--  <TD CLASS="style3" WIDTH="200"> Running Total:</TD>
					  <TD WIDTH="200"> 
						<?
					if($_GET["total"]=="first")
					{
					$_SESSION["grandTotal"] = $Balance;
					$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="pre"){
						$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
						$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="next"){
						$_SESSION["grandTotal"] += $Balance;
						$_SESSION["CurrentTotal"]= $Balance;
					}else
						{
						$_SESSION["grandTotal"] = $Balance;
						$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
						}
					//echo number_format($_SESSION["grandTotal"],2,'.',',');
					?>
					  </TD>-->
				  	<TD>&nbsp;</TD>
					<TD>&nbsp;</TD>
						<TD>&nbsp;</TD>	
						<TD>&nbsp;</TD>
						<TD>&nbsp;</TD>
                </TR>
            <TR BGCOLOR="#FFFFFF">
              <TD COLSPAN="5">
<!--
/************************************************************************/

Here You will write your Comments or any Information

/************************************************************************/
-->			  
			  </TD>

            </TR>
			</TABLE>		
            <?
			} // greater than zero
			else
			{
			?>
			<TR>
				<TD BGCOLOR="#FFFFCC"  align="center">
				<FONT COLOR="#FF0000">No data to view for Selected Filter. Select Proper Date and Agent </FONT>
				</TD>
			</TR>
        <TR>
          <TD nowrap BGCOLOR="#EFEFEF">
		  <TABLE WIDTH="781" BORDER="0" BORDERCOLOR="#EFEFEF">
            <TR BGCOLOR="#FFFFFF">
				<TD WIDTH="93" ALIGN="left"><SPAN CLASS="style1">Date</SPAN></TD>
                 
                <TD WIDTH="215" ALIGN="left"><SPAN CLASS="style1">Modified By</SPAN></TD>
                      
                 <TD WIDTH="174" ALIGN="left"><SPAN CLASS="style1">Money In</SPAN></TD>
                    
                 <TD WIDTH="281" ALIGN="left">Money Out</TD>
				 <TD WIDTH="120"  align="left"><SPAN CLASS="style1">Transaction</SPAN></TD>
						<TD WIDTH="96" ALIGN="left"><SPAN CLASS="style1">Description</SPAN></TD>
            </TR>	
			<TR BGCOLOR="#FFFFFF">
				<TD>&nbsp;</TD>
              	<TD>&nbsp;</TD>
              	<TD>&nbsp;</TD>
				<TD>&nbsp;</TD>
				<TD>&nbsp;</TD>
              	<TD ALIGN="left">&nbsp;</TD>
            </TR>
			</TABLE>
		   </TD>
		  </TR>
			<?
			}
			//if($slctID!='')
				//{
					if($agentType == "admin" || $agentType == "Admin Manager")//101
					{
						?>
					  <TR> 
						<TD BGCOLOR="#c0c0c0"><SPAN CLASS="child"><A HREF="add_AgentsCustomerAccount.php?agent_Account=<?=$_SESSION["custID"];?>&modifiedBy=<?=$_SESSION["loggedUserData"]["userID"];?>" TARGET="mainFrame" CLASS="tblItem style4"><FONT COLOR="#990000">Deposit 
						  Money</FONT></A></SPAN></TD>
					  </TR>
					  <? 
				  	}
				 //}
	?>
       </TABLE>
	   
	<?	
	 }else
			{
				$_SESSION["grandTotal"]="";
				$_SESSION["CurrentTotal"]="";
		 
	
				$_SESSION["fMonth"]="";
				$_SESSION["fDay"]="";
				$_SESSION["fYear"]="";
				
				$_SESSION["tMonth"]="";
				$_SESSION["tDay"]="";
				$_SESSION["tYear"]="";
			}///End of If Search
	 ?>
	 
	 </td>
	</tr>
	<? if($slctID!=''){?>
		<TR>
		  	<TD> 
				<DIV ALIGN="center">
					<INPUT TYPE="button" NAME="Submit2" VALUE="Print This Report" onClick="print()">
				</DIV>
			</TD>
		  </TR>
		  <? }?>
	</FORM>
	</td>
	</tr>
</TABLE>
</BODY>
</HTML>