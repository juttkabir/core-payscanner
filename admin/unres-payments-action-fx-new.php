<?
	session_start();
	
	$date_time = date('d-m-Y  h:i:s A');
	
	include ("../include/config.php");
	include ("security.php");

	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");
	if($_REQUEST['complete']=="Complete"){
		
		$parameters=$_REQUEST;
		debug($parameters);
		$flag=true;
		foreach($parameters as $key=>$val){
			if($val=="on")
				$flag=false;
			
		}
		if($flag)
		header('Location:unres-payments-ajax-fx-new.php?type=1&msg=1');
		
		
		$payID=array();
		$transID=array();
		$i=$j=0;
		foreach($parameters as $key=>$val){
				
				if(substr($key,0,4)=='P_UN'){
					$arr=explode('UN',$key);
					$payID[$i]=$arr[1];
					$i++;
				}
				elseif(substr($key,0,4)=='T_UN'){
					$arr2=explode('UN',$key);
					$transID[$j]=$arr2[1];
					$j++;
				}
		}
		//debug($transID);
		//debug($payID);
				$totalTrans=$j--;
				$totalPay=$i--;
				$sumPay="select sum(remAmount) as pay from barclayspayments where ";
				$transPay="select sum(remTransAmount) as amt from transactions where  ";
				for($i=0;$i<count($payID);$i++){
					$sumPay.=" id= ".$payID[$i]."";
					if($i+1<count($payID))
						$sumPay.=" OR";
				
				}
				for($j=0;$j<count($transID);$j++){
					$transPay.=" transID= ".$transID[$j]."";
					if($j+1<count($transID))
						$transPay.=" OR";
				}
				///debug($transID);
				//debug($payID);
				$totalSumPay=selectFrom($sumPay);
				
				$totalSumPayAmt=intval($totalSumPay['pay']);
				$totalTransPay=selectFrom($transPay);
				
				$totalTransPayAmt=intval($totalTransPay['amt']);
				$tran_date = date("Y-m-d");	
				if($totalTrans==1 && $totalPay==1){ //one to one
				//debug("One to One");
				if($totalSumPayAmt>$totalTransPayAmt){
					if($_POST['T_'.$transID[0]]=='on')
							$transResolvedStatus='Y';
						else if(empty($_POST['T_'.$transID[0]]))
							$transResolvedStatus='P';
					$updateTrans="update transactions set isResolved='Y', resolveDate = '".$tran_date."',remTransAmount='0'";
					if($transResolvedStatus=='Y'){
						$updateTrans.=" , transStatus='Processing'";
					}
					$updateTrans.="where transID=".$transID[0]." ";
					update($updateTrans);
					$getBatchNum=selectFrom('select max(batchNumber) as bn from payments_resolved');
					$BatchNumVal=$getBatchNum['bn'] + 1;
/**********************Start Log Maintain***********************************/
$getAmtQuery=selectFrom('select transAmount,refNumberIM,remTransAmount from transactions where transID='.$transID[0].'');
					$logQuery="INSERT INTO payments_resolved (
`payID` ,
`transID` ,
`agentAmount` ,
`action_date` ,
`resolvedBy` ,
`status` ,
`refNumberIMs` ,
`batchNumber` ,
`format` ,
`remAmount`
)
VALUES ('0',".$transID[0].",".$getAmtQuery['transAmount'].",'$tran_date',".$_SESSION["loggedUserData"]["userID"].",'Y',".$getAmtQuery['refNumberIM'].",'$BatchNumVal','1',".$getAmtQuery['remTransAmount']." )";

					debug($logQuery);
					insertInto($logQuery);
/**********************End Log Maintain***********************************/
					$getPayAmount="select id,amount from barclayspayments where id=".$payID[0]."";
					$totalPayAmount=selectFrom($getPayAmount);
					$amt=intval($totalPayAmount['amount']);
						$totalTransPay=intval($totalTransPay['amt']);
						$newTransPay=$amt-$totalTransPay;
						if($_POST['P_'.$payID[0]]=='on')
							$isResolvedStatus='Y';
						else if(empty($_POST['P_'.$payID[0]]))
							$isResolvedStatus='P';
			$updatePay="update barclayspayments set isResolved='".$isResolvedStatus."',remAmount=".$newTransPay." where id=".$payID[0]."";
						//debug($updatePay);
						update($updatePay);
/**********************Start Log Maintain***********************************/
						$getPayAmtQuery=selectFrom('select amount,remAmount from barclayspayments where id='.$payID[0].'');
						$logQuery="INSERT INTO payments_resolved (
`payID` ,
`transID` ,
`agentAmount` ,
`action_date` ,
`resolvedBy` ,
`status` ,
`refNumberIMs` ,
`batchNumber` ,
`format` ,
`remAmount`
)
VALUES (".$payID[0].",'0',".$getPayAmtQuery['amount'].",'$tran_date',".$_SESSION["loggedUserData"]["userID"].",'".$isResolvedStatus."','null','$BatchNumVal','1',".$getPayAmtQuery['remAmount']." )";
						insertInto($logQuery);
/**********************End Log Maintain***********************************/
						debug($logQuery);
						debug($updateTrans);
						debug($updatePay);
				}
				else if($totalSumPayAmt<$totalTransPayAmt){
					//debug("P<T");
					if($_POST['P_'.$payID[0]]=='on')
							$isResolvedStatus='Y';
					else if(empty($_POST['P_'.$payID[0]]))
							$isResolvedStatus='P';
					$updatePay="update barclayspayments set isResolved='Y', resolved_by=".$_SESSION["loggedUserData"]["userID"].", remAmount='0' where id=".$payID[0]."";
					debug("updating payment");
					debug($updatePay);
					
					update($updatePay);
					$getBatchNum=selectFrom('select max(batchNumber) as bn from payments_resolved');
					$BatchNumVal=$getBatchNum['bn'] + 1;
/******************************Start Log Maintain***********************************/
	$getPayAmtQuery=selectFrom('select amount,remAmount from barclayspayments where id='.$payID[0].'');
						$logQuery="INSERT INTO payments_resolved (
`payID` ,
`transID` ,
`agentAmount` ,
`action_date` ,
`resolvedBy` ,
`status` ,
`refNumberIMs` ,
`batchNumber` ,
`format` ,
`remAmount`
)
VALUES (".$payID[0].",'0',".$getPayAmtQuery['amount'].",'$tran_date',".$_SESSION["loggedUserData"]["userID"].",'Y','null','$BatchNumVal','1',".$getPayAmtQuery['remAmount']." )";
debug("log payment");
					debug($logQuery);
						insertInto($logQuery);


/**********************End Log Maintain***********************************/
					$newAmnt=$totalTransPayAmt-$totalSumPayAmt;
					if($_POST['T_'.$transID[0]]=='on')
							$transResolvedStatus='Y';
						else if(empty($_POST['T_'.$transID[0]]))
							$transResolvedStatus='P';
					$updateTrans="update transactions set remTransAmount=".$newAmnt." , isResolved='".$transResolvedStatus."'";
					if($transResolvedStatus=='Y'){
						$updateTrans.=" , transStatus='Processing'";
					}
					$updateTrans.="where transID=".$transID[0]." ";
					debug("updating transactions");
				debug($updateTrans);
					update($updateTrans);
/**********************Start Log Maintain***********************************/
$getAmtQuery=selectFrom('select transAmount,refNumberIM,remTransAmount from transactions where transID='.$transID[0].'');
					$logQuery="INSERT INTO payments_resolved (
`payID` ,
`transID` ,
`agentAmount` ,
`action_date` ,
`resolvedBy` ,
`status` ,
`refNumberIMs` ,
`batchNumber` ,
`format` ,
`remAmount`
)
VALUES ('0',".$transID[0].",".$getAmtQuery['transAmount'].",'$tran_date',".$_SESSION["loggedUserData"]["userID"].",'".$transResolvedStatus."',".$getAmtQuery['refNumberIM'].",'$BatchNumVal','1',".$getAmtQuery['remTransAmount']." )";
debug("log trans");
					debug($logQuery);
					
					insertInto($logQuery);
/**********************End Log Maintain***********************************/
					
					
					debug($updatePay);
					debug($updateTrans);
				}
				else{
					//debug("P Equal to T");
					if($_POST['P_'.$payID[0]]=='on')
							$isResolvedStatus='Y';
						else if(empty($_POST['P_'.$payID[0]]))
							$isResolvedStatus='P';
					if($_POST['T_'.$transID[0]]=='on')
							$transResolvedStatus='Y';
						else if(empty($_POST['T_'.$transID[0]]))
							$transResolvedStatus='P';
					$updateTrans="update transactions set isResolved='Y', resolveDate = '".$tran_date."' ,remTransAmount='0'"; 
					
					$updateTrans.=" , transStatus='Processing'";
					
					$updateTrans.=" where transID=".$transID[0]." ";
					update($updateTrans);
					$getBatchNum=selectFrom('select max(batchNumber) as bn from payments_resolved');
					$BatchNumVal=$getBatchNum['bn'] + 1;
/**********************Start Log Maintain***********************************/
$getAmtQuery=selectFrom('select transAmount,refNumberIM,remTransAmount from transactions where transID='.$transID[0].'');
					$logQuery="INSERT INTO payments_resolved (
`payID` ,
`transID` ,
`agentAmount` ,
`action_date` ,
`resolvedBy` ,
`status` ,
`refNumberIMs` ,
`batchNumber` ,
`format` ,
`remAmount`
)
VALUES ('0',".$transID[0].",".$getAmtQuery['transAmount'].",'$tran_date',".$_SESSION["loggedUserData"]["userID"].",'Y',".$getAmtQuery['refNumberIM'].",'$BatchNumVal','1',".$getAmtQuery['remTransAmount']." )";

					
					insertInto($logQuery);
/**********************End Log Maintain***********************************/
					$updatePay="update barclayspayments set isResolved='Y', remAmount='0' where id=".$payID[0]."";
					update($updatePay);
/******************************Start Log Maintain***********************************/
	$getPayAmtQuery=selectFrom('select amount,remAmount from barclayspayments where id='.$payID[0].'');
						$logQuery="INSERT INTO payments_resolved (
`payID` ,
`transID` ,
`agentAmount` ,
`action_date` ,
`resolvedBy` ,
`status` ,
`refNumberIMs` ,
`batchNumber` ,
`format` ,
`remAmount`
)
VALUES (".$payID[0].",'0',".$getPayAmtQuery['amount'].",'$tran_date',".$_SESSION["loggedUserData"]["userID"].",'Y','null','$BatchNumVal','1',".$getPayAmtQuery['remAmount']." )";
						insertInto($logQuery);


/**********************End Log Maintain***********************************/


					debug($updateTrans);
					debug($updatePay);
				
				}
				}
				elseif($totalTrans==1 && $totalPay>1){ //many to one
				//	debug("Many to One");
					if($totalSumPayAmt>$totalTransPayAmt){
					//	debug("P grter T");
						$updateQ="update transactions set transStatus='processing' where transID=".$transID[0]."";
						$transPayment=$totalTransPayAmt;
						update($updateQ);
					//	debug($updateQ);
						for($i=0;$i<count($payID);$i++){
							$getPayAmount="select amount from barclayspayments where id=".$payID[$i]."";
							$totalPayAmount=selectFrom($getPayAmount);
							$amt=intval($totalPayAmount['amount']);
						//	debug($totalPayAmount);
						//	debug($getPayAmount);
					//		debug($amt);
					//		debug($transPayment);
							$remainingAmt=$transPayment;
							$transPayment=$transPayment-$amt;
					//		debug($transPayment);
							if($transPayment>=0){
								
								$updatePayQ="update barclayspayments set isResolved='Y' where id=".$payID[$i]."";
								update($updatePayQ);
								
							}
							else{
								$newAmt=$amt-$remainingAmt;
								if($_POST['P_'.$payID[$i]]=='on')
								$isResolvedStatus='Y';
								else if(empty($_POST['P_'.$payID[$i]]))
								$isResolvedStatus='P';
								$updatePayQ="update barclayspayments set isResolved='".$isResolvedStatus."',amount=".$newAmt." where id=".$payID[$i]."";
								update($updatePayQ);
								for($j=$i;$j<count($payID);$j++){
									if($_POST['P_'.$payID[$j]]=='on')
										$isResolvedStatus='Y';
									else if(empty($_POST['P_'.$payID[$j]]))
										$isResolvedStatus='P';
									$updatePayQ="update barclayspayments set isResolved='".$isResolvedStatus."' where id=".$payID[$j]."";
									update($updatePayQ);
								}
								$flag=true;
							}
							
							
						//	debug($updatePayQ);
							if($flag)
								break;
						
						}
						
					
					}
					else if($totalSumPayAmt<$totalTransPayAmt){
					//	debug("P less T");
						for($i=0;$i<count($payID);$i++){
							if($_POST['P_'.$payID[$j]]=='on')
									$isResolvedStatus='Y';
							else if(empty($_POST['P_'.$payID[$j]]))
									$isResolvedStatus='P';
							$updateQ="update barclayspayments set isResolved='".$isResolvedStatus."' where id=".$payID[$i]."";
							update($updateQ);
						//	debug($updateQ);
							
						}
						$newTransAmt=$totalTransPayAmt-$totalSumPayAmt;
						$updateTransQ="update transactions set totalAmount=".$newTransAmt." where transID=".$transID[0]."";
						update($updateTransQ);
					//	debug($updateTransQ);
						
					}
					else
					{
						for($i=0;$i<count($payID);$i++){
							$updateQ="update barclayspayments set isResolved='Y' where id=".$payID[$i]."";
							update($updateQ);
						}
						$updateTrans="update transactions set transStatus='processing' where transID=".$transID[0]."";
						update($updateTrans);
					}
				
				}
				elseif($totalTrans>1 && $totalPay==1){ //one to many
				//	debug("One to many");
					if($totalSumPayAmt>$totalTransPayAmt){
					//	debug($totalSumPayAmt);
				//		debug($totalTransPayAmt);
						$newPay=$totalSumPayAmt-$totalTransPayAmt;
						if($_POST['P_'.$payID[$j]]=='on')
									$isResolvedStatus='Y';
						else if(empty($_POST['P_'.$payID[$j]]))
									$isResolvedStatus='P';
									
					$updateQ="update barclayspayments set isResolved='".$isResolvedStatus."',amount=".$newPay." where id=".$payID[0]."";
					update($updateQ);
					//debug($updateQ);
						for($i=0;$i<count($transID);$i++){
							$updateTra="update transactions set transStatus='processing' where transID=".$transID[$i]."";
							//debug($updateTra);
						}
						
					
					}
					elseif($totalSumPayAmt<$totalTransPayAmt){
						//debug("P less T");
						$updateQ="update barclayspayments set isResolved='Y' where id=".$payID[0]."";
						update($updateQ);
						$transPayment=$totalSumPayAmt;
						for($i=0;$i<count($transID);$i++){
							$getTransAmount="select totalAmount from transactions where transID=".$transID[$i]."";
							$totaltransAmount=selectFrom($getTransAmount);
							$amt=intval($totaltransAmount[0]);
							$tempAmt=$transPayment;
							//debug($getTransAmount);
							//debug($totaltransAmount);
						//	debug($transPayment);
						//	debug($amt);
							$transPayment=$transPayment-$amt;
						//	debug($transPayment);
							if($transPayment>=0){
								$updateTransQ="update transactions set transStatus='processing' where transID=".$transID[$i]."";
								update($updateTransQ);
							}
							else{
								$newAmt=$amt-$tempAmt;
								$updateTransQ1="update transactions set totalAmount=".$newAmt." where transID=".$transID[$i]."";
								update($updateTransQ1);
							//	debug($updateTransQ1);
								/*for($j=$i;$j<count($transID);$j++){
									//$updateTransQ="update transactions set transStatus='processing' where transID=".$transID[$j]."";
									//debug($updateTransQ);
								} */
								$flag=true;
							}
							
							if($flag)
								break;
						
						}
							
							
					}
					else
					{
						$updateQ="update barclayspayments set isResolved='Y' where id=".$payID[0]."";
						update($updateQ);
						for($i=0;$i<count($transID);$i++){
							$updateTrans="update transactions set transStatus='processing' where transID=".$transID[$i]."";
							update($updateTrans);
						} 
						
					}
					
				
				}
				elseif($totalTrans>1 && $totalPay>1){//many to many
					//debug("Many to Many");
					if($totalSumPayAmt>$totalTransPayAmt){
						//same as many to one
						for($i=0;$i<count($transID);$i++){
							$updateTrans="update transactions set transStatus='processing' where transID=".$transID[$i]."";
							update($updateTrans);
						//	debug($updateTrans);
					
						}
						
						$transPayment=$totalTransPayAmt;
					for($i=0;$i<count($payID);$i++){
							$getPayAmount="select amount from barclayspayments where id=".$payID[$i]."";
							$totalPayAmount=selectFrom($getPayAmount);
							$amt=intval($totalPayAmount['amount']);
						//	debug($totalPayAmount);
						////	debug($getPayAmount);
						//	debug($amt);
						//	debug($transPayment);
							$remainingAmt=$transPayment;
							$transPayment=$transPayment-$amt;
						//	debug($transPayment);
							if($transPayment>=0){
								$updatePayQ="update barclayspayments set isResolved='Y' where id=".$payID[$i]."";
								update($updatePayQ);
							}
							else{
								$newAmt=$amt-$remainingAmt;
								if($_POST['P_'.$payID[$i]]=='on')
								$isResolvedStatus='Y';
								else if(empty($_POST['P_'.$payID[$i]]))
								$isResolvedStatus='P';
								$updatePayQ="update barclayspayments set isResolved='".$isResolvedStatus."',amount=".$newAmt." where id=".$payID[$i]."";
								update($updatePayQ);
								for($j=$i;$j<count($payID);$j++){
									if($_POST['P_'.$payID[$j]]=='on')
										$isResolvedStatus='Y';
									else if(empty($_POST['P_'.$payID[$j]]))
										$isResolvedStatus='P';
									$updatePayQ="update barclayspayments set isResolved='".$isResolvedStatus."' where id=".$payID[$j]."";
									update($updatePayQ);
								}
								$flag=true;
							}
							
							
							
							if($flag)
								break;
						
						}
				
				}
				elseif($totalSumPayAmt<$totalTransPayAmt){
					for($i=0;$i<count($payID);$i++){
						if($_POST['P_'.$payID[0]]=='on')
							$isResolvedStatus='Y';
						else if(empty($_POST['P_'.$payID[0]]))
							$isResolvedStatus='P';
						$updatePay="update barclayspayments set isResolved='".$isResolvedStatus."' where id=".$payID[$i]."";
						update($updatePay);
						}
						//same as one to many
						$transPayment=$totalSumPayAmt;
					for($i=0;$i<count($transID);$i++){
							$getTransAmount="select totalAmount from transactions where transID=".$transID[$i]."";
							$totaltransAmount=selectFrom($getTransAmount);
							$amt=intval($totaltransAmount[0]);
							$tempAmt=$transPayment;
						//	debug($getTransAmount);
						//	debug($totaltransAmount);
						//	debug($transPayment);
						//	debug($amt);
							$transPayment=$transPayment-$amt;
						//	debug($transPayment);
							if($transPayment>=0){
								$updateTransQ="update transactions set transStatus='processing' where transID=".$transID[$i]."";
						//		debug($updateTransQ);
								update($updateTransQ);
							}
							else{
								$newAmt=$amt-$tempAmt;
								$updateTransQ1="update transactions set totalAmount=".$newAmt." where transID=".$transID[$i]."";
								update($updateTransQ1);
							//	debug($updateTransQ1);
								/*for($j=$i;$j<count($transID);$j++){
									//$updateTransQ="update transactions set transStatus='processing' where transID=".$transID[$j]."";
									//debug($updateTransQ);
								} */
								$flag=true;
							}
							
							if($flag)
								break;
						
						}
					
					
				
				}
				else{
					for($i=0;$i<count($payID);$i++){
							$updateTrans="update barclayspayments set description='updated during reconciliation' where id=".$payID[$i]."";
						//	debug($updateTrans);
							update($updateTrans);
						}
					for($j=0;$j<count($transID);$j++){
							$updateTrans="update transactions set transStatus='processing' where transID=".$transID[$j]."";
						///	debug($updateTrans);
							update($updateTransQ1);
						}
				
				
				}
		
	}
		//debug('as');
		//header('Location:unres-payments-ajax-fx-new.php?type=1');

	}
	if($_REQUEST['getCustomersList']==true){
		
		$transID=explode(',',$_REQUEST['transIDs']);
		$payID=explode(',',$_REQUEST['payIDs']);
		$data='<form name="completeForm" id="completeForm" action="unres-payments-action-fx-new.php" method="POST">';
		for($i=0;$i<count($payID);$i++){
			$getDet=selectFrom("select currency,description,amount  from barclayspayments where id=".$payID[$i]."");
			$data.='<tr><td>'.$getDet['amount'].' '.$getDet['currency'].' '.$getDet['description'].'<input type="checkbox" name="P_'.$payID[$i].'" id="'.$payID[$i].'" checked=checked>
			<input type="hidden" name="P_UN'.$payID[$i].'" id="UN'.$payID[$i].'" value='.$payID[$i].'>
			</td></tr>';
		
		}
		for($i=0;$i<count($transID);$i++){
			$getDetTran=selectFrom("select refNumberIM,totalAmount,currencyFrom  from transactions where transID=".$transID[$i]."");
			$data.='<tr><td>'.$getDetTran['refNumberIM'].' '.$getDetTran['totalAmount'].' '.$getDetTran['currencyFrom'].'<input type="checkbox" name="T_'.$transID[$i].'" id="'.$transID[$i].'"  checked=checked> 
			<input type="hidden" name="T_UN'.$transID[$i].'" id="UN'.$transID[$i].'"  value='.$transID[$i].'> 
			</td></tr>';
		}
		$data.='<input type="submit" name="complete" id="complete" value="Complete" >';
		$data.='<input type="submit" name="cancel" id="cancel" value="Cancel" >';
		$data.='</form>';
		echo $data;	
		
		}
	//exit();
if(CONFIG_TRANS_ROUND_NUMBER == "1" && defined('CONFIG_TRANS_ROUND_LEVEL'))
{
	 $roundLevel1 = $roundLevel2 = CONFIG_TRANS_ROUND_LEVEL;
}else{
	 $roundLevel1 = 2;
	 $roundLevel2 = 4;	 
}

	$response = new Services_JSON();
	$page = $_REQUEST['page']; // get the requested page 
	$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord = $_REQUEST['sord']; // get the direction 
	
	if(!$sidx && $_REQUEST['getGrid']!='transList')
	{
		$sidx = 'importedOn DESC, id ';
	}elseif(!$sidx)
	{
		$sidx = 1;
	}
	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
			
	if($limit == 0)
	{
		$limit=50;
	}
	
	$SubmitJS		= "";
	$fromJS     	= "";
	$toJS			= "";
	$refNumberJS	="";
	$amount1JS		= "";
	$descJS	= "";

if($_REQUEST["btnAction"] == "Delete")
{
	$payIDArray		= $_REQUEST["payIDs"];
	for ($d=0;$d < count($payIDArray);$d++)
	{
		if(isset($payIDArray[$d]))
		{
			update("update barclayspayments set isDeleted = 'Y' where id = '".$payIDArray[$d]."'");
			$descript="Deleted record for Barclays Payment by ". $_SESSION["loggedUserData"]["username"];;
			activities($_SESSION["loginHistoryID"],"UPDATE",$payIDArray[$d],'barclayspayments',$descript);
			if(CONFIG_IMPORT_PAYMENT_AUDIT=="1"){
				$query_pay_action = "
								INSERT INTO payment_action(payID,action_id)
											values('".$payIDArray[$d]."', '".$_SESSION["loggedUserData"]["userID"]."')";
				insertInto($query_pay_action);
			}				
			$err = "";
		}
	}

	if(trim($err) == "")
	{
		$err = "Payment(s) deleted successfully.";
	}
	// Number of iteration should be equal to the number of  transactions
		echo "<input type='hidden' name='payListDelete' id='payListDelete' value='".$err."'>";
		exit;
}
if($_REQUEST["btnAction"] == "Resolve")
{

//\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\
	$successResolved = false;
	$transIDArray	= $_REQUEST["transIDs"];
	$payIDArray		= $_REQUEST["payIDs"];
	if(count($transIDArray)==1 && count($payIDArray)==1){
		$format=1;
	}
	else if(count($transIDArray)>1 && count($payIDArray)==1){
		$format=2;
	}
	else if(count($transIDArray)==1 && count($payIDArray)>1){
		$format=3;
	}
	else if(count($transIDArray)>1 && count($payIDArray)>1){
		$format=4;
	}
	$batchNum=selectFrom("select max(batchNumber) as bn from payments_resolved");
	$intBatchNumber=$batchNum['bn'];
	$intBatchNumber++;

	if(count($transIDArray)>0 && is_array($transIDArray)){
		for($s=0;$s<count($transIDArray);$s++)	{

			$transID = $transIDArray[$s];
			$transRS = selectFrom("SELECT transID,totalAmount,transAmount,bankCharges,refNumberIM,benAgentID,custAgentID,customerID,AgentComm,currencyTo,currencyFrom,createdBy FROM transactions where transID='".$transID."'");
			
			//debug($transRS);
			$transID = $transRS["transID"];	
			$totalAmount = $transRS["totalAmount"];
			$transAmount = $transRS["transAmount"];
			$bankCharges = $transRS["bankCharges"];
			$imReferenceNumber = $transRS["refNumberIM"];
			$benAgentID = $transRS["benAgentID"];
			$custAgentID = $transRS["custAgentID"];											
			$customerID = $transRS["customerID"];
			$agentComm = $transRS["AgentComm"];
			$currencyTo = $transRS["currencyTo"];
			$currencyFrom = $transRS["currencyFrom"];
			$transCreatedBy = $transRS["createdBy"];
			$modifyby = $agentID; // to keep track of logged user ID in modify column
			$amount = $totalAmount;
			$payinFlag = true;
			/*******
				#5084 - AMB Exchange
				This block below is to check either functionality is PayinBook or not Also
				if PayinBook then Customer's Ledgers should also be affected alongwith
				Payin Agent either All or assigned in CONFIG_PAYIN_AGENT_NUMBER.
			*******/
			if(CONFIG_PAYIN_CUST_AGENT == "1" && CONFIG_PAYIN_CUSTOMER=="1"){
				if(CONFIG_PAYIN_CUST_AGENT_ALL=="1"){
					$ledgerAgent = $custAgentID;
				}
				elseif(defined("CONFIG_PAYIN_AGENT_NUMBER") && CONFIG_PAYIN_AGENT_NUMBER!="0"  && CONFIG_PAYIN_CUST_AGENT_ALL!="1"){
					$ledgerAgent = CONFIG_PAYIN_AGENT_NUMBER;
				}
				$agentPayinContentsT = selectFrom("select userID from " . TBL_ADMIN_USERS . " where userID = '".$ledgerAgent."'");
				$agentPayinID = $agentPayinContentsT["userID"];
				if($agentPayinID=="" && $transCreatedBy!="CUSTOMER")
					$payinFlag = false;
			}
			elseif($transCreatedBy!="CUSTOMER"){
					$payinFlag = false;
			}
			if($_REQUEST["userTypes"]  == "customer" && $payinFlag)
			{
				if($transCreatedBy == "CUSTOMER" && isExist("select c_id from cm_customer where c_id ='".$customerID."'"))
				{
					$strQuery = "SELECT Balance,c_id FROM cm_customer where c_id = '".$customerID."'";
					$rstRow = selectFrom($strQuery);
					$Balance = $rstRow["Balance"]+$amount;	
					update("update cm_customer set Balance = '".$Balance."' where c_id = '".$customerID."'");
					$agentLedgerAmount = $amount;
					$tran_date = date("Y-m-d");
					
					/**
					 * #5677
					 * Reconciling REV(Reveersal entry into customer ledger if exists with same amount having "-" sign)
					 * Enable = "1"
					 * Disable = "0"
					 * by Aslam Shahid 
					 */
					$inputArgsRev = array("userTypes"=>$_REQUEST["userTypes"],"amount"=>$amount,"transDate"=>$tran_date,"type"=>"WITHDRAW",
										  "transRefNo"=>$imReferenceNumber,"customerID"=>$customerID,"modifiedBy"=>$modifyby,"note"=>$note,
										  "tableCustomer"=>"customer_account"
										 );
					$retInputArgsRevC = gateway("CONFIG_UNRESOLVED_REVERSAL_ENTRY",$inputArgsRev,CONFIG_UNRESOLVED_REVERSAL_ENTRY);
					
					insertInto("insert into  customer_account (customerID ,Date ,tranRefNo,payment_mode,Type ,amount,note ) 
								 values('".$customerID."','".$tran_date."','".$imReferenceNumber."','Payment by Bank','DEPOSIT','".$agentLedgerAmount."','".$note."'
								 )");
									
					if(CONFIG_EXACT_BALANCE == "1")
					{
						if($Balance == $totalAmount)
						{
							$conditionFlag = 1;
						}else{
							$conditionFlag = 0;
						}
					}else{
						if($Balance >= $totalAmount)
						{
							$conditionFlag = 1;
						}else{
							$conditionFlag = 0;
						}
					}								
				
					if($conditionFlag)// >= $transAmount)
					{	
						$tran_date = date("Y-m-d");	
						/**************************************************************
						 * The puporse of this config (CONFIG_VERIFY_TRANSACTION_ENABLED) when user resolve payment then it will go in processing status
						 * In processiing status ledger not affect.
						 * Added by Niaz Ahmad at 03-08-2009 @5256- Minascenter
											
						*****************************************************************///
						if(CONFIG_VERIFY_TRANSACTION_ENABLED == "1")
						{													
							update("update transactions set transStatus  = 'Processing',isResolved  = 'Y'  , verificationDate = '".$tran_date."' , resolveDate = '".$tran_date."' where transID = '".$transID."'");
						}else{
						
							update("update transactions set transStatus  = 'Authorize' , authoriseDate = '".$tran_date."' where transID = '".$transID."'");
	
							$checkBalance2 = selectFrom("select agentType, balance, isCorrespondent from ".TBL_ADMIN_USERS." where userID = '".$benAgentID."'");
							
							if($checkBalance2["agentType"] == 'Sub')
							{
								updateSubAgentAccount($benAgentID, $transAmount, $transID, "WITHDRAW", "Transaction Authorized", "Distributor");
								updateAgentAccount($benAgentID, $transAmount, $transID, "WITHDRAW", "Transaction Authorized", "Distributor");
							}else{
								updateAgentAccount($benAgentID, $transAmount, $transID, "WITHDRAW", "Transaction Authorized", "Distributor");
							}
					  }
					}
				}
				elseif($transCreatedBy != "CUSTOMER"  && isExist("select customerID from customer where customerID ='".$customerID."'") && $payinFlag)
				{
					/*					
					$queryResolve = "update barclayspayments set isResolved = 'Y', username='$uname', paymentFrom = 'agent customer' where id = '".$_REQUEST["allIds"][$i]."'";
					update($queryResolve);
			
					$strQuery = "SELECT * FROM barclayspayments where username = '$uname' and id = '".$_REQUEST["allIds"][$i]."'";
					
					$Balance = 0;
					$nResultBarkleyTable = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
					while($rstRowBT = mysql_fetch_array($nResultBarkleyTable))	
					*/			
					$Balance = $amount;
					$rstRow = selectFrom("SELECT * FROM customer where customerID = '".$customerID."'");
					$tran_date = date("Y-m-d");
					$agentLedgerAmount = $amount;
					/**
					 * #5677
					 * Reconciling REV(Reveersal entry into customer ledger if exists with same amount having "-" sign)
					 * Enable = "1"
					 * Disable = "0"
					 * by Aslam Shahid 
					 */
					$inputArgsRev = array("userTypes"=>$_REQUEST["userTypes"],"amount"=>$agentLedgerAmount,"transDate"=>$tran_date,"type"=>"WITHDRAW",
										  "transRefNo"=>"","customerID"=>$customerID,"modifiedBy"=>$modifyby,"note"=>$note,
										  "tableCustomer"=>"agents_customer_account"
										 );
										 
					$retInputArgsRev = gateway("CONFIG_UNRESOLVED_REVERSAL_ENTRY",$inputArgsRev,CONFIG_UNRESOLVED_REVERSAL_ENTRY);
/*					if(CONFIG_UNRESOLVED_REVERSAL_ENTRY=="1"){
						$queryResolve = "SELECT id FROM barclayspayments WHERE isResolved = 'N' AND amount='-".$amount."' AND tlaCode='REV' ORDER BY id DESC ";
						$queryResolveRS = selectFrom($queryResolve);
						if($queryResolveRS["id"]!=""){
							update("update barclayspayments 
										set isResolved = 'Y', 
											username='', 
											paymentFrom = '".($_REQUEST["userTypes"]!="agent"?"customer":"agent")."' 
										where id = '".$queryResolveRS["id"]."' ");
							insertInto("insert into  agents_customer_account (customerID ,Date,tranRefNo ,payment_mode,Type ,amount, modifiedBy) 
									 values('".$customerID."','".$tran_date."','','Payment by Bank','WITHDRAW','".$amount."','".$modifyby."'
									 )");
						}
					}*/
					update("insert into  agents_customer_account (customerID ,Date,tranRefNo ,payment_mode,Type ,amount, modifiedBy) 
									 values('".$customerID."','".$tran_date."','','Payment by Bank','DEPOSIT','".$agentLedgerAmount."','".$modifyby."'
									 )");

					if(CONFIG_VERIFY_TRANSACTION_ENABLED == "1")
					{													
				   	 update("update transactions set transStatus  = 'Processing' , verificationDate = '".$tran_date."',isResolved  = 'Y' , resolveDate= '".$tran_date."' where transID = '".$transID."'");
					}else{			
					
					if(CONFIG_PAYIN_CUST_AGENT == '1')
					{
						$agentPayinContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".CONFIG_PAYIN_AGENT_NUMBER."'");
						if($agentPayinContents["agentType"] == 'Sub')
						{
							updateSubAgentAccount($agentPayinContents["userID"], $agentLedgerAmount, $transID, 'DEPOSIT', "Barclays Payment", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
							$q = updateAgentAccount($agentPayinContents["parentID"], $agentLedgerAmount, $transID, 'DEPOSIT', "Barclays Payment", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
						}else{
							$q = updateAgentAccount($agentPayinContents["userID"], $agentLedgerAmount, $transID, 'DEPOSIT', "Barclays Payment", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
						}	
					}
					///////////Balance Condition//////////	
					// to Authorize trasaction in any case;
				
					if(CONFIG_EXACT_BALANCE == "1")
					{
						if($Balance == $totalAmount)
						{
							$conditionFlag = 1;
						}else{
							$conditionFlag = 0;
						}
						}else{
							if($Balance >= $totalAmount)
							{
								$conditionFlag = 1;
							}else{
								$conditionFlag = 0;
						}
					}								
															
					///////$Balance >= $totalAmount///// Done on requirement of Express
														
					if($conditionFlag)
					{	
						/*$strQuery = "select * from transactions  where customerID = $CustomerID and transStatus  = 'Pending' and transAmount = $transAmount and createdBy != 'CUSTOMER'";
						$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
						$rstRows = mysql_fetch_array($nResult);
						$imReferenceNumber = $rstRows["refNumberIM"];
						$totalAmount = $rstRows["totalAmount"];*/
						$date_time = date("Y-m-d H:i:s");
						update("update transactions set transStatus  = 'Authorize' , authoriseDate = '".$date_time."' where transID = '".$transID."'");
						/*$strQuery = "insert into  agent_customer_account (customerID ,Date,tranRefNo ,payment_mode,Type ,amount ) 
										 values('".$CustomerID."','".$tran_date."','$imReferenceNumber','Payment by Bank','WITHDRAW','".$totalAmount."'
										 )";	
					
						$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());	
						*/																														
					}		
					else
					{}
				  }	
				}
				else
				{
					$err = "Error occured while processing some records\n(Payin Agent is not specified properly OR select Agent from Search Type dropdown\nand give Agent Name for Simple Agent transaction Reconcile.)";
				}
				
			}
			
			elseif($_REQUEST["userTypes"] == "agent" || $_REQUEST["userTypes"] == "distributor"  || !$payinFlag)
			{
				if(CONFIG_VERIFY_TRANSACTION_ENABLED == "1")
				{													
					update("update transactions set transStatus  = 'Processing' , verificationDate = '".$tran_date."',isResolved  = 'Y' ,resolveDate= '".$tran_date."' where transID = '".$transID."'");
				}
				else
				{
				$custAgentR = selectFrom("select username from " . TBL_ADMIN_USERS . " where userID='".$custAgentID."'");
				$uname = $custAgentR["username"];
				if($uname!="")
				{
					$type = "DEPOSIT";
					$description = "Barclays Payment";
					
					$contantagentID = selectFrom("select userID,balance,limitUsed,agentAccountLimit,isCorrespondent from " . TBL_ADMIN_USERS . " where username ='".$uname."'");
					$agentID = $contantagentID["userID"];
					$agentlimitUsed	= $contantagentID["limitUsed"];
					$agentLimit	= $contantagentID["agentAccountLimit"];
					$agentBalance	= $contantagentID["balance"];
					$paymentFrom = "agent";
					
					$Balance = $agentBalance + $amount;
					
					if(CONFIG_AnD_ENABLE == '1' && $contantagentID["isCorrespondent"] == 'Y')
					{
						$isAgentAnD = 'Y';
					}
					
					update("update " . TBL_ADMIN_USERS . " set balance  = '".$Balance."' where userID = '".$agentID."'");
					if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == '1' && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD != "1")
					{
						/* Changing the localAmount to totalAmount as bug found via ticket #3829 */
						$amount = $totalAmount;
						$currencyFrom = $currencyTo;
					}else{
						$amount = $amount;
						$currencyFrom = $currencyFrom;
					}
					$agentLedgerAmount = $amount;
					if($isAgentAnD == 'Y')
					{
						$queryAgent = "insert into ".TBL_AnD_ACCOUNT." (agentID, dated, amount, type, modified_by, description,TransID,actAs,currency) values('".$agentID."','".getCountryTime(CONFIG_COUNTRY_CODE)."','".$agentLedgerAmount."','".$type."', '".$modifyby."','".$description."','".$transID."','Agent','".$currencyFrom."')";
					}else{
						if($contantagentID["agentType"] == 'Sub')
						{
							updateSubAgentAccount($agentID, $agentLedgerAmount, $transID, $type, $description, "Agent",$note,$currencyFrom);
							updateAgentAccount($agentID, $agentLedgerAmount, $transID, $type, $description, "Agent",$note,$currencyFrom);
							
						}else{
							updateAgentAccount($agentID, $agentLedgerAmount, $transID, $type, $description, "Agent",$note,$currencyFrom);
						}
					}

						$amountInHand = $agentBalance + $amount;
						if(CONFIG_AGENT_LIMIT == "1")
						{
						$agentValidBalance = $amountInHand + $agentLimit;///Agent Limit is also used
						}else{
						$agentValidBalance = $amountInHand;///Agent Limit is not used
						}
						
						///
						if(CONFIG_EXCLUDE_COMMISSION == '1')
						{
							$agentLedgerAmount = $totalAmount - $agentComm;
						}else{
							$agentLedgerAmount = $totalAmount;
						}
						///
								
						update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='".$uname."', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='".$transID."'");
						update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='".$uname."', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='".$transID."'");
						
						if($agentValidBalance >= $agentLedgerAmount)
						{	//////////////////If Balance is enough
							
							$queryCust = "select payinBook  from ".TBL_CUSTOMER." where customerID ='" . $customerID . "'";
							$custContents = selectFrom($queryCust);
							
							if($custContents["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")///////If Not a payin book customer
							{	
								$Status	= "Authorize";
								$tran_date = date("Y-m-d");	
								
								$amountInHand = $amountInHand - $agentLedgerAmount;			
								
								if(CONFIG_LEDGER_AT_CREATION != "1")
								{
									
									if($isAgentAnD == 'Y')
									{
										$insertQuery = "insert into ".TBL_AnD_ACCOUNT." (agentID, dated, type, amount, modified_by, TransID, description,currency) values('".$agent."', '".$tran_date."', 'WITHDRAW', '".$agentLedgerAmount."', '".$modifyby."', '". $transID."', 'Transaction Authorized','".$currencyFrom."')";
									}else{
										$insertQuery = "insert into agent_account (agentID, dated, type, amount, modified_by, TransID, description,note,currency) values('".$agentID."', '".$tran_date."', 'WITHDRAW', '".$agentLedgerAmount."', '".$modifyby."', '". $transID."', 'Transaction Authorized','".$note."','".$currencyFrom."')";
									}
									$q=mysql_query($insertQuery);//////Insert into Agent Account
									update("update " . TBL_ADMIN_USERS . " set balance  = $amountInHand where userID = '".$agentID."'");											
									agentSummaryAccount($agentID, "WITHDRAW", $agentLedgerAmount);	
								}
							////////////////////Auto Authorization//////////
							/******
								#5160
								Distributors ledgers are affected in different cases like here we checked
								that when CONFIG_AUTO_AHTHORIZE is ON then in some clients on the basis of this
								config Ledgers of distributors are WITHDRAWN.
							******/
							if(CONFIG_AUTO_AHTHORIZE == "1")
							{
									$checkBalance2 = selectFrom("select balance, isCorrespondent from ".TBL_ADMIN_USERS." where userID = '".$benAgentID."'");						
									$agentLedgerAmount = $transAmount;
								if(CONFIG_AnD_ENABLE == '1' && $checkBalance2["isCorrespondent"] == 'Y')
								{
									$isBankAnD = 'Y';
								}
								$currentBalanceBank = $checkBalance2["balance"]	- $transAmount;				
								update("update " . TBL_ADMIN_USERS . " set balance  = '".$currentBalanceBank."' where userID = '".$benAgentID."'");											
								if($isBankAnD == 'Y')
								{
									insertInto("insert into ".TBL_AnD_ACCOUNT." (agentID, dated, type, amount, modified_by, TransID, description, actAs,currency) values('".$benAgentID."', '".$tran_date."', 'WITHDRAW', '".$agentLedgerAmount."', '".$modifyby."', '".$transID."', 'Transaction Authorized','Distributor','".$currencyTo."')");
								}else{
									$agentLedgerAmount = (DEST_CURR_IN_ACC_STMNTS == "1" ? $localAmount : $transAmount);
									insertInto("insert into bank_account (bankID, dated, type, amount, currency, modified_by, TransID, description) values('".$benAgentID."', '".$tran_date."', 'WITHDRAW', '".$agentLedgerAmount."', '".(DEST_CURR_IN_ACC_STMNTS == "1" ? $currencyTo : $currencyFrom)."', '".$modifyby."', '".$transID."', 'Transaction Authorized')");		
								}
								agentSummaryAccount($benAgentID, "WITHDRAW", $agentLedgerAmount);
							}
						}
					//update("update " . TBL_ADMIN_USERS . " set balance  = $amountInHand where userID = '$agentID'");
					}
					
					/////////////////////////
				}
				else
				{
					$err = "Error occured while processing some records";
				}
			  }	  // end processing trans status if condition
			}
			$tran_date = date("Y-m-d");	
			if(trim($err) == "")
			{
				$err = "Payments resolved successfully.";
				$successResolved = true;
				
				//redirect("unres-payments.php?type=" . $_GET["type"] . "&msg=" . urlencode($err));
			}			
		}
	}  
	// Number of iteration should be equal to the number of  transactions
			
		if($_REQUEST["userTypes"]!="" && $transID!="" && $successResolved){
			$counter = 0;
			for($b=0;$b<count($payIDArray);$b++){
				$queryResolve = "update barclayspayments set isResolved = 'Y', username='', paymentFrom = '".($_REQUEST["userTypes"]!="agent"?"customer":"agent")."' where id = '".$payIDArray[$b]."' ";
				update($queryResolve);
				
			
				for($c=0;$c<count($transIDArray);$c++){
					if($c == 0){
					$transiMno=$transIDArray['refNumberIM'];
					}
					else
						$transiMno  .="/".$transIDArray['refNumberIM'];
					$transRS = selectFrom("SELECT transID,totalAmount,transAmount,bankCharges,refNumberIM,benAgentID,custAgentID,customerID,AgentComm,currencyTo,currencyFrom,createdBy FROM transactions where transID='".$transIDArray[$c]."'");
//					debug($transRS,true);
					if(CONFIG_IMPORT_PAYMENT_AUDIT=="1"){
						$query_pay_action = "
										INSERT INTO payment_action(payID,transID,action_id,agentAmount)
													values('".$payIDArray[$b]."', '".$transIDArray[$c]."', '".$_SESSION["loggedUserData"]["userID"]."','".$transRS['totalAmount']."')";
						if($transIDArray[$c]>0){							
							insertInto($query_pay_action);
							}
					}
				}
				if(count($transIDArray)>0){
					if(empty($transIDArray[$b])) 
							$transIDArray[$b] = $transIDArray[$b-1]; 
					 
					if(empty($payIDArray[$b])) 
							$payIDArray[$b] = $payIDArray[$b-1];     
					 
					$transVal = selectFrom("select refNumberIM ,totalAmount  from transactions where transID = '".$transIDArray[$b]."'"); 
					//debug("select refNumberIM ,totalAmount  from transactions where transID = '".$transIDArray[$b]."'"); 
					//debug($transVal); 
					 
					$query_pay_resolved = "INSERT INTO  payments_resolved ( 
		 payID, transID,agentAmount,resolvedBy,refNumberIMs,batchNumber,format ) VALUES
('".$payIDArray[$b]."','".$transIDArray[$b]."','".$transVal["totalAmount"]."','".$_SESSION["loggedUserData"]["userID"]."','".$transVal["refNumberIM"]."','".$intBatchNumber."','".$format."')"; 
							 
							
							insertInto($query_pay_resolved);
				
					$descript="Resolved record of Barclays Payment for transactions ".$transiMno;
				activities($_SESSION["loginHistoryID"],"UPDATE", $payIDArray[$b],'barclayspayments',$descript);
				}
				else{
					$descript="Resolved record for Barclays Payment for transaction ".$imReferenceNumber;
					activities($_SESSION["loginHistoryID"],"UPDATE", $payIDArray[$b],'barclayspayments',$descript);
				}
				$counter = $counter+1;
			}
		}
	//	debug($payIDArray);
		echo "<input type='hidden' name='payListResolve' id='payListResolve' value='".$err."'>";
		exit;
}
	
if($_REQUEST["type"] == 1)
{//tlaCode = 'REM'
	$query = "select * from barclayspayments where  ((isResolved='N' and isDeleted='N') OR isResolved='P') ";
}elseif($_REQUEST["type"] == 2)
{
	$query = "select * from natwestpayments where isResolved='N' and isDeleted='N' ";
}
	
if($_REQUEST["Submit"] == "SearchPay")
{
	if ($_REQUEST["from"]!="")
		$fromJS = $_REQUEST["from"];
	if ($_REQUEST["to"]!="")
		$toJS = $_REQUEST["to"];
	if ($_REQUEST["amount1"]!="")
		$amount1JS = $_REQUEST["amount1"];
	if ($_REQUEST["desc"]!="")
		$descJS = $_REQUEST["desc"];
	if ($_REQUEST["currencyPay"]!="")
		$currencyP = $_REQUEST["currencyPay"];
	if ($fromJS != "")
	{
		$dDateF = explode("/",$fromJS);
		if(count($dDateF) == 3)
			$fromJS = $dDateF[2]."-".$dDateF[1]."-".$dDateF[0];
	}
	if ($toJS != "")
	{
		$dDateT = explode("/",$toJS);
		if(count($dDateT) == 3)
			$toJS = $dDateT[2]."-".$dDateT[1]."-".$dDateT[0];
	}
	if(!empty($_REQUEST["type"]))
	{
		if ($fromJS != "" || $toJS != "")
		{
			if ($fromJS != "" && $toJS != "")
			{
				$queryDated = " and (importedOn >= '$fromJS 00:00:00' and importedOn <= '$toJS 23:59:59') ";	
			}
			elseif ($fromJS != "")
			{
				$queryDated = " and (importedOn >= '$fromJS 00:00:00') ";	
			}
			elseif ($toJS != "")
			{
				$queryDated = " and (importedOn <= '$toJS 23:59:59') ";	
			}	
		}
		if ($amount1JS != "")
		{
			$queryAmount = " and amount = '$amount1JS' ";	
		}
		if ($descJS != "")
		{
			$queryDesc = " and (description like '%$descJS%'  or unResReason like '%$descJS%')";	
		}	
	}
	if ($queryDated != "")
	{
		$query .= $queryDated;
	}
	if ($queryrefNumber != "")
	{
		$query .= $queryrefNumber;
	}
	if ($queryAmount != "")
	{
		$query .= $queryAmount;
	}
	if ($queryDesc != "")
	{
		$query .= $queryDesc;	
	}
	if ($currencyP != "")
	{
		$query .= " and currency = '".$currencyP."'";
	}
}

//debug($query);
$queryT = "select transID,totalAmount,refNumberIM,transDate,customerID,createdBy,senderBank,currencyFrom,currencyTo,remTransAmount from ". TBL_TRANSACTIONS." where transStatus = 'Pending' AND transType='Bank Transfer' AND (isResolved='N' OR isResolved='P') ";
if($_REQUEST["Submit"] == "SearchTrans")
{
	if ($_REQUEST["fromT"]!="")
		$fromTJS = $_REQUEST["fromT"];
	if ($_REQUEST["toT"]!="")
		$toTJS = $_REQUEST["toT"];
	if ($_REQUEST["amountT"]!="")
		$amountTJS = $_REQUEST["amountT"];
	if ($_REQUEST["searchBy"]!="")
		$searchByTJS = $_REQUEST["searchBy"];
	if ($_REQUEST["searchName"]!="")
		$searchNameT = $_REQUEST["searchName"];
	if ($_REQUEST["currencyTrans"]!="")
		$currencyT = $_REQUEST["currencyTrans"];
	if ($_REQUEST["paymentMod"]!="")
		$paymentMod = $_REQUEST["paymentMod"];
		
	if ($fromTJS != "")
	{
		$dDateFT = explode("/",$fromTJS);
		if(count($dDateFT) == 3)
			$fromTJS = $dDateFT[2]."-".$dDateFT[1]."-".$dDateFT[0];
	}
	if ($toTJS != "")
	{
		$dDateTT = explode("/",$toTJS);
		if(count($dDateTT) == 3)
			$toTJS = $dDateTT[2]."-".$dDateTT[1]."-".$dDateTT[0];
	}
	if ($fromTJS != "" || $toTJS != "")
	{
		if ($fromTJS != "" && $toTJS != "")
		{
			$queryDatedT = " and (transDate >= '$fromTJS 00:00:00' and transDate <= '$toTJS 23:59:59') ";	
		}
		elseif ($fromTJS != "")
		{
			$queryDatedT = " and (transDate >= '$fromTJS 00:00:00') ";	
		}
		elseif ($toTJS != "")
		{
			$queryDatedT = " and (transDate <= '$toTJS 23:59:59') ";	
		}	
	}
	if ($searchNameT != "")
	{
		if($searchByTJS=="customer"){
			$searchCustQ = selectFrom("select transID from ".TBL_TRANSACTIONS." where refNumberIM='".$searchNameT."'");
			$searchID = $searchCustQ["transID"];
			$querysearchNameT = " and transID = '".$searchID."' "; 
		}
		elseif($searchByTJS=="agent"){
			$searchAgentQ = selectFrom("select userID from ".TBL_ADMIN_USERS." where username='".$searchNameT."'");
			$searchID = $searchAgentQ["userID"];
			$querysearchNameT = " and custAgentID = '".$searchID."' "; 
		}
		elseif($searchByTJS=="distributor"){
			$searchDistQ = selectFrom("select userID from ".TBL_ADMIN_USERS." where username='".$searchNameT."'");
			$searchID = $searchDistQ["userID"];
			$querysearchNameT = " and benAgentID = '".$searchID."' "; 
		}
		elseif($searchByTJS=="sender"){
			$searchNameExp=explode(' ',$searchNameT);
			$n=count($searchNameExp);
			$searchDistQ = selectFrom("select customerID from customer where customerName like '%".$searchNameExp[0]."%' or customerName like '%".$searchNameExp[0]."%' ");
			$searchID = $searchDistQ["customerID"];
			$querysearchNameT = " and customerID = '".$searchID."' "; 
		}

	}
	if ($amountTJS != "")
	{
		$queryAmountT = " and totalAmount = '$amountTJS' ";	
	}
	if ($queryDatedT != "")
	{
		$queryT .= $queryDatedT;
	}
	if ($querysearchNameT != "")
	{
		$queryT .= $querysearchNameT;
	}
	if ($queryAmountT != "")
	{
		$queryT .= $queryAmountT;
	}
	if ($currencyT != "")
	{
		$queryT .= " and currencyFrom = '".$currencyT."'";
	}
	if ($paymentMod != "")
	{
		$queryT .= " and moneyPaid = '".$paymentMod."'";
	}
}
/*$contents = selectMultiRecords($query);
$allCount = countRecords($queryCnt);*/
	//$allCount = countRecords($queryCnt);
	//$count = $allCount;
if($_REQUEST["getGrid"] == "payList"){
	
	if ($_REQUEST["from"]=="")
  $fromTJS = date('Y-m-d');
 if ($_REQUEST["to"]=="")
  $toTJS = date('Y-m-d');
 if($_REQUEST["Submit"] != "SearchPay"){
  $queryDatedT = " and (importedOn >= '$fromTJS 00:00:00' and importedOn <= '$toTJS 23:59:59') ";
  $query.=$queryDatedT;
 }
	//debug($query);
	$result = mysql_query($query) or die(__LINE__.": ".mysql_query());
	$count = mysql_num_rows($result);
	
	//debug($_REQUEST);
	
	
	if($count > 0)
	{
		$total_pages = ceil($count / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 *
	 * @author: Waqas Bin Hasan
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	$query .= " order by $sidx $sord LIMIT $start , $limit";
	
	$result = mysql_query($query) or die(__LINE__.": ".mysql_error());

	
	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;

	$i=0;
	
	while($row = mysql_fetch_array($result))
	{
		$response->rows[$i]['id'] = $row["id"];
/*		$bankNameSql = "select bankName from bankDetails where transID = ".$row["transID"];
		$bankNameData = selectFrom($bankNameSql);*/
		
		$response->rows[$i]['cell'] = array(
									dateFormat($row["importedOn"], "2"),
									customNumberFormat($row["amount"],$roundLevel1,''),
									customNumberFormat($row["remAmount"],$roundLevel1,''),
									$row["currency"],
									$row["tlaCode"],
									$row["description"]
								);
		$i++;
	}
	
	echo $response->encode($response); 

}	

if($_REQUEST["getGrid"] == "transList"){
	
	if ($_REQUEST["fromT"]=="")
  $fromTJS = date('Y-m-d');
 if ($_REQUEST["toT"]=="")
  $toTJS = date('Y-m-d');
 if($_REQUEST["Submit"] != "SearchPay"){
  $queryDatedT = " and (transDate >= '$fromTJS 00:00:00' and transDate <= '$toTJS 23:59:59') ";
  $queryT.=$queryDatedT;
 }
	
//echo $queryT;
	$resultT = mysql_query($queryT) or die(__LINE__.": ".mysql_query());
	$countT = mysql_num_rows($resultT);
	
	 //debug($_REQUEST);
	//debug($queryT);

	if($countT > 0)
	{
		$total_pages = ceil($countT / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 *
	 * @author: Waqas Bin Hasan
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	$queryT .= " order by $sidx $sord LIMIT $start , $limit";
	$resultT = mysql_query($queryT) or die(__LINE__.": ".mysql_error());

	

	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $countT;

	$i=0;
	
	while($rowT = mysql_fetch_array($resultT))
	{
		$response->rows[$i]['id'] = $rowT["transID"];
		if($rowT["createdBy"]!="CUSTOMER"){
			$custQ = "select accountName,firstName,lastName from ".TBL_CUSTOMER." where customerID = '".$rowT["customerID"]."'";
			
			
			$custRS = selectFrom($custQ);
			$custNumber = $custRS["accountName"];
			$custName = $custRS["firstName"]." ".$custRS["lastName"];
			
			//debug($custName);
			
		}
		$senderBankRS   = selectFrom("select branchAddress from banks where bankId ='".$rowT["senderBank"]."'");
		$senderBankName = $senderBankRS["branchAddress"];
		$response->rows[$i]['cell'] = array(
									dateFormat($rowT["transDate"], "2"),
									$rowT["refNumberIM"],
									customNumberFormat($rowT["totalAmount"],$roundLevel1,''),
									customNumberFormat($rowT["remTransAmount"],$roundLevel1,''),
									$rowT["currencyFrom"],
									$rowT["currencyTo"],
									$custNumber,
									$custName
								);
		$i++;
	}
	
	echo $response->encode($response); 
}	
?>