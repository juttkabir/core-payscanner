<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
//$agentType = getAgentType();
///////////////////////History is maintained via method named 'activities'
$setRight = $_POST["setRight"];
$userGroup = $_POST["userGroup"];
$rightName = $_POST["rightsOption"];
$rightValue = $_POST["rightValue"];
$rightValue1 = $_POST["rightValue1"];
$rightValue2 = $_POST["rightValue"];
$rightStatus = $_POST["rightStatus"];

$queryRights = "select * from setUserRights where userGroup = '".$userGroup."' AND rightName  = '".$rightName."'";
$rightsData = selectFrom($queryRights);

if($_POST["Submit"] == "Submit")
{
	if($rightValue1!="")
		$rightValue .= "|".$rightValue1;
	if($rightsData!="" ){
		$updateQuery = "Update setUserRights set rightName = '".$rightName."',rightStatus = '".$rightStatus."', rightValue ='".$rightValue."' where userGroup = '".$userGroup."'";
		update($updateQuery);
		$msg = "Right for User Group(s) has been Updated.";
	}else{
		$insertQuery = "Insert into setUserRights(userGroup, rightName, rightStatus, rightValue,updated) values('".$userGroup."','".$rightName."','".$rightStatus."','".$rightValue."','".date("Y-d-m")."')";
		insertInto($insertQuery);
		$msg = "Right for User Group(s) has been Created.";
	}
}
else{
	$rightName	= $rightsData["rightName"];
	$rightValues= explode("|",$rightsData["rightValue"]);
	$rightValue2	= $rightValues[0];
	$rightValue1= $rightValues[1];
	$rightStatus = $rightsData["rightStatus"];
}

?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
<body>
<script language="javascript">
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function checkForm(theForm) {
	if(theForm.userGroup.value == ""){
    	alert("Please select a right.");
        theForm.userGroup.focus();
        return false;
    }
	if(theForm.rightValue.value == "" || IsAllSpaces(theForm.rightValue.value) || theForm.rightValue.value >24 || theForm.rightValue.value < 0){
    	alert("Please provide Start Time (between 0-24).");
        theForm.rightValue.focus();
        return false;
    }
	if(theForm.rightValue1.value == "" || IsAllSpaces(theForm.rightValue1.value) || theForm.rightValue1.value >24 || theForm.rightValue1.value <0){
    	alert("Please provide End Time (between 0-24).");
        theForm.rightValue1.focus();
        return false; 
    }
	if(eval(theForm.rightValue1.value) < eval(theForm.rightValue.value)){
    	alert("Start time should be less than End time (in hrs).");
        theForm.rightValue.value="";
        theForm.rightValue1.value="";
        theForm.rightValue.focus();
        return false;
    }
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
 
    
</script>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	<tr>
		<td class="topbar"><b><strong><font class="topbar_tex">Add/Update Rights</font></strong></b></td>
	</tr>
	<tr>
		<td><table width="70%"  border="0">
				<tr>
					<td><fieldset>
						<legend class="style2">Add/Update Rights </legend>
						<br>
						<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
							<form name="addUserRight" id="addUserRight" method="post" onSubmit="return checkForm(this);">
								<? if($msg!= "")
			  {
			  ?>
								<tr align="center">
									<? if (CLIENT_NAME == "Spinzar") { ?>
									<td colspan="2"><b><font color= <? echo SUCCESS_COLOR ?> ><? echo $msg?></font></b><br></td>
									<? } else { ?>
									<td colspan="2" class="tab-r"><? echo $msg?><br></td>
									<? } ?>
								</tr>
								<?
			  }
			  
			  ?>
								<tr>
									<td width="35%" align="right" bgcolor="#DFE6EA"> Right Name</td>
									<td width="65%" bgcolor="#DFE6EA"><SELECT name="rightsOption" id="rightsOption" style="font-family:verdana; font-size: 11px">
											<OPTION value="Create Transaction">Create Transaction</OPTION>
										</SELECT>
										<script language="JavaScript">
         	SelectOption(document.forms[0].rightsOption, "<?=$rightName?>");
                                </script></td>
								</tr>
								<tr>
									<td width="35%" align="right" bgcolor="#DFE6EA"><input type="hidden" name="setRight" value="Y">
										User Group</td>
									<td width="65%" bgcolor="#DFE6EA"><SELECT id="userGroup" name="userGroup" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].setRight.value = ''; document.forms[0].submit();">
											<OPTION value="">- Select User Group -</OPTION>
											<OPTION value="All">All</OPTION>
											<OPTION value="admin">Super Admin</OPTION>
											<OPTION value="SUPA">Super Agent</OPTION>
											<OPTION value="SUBA">Sub Agent</OPTION>
											<OPTION value="SUPI">Super Distributor</OPTION>
											<OPTION value="SUBI">Sub Distributor</OPTION>
											<OPTION value="SUPAI">Super Agent Distributor</OPTION>
											<OPTION value="SUBAI">Sub Agent Distributor</OPTION>
											<OPTION value="Admin">Admin Staff</OPTION>
											<OPTION value="Admin Manager">Admin Manager</OPTION>
											<OPTION value="Branch Manager">Branch Manager</OPTION>
											<OPTION value="SUPI Manager">Distributor Manager</OPTION>
											<OPTION value="TELLER">TELLER</OPTION>
											<OPTION value="Call">Call Center Staff</OPTION>
											<OPTION value="COLLECTOR">Collectors</OPTION>
											<OPTION value="Support">Support Staff</OPTION>
											<OPTION value="MLRO">MLRO</OPTION>
										</SELECT>
										<script language="JavaScript">
         	SelectOption(document.forms[0].userGroup, "<?=$userGroup?>");
                                </script>
									</td>
								</tr>
								<tr>
									<td align="right" valign="top" bgcolor="#DFE6EA" style="padding-top:15px;">Functionality</td>
									<? if($rightStatus==""){?>
									<td bgcolor="#DFE6EA"><input name="rightStatus" type="radio" value="1" checked>
									Enable<br>
										<input name="rightStatus" type="radio" value="0" >
									Disable
									</td>
									<? }else{ ?>
									<td bgcolor="#DFE6EA"><input name="rightStatus" type="radio" value="1" <? echo ($rightStatus=="1" ? "checked"  : "")?>>
									Enable<br>
										<input name="rightStatus" type="radio" value="0" <? echo ($rightStatus=="0" ? "checked"  : "")?>>
									Disable
									</td>
									<? } ?>
								</tr>
								<!--
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">Receiving Currency</td>
			    <td bgcolor="#DFE6EA"><input name="currency" type="text" id="currency" size="10" value="<? //echo $_SESSION["currency"]?>"></td>
			  </tr>
			  -->
								<tr id="rightDetails">
									<td align="right" valign="top" bgcolor="#DFE6EA">Start Time 
									<input name="rightValue" id="rightValue" size="10" value="<?=$rightValue2?>" > hrs</td>
									<td align="left" valign="top" bgcolor="#DFE6EA">End Time 
									<input name="rightValue1" id="rightValue1" size="10" value="<?=$rightValue1?>"> hrs</td>
								</tr>
								<tr>
									<td align="right" valign="top" bgcolor="#DFE6EA">&nbsp;</td>
									<td align="center" bgcolor="#DFE6EA"><input name="Submit" type="submit" class="flat" value="Submit"></td>
								</tr>
							</form>
						</table>
						<br>
						</fieldset></td>
				</tr>
			</table></td>
	</tr>
</table>
</body>
</html>
