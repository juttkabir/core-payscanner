<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$today = date('d-m-Y  h:i:s A');
$agentType = getAgentType();

if(sizeof($_REQUEST["checkbox"]) > 0)
{
	for($i=0; $i<sizeof($_REQUEST["checkbox"]); $i++)
	{
		$sql = "select * from ".TBL_TRANSACTIONS." where transID=".$_REQUEST["checkbox"][$i];
		$result = selectFrom($sql);

		$reportData[$i]["transID"]				= $_REQUEST["checkbox"][$i];
		$reportData[$i]["custAgentID"]			= $result["custAgentID"];
		$reportData[$i]["customerID"]			= $result["customerID"];
		$reportData[$i]["imReferenceNumber"]	= $result["refNumberIM"];
		$reportData[$i]["refNumber"]			= $result["refNumber"];
		$reportData[$i]["benID"]				= $result["benID"];
		$reportData[$i]['transType']			= $result["transType"];
		$reportData[$i]["collectionPointID"]	= $result["collectionPointID"];
		$reportData[$i]["currencyFrom"]			= $result["currencyFrom"];
		$reportData[$i]["transAmount"]			= $result["transAmount"];
		$reportData[$i]['currencyTo']			= $result["currencyTo"];
		$reportData[$i]["exchangeRate"]			= $result["exchangeRate"];
		$reportData[$i]["IMFee"]				= $result["IMFee"];
		$reportData[$i]["currencyCharge"]		= $result["outCurrCharges"];
		$reportData[$i]["totalAmount"]			= $result["totalAmount"];
		$reportData[$i]["localAmount"]			= $result["localAmount"];
		$reportData[$i]["moneyPaid"]			= $result["moneyPaid"];

		if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
		{
			$reportData[$i]["chequeAmount"] = $result["chequeAmount"];
		}

		$reportData[$i]["dDate"]				= $result["transDate"];
		$reportData[$i]["mydDate"]				= $result["transDate"];
		$reportData[$i]["addedBy"]				= $result["addedBy"];
	}
} else {
	$reportData[0]["transID"]				= $_REQUEST["transID"];
	$reportData[0]["custAgentID"]			= $_SESSION["custAgentID"];
	$reportData[0]["customerID"]			= $_SESSION["customerID"];
	$reportData[0]["imReferenceNumber"]		= $_SESSION["imReferenceNumber"];
	$reportData[0]["refNumber"]				= $_SESSION["refNumber"];
	$reportData[0]["benID"]					= $_SESSION["benID"];
	$reportData[0]['transType']				= $_SESSION["transType"];
	$reportData[0]["collectionPointID"]		= $_SESSION["collectionPointID"];
	$reportData[0]["currencyFrom"]			= $_SESSION["currencyFrom"];
	$reportData[0]["transAmount"]			= $_SESSION["transAmount"];
	$reportData[0]['currencyTo']			= $_SESSION["currencyTo"];
	$reportData[0]["exchangeRate"]			= $_SESSION["exchangeRate"];
	$reportData[0]["IMFee"]					= $_SESSION["IMFee"];
	$reportData[0]["currencyCharge"]		= $_SESSION["outCurrCharges"];
	$reportData[0]["totalAmount"]			= $_SESSION["totalAmount"];
	$reportData[0]["localAmount"]			= $_SESSION["localAmount"];
	$reportData[0]["moneyPaid"]				= $_SESSION["moneyPaid"];

	if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
	{
		$reportData[0]["chequeAmount"] = $_SESSION["chequeAmount"];
	}

	$reportData[0]["dDate"]					= $_SESSION["transDate"];
	$reportData[0]["mydDate"]				= $_SESSION["transDate"];
}

for($i=0; $i<sizeof($reportData); $i++)
{
	if($reportData[$i]["custAgentID"] != "")
	{
		$strAddressQuery = selectFrom("select * from ". TBL_ADMIN_USERS." where userID ='".$reportData[$i]["custAgentID"]."'");
		$CustomerData = selectFrom("select accountName from ". TBL_CUSTOMER." where customerID ='".$reportData[$i]["customerID"]."'");
		
		$printAddress = $strAddressQuery["agentAddress"];
		$printPhone = $strAddressQuery["agentPhone"];
		$printSenderNo = $CustomerData["accountName"];
		
		$reportData[$i]["senderNumber"] = $printSenderNo;
		
	}
}
?>
<html>
<head>
<title>Transaction Confirmation</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style5 {
	color: #005b90;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="700" border="0" cellspacing="1" cellpadding="5" align="center">
 
  <? if ($_GET["msg"] == "Y"){ ?>
  
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
              	
                <td width="40" align="center"><div class='noPrint'><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></div></td>
                <td width="635"><div class='noPrint'><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?></div></td>
                 
              </tr>
            </table></td>
          </tr>
       
		  <?
		  }
		  ?>

   <tr>
      <td align="center"><br>
		<?
			for($i=0; $i<sizeof($reportData); $i++)
			{
		?>
			   <table width="100%" border="0" bordercolor="#FF0000">
				  <tr>
					<td><fieldset>
					<legend class="style2">Sender Company</legend>
					<br>
					
					<table border="0" width="100%" cellspacing="2" cellpadding="3" bgcolor="#000000">
					  <tr  bgcolor="#ffffff">
						<td align="center" bgcolor="#D9D9FF" >
							<?=printLogoOnReciept($reportData[$i]["custAgentID"], $_SESSION["loggedUserData"]["username"])?>	
						</td>
						 <td width="100%" align="center" colspan="3">
					  <!--
					  <?
						if($_SESSION["loggedUserData"]["adminType"] == "Agent")
						{
					  ?>
								<strong><font size='3'><? echo strtoupper($_SESSION["loggedUserData"]["agentCompany"]); ?></font></strong>
								<br><font size='2'>Address:
								<? 
									if(CONFIG_POS_ADDRESS == '1')
										{ 
											echo $_SESSION["loggedUserData"]["agentAddress"]; 
											if(!empty($_SESSION["loggedUserData"]["agentAddress2"]))
												echo ', '.$_SESSION["loggedUserData"]["agentAddress2"];
											echo '<br />';
		
											echo 'Phone: '.$_SESSION["loggedUserData"]["agentPhone"];
											if(!empty($_SESSION["loggedUserData"]["agentFax"]))
												echo ' Fax: '.$_SESSION["loggedUserData"]["agentFax"];	
										}
										else
										{
											echo $_SESSION["loggedUserData"]["agentAddress"]; 
											if(!empty($_SESSION["loggedUserData"]["agentAddress2"]))
												echo ', '.$_SESSION["loggedUserData"]["agentAddress2"];
											echo '<br />';
											echo 'Phone: '.$_SESSION["loggedUserData"]["agentPhone"];
										} 
								?>
							</font>
						<?
							}
							else
							{
						?>
								<strong><font size='3'><? echo strtoupper($company." ".COMPANY_NAME_EXTENSION); ?></font></strong>
								
								<br><font size='2'>Address:
								<? if(CONFIG_POS_ADDRESS == '1'){ echo(COMPANY_ADDRESS); ?><br>
								<? echo(COMPANY_PHONE);}else{echo($printAddress); ?> <br>
								<? echo("Phone:".$printPhone);} ?>
								</font>
						<?
							}
						?>
						-->
						
						<?
							printRecieptHeader($_SESSION["loggedUserData"]["adminType"]);
						?>
					</td>
					  </tr>
					  <tr bgcolor="#ffffff">
						 <td width="100" align="left"><font color="#005b90">Date</font></td>
						<td width="250">
							<!--Against the back date condition according to the 
									latest requirement have to show the current date and time -->
							<? if($reportData[$i]["dDate"]!= "" && CONFIG_BACK_LEDGER_DATES == '1'){echo($reportData[$i]["dDate"]);}else{echo($reportData[$i]["mydDate"]);} ?>
						</td>
						<!-- Added by Niaz Ahmad againist ticket # 2533 at 10-10-2007 -->
						<? if(CONFIG_SHOW_REFERENCE_CODE_BOX == "1"){ ?>
						<td width="100" align="left"><font color="#005b90">Transaction No</font></td>
						<td width="250"><table border="1"><tr><td align="center"><strong><font size="3"><? echo $reportData[$i]["imReferenceNumber"]?></font></strong></td></tr></table></td>
						
						<? }else{ ?>
						
						 <td width="100" align="left"><font color="#005b90">Transaction No</font></td>
						<td width="250">
							<? echo $reportData[$i]["imReferenceNumber"]?>
						</td>
						<? } ?>
					</tr>
					<tr bgcolor="#ffffff">
						  <td width="100" align="left"><font color="#005b90">Sender No</font></td>
						<td width="250"><font size="2"><b> <?=$reportData[$i]["senderNumber"]?></b></font></td>
						 <td width="150" align="left"><font color="#005b90"><? echo $manualCode;?></font></td>
						<td width="200">
							<? echo($reportData[$i]["refNumber"]); ?>
						</td>           	
					</tr>
					<tr bgcolor="#ffffff">
						<td width="100" align="left">&nbsp;</td>
						<td width="250">&nbsp;</td>
						 <td width="150" align="left"><font color="#005b90">Created By</font></td>
						<td width="200">
						<? 
							if(!empty($_REQUEST["transID"]))
							{
								$addedBySql = "select addedBy from ".TBL_TRANSACTIONS." where transID=".$_REQUEST["transID"];
								$addedByData = selectFrom($addedBySql);
								echo $addedByData["addedBy"];
							}
							else
								echo($reportData[$i]["addedBy"]); 
						?>
						</td>           	
					</tr>
					<?
					if(CONFIG_MANAGE_CLAVE == '1')
					{
						
						$getClave = selectFrom("select clave from ".TBL_TRANSACTION_EXTENDED." as e, ".TBL_TRANSACTIONS." as t where t.transID = e.transID and t.refNumberIM= '".$reportData[$i]["imReferenceNumber"]."'");
						?>
						<tr bgcolor="#ffffff">
						  <td width="100" align="left">&nbsp;</td>
						<td width="250">&nbsp;</td>
						 <td width="150" align="left"><font color="#005b90">Clave</font></td>
						<td width="200">
							<? echo($getClave["clave"]); ?>
						</td>           	
					</tr>
						<?
						}
					?>
					 <tr bgcolor="#ffffff">
						 <td width="700" align="left" colspan="4"  height="25"><font color="#005b90"><strong>Sender Detail</strong></font></td>
						
					</tr>
					
			 <? 
					$queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Mobile, Email  from ".TBL_CUSTOMER." where customerID ='" . $reportData[$i]["customerID"] . "'";
					$customerContent = selectFrom($queryCust);
					if($customerContent["customerID"] != "")
					{
				?>
					<tr bgcolor="#ffffff">
						 <td width="150" align="left"><font color="#005b90">Full Name</font></td>
						<td width="550" colspan="3">
							<? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?>
						</td>              	
					</tr>
					 <tr bgcolor="#ffffff">
						 <td width="150" align="left"><font color="#005b90">Address</font></td>
						<td width="550" colspan="3">
							<? echo $customerContent["Address"] . " " . $customerContent["Address1"]?>, <?=$customerContent["City"]?> - <?=$customerContent["Zip"]?>, <?=$customerContent["State"]?>, <?=$customerContent["Country"]?>.
							 Ph: <?=$customerContent["Phone"]?> Mob: <?=$customerContent["Mobile"]?>
						</td>              	
					</tr>
					 
					<?
				}
					?>
					
					  <tr bgcolor="#ffffff">
						 <td width="700" align="left" colspan="4" height="25"><font color="#005b90"><strong>Beneficiary Detail</strong></font></td>
					 </tr>
					  <? 
				
					$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Mobile, Email from ".TBL_BENEFICIARY." where benID ='" . $reportData[$i]["benID"] . "'";
					$benificiaryContent = selectFrom($queryBen);
					if($benificiaryContent["benID"] != "")
					{
				?>
					<tr bgcolor="#ffffff">
						 <td width="200" align="left"><font color="#005b90">Beneficiary Name</font></td>
						<td width="500" colspan="3">
							<? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?>
						</td>
					</tr>
							 
					 <tr bgcolor="#ffffff">
						 <td width="200" align="left"><font color="#005b90">Address</font></td>
						<td width="500" colspan="3">
							<? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?>, <?=$benificiaryContent["City"]?> - <?=$benificiaryContent["Zip"]?>, <?=$benificiaryContent["State"]?>, <?=$benificiaryContent["Country"]?>.
							 Ph: <?=$benificiaryContent["Phone"]?> Mob: <?=$benificiaryContent["Mobile"]?>
						</td>
						
					</tr> 
				<?
				}            
				?>
				   
					<tr bgcolor="#ffffff">
						 <td width="750" align="left" colspan="4" height="25"><font color="#005b90"><strong>Transaction Detail</strong></font></td>
							</tr>
							<tr bgcolor="#ffffff">
							 <td width="200" align="left"><font color="#005b90" size='2'><b>Destination</b></font></td>
							 <td width="500" colspan="3">
									<?php
						if($reportData[$i]['transType'] == "Pick up")
						{
									$queryColl = "select *  from ".TBL_COLLECTION." where cp_id ='".$reportData[$i]["collectionPointID"]."'";
													
													$destinCollection = selectFrom($queryColl);
													if($destinCollection["cp_id"] != '')
													{
														$distData = selectFrom("select name,distalias from admin where userID = '".$destinCollection["cp_ida_id"]."'");
														$destination = $destinCollection["cp_corresspondent_name"]." ".$destinCollection["cp_branch_name"];
														$cp_address = $destinCollection["cp_corresspondent_name"]." ".$destinCollection["cp_branch_name"].", ".$destinCollection["cp_branch_address"].", ".$destinCollection["cp_city"].", ".$destinCollection["cp_state"].", ".$destinCollection["cp_country"];
													}
		
													if(CONFIG_CP_DATA_ON_RECIEPT == "1"){
														
													if($distData["distalias"] != ""){
															
															echo $distData["distalias"].", ";
													}else{
														IF($agentType == "SUPA" || $agentType == "SUBA"){
															
														}else{
															echo $distData["name"].", ";
														}
													
													}
												}
													// Added by Niaz Ahmad #3096 at 18-04-2008
														echo $cp_address;
													/**
													 * Adding the phone number #3202
													 */
													echo " ,Ph:".$destinCollection["cp_phone"] .", Fax:" .$destinCollection["cp_fax"];
												}
											?>
							</td>
						 </tr> 
					   <tr bgcolor="#ffffff">
						 <td width="100" align="left"><font color="#005b90"><b>Amount Paid</b></font></td>
						<td width="250">
							<b><? echo($reportData[$i]["currencyFrom"]." ".$reportData[$i]["transAmount"]);?></b>
						</td>
						 <td width="100" align="left"><font color="#005b90"><b>Exchange Rate</b></font></td>
						<td width="250">
							<b><? echo($reportData[$i]['currencyTo']." ".$reportData[$i]["exchangeRate"]); ?> </b>
						</td>
					</tr> 
					 <tr bgcolor="#ffffff">
						 <td width="100" align="left"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
						<td width="250"> 
							<? echo($reportData[$i]["currencyFrom"]." ".$reportData[$i]["IMFee"]);?>
						</td>
						 <td width="100" align="left"><font color="#005b90">Curr. Charges</font></td>
						<td width="250">
							<? echo($reportData[$i]["currencyFrom"]." ".$reportData[$i]["currencyCharge"]);?> 
						</td>
					</tr> 
					 <tr bgcolor="#ffffff">
						 <td width="200" align="left"><font color="#005b90">Total Amount</font></td>
						<td width="250">
							<? echo($reportData[$i]["currencyFrom"]); ?> <? echo number_format($reportData[$i]["totalAmount"],2,'.',','); ?> 
							
						</td>
						 <td width="200" align="left"><font color="#005b90"><b>Foriegn Amount</b></font></td>
						<td width="250">
							
							<b><? echo($reportData[$i]["currencyTo"]); ?> <? echo number_format($reportData[$i]["localAmount"],0,'.',','); ?> </b>
						</td>
					</tr>
			<?
				if ( $reportData[$i]["moneyPaid"] == "By Cheque" && defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1 )
				{
					?>
					<tr bgcolor="#ffffff">
						 <td width="200" align="left"><font color="#005b90">By Cheque</font></td>
						<td width="250" colspan="3">
							<?=$reportData[$i]["currencyFrom"];?>
							<?=(!empty($reportData[$i]["chequeAmount"]) ? number_format($reportData[$i]["chequeAmount"], 2) : 0.00); ?>
						</td>
					</tr> 
					<tr bgcolor="#ffffff">
						 <td width="200" align="left"><font color="#005b90">By Cash</font></td>
						<td width="250" colspan="3">
							<?=$reportData[$i]["currencyFrom"];?>
							<?=number_format( $reportData[$i]["totalAmount"] - $reportData[$i]["chequeAmount"], 2); ?>
						</td>
					</tr> 					
					<?
				}
			?>
				
					<?php
						if(CONFIG_CURRENCY_DENOMINATION_ON_CONFIRM == "1")
						{
							$queryCurrencyDinomination = "select *  from ".TBL_CURRENCYNOTES." where transId =" . $reportData[$i]["transID"] . "";
							$currencyNotesContent = selectFrom($queryCurrencyDinomination);
					?>
							<tr bgcolor="#ffffff">
								 <td width="750" align="left" colspan="4" height="25"><font color="#005b90"><strong>Currency Denomination</strong></font></td>
							</tr>
							
							<tr bgcolor="#ffffff">
								<td width="200" align="right">
										<font color="#005b90">1 GBP X </font>
								</td>
								<td width="250">&nbsp;
									<?=$currencyNotesContent['notes1']?>
									&nbsp;<b>=</b>&nbsp;
									<?=$currencyNotesContent['notes1']?> GBP
								</td>
								 <td width="200" align="right"><font color="#005b90">10 GBP X </font></td>
								<td width="250">&nbsp;
									<?=$currencyNotesContent['notes10']?>
									&nbsp;<b>=</b>&nbsp;
									<? echo $currencyNotesContent['notes10'] * 10; ?> GBP
								</td>
							</tr> 
							<tr bgcolor="#ffffff">
								<td width="200" align="right">
										<font color="#005b90">2 GBP X </font>
								</td>
								<td width="250">&nbsp;
									<?=$currencyNotesContent['notes2']?>
									&nbsp;<b>=</b>&nbsp;
									<? echo $currencyNotesContent['notes2'] * 2; ?> GBP
								</td>
								 <td width="200" align="right"><font color="#005b90">20 GBP X </font></td>
								<td width="250">&nbsp;
									<?=$currencyNotesContent['notes20']?>
									&nbsp;<b>=</b>&nbsp;
									<? echo $currencyNotesContent['notes20'] * 20; ?> GBP
								</td>
							</tr> 
							<tr bgcolor="#ffffff">
								<td width="200" align="right">
										<font color="#005b90">5 GBP X </font>
								</td>
								<td width="250">&nbsp;
									<?=$currencyNotesContent['notes5']?>
									&nbsp;<b>=</b>&nbsp;
									<? echo $currencyNotesContent['notes5'] * 5; ?> GBP
								</td>
								 <td width="200" align="right"><font color="#005b90">50 GBP X </font></td>
								<td width="250">&nbsp;
									<?=$currencyNotesContent['notes50']?>
									&nbsp;<b>=</b>&nbsp;
									<? echo $currencyNotesContent['notes50'] * 50; ?> GBP
								</td>
							</tr>
							<tr bgcolor="#ffffff" height="20">
								<td width="200" align="right">&nbsp;</td>
								<td width="250"><font size="2"><b>Total Paid:</b></font>
								</td>
								 <td width="200" align="right">&nbsp;
										<font size="2"><b><?=$currencyNotesContent['total']?> GBP</b></font>
										
								 </td>
								<td width="250">&nbsp;</td>
							</tr> 
							
							<? 
								$cashAmount = $reportData[$i]["totalAmount"];
								if ( !empty( $reportData[$i]["chequeAmount"] ) )
								{
									$cashAmount = number_format( $reportData[$i]["totalAmount"] - $reportData[$i]["chequeAmount"], 2);
								}
								
								if(!empty($reportData[$i]["chequeAmount"]) && $reportData[$i]["chequeAmount"] > 0) { ?> 
								<tr bgcolor="#ffffff" 		height="20"> <td width="200" align="left">&nbsp;</td> 
								<td width="250"><font size="2"><b>Outstanding Amount:</b></font> 
								</td> <td width="200" align="right">&nbsp; <font 
								size="2"><b><?=$reportData[$i]["chequeAmount"]?> GBP</b></font> 
								</td> <td width="250">&nbsp;</td> </tr> 
									
								<?
							}
								else
								{
									
									$intAmount = $reportData[$i]["totalAmount"]-$currencyNotesContent['total'];
									 
								if(empty($reportData[$i]["chequeAmount"]) && $intAmount > 0) { ?>
									<tr bgcolor="#ffffff" 		height="20">
										<td width="200" align="left">&nbsp;</td> 
										<td width="250"><font size="2"><b>Outstanding Amount:</b></font> </td> 
										<td width="200" align="right">
												&nbsp; <font size="2"><b><?=$reportData[$i]["totalAmount"]-$currencyNotesContent['total']?> GBP</b></font> 
										</td>
										<td width="250">&nbsp;</td>
										</tr>
								
								<?	
								}
									
									
									
								elseif(($currencyNotesContent['total'] - $cashAmount) >= 0) 
								{ 
							?>
							<tr bgcolor="#ffffff" height="20">
								<td width="200" align="left">&nbsp;</td>
								<td width="250"><font size="2"><b>Change Given:</b></font>
								</td>
								 <td width="200" align="right">&nbsp;
									<font size="2"><b><? echo $currencyNotesContent['total'] - $cashAmount ?> GBP</b></font>
								 </td>
								 <td width="250">&nbsp;</td>
							</tr> 
						<? 
							} 
						}
						}
					?>
					
					</table>
				 
					</fieldset></td>
				   
				  </tr>         
				<tr><td align="center">
					<table border="0" width="80%" cellspacing="1" cellpadding="0" align="center">
						<tr>
							<td align="center"><strong><u>TERMS AND CONDITIONS</u></strong><br><br></td>
						</tr>
						<tr>
							<td align="center">&quot;<? echo (CONFIG_TRANS_COND);?>&quot;</td>
						</tr>
						</table>
					<br />
					<br />
						<table border="0" width="100%" cellspacing="1" cellpadding="0">
							<tr bgcolor="#ffffff">
								<td width="50%" align="center"><font color="#005b90"><b>CUSTOMER SIGNATURE</b></font></td>
								<td width="50%" align="center"><font color="#005b90"><b>AGENT SIGNATURE</b></font></td>
							</tr> 
						</table>
					<br /><br /><br />
					<hr noshade="noshade" size="1" />
					</td></tr>
				   <tr>
					<td align="center">
						<?
						if($_SESSION["loggedUserData"]["adminType"] == "Agent")
						{
							if(CONFIG_POS_ADDRESS == '1')
							{ 
								echo $_SESSION["loggedUserData"]["agentAddress"]; 
								if(!empty($_SESSION["loggedUserData"]["agentAddress2"]))
									echo ', '.$_SESSION["loggedUserData"]["agentAddress2"];
								echo ', '.$_SESSION["loggedUserData"]["agentCity"].", ".$_SESSION["loggedUserData"]["agentCountry"];
								echo '<br />';
								echo 'Phone: '.$_SESSION["loggedUserData"]["agentPhone"];
							}
							else
							{
								echo $_SESSION["loggedUserData"]["agentAddress"]; 
								if(!empty($_SESSION["loggedUserData"]["agentAddress2"]))
									echo ', '.$_SESSION["loggedUserData"]["agentAddress2"];
								echo ', '.$_SESSION["loggedUserData"]["agentCity"].", ".$_SESSION["loggedUserData"]["agentCountry"];
								echo '<br />';
								echo 'Phone: '.$_SESSION["loggedUserData"]["agentPhone"];
							} 
						}
						else
							echo COMPANY_ADDRESS.'<br />'.COMPANY_PHONE.'<br />Email: '.INQUIRY_EMAIL; 
					?>
					</td>
				   </tr> 
			</table>
		<?
			}
		?>
		<br /><br />
		<table width="100%" border="0" bordercolor="#FF0000">
			<tr>
				<td align="center">
					<div class='noPrint'>
						<input type="button" name="Submit2" value="Print this Receipt" onClick="print()">
						&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="style2">Go to Create Transaction</a>
					</div>
				</td>
			</tr>
		</table>
        </td>
          </tr>
              </table>
</body>
</html>
