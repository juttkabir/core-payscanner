<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
dbConnect();
$username=loggedUser();
$agentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$_GET["userID"]."'");
if ($_GET["caller"] != ""){
	$supperAgentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$agentDetails["parentID"]."'");
}
?>
<html>
<head>
	<title>View Supper Admin Details</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">
		<table width="100%"><tr><td><strong><font color="#FFFFFF" size="2">View <? if ($_GET["caller"] == "") echo "Supper"; else echo "Sub";?> Agent/Correspondent Details</font></strong></td><td><a href="javascript:window.close()"><font size="4" color="#ffffff"><b>X</b></font></a>&nbsp;</td></tr></table></td>
  </tr>
  <tr>
    <td align="center">
		<table width="426" border="0" cellspacing="1" cellpadding="2" align="center">
		
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Contact Person Name</strong></font></td>
            <td><?=stripslashes($agentDetails["name"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Company Name</strong></font></td>
            <td><?=stripslashes($agentDetails["agentCompany"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Address Line 1*</b></font></td>
            <td><?=stripslashes($agentDetails["agentAddress"]); ?></td>
        </tr><? if (trim($agentDetails["agentAddress2"]) != ""){ ?>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Address Line 2</b></font></td>
            <td><?=stripslashes($agentDetails["agentAddress2"]); ?></td>
        </tr><? } ?>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Country*</b></font></td>
            <td><?=$agentDetails["agentCountry"]; ?></td>
        </tr>
		
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>City*</b></font></td>
            <td><?=$agentDetails["agentCity"];?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Postal Code*</b></font></td>
            <td><?=stripslashes($agentDetails["postCode"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Phone*</strong></font></td>
            <td><?=stripslashes($agentDetails["agentPhone"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Mobile*</strong></font></td>
            <td><?=stripslashes($agentDetails["mobile"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Fax</strong></font></td>
            <td><?=stripslashes($agentDetails["agentFax"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Email*</strong></font></td>
            <td><?=stripslashes($agentDetails["email"]); ?></td>
       </tr>
       <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>User Created Date</strong></font></td>
            <td><?=stripslashes($agentDetails["created"]); ?></td>
       </tr>
         <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Designation*</strong></font></td>
            <td><?=stripslashes($agentDetails["designation"]); ?></td>
       </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Status</strong></font></td>
            <td><?=$agentDetails["agentStatus"];?></td>
        </tr>
	<?	if ($agentDetails["agentStatus"] == 'Disabled') {  ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Disable Reason</strong></font></td>
            <td><?=$agentDetails["disableReason"];?></td>
        </tr>
	<?	} else if ($agentDetails["agentStatus"] == 'Suspended') {  ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Suspension Reason</strong></font></td>
            <td><?=$agentDetails["suspensionReason"];?></td>
        </tr>
	<?	}  ?>
      </table>
	</td>
  </tr>
</table>
</body>
</html>
