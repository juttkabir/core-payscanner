<?
session_start();
include ("../include/config.php");
include ("security.php");
$agentType = getAgentType();
$changedBy = $_SESSION["loggedUserData"]["userID"];	
$modifiedDate = getCountryTime(CONFIG_COUNTRY_CODE);

///////////////////////History is maintained via method named 'activities'
$superAgent = "";
$SUPAShare  = "";
$SUPAShareType = "";

$superIDA = "";
$SUPIDAShare = "";
$SUPIDAShareType = "";
if($_POST["View"] != "")
{
	if($_POST["superAgent"] != "")
	{
		$superAgentConfig = selectFrom("select shareType, shareValue from ".TBL_SHARE_CONFIG." where userid = '".$_POST["superAgent"]."' and userType = 'Agent'");
		
		$superAgent = $_POST["superAgent"];
		$SUPAShare = $superAgentConfig["shareValue"];
		$SUPAShareType = $superAgentConfig["shareType"];
		
	}
	
	if($_POST["superIDA"] != "")
	{
		$superIDAConfig = selectFrom("select shareType, shareValue from ".TBL_SHARE_CONFIG." where userid = '".$_POST["superIDA"]."' and userType = 'Distributor'");
		$superIDA = $_POST["superIDA"];
		$SUPIDAShare = $superIDAConfig["shareValue"];
		$SUPIDAShareType = $superIDAConfig["shareType"];
		
	}
	
	if($_POST["subAgent"] != "")
	{
		$subAgentConfig = selectFrom("select shareConfigID, shareType, shareValue, shareFrom from ".TBL_SHARE_CONFIG." where userid = '".$_POST["subAgent"]."' and userType = 'Agent'");
		$subAgent = $_POST["subAgent"];
		$subAgentShare = $subAgentConfig["shareValue"];
		$subAgentShareType = $subAgentConfig["shareType"];
		$subAgentShareFrom = $subAgentConfig["shareFrom"];
		if($subAgentShareFrom != 'company')
		{
			$subAgentShareFrom = 'parent';
		}
		
	}
	
	if($_POST["subIDA"] != "")
	{
		$subIDAConfig = selectFrom("select shareConfigID, shareType, shareValue, shareFrom from ".TBL_SHARE_CONFIG." where userid = '".$_POST["subIDA"]."' and userType = 'Distributor'");
		$subIDA = $_POST["superIDA"];
		$subIDAShare = $subIDAConfig["shareValue"];
		$subIDAShareType = $subIDAConfig["shareType"];
		$subIDAShareFrom = $subIDAConfig["shareFrom"];
		if($subIDAShareFrom != 'company')
		{
			$subIDAShareFrom = 'parent';
		}
		
	}
}elseif($_POST["Save"] != "")
{
	if($_POST["superAgent"] != "")
	{
		$superAgentConfig = selectFrom("select shareConfigID, shareType, shareValue from ".TBL_SHARE_CONFIG." where userid = '".$_POST["superAgent"]."' and userType = 'Agent'");
		
		$superAgent = $_POST["superAgent"];
		$SUPAShare = $_POST["SUPAShare"];
		$SUPAShareType = $_POST["SUPAShareType"];
		
		
		if($superAgentConfig["shareConfigID"] > 0)
		{
			
			update(" update ".TBL_SHARE_CONFIG." set 
			shareType = '".$SUPAShareType."',
			shareValue= '".$SUPAShare."',
			lastModified = '".$modifiedDate."',
			modified_by = '".$changedBy."',
			status = 'Active',
			userType = 'Agent' where shareConfigID = '".$superAgentConfig["shareConfigID"]."'");
			
		}else{
			insertInto("insert into ".TBL_SHARE_CONFIG." 
			(shareType, shareValue, lastModified, modified_by, status, userType, userid) values
			('".$SUPAShareType."', '".$SUPAShare."', '".$modifiedDate."','".$changedBy."', 'Active', 'Agent', '".$superAgent."' )");
			}
		
	}
	
	if($_POST["superIDA"] != "")
	{
		$superIDAConfig = selectFrom("select shareConfigID, shareType, shareValue from ".TBL_SHARE_CONFIG." where userid = '".$_POST["superIDA"]."' and userType = 'Distributor'");
		$superIDA = $_POST["superIDA"];
		$SUPIDAShare = $_POST["SUPIDAShare"];
		$SUPIDAShareType = $_POST["SUPIDAShareType"];
		
		if($superIDAConfig["shareConfigID"] > 0)
		{
			
			update(" update ".TBL_SHARE_CONFIG." set 
			shareType = '".$SUPIDAShareType."',
			shareValue= '".$SUPIDAShare."',
			lastModified = '".$modifiedDate."',
			modified_by = '".$changedBy."',
			status = 'Active',
			userType = 'Distributor' where shareConfigID = '".$superIDAConfig["shareConfigID"]."'");
		}else{
			insertInto("insert into ".TBL_SHARE_CONFIG." 
			(shareType, shareValue, lastModified, modified_by, status, userType, userid) values
			('".$SUPIDAShareType."', '".$SUPIDAShare."', '".$modifiedDate."','".$changedBy."', 'Active', 'Distributor', '".$superIDA."' )");
			}
	}
	
	if($_POST["subAgent"] != "")
	{
		$subAgentConfig = selectFrom("select shareConfigID, shareType, shareValue from ".TBL_SHARE_CONFIG." where userid = '".$_POST["subAgent"]."' and userType = 'Agent'");
		$subAgent = $_POST["subAgent"];
		$subAgentShare = $_POST["subAgentShare"];
		$subAgentShareType = $_POST["subAgentShareType"];
		$subAgentShareFrom = $_POST["subAgentShareFrom"];
		if($subAgentShareFrom != 'company')
		{
			$parentContent = selectFrom("select parentID from ".TBL_ADMIN_USERS." where userID = '".$subAgent."'");	
			$shareFrom = $parentContent["parentID"];
		}else{
			$shareFrom = "company";
			}
		
		if($subAgentConfig["shareConfigID"] > 0)
		{			
			update(" update ".TBL_SHARE_CONFIG." set 
			shareType = '".$subAgentShareType."',
			shareValue= '".$subAgentShare."',
			lastModified = '".$modifiedDate."',
			modified_by = '".$changedBy."',
			shareFrom = '".$shareFrom."',
			status = 'Active',
			userType = 'Agent' where shareConfigID = '".$subAgentConfig["shareConfigID"]."'");
		}else{
			insertInto("insert into ".TBL_SHARE_CONFIG." 
			(shareType, shareValue, lastModified, modified_by, status, userType, userid, shareFrom) values
			('".$subAgentShareType."', '".$subAgentShare."', '".$modifiedDate."','".$changedBy."', 'Active', 'Agent', '".$subAgent."', '".$shareFrom."')");
			}
	}
	if($_POST["subIDA"] != "")
	{
		$subIDAConfig = selectFrom("select shareConfigID, shareType, shareValue from ".TBL_SHARE_CONFIG." where userid = '".$_POST["subIDA"]."' and userType = 'Distributor'");
		$subIDA = $_POST["subIDA"];
		$subIDAShare = $_POST["subIDAShare"];
		$subIDAShareType = $_POST["subIDAShareType"];
		$subIDAShareFrom = $_POST["subIDAShareFrom"];
		if($subIDAShareFrom != 'company')
		{
			$parentContent = selectFrom("select parentID from ".TBL_ADMIN_USERS." where userID = '".$subIDA."'");	
			$shareFrom = $parentContent["parentID"];
		}else{
			$shareFrom = "company";
			}
		if($subIDAConfig["shareConfigID"] > 0)
		{			
			update(" update ".TBL_SHARE_CONFIG." set 
			shareType = '".$subIDAShareType."',
			shareValue= '".$subIDAShare."',
			lastModified = '".$modifiedDate."',
			modified_by = '".$changedBy."',
			shareFrom = '".$shareFrom."',
			status = 'Active',
			userType = 'Distributor' where shareConfigID = '".$subIDAConfig["shareConfigID"]."'");
		}else{
			insertInto("insert into ".TBL_SHARE_CONFIG." 
			(shareType, shareValue, lastModified, modified_by, status, userType, userid, shareFrom) values
			('".$subIDAShareType."', '".$subIDAShare."', '".$modifiedDate."','".$changedBy."', 'Active', 'Distributor', '".$subIDA."', '".$shareFrom."')");
			}
	}
}

?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
<body>
<script language="javascript">
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}


</script>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><b><strong><font class="topbar_tex">Add/Update Sharing Configuration</font></strong></b></td>
  </tr>
  <tr>
    <td>
	<table width="70%"  border="0">
  <tr>
    <td><fieldset>
    <legend class="style2">Add/Update Sharing Configuration</legend>
    <br>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			<form name="shareConfig" action="set-agent-share.php" method="post">
			  <? if($msg!= "")
			  {
			  ?>
			  <tr align="center">
			    <td colspan="4" class="tab-r"><? echo $msg?><br></td>
			    </tr>
			  <?
			  }
			  
			  ?>
		    <tr bgcolor="#DFE6EA">
			 	<td width="35%" align="right">
				  Super Agent</td>
			    <td width="65%">
			    	<SELECT name="superAgent" style="font-family:verdana; font-size: 11px">
                  <OPTION value="">- Select Super Agent-</OPTION>
                 <?          
					$querySuperAgent = selectMultiRecords("select userID, username, name, agentContactPerson, agentCompany from ".TBL_ADMIN_USERS." where parentID > 0 and agentType = 'Supper' and adminType='Agent' and isCorrespondent != 'ONLY' order by agentCompany");
					for($i=0; $i < count($querySuperAgent); $i++){
				?>
                  <option value="<?=$querySuperAgent[$i]["userID"]; ?>"><? echo($querySuperAgent[$i]["agentCompany"]." [".$querySuperAgent[$i]["username"]."]"); ?></option>
                  <?
					}
				?>
                </SELECT>
			      <script language="JavaScript">
         	SelectOption(document.forms[0].superAgent, "<?=$superAgent?>");
                                </script></td>
              <td colspan="2">&nbsp;</td>                   
			  </tr>
			 <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Sharing Rate&nbsp;</b></font></td>
                  <td width="26%"><input type="text" name="SUPAShare" value="<?=$SUPAShare?>" maxlength="25"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>Sharing Type</b></font></td>
                  <td width="37%">
                  	<select name="SUPAShareType" style="font-family:verdana; font-size: 11px">
		             <option value="percent">- Percentage of Margin -</option>
		             <option value="fixed">- Fixed -</option>
		            </select>
		         <script language="JavaScript">SelectOption(document.forms[0].SUPAShareType,"<?=$SUPAShareType?>");</script>
                  </td>
                </tr>
                <tr>
                	<td>
                		&nbsp;
                	</td>
                </tr>
                
                <tr bgcolor="#DFE6EA">
			 	<td width="35%" align="right">
				  Super Distributor</td>
			    <td width="65%">
			    	<SELECT name="superIDA" style="font-family:verdana; font-size: 11px">
                  <OPTION value="">- Select Super Distributor-</OPTION>
                 <?          
					$querySuperIDA = selectMultiRecords("select userID, username, name, agentContactPerson, agentCompany from ".TBL_ADMIN_USERS." where parentID > 0 and agentType = 'Supper' and adminType='Agent' and isCorrespondent != 'N' order by agentCompany");
					for($i=0; $i < count($querySuperIDA); $i++){
				?>
                  <option value="<?=$querySuperIDA[$i]["userID"]; ?>"><? echo($querySuperIDA[$i]["agentCompany"]." [".$querySuperIDA[$i]["username"]."]"); ?></option>
                  <?
					}
				?>
                </SELECT>
			      <script language="JavaScript">
         	SelectOption(document.forms[0].superIDA, "<?=$superIDA?>");
                                </script></td>
                <td colspan="2">&nbsp;</td>                 
			  </tr>
			   <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Sharing Rate&nbsp;</b></font></td>
                  <td width="26%"><input type="text" name="SUPIDAShare" value="<?=$SUPIDAShare?>" maxlength="25"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>Sharing Type</b></font></td>
                  <td width="37%">
                  	<select name="SUPIDAShareType" style="font-family:verdana; font-size: 11px">
		             <option value="percent">- Percentage of Margin -</option>
		             <option value="fixed">- Fixed -</option>
		            </select>
		         <script language="JavaScript">SelectOption(document.forms[0].SUPIDAShareType,"<?=$SUPIDAShareType?>");</script>
                  </td>
                </tr>
			  
			  <tr>
                	<td>
                		&nbsp;
                	</td>
                </tr>
                
                <tr bgcolor="#DFE6EA">
			 	<td width="35%" align="right">
				  Sub Agent</td>
			    <td width="65%">
			    	<SELECT name="subAgent" style="font-family:verdana; font-size: 11px">
                  <OPTION value="">- Select Sub Agent-</OPTION>
                 <?          
					$querySubAgent = selectMultiRecords("select userID, username, name, agentContactPerson, agentCompany from ".TBL_ADMIN_USERS." where parentID > 0 and agentType = 'Sub' and adminType='Agent' and isCorrespondent != 'ONLY' order by agentCompany");
					for($i=0; $i < count($querySubAgent); $i++){
				?>
                  <option value="<?=$querySubAgent[$i]["userID"]; ?>"><? echo($querySubAgent[$i]["agentCompany"]." [".$querySubAgent[$i]["username"]."]"); ?></option>
                  <?
					}
				?>
                </SELECT>
			      <script language="JavaScript">
         	SelectOption(document.forms[0].subAgent, "<?=$subAgent?>");
                                </script></td>
                <td colspan="2">&nbsp;</td>                 
			  </tr>
			 <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Sharing Rate&nbsp;</b></font></td>
                  <td width="26%"><input type="text" name="subAgentShare" value="<?=$subAgentShare?>" maxlength="25"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>Sharing Type</b></font></td>
                  <td width="37%">
                  	<select name="subAgentShareType" style="font-family:verdana; font-size: 11px">
		             <option value="percent">- Percentage of Margin -</option>
		             <option value="fixed">- Fixed -</option>
		            </select>
		         <script language="JavaScript">SelectOption(document.forms[0].subAgentShareType,"<?=$subAgentShareType?>");</script>
                  </td>
                </tr>
               <tr bgcolor="#ededed"> 
                  <td width="20%" align="right"><font color="#005b90"><b>Sharing From</b></font></td>
                  <td width="37%">
                  	<select name="subAgentShareFrom" style="font-family:verdana; font-size: 11px">
		             <option value="parent">- Parent Agent -</option>
		             <option value="company">- <?=COMPANY_NAME?> -</option>
		            </select>
		         <script language="JavaScript">SelectOption(document.forms[0].subAgentShareFrom,"<?=$subAgentShareFrom?>");</script>
                  </td>
                   <td colspan="2">&nbsp;</td>
                </tr>
                
                
                
                <tr>
                	<td>
                		&nbsp;
                	</td>
                </tr>
                
                <tr bgcolor="#DFE6EA">
			 	<td width="35%" align="right">
				  Sub Distributor</td>
			    <td width="65%">
			    	<SELECT name="subIDA" style="font-family:verdana; font-size: 11px">
                  <OPTION value="">- Select Sub Distributor -</OPTION>
                 <?          
					$querySubIDA = selectMultiRecords("select userID, username, name, agentContactPerson, agentCompany from ".TBL_ADMIN_USERS." where parentID > 0 and agentType = 'Sub' and adminType='Agent' and isCorrespondent != 'N' order by agentCompany");
					for($i=0; $i < count($querySubIDA); $i++){
				?>
                  <option value="<?=$querySubIDA[$i]["userID"]; ?>"><? echo($querySubIDA[$i]["agentCompany"]." [".$querySubIDA[$i]["username"]."]"); ?></option>
                  <?
					}
				?>
                </SELECT>
			      <script language="JavaScript">
         	SelectOption(document.forms[0].subIDA, "<?=$subIDA?>");
                                </script></td>
                <td colspan="2">&nbsp;</td>                 
			  </tr>
			 <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Sharing Rate&nbsp;</b></font></td>
                  <td width="26%"><input type="text" name="subIDAShare" value="<?=$subIDAShare?>" maxlength="25"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>Sharing Type</b></font></td>
                  <td width="37%">
                  	<select name="subIDAShareType" style="font-family:verdana; font-size: 11px">
		             <option value="percent">- Percentage of Margin -</option>
		             <option value="fixed">- Fixed -</option>
		            </select>
		         <script language="JavaScript">SelectOption(document.forms[0].subIDAShareType,"<?=$subIDAShareType?>");</script>
                  </td>
                </tr>
               <tr bgcolor="#ededed"> 
                  <td width="20%" align="right"><font color="#005b90"><b>Sharing From</b></font></td>
                  <td width="37%">
                  	<select name="subIDAShareFrom" style="font-family:verdana; font-size: 11px">
		             <option value="parent">- Parent Distributor -</option>
		             <option value="company">- <?=COMPANY_NAME?> -</option>
		            </select>
		         <script language="JavaScript">SelectOption(document.forms[0].subIDAShareFrom,"<?=$subIDAShareFrom?>");</script>
                  </td>
                  <td colspan="2">&nbsp;</td>
                </tr>
                
                 
			   <tr> 
			    <td align="center" valign="top" bgcolor="#DFE6EA" colspan="2">
			    	<input name="View" type="submit" class="flat" value="View">
			    </td>
			    <td align="center" bgcolor="#DFE6EA" colspan="2">
			    	<input name="Save" type="submit" class="flat" value="Save">
			    </td>
			    </tr>
			   </form>
			</table>
		    <br>
        </fieldset></td>
  </tr>
</table>

	</td>
  </tr>
</table>
</body>
</html>