<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: Add Beneficiary
	 * @author: Mirza Arslan Baig
	 */
	session_start();  
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php?notLogin=1");
	}
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	
	
	include_once "includes/functions.php";
	include_once "javascript/audit-functions.php";
	$benCountries="select countryId,countryName,isocode,ibanlength from countries";
		//debug($benCountries);
		$benCountriesResult=selectMultiRecords($benCountries);
		//debug($benCountriesResult);
 
	if(!empty($_POST['addBen']) || !empty($_POST['updateBen'])){

		$patternIBAN = '/((NO)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(NO)[0-9A-Z]{15}|(BE)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BE)[0-9A-Z]{16}|(DK|FO|FI|GL|NL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(DK|FO|FI|GL|NL)[0-9A-Z]{18}|(MK|SI)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MK|SI)[0-9A-Z]{19}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{20}|(HR|LI|LV|CH)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(HR|LI|LV|CH)[0-9A-Z]{21}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{22}|(GI|IL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(GI|IL)[0-9A-Z]{23}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{24}|(PT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(PT)[0-9A-Z]{25}|(IS|TR)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(IS|TR)[0-9A-Z]{26}|(FR|GR|IT|MC|SM)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(FR|GR|IT|MC|SM)[0-9A-Z]{27}|(AL|CY|HU|LB|PL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AL|CY|HU|LB|PL)[0-9A-Z]{28}|(MU)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(MU)[0-9A-Z]{30}|(MT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MT)[0-9A-Z]{31})$/';
		$strIBAN = mysql_real_escape_string(trim($_POST['iban']));
		$strBenName = mysql_real_escape_string(trim($_POST['benName']));
		
		$strBenCountry = mysql_real_escape_string($_POST['benCountry']);
		$strBenCountryId = $strBenCountry;
		$strBenCurrency = mysql_real_escape_string(trim($_POST['benCurrency']));
		$strSwiftCode = mysql_real_escape_string(trim($_POST['swift']));
		$strSortCode = mysql_real_escape_string(trim($_POST['sortCode']));
		$strReference = mysql_real_escape_string(trim($_POST['reference']));
		$strAccountName = mysql_real_escape_string(trim($_POST['accountName']));
		$strRoutingNumber = mysql_real_escape_string(trim($_POST['routingNumber']));
		$strAccountNumber = mysql_real_escape_string(trim($_POST['accountNumber']));
		$strSendingCurrency = mysql_real_escape_string(trim($_POST['sendingCurrency']));
		$strBranchNameNumber = mysql_real_escape_string(trim($_POST['branchNameNumber']));
		$strBranchAddress = mysql_real_escape_string(trim($_POST['branchAddress']));
		$strBranchAddress=cleanString($strBranchAddress);
		$intCustomerID = $_SESSION['loggedInUser']['userID'];
		if(!empty($_POST['benID'])){
			$intBenID = mysql_real_escape_string(trim($_POST['benID']));
		}
		$strCurrentDate = date("Y-m-d");
		 
			if(!empty($intBenID)){
				$benCountry = selectFrom("select countryName from countries where countryId = '".$strBenCountry."' ");
				$strBenCountry = $benCountry["countryName"];
				
				$strQueryUpdateBen = "UPDATE ".TBL_BENEFICIARY." SET `firstName` = '$strBenName', `beneficiaryName` = '$strBenName', `Country` = '$strBenCountry', `reference` = '$strReference' WHERE benID = '$intBenID'";
				$getPreviousArr = "SELECT * FROM ben_banks WHERE benID = '$intBenID'";
				$fetPreviousArr = selectFrom($getPreviousArr);
		
				$getPreviouBensArr = "SELECT * FROM beneficiary WHERE benID = '$intBenID'";
				$fetPreviousBenArr = selectFrom($getPreviouBensArr);
				$benName = $fetPreviousBenArr["firstName"]." ".$fetPreviousBenArr["middleName"]." ".$fetPreviousBenArr["lastName"];
				
				if(update($strQueryUpdateBen)){
					$strMsg = "Beneficiary ".$benName."updated successfully";
					
				activities($_SESSION["loginHistoryID"],"UPDATED",$intBenID,"beneficiary","beneficiary updated successfully");
				 
				logChangeSet($fetPreviousArr,$_SESSION["loggedInUser"]["accountName"],"ben_banks","benId",$intBenID,$_SESSION["loggedInUser"]["accountName"]);
				
				logChangeSet($fetPreviousBenArr,$_SESSION["loggedInUser"]["accountName"],"beneficiary","benId",$intBenID,$_SESSION["loggedInUser"]["accountName"]);
				
				

		
				   
				}else{
					$strError = "Beneficiary cannot be updated";
				}
			}else{
				$benCountry = selectFrom("select countryName from countries where countryId = '".$strBenCountry."' ");
				$strBenCountry = $benCountry["countryName"];
				$strQueryInsertBen = "INSERT INTO ".TBL_BENEFICIARY." (`firstName`, `middleName`, `lastName`, `beneficiaryName`, `Country`, `reference`, `customerID`, `created`) VALUES('$strBenName', '', '', '$strBenName', '$strBenCountry', '$strReference', '$intCustomerID', '$strCurrentDate')";
				if(insertInto($strQueryInsertBen)){
				$BenID = mysql_insert_id();
			
			$getPreviouBensArr = "SELECT * FROM beneficiary WHERE benID = '$BenID'";
				$fetPreviousBenArr = selectFrom($getPreviouBensArr);
			
			$benName = $fetPreviousBenArr["firstName"]." ".$fetPreviousBenArr["middleName"]." ".$fetPreviousBenArr["lastName"];
			 
			 activities($_SESSION["loginHistoryID"],"INSERTION",$BenID,"beneficiary","beneficiary ".$benName." added successfully");
					$strMsg = "Beneficiary ".$benName." added successfully";
					//$intBenID = mysql_insert_id();
				}else{
					$strError = "Beneficiary cannot be added";
				}
			}
			if($BenID!="")
			$intBenID=$BenID;
			
		
			if(!empty($intBenID)){
				$strQueryChkBankInfo = "SELECT count(id) AS record FROM ".TBL_BEN_BANK_DETAILS." WHERE benID = '$intBenID'";
				$arrBenBankInfo = selectFrom($strQueryChkBankInfo);

				if($arrBenBankInfo['record'] == 0){
				
					$strQueryInsertBenBank = "INSERT INTO ".TBL_BEN_BANK_DETAILS." 
					(	benId, 
						IBAN, 
						swiftCode,
						sortCode,
						bankName,
						accountNo,
						routingNumber,
						branchCode,
						branchAddress,
						sendCurrency
					) 
					VALUES
					(	'$intBenID',
						'$strIBAN', 
						'$strSwiftCode',
						'$strSortCode', 
						'$strBenName',
						'$strAccountNumber',
						'$strRoutingNumber',
						'$strBranchNameNumber',
						'$strBranchAddress',
						'$strSendingCurrency'
					)";
				 
					if(!insertInto($strQueryInsertBenBank)){
						$strError .= " and beneficiary bank info cannot added.";
				}else{
activities($_SESSION["loginHistoryID"],"INSERTION",$_SESSION["loggedInUser"]["accountName"],"bankDetails","bankDetails added successfully");
				}
				}else{
				
					$strQueryUpdateBenBank = "UPDATE ".TBL_BEN_BANK_DETAILS." SET";
				 if(!empty($strIBAN)){ 
					$strQueryUpdateBenBank .="`IBAN` = '$strIBAN',"; 
					 } 
					if(!empty($strSwiftCode)){ 
					$strQueryUpdateBenBank .="`swiftCode` = '$strSwiftCode',"; 
					} 
					/*
					if(!empty($strBenAccountName)){ 
					$strQueryUpdateBenBank .="`bankName` = '$strBenAccountName',"; 
					} 
					*/
					if(!empty($strBenName)){ 
					$strQueryUpdateBenBank .="`bankName` = '$strBenName',"; 
					}
				//	if(!empty($strAccountNumber)){
					$strQueryUpdateBenBank .="`accountNo` = '$strAccountNumber',"; 
					//} 
					if(!empty($strBranchNameNumber)){ 
					$strQueryUpdateBenBank .="`branchCode` = '$strBranchNameNumber',"; 
					} 
					 if(!empty($strBranchAddress)){
					$strQueryUpdateBenBank .="`branchAddress` = '$strBranchAddress',"; 
					} 
					$strQueryUpdateBenBank .="`sortCode` = '$strSortCode',"; 
					 if(!empty($strRoutingNumber)){ 
					$strQueryUpdateBenBank .="`routingNumber` = '$strRoutingNumber',"; 
					} 
					if(!empty($strSendingCurrency)){
					$strQueryUpdateBenBank .="`sendCurrency` = '$strSendingCurrency',"; 
					} 
					$strQueryUpdateBenBank = substr($strQueryUpdateBenBank,0,strlen($strQueryUpdateBenBank )-1);
					$strQueryUpdateBenBank .= " WHERE benId = '$intBenID'";
				 
					if(update($strQueryUpdateBenBank)){
						$strMsg .= " and beneficiary bank info is updated";

						
activities($_SESSION["loginHistoryID"],"UPDATED",$_SESSION["loggedInUser"]["accountName"],"bankDetails","ben_banks added successfully");


$getPreviousArr2 = "SELECT * FROM ben_banks WHERE benID = '$intBenID'";
$fetPreviousArr2 = selectFrom($getPreviousArr2);
 

logChangeSet($fetPreviousArr,$_SESSION["loggedInUser"]["accountName"],"ben_banks","benId",$intBenID,$_SESSION["loggedInUser"]["accountName"]);						
						 }
					else
						$strError .= " and beneficiary bank info cannot be updated";
				}
			}
		
	}
	//debug($intBenID);
	if(!empty($_POST['ben'])){
		$intBenID = trim($_POST['ben']);
		$strQueryBen = "SELECT countries.countryId, ben.benID, ben.firstName AS benName, ben.Country, ben.reference, bd.swiftCode,bd.sortCode, bd.IBAN, bd.bankName,bd.routingNumber, bd.accountNo,bd.branchName,bd.branchAddress,bd.sendCurrency,bd. 	branchCode FROM ".TBL_BENEFICIARY." AS ben LEFT JOIN ".TBL_BEN_BANK_DETAILS." AS bd ON bd.benId = ben.benID left join countries on ben.Country=countries.countryName WHERE ben.customerID = '{$_SESSION['loggedInUser']['userID']}' AND ben.status = 'Enabled' AND ben.benID = '$intBenID'";
	
		$arrBen = selectFrom($strQueryBen);
		 
		 
			
		$strIBAN = $arrBen['IBAN'];
		$strBenName = $arrBen['benName'];
		$strSendingCurrency = $arrBen['sendCurrency'];
		$strBenCountry = $arrBen['Country'];
		$strBenCountryId = $arrBen['countryId'];
		$strSwiftCode = $arrBen['swiftCode'];
		$strSortCode = $arrBen['sortCode'];
		$strReference = $arrBen['reference'];
		$intCustomerID = $arrBen['customerID'];
		$strAccountName = $arrBen['bankName'];
		$strRoutingNumber=$arrBen['routingNumber'];
		$strAccountNumber = $arrBen['accountNo'];
		$strBranchNameNumber = $arrBen['branchCode'];
		$strBranchAddress = $arrBen['branchAddress'];
 
	}
		$strUserId=$_SESSION['loggedInUser']['accountName'];
		$strEmail=$_SESSION['loggedInUser']['email'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php if(!empty($intBenID)) echo "Edit"; else echo "Add"; ?> Beneficiary</title>
		<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." /><meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading, Premier FX, Algarve, Portugal, www.premfx.com" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="imagetoolbar" content="no">
	<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
		<link href="css/datePicker.css" rel="stylesheet" type="text/css" />
		
		<link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />
		<!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->
		
		
		<script type="text/javascript" src="jquery.js"></script>
		<script type="text/javascript" src="javascript/jquery.validate.js"></script>
		<script type="text/javascript" src="javascript/date.js"></script>
		<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
		<script type="text/javascript" src="javascript/jquery.maskedinput.min.js"></script>
		<script type="text/javascript" src="javascript/ajaxupload.js"></script>
        	<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/jquery.validate.js"></script>
		<script src="scripts/bootstrap-twipsy.js"></script>
		<script src="scripts/bootstrap-popover.js"></script>
        <script type="text/javascript" src="../admin/javascript/jquery.maskedinput.min.js"></script>
	</head>
	<body>
		
<!-- main container-->
<div class="container">
<div class="upper_container">
<div class="header">
<div class="logo">
<a href="http://www.premfx.com"><img id="logo" src="images/logo_png.png"></a>
</div>
<div class="menu">
<ul class="menu_ul">
<li class="menu_li"><a href="make_payment-ii-new.php" class="menu_link" target="_parent">Send Money</a></li>
<li class="menu_li"><a href="transaction_history_new.php" class="menu_link" target="_parent">Transaction History</a></li>
<li class="menu_li"><a href="view_beneficiariesnew.php" class="menu_link menu_selected"  target="_parent">Beneficiaries</a></li>
<li class="menu_li"><a href="change_passwordnew.php" class="menu_link" target="_parent">Change Password</a></li>
<li class="menu_li"><a href="contact_us.php" class="menu_link" target="_parent">Contact Us</a></li>
<li class="menu_li"><a href="faqs.php" class="menu_link" target="_parent">FAQs</a></li>
</ul>
</div>
</div>
</div>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
<div class="logout_area">
<h2 class="heading">Add Beneficiary Details</h2>
<?php include('top-menu.php');?>


<!-- content area -->
<div class="content_area">
<div class="content_area_center">
<p class="content_heading">Add Beneficiary Information</p>
<p class="content_subheading"></p>
</div>
<!-- content area left-->
<div class="content_area_left">
<form id="addBenForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" onSubmit="return checkAgree();" autocomplete="off">

<div class="field_wrapper">
<label class="form_label">Beneficiary Country:<span style="color:red">*</span></label><br>

	<select name="benCountry" id="benCountry" class="select_field">
	
	<option value="">- Select country for beneficiary -</option>
	<?php 
	$strQueryCountries = "select countries.countryId, countries.countryName from countries right join ben_country_rule_bank_details on countries.countryId=ben_country_rule_bank_details.benCountry where ben_country_rule_bank_details.status = 'Enable' GROUP BY countries.countryName ORDER BY countries.countryName ASC";
												
	$arrCountry = selectMultiRecords($strQueryCountries);
	
	for($x = 0;$x < count($arrCountry);$x++){
	
	echo "<option value='".$arrCountry[$x]['countryId']."'>".$arrCountry[$x]['countryName']."</option>";
	}
	
	/** 
	$strQueryCountries = "SELECT DISTINCT countryName AS countryName, countryCode, countryId FROM ".TBL_COUNTRIES." WHERE countryCode != '' GROUP BY countryName ORDER BY countryName ASC";
	$arrCountry = selectMultiRecords($strQueryCountries);
	for($x = 0;$x < count($arrCountry);$x++){
	echo "<option value='".$arrCountry[$x]['countryId']."'>".$arrCountry[$x]['countryName']." - ".$arrCountry[$x]['countryCode']."</option>";
												}
	*/
?>
	</select>
	
</div>

<div class="field_wrapper">
<label class="form_label">Currency You Wish To Send:</label><br>

	<select name="sendingCurrency" id="sendingCurrency" type="text" class="select_field">
											<option value="">- Select Currency -</option>
<?php 
	$strQueryCurrency = "SELECT currencies, COUNT(*) AS c FROM   (SELECT quotedCurrency AS currencies FROM ".TBL_FX_RATES_MARGIN." UNION ALL SELECT baseCurrency AS currencies FROM ".TBL_FX_RATES_MARGIN.") AS tg GROUP BY currencies;";			

	   $arrCurrency = selectMultiRecords($strQueryCurrency);
												
		for($x = 0;$x < count($arrCurrency);$x++)
			echo "<option value='".$arrCurrency[$x]['currencies']."'>".$arrCurrency[$x]['currencies']."</option>";
?>
	</select>
	
</div>



<div id="accountNameCont" class="field_wrapper">
<label class="form_label">Beneficiary Account Name:<span style="color:red">*</span></label><br>

	<input name="benName" id="benName" class="form_fields" type="text" maxlength ="30"  autocomplete="off" />
</div>

<div id="accountNumberCont" class="field_wrapper">
<label class="form_label">Account Number:<span style="color:red">*</span></label><br>

<input onblur="check_field(this.id);" name="accountNumber" id="accountNumber" type="text" class="form_fields" maxlength="40" autocomplete="off" />
</div>

<div id="branchNameNumberCont" class="field_wrapper">
<label class="form_label">Branch Name / Number:<span style="color:red">*</span></label><br>

	<input name="branchNameNumber" id="branchNameNumber" type="text" class="form_fields" maxlength="40" autocomplete="off"/>
</div>

<div id="branchAddressCont" class="field_wrapper">
<label class="form_label">Branch Address:<span style="color:red">*</span></label><br>

<input name="routingNumber" id="routingNumber" type="text" class="form_fields" maxlength="40" autocomplete="off"/>
</div>


<!--<div class="field_wrapper">

	<?php 
								if(empty($intBenID)){
							?>
							<input name="addBen" id="addBen" type="submit" value="Add" class="submit_btn" onclick="return checkValidations();"/>
							
						
							
							<?php 
								}else{
							?>
							<input name="updateBen" id="updateBen" type="submit" value="Update" class="submit_btn" onclick="return checkValidations();" />
							<input name="benID" type="hidden" value="<?php echo $intBenID; ?>" />
							<input name="sendCurrencyVal" type="hidden" value="" />
							<?php 
								}
							?>
		<input name="clear" id="clear" type="reset" value="Clear Details" class="clear_btn"/>					
</div>-->


 
</div>




<!-- content area right-->
<div class="content_area_right" style="margin-top:31px;">

<div id="routingNumberCont" class="field_wrapper">
<label class="form_label">Routing Number:<span style="color:red">*</span></label><br>

<input name="routingNumber" id="routingNumber" type="text" class="form_fields" maxlength="40" autocomplete="off"/>
</div>

<div id="sortCodeCont" class="field_wrapper">
<label class="form_label">Sort Code:<span style="color:red">*</span></label><br>

<input onblur="" name="sortCode" id="sortCode" type="text" class="form_fields"  maxlength="40" autocomplete="off" />
</div>

<div id="chackboxnew" style="display:none;" class="field_wrapper">

<input  id="chackbox_new" name="chackbox_new" type="checkbox" value="chackbox_new" onclick="CheckValidation(this)" class="bottom" />
<span class="customer_rn_vl" style="font:size:11px;">Please mark the check box if u want to add IBAN instead of SortCode and Account No.</span>
</div>



<div id="ibanCont" class="field_wrapper">
<label class="form_label">IBAN Number:<span style="color:red">*</span></label><br>

<input name="iban" id="iban" type="text" class="form_fields" maxlength="40" onBlur = "checkAgree();" autocomplete="off"/>
                                            <input name="isiban" id = "isiban" type="hidden" />
											
</div>

<div id="swiftCont" class="field_wrapper">
<label class="form_label">SWIFT Code:<span style="color:red">*</span></label><br>

<input name="swift" id="swift" type="text" class="form_fields" maxlength="16" autocomplete="off"/>
<span class="please_do"></span>
</div>

<div class="field_wrapper">
<label class="form_label">Your Reference:</label><br>

<input name="reference" id="reference" maxlength="15" type="text" class="form_fields" autocomplete="off"/>
</div>

 

</div>


<div class="button_wrapper">

	<?php 
								if(empty($intBenID)){
							?>
							<input name="addBen" id="addBen" type="submit" value="Add" class="submit_btn" onclick="return checkValidations();"/>
							
						
							
							<?php 
								}else{
							?>
							<input name="updateBen" id="updateBen" type="submit" value="Update" class="submit_btn" onclick="return checkValidations();" />
							<input name="benID" type="hidden" value="<?php echo $intBenID; ?>" />
							<input name="sendCurrencyVal" type="hidden" value="" />
							<?php 
								}
							?>
		<input name="clear" id="clear" type="reset" value="Clear Details" class="clear_btn"/>					
</div>

</form>



							
	<?php 
								if(isset($strError) && !empty($strError)){
							?>
							<div class="error_msg"><?php echo $strError; ?></div>
							
							<?php 
								}
								if(isset($strMsg) && !empty($strMsg)){
							?>
							<div class="error_msg"><?php echo $strMsg; ?></div>
							
							<?php 
								}
							?>						
</div>


							
<!-- content area ends here-->
</div>

<!-- footer area -->
<?php include('footer.php');?>
</div>



</div>
</body>
                <script>
		<?php
			
		/*$strCountries = "{";
		for($ii=0;$ii<count($benCountriesResult);$ii++)
		{
			$arrCount[] = "'".$benCountriesResult[$ii]["countryName"]."':{'c':'".$benCountriesResult[$ii]["countryName"]."','i':'".$benCountriesResult[$ii]["isocode"]."','g':'".$benCountriesResult[$ii]["ibanlength"]."'}";
			//$strCountries .= 
		}
		$strCountries .= implode(",",$arrCount);
		$strCountries .= "}";*/
		$strCountries = "Array(";
		for($ii=0;$ii<count($benCountriesResult);$ii++)
		{
			$arrCount[] = "Array('".strtoupper($benCountriesResult[$ii]["countryName"])."','".strtoupper($benCountriesResult[$ii]["isocode"])."','".$benCountriesResult[$ii]["ibanlength"]."','".$benCountriesResult[$ii]["countryId"]."')";
			//$strCountries .= 
		}
		$strCountries .= implode(",",$arrCount);
		$strCountries .= ")";
		
		echo 'var arrCountrues ='.$strCountries.';'; 
		?>
				/*$(document).ready(
			function() {
					$('#iban').mask('aa99 **** 9999 9999 9999 9999 9999');

					
			});*/
		
		
		function isValidIBAN($v){ //This function check if the checksum if correct
			
			$v = $v.replace(/^(.{4})(.*)$/,"$2$1"); //Move the first 4 chars from left to the right
			$v = $v.replace(/[A-Z]/g,function($e){return $e.charCodeAt(0) - 'A'.charCodeAt(0) + 10}); //Convert A-Z to 10-25
			var $sum = 0;
			var $ei = 1; //First exponent 
			for(var $i = $v.length - 1; $i >= 0; $i--){
				$sum += $ei * parseInt($v.charAt($i),10); //multiply the digit by it's exponent 
				$ei = ($ei * 10) % 97; //compute next base 10 exponent  in modulus 97
			}; 
			return $sum % 97 == 1;
		}
		
		var $patterns = { //Map automatic generated by IBAN-Patterns Online Tool
				IBAN: function($v){
					//alert($v);
					//alert($v.length);
					$v = $v.replace(/[ ]/g,''); 
					return /^AL\d{10}[0-9A-Z]{16}$|^AD\d{10}[0-9A-Z]{12}$|^AT\d{18}$|^BH\d{2}[A-Z]{4}[0-9A-Z]{14}$|^BE\d{14}$|^BA\d{18}$|^BG\d{2}[A-Z]{4}\d{6}[0-9A-Z]{8}$|^HR\d{19}$|^CY\d{10}[0-9A-Z]{16}$|^CZ\d{22}$|^DK\d{16}$|^FO\d{16}$|^GL\d{16}$|^DO\d{2}[0-9A-Z]{4}\d{20}$|^EE\d{18}$|^FI\d{16}$|^FR\d{12}[0-9A-Z]{11}\d{2}$|^GE\d{2}[A-Z]{2}\d{16}$|^DE\d{20}$|^GI\d{2}[A-Z]{4}[0-9A-Z]{15}$|^GR\d{9}[0-9A-Z]{16}$|^HU\d{26}$|^IS\d{24}$|^IE\d{2}[A-Z]{4}\d{14}$|^IL\d{21}$|^IT\d{2}[A-Z]\d{10}[0-9A-Z]{12}$|^[A-Z]{2}\d{5}[0-9A-Z]{13}$|^KW\d{2}[A-Z]{4}22!$|^LV\d{2}[A-Z]{4}[0-9A-Z]{13}$|^LB\d{6}[0-9A-Z]{20}$|^LI\d{7}[0-9A-Z]{12}$|^LT\d{18}$|^LU\d{5}[0-9A-Z]{13}$|^MK\d{5}[0-9A-Z]{10}\d{2}$|^MT\d{2}[A-Z]{4}\d{5}[0-9A-Z]{18}$|^MR13\d{23}$|^MU\d{2}[A-Z]{4}\d{19}[A-Z]{3}$|^MC\d{12}[0-9A-Z]{11}\d{2}$|^ME\d{20}$|^NL\d{2}[A-Z]{4}\d{10}$|^NO\d{13}$|^PL\d{10}[0-9A-Z]{,16}n$|^PT\d{23}$|^RO\d{2}[A-Z]{4}[0-9A-Z]{16}$|^SM\d{2}[A-Z]\d{10}[0-9A-Z]{12}$|^SA\d{4}[0-9A-Z]{18}$|^RS\d{20}$|^SK\d{22}$|^SI\d{17}$|^ES\d{22}$|^SE\d{22}$|^CH\d{7}[0-9A-Z]{12}$|^TN59\d{20}$|^TR\d{7}[0-9A-Z]{17}$|^AE\d{21}$|^GB\d{2}[A-Z]{4}\d{14}$/.test($v) && isValidIBAN($v);		
				},
		};
		//alert($patterns.IBAN('CY17 0020 0128 0000 0012 0052 7600'));
		
		
		
		
		
			function SelectOption(OptionListName, ListVal){
				for (i=0; i < OptionListName.length; i++)
				{
					if (OptionListName.options[i].value == ListVal)
					{
						OptionListName.selectedIndex = i;
						break;
					}
				}
			}
			$('#countryTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#currencyTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#benNameTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#accountNameTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#accountNumberTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#branchNameNumberTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#branchAddressTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#routingNumberTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#sortCodeTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#ibanTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#swiftTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#refTip').popover({
				trigger: 'hover',
				offset: 5
			});
			<?php
				if($strError){
					
					//debug($arrBenInfo);
			?>
				SelectOption(document.forms[0].benCountry, '<?php echo $strBenCountryId; ?>');
				 $('#sendingCurrency').val('<?php echo $strSendingCurrency; ?>');
				$('#benName').val('<?php echo $strAccountName; ?>');
				$('#accountName').val('<?php echo $strAccountName; ?>');
				$('#routingNumber').val('<?php echo $strRoutingNumber; ?>');
				$('#branchAddress').val('<?php echo $strBranchAddress; ?>');
				
				 $('#benCurrency').val('<?php echo $arrBenInfo['currency']; ?>');
				$('#iban').focus();
				//$('#swift').val('<?php echo $strSwiftCode; ?>');
				$('#sortCode').val('<?php echo $strSortCode; ?>');
				$('#accountNumber').val('<?php echo $strAccountNumber ?>');
				$('#reference').val('<?php echo $strReference; ?>');
				$('#branchNameNumber').val('<?php echo $strBranchNameNumber; ?>');
			<?php 
				}

				if(!empty($intBenID)){
?>
				SelectOption(document.forms[0].benCountry, '<?php echo $strBenCountryId; ?>');
				$('#benName').val('<?php echo $strBenName; ?>');
				$('#accountName').val('<?php echo $strAccountName; ?>');
				$('#routingNumber').val('<?php echo $strRoutingNumber; ?>');
				 $('#sendingCurrency').val('<?php echo $strSendingCurrency; ?>');
				$('#iban').val('<?php echo $strIBAN; ?>');
				$('#swift').val('<?php echo $strSwiftCode; ?>');
				$('#sortCode').val('<?php echo $strSortCode; ?>');
				$('#accountNumber').val('<?php echo $strAccountNumber ?>');
				$('#reference').val('<?php echo $strReference; ?>');
				$('#branchNameNumber').val('<?php echo $strBranchNameNumber; ?>');
				$('#branchAddress').val('<?php echo $strBranchAddress; ?>');
			<?php 
				}
			?>
			
		
		var changeStausFlag='YES';
	function changeStausValidation()
		{
		   
		//var countryVal2=$("#sendBenValue").val();
		var countryVal2= $("#benCountry option:selected").text();
		
		
		//alert(countryVal2);
		
		/**1=Uk*/
		
		if((countryVal2=='United Kingdom'))
		{
			
		$('#countrymsgnote').html("<b>Enter an account no. or IBAN</b>");
		
		var test = $('#accountNumber').val();
		
		
		if($('#accountNumber').val() !='')
		{
			
		changeStausFlag='no';
		//$('#isiban').val('NO');
		//$("#iban").rules("remove");
		$('#isiban').val('NO');
		$("#iban").rules("remove");
	
		}else if($('#iban').val() !='')
		{

		$("#accountNumber").rules("remove");
		$("#sortCode").rules("remove");
		
		changeStausFlag='YES';
		$('#isiban').val('YES');
		
		}else {
		$('#countrymsgnote').html("");
		changeStausFlag='YES';
		//$("#routingNumber").rules("add");
		//$("#swift").rules("add");
		}
		
		
		}
		}
		
			function checkAgree(){
					var iban = $('#iban').val();
					var country=$('#benCountry').val();
					var checkLength=checkIBANLength(country,iban);
					 
				 var isiban = $('#isiban').val();
				  
					if(checkLength==false && $('#iban').is(':visible')){

					alert("Please enter the correct IBAN number for your beneficiary country");
						//$('#iban').focus();
						return false;
					}
					
					//CY17 0020 0128 0000 0012 0052 7600
					if(!$patterns.IBAN(iban) && $('#iban').is(':visible'))
					{
						alert("Please enter the correct IBAN number for your beneficiary country");
						return false;
					}
					else
					{
						
						return true;
					}
					
			}
				function checkIBANvalid(){
				var iban = $('#iban').val();
			var country=$('#sendBenValue').val();
				
					var checkLength=checkIBANLength(country,iban);
					if(checkLength==false && $('#iban').is(':visible')){
						
						alert("Please enter the correct IBAN number for your beneficiary country");
						return false;
					}
				
				if(!$patterns.IBAN(iban) && $('#iban').is(':visible') )
				{
					
					alert("Please enter the correct IBAN number for your beneficiary country");
					return false;
				}
				else
				{
					return true;
				}
			}
			
			
			 function onChangeCurrency(country){

			$.ajax({
					url:"http://<?php echo $_SERVER['HTTP_HOST']."/premier_fx_online_module/get_currency.php"?>",
					type:"POST",
					data: {
						country: country,
						
					},
					cache:false,
					dataType: 'json',
					success: function(data){
						var countryVal2= $("#benCountry option:selected").text();
	
						if(data['currency'] == 'GBP' && countryVal2=='United Kingdom'){
						/* document.getElementById("ibanCont").style.display= "none";
						document.getElementById("swiftCont").style.display= "none"; */
						//document.getElementById("chackboxnew").style.display= "block";
						
						}

						else {	
					    //document.getElementById("ibanCont").style.display= "block";
						//document.getElementById("swiftCont").style.display= "block";
	             		document.getElementById("chackboxnew").style.display= "none";
						}
						 
						$('#sendingCurrency').val(data['currency']);
						//changeStausValidation();
					}
			});
		} 
		function checkIBANLength(country,iban){
			//alert(arrCountrues.length);
			 
			for (var m=0;m<arrCountrues.length;m++) {
				if(country==arrCountrues[m][3]){
				if(iban.length !=arrCountrues[m][2]){
					
					return false;
					break;
				
				}
				if(iban.substr(0,2) !=arrCountrues[m][1]){
					
					return false;
					break;
				
				}
				}
			
			}
			return true;
		
		}
		
		function getCountry(currency){
				$.ajax({
					url:"http://<?php echo $_SERVER['HTTP_HOST']."/premier_fx_online_module/get_country.php"?>",
					type:"POST",
					data: {
						currency: currency,
						
					},
					cache:false,
					dataType: 'json',
					success: function(dataFun){
						//$('#benCountry').val(dataFun['c']);
						$('#sendBenValue').val(dataFun['c']);
						getBenRule(dataFun['c'],'country');
					
					}
			});
		}
			
			
			function getBenRule(country,field){
				//alert(country);
				currency = $("#sendingCurrency").val();
			 
				$.ajax({
					url: "http://<?php echo $_SERVER['HTTP_HOST']."/premier_fx_online_module/ben_country_base_rule.php"; ?>",
					type: "POST",
					data: {
						country: country,
						currency:currency,
						field:field,
					},
					cache: false,
					dataType: 'json',
					success: function(data){
						if(!data){
							errorTxt = "<p style='font-weight:bold;text-align:justify;'>We are sorry for the inconvenience, we are not allowing online deals in this country. Please call support on the following numbers and place your order by phone.</p><br /><div style='font-weight:bold;'>United Kingdom : +44 (0)845 021 2370 Portugal: +351 289 358 511 Spain:+34 971 576 724</div>";
							//$('#overlay').fadeIn('slow');
							$('#errorRule').fadeIn('slow');
							$('#errorRule').html(errorTxt);
						}else{
							
							$('#errorRule').hide();
							accountName = data.accountName;
							accNo = data.accNo;
							branchNameNumber = data.branchNameNumber;
							branchAddress = data.branchAddress;
							swiftCode = data.swiftCode;
							IBAN = data.iban;
							routingNumber = data.routingNumber;
							sortCode = data.sortCode;
							//$('#sendingCurrency').val("2");
							var getrulesval=accountName+"|"+accNo+"|"+branchNameNumber+"|"+branchAddress+"|"+swiftCode+"|"+IBAN+"|"+routingNumber+"|"+sortCode;
							//if(country != 'United Kingdom')
							{
							if(accountName == 'N'){
								$('#accountName').attr('disabled', 'disabled');
								$('#accountNameCont').fadeOut('fast');
								
							}else if(accountName == 'Y'){
								$('#accountName').removeAttr('disabled');
								$('#accountNameCont').fadeIn('fast');
																
							}
							}
							if(accNo == 'N'){
								$('#accountNumber').attr('disabled', 'disabled');
								$('#accountNumberCont').fadeOut('fast');
							}else if(accNo == 'Y'){
								$('#accountNumber').removeAttr('disabled');
								$('#accountNumberCont').fadeIn('fast');
							}
							
							if(branchNameNumber == 'N'){
								$('#branchNameNumber').attr('disabled', 'disabled');
								$('#branchNameNumberCont').fadeOut('fast');
							}else if(branchNameNumber == 'Y'){
								$('#branchNameNumber').removeAttr('disabled');
								$('#branchNameNumberCont').fadeIn('fast');
							}
							
							if(branchAddress == 'N'){
								$('#branchAddress').attr('disabled', 'disabled');
								$('#branchAddressCont').fadeOut('fast');
							}else if(branchAddress == 'Y'){
								$('#branchAddress').removeAttr('disabled');
								$('#branchAddressCont').fadeIn('fast');
							}
							//alert(country);
							//if(country != 'United Kingdom')
							
							{
							if(swiftCode == 'N'){
								//$('#swift').attr('disabled', 'disabled');
								$('#swiftCont').fadeOut('fast');
							}else if(swiftCode == 'Y'){
								$('#swift').removeAttr('disabled');
								$('#swiftCont').fadeIn('fast');
							}
							}
							//alert(country);
							//if(country != 1)
							{
							if(IBAN == 'N'){
								
							//	$('#iban').attr('disabled', 'disabled');
								$('#ibanCont').fadeOut('fast');
								$('#isiban').val('NO');
							}else if(IBAN == 'Y'){
								
								$('#iban').removeAttr('disabled');
								$('#ibanCont').fadeIn('fast');
								$('#isiban').val('YES');
							}
							}
							if(routingNumber == 'N'){
							//	$('#routingNumber').attr('disabled', 'disabled');
								$('#routingNumberCont').fadeOut('fast');
							}else if(routingNumber == 'Y'){
								$('#routingNumber').removeAttr('disabled');
								$('#routingNumberCont').fadeIn('fast');
							}
							
							if(sortCode == 'N'){
							//if(country != 'United Kingdom')
							{
							//	$('#sortCode').attr('disabled', 'disabled');
								}
								$('#sortCodeCont').fadeOut('fast');
							}else if(sortCode == 'Y'){
								$('#sortCode').removeAttr('disabled');
								$('#sortCodeCont').fadeIn('fast');
							}
						
							CheckValidation(getrulesval);   
						}
						
					}
				});
					
						if(field == 'country'){
						  onChangeCurrency(country);
						}
				  
		}
			
			
			
			if($('#benCountry').val())
				getBenRule($('#benCountry').val(),'currency');
				$('#benCountry').change(
				function(){
					$('#errorRule').hide();
					if($(this).val() != ''){
						$('#iban').val('');
						$('#sendBenValue').val($(this).val());
						getBenRule($(this).val(),'country');
					}
				//countryVal=$(this).attr('');
						
					 	 
				}
			);
			
			 
			
			
			$('#sendingCurrency').change(
				function(){
			 country = $("#benCountry").val();
					 getBenRule(country,'currency');
				}
			);
			
			
			
			
			/*$.validator.addMethod("ibanValidate", function(value, element) {
				//alert('test');
				pattern = /^((NO)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(NO)[0-9A-Z]{15}|(BE)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BE)[0-9A-Z]{16}|(DK|FO|FI|GL|NL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(DK|FO|FI|GL|NL)[0-9A-Z]{18}|(MK|SI)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MK|SI)[0-9A-Z]{19}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{20}|(HR|LI|LV|CH)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(HR|LI|LV|CH)[0-9A-Z]{21}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{22}|(GI|IL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(GI|IL)[0-9A-Z]{23}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{24}|(PT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(PT)[0-9A-Z]{25}|(IS|TR)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(IS|TR)[0-9A-Z]{26}|(FR|GR|IT|MC|SM)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(FR|GR|IT|MC|SM)[0-9A-Z]{27}|(AL|CY|HU|LB|PL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AL|CY|HU|LB|PL)[0-9A-Z]{28}|(MU)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(MU)[0-9A-Z]{30}|(MT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MT)[0-9A-Z]{31})$/;
			  return this.optional(element) || pattern.test(value);
			}, "Please enter correct IBAN number");*/
						$('#addBenForm').validate({
				rules: {
					benCountry:{
						required: true
					}
				}
			});
			
					window.onload = function() {
    document.getElementById("chackbox_new").onclick = function fun() {
	var val="|";
        CheckValidation(val);
    
    }
	}
			
		
			
			
			/* $('#addBenForm').validate({
				rules: {
					benCountry:{
						required: true
					},
					benName: {
						required: true
					},
					accountName: {
						required: true
						
					},
					 accountNumber: {
						required: true
						
					}, 
					branchNameNumber: {
						required: true
						
					},
					branchAddress: {
						required: true
						
					},
					routingNumber: {
						required: true
						
					},
					 sortCode: {
						required: true
						
					},
					
					iban: {
						required: true
					},
					swift: {
						required: true
					}
				}
			});
			
	/** ticket #11612 work  start here 	
		var countryVal2= $("#benCountry option:selected").text();
		if((countryVal2!='United Kingdom'))
		{	
			$('#addBenForm').validate({
			rules: {
					accountNumber: {
						required: true
						
					}, 
					 sortCode: {
						required: true
						
					},
					
				 	iban: {
						required: true
					},
					swift: {
						required: true
					} 
					
				}
				
			});
			}
			
	if((countryVal2=='United Kingdom'))
		{
		$('#addBenForm').validate({
				rules: {
					benCountry:{
						required: true
					},
					benName: {
						required: true
					},
					accountName: {
						required: true
						
					},
					branchNameNumber: {
						required: true
						
					},
					branchAddress: {
						required: true
						
					},
					routingNumber: {
						required: true
						
					}
				}
			});
			}*/
			 function check_field(id) {
            var field = $( "#accountNumber" ).val();

            if (isNaN(field)) {
                alert('Please enter a numeric Account number');
				$( "#accountNumber" ).val('');
            }
			}
			
		
	function CheckValidation(val){
		
	 	var chackbox=$('#chackbox_new').prop('checked');
		
	
		 var myArray = val.split('|'); 
		 //alert(myArray[0]);
		 var accountName=myArray[0];
		 var accNo=myArray[1];
		 var branchNameNumber=myArray[2];
		 var branchAddress=myArray[3];
		 var swiftCode=myArray[4];
		 var IBAN=myArray[5];
		 var routingNumber=myArray[6];
		 var sortCode=myArray[7];
		 
		
		 
		
		var countryVal2= $("#benCountry option:selected").text();
		//alert (countryVal2);
		if((countryVal2=='United Kingdom'))
		{
		var ibanval= $("#iban").val();
			var swiftcodeval= $("#swift").val();
			var existbenval= $("#existBen").val();
		
		//alert(ibanval);
		//alert(swiftcodeval);
		if(ibanval!='' && swiftcodeval!='' && existbenval!='')
		chackbox = true;
		$("#iban").rules("remove");
		$("#swift").rules("remove");
		if(chackbox == true){
		
					$( "#benName" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					
		                    
					});
					
					$( "#branchNameNumber" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					});
					$( "#iban" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					});
					$( "#swift" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					});
		
		document.getElementById("accountNumberCont").style.display= "none";
		//document.getElementById("accountNumber").value= "";
		document.getElementById("sortCodeCont").style.display= "none";
		//document.getElementById("sortCode").value= "";
		document.getElementById("ibanCont").style.display= "block";
		document.getElementById("swiftCont").style.display= "block";
		
			   }
	else if(chackbox== false)
			  {
			  
					$( "#benName" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					});
					$( "#branchNameNumber" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					});
					$( "#sortCode" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					});
					$( "#accountNumber" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					});
		
		document.getElementById("ibanCont").style.display= "none";
		//document.getElementById("iban").value= "";
		document.getElementById("swiftCont").style.display= "none";
		//document.getElementById("swift").value= "";
		document.getElementById("accountNumberCont").style.display= "block";
		document.getElementById("sortCodeCont").style.display= "block";
		
		}
	} 
	else if((countryVal2!='United Kingdom'))
		{
				
			if (accountName=='Y')
			{
			$( "#benName" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					});
					
			}
			if (accNo=='Y')
			{
					$( "#accountNumber" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					});
			}
			if (branchNameNumber=='Y')
			{
					$( "#branchNameNumber" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					});
			}
			if (branchAddress=='Y')
			{
					$( "#branchAddress" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					});
			}
			if (swiftCode=='Y')
			{
					$( "#swift" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					});
			}
			if (IBAN=='Y')
			{
					$( "#iban" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					});
			}
			if (routingNumber=='Y')
			{
					$( "#routingNumber" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
			//alert('It must not exceed 9 characters example:XXXXYYYYC');		
					});
			}
			if (sortCode=='Y')
			{
					$( "#sortCode" ).rules( "add", {
					required: true,
					messages: {
					required: "This field is required."
					}
					});
			}
			

			
			
		} 
}

function checkValidations(){

if($('#benCountry').is(':visible') && $.trim($('#benCountry').val()) == ''){
alert('Select the country please!');
return false;
}

if($('#sendingCurrency').is(':visible') && $.trim($('#sendingCurrency').val()) == ''){
alert('Select the currency please!');
return false;
}

if($('#benName').is(':visible') && $.trim($('#benName').val()) == ''){
alert('Select the Account Name please!');
return false;
}

if($('#branchNameNumber').is(':visible') && $.trim($('#branchNameNumber').val()) == ''){
alert('Enter the Branch Number/Name please!');
return false;
}

if($('#branchAddress').is(':visible') && $.trim($('#branchAddress').val()) == ''){
alert('Enter the Branch Address please!');
return false;
}

if($('#sortCode').is(':visible') && $.trim($('#sortCode').val()) == ''){
alert('Enter the sort code please!');
return false;
}

if($('#accountNumber').is(':visible') && $.trim($('#accountNumber').val()) == ''){
alert('Enter the Account Number please!');
return false;
}

if($('#swift').is(':visible') && $.trim($('#swift').val()) == ''){
alert('Enter the Swift Code please!');
return false;
}

if($('#routingNumber').is(':visible') && $.trim($('#routingNumber').val()) == ''){
alert('Enter the Routing Number please!');
return false;
}
}
	
/**  ticket #11612 work  ends here **/	
		</script>
		<script type="text/javascript">
		</script>

</html> 