<?php
	/**
	 * @package: Online Module	
	 * @subpackage: Confirm Page
	 * @author: Awais Umer
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	include_once "javascript/audit-functions.php";
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	
	dbConnect();
	include_once "includes/functions.php";
	$currency = $_SESSION['sendingCurrency'];
	
	//debug($_SESSION);


	if($_POST['benName'])
	$_SESSION['benName']		= 	$_POST['benName'];
	
	if($_POST['benCountry'])
	$_SESSION['benCountry']		= 	$_POST['benCountry'];
	
	if($_POST['benCurrency'])
	$_SESSION['benCurrency']		= 	$_POST['benCurrency'];
	
	if($_POST['accountName'])
	$_SESSION['accountName']		= 	$_POST['accountName'];
	
	if($_POST['accountNumber'])
	$_SESSION['accountNumber']		=	$_POST['accountNumber'];
	
	if($_POST['branchNameNumber'])
	$_SESSION['branchNameNumber']	=	$_POST['branchNameNumber'];
	
	if($_POST['branchAddress'])
    $_SESSION['branchAddress'] 		=	$_POST['branchAddress'];
	
	if($_POST['routingNumber'])
	$_SESSION['routingNumber']		=	$_POST['routingNumber'];
	
	if($_POST['sortCode'])
    $_SESSION['sortCode'] 			=	$_POST['sortCode'];
	
	if($_POST['swift'])
	$_SESSION['swift']				=	$_POST['swift'];
	
	if($_POST['reference'])
	$_SESSION['reference'] 			=	$_POST['reference'];
	
	if($_POST['existBen'])
    $_SESSION['existBen'] 			=	$_POST['existBen'];
	
	if($_POST['iban'])
	$_SESSION['iban'] 				=	$_POST['iban'];
	
	
	
	$test = $_POST["test"];
	$_SESSION["test"] = $_POST["test"];
	
	
	$strUserId=$_SESSION['loggedInUser']['accountName'];
	$strEmail=$_SESSION['loggedInUser']['email'];
	 
if($_POST["existBen"] != ""){
$getPreviousArr = "SELECT * FROM ben_banks WHERE benID = ".$_POST["existBen"]." ";
$fetPreviousArr = selectFrom($getPreviousArr);	
	
	$updateBenBank = "UPDATE ".TBL_BEN_BANK_DETAILS." SET
	 
	accountNo	=	'".$_POST["accountNumber"]."',
	sortCode	=	'".$_POST["sortCode"]."',
	iban		=	'".$_POST["iban"]."'
	WHERE benId = 	".$_POST["existBen"]."
	";
	if(update($updateBenBank)){
	
	activities($_SESSION["loginHistoryID"],"UPDATED",$_SESSION["loggedInUser"]["accountName"],"ben_banks","ben_banks updated successfully");

logChangeSet($fetPreviousArr,$_SESSION["loggedInUser"]["accountName"],"ben_banks","benId",$_POST["existBen"],$_SESSION["loggedInUser"]["accountName"]);
	
	}
	}
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>Private Client Registration Form - Premier FX Online Currency Transfers, Algarve, Portugal</title>
		<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." /><meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading, Premier FX, Algarve, Portugal, www.premfx.com" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="imagetoolbar" content="no">
			<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
		 <link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />
		<!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->

	<script src="scripts/jquery-1.7.2.min.js"></script>
	<script src="scripts/jquery.validate.js"></script>
	<script>
function changeModeOfPayment(val) {

var Value = val.value;

if(Value=="Bank" || Value=="bank")
{
     document.getElementById("debitCardConfirm").style.display="none";
	  document.getElementById("bankConfirm").style.display="block";
}

if(Value=="Card" || Value=="card")
{
      document.getElementById("bankConfirm").style.display="none";
     document.getElementById("debitCardConfirm").style.display="block";
	 
}

//alert(val.value);   
  
	
   }
</script>
	</head>
	<body>

<div id="wait" style="position:fixed;height:100%;width:100%;background:#222;opacity:0.7;display:none;z-index:999">
					<div style="margin: 0 auto;position: relative;top: 50%;font-size:24px;color:#fff;font-family:arial;text-align:center">
						<img src="images/premier/formImages/loading.gif" alt=""/>
						<span id="loadingMessage" style="white-space:nowrap;"> Loading...</span>
					</div>
				</div>
				
<!-- main container-->
<div class="container">
<div class="upper_container">
<div class="header">
<div class="logo">
<a href="http://www.premfx.com"><img id="logo" src="images/logo_png.png"></a>
</div>
<div class="menu">
<ul class="menu_ul">
<li class="menu_li"><a href="make_payment-ii-new.php" class="menu_link" target="_parent">Send Money</a></li>
<li class="menu_li"><a href="transaction_history_new.php" class="menu_link" target="_parent">Transaction History</a></li>
<li class="menu_li"><a href="view_beneficiariesnew.php" class="menu_link"  target="_parent">Beneficiaries</a></li>
<li class="menu_li"><a href="change_passwordnew.php" class="menu_link" target="_parent">Change Password</a></li>
<li class="menu_li"><a href="contact_us.php" class="menu_link" target="_parent">Contact Us</a></li>
<li class="menu_li"><a href="faqs.php" class="menu_link" target="_parent">FAQs</a></li>
</ul>
</div>
</div>
</div>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
<div class="logout_area">
<h2 class="heading">Payment Method</h2>
<?php include('top-menu.php');?>

<!-- content area -->
<div class="content_area">
<div class="content_area_center">
<p class="content_heading">Payment Method</p>
<p class="content_subheading">Please select the method of payment you wish to choose.<br>
Bank transfer or secure debit card payment.</p>
</div>
<!-- content area left-->
<div class="content_area_left">

<form name="continue" id="continue" method="post">


<div class="field_wrapper" style="margin-bottom:50px;">
<h1 class="heading_beneficiary">Payment Method Selection</h1>
<p class="question_answer">Please select the mode of payment</p>

<input class="input1" type="radio" id="test" name="test" value="bank"  onClick="changeModeOfPayment(this);"/><label class="form_label">Bank Transfer</label>
<br>
<input class="input1" type="radio" id="test1" name="test" value="card"  onClick="changeModeOfPayment(this);"/><label class="form_label">Debit Cards</label>
 </form>
</div>

<div class="field_wrapper">
<form name="confirmTrans" id="confirmTrans" method="post" action="">
<input id="benName" name="benName" type="hidden" value="<? echo $_SESSION['accountName'] ?>" />
<input id="benCountry" name="benCountry" type="hidden" value="<? echo $_SESSION['benCountry'] ?>" />
<input id="benCurrency" name="benCurrency" type="hidden" value="<? echo $_SESSION['benCurrency'] ?>" />
<input id="accountName" name="accountName" type="hidden" value="<? echo $_SESSION['accountName'] ?>" />
<input id="accountNumber" name="accountNumber" type="hidden" value="<? echo $_SESSION['accountNumber'] ?>" />
<input id="branchNameNumber" name="branchNameNumber" type="hidden" value="<? echo $_SESSION['branchNameNumber'] ?>" />
<input id="branchAddress" name="branchAddress" type="hidden" value="<? echo $_SESSION['branchAddress'] ?>" />
<input id="routingNumber" name="routingNumber" type="hidden" value="<? echo $_SESSION['routingNumber'] ?>" />
<input id="sortCode" name="sortCode" type="hidden" value="<? echo $_SESSION['sortCode'] ?>" />
<input id="swift" name="swift" type="hidden" value="<? echo $_SESSION['swift'] ?>" />
<input id="reference" name="reference" type="hidden" value="<? echo $_SESSION['reference'] ?>" />
<input id="existBen" name="existBen" type="hidden" value="<? echo $_SESSION['existBen'] ?>" />
<input id="iban" name="iban" type="hidden" value="<? echo $_SESSION['iban'] ?>" />
<input type="hidden" name="transType" value="Bank Transfer" />
<!--<input id="sendingCurrency" name="sendingCurrency" type="hidden" value="<? //echo $_SESSION['sendingCurrency'] ?>" /> -->
<!-- changed against @ 12703 -->
<input id="sendingCurrency" name="sendingCurrency" type="hidden" value="<? echo $_POST['sendingCurrency'] ?>" />
</div>
							
                                             
<div id="bankConfirm" class="field_wrapper" style="display:none" >
<input name="bankConfirmb" id="bankConfirmb" type="submit" onClick="this.form.action='payment_detailnew.php';this.form.submit();" value="Continue" class="submit_btn"/>
</div>						
<div class="field_wrapper" id="debitCardConfirm" style="display:none" >													
<input name="debitCardConfirmb" id="debitCardConfirmb" type="submit" onClick="this.form.action='debit_card_bank.php';this.form.submit();" value="Continue" class="submit_btn"  />
</div>

</div>

<!-- content area right-->
<div class="content_area_right">
<!--<div class="info_change_password margin_0"><p>Please select the method of payment you wish to choose - Please choose how you wish to pay for your transaction. Bank transfer or secure immediate debit card payment.</p></div>-->
</div>
</div>
<!-- content area ends here-->
</div>



<!-- footer area -->
<?php include('footer.php');?>
</div>



</div>
</body>
</html>
