<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: Transaction History
	 * @author: Mirza Arslan Baig
	 */
	
	
	session_start();
	
	
	
	
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	$custIDi = $_SESSION['loggedInUser']['userID'];
	
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	include_once "includes/functions.php";
	
	if(isset($_GET['limit']) && $_GET['limit'] > 0)
		$limit = intval(trim($_GET['limit']));
	else
		$limit = 5;
	if(!empty($_GET['offset']))
		$offset = $_GET['offset'];
	elseif(!empty($_POST['offset']))
		$offset = $_POST['offset'];
	else
		$offset = 0;
		
	$next = $offset + $limit;
	$prev = $offset - $limit;
	
	dbConnect();
	//print_r($_REQUEST);
	$intSearchFlag = false;
	if(!empty($_POST['search']) || !empty($_GET['search'])){
		$intSearchFlag = true;
		
		
		if(!empty($_POST['search'])){
		
			
			
			$fromDate=$_POST['fromDate'];
			sscanf($fromDate, '%d-%d-%d', $d, $m, $Y);
			$strFromDate = date("Y-m-d", strtotime("$Y-$m-$d"));
			
			$strFromDate=$strFromDate." 00:00:00";
			$toDate=$_POST['toDate'];
			
			sscanf($toDate, '%d-%d-%d', $d, $m, $Y);
			$strToDate = date("Y-m-d", strtotime("$Y-$m-$d"));
			$strToDate=$strToDate." 23:59:59";
			
			// $strSearchText = trim($_POST['searchText']);
			// $strSearchType = $_POST['searchType'];
			// $strAmountSearchCriteria = $_POST['amountSearch'];
			// $strAmount = trim($_POST['amount']);
			// $strCurrency = $_POST['currency'];
			// $strTransStatus = $_POST['status'];
			// $strApplyOnAmount = $_POST['applyOnAmount'];
			// $strCondition = " ";
		}else{
			$strFromDate=$_GET['fromDate']." 00:00:00";
			$strToDate = $_GET['toDate']." 23:59:59";
			$strSearchText = trim($_GET['searchText']);
			
			// $strSearchType = $_GET['searchType'];
			// $strAmountSearchCriteria = $_GET['amountSearch'];
			// $strAmount = trim($_GET['amount']);
			// $strCurrency = $_GET['currency'];
			// $strTransStatus = $_GET['status'];
			// $strApplyOnAmount = $_GET['applyOnAmount'];
			// $strCondition = " ";
		}
		// if(!empty($strSearchText) && $strSearchType == 'transRefNumber')
			// $strCondition .= " AND refNumberIM = '$strSearchText'";
		// elseif(!empty($strSearchText) && $strSearchType == 'beneficiaryName')
			// $strCondition .= " AND (ben.firstName LIKE '$strSearchText%' OR ben.middleName LIKE '$strSearchText%' OR ben.lastName LIKE '$strSearchText%' OR ben.beneficiaryName LIKE '$strSearchText%') ";
		
		// if(!empty($strApplyOnAmount)){
			// if($strApplyOnAmount == 'sendingAmount')
				// $strApplyAmount = "transAmount";
			// elseif($strApplyOnAmount == 'receivingAmount')
				// $strApplyAmount = "localAmount";
			// else
				// $strApplyAmount = "transAmount";
		// }
		
		// if($strAmountSearchCriteria != 'all' && !empty($strAmountSearchCriteria)){
			// switch($strAmountSearchCriteria){
				// case ">":
					// $strCondition .= " AND $strApplyAmount > '$strAmount' ";
					// break;
				// case "<":
					// $strCondition .= " AND $strApplyAmount < '$strAmount' ";
					// break;
				// case "=":
					// $strCondition .= " AND $strApplyAmount = '$strAmount'";
					// break;
				// case ">=":
					// $strAmount .= " AND $strApplyAmount >= '$strAmount' ";
					// break;
				// case "<=":
					// $strCondition .= " AND $strApplyAmount <= '$strAmount' ";
					// break;
			// }
		// }
		
		// if(!empty($strCurrency) && $strCurrency != 'all'){
			// $strCondition .= " AND currencyFrom = '$strCurrency' ";
		// }
		
		// if($strTransStatus != 'all' && !empty($strTransStatus))
			// $strCondition .= " AND transStatus = '$strTransStatus' ";
		$strCondition .= " AND (transDate >= '$strFromDate' AND transDate <= '$strToDate')";
		
		
		$strCondition .= " AND trans_source = 'O'";
		$strQuerySearch = "SELECT refNumberIM AS refNumber, transStatus, DATE_FORMAT(transDate, '%d-%m-%Y') AS transDate, FORMAT(transAmount, 2) AS sendingAmount, FORMAT(localAmount, 2) AS recievingAmount, FORMAT(exchangeRate, 4) AS rate,currencyFrom AS sendingCurrency, currencyTo AS recievingCurrency, CONCAT(ben.firstName, ben.middleName, ben.lastName) AS benName FROM ".TBL_TRANSACTIONS." AS trans INNER JOIN ".TBL_BENEFICIARY." AS ben ON ben.benID = trans.benID WHERE trans.customerID = '".$_SESSION['loggedInUser']['userID']."'".$strCondition;
		$strQueryCountTotal = "SELECT COUNT(transID) AS records FROM ".TBL_TRANSACTIONS." AS trans INNER JOIN ".TBL_BENEFICIARY." AS ben ON ben.benID = trans.benID WHERE trans.customerID = '".$_SESSION['loggedInUser']['userID']."'".$strCondition;
		
		
		$strQueryGroupBy = " group by transDate DESC";
		$strQueryPagination = " LIMIT $offset, $limit";
		$strQuerySearch .= $strCondition;
		$strQuerySearch .= $strQueryGroupBy;
		$exportQuery = $strQuerySearch;
		$strQuerySearch .= $strQueryPagination;
		$arrTotalRecords = selectFrom($strQueryCountTotal);
		$intTotalRecords = $arrTotalRecords['records'];
		$arrTrans = selectMultiRecords($strQuerySearch);
		$intTotalTrans = count($arrTrans);
		
	}
		$strUserId=$_SESSION['loggedInUser']['accountName'];
		$strEmail=$_SESSION['loggedInUser']['email'];
?>
<!DOCTYPE HTML>  
<html>
	<head>
		<title>Private Client Registration Form - Premier FX Online Currency Transfers, Algarve, Portugal</title>
		<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." /><meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading, Premier FX, Algarve, Portugal, www.premfx.com" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="imagetoolbar" content="no">
		<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
		
		<script type="text/javascript" src="javascript/jquery.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript">
$(document).ready(function(){
	Date.format = 'dd-mm-yyyy';
	$("#fromDate").datePicker({
		startDate: '01-01-2000',
		clickInput:true,
			});
	
	
	
	$("#toDate").datePicker({
		startDate: '01-01-2000',
		clickInput:true
	});
	
	$("#printReport").click(function(){
		// Maintain report print logs 
		$.ajax({
			url: "maintainReportsLogs.php",
			type: "post",
			data:{
				maintainLogsAjax: "Yes",
				pageUrl: "<?=$_SERVER['PHP_SELF']?>",
				action: "P"
			}
		});
		
		$("#searchTable").hide();
		$("#pagination").hide();
		$("#actionBtn").hide();
		$(".actionField").hide();
		print();
		$("#searchTable").show();
		$("#pagination").show();
		$("#actionBtn").show();
		$(".actionField").show();
	});

	
	//************ajax call for ref number************//
	
		$("#searchText").keyup(function(){
		var searchType = $("#searchType").val();
		var searchText = $("#searchText").val();
		var custIDi	   = $("#userIDi").val();
		var htmlData='';
		
		$.ajax({
			url: "../admin/registration_form_conf.php",
			type: "post",
			async:false,
			error: function( xhr, status )
			{
            console.log("xhr : "+xhr+" Status: "+status);
			},
			data:{
				action: "showRefNumber",
				searchType 	:	searchType, 
				searchText	:	searchText,
				custIDi 	:	custIDi
			},
			success: function(data){
			htmlData = data ;
			}
		});
		
		$("#showRefNumber").html(htmlData);
		$("#_showRefNumber").show();
			
	});	
	
	});

function selectRefNumber(getid){
var ids = $("#searchText").val(getid);
$("#_showRefNumber").hide();

}

	
	</script>
	</head> 
	<body>

				
				
<!-- main container-->
<div class="container">
<div class="upper_container">
<div class="header">
<div class="logo">
<a href="http://www.premfx.com"><img id="logo" src="images/logo_png.png"></a>
</div>
<div class="menu">
<ul class="menu_ul">
<li class="menu_li"><a href="make_payment-ii-new.php" class="menu_link" target="_parent">Send Money</a></li>
<li class="menu_li"><a href="transaction_history_new.php" class="menu_link menu_selected" target="_parent">Transaction History</a></li>
<li class="menu_li"><a href="view_beneficiariesnew.php" class="menu_link" target="_parent">Beneficiaries</a></li>
<li class="menu_li"><a href="change_passwordnew.php" class="menu_link" target="_parent">Change Password</a></li>
<li class="menu_li"><a href="contact_us.php" class="menu_link" target="_parent">Contact Us</a></li>
<li class="menu_li"><a href="faqs.php" class="menu_link" target="_parent">FAQs</a></li>
</ul>
</div>

</div>
</div>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
				<div class="logout_area">
<h2 class="heading">Transaction History</h2>
<?php include('top-menu.php');?>



<!-- content area -->


<!-- content area -->
<div class="content_area">
<div class="content_area_center">
<p class="content_heading">Search Your Transactions</p>
<p class="content_subheading">All transactions you make through PFX Online are logged for your reference, you may search your transaction history using the form
below.</p>
</div>
<!-- content area left-->
<div class="content_area_left">
 <form id="filters" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">

<div class="field_wrapper">
<label class="form_label">Date from:</label><br>
<input name="fromDate" id="fromDate" type="text" class="form_fields calender_bg" autocomplete="off" value="<?=$fromDate?>" />

</div>

<!--<div class="field_wrapper">
<label class="form_label">Search criteria:</label><br>
<select name="searchType" class="select_field" id="searchType">
<option value="" <?php if($strSearchType ==''){echo 'selected';}?>>- Select Search Criteria -</option>
<option value="transRefNumber" <?php if($strSearchType =='transRefNumber'){echo 'selected';} ?>>Transaction Reference Number</option>
<option value="beneficiaryName" <?php if($strSearchType =='beneficiaryName'){echo 'selected';}?>>Beneficiary Name</option>
</select>
</div>-->

<!--<div class="field_wrapper">
<label class="form_label">Amount criteria:</label><br>
<select id="amountSearch" class="select_field" name="amountSearch">

<option value="" <?php if($strAmountSearchCriteria=='')  {echo 'selected';}?> >- Select Amount Criteria -</option>
<option value=">" <?php if($strAmountSearchCriteria=='>'){echo 'selected';}?>>Greater Than</option>
<option value="<" <?php if($strAmountSearchCriteria=='<'){echo 'selected';}?> >Less Than</option>
<option value="=" <?php if($strAmountSearchCriteria=='='){echo 'selected';}?>>Equal to</option>
<option value=">=" <?php if($strAmountSearchCriteria=='>='){echo 'selected';}?>>Greater than and equal to</option>
<option value="<=" <?php if($strAmountSearchCriteria=='<='){echo 'selected';}?>>Less than and equal to</option>
</select>
</div>-->
<!--<div class="field_wrapper">
<label class="form_label">Apply on:</label><br>
<select name="applyOnAmount" class="select_field" id="applyOnAmount">
<option value="transAmount"   <?php if($strApplyOnAmount =='transAmount'){echo 'selected';}?>>- Select Amount to apply on -</option>
<option value="sendingAmount" <?php if($strApplyOnAmount=='sendingAmount'){echo 'selected';}?>>Sending Amount</option>
<option value="receivingAmount" <?php if($strApplyOnAmount=='receivingAmount'){echo 'selected';}?> >Receving Amount</option>
</select>
</div>-->

<!--<div class="field_wrapper">
<input type='hidden' name='userIDi' id='userIDi' value='<?echo $custIDi ?>' />
<input name="search" id="search" class="submit_btn" type="submit" value="Search">

</div>-->
</div>

<!-- content area right-->
<div class="content_area_right margin_10">
<br>

<div class="field_wrapper">
<label class="form_label">Date to:</label><br>
<input name="toDate" id="toDate" type="text" class="form_fields calender_bg" autocomplete="off" value="<?=$toDate?>" />

</div>

<!--<div class="field_wrapper">
<label class="form_label">Search for:</label><br>

<input name="searchText" value="<?=$strSearchText?>" id="searchText" type="text"  class="form_fields" />
</div>-->

<!--<div id="_showRefNumber"  style="display:none" class="field_wrapper">
<div id="showRefNumber" name="showRefNumber" > 
</div>
</div>-->



<!--<div class="field_wrapper">
<label class="form_label">Amount:</label><br>
<input name="amount" value="<?=$strAmount?>" id="amount" type="text" class="form_fields" />
</div>-->

<!--<div id="_showExpectedAmount"  style="display:none" class="field_wrapper">
<div id="showExpectedAmount" name="showExpectedAmount" > 
	
</div>
</div>-->


<!--<div class="field_wrapper">
<label class="form_label">Currency:</label><br>
<select name="currency" class="select_field" id="currency">
<option value="all">All</option>
<?php 

// $strQueryCurrency = "SELECT DISTINCT quotedCurrency FROM ".TBL_ONLINE_EXCHANGE_RATES." ORDER BY quotedCurrency ASC";
// $arrCurrency = selectMultiRecords($strQueryCurrency);
// for($x = 0;$x < count($arrCurrency);$x++){

// if($strCurrency == ''){
// echo "<option value='".$arrCurrency[$x]['quotedCurrency']."'>".$arrCurrency[$x]['quotedCurrency']."</option>";
// }
// else if(!empty($strCurrency))
// {
// if($strCurrency == $arrCurrency[$x]['quotedCurrency']){
// echo "<option value='".$arrCurrency[$x]['quotedCurrency']."' selected>".$arrCurrency[$x]['quotedCurrency']."</option>";
// }
// else {
// echo "<option value='".$arrCurrency[$x]['quotedCurrency']."'>".$arrCurrency[$x]['quotedCurrency']."</option>";
// }
// }
// }
?>
</select>
</div>-->


</div>

<div class="button_wrapper">
<input type='hidden' name='userIDi' id='userIDi' value='<?echo $custIDi ?>' />
<input name="search" id="search" class="submit_btn" type="submit" value="Search">

</div>
</form>
<div class="position_btn">
<form action="export_transaction.php?query=<? echo base64_encode(urlencode($exportQuery)); ?>&querycount=<? echo $intTotalRecords; ?>" method="post" style="display:inline-block;">
<input id="export" class="submit_btn" type="submit" value="Export to Excel" >
</form>
</div>


</div>

<!-- table area -->

											<?php 
						if($intTotalTrans > 0){
					?>
					<div class="content_area">
<div class="table_area">
<div >
					<table width="100%" border="0">
						<tr class="desc_tr">
							<td class="desc_td">Showing <?php echo ($offset+1)." - ".($intTotalTrans + $offset)." of ".$intTotalRecords ?></td>
							<td class="desc_td">
								Records Per Page:
								<select name="limit" id="limit" class="input-small" onChange="javascript:document.location.href='<?php echo $_SERVER['PHP_SELF']; ?>?offset=0&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&search=search&limit='+this.value;" >
									<option value="5" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '5'){ echo ' selected="selected"';}?>>5</option>
									<option value="10" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '10'){ echo ' selected="selected"';}?>>10</option>
									<option value="20" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '20'){ echo ' selected="selected"';}?>>20</option>
									<option value="30" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '30'){ echo ' selected="selected"';}?>>30</option>
									<option value="50" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '50'){ echo ' selected="selected"';}?>>50</option>
								</select>
							</td>
							<td class="desc_td" >
								<?php 
									if($prev >= 0){
								?>
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=0&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=search">First</a>&nbsp;
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $prev;?>&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=search">Previous</a>&nbsp;
								<?php 
									}
									if($next > 0 && $next < $intTotalRecords){
								?>
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $next; ?>&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=search">Next</a>&nbsp;
									<?php 
										$intLastOffset = (ceil($intTotalRecords / $limit) - 1) * $limit;
									?>
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $intLastOffset;?>&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=search">Last</a>
								<?php 
									}
								?>
							</td>
						</tr>
					</table>
					<table width="100%" border="1">
						<tr class="desc_tr">
							<th class="desc_td" width="170px;">
								Transaction Reference No.
								
							</th>
							<th class="desc_td">
								Transaction Date
							</th>
							<th class="desc_td">
								Benficiary Name
							</th>
							<th class="desc_td">
								Sending Amount
							</th>
							<th class="desc_td">
								Sending Currency
							</th>
							<th class="desc_td">
								Receiving Amount
							</th>
							<th class="desc_td">
								Receiving Currency
							</th>
							<th width="50px" class="desc_td">
								Rate
							</th>
						</tr>
						<?php 
							for($x = 0;$x < $intTotalTrans;$x++){
						?>
						<tr class="desc_tr">
							<td class="desc_td" width="80px;">
								<?php echo $arrTrans[$x]['refNumber']; ?>
							</td>
							<td class="desc_td">
								<?php echo $arrTrans[$x]['transDate']; ?>
							</td>
							<td class="desc_td">
								<?php echo $arrTrans[$x]['benName']; ?>
							</td>
							<td class="desc_td">
								<?php 
								?>
								<?php echo $arrTrans[$x]['sendingAmount']; ?>
							</td>
							<td class="desc_td">
								<?php echo $arrTrans[$x]['sendingCurrency']; ?>
							</td>
							<td class="desc_td">
								<?php echo $arrTrans[$x]['recievingAmount']; ?>
							</td>
							<td class="desc_td">
								<?php echo $arrTrans[$x]['recievingCurrency']; ?>
							</td>
							<td class="desc_td" width="50px">
								<?php echo $arrTrans[$x]['rate']; ?>
							</td>
							<!--<td width="50px">
								<?php echo $arrTrans[$x]['transStatus']; ?>
							</td>-->
						</tr>
						<?php 
							}
						?>
					</table>
											</div>
</div>
</div>
					<?php 
						}elseif(isset($_REQUEST['search'])){
					?>
											<div class="content_area">
<div class="table_area">
<div >				
					<table width="100%">
							<tr class="desc_tr">
								<td class="desc_td" style="  text-align: center;">There are no transaction(s) to display.</td>
							</tr>
						</table>
						</div>
</div>
</div>
					<?php 
						}
					?>
										 

<!-- content area ends here-->
</div>
<!-- footer area -->
<?php include('footer.php');?>
</div>



</div>
</body>
	
	<script>
			/*
			var succssMsg = '';
			var fileValidateFlag = 0;
			$(function() {
				$('#addressContainer').hide();
				var button = $('#file');
				var fileUpload = new AjaxUpload(button, {
					action: 'registration_form_conf.php',
					name: 'passportDoc',
					
					autoSubmit: false,
					allowedExtensions: ['.jpg', '.png', '.gif', '.jpeg', '.pdf'],
					  
					onChange: function(file, ext) {
						if ((ext && /^(jpg|png|jpeg|gif|pdf)$/i.test(ext))) {
							var fileSizeBytes = this._input.files[0].size;
							if(fileSizeBytes < 2097152){
								fileValidateFlag = 1;
								button.css({
									"background": "#CCCCCC",
									"font-weight": "bold"
								});
								button.html('Document Selected');
							}else{
								alert("file is too large, maximum file size is 2MB");
							}
						} else {
							alert('Please choose a standard image file to upload, JPG or JPEG or PNG or GIF or PDF');
							button.css({
								"background": "#ECB83A",
								"font-weight": "normal"
							});
							button.html('Select Document');
						}
					},
					onSubmit: function(file, ext) {
						this.disable();
						$('#wait').fadeOut("slow");
						$('#wait').fadeIn("slow");
						$('#fileUploader').fadeIn("slow");
					},
					onComplete: function(file, response) {
						this.enable();
						$('#emailValidator').html(' ');
						$('#wait').fadeOut("fast");
						$('#fileUploader').fadeOut("slow");
						alert(response);
						$('#msg').html(succssMsg);
					}
				});
				$(document).ready(
				function() {
					Date.format = 'dd/mm/yyyy';
					$('#passportExpiry').datePicker({
						clickInput: true,
						createButton: false
					});
					$('#passportIssue').datePicker({
						clickInput: true,
						createButton: false,
						endDate: (new Date()).asString()
					});
					$('#idExpiry').datePicker({
						clickInput: true,
						createButton: false
					});
					$('#passportIssue').dpSetStartDate(' ');
					$('#passportNumber').mask('999999999-9-aaa-9999999-a-9999999-**************-9-9');
					$('#line1').mask('**-***-*********-9-***************');
					$('#line2').mask('999999-9-*-999999-9-aaa-***********');
					// Form Validation 
					$("#senderRegistrationForm").validate({
						rules: {
							forename: {
								required: true
							},
							surname: {
								required: true
							},
							postcode: {
								required: true
							},
							passportNumber: {
								required: function() {
									if ($('#line1').val() == '') return true;
									else return false;
								}
								//,minlength: 37
							},
							line1: {
								required: function() {
									if ($('#passportNumber').val() == '') return true;
									else return false;
								}
							},
							line2: {
								required: function() {
									if ($('#line1').val() == '') return false;
									else return true;
								}
							},
							line3: {
								required: function() {
									if ($('#line2').val() == '' || $('#line1').val() == '') return false;
									else return true;
								}
							},
							idExpiry: {
								required: function() {
									if ($('#line1').val() != '' && $('#line2').val() != '' && $('#line3').val() != '') return true;
									else return false;
								}
							},
							passportExpiry: {
								required: function() {
									if ($('#passportNumber').val() != '') return true;
									else return false;
								}
							}
						},
						messages: {
							passportNumber: "Please provide your Passport or National Identity card number",
							line1: "Please provide your Passport or National Identity card number",
							line2: "Please enter the remaining part of your National Identity card number",
							line3: "Please enter the remaining part of your National Identity card number",
							passportExpiry: "Please enter the expiry date of your Passport"
						},
						submitHandler: function() {
							register();
							return false;
						}
					});
					// Validating Email availability checks
					$("#email").blur(
					function() {
						var email = $('#email').val();
						if (email != '') {
							$.ajax({
								url: "registration_form_conf.php",
								data: {
									email: email,
									chkEmailID: '1'
								},
								type: "POST",
								cache: false,
								success: function(data) {
									$('#emailValidator').html(data);
								}
							});
						}
					});
					// Check if the passport availability checks
					$('#passportNumber').blur(
					function() {
						setTimeout(function() {
							passportAvailabilty();
						}, 100);
					});
					function passportAvailabilty() {
						var passport = $('#passportNumber').val();
						if (passport != '') {
							$.ajax({
								url: "registration_form_conf.php",
								data: {
									passportNum: passport,
									chkPassport: '1'
								},
								type: "POST",
								cache: false,
								success: function(data) {
									$('#passportAvailability').html(data);
								}
							});
						}
					}
					// ID card availability checks
					$('#line3').blur(
					function() {
						var idLine1 = $('#line1').val();
						var idLine2 = $('#line2').val();
						var idLine3 = $('#line3').val();
						if (idLine1 != '' && idLine2 != '' && idLine3 != '') {
							var idCard = idLine1 + idLine2 + idLine3;
						} else return false;
						$.ajax({
							url: "registration_form_conf.php",
							data: {
								idCardNumber: idCard,
								chkIDCard: '1'
							},
							type: "POST",
							cache: false,
							success: function(data) {
								$('#NICAvailability').html(data);
							}
						});
					});
					// Ajax Registration of Sender
					function register() {
						//Passport Issue and Expiry Date
						var passportIssue = new Date($('#passportIssue').val());
						var passportExpiry = new Date($('#idExpiry').val());
						if (passportIssue != '') {
							if (passportIssue >= passportExpiry) {
								alert("Your Passport issued date must be before the expiry date.");
								return false;
							}
						}
						// Registration Request
						var data = $('#senderRegistrationForm').serialize();
						//alert(data);
						data += "&register=Register";
						data += "&slog=yes";
						$('#wait').fadeIn("fast");
						$("#loadingMessage").text('Submitting your registration');
						$.ajax({
							url: "registration_form_conf.php",
							data: data,
							type: "POST",
							cache: false,
							success: function(msg) {
								if (msg.search(/Sender is registered successfully/i) >= 0) {
									if (fileValidateFlag != 0) {
										succssMsg = msg;
										$("#loadingMessage").text('Please wait for the confirmation of your passport upload.');
										var plog = "yes";
										var chk = "1";
										$.ajax({
										url: "registration_form_conf.php",
										data: {
											plog: plog,
											chk: chk
										},
										type: "POST",
										cache: false,
										success: function(msg) {
										}});
										fileValidateFlag = 0;
										fileUpload.submit();
									}else{
										$('#msg').html(msg);
										$('#wait').fadeOut("fast");
									}
									resetFormData();
								}else{
									//$('#wait').fadeOut("fast");
									$('#msg').html(msg);
									$('#wait').fadeOut("fast");
								}
							}
						});
					}
					function resetFormData() {
						$('#addressContainer').fadeOut('fast');
						document.forms[0].reset();
						$('#file').css({
							"background-color": "#ECB83A",
							"border": "1px solid #E5A000",
							"font-weight": "normal"
						});
						$('#file').html('Select Document');
						$('#senderRegistrationForm')[0].reset();
					}
					// Trigger the search address function
					$('#searchAddress').click(
					function() {
						searchAddress();
					});
				});
			});
			// Populate the Address in the fields
			function getAddressData(ele) {
				var value = ele.value;
				var arrAddress = value.split('/');
				var buildingNumber = $.trim(arrAddress[0]);
				var buildingName = $.trim(arrAddress[1]);
				var subBuilding = $.trim(arrAddress[2]);
				var street = $.trim(arrAddress[3]);
				var subStreet = $.trim(arrAddress[4]);
				var town = $.trim(arrAddress[5]);
				//var postcode = $.trim(arrAddress[6]);
				var organization = $.trim(arrAddress[7]);
				var buildingNumberVal = '';
				var buildingNameVal = '';
				var streetValue = '';
				var postCode = $('#postCodeSearch').val();
				if (buildingNumber != '') buildingNumberVal += buildingNumber;
				if (buildingName != '') buildingNameVal += buildingName;
				if (subBuilding != '') buildingNameVal += ' ' + subBuilding;
				if (street != '') streetValue += street;
				if (subStreet != '') streetValue += ' ' + subStreet;
				$('#buildingNumber').val(buildingNumberVal);
				$('#buildingName').val(buildingNameVal);
				$('#street').val(streetValue);
				$('#town').val(town);
				$('#postcode').val(postCode);
			}
			// if Press Enter on any field in the address area trigger the search address function
			function enterToSearch(e) {
				if (e.which) {
					keyCode = e.which;
					if (keyCode == 13) {
						e.preventDefault();
						searchAddress();
					}
				}
			}
			// Calls the API for suggessted address
			function searchAddress() {
				$('#residenceCountry').val('United Kingdom');
				$('#addressContainer').fadeOut('fast');
				postcode = $.trim($('#postCodeSearch').val());
				buildingNumber = $.trim($('#buildingNumber').val());
				street = $.trim($('#street').val());
				town = $.trim($('#town').val());
				if (postcode == '') {
					alert("Enter a postcode to search for your address");
					$('#postCodeSearch').focus();
					return;
				}
				$("#loadingMessage").text('Searching Address...');
				$('#wait').fadeIn("fast");
				$.ajax({
					url: "http://premierfx.live.hbstech.co.uk/api/gbgroup/addresslookupCus.php",
					data: {
						postcode: postcode,
						buildingNumber: buildingNumber,
						street: street, 
						town: town
					},
					type: "POST",
					cache: false,
					success: function(data) {
						//alert(data.match(/option/i));
						$('#wait').fadeOut("slow");
						if (data.search(/option/i) >= 0) {
							$('#addressContainer').fadeIn('fast');
							$('#suggesstions').html(data);
						} else {
							$('#addressContainer').fadeIn('fast');
							$('#suggesstions').html('<i style="color:#FF4B4B;font-family:arial;font-size:11px">Sorry there was no address found against your postal code</i>');
						}
					}
				});
			}
			// Clear the address section
			function clearAddress() {
				$('#buildingNumber').val('');
				$('#buildingName').val('');
				$('#street').val('');
				$('#town').val('');
				$('#province').val('');
			}
			function passportMask() {
				passportCountry = $('#passportCountry').val();
				switch (passportCountry) {
				case 'United Kingdom':
					 $('#passportNumber').unmask().mask('*********-9-***-9999999-a-9999999-<<<<<<<<<<<<<<-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				case 'Portugal':
					 $('#passportNumber').unmask().mask('*******<<-9-***-9999999-a-9999999-**********<<<<-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				case 'USA':
					$('#passportNumber').unmask().mask('*********-9-***-9999999-a-9999999-*********<****-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				case 'Australia':
					 $('#passportNumber').unmask().mask('********<-9-***-9999999-a-9999999-<*********<<<<-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				case 'Spain':
					$('#passportNumber').unmask().mask('********<-9-aaa-9999999-a-9999999-***********<<<-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				default:
					alert('Please select a country and enter your passport number');
					break;
				}
			}
			*/
			</script>
</html>
